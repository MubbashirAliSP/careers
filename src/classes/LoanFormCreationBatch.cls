/**
 * @description Batch class to handle creation of Loan Application Forms after a specified delay from the submission of the related Enrolment Application Form
 * @author Ranyel Maliwanag
 * @date 07.OCT.2015
 * @history
 *       16.NOV.2015    Ranyel Maliwanag    - Added `IsExcludedFromLoanBatch__c` condition to the batch query to exclude enrolment forms that are specificially marked for exclusion in the Loan Form Creation process
 *       08.DEC.2015    Ranyel Maliwanag    - Revised computation of holiday offsets
 *       10.JAN.2015    Biao Zhang          - Line 156, quick fix for test method 'testMinuteDelay' 
 *       03.MAR.2016    Ranyel Maliwanag    - Added condition to the batch query to only send out Loan Forms to Enrolment Applications that don't have their Service Cases in Closed status
 *       19.APR.2016    Biao Zhang          - Only generate loan forms for VFH applications
 */
public without sharing class LoanFormCreationBatch implements Database.Batchable<SObject> {
    public String queryString;
    public ApplicationsPortalCentralSettings__c portalSettings;
    public Integer daysDelayed;
    public Integer minutesDelayed;
    public Datetime referenceDate;
    public Datetime today;

    public LoanFormCreationBatch(Datetime nowStamp) {
        portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        daysDelayed = Integer.valueOf(portalSettings.LoanApplicationFormCreationOffset__c);
        minutesDelayed = null;

        if (portalSettings.MinutesDelayed__c != null) {
            minutesDelayed = Integer.valueOf(portalSettings.MinutesDelayed__c);
        }

        today = Datetime.now();
        if (nowStamp != null) {
            today = nowStamp;
        }

        if (daysDelayed != null && daysDelayed > 0) {
            referenceDate = today.addDays(- daysDelayed);
        }

        // Minutes override
        if (minutesDelayed != null && minutesDelayed > 0) {
            referenceDate = today.addMinutes(- minutesDelayed);
        }

        queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'WHERE RecordType.Name = \'Enrolment\' AND Status__c = \'Submitted\' AND LinkedLoanFormId__c = null AND SubmissionDate__c < :referenceDate AND IsExcludedFromLoanBatch__c != true AND ServiceCaseId__r.Status__c != \'Closed\' AND FundingStreamTypeId__r.FundingStreamShortname__c = \'VFH\'';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 07.OCT.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 07.OCT.2015
     */
    public void execute(Database.BatchableContext BC, List<ApplicationForm__c> records) {
        Id loanRTID = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId();
        // Mapping of <Enrolment Application Form> : <Loan Application Form>
        Map<ApplicationForm__c, ApplicationForm__c> linkedEnrolmentLoanForms = new Map<ApplicationForm__c, ApplicationForm__c>();
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault = true];
        Date todayDt = today.date();

        // Get earliest submitted date
        Datetime earliestSubmission = null;
        for (ApplicationForm__c record : records) {
            if ((earliestSubmission == null) || (earliestSubmission != null && record.SubmissionDate__c < earliestSubmission)) {
                earliestSubmission = record.SubmissionDate__c;
            }
        }
        Date referenceDateDt = earliestSubmission.date();

        // Get all public holidays that span the period between the earliest submitted EAF and today
        List<Holiday__c> holidays = [SELECT Id, StateId__c, Date__c FROM Holiday__c WHERE Date__c >= :referenceDateDt and Date__c <= :todayDt];

        // Get a set of all state Ids for the mapping piece
        Set<Id> stateIds = new Set<Id>();
        for (ApplicationForm__c record : records) {
            if (String.isNotBlank(record.StateId__c)) {
                stateIds.add(record.StateId__c);
            }
        }

        // Build a mapping of holidays per state
        // Mapping of <State Id> : (Mapping of <Date> : <Holiday>)
        Map<Id, Map<Date, Holiday__c>> stateHolidayMap = new Map<Id, Map<Date, Holiday__c>>();
        for (Id stateId : stateIds) {
            stateHolidayMap.put(stateId, new Map<Date, Holiday__c>());
        }

        for (Holiday__c holiday : holidays) {
            if (BusinessHours.isWithin(bh.Id, holiday.Date__c)) {
                // Condition: Holiday falls on a business day
                // We'll only offset holidays that fall on a business day
                if (String.isNotBlank(holiday.StateId__c)) {
                    // Condition: Holiday is state-specific
                    // Add only to the affected state
                    if (stateHolidayMap.containsKey(holiday.StateId__c)) {
                        // Condition: State Holiday is applicable to at least one form
                        stateHolidayMap.get(holiday.StateId__c).put(holiday.Date__c, holiday);
                    }
                } else {
                    // Condition: Holiday is not state-specific
                    for (Id stateId : stateHolidayMap.keySet()) {
                        stateHolidayMap.get(stateId).put(holiday.Date__c, holiday);
                    }
                }
            }
        }


        //// Build a list of state-specific holiday map
        //// Mapping of <State Id> : <Holiday Offset>
        //Map<Id, Integer> stateHolidayMap = new Map<Id, Integer>();
        //Integer generalOffset = 0;
        //for (Holiday__c holiday : holidays) {
        //    if (BusinessHours.isWithin(bh.Id, holiday.Date__c)) {
        //        // Condition: Holiday falls in a business day
        //        // We'll only offset holidays that fall on a business day
        //        if (String.isNotBlank(holiday.StateId__c)) {
        //            // Condition: Holiday is state-specific. Will be added to the map
        //            if (stateHolidayMap.containsKey(holiday.StateId__c)) {
        //                // Increment existing offset
        //                Integer existingOffset = stateHolidayMap.get(holiday.StateId__c);
        //                existingOffset += 24;
        //                stateHolidayMap.put(holiday.StateId__c, existingOffset);
        //            } else {
        //                // Introduce a new state offset
        //                stateHolidayMap.put(holiday.StateId__c, 24);
        //            }
        //        } else {
        //            // Condition: Holiday is not state-specific, so offset will apply to all
        //            generalOffset += 24;
        //        }
        //    }
        //}

        // Go over the Application Form records for this batch
        for (ApplicationForm__c record : records) {
            // Ensure that record has aged properly
            // Compute the business hours difference between submission timestamp and the current time
            Datetime now = Datetime.now();
            if (todayDt != Date.today()) {
                now = Datetime.newInstance(todayDt, Time.newInstance(0, 0, 0, 0));
            }
            Integer submissionAge = Integer.valueOf(BusinessHours.diff(bh.Id, record.SubmissionDate__c, now) / 1000 / 60 / 60);
            if (minutesDelayed != null && minutesDelayed > 0) {
                /*
                 * @history
                 *      Biao Zhang - 10.JAN.2016 Fix for the test method "testMinuteDelay"
                 */
                //submissionAge = Integer.valueOf(BusinessHours.diff(bh.Id, record.SubmissionDate__c, now) / 1000 / 60);
                submissionAge = Integer.valueOf(BusinessHours.diff(bh.Id, record.SubmissionDate__c, today) / 1000 / 60);
            }

            //submissionAge -= generalOffset;
            if (stateHolidayMap.containsKey(record.StateId__c)) {
                for (Holiday__c holiday : stateHolidayMap.get(record.StateId__c).values()) {
                    if (holiday.Date__c >= record.SubmissionDate__c.date()) {
                        // Condition: Holiday is on or after the submission date
                        // Delay ageing by 24 hours for that holiday
                        submissionAge -= 24;
                    }
                }
            }

            if (submissionAge >= 48) {
                // Clone the Enrolment Application Form record that will serve as a basis for the new Loan Application Form record
                ApplicationForm__c loanForm = record.clone();

                // Field initialisations for the Loan Application Form
                loanForm.RecordTypeId = loanRTID;
                loanForm.Status__c = 'Draft';
                loanForm.LinkedDoubleEnrolmentFormId__c = null;
                loanForm.IsMainApplicationForm__c = true;
                loanForm.LinkedEnrolmentFormId__c = record.Id;
                loanForm.SubmissionDate__c = null;

                if (String.isNotBlank(portalSettings.LoanFormQueueId__c)) {
                    loanForm.OwnerId = portalSettings.LoanFormQueueId__c;
                }

                // Add a mapping entry for this Enrolment - Loan Application Form record relation
                linkedEnrolmentLoanForms.put(record, loanForm);
            }
        }

        // Insert the newly created Loan Application Form records directly from the map values in order to retain the link between Enrolment and Loan forms
        insert linkedEnrolmentLoanForms.values();

        // Go over the mapping piece to update the LinkedLoanFormId__c field with the new Ids from the recently inserted Loan forms
        for (ApplicationForm__c record : linkedEnrolmentLoanForms.keySet()) {
            record.LinkedLoanFormId__c = linkedEnrolmentLoanForms.get(record).Id;
        }

        // Commit the changes to the Enrolment Form to the database
        update new List<ApplicationForm__c>(linkedEnrolmentLoanForms.keySet());
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 07.OCT.2015
     */
    public void finish(Database.BatchableContext BC) {
    }
}