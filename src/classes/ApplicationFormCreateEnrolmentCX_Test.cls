/**
 * @description Test class for `ApplicationFormCreateEnrolmentCX` class
 * @author Ranyel Maliwanag
 * @date 03.AUG.2015
 */
@isTest
public with sharing class ApplicationFormCreateEnrolmentCX_Test {
    @testSetup
    static void controllerSetup() {
        CentralTriggerSettings__c triggerSettings = new CentralTriggerSettings__c(
                GuestLogin__c = true,
                GuestLogin_CreateAccounts__c = true
            );
        insert triggerSettings;
        
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();
        ApplicationsTestUtilities.initialiseEnrolmentApplication();
    }


    static testMethod void testConstructor() {
        Test.setCurrentPage(Page.ApplicationFormCreateEnrolment);
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        Test.startTest();
            System.debug(ApplicationsEnrolmentFieldMapping__c.getAll());
            ApexPages.StandardController stdController = new ApexPages.StandardController(form);
            ApplicationFormCreateEnrolmentCX controller = new ApplicationFormCreateEnrolmentCX(stdController);
            controller.evaluateRedirects();
        Test.stopTest();
    }


    static testMethod void testAnswer() {
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'DeliveryMode'];
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = form.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Online'
            );
        insert answer;

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(form);
            ApplicationFormCreateEnrolmentCX controller = new ApplicationFormCreateEnrolmentCX(stdController);
            controller.evaluateRedirects();
        Test.stopTest();
    }
}