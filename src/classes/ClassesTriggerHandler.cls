/**
 * @description Trigger Handler for Class Object
 * @author Janella Lauren Canlas
 * @date 06.NOV.2012
 *
 * HISTORY
 * - 06.NOV.2012	Janella Canlas - Created.
 */
public with sharing class ClassesTriggerHandler {
	/*
		This method will create Class Staff members depending on the related Intake Staff Members
	*/
	public static void insertClassStaffMembers(List<Classes__c> classList){
		Set<Id> intakeIds = new Set<Id>();
		List<Class_Staff_Member__c> csmList = new List<Class_Staff_Member__c>();
		//collect intake ids
		for(Classes__c c : classList){
			intakeIds.add(c.Intake__c);
		}
		//query staff member records related to the intake ids and the field Add Staff Member to all Classes checked
		List<Staff_Member__c> smList = [SELECT id,Intake__c 
										from Staff_Member__c 
										where Intake__c IN: intakeIds 
										and Add_staff_member_to_all_classes__c = true];
		//if list is not null, loop staff members and class to create new Class Staff Member records
		if(smList.size() > 0){
			for(Staff_Member__c s : smList){
				for(Classes__c c : classList){
					if(s.Intake__c == c.Intake__c){
						Class_Staff_Member__c csm = new Class_Staff_Member__c();
						csm.Intake_Staff_Member__c = s.Id;
						csm.Class__c = c.Id;
						csmList.add(csm);
					}
				}
			}
			//insert Class Staff Member records
			try{
				insert csmList;}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());
			}
		}
	}
}