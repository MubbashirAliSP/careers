public with sharing class PriorEducationalAchievmentObject {
	
	public String student_identifier {get;set;}
	public String prior_identifier {get;set;}
	
	public PriorEducationalAchievmentObject(String sId, String pId) {
		student_identifier = sId;
		prior_identifier = pId; 
	}

}