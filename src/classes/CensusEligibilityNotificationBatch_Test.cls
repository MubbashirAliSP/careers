/**
 * @description Test class for CensusEligibilityNotificationBatch class
 * @author Ranyel Maliwanag
 * @date 10.JUN.2016
 */
@isTest
public with sharing class CensusEligibilityNotificationBatch_Test {
	@testSetup
	static void setup() {
		Census__c census = SASTestUtilities.createCensus();            
		census.CensusDate__c = Date.Today().addDays(+16);
		insert census;
		
		Stage__c stage1 = new Stage__c(Name = 'Stage 1');
		insert stage1;
		
		Intake__c intake = [Select Stage__c From Intake__c];
		intake.Stage__c = stage1.Id;
		update intake;
		
		Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
		enrl.Assigned_Trainer__c = UserInfo.getUserId();
		update enrl;
	}


	/**
	 * @scenario Census task gets deleted, and batch runs again and tries to update the deleted task
	 * @acceptanceCriteria Should run normally without errors
	 * @acceptanceCriteria Should create a new Task and store it in the Census record
	 */
	static testMethod void deletedTaskTest() {
		Task testTask = new Task();
		insert testTask;

		Id priorId = testTask.Id;
		Census__c record = [SELECT Id, CensusTaskId__c FROM Census__c];
		record.CensusTaskId__c = testTask.Id;
		update record;

		delete testTask;

		Test.startTest();
			Database.executeBatch(new CensusEligibilityNotificationBatch());
		Test.stopTest();

		record = [SELECT Id, CensusTaskId__c FROM Census__c];
		System.assertNotEquals(priorId, record.CensusTaskId__c);
	}
}