/*Custom Enrolment Look up, used to override standard Enrolment
 *lookup functionality, for invoicing.
 * #Invoicing
 * @date 22.JAN.2014    W Malibago allow special characters in query
 * @date 08.AUG.2014    W Malibago update saveEnrolment()
 */


public with sharing class CustomEnrolmentLookupController {
    
    public Enrolment__c enrolment {get;set;} // new account to create
    public List<Enrolment__c> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    private Id RTOId;
    public String newString = System.currentPageReference().getParameters().get('accid');
    
 
          public CustomEnrolmentLookupController() {
            enrolment = new Enrolment__c();
            // get the current search string
            searchString = System.currentPageReference().getParameters().get('lksrch');
            runSearch();  
          }
            
         
          // performs the keyword search
          public PageReference search() {
            runSearch();
            return null;
          }
         
          // prepare the query and issue the search command
          private void runSearch() {
            // TODO prepare query string for complex serarches & prevent injections
            results = performSearch(searchString);               
          } 
         
          // run the search and return the records found. 
          private List<Enrolment__c> performSearch(string searchString) {
              //WBM 22.JAN.2014 Start
              
              //String newString = System.currentPageReference().getParameters().get('accid');
              if(newString.contains('\'')){
                  
                  newString = newString.replace('\'','\\\'');
                  system.debug('now: ' + newString);
                  
              }
              
            String soql = 'select id, name,Start_Date__c, End_Date__c, Enrolment_Status__c, Student__r.Name,Qualification__r.name, Qualification__r.Qualification_Name__c, Student__c from Enrolment__c where Student__r.Name =  \'' + newString + '\'';
            //WBM End
            if(searchString != '' && searchString != null)
              soql = soql +  ' and name LIKE \'%' + searchString +'%\'';
            soql = soql + ' limit 25';
            System.debug(soql);
            return database.query(soql);
            
         
          }
         
          // save the new account record
          public PageReference saveEnrolment() {
            enrolment.Student__c = newString; //08.AUG.2014 WBM
            insert enrolment;
            // reset the account
            enrolment = new Enrolment__c();
            return null;
          }
         
          // used by the visualforce page to send the link to the right dom element
          public string getFormTag() {
            return System.currentPageReference().getParameters().get('frm');
          }
         
          // used by the visualforce page to send the link to the right dom element for the text box
          public string getTextBox() {
            return System.currentPageReference().getParameters().get('txt');
          }
          
          
          @istest static void testController() {
          
            PageReference pageRef = Page.CustomEnrolmentLookUp;
            Test.setCurrentPage(pageRef);
             
            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('accId','0019000000hE3lw');          
            
            CustomEnrolmentLookupController controller = new CustomEnrolmentLookupController();
            
            controller.getFormTag();
            controller.getTextBox();
            controller.search();
            controller.saveEnrolment();
          }

}