/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchDiscipline implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchDiscipline() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPD_Description__c, IPD_Sync_on_next_update__c, Name, Id ';
        queryString += 'FROM InPlace_Discipline__c ';
        queryString += 'WHERE IPD_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<InPlace_Discipline__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (InPlace_Discipline__c record : records) {
            if (!exportedIdentifiers.contains(record.Name)) {
                InPlace__c forExport = new InPlace__c(
                        IPDiscipline_Description__c = record.IPD_Description__c,
                        IPDiscipline_DisciplineCode__c = record.Name
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.Name);
            }
            record.IPD_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchFaculty nextBatch = new InPlaceExportBatchFaculty();
        Database.executeBatch(nextBatch);
    }
}