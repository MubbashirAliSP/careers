@isTest
public class NATDataDefinitionNSWAPL_Test {
    
    public static final String FeeAmount = 'FeeAmount'; // value: 0000
    public static final String EquityFlag = 'EquityFlag'; // value: N
    public static final String BookingId = 'Enrolment__r.Booking_ID__c';
    public static final String CourseSiteId = 'Course_Site_ID__c';
    public static final String TrainingPlanDeveloped = 'Enrolment__r.Training_Plan_Developed__c';
    public static final String OutcomeIdentifierTrainingOrg = 'BlankField';//the value is based on enrolment status and hence added here but can be removed later
    public static final String CommitmentIdentifier = 'Enrolment__r.Commitment_Identifier__c';
    public static final String BlankField = 'BlankField';
    public static final String YearProgramCompleted = 'Year_Program_Completed__c';
    public static final String OutcomeIdentifierNational = 'AVETMISS_National_Outcome__c';
    public static final String PostalBuildingPropertyName = 'Postal_building_property_name__c';
    public static final String PostalFlatUnitDetails = 'Postal_flat_unit_details__c';
    public static final String PostalStreetNumber = 'Postal_street_number__c';
    public static final String PostalStreetName = 'Postal_street_name__c';
    public static final String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static final String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static final String PostalPostCode = 'PersonOtherPostalCode';
    public static final String StateIdentifier='State_Identifier__c';
    public static final String NationalProviderNumber  = 'Account.National_Provider_Number__c';
    public static final String TrainingOrganisationNationalProviderNumber = 'Training_Organisation__r.National_Provider_Number__c';
    public static Final String AssociatedCourseIdentifier = 'Enrolment__r.Qualification__r.Associated_Course_Identifier__c';

    
    public static final String StudentIdentifier = 'Student_Identifier__c';
    public static final String StudentIdentifier2 = 'Student_Identifer__c';
    public static final String ParentQualificationCode = 'Parent_Qualification_Code__c';
    public static final String TrainingOrganisationTrainingOrgIdentifier = 'Training_Organisation__r.Training_Org_Identifier__c';
    public static final String DeliveryLocationIdentifier = 'Delivery_Location_Identifier__c';
    public static final String LocName = 'Name';
    public static final String ChildPostcode = 'Child_Postcode__c';
    public static final String LocStateIdentifier = 'State_Identifier__c';
    public static final String ChildSuburb = 'Child_Suburb__c';
    public static final String CountryIdentifier = 'Country_Identifier__c';
    public static final String ParentQualificationName = 'Parent_Qualification_Name__c';
    public static final String ReportableQualificationHours = 'BlankField'; 
    public static final String RecognitionStatusIdentifier = 'Recognition_Status_Identifier__c';
    public static final String QualificationCategoryIdentifier = 'Qualification_Category_Identifier__c';
    public static final String FieldOfEducationIdentifier = 'Field_Of_Education_Identifier__c';
    public static final String ANZSCOIdentifier = 'ANZSCO_Identifier__c';
    public static final String VetNonVet = 'Vet_Non_Vet__c';
    public static final String SubjectFlag = 'C';
    public static final String SubjectIdentifier = 'Name';
    public static final String SubjectName = 'Unit_Name__c';
    public static final String SubjectFieldOfEducationIdentifier = 'Field_of_Education_Identifier__c';    
    public static final String VetFlag = 'Vet_Non_Vet__c';  
    public static final String NameForEncryption = 'Name_for_encryption_student__c';
    public static final String FullStudentName = 'Full_Student_Name__c';
    public static final String HighestSchoolLevelCompIdentifier = 'Highest_School_Level_Comp_Identifier__c';
    public static final String YearHighestEducationCompleted = 'Year_Highest_Education_Completed__c';
    public static final String Sex = 'Sex__c';
    public static final String PersonBirthDate = 'PersonBirthDate';
    public static final String PersonMailingPostalCode = 'PersonMailingPostalCode';
    public static final String IndigenousStatusIdentifier = 'Indigenous_Status_Identifier__c';
    public static final String MainLanguageSpokenatHomeIdentifier = 'Main_Language_Spoken_at_Home_Identifier__c';
    public static final String LabourForceStatusIdentifier = 'Labour_Force_Status_Identifier__c';
    public static final String CountryofBirthIdentifer = 'Country_of_Birth_Identifer__c';
    public static final String DisabilityFlag0 = 'Disability_Flag_0__c';
    public static final String PriorAchievementFlag = 'Prior_Achievement_Flag__c';
    public static final String AtSchoolFlag = 'At_School_Flag__c';
    public static final String ProficiencyinSpokenEnglishIdentifier = 'Proficiency_in_Spoken_English_Identifier__c';
    public static final String PersonMailingCity = 'PersonMailingCity';
    public static final String UniqueStudentIdentifier = 'Unique_Student_Identifier__c';
    public static final String Addressbuildingpropertyname = 'Address_building_property_name__c';
    public static final String Addressflatunitdetails = 'Address_flat_unit_details__c';
    public static final String Addressstreetnumber = 'Address_street_number__c';
    public static final String Addressstreetname = 'Address_street_name__c';
    public static final String ClientTitle = 'Salutation';
    public static final String ClientFirstGivenName = 'FirstName';
    public static final String ClientLastName = 'LastName';
    public static final String ReportingOtherStateIdentifier = 'Reporting_Other_State_Identifier__c';
    public static final String PersonHomePhone = 'PersonHomePhone';
    public static final String PersonWorkPhone = 'Work_Phone__c';
    public static final String PersonMobilePhone = 'PersonMobilePhone';
    public static final String PersonEmail = 'PersonEmail';
    public static final String DisabilityTypeIdentifier = 'Disability_Type__c';
    public static final String PriorEducationAchievementIdentifier = 'Prior_Achievement_Type_s__c';
    public static final String TrainingDeliveryLocationIdentifier = 'Delivery_Location_Identifier__c';
    public static final String EUSubjectIdentifier = 'Unit_of_Competency_Identifier__c';
    public static final String ActivityStartDate = 'Start_Date__c';
    public static final String ActivityEndDate = 'End_Date__c';
    public static final String DeliveryModeIdentifier = 'Delivery_Mode_Identifier__c';
    public static final String ScheduledHours = 'Scheduled_Hours__c';    
    public static final String CommencingProgramIdentifier = 'Commencing_Course_Identifier__c';
    public static final String ClientIdentifierApprenticeships = 'Client_Identifier_New_Apprenticeships__c';
    public static final String StudyReasonIdentifier = 'Study_Reason_Identifier__c';
    public static final String VETInSchoolsFlag = 'VET_in_Schools__c';
    public static final String ClientTuitionFee = 'Reportable_Tuition_Fee__c';
    public static final String FeeExemptionTypeIdentifier = 'Enrolment__r.Fee_Exemption__r.Code__c'; 
    public static final String PurchasingContractIdentifier = 'Purchasing_Contract_Identifier__c';
    public static final String PurchasingContractScheduleIdentifier = 'Purchasing_Contract_Schedule_Identifier__c';
    public static final String HoursAttended = 'Hours_Attended__c';
    public static final String ProgramIdentifier = 'Enrolment__r.Qualification__r.Parent_Qualification_Code__c';
    public static final String FundingSourceNational = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c';
    public static final String TrainingContractIdentifier = 'Enrolment__r.Training_Contract_Identifier__c';
    public static final String SpecificFundingIdentifier = 'Enrolment__r.Specific_Program_Identifier__c';
    //public static final String StateFundSourceIdentifier = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c';
    public static final String StateFundSourceIdentifier = 'BlankField';

    public static final String NominalHours = 'Line_Item_Qualification_Unit__r.Nominal_Hours__c';
    public static final String TrainingOrganisationIdentifier = 'Enrolment__r.Training_Organisation_Identifier__c';
    public static final String ParentQualificationCodeE = 'Enrolment__r.Parent_Qualification_Code__c';
    public static final String StudentIdentifierE = 'Enrolment__r.Student_Identifier__c';
    public static final String IssuedFlag = 'Qualification_Issued__c';
    public static final String TrainingOrgIdentifier = 'Account.Training_Org_Identifier__c';
    public static final String AVETMISSOrganisationName = 'Account.AVETMISS_Organisation_Name__c';
    public static final String TrainingOrgTypeIdentifier = 'Account.Training_Org_Type_Identifier__c';
    public static final String BillingStreet = 'Account.BillingStreet';
    public static final String BillingCity = 'Account.BillingCity';
    public static final String BillingPostalCode = 'Account.BillingPostalCode';
    public static final String StateIdentifierA = 'Account.State_Identifier__c';
    public static final String FirstName = 'FirstName';
    public static final String LastName = 'LastName';
    public static final String Phone = 'Phone';
    public static final String Fax = 'Fax';
    public static final String Email = 'Email';
    public static final String FirstLast = 'FirstLast';
    
   @isTest static void testNAT00010() {
        
        Map<Integer, NATDataElement > nat10 = NATDataDefinitionNSWAPL.nat00010();
                
        system.assertEquals(String.valueOf(nat10.get(1) ), String.valueOf(new NATDataElement(TrainingOrgIdentifier, 10, true, 'string') ) ) ;  
        system.assertEquals(String.valueOf(nat10.get(2) ), String.valueOf(new NATDataElement(AVETMISSOrganisationName, 100, true, 'string' ) ) ) ;  
        system.assertEquals(String.valueOf(nat10.get(3) ), String.valueOf(new NATDataElement(TrainingOrgTypeIdentifier, 2, true, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat10.get(4) ), String.valueOf(new NATDataElement(BillingStreet, 50, true, 'string' ) ) );   
        system.assertEquals(String.valueOf(nat10.get(5) ), String.valueOf(new NATDataElement(BlankField, 50, true, 'string' ) ) );
        system.assertEquals(String.valueOf(nat10.get(6) ), String.valueOf(new NATDataElement(BillingCity, 50, true, 'string' ) ) );   
        system.assertEquals(String.valueOf(nat10.get(7) ), String.valueOf(new NATDataElement(BillingPostalCode, 4, true, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat10.get(8) ), String.valueOf(new NATDataElement(StateIdentifierA, 2, true, 'string' ) ) );
        system.assertEquals(String.valueOf(nat10.get(9) ), String.valueOf(new NATDataElement(FirstLast, 60, true, 'string') ) );
        system.assertEquals(String.valueOf(nat10.get(10) ), String.valueOf(new NATDataElement(Phone, 20, false, 'string') ) );
        system.assertEquals(String.valueOf(nat10.get(11) ), String.valueOf(new NATDataElement(Fax, 20, false, 'string') ) );
        system.assertEquals(String.valueOf(nat10.get(12) ), String.valueOf(new NATDataElement(Email, 80, false, 'string') ) );
        
    }
    
    @isTest static void testNAT00020() {
        
        Map<Integer, NATDataElement > nat20 = NATDataDefinitionNSWAPL.nat00020();
            
        system.assertEquals(String.valueOf(nat20.get(1) ), String.valueOf(new NATDataElement(TrainingOrganisationTrainingOrgIdentifier, 10, true, 'string') ) );
        system.assertEquals(String.valueOf(nat20.get(2) ), String.valueOf(new NATDataElement(DeliveryLocationIdentifier, 10, FALSE, 'string' ) ) );   
        system.assertEquals(String.valueOf(nat20.get(3) ), String.valueOf(new NATDataElement(LocName, 100, FALSE, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat20.get(4) ), String.valueOf(new NATDataElement(ChildPostcode, 4, FALSE, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat20.get(5) ), String.valueOf(new NATDataElement(LocStateIdentifier, 2, FALSE, 'string' ) ) ) ;   
        system.assertEquals(String.valueOf(nat20.get(6) ), String.valueOf(new NATDataElement(ChildSuburb, 50, FALSE, 'string' ) ) ) ; 
        system.assertEquals(String.valueOf(nat20.get(7) ), String.valueOf(new NATDataElement(CountryIdentifier, 4, FALSE, 'string' ) ) );  
        //system.assertEquals(String.valueOf(nat20.get(8) ), String.valueOf(new NATDataElement(BlankField, 1, true, 'string') ) );
     
    }
    
	@isTest static void testNAT00030() {        

        Map<Integer, NATDataElement> nat30 = NATDataDefinitionNSWAPL.nat00030();      
        
        system.assertEquals(String.valueOf(nat30.get(1) ), String.valueOf(new NATDataElement(ParentQualificationCode, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat30.get(2) ), String.valueOf(new NATDataElement(ParentQualificationName, 100, false, 'string') ) );
        system.assertEquals(String.valueOf(nat30.get(3) ), String.valueOf(new NATDataElement(ReportableQualificationHours, 4, true, 'number') ) );
        system.assertEquals(String.valueOf(nat30.get(4) ), String.valueOf(new NATDataElement(RecognitionStatusIdentifier, 2, false, 'string') ) );
        system.assertEquals(String.valueOf(nat30.get(5) ), String.valueOf(new NATDataElement(QualificationCategoryIdentifier, 3, false, 'number') ) );
        system.assertEquals(String.valueOf(nat30.get(6) ), String.valueOf(new NATDataElement(FieldOfEducationIdentifier, 4, false, 'number') ) );
        system.assertEquals(String.valueOf(nat30.get(7) ), String.valueOf(new NATDataElement(ANZSCOIdentifier, 6, false, 'string') ) );
        system.assertEquals(String.valueOf(nat30.get(8) ), String.valueOf(new NATDataElement(VetNonVet, 1, false, 'boolean' ) ) ); 
                 
    }
    
    @isTest static void testNAT00060() {  
        
        Map<Integer, NATDataElement> nat60 = NATDataDefinitionNSWAPL.nat00060();      
        
        system.assertEquals(String.valueOf(nat60.get(1) ),String.valueOf(new NATDataElement(SubjectFlag, 1, true, 'string') ) );
        system.assertEquals(String.valueOf(nat60.get(2) ),String.valueOf(new NATDataElement(SubjectIdentifier, 12, false, 'string') ) );
        system.assertEquals(String.valueOf(nat60.get(3) ),String.valueOf(new NATDataElement(SubjectName, 100, false, 'string') ) ) ;
        system.assertEquals(String.valueOf(nat60.get(4) ),String.valueOf(new NATDataElement(SubjectFieldOfEducationIdentifier, 6, false, 'string') ) );
        system.assertEquals(String.valueOf(nat60.get(5) ),String.valueOf(new NATDataElement(VetFlag, 3, false, 'boolean') ) ); 
        
    }
    
    @isTest static void testNAT00080() {  
        
        Map<Integer, NATDataElement> nat80 = NATDataDefinitionNSWAPL.nat00080();       
        
        system.assertEquals(String.valueOf(nat80.get(1) ), String.valueOf(new NATDataElement(StudentIdentifier2, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(2) ), String.valueOf(new NATDataElement(NameForEncryption, 60, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(3) ), String.valueOf(new NATDataElement(HighestSchoolLevelCompIdentifier, 2, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(4) ), String.valueOf(new NATDataElement(YearHighestEducationCompleted, 4, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(5) ), String.valueOf(new NATDataElement(Sex, 1, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(6) ), String.valueOf(new NATDataElement(PersonBirthDate, 8, false, 'date') ) );
        system.assertEquals(String.valueOf(nat80.get(7) ), String.valueOf(new NATDataElement(PersonMailingPostalCode, 4, false, 'string') ) );
        system.assertEquals(String.valueOf(nat80.get(8) ), String.valueOf(new NATDataElement(IndigenousStatusIdentifier, 1, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(9) ), String.valueOf(new NATDataElement(MainLanguageSpokenatHomeIdentifier, 4, false, 'string') ) ); 
        system.assertEquals(String.valueOf(nat80.get(10) ), String.valueOf(new NATDataElement(LabourForceStatusIdentifier, 2, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(11) ), String.valueOf(new NATDataElement(CountryofBirthIdentifer, 4, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(12) ), String.valueOf(new NATDataElement(DisabilityFlag0, 1, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(13) ), String.valueOf(new NATDataElement(PriorAchievementFlag, 1, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(14) ), String.valueOf(new NATDataElement(AtSchoolFlag, 1, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(15) ), String.valueOf(new NATDataElement(ProficiencyinSpokenEnglishIdentifier, 1, true, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat80.get(16) ), String.valueOf(new NATDataElement(PersonMailingCity, 50, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(17) ), String.valueOf(new NATDataElement(UniqueStudentIdentifier, 10, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(18) ), String.valueOf(new NATDataElement(StateIdentifier, 2, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(19) ), String.valueOf(new NATDataElement(Addressbuildingpropertyname, 50, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(20) ), String.valueOf(new NATDataElement(Addressflatunitdetails, 30, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(21) ), String.valueOf(new NATDataElement(Addressstreetnumber, 15, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat80.get(22) ), String.valueOf(new NATDataElement(Addressstreetname, 70, false, 'string' ) ) );
                  
    }
    
    
	@isTest static void testNAT00085() {  
        
        Map<Integer, NATDataElement> nat85 = NATDataDefinitionNSWAPL.nat00085();       
        
        system.assertEquals(String.valueOf(nat85.get(1) ),  String.valueOf(new NATDataElement(StudentIdentifier2, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(2) ),  String.valueOf(new NATDataElement(ClientTitle, 4, false, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(3) ),  String.valueOf(new NATDataElement(ClientFirstGivenName, 40, false, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(4) ),  String.valueOf(new NATDataElement(ClientLastName, 40, false, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(5) ),  String.valueOf(new NATDataElement(PostalBuildingPropertyName, 50, true, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(6) ),  String.valueOf(new NATDataElement(PostalFlatUnitDetails, 30, true, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(7) ),  String.valueOf(new NATDataElement(PostalStreetNumber, 15, true, 'string') ) );
        system.assertEquals(String.valueOf(nat85.get(8) ),  String.valueOf(new NATDataElement(PostalStreetName, 70, true, 'string' ) ) );  
        system.assertEquals(String.valueOf(nat85.get(9) ),  String.valueOf(new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat85.get(10) ),  String.valueOf(new NATDataElement(PostalSuburbLocalityTown, 50, true, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat85.get(11) ),  String.valueOf(new NATDataElement(PostalPostCode, 4, true, 'string' ) ) );
        system.assertEquals(String.valueOf(nat85.get(12) ),  String.valueOf(new NATDataElement(StateIdentifier, 2, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat85.get(13) ), String.valueOf(new NATDataElement(PersonHomePhone, 20, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat85.get(14) ), String.valueOf(new NATDataElement(PersonWorkPhone, 20, false, 'string' ) ) ); 
        system.assertEquals(String.valueOf(nat85.get(15) ), String.valueOf(new NATDataElement(PersonMobilePhone, 20, false, 'string' ) ) );
        system.assertEquals(String.valueOf(nat85.get(16) ), String.valueOf(new NATDataElement(PersonEmail, 80, false, 'string' ) ) );
              
    }
    
    @isTest static void testNAT00090() {  
        
        Map<Integer, NATDataElement> nat90 = NATDataDefinitionNSWAPL.nat00090();        
        
        system.assertEquals(String.valueOf(nat90.get(1) ), String.valueOf(new NATDataElement(StudentIdentifier2, 1, true, 'string') ) );
        system.assertEquals(String.valueOf(nat90.get(2) ), String.valueOf(new NATDataElement(DisabilityTypeIdentifier, 2, true, 'string') ) );
            
    }
    
    @isTest static void testNAT00100() {  
        
        Map<Integer, NATDataElement> nat100 = NATDataDefinitionNSWAPL.nat00100();      
        
        system.assertEquals(String.valueOf(nat100.get(1) ), String.valueOf(new NATDataElement(StudentIdentifier2, 1, true, 'string') ) );
        system.assertEquals(String.valueOf(nat100.get(2) ), String.valueOf(new NATDataElement(PriorEducationAchievementIdentifier, 3, true, 'string') ) );
    
    }
    
    @isTest static void testNAT00120() {  
        
        Map<Integer, NATDataElement> nat120 = NATDataDefinitionNSWAPL.nat00120();       
        
        system.assertEquals(String.valueOf(nat120.get(1 ) ), String.valueOf(new NATDataElement(TrainingDeliveryLocationIdentifier, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(2 ) ), String.valueOf(new NATDataElement(StudentIdentifier, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(3 ) ), String.valueOf(new NATDataElement(EUSubjectIdentifier, 12, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(4 ) ), String.valueOf(new NATDataElement(ProgramIdentifier, 10, true, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(5 ) ), String.valueOf(new NATDataElement(ActivityStartDate, 8, false, 'date') ) );
        system.assertEquals(String.valueOf(nat120.get(6 ) ), String.valueOf(new NATDataElement(ActivityEndDate, 8, false, 'date') ) );
        system.assertEquals(String.valueOf(nat120.get(7 ) ), String.valueOf(new NATDataElement(DeliveryModeIdentifier, 2, false, 'number') ) );

        system.assertEquals(String.valueOf(nat120.get(8 ) ), String.valueOf(new NATDataElement(OutcomeIdentifierNational , 2, true, 'string') ) ); 

        system.assertEquals(String.valueOf(nat120.get(9 ) ), String.valueOf(new NATDataElement(ScheduledHours, 4, true, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(10) ), String.valueOf(new NATDataElement(FundingSourceNational, 2, true, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(11) ), String.valueOf(new NATDataElement(CommencingProgramIdentifier, 1, false, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(12) ), String.valueOf(new NATDataElement(TrainingContractIdentifier, 10, true, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(13) ), String.valueOf(new NATDataElement(ClientIdentifierApprenticeships, 10, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(14) ), String.valueOf(new NATDataElement(StudyReasonIdentifier, 2, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(15) ), String.valueOf(new NATDataElement(VETInSchoolsFlag, 1, false, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(16) ), String.valueOf(new NATDataElement(SpecificFundingIdentifier, 10, true, 'string') ) );

        system.assertEquals(String.valueOf(nat120.get(17) ), String.valueOf(new NATDataElement(OutcomeIdentifierTrainingOrg, 3, true, 'string') ) ); // set to 'TNC' for Cancelled, Withdrawn and Expired, 'D' for Deferred
        system.assertEquals(String.valueOf(nat120.get(18) ), String.valueOf(new NATDataElement(BlankField, 3, true, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(19) ), String.valueOf(new NATDataElement(FeeAmount, 4, true, 'number') ) );
        
        
        //system.assertEquals(String.valueOf(nat120.get(18) ), String.valueOf(new NATDataElement(StateFundSourceIdentifier, 3, true, 'string') ) );
		//system.assertEquals(String.valueOf(nat120.get(19) ), String.valueOf(new NATDataElement(ClientTuitionFee, 4, false, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(20) ), String.valueOf(new NATDataElement(EquityFlag, 1, true, 'string') ) );
 
        system.assertEquals(String.valueOf(nat120.get(21) ), String.valueOf(new NATDataElement(BlankField, 12, true, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(22) ), String.valueOf(new NATDataElement(BlankField, 3, true, 'string') ) );
        system.assertEquals(String.valueOf(nat120.get(23) ), String.valueOf(new NATDataElement(HoursAttended, 4, false, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(24) ), String.valueOf(new NATDataElement(AssociatedCourseIdentifier, 10, true, 'string') ) ); 
        
        //system.assertEquals(String.valueOf(nat120.get(22) ), String.valueOf(new NATDataElement(PurchasingContractScheduleIdentifier, 3, false, 'string') ) ); 
        //system.assertEquals(String.valueOf(nat120.get(23) ), String.valueOf(new NATDataElement(HoursAttended, 4, false, 'number') ) );
        //system.assertEquals(String.valueOf(nat120.get(24) ), String.valueOf(new NATDataElement(AssociatedCourseIdentifier, 10, true, 'string') ) ); 
        
        system.assertEquals(String.valueOf(nat120.get(25) ), String.valueOf(new NATDataElement(BookingId, 10, true, 'number') ) );
        system.assertEquals(String.valueOf(nat120.get(26) ), String.valueOf(new NATDataElement(CourseSiteId, 10, false, 'string') ) );	
        system.assertEquals(String.valueOf(nat120.get(27) ), String.valueOf(new NATDataElement(TrainingPlanDeveloped, 1, true, 'string') ) );
           
    }
    
    @isTest static void testNAT00130() {  
        
        Map<Integer, NATDataElement> nat130 = NATDataDefinitionNSWAPL.nat00130();      
        
        system.assertEquals(String.valueOf(nat130.get(1) ), String.valueOf(new NATDataElement(TrainingOrganisationIdentifier, 10, true, 'string') ) );
        system.assertEquals(String.valueOf(nat130.get(2) ), String.valueOf(new NATDataElement(ParentQualificationCodeE, 10, true, 'string') ) );
        system.assertEquals(String.valueOf(nat130.get(3) ), String.valueOf(new NATDataElement(StudentIdentifierE, 10, true, 'string') ) );

        system.assertEquals(String.valueOf(nat130.get(4) ), String.valueOf(new NATDataElement(YearProgramCompleted, 4, false, 'number') ) );

        system.assertEquals(String.valueOf(nat130.get(5) ), String.valueOf(new NATDataElement(IssuedFlag, 1, false, 'string') ) );
        
    }
}