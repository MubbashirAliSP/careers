/**
 * @description Trigger definition for Enrolment Letters(Correspondence) Object
 * @author Nick Guia
 * @history
 *		Nick Guia	04.AUG.2015		Created
 */
public class EnrolmentLettersTriggerHandler {

	private static final String DEFERRED_NOT_STATE_SUBSIDISED_CODE = '401';
	private static final String DEFERRED_RESTRICTED_ACCESS_ARRANGEMENT = '402';
	private static final String DEFERRED_ACT_GOVERNMENT = '410';
	private static final String DEFERRED_NSW_GOVERNMENT = '404';
	private static final String DEFERRED_NT_GOVERNMENT = '409';
	private static final String DEFERRED_QLD_GOVERNMENT = '405';
	private static final String DEFERRED_TAS_GOVERNMENT = '408';
	private static final String DEFERRED_VICTORIAN_GOVERNMENT = '403';
	private static final String DEFERRED_WA_GOVERNMENT = '407';
	private static final String DEFERRED_SOUTH_ASIAN_GOVERNMENT = '406';
	private static final String PAID_FULL_NON_STATE = '501';
	private static final String PAID_FULL_RESTRICTED_ACCESS = '502';
	private static final String PAID_FULL_ACT_GOVERNMENT = '510';
	private static final String PAID_FULL_NSW_GOVERNMENT = '504';
	private static final String PAID_FULL_NT_GOVERNMENT = '509';
	private static final String PAID_FULL_QLD_GOVERNMENT = '505';
	private static final String PAID_FULL_TAS_GOVERNMENT = '508';
	private static final String PAID_FULL_VICTORIAN_GOVERNMENT = '503';
	private static final String PAID_FULL_WA_GOVERNMENT = '507';
	private static final String PAID_FULL_SOUTH_AUSTRALIAN_GOVERNMENT = '506';
	private static final String ACT_CODE = 'ACT';
	private static final String NSW_CODE = 'NSW';
	private static final String NT_CODE = 'NT';
	private static final String QLD_CODE = 'QLD';
	private static final String TAS_CODE = 'TAS';
	private static final String VIC_CODE = 'VIC';
	private static final String WA_CODE = 'WA';
	private static final String SA_CODE = 'SA';

	public static void onBeforeInsert(List<Enrolment_Letters__c> records) {
		List<Enrolment_Letters__c>  enrolmentLettersList = new List<Enrolment_Letters__c> ();
		enrolmentLettersList = populateEnrolmentLetterAmount(records);
    }

    /**
 	* @description - Method to populate Course Amount/Loan Amount based in certain conditions
 	*			   - This is also used in EnrolmentTriggerHandler (updateEnrolmentLetters)
 	*			   - Added return value so that code method is reusable
 	*/
    public static List<Enrolment_Letters__c> populateEnrolmentLetterAmount(List<Enrolment_Letters__c> elRecordsList) {
    	List<Enrolment_Letters__c> elVFHRecordsList = new List<Enrolment_Letters__c>();
    	Map<Id, Qualification__c> qualificationUpdateMap = new Map<Id, Qualification__c>();
    	Map<Id, Id> enrolmentLetterIdToEnrolmentIdMap = new Map<Id, Id>();
    	Map<Id, Id> enrolmentToQualificationMap = new Map<Id, Id>();
    	Map<Id, String> enrolmentToStatusCodeMap = new Map<Id, String>();
    	Map<Id, String> enrolmentToStateMap = new Map<Id, String>();
		Id vfhRecTypeId = Schema.SObjectType.Enrolment_Letters__c.getRecordTypeInfosByName().get('VFH Welcome Letter').getRecordTypeId();

		//filter VFH Welcome Letter rec type
		for(Enrolment_Letters__c el : elRecordsList) {
			if(el.RecordTypeId == vfhRecTypeId) {
				elVFHRecordsList.add(el);
				enrolmentLetterIdToEnrolmentIdMap.put(el.Id, el.Enrolment__c);
			}
		}

		//build map of enrolment and qualification
		//build map of enrolment and student code
		for(Enrolment__c e : [SELECT Id, Qualification__c, Student_Status_Code__r.Code__c, Delivery_Location__r.State_Short_Name__c
							FROM Enrolment__c
							WHERE Id in :enrolmentLetterIdToEnrolmentIdMap.values()])
		{
			enrolmentToQualificationMap.put(e.Id, e.Qualification__c);
			enrolmentToStatusCodeMap.put(e.Id, e.Student_Status_Code__r.Code__c);
			enrolmentToStateMap.put(e.Id, e.Delivery_Location__r.State_Short_Name__c);
		}

		//build map of qualification ID and qualification object
		Map<Id, Qualification__c> qualificationMap = new Map<Id, Qualification__c>([SELECT Id, 
			Course_Amount_ACT__c, Course_Amount_NSW__c, Course_Amount_NT__c, Course_Amount_QLD__c,
			Course_Amount_SA__c, Course_Amount_TAS__c, Course_Amount_VIC__c, Course_Amount_WA__c,
			S_Course_Amount_ACT__c, S_Course_Amount_NSW__c, S_Course_Amount_NT__c, S_Course_Amount_QLD__c,
			S_Course_Amount_SA__c, S_Course_Amount_TAS__c, S_Course_Amount_VIC__c, S_Course_Amount_WA__c,
			Government_Loan_Fees_ACT__c, Government_Loan_Fees_NSW__c, Government_Loan_Fees_NT__c, 
			Government_Loan_Fees_QLD__c, Government_Loan_Fees_SA__c, Government_Loan_Fees_TAS__c,
			Government_Loan_Fees_VIC__c, Government_Loan_Fees_WA__c FROM Qualification__c
			WHERE Id in :enrolmentToQualificationMap.values()]);

		for(Enrolment_Letters__c el : elVFHRecordsList) {
			//check status code
			String statusCode = enrolmentToStatusCodeMap.get(enrolmentLetterIdToEnrolmentIdMap.get(el.Id));
			String state = enrolmentToStateMap.get(enrolmentLetterIdToEnrolmentIdMap.get(el.Id));
			Qualification__c q = qualificationMap.get(enrolmentToQualificationMap.get(enrolmentLetterIdToEnrolmentIdMap.get(el.Id)));
			System.debug('*** DEBUG - QUALIFICATION ID : ' + q.Id);
			System.debug('*** DEBUG - statusCode : ' + statusCode);
			System.debug('*** DEBUG - state : ' + state);
			if(statusCode != null && state != null && q != null) {
				if(statusCode == DEFERRED_NOT_STATE_SUBSIDISED_CODE ||
					statusCode == DEFERRED_RESTRICTED_ACCESS_ARRANGEMENT)
				{
					System.debug('*** CASE 1');
					//pull course fees from the Full Course amount
					//and the loan fee from the Government Loan Fee
					if(state == ACT_CODE) {
						el.Course_Amount__c = q.Course_Amount_ACT__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_ACT__c;
					} else if(state == NSW_CODE) {
						el.Course_Amount__c = q.Course_Amount_NSW__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_NSW__c;
					} else if(state == NT_CODE) {
						el.Course_Amount__c = q.Course_Amount_NT__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_NT__c;
					} else if(state == QLD_CODE) {
						el.Course_Amount__c = q.Course_Amount_QLD__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_QLD__c;
					} else if(state == SA_CODE) {
						el.Course_Amount__c = q.Course_Amount_SA__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_SA__c;
					} else if(state == TAS_CODE) {
						el.Course_Amount__c = q.Course_Amount_TAS__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_TAS__c;
					} else if(state == VIC_CODE) {
						el.Course_Amount__c = q.Course_Amount_VIC__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_VIC__c;
					} else if(state == WA_CODE) {
						el.Course_Amount__c = q.Course_Amount_WA__c;
						el.Loan_Amount__c = q.Government_Loan_Fees_WA__c;
					} else {
						System.debug('ERROR! Invalid State!');
					}
					if(!qualificationUpdateMap.containsKey(q.Id)) {
						qualificationUpdateMap.put(q.Id, q);
					}
				} else if(statusCode == DEFERRED_ACT_GOVERNMENT || statusCode == DEFERRED_NSW_GOVERNMENT ||
					statusCode == DEFERRED_NT_GOVERNMENT || statusCode == DEFERRED_QLD_GOVERNMENT ||
					statusCode == DEFERRED_TAS_GOVERNMENT || statusCode == DEFERRED_VICTORIAN_GOVERNMENT ||
					statusCode == DEFERRED_WA_GOVERNMENT || statusCode == DEFERRED_SOUTH_ASIAN_GOVERNMENT) 
				{
					System.debug('*** CASE 2');
					//pull course fees from the Subsidised Course amount and 
					//the Loan fee and the Government loan fee populates as $0.00
					if(state == ACT_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_ACT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_ACT__c = 0;
					} else if(state == NSW_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_NSW__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NSW__c = 0;
					} else if(state == NT_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_NT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NT__c = 0;
					} else if(state == QLD_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_QLD__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_QLD__c = 0;
					} else if(state == SA_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_SA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_SA__c = 0;
					} else if(state == TAS_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_TAS__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_TAS__c = 0;
					} else if(state == VIC_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_VIC__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_VIC__c = 0;
					} else if(state == WA_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_WA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_WA__c = 0;
					} else {
						System.debug('ERROR! Invalid State!');
					}
					if(!qualificationUpdateMap.containsKey(q.Id)) {
						qualificationUpdateMap.put(q.Id, q);
					}
				} else if(statusCode == PAID_FULL_NON_STATE || 
					statusCode == PAID_FULL_RESTRICTED_ACCESS) 
				{
					System.debug('*** CASE 3');
					//pull course fees from the Full Course amount  
					//and the loan fee and the Government loan fee populates as $0.00
					if(state == ACT_CODE) {
						el.Course_Amount__c = q.Course_Amount_ACT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_ACT__c = 0;
					} else if(state == NSW_CODE) {
						el.Course_Amount__c = q.Course_Amount_NSW__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NSW__c = 0;
					} else if(state == NT_CODE) {
						el.Course_Amount__c = q.Course_Amount_NT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NT__c = 0;
					} else if(state == QLD_CODE) {
						el.Course_Amount__c = q.Course_Amount_QLD__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_QLD__c = 0;
					} else if(state == SA_CODE) {
						el.Course_Amount__c = q.Course_Amount_SA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_SA__c = 0;
					} else if(state == TAS_CODE) {
						el.Course_Amount__c = q.Course_Amount_TAS__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_TAS__c = 0;
					} else if(state == VIC_CODE) {
						el.Course_Amount__c = q.Course_Amount_VIC__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_VIC__c = 0;
					} else if(state == WA_CODE) {
						el.Course_Amount__c = q.Course_Amount_WA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_WA__c = 0;
					} else {
						System.debug('ERROR! Invalid State!');
					}
					if(!qualificationUpdateMap.containsKey(q.Id)) {
						qualificationUpdateMap.put(q.Id, q);
					}
				} else if(statusCode == PAID_FULL_ACT_GOVERNMENT ||
					statusCode == PAID_FULL_NSW_GOVERNMENT ||
					statusCode == PAID_FULL_NT_GOVERNMENT ||
					statusCode == PAID_FULL_QLD_GOVERNMENT ||
					statusCode == PAID_FULL_TAS_GOVERNMENT ||
					statusCode == PAID_FULL_VICTORIAN_GOVERNMENT ||
					statusCode == PAID_FULL_WA_GOVERNMENT ||
					statusCode == PAID_FULL_SOUTH_AUSTRALIAN_GOVERNMENT) 
				{
					System.debug('*** CASE 4');
					//pull course fees pull from the Subsidised Course amount  
					//and the loan fee and the Government loan fee populates as $0.00
					if(state == ACT_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_ACT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_ACT__c = 0;
					} else if(state == NSW_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_NSW__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NSW__c = 0;
					} else if(state == NT_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_NT__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_NT__c = 0;
					} else if(state == QLD_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_QLD__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_QLD__c = 0;
					} else if(state == SA_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_SA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_SA__c = 0;
					} else if(state == TAS_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_TAS__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_TAS__c = 0;
					} else if(state == VIC_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_VIC__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_VIC__c = 0;
					} else if(state == WA_CODE) {
						el.Course_Amount__c = q.S_Course_Amount_WA__c;
						el.Loan_Amount__c = 0;
						q.Government_Loan_Fees_WA__c = 0;
					} else {
						System.debug('ERROR! Invalid State!');
					}
					if(!qualificationUpdateMap.containsKey(q.Id)) {
						qualificationUpdateMap.put(q.Id, q);
					}
				} else {
					System.debug('ERROR! Invalid Status Code!!');
				}
			} else {
				System.debug('ERROR! Please check if Enrolment has a state, student status code, or qualification.');
			}
			
		}
		update qualificationUpdateMap.values();
		return elVFHRecordsList;
    }
}