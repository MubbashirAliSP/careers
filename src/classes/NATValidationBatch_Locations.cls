global without sharing class NATValidationBatch_Locations implements Database.Batchable<sObject>,Database.Stateful {
    
    global NAT_Validation_Log__c log;
    global String queryStringMain;
    global String endQuery;
    global Integer locationRecordCount = 0;
    global Integer locationErrorCount = 0;
    global String tOrgId;
    global String validationState;
    global DateTime currentDT;
    
    global NATValidationBatch_Locations(Id logId) {
                          
        log = [ SELECT ID, Validation_State__c, Training_Organisation_Id__c, end_query__c, log_Run_at__c, query_main__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
        
        tOrgId = log.Training_Organisation_Id__c;
        validationState = log.Validation_State__c;
        currentDT = log.Log_Run_At__c;

        queryStringMain = 'Select Id,'+
            'LastModifiedDate,'+
            'Delivery_Location_Identifier__c,'+
            'Training_Organisation__r.Training_Org_Identifier__c,'+
            'Training_Organisation__r.Name,'+
            'Name,'+
            'Postcode__c,'+
            'Address_Location__c,'+
            'Child_Postcode__c,'+
            'Child_Suburb__c,'+
            'Country_Identifier__c,'+
            'State_Identifier__c,'+
            'Child_Country__c,'+
            'Child_Address_building_property_name__c,'+
            'Child_Address_flat_unit_details__c,'+
            'Child_Address_street_number__c,'+
            'Child_Address_street_name__c,'+
            'Country__r.Country_Code__c '+
            'FROM Locations__c ' +
            'WHERE Training_Organisation__c = :tOrgId ';
        
            if(validationState == 'National') {
                queryStringMain += 'AND RecordType.Name = \'Training Delivery Location\' ' +
                                   'AND Id in (Select Training_Delivery_Location__c ' + log.End_Query__c + ')';
            }
            else {
                queryStringMain += 'AND Child_State__c = :validationState ' +
                                   'AND RecordType.Name = \'Training Delivery Location\' ' +
                                   'AND Id in (Select Training_Delivery_Location__c ' + log.End_Query__c + ')';
            }
        }
                                    
    
    global database.querylocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(queryStringMain);
            
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
        Set<String> cnIdentifiers = new Set<String>{'1100','1101','1102','1199'};
        Set<String> stIdentifiers = new Set<String>{'09','99'};
            
            for(SObject so : scope) {
                
            Locations__c loc = (Locations__c) so;
            locationRecordCount++;
            
           NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = loc.Id, Nat_File_Name__c = 'NAT00020',Parent_Record_Last_Modified_Date__c = loc.LastModifiedDate);
        
           if(loc.Delivery_Location_Identifier__c == null) 
                event.Error_Message__c = 'Delivery Location Identifier Cannot Be Blank';
                
           else if(loc.Child_Suburb__c == null) 
                event.Error_Message__c = 'Delivery Location Suburb Cannot Be Blank';

           else if((loc.Child_Address_street_name__c == null || loc.Child_Address_street_number__c == null) && log.Validation_State__c == 'Victoria')
                event.Error_Message__c = 'Street Number Or Street Name cannot be blank';
                
           else if(loc.Child_Postcode__c == null || loc.Country_Identifier__c == null || loc.State_Identifier__c == null)
                event.Error_Message__c = 'Postcode, Country Identifier or State Identifier Cannot Be Blank';
                
           else if(loc.Child_Postcode__c == 'OSPC') {
           
                if(!(cnIdentifiers.contains(loc.Country_Identifier__c)) || !(stIdentifiers.contains(loc.State_Identifier__c))) 
                        event.Error_Message__c = 'Country Identifier cannot be 1100, 1101, 1102, 1199 or State Identifier cannot be 09 or 99 when postcode = OSPC';
               
           }
           
           if(event.Error_Message__c != null)
            errorEventList.add(event);
        }
        
        insert errorEventList; 
                
    }
    
    global void finish(Database.BatchableContext BC) {
        
        log.NAT00020_Record_Count__c = locationRecordCount;
         
        update log;
        
        Database.executeBatch(new NATValidationBatch_Qualifications( log.id ) );

    }
}