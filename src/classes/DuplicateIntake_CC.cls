/**
 * @description Controller for DuplicateIntake page
 * @author Janella Lauren Canlas
 * @date 08.NOV.2012
 * @history
 *       08.NOV.2012    Janella Canlas          Created.
 *       20.AUG.2013    Michelle Magsarili      Updated saveIntake method to store cloned Class Configuration
 *       21.AUG.2013    Michelle Magsarili      Updated saveIntake method to set the Intake start and end dates to the dates entered in the VF page
 *       26.SEP.2013    Warjie Malibago         Updated saveIntake method to adjust the class dates based on the cloned intake records
 *       30.SEP.2013    Warjie Malibago         Updated saveIntake method to carry over new Intake Ids to new Classes
 *       02.OCT.2013    Warjie Malibago         Updated saveIntake method for duplication; see else condition
 *       09.DEC.2013    Warjie Malibago         Updated saveIntake method to also duplicate IUOS records
 *       06.MAR.2014    Michelle Magsarili      Updated Class from with sharing to without sharing to eliminate SC-16062
 *       15.SEP.2014    Warjie Malibago         Update saveIntake() to not include one-off class in duplication
 *       23.SEP.2014    Warjie Malibago         Update saveIntake() to exclude any class linked to a one-off upond duplication
 *       22.JAN.2014    Nick Guia               Modified saveIntake to correct the class configuration assignment to duplicate classes
 *       22.APR.2014    Arnie Ug                Modified SOQL ORDER BY of Class Object to clone by date
 *       22.JAN.2016    Ranyel Maliwanag        Blanked out the Adjusted Census Date when cloning an intake
*/
public without sharing class DuplicateIntake_CC {
    public Id intakeId;
    public Intake__c intakeRecord {get;set;}
    public String soql = '';
    
    public DuplicateIntake_CC(){
        //retrieve current intake Id
        intakeId = ApexPages.currentPage().getParameters().get('intakeId');
        //retrieve current intake record
        intakeRecord = getIntakeRecord();
    }
    
    //retrieve Intake record
    public Intake__c getIntakeRecord(){
        Intake__c intake = [SELECT OwnerId, Owner.Name, Start_Date__c, End_Date__c, Qualification__c, Program__c, 
                            Delivery_Type_del__c, Training_Delivery_Location__c, Min_clients__c, Max_clients__c, Name, Funding_Stream__c, Stage__c
                            FROM Intake__c
                            WHERE id =: intakeId];
        return intake;
    }  
    
    // Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
    public static string getCreatableFieldsSOQL(String objectName, String whereClause){
        
        String selects = '';
        
        if (whereClause == null || whereClause == ''){ return null; }
        
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        
        if (fMap != null){
            system.debug('@@fMap: '+fMap);
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                system.debug('@@fd: '+fd);
                if (fd.isCreateable()){ // field is creatable
                    system.debug('@@fd: '+fd.isCreateable());
                    selectFields.add(fd.getName());
                }
            }
        }
        
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                if(s!='OwnerId')
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
            
        }
        
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
        
    }
        
    public PageReference saveIntake(){
        
        //CLONE INTAKE START

        //NG: Added limit for the where clause
        soql = getCreatableFieldsSOQL('Intake__c','id = \''+intakeId+'\' LIMIT 1'); 
        //query current intake with creatable fields
        Intake__c intakeOrig = Database.query(soql);
        //container of cloned intake - deep clone
        Intake__c intakeClone = intakeOrig.clone(false, true, false, false);

        //update fields
        if(intakeClone != null){
            //MAM 08/21/2013 Update Start and End Dates start
            intakeClone.Start_Date__c = IntakeRecord.Start_Date__c;
            intakeClone.End_Date__c = IntakeRecord.End_Date__c;
            intakeClone.Adjusted_Census_Date__c = null;
            //WBM 09/26/2013 start
            intakeClone.Training_Delivery_Location__c = IntakeRecord.Training_Delivery_Location__c;
            intakeClone.Min_clients__c = IntakeRecord.Min_clients__c;
            intakeClone.Max_clients__c = IntakeRecord.Max_clients__c;
            //WBM end
            //MAM 08/21/2013 end
            insert intakeClone;
        }

        //CLONE INTAKE END

        //WBM Start 09262013
        Date origIntakeStart, cloneIntakeStart, origIntakeEnd, cloneIntakeEnd;
        ID intakeCloneId;

        origIntakeStart = Date.valueOf(intakeOrig.Start_Date__c);  
        origIntakeEnd = Date.valueOf(intakeOrig.End_Date__c);              
        
        cloneIntakeStart = Date.valueOf(intakeClone.Start_Date__c);
        cloneIntakeEnd = Date.valueOf(intakeClone.End_Date__c);
        intakeCloneId = intakeClone.Id;
        //WBM End
              

        //CLONE INTAKE UNIT START
        List<Intake_Unit__c> intakeUnitOrig = new List<Intake_Unit__c>();
        List<Id> iuOrigIds = new List<Id>();
        List<Id> iuCloneIds = new List<Id>();
        Map<Id, Id> intakeIdMap = new Map<Id, Id>();

        //collect Orignal Intake Units and Ids
        for(Intake_Unit__c iu : [SELECT Id, Name, Unit_Type__c, Unit_Name__c, Census_Date__c, Intake__c, Sync_When_Start_Date_Reached__c, 
                                        Unit_of_Study__c, Unit_Start_Date__c, Unit_End_Date__c, Stage__c
                                FROM Intake_Unit__c 
                                WHERE Intake__c =: intakeId]) {
            intakeUnitOrig.add(iu);
            iuOrigIds.add(iu.Id);
        }

        //deep clone original Intake Units
        List<Intake_Unit__c> intakeUnitClone = intakeUnitOrig.deepClone(false, false, false);

        //assign the intake clone to intake unit clone.
        if(intakeUnitClone.size()>0){
            for(Intake_Unit__c i: intakeUnitClone){
                i.Intake__c = intakeClone.Id;   
            }
            insert intakeUnitClone;   
        }
        
        //collect IntakeUnitClone Ids
        for(Intake_Unit__c iu : intakeUnitClone){
            iuCloneIds.add(iu.Id);
        }
        
        //create map of IU orig Id to IU clone Id
        for(Integer i=0; i<iuCloneIds.size(); i++){
            intakeIdMap.put(iuOrigIds[i], iuCloneIds[i]);
        }
        //CLONE INTAKE UNIT END


        //CLONE INTAKE UNIT OF STUDY START
        //deep clone intake unit of study
        List<Intake_Unit_of_Study__c> iuosOrigList = [SELECT Id, Name, Intake_Unit_of_Competency__c, Unit__c 
                                                    FROM Intake_Unit_of_Study__c 
                                                    WHERE Intake_Unit_of_Competency__c IN: iuOrigIds];
        List<Intake_Unit_of_Study__c> iuosCloneList = iuosOrigList.deepClone(false, false, false);
        
        if(iuosCloneList.size() > 0){
            for(Intake_Unit_of_Study__c iuos : iuosCloneList){
                iuos.Intake_Unit_of_Competency__c = intakeIdMap.get(iuos.Intake_Unit_of_Competency__c);
            }
            insert iuosCloneList;
        }
        //CLONE INTAKE UNIT OF STUDY END
        

        //CLONE STAFF MEMBER START
        //deep clone staff member
        soql = getCreatableFieldsSOQL('Staff_Member__c','Intake__c = \''+intakeId+'\'');
        List<Staff_Member__c> staffMemberOrig = Database.query(soql);
        List<Staff_Member__c> staffMemberClone = staffMemberOrig.deepClone(false, false, false);

        //update intake
        if(staffMemberClone.size()>0){
            for(Staff_Member__c i: staffMemberClone){
                i.Intake__c = intakeClone.Id;
            }
            insert staffMemberClone;
        }
        //CLONE STAFF MEMBER END


        //CLONE CLASS CONFIGURATION START
        List<Class_Configuration__c> classConOrig = new List<Class_Configuration__c>();
        List<Class_Configuration__c> classConClone = new List<Class_Configuration__c>();
        Map<Id, Id> mapClassConIds = new Map<Id, Id>();
        List<Id> classConOrigIds = new List<Id>();
        List<Id> classConCloneIds = new List<Id>();

        //collect Class Configurations and Ids
        for(Class_Configuration__c cc : [SELECT Id, Name, Class_End_Time__c, Class_Room__c, Class_Start_Time__c,
                                                Day__c, End_Date_Time__c, Intake__c, Intake_Unit__c, Start_Date_Time__c, 
                                                Unit_Name__c, RecordTypeId, RecordType.Name
                                        FROM Class_Configuration__c
                                        WHERE Intake__c =: intakeId]) {
            classConOrig.add(cc);
            classConOrigIds.add(cc.Id);
        }

        //deep clone class config
        classConClone = classConOrig.deepClone(false, false, false);
    
        //update Intake then collect class config clone Ids
        if(classConClone.size()>0){
            for(Class_Configuration__c i: classConClone){
                i.Intake__c = intakeClone.Id;
            }
            insert classConClone;
 
            for(Class_Configuration__c cc: classConClone){
                classConCloneIds.add(cc.id);
            }
        }

        //create map of class config orig Id to class config clone Id
        for(Integer i = 0; i < classConCloneIds.size(); i++){
            mapClassConIds.put(classConOrigIds[i], classConCloneIds[i]);
        }
        //CLONE CLASS CONFIGURATION END


        //CLONE CLASSES START
        //If Dates were'nt changed
        if(origIntakeStart == cloneIntakeStart && origIntakeEnd == cloneIntakeEnd){
            Set<Id> cloneClassIds = new Set<Id>();
            List<Classes__c> classesClone = new List<Classes__c>();
            List<Classes__c> classesOrig = [SELECT Id, Name, Alternate_Class_Delivery_Option__c, Attendance_Percentage__c, Class_Configuration__c, 
                                                Class_Configuration__r.RecordTypeId, Class_Configuration__r.RecordType.Name, Class_Delivery__c, 
                                                Class_Size__c, End_Date_Time__c, External_ID__c, Intake__c, Intake_Unit__c, No_of_Students_that_attended_class__c, 
                                                No_of_Students_that_Didnt_Attend_Class__c, Room__c, Room_Address__c, Start_Date_Time__c, Unit_Code__c, Unit_Name__c   
                                            FROM Classes__c 
                                            WHERE Intake__c = :intakeId
                                            AND Class_Configuration__r.RecordType.Name != 'One-Off Class']; 
            //deep clone classes
            classesClone = classesOrig.deepClone(false, false, false);

            if(classesClone.size() > 0){
                for(Classes__c c: classesClone){
                    //update Intake
                    c.Intake__c = intakeClone.Id;
                    //update class configuration
                    if(mapClassConIds.containsKey(c.Class_Configuration__c)){
                        c.Class_Configuration__c = mapClassConIds.get(c.Class_Configuration__c);
                    }
                    //update intake unit
                    if(intakeIdMap.containsKey(c.Intake_Unit__c)){
                        c.Intake_Unit__c = intakeIdMap.get(c.Intake_Unit__c);
                    }                                
                }
                insert classesClone;

                for(Classes__c c: classesClone){
                    cloneClassIds.add(c.Id);
                }
            }
            
            List<Classes__c> notClonedClasses = [SELECT Id, Class_Configuration__c, Start_Date_Time__c, End_Date_Time__c FROM Classes__c WHERE Id NOT IN: cloneClassIds AND Intake__c =: intakeClone.Id];

            if(notClonedClasses.size()>0){
                delete notClonedClasses;
            }
        }
        //If dates were changed
        else {
            //23.SEPT.2014 WBM
            //22.APR.2014 AUG
            List<Classes__c> classesOrig = new List<Classes__c>();
            classesOrig = [SELECT Id, Name, Alternate_Class_Delivery_Option__c, Attendance_Percentage__c, 
                          Class_Configuration__c, Class_Configuration__r.RecordTypeId, Class_Configuration__r.RecordType.Name,
                          Class_Delivery__c, Class_Size__c, End_Date_Time__c, External_ID__c, Intake__c, Intake_Unit__c, Intake_Unit__r.Name,
                          No_of_Students_that_attended_class__c, No_of_Students_that_Didnt_Attend_Class__c, Room__c, Intake_Unit__r.Unit_Name__c, 
                          Room_Address__c, Start_Date_Time__c, Unit_Code__c, Unit_Name__c   
                             FROM Classes__c WHERE Intake__c =: intakeId AND Class_Configuration__r.RecordType.Name != 'One-Off Class' ORDER BY End_Date_Time__c ASC];               
            
            List<Classes__c> classList = new List<Classes__c>();
            classList = [SELECT Id, Name, Alternate_Class_Delivery_Option__c, Attendance_Percentage__c, 
                          Class_Configuration__c, Class_Configuration__r.RecordTypeId, Class_Configuration__r.RecordType.Name,
                          Class_Delivery__c, Class_Size__c, End_Date_Time__c, External_ID__c, Intake__c, Intake_Unit__c,
                          No_of_Students_that_attended_class__c, No_of_Students_that_Didnt_Attend_Class__c, Room__c, 
                          Room_Address__c, Start_Date_Time__c, Unit_Code__c, Unit_Name__c   
                             FROM Classes__c WHERE Intake__c =: intakeCloneId AND Class_Configuration__r.RecordType.Name != 'One-Off Class' ORDER BY End_Date_Time__c ASC];   
            
            system.debug('**Class List: ' + classList.size() + ' ,Orig: ' + classesOrig.size());
            
            Map<String, String> clonedIUMap = new Map<String, String>();
            for(Intake_Unit__c i: intakeUnitClone){
                clonedIUMap.put(i.Unit_Name__c, i.Id);
                system.debug('**Map Cloned: ' + i.Id + ' - ' + i.Unit_Name__c);
            }
            
            List<String> origIU = new List<String>();
            for(Classes__c c: classesOrig){
                if(c.Intake_Unit__c != null){
                    origIU.add(clonedIUMap.get(c.Intake_Unit__r.Unit_Name__c));
                }
                else if(c.Alternate_Class_Delivery_Option__c != null){
                    origIU.add(c.Alternate_Class_Delivery_Option__c);
                }
                else if(c.Intake_Unit__c == null && c.Alternate_Class_Delivery_Option__c == null){
                    origIU.add('');
                }
            }      

            system.debug('**Orig IU: ' + origIU.size() + ' ,Class List: ' + classList.size());
            for(Integer i=0; i<origIU.size(); i++){
                if(origIU.size() < classList.size() || origIU.size() == classList.size()){
                    if(origIU[i].contains('a1U')){
                        classList[i].Intake_Unit__c = origIU[i];
                        system.debug('**C1: ' + classList[i].Intake_Unit__c);
                    }
                    else if(origIU[i].containsNone('a1U') || origIU[i] != ''){
                        classList[i].Alternate_Class_Delivery_Option__c = origIU[i];
                        system.debug('**C2: ' + classList[i].Alternate_Class_Delivery_Option__c);
                    }
                    else if(origIU[i] == ''){
                        system.debug('**C3');
                    }
                }
                else if(origIU.size() > classList.size()){
                    if(i < classList.size()){
                        if(origIU[i].contains('a1U')){
                            classList[i].Intake_Unit__c = origIU[i];
                            system.debug('**C1: ' + classList[i].Intake_Unit__c);
                        }
                        else if(origIU[i].containsNone('a1U') || origIU[i] != ''){
                            classList[i].Alternate_Class_Delivery_Option__c = origIU[i];
                            system.debug('**C2: ' + classList[i].Alternate_Class_Delivery_Option__c);
                        }
                        else if(origIU[i] == ''){
                            system.debug('**C3');
                        }                    
                    }
                }
            }
            
            update classList;
        }

        List<Class_Configuration__c> cConDelete = new List<Class_Configuration__c>();
        for(Class_Configuration__c c: classConClone){
            if(c.RecordType.Name == 'One-Off Class'){
                cConDelete.add(c);
            }
        }
        delete cConDelete;
            
        PageReference p = new PageReference('/'+intakeClone.Id);
        p.setredirect(true);
        return p;
    }
    
    //cancel method
     public PageReference cancel(){
        PageReference p = new PageReference('/'+intakeId);
        p.setredirect(true);
        return p;
    }
}