/**
 * @description USI Identification Document object Trigger Handler Test Class
 * @author      Sairah Hadjinoor
 * @Company     CloudSherpas
 * @date        23.OCT.201
 *
 * HISTORY
 * - 23.OCT.2014    Sairah Hadjinoor      Created.
 */

@isTest
private class USIIdentificationDocTriggerHandler_Test {

	static testMethod void insertAndDeleteUSIDoc() {
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            Id recrdType = [select Id from RecordType where DeveloperName = 'PersonAccount' And SObjectType = 'Account' LIMIT 1].Id;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                sacct.Permission_Granted_to_Apply_For_USI__c = true;
                accList.add(sacct);
            //}
            insert accList;
            test.startTest();
            USI_Identification_Document__c newUSIDocument = new USI_Identification_Document__c();
            newUSIDocument.Document_Number_Encrypted__c = 'A82137812';
            newUSIDocument.Account__c = accList[0].Id;
            insert newUSIDocument;

            List<Account> accList2 = [Select USI_ID_Document_Submitted__c from Account where Id=: accList[0].Id];
            System.assert(accList2[0].USI_ID_Document_Submitted__c == True);

            delete newUSIDocument; 
            List<Account> accList3 = [Select USI_ID_Document_Submitted__c from Account where Id=: accList[0].Id];
            System.assert(accList3[0].USI_ID_Document_Submitted__c == False);

            test.stopTest();
        }       

	}

	/*static testMethod void insertAndDeleteBulkUSIDoc() {
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            Id recrdType = [select Id from RecordType where DeveloperName = 'PersonAccount' And SObjectType = 'Account' LIMIT 1].Id;
            List<Account> accList = new List<Account>();
            
            for(integer i=0; i<=200;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            }
            insert accList;

            Set<Id> accIdSet = new Set<Id>();
            for(Account acc: accList){
            	accIdSet.add(acc.Id);
            }
            test.startTest();
            List<USI_Identification_Document__c> USIList = new List<USI_Identification_Document__c>();

            for(integer x=0; x<=200;x++){
	            USI_Identification_Document__c newUSIDocument = new USI_Identification_Document__c();
	            newUSIDocument.Document_Number_Encrypted__c = 'A82137812';
	            newUSIDocument.Account__c = accList[x].Id;
	            USIList.add(newUSIDocument);
	        }

            insert USIList;

            //List<Account> accList2 = ;
            
            for(Account acc1: [Select USI_ID_Document_Submitted__c from Account where Id IN: accIdSet]){
            	System.assert(acc1.USI_ID_Document_Submitted__c == True);
            }
            

            delete USIList; 

            for(Account acc1: [Select USI_ID_Document_Submitted__c from Account where Id IN: accIdSet]){
            	System.assert(acc1.USI_ID_Document_Submitted__c == False);
            }
            test.stopTest();
        }       

	}*/
}