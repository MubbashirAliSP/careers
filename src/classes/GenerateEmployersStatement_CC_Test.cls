/**
 * @description Test Class for GenerateEmployersStatement_CC Class
 * @author Warjie Malibago
 * @date 31.JAN.2014
 *
 * HISTORY
 * - 31.JAN.2014    Warjie Malibago - Created.
*/
@isTest
public class GenerateEmployersStatement_CC_Test {
    static testMethod void testGenerateTimetable_1() {
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test', Part_of_Support_Group__c=TRUE);
                    //insert u;
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c lang = new Language__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        
        system.runAs(u){
            test.startTest();
            lang = TestCoverageUtilityClass.createLanguage();
            acc = TestCoverageUtilityClass.createStudent();
            acc2 = TestCoverageUtilityClass.createStudentEUOS(lang.Id);
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            /*
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            insert enrl;
            */
            
            PageReference testPage = Page.Employers_Statement;
            testPage.getParameters().put('aid', acc.Id);
            Test.setCurrentPage(testPage);
            
            GenerateEmployersStatement_CC gen = new GenerateEmployersStatement_CC();
            gen.acc2.Start_Date_Conga__c  = date.today().addDays(30);
            gen.acc2.End_Date_Conga__c = date.today().addMonths(5);
            
            gen.GenerateStatement();
            gen.Cancel();
            
            //Set EnrolmentId to Empty then Cancel
            gen.accountId = null;
            gen.Cancel();
            
            test.stopTest();
        }
    }
}