public without sharing class ApplicationFormButtonsCX {
	public ApplicationForm__c form {get; set;}

	public List<ApplicationFormButton__mdt> formButtons {get; set;}

	public ApplicationFormButtonsCX(ApexPages.StandardController controller) {
		Id recordId = controller.getId();
		form = [SELECT Id, FundingStreamTypeId__r.Name FROM ApplicationForm__c WHERE Id = :recordId];
		formButtons = [SELECT MasterLabel, DeveloperName, FundingStreamType__c, SortOrder__c FROM ApplicationFormButton__mdt WHERE FundingStreamType__c = '' ORDER BY SortOrder__c];

		if (String.isNotBlank(form.FundingStreamTypeId__c)) {
			List<ApplicationFormButton__mdt> allButtons = [SELECT MasterLabel, DeveloperName, FundingStreamType__c, SortOrder__c FROM ApplicationFormButton__mdt WHERE FundingStreamType__c = :form.FundingStreamTypeId__r.Name ORDER BY SortOrder__c];
			formButtons.addAll(allButtons);
		}
	}
}