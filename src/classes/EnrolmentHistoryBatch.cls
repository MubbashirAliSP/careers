global class EnrolmentHistoryBatch implements Database.Batchable<sObject> {
    private Date modifiedStartDate;
    private Date modifiedEndDate;
    private Date runTillMonth;
    private Date FromDate;
    public integer processed = 0;
    public Map<Id, Map<Date, boolean>> testHistory;
    public Map<Id, boolean> testStartIsSuspended;
    public String limitId;
    
    public EnrolmentHistoryBatch(Date modifiedStartDate, Date modifiedEndDate, Date runTillMonth, Date FromDate){
        this.modifiedStartDate = modifiedStartDate;
        this.modifiedEndDate = modifiedEndDate;
        this.runTillMonth = runTillMonth;
        this.FromDate = FromDate;
        system.assert( runTillMonth.day() == 1 , 'Run Till date must be on the first of the month');
        system.debug('**Modified Start Date: ' + this.modifiedStartDate);
        system.debug('**Modified End Date: ' + this.modifiedEndDate);
        system.debug('**Run Till Month: ' + this.runTillMonth);
        system.debug('**From Date: ' + this.FromDate);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        try{
            Date startDate = this.modifiedStartDate;
            Date endDate = this.modifiedEndDate;
    
            String soql = 'select id, Start_Date__c, End_Date__c, (Select ParentId, OldValue, NewValue, Field, CreatedDate From Histories Where Field=\'enrolment_status__c\' Order By CreatedDate ASC) From Enrolment__c ';
            soql += ' Where';
    
            if ( limitId != null && limitId != '' ){
                system.debug('**LIMIT: ' + limitId);
                soql += ' id=\'' + limitId + '\'';
            }    
            else{
                //limit enrolments processed to ones active during the start-end period
                soql += '(LastModifiedDate >= :startDate and LastModifiedDate <= :endDate)';
            }
            soql += ' Order By End_Date__c';
            
            return Database.getQueryLocator(soql);
            
        }catch(Exception e){
            system.debug('**ERROR EMAIL 1');
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
              TotalJobItems, CreatedBy.Email
              FROM AsyncApexJob WHERE Id =
              :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'ben@villagechief.com', a.CreatedBy.Email};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Enrolment History Recalculation Start ERROR');
           mail.setPlainTextBody
           (e+'\n' + e.getStackTraceString());
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           return Database.getQueryLocator('Select Id From Enrolment__c Where Name = \'never match anything\'');
        }
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            List<Enrolment_History__c> newHistory = new List<Enrolment_History__c>();
            List<Enrolment_History__c> updateHistory = new List<Enrolment_History__c>();
            List<Enrolment_History__c> deleteHistory = new List<Enrolment_History__c>();
            
            //temporary tables
            Set<Enrolment_History__c> historyToDelete = new Set<Enrolment_History__c>();
            List<Enrolment_History__c> enrolmentHistoryList = new List<Enrolment_History__c>();
            
            //find existing Enrolment_History__c
            Map<Id, Map<Date, Enrolment_History__c>> enrolmentsExistingHistory = new Map<Id, Map<Date, Enrolment_History__c>>();
            for ( Enrolment_History__c h : [
                Select Enrolment__c, Is_Active__c, Month_of_Enrolment__c From Enrolment_History__c Where Enrolment__c in :scope
            ] ){
                Map<Date, Enrolment_History__c> e = enrolmentsExistingHistory.get(h.Enrolment__c);
                if ( e == null ){
                    e = new Map<Date, Enrolment_History__c>();
                    enrolmentsExistingHistory.put(h.Enrolment__c, e);
                }
                e.put(h.Month_of_Enrolment__c, h);
            }
            
            
            
            for ( sObject obj : scope ){
                Enrolment__c enrolment = (Enrolment__c)obj;
                
                
                //get field history...
                boolean isSuspended = false;
                Map<Date, boolean> fieldHistory = new Map<Date, boolean>();
                if ( Test.isRunningTest() ){
                    //for testing we can use our test data...
                    if ( testHistory != null && testHistory.containsKey(enrolment.id) )
                        fieldHistory = testHistory.get(enrolment.id);
                    if ( testStartIsSuspended != null && testStartIsSuspended.containsKey(enrolment.id) )
                        isSuspended = testStartIsSuspended.get(enrolment.id);
                }else{
                    boolean isFirst = true;
                    for ( Enrolment__History h : enrolment.getSobjects('Histories') ){
                        if ( h.NewValue == null )
                            continue;
                        if ( isFirst ){
                            //figure out what start state is...
                            isFirst = false;
                            isSuspended = h.OldValue != null && !((String)h.OldValue).contains('Active');
                        }
                        boolean s = !((String)h.NewValue).contains('Active');
                        fieldHistory.put(Date.newInstance(h.CreatedDate.year(), h.CreatedDate.month(), h.CreatedDate.day()), s);
                    }
                }
                
                //get Enrolment_History__c for enrolment - and record the objects so we know what to delete
                Map<Date, Enrolment_History__c> existingEnrolmentHistory = enrolmentsExistingHistory.get(enrolment.id);
                historyToDelete.clear();
                if ( existingEnrolmentHistory != null )
                    historyToDelete.addAll(existingEnrolmentHistory.values());
                
                Date endDate = enrolment.End_Date__c.toStartOfMonth();
                if ( endDate > runTillMonth )
                    endDate = runTillMonth;
                Date startDate = enrolment.Start_Date__c.toStartOfMonth();
                if ( startDate < fromDate )
                    startDate = fromDate;
                for ( Date monthDate = startDate; monthDate <= EndDate; monthDate=monthDate.addMonths(1) ){
                    Enrolment_History__c h;
                    Date lastDayOfMonth = monthDate.addMonths(1).addDays(-1);
                    
                    //get or create Enrolment_History__c object
                    if ( existingEnrolmentHistory != null && existingEnrolmentHistory.containsKey(monthDate) ){
                        h = existingEnrolmentHistory.get(monthDate);
                        historyToDelete.remove(h);
                    }else
                        h = new Enrolment_History__c();
                    enrolmentHistoryList.add(h);
                    
                    h.Enrolment__c = enrolment.id;
                    h.Month_of_Enrolment__c = monthDate;
                    
                    //set name to date
                    h.Name = h.Month_of_Enrolment__c + '';
                    h.Name = h.Name.subString(0, h.Name.indexOf(' '));
                    
                    //create a unique field...
                    h.Unique__c = h.Month_of_Enrolment__c + '';
                    h.Unique__c = h.Unique__c.subString(0, h.Unique__c.indexOf(' '));
                    h.Unique__c += '-' + h.Enrolment__c;
                    
                    //add to update/insert history
                    if ( h.id == null )
                        newHistory.add(h);
                    else
                        updateHistory.add(h);
                    
                    //flag whether student was actually enrolled on lastDayOfMonth
                    if ( (enrolment.Start_Date__c <= lastDayOfMonth && enrolment.End_Date__c >= lastDayOfMonth) && (enrolment.Start_date__c.month() + enrolment.Start_date__c.year() != enrolment.End_date__c.month() + enrolment.End_date__c.year()  ) )
                        h.Is_Active__c = null;
                    else
                        h.Is_Active__c = false;
                        
                    
                }
                
                //get a sorted field history iterator
                List<Date> fhByDate = new List<Date>();
                fhByDate.addAll(fieldHistory.keySet());
                fhByDate.sort();
                Iterator<Date> fhDateItr = fhByDate.iterator();
                Date fhDate = null;
                if ( fhDateItr.hasNext() )
                    fhDate = fhDateItr.next();
                
                //go through all Enrolment_History__c objects and fill in the isActive
                //we do a double-inner iteratation through the Enrolment_History__c and field history to find interesting changes
                //and keep a running status of whether the student is suspended.
                for ( Enrolment_History__c eh : enrolmentHistoryList ){
                    if ( eh.get('Is_Active__c') == null ){
                        Date lastDayOfMonth = eh.Month_of_Enrolment__c.addMonths(1).addDays(-1);
                        while ( fhDate <= lastDayOfMonth ){
                            isSuspended = fieldHistory.get(fhDate);
                            if ( fhDateItr.hasNext() )
                                fhDate = fhDateItr.next();
                            else
                                break;
                        }
                        eh.Is_Active__c = !isSuspended;
                    }
                }
                
                deleteHistory.addAll(historyToDelete);
            }
            insert newHistory;
            update updateHistory;
            delete deleteHistory;
        
        }catch(Exception e){
            system.debug('**ERROR EMAIL 2');
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
              TotalJobItems, CreatedBy.Email
              FROM AsyncApexJob WHERE Id =
              :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'ben@villagechief.com', a.CreatedBy.Email};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Enrolment History Recalculation Batch ERROR');
           mail.setPlainTextBody
           (e+'\n' + e.getStackTraceString());
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    global void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
          TotalJobItems, CreatedBy.Email
          FROM AsyncApexJob WHERE Id =
          :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Enrolment History Recalculation ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       system.debug('**SEND EMAIL');
    }
}