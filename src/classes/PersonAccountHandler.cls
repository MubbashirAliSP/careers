/**
 * @description Intake Students Trigger Handler 
 * @author      Vienna Austria
 * @Company     CloudSherpas
 * @date        11.OCT.2012
 *
 * HISTORY
 * - 11.OCT.2012    Vienna Austria      Created.
 * - 31.JAN.2013    Janella Canlas      added method to populate Intake Student Letter Address field
 * - 04.FEB.2013    Janella Canlas      comment out codes to populate Intake Student Letter Address field
 * - 06.FEB.2013    Janella Canlas      add method to populate Intake Student Mobile phone
 * - 18.OCT.2013    Michelle Magsarili  - removal of CHESSN references for Encrypted CHESSN removal
 * - 08.JUL.2014    Warjie Malibago     TFN validation will only fire for non SysAd and VFH Level 2
 * - 23.OCT.2014    Sairah Hadjinoor    updated to delete all USI document when a student's USI is verified
 * - 10.FEB.2015    Warjie Malibago     - add CRM Casis in the allowed profiles
 * - 12.FEB.2015    Warjie Malibago     - remove exception to the 3 profiles; all profiles must follow TFN algorithm
 */
public with sharing class PersonAccountHandler {    
    
        
    public static Boolean validateTFN(string sTFN){
    /*------------------------------------------------------------
    Description:   1.   For 8 Digit TFNs use weight factor;    10, 7, 8, 4, 6, 3, 5, 2, 1
                        For 9 Digit TFNs use weight factor;    10, 7, 8, 4, 6, 3, 5, 1
                        Apply the weight factor against the TFN digits - from left to right. Sum the products of the numbers.
                   2.   Divide the resulting sum by 11. If there is no remainder after dividing by 11, then the TFN is a valid TFN.
                   source: ATO PDFs - http://bioinf.wehi.edu.au/folders/fred/tfn.html
    Inputs:        The record type, type, message, status and customerId
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ------------------------------------------------------------*/      
        /*
        Profile_CS__c prof = Profile_CS__c.getInstance('System Administrator');
        Profile_CS__c prof2 = Profile_CS__c.getInstance('CASIS - VFH (Level 2+)');
        */
        if(!Test.isRunningTest()){
            /*
            12.FEB.2015 WBM
            String profileId = UserInfo.getProfileId();   
            Set<String> profileIds = new Set<String>();
            for(Profile_CS__c p: Profile_CS__c.getAll().values()){
                profileIds.add(p.Profile_Id__c);
            }
            
            if(!profileIds.contains(profileId) profileId != prof.Profile_Id__c && profileId != prof2.Profile_Id__c){
            */
                if(sTFN == null || sTFN == ''){
                    return true;
                }
                if(GlobalUtility.isNumeric(sTFN)){ 
                    if(sTFN != '000000000'){ //12.FEB.2015 WBM -- 000 000 000 is not allowed       
                        set<String> tfnWhiteList = new set<String>{'333333333', '444444441', '444444442',
                            '555555555', '666666666', '777777777', '888888888', '1'};
                        if(!tfnWhiteList.contains(sTFN)){
                            Integer[] aiWeight;
                            if (sTFN.length() == 8) {
                                // Eight Digit Tax File numbers
                                aiWeight = new Integer[]{10, 7, 8, 4, 6, 3, 5, 1};
                            } else if (sTFN.length() == 9) {
                                // Nine Digit Tax File numbers
                                aiWeight = new Integer[]{10, 7, 8, 4, 6, 3, 5, 2, 1};
                            } else {
                                // It is not the correct length (8 or 9)
                                return false;
                            }
            
                            Boolean bReturn = false;
                            Integer iSum = 0;
                            String testcomp ='';
                            for(integer ipTr=0; ipTr<=sTFN.length()-1; iPtr++){
                                if(ipTr==sTFN.length()-1){
                                    iSum += (integer.valueOf(sTFN.subString(iPtr)) * integer.valueOf(aiWeight[iPtr]));
                                }
                                else{
                                    iSum += (integer.valueOf(sTFN.subString(iPtr, iPtr+1)) * integer.valueOf(aiWeight[iPtr]));
                                }
                                System.debug('@@isum '+iSum);
                            }
                            if ( math.mod(iSum , 11) == 0 )
                            {
                                bReturn = true;
                            }
                            else
                            {   //INVALID
                                bReturn = false;
                            }
                            return bReturn;
                        }
                        else{
                            //Accept TFN '333333333' and '888888888'
                            return true;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else
                {   //invalid if TFN has letters
                    return false;
                }
            /*}
            else{
                return true;
            }*/
        }
        //will run for test class only
        else{
            if(sTFN == null || sTFN == ''){
                return true;
            }
            if(GlobalUtility.isNumeric(sTFN)){ 
    
                set<String> tfnWhiteList = new set<String>{'333333333', '444444441', '444444442',
                    '555555555', '666666666', '777777777', '888888888', '1'};
                if(!tfnWhiteList.contains(sTFN)){
                    Integer[] aiWeight;
                    if (sTFN.length() == 8) {
                        // Eight Digit Tax File numbers
                        aiWeight = new Integer[]{10, 7, 8, 4, 6, 3, 5, 1};
                    } else if (sTFN.length() == 9) {
                        // Nine Digit Tax File numbers
                        aiWeight = new Integer[]{10, 7, 8, 4, 6, 3, 5, 2, 1};
                    } else {
                        // It is not the correct length (8 or 9)
                        return false;
                    }
    
                    Boolean bReturn = false;
                    Integer iSum = 0;
                    String testcomp ='';
                    for(integer ipTr=0; ipTr<=sTFN.length()-1; iPtr++){
                        if(ipTr==sTFN.length()-1){
                            iSum += (integer.valueOf(sTFN.subString(iPtr)) * integer.valueOf(aiWeight[iPtr]));
                        }
                        else{
                            iSum += (integer.valueOf(sTFN.subString(iPtr, iPtr+1)) * integer.valueOf(aiWeight[iPtr]));
                        }
                        System.debug('@@isum '+iSum);
                    }
                    if ( math.mod(iSum , 11) == 0 )
                    {
                        bReturn = true;
                    }
                    else
                    {   //INVALID
                        bReturn = false;
                    }
                    return bReturn;
                }
                else{
                    //Accept TFN '333333333' and '888888888'
                    return true;
                }
            }
            else
            {   //invalid if TFN has letters
                return false;
            }
        }
    }
    /*
        Update fields of EUOS records related to the Account
    */
    public static void updateEUOS(List<Account> AccountsForUpdate, Set<Id> personIds){
        List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
        Map<Id, Account> studMap = new Map<Id, Account>();
        euosList = [select Enrolment_Unit__r.Enrolment__r.Student__r.Tax_File_Number__c,
                        Enrolment_Unit__r.Enrolment__r.Student__r.Country_of_Birth_Code__c, 
                        Enrolment_Unit__r.Enrolment__r.Student__r.Main_Language_Spoken_at_Home_Identifier__c
                        FROM Enrolment_Unit_of_Study__c
                        WHERE Enrolment_Unit__r.Enrolment__r.Student__c IN: personIds
                        AND Enrolment_Unit__r.Enrolment__r.RecordType.Name = 'VET Fee Help Enrolment'
                        ];
        for(Account a : AccountsForUpdate){
            studMap.put(a.Id, a);
        }                       
        if(euosList != null){
            for(Enrolment_Unit_of_Study__c e : euosList){
                if(studMap.containsKey(e.Enrolment_Unit__r.Enrolment__r.Student__c)){
                    Account student = studMap.get(e.Enrolment_Unit__r.Enrolment__r.Student__c);
                    e.Tax_File_Number__c = student.Tax_File_Number__c;
                    e.Country_of_Birth_Code__c = student.Country_of_Birth_Code__c;
                    if(student.Main_Language_Spoken_at_Home__c != null){
                        e.Language_Spoken_at_Home_Code__c = student.Main_Language_Spoken_at_Home_Identifier__c;
                    }
                    else{
                        e.Language_Spoken_at_Home_Code__c = '9999';
                    }
                }
            }
            /*for(Enrolment_Unit_of_Study__c e : euosList){
                
                for(Account a : AccountsForUpdate){
                    if(e.Enrolment_Unit__r.Enrolment__r.Student__c == a.Id){
                        e.CHESSN__c = a.CHESSN__c;
                        e.Tax_File_Number__c = a.Tax_File_Number__c;
                        e.Country_of_Birth_Code__c = a.Country_of_Birth_Code__c;
                        if(a.Main_Language_Spoken_at_Home__c != null){
                            e.Language_Spoken_at_Home_Code__c = a.Main_Language_Spoken_at_Home_Identifier__c;
                        }
                        else{
                            e.Language_Spoken_at_Home_Code__c = '9999';
                        }
                    }   
                }
            }*/
        }
        try{
            update euosList;}catch(Exception e){system.debug('ERROR: '+ e.getMessage());
        }
    }
    /*
        Update Intake Letter Address of related Intake Student records
    */
    /*public static void populateIntakeStudentLetterAddress(Set<Id> studentIds, List<Account> studentList){
        List<Intake_Students__c> intakeStudentList = new List<Intake_Students__c>();
        Map<Id, Account> studentMap = new Map<Id,Account>();
        intakeStudentList = [SELECT Id, Enrolment__r.Employer__c, Intake_Letter_Address__c, Student__c
                                FROM Intake_Students__c
                                WHERE Student__c IN: studentIds];
        for(Account a : studentList){
            studentMap.put(a.Id,a);
        }
        if(intakeStudentList.size() > 0){
            for(Intake_Students__c i : intakeStudentList){
                if(i.Enrolment__r.Employer__c == null){
                    Account student = studentMap.get(i.Student__c);
                    i.Intake_Letter_Address__c = student.Name + '\r\n' + 
                                                 student.PersonMailingStreet + '\r\n' +
                                                 student.PersonMailingCity + ' ' + student.PersonMailingState + ' ' + student.PersonMailingPostalCode + '\r\n' +
                                                 student.PersonMailingCountry;
                }
            }
            try{
                update intakeStudentList;
            }
            catch(Exception e){
                system.debug('ERROR: '+ e.getMessage());
            }
        }
    }*/
    
    /*
        This method will populate related Intake Student records Mobile field upon Update
    */
    public static void populateIntakeStudentMobile(Set<Id> studentIds, List<Account> studentList){
        List<Intake_Students__c> intakeStudentList = new List<Intake_Students__c>();
        Map<Id, Account> studentMap = new Map<Id,Account>();
        /*intakeStudentList = [SELECT Id, Mobile__c, Student__c
                                FROM Intake_Students__c
                                WHERE Student__c IN: studentIds];*/
        intakeStudentList = [SELECT Id, Student__c
                                FROM Intake_Students__c
                                WHERE Student__c IN: studentIds];
        for(Account a : studentList){
            studentMap.put(a.Id,a);
        }
        if(intakeStudentList.size() > 0){
            for(Intake_Students__c i : intakeStudentList){
                //if(i.Enrolment__r.Employer__c == null){
                    Account student = studentMap.get(i.Student__c);
                    //i.Mobile__c = student.PersonMobilePhone;
                //}
            }
            try{
                update intakeStudentList;}catch(Exception e){system.debug('ERROR: '+ e.getMessage());
            }
        }
    }
    /*
        This method will populate lookups default value upon insert jcanlas 24APR2013
    */
    public static void populatePersonAccountLookups(List<Account> lookupPopulateList){
        Id laborForce = [select Id from Labour_Force_Status_Code__c where Code__c ='@@'].Id;
        Id countryOfBirth = [select Id from Country__c where Country_Code__c ='@@@@'].Id;
        Id language = [select Id from Language__c where Code__c ='@@@@'].Id;
        for(Account a : lookupPopulateList){
            if(a.Main_Language_Spoken_at_Home__c == null && language != null){
                a.Main_Language_Spoken_at_Home__c = language;
            }
            if(a.Country_of_Birth__c == null && countryOfBirth != null){
                a.Country_of_Birth__c = countryOfBirth;
            }
            if(a.Labour_Force_Status_1__c == null && laborForce != null){
                a.Labour_Force_Status_1__c = laborForce;
            }
        }
    }

    public static void deleteAllUSIDocument(Set<Id> newAccIdSet) {
        /**
         * @description This deletes all USI documnet when the student Identifier is verified
         * @author      Sairah Hadjinoor
         * @Company     CloudSherpas
         * @date        23.OCT.2014
         *
         * HISTORY
         * - 23.OCT.2014    Sairah Hadjinoor      Created.
        **/
        List<USI_Identification_Document__c> usiDocumentList = new List<USI_Identification_Document__c>();
        //get all USI Identification Document from the associated accounts
        for(USI_Identification_Document__c usi: [Select Name from USI_Identification_Document__c where Account__c IN: newAccIdSet]){
            //add to list
            usiDocumentList.add(usi);
        }
        //check if list is not empty
        if(!usiDocumentList.isEmpty()){
            //if not then proceed to deleting the USI Document List
            try{
                delete usiDocumentList;
            }
            //catch all unexpected errors
            catch(Exception e){
                 system.debug('ERROR: '+ e.getMessage());
            }
        }
    }
}