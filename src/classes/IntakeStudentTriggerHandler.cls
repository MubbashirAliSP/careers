/**
 * @description Intake Students Trigger Handler 
 * @author      Janella Canlas
 * @Company     CloudSherpas
 * @date        31.JAN.2013
 *
 * HISTORY
 * - 31.JAN.2013    Janella Canlas      Created.
 * - 04.FEB.2013    Janella Canlas      Comment method populateIntakeLetterAddress
 */
public with sharing class IntakeStudentTriggerHandler {
    /*
        This method will retrieve Business Address or Student Address (if Employer of Enrolment is null) 
        and populate Intake Student Letter Address
    */
    /*public static void populateIntakeLetterAddress(List<Intake_Students__c> intakeStudentList){
        Set<Id> enrolmentIds = new Set<Id>();
        Set<Id> studentIds = new Set<Id>();
        List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
        List<Account> studentList = new List<Account>();
        Map<Id, Enrolment__c> enrolmentMap = new Map<Id, Enrolment__c>();
        Map<Id, Account> studentMap = new Map<Id, Account>();
        for(Intake_Students__c i : intakeStudentList){
            enrolmentIds.add(i.Enrolment__c);
            studentIds.add(i.Student__c);
        }
        enrolmentList = [SELECT Employer__c, Employer__r.Name, Id, Employer__r.BillingCity, Employer__r.BillingStreet, Employer__r.BillingState, 
                            Employer__r.BillingCountry, Employer__r.BillingPostalCode 
                            FROM Enrolment__c WHERE Id IN: enrolmentIds];
        for(Enrolment__c e : enrolmentList){
            enrolmentMap.put(e.Id, e);
        }
        studentList = [SELECT Name, Id, PersonTitle, PersonMailingCity, PersonMailingStreet, PersonMailingState, 
                            PersonMailingCountry, PersonMailingPostalCode 
                            FROM Account WHERE Id IN: studentIds];
        for(Account a : studentList){
            studentMap.put(a.Id, a);
        }       
        for(Intake_Students__c i : intakeStudentList ){
            Enrolment__c enrolment = enrolmentMap.get(i.Enrolment__c);
            if(enrolment.Employer__c != null){
                i.Intake_Letter_Address__c = enrolment.Employer__r.Name + '\r\n' + 
                                             enrolment.Employer__r.BillingStreet + '\r\n' +
                                             enrolment.Employer__r.BillingCity + ' ' + enrolment.Employer__r.BillingState + ' ' + enrolment.Employer__r.BillingPostalCode + '\r\n' +
                                             enrolment.Employer__r.BillingCountry;
            }
            else{
                Account student = studentMap.get(i.Student__c);
                i.Intake_Letter_Address__c = student.Name + '\r\n' + 
                                             student.PersonMailingStreet + '\r\n' +
                                             student.PersonMailingCity + ' ' + student.PersonMailingState + ' ' + student.PersonMailingPostalCode + '\r\n' +
                                             student.PersonMailingCountry;
            }
        }                   
    }*/
    /*
        This method populates the Mobile Number of the student upon Insert
    */
    public static void populateMobileNumber(List<Intake_Students__c> intakeStudentList){
        Set<Id> studentIds = new Set<Id>();
        List<Account> studList = new List<Account>();
        for(Intake_Students__c i : intakeStudentList){
            studentIds.add(i.Student__c);
        }
        studList = [select Id, PersonMobilePhone from Account where Id IN: studentIds];
        
        if(studList.size() > 0){
            for(Intake_Students__c i : intakeStudentList){
                for(Account a : studList){
                    if(i.Student__c == a.Id){
                        //i.Mobile__c = a.PersonMobilePhone;
                    }
                }
            }
        }
    }
}