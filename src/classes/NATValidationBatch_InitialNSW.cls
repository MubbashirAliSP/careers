global without sharing class NATValidationBatch_InitialNSW implements Database.Batchable<sObject>,Database.Stateful {
    
	global String currentState;
    global String query;
    global Id tOrgId;
	global List<String> selectedEnrolmentIds;
    global List<String> selectedStudentIds;
    global Boolean delOldFiles;
    global NAT_Validation_Log__c log;
    global final Integer max_query_length = 131072;
    global String queryStringStudents;
    global String queryStringMain;
   	global String endQuery;
    global String libraryId;
    global String enrolmentIdsString;
    global String studentIdsString;

    
    global NATValidationBatch_InitialNSW(Id trainingOrgId, List<String> enrolmentIds, Boolean deleteOldFiles) {
                
        currentState = 'New South Wales APL';
       	tOrgId = trainingOrgId;
        selectedEnrolmentIds = enrolmentIds;
        delOldFiles = deleteOldFiles;
        
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        log = new NAT_Validation_Log__c(Validation_State__c = currentState, Training_Organisation_Id__c = tOrgId, Delete_Existing_NAT_Files__c = delOldFiles);
        
        enrolmentIdsString = '';
        studentIdsString = '';
        
        for(String s : selectedEnrolmentIds) {
            enrolmentIdsString += '\'' + s + '\',';
        }       
        
        enrolmentIdsString = enrolmentIdsString.lastIndexOf(',') > 0 ? '(' + enrolmentIdsString.substring(0,enrolmentIdsString.lastIndexOf(',')) + ')' : enrolmentIdsString;
        
        query = 'SELECT Id, Student__c FROM Enrolment__c WHERE ID in ' + enrolmentIdsString + ' AND Final_Payment_Made__c = false' ;	
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
                        
        for (SObject s :scope) {
            Enrolment__c e = (Enrolment__c) s;
            studentIdsString += '\'' + e.Student__c + '\',';
        }
       
    }
       
    global void finish(Database.BatchableContext BC) {
                
        studentIdsString = studentIdsString.lastIndexOf(',') > 0 ? '(' + studentIdsString.substring(0,studentIdsString.lastIndexOf(',')) + ')' : studentIdsString;
                 
        String queryStringMain = 'Select Id,' +
                            'Name,' +
                            'Enrolment__r.Qualification__c,'+
                            'Scheduled_Hours__c,'+
                            'Unit_of_Competency_Identifier__c,'+
                            'Training_Delivery_Location__c,'+
                            'Unit__r.RecordType.Name,'+
                            'Unit__r.Field_of_Education_Identifier__c,'+
                            'Unit__r.Unit_Flag__c,'+
                            'Unit__r.Vet_Non_Vet__c,'+
                            'Enrolment__r.Student__c,'+
                            'Enrolment__r.Student_Identifier__c,'+
                            'Delivery_Location_Identifier__c,'+
                            'Delivery_Mode_Identifier__c,'+
                            'AVETMISS_National_Outcome__c,'+
                            'Commencing_Course_Identifier__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c,'+
                            'Purchasing_Contract_Identifier__c,'+
                            'VET_in_Schools__c,'+
                            'Registration_Number_Identifier__c,'+
                            'Client_Identifier_New_Apprenticeships__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c,'+
                            'Enrolment__r.Qualification__r.Associated_Course_Identifier__c,'+
                            'Reportable_Tuition_Fee__c,'+
                            'Fee_Exemption_Identifier__c,'+
                            'Enrolment__r.Specific_Program_Identifier__c,'+
                            'Enrolment__r.Training_Contract_Identifier__c,'+
                            'Purchasing_Contract_Schedule_Identifier__c,'+
                            'Hours_Attended__c,'+
                            'Enrolment__r.Fee_Exemption__r.Code__c,' +
                            'Course_Commencement_Date__c,'+
                            'Eligibility_Exemption__c,'+
                            'VET_Fee_Help__c,'+
                            'ANZSIC_Code__c,'+
                            'Enrolment__c,' +
                            'Full_Time_Learning__c,'+
                            'Start_Date__c,'+
                            'End_Date__c,'+
                            'Study_Reason_Identifier__c,'+
                            'Unit__r.name,'+
                            'Qualification_Identifier__c,'+
                            'Has_Been_Superseded__c,' +
                            'Enrolment__r.Qualification__r.Parent_Qualification_Code__c,'+
                            'Enrolment__r.Qualification_Identifier__c,'+
                            'Enrolment__r.Start_Date__c,'+
                            'Enrolment__r.End_Date__c,'+
                            'Unit__r.Unit_Flag_Identifier__c,'+
                            'Unit__r.Unit_Name__c,'+
                            'Student_Identifier__c,'+ 
                            'Enrolment__r.Qualification_Hours__c,'+
                            'Line_Item_Qualification_Unit__r.Nominal_Hours__c,'+
                            'Enrolment__r.Qualification__r.Parent_Qualification_Id__c,'+
                            'Enrolment__r.VET_FEE_HELP_Indicator__c,'+
                            'Enrolment__r.Victorian_Commencement_Date__c,'+
                            'Enrolment__r.Delivery_Location_State__c,'+
                            'Training_Delivery_Location__r.Training_Organisation_ABN__c,' +    
                            'Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c,' + 
                            'Supersession_Count__c,' +  
                            'Superseded_1_EU_ID__c,' +
                            'Superseded_1_Unit_ID__c,' +
                            'Superseded_1_EU_End_Date__c,' +
                            'Superseded_2_EU_End_Date__c,' +
                            'Superseded_2_EU_ID__c,' +
                            'Superseded_2_Unit_ID__c,' +
                            'Superseded_3_EU_End_Date__c,' +
                            'Superseded_3_EU_ID__c,' +
                            'Superseded_3_Unit_ID__c,' +
                            'Superseded_4_EU_End_Date__c,' +
                            'Superseded_4_EU_ID__c,' +
                            'Superseded_4_Unit_ID__c,' +
                            'Superseded_5_EU_End_Date__c,' +
                            'Superseded_5_EU_ID__c,' +
                            'Superseded_5_Unit_ID__c,' +
                            'Superseded_6_EU_End_Date__c,' +
                            'Superseded_6_EU_ID__c,' +
                            'Superseded_6_Unit_ID__c,' +
                            'Victorian_Tuition_Rate__c,' +
                            'Enrolment__r.Enrolment_Category_Identifier__c,' +
                            'WA_Resource_Fee__c,' +
                            'Course_Site_ID__c,' +
                            'LastModifiedDate,' +
                            'Enrolment__r.Booking_ID__c,' +
                            'Enrolment__r.Commitment_Identifier__c,' +
                            'Enrolment__r.Enrolled_School_Identifier__c,' +
                            'Enrolment__r.Enrolment_Status__c,' +
                            'Enrolment__r.Training_Type_Identifier__c,' +
                            'Enrolment__r.Training_Plan_Developed__c,' +
            				'Enrolment__r.Name,' +
                            'Client_Fees_Other__c';     
                             
        String endQuery = ' FROM Enrolment_Unit__c WHERE Enrolment__c in ' + EnrolmentIdsString  + ' AND Enrolment__r.Final_Payment_Made__c = false AND Unit__r.RecordType.Name = \'Unit Of Competency\'' ;
                     
        String queryStringStudents =   'SELECT ID, Student_Identifer__c,Reporting_Other_State_Identifier__c,PersonOtherPostalCode, Highest_School_Level_Comp_Identifier__c, Year_Highest_Education_Completed__c,'+
                                'Sex__c, PersonBirthdate, PersonMailingPostalCode, PersonMailingState,Indigenous_Status_Identifier__c,'+
                                'Main_Language_Spoken_at_Home__r.Name, Main_Language_Spoken_at_Home_Identifier__c, Labour_Force_Status_Identifier__c,'+ 
                                'Country_of_Birth_Identifer__c, At_School_Flag__c,'+
                                'Client_Industry_of_Employment__r.Division__c,' + 
                                'Proficiency_in_Spoken_English_Identifier__c,Learner_Unique_Identifier__c,'+
                                'Has_prior_educational_achievement_s__c,Prior_Achievement_Flag__c,'+
                                'Prior_Achievement_Type_s__c,Prior_Education_Achievement_Recognition__c,Does_student_have_a_disability__c,Disability_Type__c,Disability_Flag_0__c,'+
                                'FirstName, LastName, PersonMailingStreet, Full_Student_Name__c,'+
                                'PersonMailingCity, State_Identifier__c, PersonHomePhone, Work_Phone__c, Address_building_property_name__c,Address_flat_unit_details__c,Address_street_number__c,Address_street_name__c,Postal_Delivery_Box__c,'+
                                'PersonMobilePhone, PersonEmail, Victorian_Student_Number__c, Salutation,Name_for_encryption_student__c,Age_Group__c,Unique_Student_Identifier__c,'+
                                'Postal_building_property_name__c,Postal_flat_unit_details__c,Postal_street_number__c,Postal_street_name__c,Postal_suburb_locality_or_town__c, '+                                
                                'Labour_Force_Status_1__c, LastModifiedDate, Client_Industry_of_Employment__c, Client_Occupation_Identifier__c, Suburb_locality_or_town__c, USI_Status__c, Address_Post_Code__c,' +
                                'Labour_Force_Status_1__r.Code__c, Training_Org_Identifier__c, AVETMISS_Organisation_Name__c, Training_Org_Type_Identifier__c, BillingStreet, BillingCity, BillingPostalCode, Address_Dwelling_Type_Identifier__c, Education_Identifier__c' +
                                ' FROM Account WHERE Id in ' + studentIdsString;

        log.end_query__c = endQuery;
        
        queryStringMain += endQuery;
        
        log.query_main__c = queryStringMain;
        
        if(queryStringStudents.length() > max_query_length ) {
        	log.query_students__c = queryStringStudents.Substring(0,max_query_length);
        	String queryStringStudents2 =  queryStringStudents.Substring(max_query_length);
            String queryStringStudents3;
            if(queryStringStudents2.length() > max_query_length ) {
                log.query_students_2__c = queryStringStudents2.Substring(0,max_query_length);
                queryStringStudents3 = queryStringStudents2.Substring(max_query_length);
				if(queryStringStudents3.length() > max_query_length ) {
                    log.query_students_3__c = queryStringStudents3.Substring(0,max_query_length);
                    log.Query_Students_4__c = queryStringStudents3.Substring(max_query_length);
                }
                else {
                    log.query_students_3__c = queryStringStudents3;
                }
            }
        }
        else {
            log.query_students__c = queryStringStudents;

        }
        
        		
        if (currentState == 'Queensland') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
        }
        else if (currentState == 'New South Wales' || currentState == 'New South Wales APL') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        }
        else if (currentState == 'Australian Capital Territory') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files ACT' LIMIT 1].Id;
        }
        else if(currentState == 'Victoria') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        }
        else if (currentState == 'South Australia') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        }
        else if (currentState == 'Western Australia') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files WA' LIMIT 1].Id;
        }
        else if(currentState == 'Northern Territory') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NT' LIMIT 1].Id;
        }
        else if(currentState == 'Tasmania') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files TAS' LIMIT 1].Id;
        }
        else if(currentState == 'National') {
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files National' LIMIT 1].Id;
        }

        Account tOrgAct = [ SELECT ID, Name FROM Account WHERE ID = :tOrgId];
        
        log.RTO__c = tOrgAct.Name;
        
        log.Content_Library_Id__c = libraryId;
                    
        Insert log;
        
        Database.executeBatch(new NATValidationBatch_Students( log.id ) ); //students batch will call the other batches

    }
    
}