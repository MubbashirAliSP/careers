/**
 * @description Batch process for cleaning old export records to improve export batch query performance
 * @author Ranyel Maliwanag
 * @date 12.NOV.2015
 */
public with sharing class InPlaceExportCleanupBatch implements Database.Batchable<SObject> {
    public String queryString;
    public Date referenceDate;

    public InPlaceExportCleanupBatch(Integer referenceDateOffset) {
        referenceDate = Date.today().addDays(referenceDateOffset);
        queryString = 'SELECT Id FROM InPlace__c WHERE CreatedDate < :referenceDate';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 12.NOV.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 12.NOV.2015
     */
    public void execute(Database.BatchableContext BC, List<InPlace__c> records) {
        delete records;
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 12.NOV.2015
     */
    public void finish(Database.BatchableContext BC) {
    }
}