@isTest(seeAllData=true)
private class EnrolmentStatusChecker_CC_Test {

    static testMethod void myUnitTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Enrolment_Intake_Unit__c enrolmentIntakeUnit = new Enrolment_Intake_Unit__c();
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Class_Enrolment__c classEnrolment = new Class_Enrolment__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        List<Class_Enrolment__c> ceList = new List<Class_Enrolment__c>();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Results__c resNat = new Results__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                cattend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Type_of_Attendance__c = cattend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                insert enrl;
                res = TestCoverageUtilityClass.createResult(state.Id);
                resNat = TestCoverageUtilityClass.createResultWithNational(state.Id, res.Id);
                eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                eu.Unit_Result__c = resNat.id;
                eu.Start_Date__c = Date.TODAY() - 25;
                eu.End_Date__c = Date.TODAY() - 1;
                update eu;
                List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c where Enrolment__c =: enrl.Id];
                fundStream = TestCoverageUtilityClass.createFundingStream();
                program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
                intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
                intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
                attend = TestCoverageUtilityClass.createClassAttendanceType();
                for(Enrolment_Unit__c e : euList){
                    enrolmentIntakeUnit = new Enrolment_Intake_Unit__c();
                    enrolmentIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, e.Id);
                    eiuList.add(enrolmentIntakeUnit);
                }
                insert eiuList;
                classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
                staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id); 
            
                Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
                clas.Intake_Unit__c = intakeUnit.Id;
                update clas;
                System.debug('@@clas '+clas);
                for(Enrolment_Intake_Unit__c eiu : eiuList){
                    classEnrolment = new Class_Enrolment__c();
                    classEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, eiu.Id, attend.Id);
                    ceList.add(classEnrolment); 
                }
                insert ceList;
                
                //enrl.Enrolment_Status__c = 'Withdrawn';
                enrl.End_Date__c = Date.TODAY();
                update enrl;
                
                PageReference testPage = Page.EnrolmentStatusChecker;
                Test.setCurrentPage(testPage);
                ApexPages.StandardController controller = new ApexPages.StandardController(enrl);
                EnrolmentStatusChecker_CC esc = new EnrolmentStatusChecker_CC(controller);
                esc.check();
                test.stopTest();
        }
    }
}