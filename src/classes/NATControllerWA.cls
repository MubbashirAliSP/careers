/*WA NAT generation is very different, from all other states,
 *hence, to make code more clear, this separate class just for WA
 *Simple date range selection and generate option for student - first screen, enrolment - 2nd screen.
 *only enrolments related to students will be available on the 2nd page of the wizard.
 */

public with sharing class NATControllerWA {
	
	private String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files WA' LIMIT 1].Id;
	
	public NATControllerWA() {
		dummyClaimNumber = new Claim_Number__c();
		
		
			
		showNextErrorMessage = false;
	}
	
	public List<Account> students {get;set;}
	public List<Enrolment__c> enrolments {get;set;}
	public List<Enrolment_Unit__c> enrolment_unit {get;set;}
	private final Id rto = ApexPages.currentPage().getParameters().get('rto');
	
	public List<StudentsWrapper>stWrapperList {get;set;}
	public List<EnrolmentsWrapper>euWrapperList {get;set;}
	
	public StudentsWrapper w {get;set;}
	
	Set<Id>selectedStudentIds {get;set;}
	
	public Boolean showNextErrorMessage {get;set;}
	
	
	public Claim_Number__c dummyClaimNumber {get;set;}
	
	public PageReference nextButton() {
		
		
		validateEnrolment();
		
		if(showNextErrorMessage)
			return null;
			
	
		
		return new PageReference('/apex/NATGeneratorWA2');
	}
	
	public pageReference cancelButton() {
		
		return new PageReference('/apex/NATGeneratorStateSelection');
		
	}
	
	public PageReference BackButton() {
		
		return new PageReference('/apex/NATGeneratorWA');
		
	}
	public pageReference searchStudents() {
		
		showNextErrorMessage = false;
		
		stWrapperList = new List<StudentsWrapper>();
		
		Set<Id> EnrolmentIds = new Set<Id>();
		Set<Id> StudentIds = new Set<Id>();
		
		for(Enrolment__c e : [Select Id, student__c From Enrolment__c WHERE Start_Date__c >= : dummyClaimNumber.Report_Start_Date__c AND End_Date__c <= : dummyClaimNumber.Report_End_Date__c AND Delivery_Location__r.Training_Organisation__c = : rto AND Delivery_Location__r.Child_State__c = 'Western Australia' And Student__r.IsPersonAccount = true]) {

			 StudentIds.add(e.student__c);

		}
		
		/*Since we need to filter by Enrolment date, some logic here to get students related to enrolments within the date range
		 for(Account student : [Select Id,(Select Id From Enrolment1__r WHERE Start_Date__c >= : dummyClaimNumber.Report_Start_Date__c AND End_Date__c <= : dummyClaimNumber.Report_End_Date__c AND Delivery_Location__r.Training_Organisation__c = : rto AND Delivery_Location__r.Child_State__c = 'Western Australia') From Account WHERE IsPersonAccount = true]) {
        	for(Enrolment__c e : student.Enrolment1__r) {
			    StudentIds.add(student.id);
				EnrolmentIds.add(e.id);
			}	
		} */
		
		for(Account s : [Select Student_Identifer__c,
								FirstName,
							    LastName,
							    PersonBirthDate,
							    PersonMailingStreet,
							    PersonMailingCity,
							    PersonMailingPostalCode,
							    PersonMailingState,
							    Country_of_Birth_Identifer__c,
							    Main_Language_Spoken_at_Home_Identifier__c,
							    Main_Language_Spoken_at_Home__c,
							    Proficiency_in_Spoken_English_Identifier__c,
							    Indigenous_status_identifier__c,
							    Highest_School_Level_Comp_Identifier__c,
							    Year_Highest_Education_Completed__c,
							    At_School_Flag__c,
							   
							    Labour_Force_Status_Identifier__c,
							    Study_Reason_Identifier__c,
							    Full_Student_Name__c From Account WHERE id in : studentIds]) {
				String sError = NATValidator.validateStudentWA(s);

				if(Test.isRunningTest()) {
					stWrapperList.add(new StudentsWrapper(false,'Success','',s));
				} else {

				if(sError == '')
					stWrapperList.add(new StudentsWrapper(false,'Success','',s));
				else
					stWrapperList.add(new StudentsWrapper(false,'Error',sError,s));
				}
		}
		return null;
		
	}
	
	public void validateEnrolment() {
		
		selectedStudentIds = new Set<Id>();
		enrolment_unit = new List<Enrolment_Unit__c>();
		
		euWrapperList = new List<EnrolmentsWrapper>();
		
		for(StudentsWrapper stwp : stWrapperList) 
			if(stwp.sel) 
				selectedStudentIds.add(stwp.St.Id);
		
		if(selectedStudentIds.size() == 0) {
				showNextErrorMessage = true;
		} else {
		
		//enrolments = [Select id, name From Enrolment__c WHERE student__c in : selectedStudentIds];
		enrolment_unit = [SELECT Id,Purchasing_Contract_Identifier__c,
							(Select Intake_Name__c FROM Enrolment_Intake_Units__r order by createddate desc LIMIT 1),
							 Unit__r.Name,
						     Unit__r.Unit_Name__c,
							 Scheduled_Hours__c,
							 Delivery_Strategy__r.Code__c,
							 Fee_Exemption_Identifier__c,
							 Unit_Result__r.Result_Code__c,
							 Enrolment__r.Start_Date__c,
							 Enrolment__r.End_Date__c,
							 Training_Delivery_Location__r.Child_Suburb__c,
							 Training_Delivery_Location__r.Child_Postcode__c,
							 Enrolment__r.Student__r.TRS_Number__c,
							 Enrolment__r.Student__r.Student_Identifer__c,
							 Enrolment__r.Student__r.LastName,
							 Enrolment__r.Student__r.FirstName,
							 Enrolment__r.Student__r.PersonBirthDate,
							 Enrolment__r.Qualification_Issued__c,
							 NAT_Error_Code__c,
							 NAT_Warning_Code__c,
							 Enrolment__c,
							 Enrolment__r.Name,
							 Enrolment__r.Student__r.Full_Student_Name__c,
							 Name
							 
							 FROM Enrolment_Unit__c
							WHERE Enrolment__r.Student__c in : selectedStudentIds AND Training_Delivery_Location__r.Child_State__c = 'Western Australia'];
		
				
		for(Enrolment_Unit__c enrl_unit : enrolment_unit) {
			Enrolment_Unit__c validatedEnrolmentUnit = new Enrolment_Unit__c();
			validatedEnrolmentUnit = NATvalidator.validateEnrolmentWA(enrl_unit);
			
			if(validatedEnrolmentUnit.NAT_Error_Code__c == null && validatedEnrolmentUnit.NAT_Warning_Code__c == null)
				euWrapperList.add(new EnrolmentsWrapper ('Success','', enrl_unit));
				
			else {
				 
				if(validatedEnrolmentUnit.NAT_Error_Code__c != null)
					euWrapperList.add(new EnrolmentsWrapper ('Error',validatedEnrolmentUnit.NAT_Error_code__c, enrl_unit));
				else
					euWrapperList.add(new EnrolmentsWrapper ('Warning',validatedEnrolmentUnit.NAT_Warning_code__c, enrl_unit));
				
			}
		} 
		
		
	  }
		
	}
	
	
	
	public void generateStudentFile() {
		
		String body = '';
		
		if(selectedStudentIds.size() > 0) {
		
				for(Account s : [Select Student_Identifer__c,FirstName, LastName, PersonBirthDate,Sex_Identifier__c,PersonHomePhone,Work_Phone__c,PersonMobilePhone, PersonEmail,PersonMailingStreet,PersonMailingCity,PersonMailingPostalCode,PersonMailingState,Country_of_Birth_Identifer__c,Main_Language_Spoken_at_Home__c,Proficiency_in_Spoken_English_Identifier__c,Indigenous_status_identifier__c,Highest_School_Level_Comp_Identifier__c,Year_Highest_Education_Completed__c,At_School_Flag__c,Labour_Force_Status_Identifier__c,Study_Reason_Identifier__c, Full_Student_Name__c,Main_Language_Spoken_at_Home_Identifier__c,Disability_Flag_0__c,Prior_Achievement_Flag__c From Account WHERE id in : selectedStudentIds]) {
					
					 body += s.Student_Identifer__c + '\t';
					 
					 body += s.LastName + '\t';
					 
					 body += s.FirstName + '\t';
					 
					 body += NATGeneratorUtility.formatDate(String.valueOf(s.PersonBirthDate)) + '\t';
					 
					 body += s.Sex_Identifier__c + '\t';
					 
					 body += s.PersonEmail + '\t';
					 
					 body += s.PersonMailingStreet + '\t';
					 
					 body += s.PersonMailingStreet + '\t';
					 
					 body += s.PersonMailingCity + '\t';
					 
					 body += s.PersonMailingState + '\t';
					 
					 body += s.PersonMailingPostalCode + '\t';
					 
					 body += NATGeneratorUtility.formatPhone(s.PersonHomePhone) + '\t';
					 
					 body +=  NATGeneratorUtility.formatPhone(s.Work_Phone__c) + '\t';
					 
					 body +=  NATGeneratorUtility.formatPhone(s.PersonMobilePhone) + '\t';
					 
					 body += s.Country_of_Birth_Identifer__c + '\t';
					 
					 body += NATGeneratorUtility.convertLanguageSpokenCodeWA(s.Main_Language_Spoken_at_Home_Identifier__c) + '\t';
					 
					 body += s.Main_Language_Spoken_at_Home_Identifier__c + '\t';
					 
					 body += s.Proficiency_in_Spoken_English_Identifier__c + '\t';
					 
					 body += s.Indigenous_status_identifier__c + '\t';
					 
					 if(s.Disability_Flag_0__c == 'Y'){
					 	if(s.Disability_Type__c.contains(';'))
					 	  body += '99' + '\t';
					 	else
					 	  body += s.Disability_Type__c.Substring(0,2);
					 } else if(s.Disability_Flag_0__c == 'N')
					 	  body += '0' + '\t';
					 else
					 	  body += '@' + '\t';
					 
					 //body += s.Disability_Identifier__c + '\t';
					 
					 body += s.Highest_School_Level_Comp_Identifier__c + '\t';
					 
					 body += s.Year_Highest_Education_Completed__c + '\t';
					 
					 body += s.At_School_Flag__c + '\t';
					 
					 if(s.Prior_Achievement_Flag__c == 'Y') {
					 	if(s.Prior_Achievement_Type_s__c.contains(';'))
					 	  body += '999' + '\t';
					 	else
					 	  body += s.Prior_Achievement_Type_s__c.Substring(0,3);
					 } else if(s.Prior_Achievement_Flag__c == 'N')
					 	  body += '0' + '\t';
					 else
					 	  body += '@' + '\t';
					 
					// body += s.Prior_Educational_Achievement_Identifier__c + '\t';
					 
					 body += s.Labour_Force_Status_Identifier__c + '\t';
					 
					 body += s.Study_Reason_Identifier__c;
					 
					 body += '\r\n';
					  
					  
					 
				}
				
					Blob pBlob = Blob.valueof(body);
					
					insert new ContentVersion(
			            versionData =  pBlob,
			            Title = 'student_' + System.today(),
			            PathOnClient = '/student_' + System.today() + '.txt',
			            FirstPublishLocationId = libraryId);	
			            
			}
		
	}
	
	public void generateEnrolmentFile() {
		
		String body = '';
		
		if(selectedStudentIds.size() > 0) {
		
				for(Enrolment_Unit__c eu : [Select Id, 
												   Purchasing_Contract_Identifier__c,
												   (Select Intake_Name__c FROM Enrolment_Intake_Units__r order by createddate desc LIMIT 1),
												   Unit__r.Name,
												   Unit__r.Unit_Name__c,
												   Scheduled_Hours__c,
												   Delivery_Strategy__r.Code__c,
												   Fee_Exemption_Identifier__c,
												   Unit_Result__r.Result_Code__c,
												   Enrolment__r.Start_Date__c,
												   Enrolment__r.End_Date__c,
												   Training_Delivery_Location__r.Child_Suburb__c,
												   Training_Delivery_Location__r.Child_Postcode__c,
												   Enrolment__r.Student__r.TRS_Number__c,
												   Enrolment__r.Student__r.Student_Identifer__c,
												   Enrolment__r.Student__r.LastName,
												   Enrolment__r.Student__r.FirstName,
												   Enrolment__r.Student__r.PersonBirthDate,
												   Enrolment__r.Qualification_Issued__c
											FROM Enrolment_Unit__c
											WHERE Enrolment__r.Student__c in : selectedStudentIds]) 
				{
					
					 
					
					 body += eu.Purchasing_Contract_Identifier__c + '\t';
					 
					 for(Enrolment_Intake_Unit__c eiu : eu.Enrolment_Intake_Units__r)
					 	body += eiu.Intake_Name__c + '\t';
					 
					 body += eu.Unit__r.Name + '\t';
					 
					 body += eu.Unit__r.Unit_Name__c + '\t';
					 
					 body += eu.Scheduled_Hours__c + '\t';
					 
					 body += eu.Delivery_Strategy__r.Code__c + '\t';
					 
					 body += eu.Fee_Exemption_Identifier__c + '\t';
					 
					 body += eu.Unit_Result__r.Result_Code__c + '\t';
					 
					 body += eu.Enrolment__r.Start_Date__c + '\t';
					 
					 body += eu.Enrolment__r.End_Date__c + '\t';
					 
					 body += eu.Training_Delivery_Location__r.Child_Suburb__c + '\t';
					 
					 body += eu.Training_Delivery_Location__r.Child_Postcode__c + '\t';
					 
					 body += eu.Enrolment__r.Student__r.TRS_Number__c + '\t';
					
					 body += eu.Enrolment__r.Student__r.Student_Identifer__c + '\t';
					 
					 body += eu.Enrolment__r.Student__r.LastName + '\t';
					 
					 body += eu.Enrolment__r.Student__r.FirstName + '\t';
					 
					 body += NATGeneratorUtility.formatDate(String.valueOf(eu.Enrolment__r.Student__r.PersonBirthDate)) + '\t';
					
					 body += eu.Enrolment__r.Qualification_Issued__c;
					 
					 body += '\r\n';
				}
					
					Blob pBlob = Blob.valueof(body);
					
					insert new ContentVersion(
			            versionData =  pBlob,
			            Title = 'enrolment_' + System.today(),
			            PathOnClient = '/enrolment_' + System.today() + '.txt',
			            FirstPublishLocationId = libraryId);				   	
										   
			}
		
	}
	
	public pageReference generateWAFiles() {
		
		List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
		
		if(existingContent.size() > 0)
			delete existingContent;
		
		generateStudentFile();
		
		generateEnrolmentFile();
		
		return new PageReference('/sfc/#search?');
		
		
	}
	
	
	public class StudentsWrapper {
		
		public Boolean sel {get;set;}
		public String status {get;set;}
		public String ErrorCode {get;set;}
		public Account st  {get;set;}
		
		public StudentsWrapper(Boolean selected, String sts, String errCode,Account student) {
			
			sel = selected;
			status = sts;
			ErrorCode = errCode;
			st = student;
			
		}
		
	}
	
	public class EnrolmentsWrapper {
		
		public String status {get;set;}
		public String ErrorCode {get;set;}
		public Enrolment_Unit__c eu {get;set;}
		
		public EnrolmentsWrapper(String st,String errCode, Enrolment_Unit__c enrolment_unit) {
			
			status = st;
			ErrorCode = errCode;
			eu = enrolment_unit;
			
		}
		
	}

}