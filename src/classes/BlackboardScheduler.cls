/*This class kicks off all blackboard jobs
 *Please view the sequence of jobs for the reference
 *Create By Yuri Gribanov - CloudSherpas 4/09/2014
 *
 *1) BlackboardInitializeAndLoginJ0 Initialize and Login
 *
 *2) BlackboardIntakeUnitLoadingJ1   Create T3 BBC Records
 *
 *3) BlackboardCourseSISConfirmSyncBatch  Second GetPK call for all SIS from yesterday
 *
 *4) BlackboardCourseSISSyncBatch  SIS sync for all new or reset eligible T3 BBCs
 *
 *5) BlackboardCourseSyncBatch     Sync T2, T4 and T5 + first GetPK call for all SIS just completed
 *
 *6) BlackboardScheduleUnitJ2A     Create T3 Student BBCMs + New Student Users & User Roles
 *
 *7) BlackboardIntakeStudentJ2B    Create T4 and T5 Student BBCMs + New Student Users & User Roles
 *
 *8) BlackboardIntakeUnitUserRoleJ2C   Create Staff (Trainer/SSO) BBCMs
 *
 *9) BlackboardIntakeStudentJ9     Assign Institutional Roles to Student Users
 *
 *10) BlackboardUserSyncBatch   Sync all BBUs
 *
 *11) Sync Course Memberships  Sync all Student BBCMs
 *
 *12) Sync Staff Course Memberships    Sync all Staff BBCMs
 */

global class BlackboardScheduler implements Schedulable {

   global void execute(SchedulableContext SC) {  
      
      
       BlackboardUtility.createBlackBoardLog(null); // insert new log record
       
       
   
       BlackboardInitializeAndLoginJ0 b1 = new BlackboardInitializeAndLoginJ0();
       if(!Test.IsRunningTest())
           database.executebatch(b1,1);
   }
   
}