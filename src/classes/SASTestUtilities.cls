/**
 * @description Test class utilities used by SAS team
 * @author Ranyel Maliwanag
 * @date 02.JUL.2015
 * @history
 *  12.OCT.2015  Nick Guia   Created: createEnrolmentUnit, createNationalResult,
 *                           createLineItemQualificationUnit, createPurchasingContract
 */
@isTest
public without sharing class SASTestUtilities {
    public static CentralTriggerSettings__c createCentralTriggerSettings() {
        CentralTriggerSettings__c result = new CentralTriggerSettings__c(
            );
        return result;
    }


    public static ApplicationsPortalCentralSettings__c createApplicationsPortalSettings() {
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        return portalSettings;
    }


    public static User createUser() {
        User userRecord = new User(
                Alias = 'tsu',
                LastName = 'Olympia',
                TimeZoneSidKey = 'Australia/Brisbane',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
        return userRecord;
    }


    public static Account createStudentAccount() {
        Account student = new Account(
                FirstName = 'Godric',
                LastName = 'Gryffindor',
                Student_Identifer__c = 'CX1230',
                PersonBirthdate = Date.newInstance(2015, 5, 19),
                PersonEmail = 'person@casis.example.com',
                Middle_Name__c = 'Taylor',
                Phone = '548242',
                PersonHomePhone = '548242',
                PersonMobilePhone = '745635',
                Salutation = 'Mr',
                Suburb_locality_or_town__c = 'Fortitude Valley',
                Emergency_Contact_Name__c = 'Albus Dumbledore',
                Relationship_to_Emergency_Contact__c = 'Headmaster',
                Emergency_Contact_Phone_Number__c = '789126',
                Emergency_Contact_Mobile_Number__c = '49785126'
            );
        return student;
    }

    /**
     * @description Used for bulk creation user
     * @author Juan Miguel de Guia
     * @param   Integer ctr - number of accounts to be created
     * @date 24.SEP.2015
     **/

    public static List<User> bulkCreateUser(Integer ctr){
        List<User> userList = new List<User>();
        for(Integer i = 1; i < ctr; i++) {
            User student = new User(
                FirstName = 'Test' + i,
                LastName = 'McTest' + i,
                Alias = 'tmct' + i,
                Email = 'tmct@test.com' + i,
                Username = 'tmct@test.com' + i,
                CommunityNickname = 'tmct' + i,
                UserRoleId = '00E90000000RMKbEAO',
                ProfileId = '00e90000000rnWHAAY',
                User_License_Type__c = 'Salesforce',
                EmailEncodingKey = 'ISO-8859-1',
                IsActive = true
            );
            userList.add(student);
        }
        return userList;
    }

    /**
     * @description Used for bulk creation class
     * @author Juan Miguel de Guia
     * @param   Integer ctr - number of accounts to be created, Id IntakeId - the Intake Id needed for the class
     * @date 24.SEP.2015
     **/

    public static List<Classes__c> bulkCreateClasses(Integer ctr, Id IntakeId){
        List<Classes__c> classList = new List<Classes__c>();
        for(Integer i = 1; i<ctr;i++){
            Classes__c c = new Classes__c(
                Intake__c = IntakeId,
                Alternate_Class_Delivery_Option__c = 'Tutorial and Resits'
                );
            classList.add(c);
        }
        return classList;
    }

    /**
     * @description Used for bulk creation staff member
     * @author Juan Miguel de Guia
     * @param   Integer ctr - number of accounts to be created, Id staffId - the UserId for the Staff Member, Id intakeId - the intakeId for the intake
     * @date 24.SEP.2015
     **/

     public static List<Staff_Member__c> bulkCreateStaffMember(Integer ctr, Id staffId, Id intakeId){
        List<Staff_Member__c> listStaffMember = new List<Staff_Member__c>();
        for(Integer i = 1; i < ctr; i++){
            Staff_Member__c staffMember = new Staff_Member__c(
                Trainer__c = staffId,
                Intake__c = intakeId,
                Add_staff_member_to_all_classes__c = true
                );
            listStaffMember.add(staffMember);
        }
        return listStaffMember;
     }


    /**
     * @description Used for bulk creation of student account
     * @author Nick Guia
     * @param   Integer ctr - number of accounts to be created
     * @date 10.AUG.2015
     */
    public static List<Account> bulkCreateStudentAccount(Integer ctr) {
        Id studRecType = [select id from RecordType where Name='Student' And SObjectType = 'Account' LIMIT 1].Id; 
        List<Account> accList = new List<Account>();
        for(Integer i = 1; i < ctr; i++) {
            Account student = new Account(
                FirstName = 'Godric' + i,
                LastName = 'Gryffindor' + i,
                Student_Identifer__c = 'CX1230' + i,
                PersonBirthdate = Date.newInstance(2015, 5, 19),
                PersonEmail = 'person' + i + '@casis.com',
                Middle_Name__c = 'Taylor' + i,
                Phone = '548242',
                PersonHomePhone = '548242',
                PersonMobilePhone = '745635',
                Salutation = 'Mr',
                Suburb_locality_or_town__c = 'Fortitude Valley',
                Emergency_Contact_Name__c = 'Albus Dumbledore',
                Relationship_to_Emergency_Contact__c = 'Headmaster',
                Emergency_Contact_Phone_Number__c = '789126',
                Emergency_Contact_Mobile_Number__c = '49785126',
                RecordTypeId = studRecType
            );
            accList.add(student);
        }
        
        return accList;
    }

    public static Enrolment__c createEnrolment(Account student) {
        Enrolment__c enrolment = new Enrolment__c(
                Student__c = student.Id,
                Enrolment_Status__c = 'Active (Commencement)',
                Mailing_Country__c = 'Australia',
                Mailing_Zip_Postal_Code__c = '4006',
                Name_of_suburb_town_locality__c = 'Fortitude Valley',
                Mailing_State_Provience__c = 'Queensland',
                Address_street_number__c = '100',
                Address_street_name__c = 'Brookes Street',
                Address_flat_unit_details__c = 'Ground Floor'
            );
        return enrolment;
    }


    public static Qualification__c createParentQualification() {
        Id parentRTID = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Qualification').getRecordTypeId();
        Qualification__c qualification = new Qualification__c(
                Name = 'Parent Qualification',
                Qualification_Name__c = 'Parent Qualification',
                RecordTypeId = parentRTID
            );
        return qualification;
    }


    public static Qualification__c createTailoredQualification(Qualification__c parentQualification) {
        Id tailoredRTID = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Tailored Qualification').getRecordTypeId();
        Qualification__c qualification = new Qualification__c(
                Name = 'Tailored Qualification',
                Qualification_Name__c = 'Tailored Qualification',
                Award_based_on__c = parentQualification.Id,
                RecordTypeId = tailoredRTID
            );
        return qualification;
    }


    public static Qualification__c createQualification(Id anzId, Id fldId){
        Qualification__c qual = new Qualification__c();
        qual.Name = 'CS Test Qualification';
        qual.Qualification_Name__c = 'CS Test Qualification Name';
        qual.ANZSCO__c = anzId;
        qual.Field_of_Education__c = fldId;
        qual.Recognition_Status__c = '14 - Other Courses';     
        return qual;
    }

    public static ANZSCO__c createANZSCO(){
        ANZSCO__c anz = new ANZSCO__c();
        anz.Name = 'CS Test ANZSCO';
        return anz;
    }

    public static Field_of_Education__c createFieldOfEducation(){
        Field_of_Education__c fld = new Field_of_Education__c();
        Id recrdType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        fld.RecordTypeId = recrdType;
        fld.Name = 'CS Test Field of Education';
        return fld;
    }

    public static Country__c createCountry(){
        Country__c c = new Country__c();
        c.Country_Code__c = '1123';
        c.Name = 'CS Country Name';
        return c;
    }

    public static State__c createState(Id country){
        State__c newState = new State__c();
        newState.Name = 'Test State';
        newState.Country__c = country;
        newState.Short_Name__c = 'VIC';
        newState.Code__c = '02';
        return newState;
    }

    public static Account createRTO() {
        Country__c country = createCountry();
        insert country;

        State__c state = createState(country.Id);
        insert state;

        Account record = new Account(
                RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Training Organisation').getRecordTypeId(),
                Name = 'Careers Australia Institute Of Training'
            );
        return record;
    }


    public static Location_Loadings__c createLocationLoadings(Id state){
        Location_Loadings__c l = new Location_Loadings__c();
        l.Name = 'loading name';
        l.State__c = state;
        l.Price_Factor__c = 50;
        return l;
    }

    public static Locations__c createLocation(Id countryId, Id pl){
        Id recrdtype = [SELECT Id FROM RecordType WHERE DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.RecordTypeId = recrdtype;
        l.Name = 'CS Sample Room';
        l.Country__c = countryId;
        l.Parent_Location__c = pl;
        return l;
    }

    public static Locations__c createParentLocation(Id country, Id state, Id loading){
        Id recrdtype = [SELECT Id FROM RecordType WHERE DeveloperName='Parent_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.Name = 'CS name';
        l.Country__c = country;
        l.State_Lookup__c = state;
        l.Location_Loading__c = loading;
        l.RecordTypeId = recrdtype;
        return l;
    }

    public static Enrolment_Letters__c createEnrolmentLetters(Id enrolmentId, Id recTypeId) {
        Enrolment_Letters__c el = new Enrolment_Letters__c();
        el.Enrolment__c = enrolmentId;
        el.RecordTypeId = recTypeId;
        return el;
    }

    public static Program__c createProgram(Id qual, Id loc){
        Program__c p = new Program__c();
        p.Program_Start_Date__c = date.today();
        p.Program_End_Date__c = date.today()+1;
        p.Qualification__c = qual;
        p.Training_Organisation_Delivery_Location__c = loc;
        return p;
    }

    public static Funding_Stream_Types__c createFundingStream(){
        Funding_Stream_Types__c f = new Funding_Stream_Types__c();
        f.Name = 'Funding Stream';
        return f;
    }

    public static VFH_Controller__c createVFHController(){
        VFH_Controller__c v = new VFH_Controller__c();
        v.Report_End_Date__c = date.today() + 50;
        v.Report_Start_Date__c = date.today();
        return v;
    }

    public static Intake__c createIntake(Id qualId, Id program, Id location, Id fundingStream){
        VFH_Controller__c vfh = createVFHController();
        insert vfh;
        Intake__c i = new Intake__c();
        i.Start_Date__c = date.today();
        i.End_Date__c = date.today() + 30;
        i.Qualification__c = qualId;
        i.VFH_Controller__c = vfh.Id;
        i.Program__c = program;
        i.Training_Delivery_Location__c = location;
        i.Funding_Stream__c = fundingStream;
        return i;
    }

    public static Intake_Students__c createIntakeStudent(Id intakeId, Id enrolmentId, Id studentId){
        Intake_Students__c is = new Intake_Students__c();
        is.Intake__c = intakeId;
        is.Enrolment__c = enrolmentId;
        is.Student__c = studentId;
        return is;
    }

    public static Enrolment__c createEnrolment(Qualification__c qual) {
        Account student = createStudentAccount();
        insert student;

        Enrolment__c enrolment = createEnrolment(student);
        return enrolment;
    }

    public static Intake__c createIntake(Qualification__c qual) {
        Country__c country = createCountry();
        insert country;

        State__c state = createState(country.Id);
        insert state;

        Location_Loadings__c load = createLocationLoadings(state.Id);
        insert load;

        Locations__c pl = createParentLocation(country.Id, state.Id, load.Id);
        insert pl;

        Locations__c loc = createLocation(country.Id, pl.Id);
        insert loc;

        Program__c program = createProgram(qual.Id, loc.Id);
        insert program;

        Funding_Stream_Types__c fundingStream = createFundingStream();
        insert fundingStream;

        // createIntake(Id qualId, Id program, Id location, Id fundingStream)
        Intake__c intake = createIntake(qual.Id, program.Id, loc.Id, fundingStream.Id);
        return intake;
    }

    public static Census__c createCensus() {
        Account student = createStudentAccount();
        insert student;

        Enrolment__c enrolment = createEnrolment(student);
        enrolment.RecordTypeId = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get('Dual Funding Enrolment').getRecordTypeId();
        insert enrolment;

        Qualification__c qual = createQualification(null, null);
        insert qual;

        Country__c country = createCountry();
        insert country;

        State__c state = createState(country.Id);
        insert state;

        Location_Loadings__c load = createLocationLoadings(state.Id);
        insert load;

        Locations__c pl = createParentLocation(country.Id, state.Id, load.Id);
        insert pl;

        Locations__c loc = createLocation(country.Id, pl.Id);
        insert loc;

        Program__c program = createProgram(qual.Id, loc.Id);
        insert program;

        Funding_Stream_Types__c fundingStream = createFundingStream();
        insert fundingStream;

        // createIntake(Id qualId, Id program, Id location, Id fundingStream)
        Intake__c intake = createIntake(qual.Id, program.Id, loc.Id, fundingStream.Id);
        insert intake;

        Census__c result = new Census__c(
                EnrolmentId__c = enrolment.Id,
                IntakeId__c = intake.Id,
                CensusDate__c = Date.today().addDays(-100),
                IsManualProgression__c = true
            );
        return result;
    }

    public static List<Census__c> createMultipleCensus() {
        Account student = createStudentAccount();
        student.Student_Identifer__c = 'CX1231';
        insert student;

        List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
        for (Integer i = 0; i < 1000; i++) {
            enrolmentList.add(createEnrolment(student));
        }
        insert enrolmentList;

        Intake__c intake = [SELECT Id FROM Intake__c];

        List<Census__c> censusList = new List<Census__c>();
        for (Enrolment__c enrolment : enrolmentList) {
            Census__c census = new Census__c(
                    EnrolmentId__c = enrolment.Id,
                    IntakeId__c = intake.Id,
                    CensusDate__c = Date.today().addDays(-100),
                    IsManualProgression__c = true
                );
            censusList.add(census);
        }
        return censusList;
    }


    /**
     * @description For creating test Enrolment Unit
     * @author Nick Guia
     * @date 12.OCT.2015
     */
    public static Enrolment_Unit__c createEnrolmentUnit(Unit__c unit, Enrolment__c enrol, Results__c res, Locations__c loc, Line_Item_Qualification_Units__c liqu){
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        eu.Unit__c = unit.Id;
        eu.Enrolment__c = enrol.Id;
        eu.Start_Date__c = date.today() + 5;
        eu.End_Date__c = date.today() + 10;
        eu.Unit_Result__c = res.Id;
        eu.Training_Delivery_Location__c = loc.Id;
        eu.Line_Item_Qualification_Unit__c = liqu.Id;                
        return eu;
    }

    /**
     * @description For creating test National Result
     * @author Nick Guia
     * @date 12.OCT.2015
     */
    public static Results__c createNationalResult(){
        Id recType = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c newRes = new Results__c();
        newRes.RecordTypeId = recType;
        newRes.Name = 'test results';
        newRes.Result_Code__c = '20'; 

        return newRes;
    }

    /**
     * @description For creating test Line Item Qualification Unit
     * @author Nick Guia
     * @date 12.OCT.2015
     */
    public static Line_Item_Qualification_Units__c createLineItemQualificationUnit(Line_Item_Qualifications__c lineItem, Qualification_Unit__c qualUnit){
        Line_Item_Qualification_Units__c  liqu = new Line_Item_Qualification_Units__c();
        liqu.Qualification_Unit__c = qualUnit.Id;
        liqu.Line_Item_Qualification__c = lineItem.Id;
        return liqu;
    }

    /**
     * @description For creating test Purchasing Contracts
     * @author Nick Guia
     * @date 12.OCT.2015
     */
    public static Purchasing_Contracts__c createPurchasingContract(Funding_Source__c fSource){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = fSource.Id;
        p.Contract_Code__c = '1234567890';
        return p;
    }


    public static Service_Cases__c createServiceCase() {
        Service_Cases__c result = new Service_Cases__c();
        return result;
    }


    public static Study_Reason_Type__c createStudyReasonType() {
        Study_Reason_Type__c result = new Study_Reason_Type__c(
                Code__c = '01',
                Name = 'To get a job'
            );
        return result;
    }
}