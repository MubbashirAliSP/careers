/**
 * @description Test class for AccountTriggerHandler
 * @author Ranyel Maliwanag
 * @date 02.JUL.2015
 */
@isTest
public with sharing class AccountTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {
        Language__c atsiEnglish = new Language__c(
                Code__c = '0001',
                Name = 'English'
            );
        insert atsiEnglish;

        Language__c nonAtsiEnglish = new Language__c(
                Code__c = '1201',
                Name = 'English'
            );
        insert nonAtsiEnglish;

        CentralTriggerSettings__c triggerSetings = new CentralTriggerSettings__c(
                Account__c = true,
                Account_DetermineEnglishCode__c = true
            );
        insert triggerSetings;
    }

    
    static testMethod void testAtsiEnglish() {
        Language__c atsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '0001'];
        Language__c nonAtsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '1201'];

        Test.startTest();
            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr',
                    Indigenous_Status__c = '1 - Yes, Aboriginal',
                    Main_Language_Spoken_at_Home__c = atsiEnglish.Id
                );
            insert student;

            student = [SELECT Id, Main_Language_Spoken_at_Home_Identifier__c FROM Account WHERE Id = :student.Id];
            System.assert(student.Main_Language_Spoken_at_Home_Identifier__c == '0001');
        Test.stopTest();
    }


    static testMethod void testAtsiEnglishNegative() {
        Language__c atsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '0001'];
        Language__c nonAtsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '1201'];

        Test.startTest();
            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr',
                    Indigenous_Status__c = '1 - Yes, Aboriginal',
                    Main_Language_Spoken_at_Home__c = nonAtsiEnglish.Id
                );
            insert student;

            student = [SELECT Id, Main_Language_Spoken_at_Home_Identifier__c FROM Account WHERE Id = :student.Id];
            System.assert(student.Main_Language_Spoken_at_Home_Identifier__c == '0001');
        Test.stopTest();
    }


    static testMethod void testNonAtsiEnglish() {
        Language__c atsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '0001'];
        Language__c nonAtsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '1201'];

        Test.startTest();
            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr',
                    Indigenous_Status__c = '4 - No, Neither Aboriginal nor Torres Strait Islander',
                    Main_Language_Spoken_at_Home__c = nonAtsiEnglish.Id
                );
            insert student;

            student = [SELECT Id, Main_Language_Spoken_at_Home_Identifier__c FROM Account WHERE Id = :student.Id];
            System.assert(student.Main_Language_Spoken_at_Home_Identifier__c == '1201');
        Test.stopTest();
    }


    static testMethod void testNonAtsiEnglishNegative() {
        Language__c atsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '0001'];
        Language__c nonAtsiEnglish = [SELECT Id FROM Language__c WHERE Code__c = '1201'];

        Test.startTest();
            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr',
                    Indigenous_Status__c = '4 - No, Neither Aboriginal nor Torres Strait Islander',
                    Main_Language_Spoken_at_Home__c = atsiEnglish.Id
                );
            insert student;

            student = [SELECT Id, Main_Language_Spoken_at_Home_Identifier__c FROM Account WHERE Id = :student.Id];
            System.assert(student.Main_Language_Spoken_at_Home_Identifier__c == '1201');
        Test.stopTest();
    }

    static testMethod void testHighestSchoolCompletedMapping() {
        Account student = new Account(
                FirstName = 'Godric',
                LastName = 'Gryffindor',
                Student_Identifer__c = 'CX1230',
                PersonBirthdate = Date.newInstance(2015, 5, 19),
                PersonEmail = 'person@inplace.com',
                Middle_Name__c = 'Taylor',
                Phone = '548242',
                PersonMobilePhone = '745635',
                Salutation = 'Mr',
                Indigenous_Status__c = '1 - Yes, Aboriginal',
                Highest_School_Level_Completed__c = 'Completed a Year 11 qualification or equivalent'
            );
        insert student;

        student = [SELECT Id, Highest_School_Level_Completed__c FROM Account WHERE Id = :student.Id];
        System.assert(student.Highest_School_Level_Completed__c == '11 - Completed Year 11');
    }


    static testMethod void updateRelatedEmailsTest() {
        Country__c country = SASTestUtilities.createCountry();
        insert country;

        State__c state = SASTestUtilities.createState(country.Id);
        insert state;

        Account student = SASTestUtilities.createStudentAccount();
        insert student;

        GuestLogin__c login = new GuestLogin__c(
                AccountId__c = student.Id,
                Email__c = student.PersonEmail,
                FirstName__c = student.FirstName,
                LastName__c = student.LastName,
                Password__c = 'secretPassword',
                RecordTypeId = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Student').getRecordTypeId()
            );
        insert login;

        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = login.Id,
                StateId__c = state.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert form;

        Service_Cases__c serviceCase = new Service_Cases__c(
                Account_Student_Name__c = student.Id,
                RecordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId()
            );
        insert serviceCase;

        EntryRequirement__c requirement = new EntryRequirement__c(
                RecordTypeId = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId(),
                ServiceCaseId__c = serviceCase.Id,
                ApplicationFormId__c = form.Id,
                Status__c = 'LLN - Pending'
            );
        insert requirement;

        Test.startTest();
            student.PersonEmail = 'newemail@casis.com.au';
            update student;

            login = [SELECT Id, Email__c FROM GuestLogin__c WHERE Id = :login.Id];
            System.assertEquals(student.PersonEmail, login.Email__c);

            form = [SELECT Id, StudentEmail__c FROM ApplicationForm__c WHERE Id = :form.Id];
            System.assertEquals(student.PersonEmail, form.StudentEmail__c);

            serviceCase = [SELECT Id, Student_Email__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals(student.PersonEmail, serviceCase.Student_Email__c);
        Test.stopTest();
    }

    // Negative test
    static testMethod void updateRelatedEmailsTest1() {
        Country__c country = SASTestUtilities.createCountry();
        insert country;

        State__c state = SASTestUtilities.createState(country.Id);
        insert state;

        Account student = SASTestUtilities.createStudentAccount();
        insert student;

        GuestLogin__c login = new GuestLogin__c(
                AccountId__c = student.Id,
                Email__c = student.PersonEmail,
                FirstName__c = student.FirstName,
                LastName__c = student.LastName,
                Password__c = 'secretPassword',
                RecordTypeId = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Student').getRecordTypeId()
            );
        insert login;

        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = login.Id,
                StateId__c = state.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert form;

        Service_Cases__c serviceCase = new Service_Cases__c(
                Account_Student_Name__c = student.Id,
                RecordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId(),
                Status__c = 'Closed'
            );
        insert serviceCase;

        EntryRequirement__c requirement = new EntryRequirement__c(
                RecordTypeId = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId(),
                ServiceCaseId__c = serviceCase.Id,
                ApplicationFormId__c = form.Id,
                Status__c = 'LLN - Confirmed'
            );
        insert requirement;

        Test.startTest();
            student.PersonEmail = 'newemail@casis.com.au';
            update student;

            login = [SELECT Id, Email__c FROM GuestLogin__c WHERE Id = :login.Id];
            System.assertEquals(student.PersonEmail, login.Email__c);

            form = [SELECT Id, StudentEmail__c FROM ApplicationForm__c WHERE Id = :form.Id];
            System.assertEquals(student.PersonEmail, form.StudentEmail__c);

            serviceCase = [SELECT Id, Student_Email__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertNotEquals(student.PersonEmail, serviceCase.Student_Email__c);

            requirement = [SELECT Id, Email__c FROM EntryRequirement__c WHERE Id = :requirement.Id];
            System.assertNotEquals(student.PersonEmail, requirement.Email__c);
        Test.stopTest();
    }
}