/**
 * @description Batch process for assigning leads to appropriate queues depending on various conditions
 * @author Ranyel Maliwanag
 * @date 12.MAY.2015
 * @history
 *       18.MAY.2015    Ranyel Maliwanag    - Added condition 7 for Lead Queue Assignment
 *                                          - Reordered evaluation of conditions based on customer request
 *       01.JUN.2015    Ranyel Maliwanag    - Integrated Holiday support to skip execution during defined holidays
 *       04.JUN.2015    Ranyel Maliwanag    - Added `Status = Completed` condition for querying related tasks to a lead
 *                                          - Added populating of `CAGSource__c` field for leads assigned to Contactability - With Partner Queue
 */
public with sharing class LeadQueueAssignmentBatch implements Database.Batchable<SObject> {
    public String queryString;
    public List<Group> leadQueues;
    public Set<String> leadQueueNames;
    public Map<String, Id> leadQueueIds;
    public String brokerQueue;
    public String eoiQueue;
    public String unqualifiedQueue;


    /**
     * @description Constructor for the batch process
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     * @history
     *       01.MAY.2015    Ranyel Maliwanag    - Integrated Holiday support to skip execution during defined holidays
     *       04.JUN.2015    Ranyel Maliwanag    - Added `Status = Completed` condition on `queryString` initialisation to filter completed tasks as per TeleSales' request
     *                                          - Added populating of `CAGSource__c` field for leads assigned to Contactability - With Partner Queue
     */
    public LeadQueueAssignmentBatch() {
        Date dateToday = Date.today();
        List<Holiday> holidays = [SELECT Id, ActivityDate FROM Holiday WHERE ActivityDate = :dateToday];
        if (holidays.isEmpty()) {
            // Condition: Today is not a holiday
            queryString = 'SELECT Id, OwnerId, CreatedDate, Status, (SELECT Id, CreatedDate, Contact_Type__c, Contact_Outcome__c FROM Tasks WHERE Status = \'Completed\')';
            queryString += 'FROM Lead ';
            queryString += 'WHERE Referral_Source_Type_Detail__c = \'Contactability\' AND IsConverted = false AND Owner.Profile.UserLicense.Name != \'Partner Community\' ';
            
            brokerQueue = 'With Partner';
            eoiQueue = 'Expression of Interest';
            unqualifiedQueue = 'Not Interested/Unqualified';

            leadQueueNames = new Set<String>{unqualifiedQueue, eoiQueue, brokerQueue};
            leadQueues = [SELECT Name, Id FROM Group WHERE Name IN :leadQueueNames];
            leadQueueIds = new Map<String, Id>();
            for (Group queue : leadQueues) {
                leadQueueIds.put(queue.Name, queue.Id);
            }
        } else {
            queryString = 'SELECT Id FROM Lead WHERE Id = null';
        }
    }


    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description Executes the actual logic for the batch process
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     */
    public void execute(Database.BatchableContext BC, List<Lead> leads) {
        // Initialise the buckets for bulkified updates
        List<Lead> leadsForUpdate = new List<Lead>();

        // Iterate over the lead records for this batch
        for (Lead leadRecord : leads) {
            // Evaluate determining conditions for queue assignments
            System.debug('ActivityHistory ::: ' + leadRecord.Tasks.size());
            
            // Analyse Activity History information
            Set<String> unsuccessfulOutcomes = new Set<String>{
                'No Answer',
                'Answering Machine/Voicemail'
            };
            Set<String> unqualifiedStatuses = new Set<String>{
                'Not Interested',
                'Unqualified'
            };
            Set<String> activityHistoryStatuses = new Set<String>{
                'Open',
                'Contacted',
                'Contact Attempted'
            };
            Set<String> unqualifiedTaskHistories = new Set<String>{
                'DNC Registered',
                'Request Opt Out of Promotional Calls',
                'Already enrolled/already contacted'
            };
            Boolean hasActivityHistoryStatus = activityHistoryStatuses.contains(leadRecord.Status);
            Boolean hasUnqualifiedHistory = false;
            Integer allActivityCount = leadRecord.Tasks.size();
            Integer unsuccessfulCount = 0;
            Integer callBackCount = 0;
            Datetime lastActivityDatetime = null;

            for (Task activity : leadRecord.Tasks) {
                if (unsuccessfulOutcomes.contains(activity.Contact_Outcome__c)) {
                    unsuccessfulCount++;
                }
                if (activity.Contact_Outcome__c == 'Call Back') {
                    callBackCount++;
                }
                if (unqualifiedTaskHistories.contains(activity.Contact_Outcome__c)) {
                    hasUnqualifiedHistory = true;
                }
                lastActivityDatetime = activity.CreatedDate;
            }

            // Evaluate conditions based on analyses and leadRecord information
            Boolean forUpdate = false;
            if (unqualifiedStatuses.contains(leadRecord.Status)) {
                // Condition 3
                leadRecord.OwnerId = leadQueueIds.get(unqualifiedQueue);
                forUpdate = true;
            } else if (leadRecord.Status == 'Expression of Interest') {
                // Condition 4
                leadRecord.OwnerId = leadQueueIds.get(eoiQueue);
                forUpdate = true;
            } else if (allActivityCount == 0 && leadRecord.CreatedDate.addHours(72) < Datetime.now() && hasActivityHistoryStatus) {
                // Condition 2
                leadRecord.OwnerId = leadQueueIds.get(brokerQueue);
                leadRecord.CAGSource__c = 'CAG TS';
                forUpdate = true;
            } else if (hasUnqualifiedHistory) {
                // Condition 7
                leadRecord.OwnerId = leadQueueIds.get(unqualifiedQueue);
                forUpdate = true;
            } else if (unsuccessfulCount == 2 && hasActivityHistoryStatus) {
                // Condition 1
                leadRecord.OwnerId = leadQueueIds.get(brokerQueue);
                leadRecord.CAGSource__c = 'CAG TS';
                forUpdate = true;
            } else if (callBackCount == 4 && hasActivityHistoryStatus) {
                // Condition 5
                leadRecord.OwnerId = leadQueueIds.get(brokerQueue);
                leadRecord.CAGSource__c = 'CAG TS';
                forUpdate = true;
            } else if (allActivityCount == 1 && lastActivityDatetime.addHours(72) < Datetime.now() && hasActivityHistoryStatus) {
                // Condition 6
                leadRecord.OwnerId = leadQueueIds.get(brokerQueue);
                leadRecord.CAGSource__c = 'CAG TS';
                forUpdate = true;
            }

            if (forUpdate) {
                System.debug('for update ::: ' + leadRecord);
                leadsForUpdate.add(leadRecord);
            }
        }

        update leadsForUpdate;
    }


    /**
     * @description Executes the finishing logic for the batch process
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     */
    public void finish(Database.BatchableContext BC) {
    }
}