/**
 * @description Test class for `IntakeTriggerHandlerSAS` class
 * @author Ranyel Maliwanag
 * @date 16.MAR.2016
 */
@isTest
public with sharing class IntakeTriggerHandlerSAS_Test {
	@testSetup
	static void setup() {
	}

	static testMethod void adjustRelatedCensusDatesTest() {
		Census__c census = SASTestUtilities.createCensus();
		Intake__c intake = [SELECT Id, Census_Date__c FROM Intake__c];
		census.CensusDate__c = intake.Census_Date__c;
		insert census;

		System.assertNotEquals(Date.today(), intake.Census_Date__c);
		System.assertEquals(intake.Census_Date__c, census.CensusDate__c);

		Test.startTest();
			// Update intake census date
			intake.Adjusted_Census_Date__c = Date.today();
			update intake;

			intake = [SELECT Id, Census_Date__c FROM Intake__c];
			System.assertEquals(Date.today(), intake.Census_Date__c);

			census = [SELECT Id, CensusDate__c FROM Census__c WHERE Id = :census.Id];
			System.assertEquals(intake.Census_Date__c, census.CensusDate__c);
		Test.stopTest();
	}
}