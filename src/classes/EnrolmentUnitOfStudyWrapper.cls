/*
 * @description Wrapper class for enrolment unit of study object. Used for HEIMS generation.
 * @author D. Crisologo (CloudSherpas)
 * @date 19.JUN.2013
*/
global class EnrolmentUnitOfStudyWrapper {
        
    public Boolean selected {get;set;}
    public Enrolment_Unit_of_Study__c EUS {get;set;}
    
    public EnrolmentUnitOfStudyWrapper (Boolean sel, Enrolment_Unit_of_Study__c EnrolUnitOfStudy) {
        selected = sel;
        EUS = EnrolUnitOfStudy;
    }
    
}