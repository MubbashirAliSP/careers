/**
 * @description Test class for `EntryRequirementsCC`
 * @author Ranyel Maliwanag
 * @date 24.FEB.2016
 */
@isTest
public with sharing class EntryRequirementsCC_Test {
    static Id llnERRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
    static Id formSCRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
    static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();
	static Id year12ReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Year 12 Certificate').getRecordTypeId();

	@testSetup
	static void setup() {
		ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.initialiseFormComponents();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();

        Account student = SASTestUtilities.createStudentAccount();
        insert student;

        GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        studentLogin.AccountId__c = student.Id;
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        portalSettings.EffectivityDate__c = Date.today().addDays(1);
        portalSettings.LoanApplicationFormCreationOffset__c = 0;
        insert portalSettings;

        ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;

        EntryRequirement__c req = new EntryRequirement__c(
			RecordTypeId = year12ReqRTID,
			ApplicationFormId__c = appForm.Id,
			Status__c = 'Awaiting Upload'
		);
		insert req;
	}

	static testMethod void controllerTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
		ApplicationForm__c form = Database.query(queryString);

		Test.setCurrentPage(Page.EntryRequirements);
		ApexPages.currentPage().getParameters().put('formId', form.Id);

		Test.startTest();
			EntryRequirementsCC controller = new EntryRequirementsCC(); 
			controller.upload();
		Test.stopTest();
	}

    static testMethod void test() {
        ApplicationForm__c form = [Select Id from ApplicationForm__c];
        EntryRequirement__c er = [Select Id from EntryRequirement__c];

        Test.setCurrentPage(Page.EntryRequirements);
        ApexPages.currentPage().getParameters().put('formId', form.Id);

        Test.startTest();
            EntryRequirementsCC controller = new EntryRequirementsCC();
            Attachment attach = controller.attachmentMap.get(er.Id);
            attach.Name = 'Unit Test Attachment';
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            attach.body = bodyBlob;
            controller.upload();
            controller.submittedERId = er.Id;
            controller.uploadSingle();
            system.assert(controller.isVFHRendered);
        Test.stopTest();

    }
}