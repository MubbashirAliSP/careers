/**
 * @description Notifies the assigned trainer if an Enrolment is approaching the end of a Stage and no Assessment Attempt record has been created yet
 * @author Yuri Gribanov
 * @date n/a
 * @history
 *       14.JAN.2016    Biao Zhang          - Added Due date(ActivityDate) for the task
 *		 18.APR.2016	Ranyel Maliwanag	- Code refactoring and documentation
 *											- Fixed logic to create tasks only for Censuses that are ineligible instead of for everything
 * 		 02.JUN.2016	Ranyel Maliwanag	- Fixed the issue where Task creation fails if the Enrolment's Assigned Trainer is inactive and defaults it to the current running user. Reference case: 00088005
 *		 10.JUN.2016	Ranyel Maliwanag	- Fixed the issue where batch tries to update a deleted Task. Reference case: 00089713
 */
public without sharing class CensusEligibilityNotificationBatch2 implements Database.Batchable<sObject>, Database.Stateful {
	public Database.QueryLocator start(Database.BatchableContext BC) {
		Date dt = Date.today().addDays(+16);
		return Database.getQueryLocator('SELECT Id, EnrolmentId__c, EnrolmentId__r.Student__r.PersonContactId, EnrolmentId__r.Assigned_Trainer__c, EnrolmentId__r.Assigned_Trainer__r.IsActive, (SELECT Id FROM AssessmentAttempts__r), Stage__c, EndDate__c, StageTaskId__c FROM Census__c WHERE EndDate__c = : dt AND EndDate__c >= TODAY AND (Stage__c = \'Stage 1\' OR Stage__c = \'Stage 2\') AND EnrolmentId__r.Enrolment_Status__c LIKE \'Active%\'');
	}


	public void execute(Database.BatchableContext BC, List<SObject> scope) {
		Database.DMLOptions dm = new Database.DMLOptions();
		dm.EmailHeader.triggerUserEmail = true;

		Map<Census__c, Task> censusTaskMap = new Map<Census__c, Task>();

		// Build a list of all Task Ids
		Set<Id> allTaskIds = new Set<Id>();
		for (SObject item : scope) {
			Census__c record = (Census__c) item;
			allTaskIds.add(record.StageTaskId__c);
		}

		// Build a list of filtered non-deleted Task Ids
		Set<Id> taskIds = new Set<Id>();
		for (Task record : [SELECT Id FROM Task WHERE Id IN :allTaskIds]) {
			taskIds.add(record.Id);
		}

		for (SObject s : scope) {
			Census__c cen = (Census__c) s;

			Integer daysBetween = Date.today().daysBetween(cen.EndDate__c);
			String subject = 'Stage Notification ' + daysBetween + ' days until end of ' + cen.Stage__c;
			String description = 'Student is approaching the stage end date and has not commenced an assessment. This student will be suspended if an assessment is not recorded.  Please confirm with student and advise they are to conduct an assessment via Blackboard.';

			if (daysBetween <= 5) {
				String days = daysBetween == 1 ? ' day ' : ' days ';
				subject = 'Important Stage Notification ' + daysBetween + days + 'until ' + cen.Stage__c;
				description = 'Student is approaching the stage end date and has not commenced an assessment. This student will be suspended if an assessment is not recorded.  Please confirm with student and advise they are to conduct an assessment via Blackboard.';
			}

			if (cen.AssessmentAttempts__r.isEmpty()) {
				Task t = new Task(
					Id = taskIds.contains(cen.StageTaskId__c) ? cen.StageTaskId__c : null,
					Recordtypeid = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
					WhatId = cen.EnrolmentId__c,
					WhoId = cen.EnrolmentId__r.Student__r.PersonContactId,
					OwnerId = String.isNotBlank(cen.EnrolmentId__r.Assigned_Trainer__c) && cen.EnrolmentId__r.Assigned_Trainer__r.IsActive ? cen.EnrolmentId__r.Assigned_Trainer__c : UserInfo.getUserId(),
					Subject = subject,
					Status = 'Not Started',
					Priority = 'High',
					Description = description,
					ActivityDate = Date.today()
				);
				censusTaskMap.put(cen, t);
			}
		}
		upsert censusTaskMap.values();
		for (Census__c record : censusTaskMap.keySet()) {
			record.StageTaskId__c = censusTaskMap.get(record).Id;
		}
		update new List<Census__c>(censusTaskMap.keySet());
	}


	public void finish(Database.BatchableContext BC) {
	}
}