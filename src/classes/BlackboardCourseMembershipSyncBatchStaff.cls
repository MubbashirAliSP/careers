global with sharing class BlackboardCourseMembershipSyncBatchStaff implements Database.Batchable<sObject>, Database.AllowsCallouts {


    
    global final String query = 'Select Blackboard_Primary_Key__c, Available_After_Sync__c, Blackboard_Course__r.Blackboard_Primary_Key__c,Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c, Name, create__c, deactivate__c,reactivate__c,Blackboard_User_Role__r.Blackboard_Role__r.Role_Id__c From Blackboard_Course_Membership__c Where Sync_Required__c = True AND RecordType.DeveloperName = \'Staff\'';
    
    global BlackboardCourseMembershipSyncBatchStaff() {


        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        for(SObject s :scope) {
             //collect blackboard t3 courses into a list
             Blackboard_Course_Membership__c bbcm = (Blackboard_Course_Membership__c) s;
            
             //send bulk courses to SIS WS
        	BlackboardCourseMembershipWS.saveCourseMembership(bbcm);
         }

       
    
    }
    
    global void finish(Database.BatchableContext BC) {

    }
    
}