/**
 * @description Test class for `ApplicationFormItemDisplayCC` class
 * @author Ranyel Maliwanag
 * @date 03.SEP.2015
 */
@isTest
public with sharing class ApplicationFormItemDisplayCC_Test {
    @testSetup
    static void controllerSetup() {
        ApplicationsTestUtilities.initialiseEnrolmentApplication();
    }

    static testMethod void componentTest() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c item = [SELECT Id FROM ApplicationFormItem__c WHERE DisplayText__c = 'First Name'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = item.Id,
                ApplicationFormId__c = form.Id
            );
        insert answer;

        Test.startTest();
            ApplicationFormItemDisplayCC controller = new ApplicationFormItemDisplayCC();
            controller.formItemRecord = item;
            controller.formItemAnswerRecord = answer;
        Test.stopTest();
    }

    static testMethod void dropdownItemNonStandardTest() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c item = new ApplicationFormItem__c(
                DisplayText__c = 'Dropdown Text',
                PageNumber__c = 1,
                QuestionType__c = 'Dropdown',
                DropdownOptions__c = 'Hello\r\nWorld',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW',
                IsOtherOptionAvailable__c = true
            );
        insert item;
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = item.Id,
                ApplicationFormId__c = form.Id,
                Answer__c = 'Not'
            );
        insert answer;

        Test.startTest();
            ApplicationFormItemDisplayCC controller = new ApplicationFormItemDisplayCC();
            controller.formItemRecord = item;
            controller.formItemAnswerRecord = answer;

            System.assert(controller.formChoices.size() > 0);
        Test.stopTest();
    }

    static testMethod void dropdownItemStandardTest() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c item = new ApplicationFormItem__c(
                DisplayText__c = 'Dropdown Text',
                PageNumber__c = 1,
                QuestionType__c = 'Dropdown',
                DropdownOptions__c = 'Hello\r\nWorld',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW',
                IsOtherOptionAvailable__c = true,
                IsVerticalChoices__c = true
            );
        insert item;
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = item.Id,
                ApplicationFormId__c = form.Id,
                Answer__c = 'Hello'
            );
        insert answer;

        Test.startTest();
            ApplicationFormItemDisplayCC controller = new ApplicationFormItemDisplayCC();
            controller.formItemRecord = item;
            controller.formItemAnswerRecord = answer;

            System.assert(controller.formChoices.size() > 0);
            System.assert(!controller.isRenderAsDropdown);
            System.assert(controller.layout == 'pageDirection');
        Test.stopTest();
    }


    static testMethod void mutliSelectDropdownTest() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c item = new ApplicationFormItem__c(
                DisplayText__c = 'Multiselect Dropdown Text',
                PageNumber__c = 1,
                QuestionType__c = 'Multiple-select Dropdown',
                DropdownOptions__c = 'Hello\r\nWorld',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW',
                IsOtherOptionAvailable__c = true
            );
        insert item;
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = item.Id,
                ApplicationFormId__c = form.Id,
                Answer__c = '[Hello]'
            );
        insert answer;

        Test.startTest();
            ApplicationFormItemDisplayCC controller = new ApplicationFormItemDisplayCC();
            controller.formItemRecord = item;
            controller.formItemAnswerRecord = answer;

            System.assert(controller.multiSelectOptions.size() > 0);
            controller.multiSelectOptions = new List<String>{'Hello', 'World'};
        Test.stopTest();
    }


    static testMethod void alternateAnswerTest() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c item = new ApplicationFormItem__c(
                DisplayText__c = 'Text Item',
                PageNumber__c = 1,
                QuestionType__c = 'Text',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW',
                IsOtherOptionAvailable__c = true
            );
        insert item;
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = item.Id,
                ApplicationFormId__c = form.Id,
                Answer__c = '0005',
                ReadableAnswer__c = 'Hello'
            );
        insert answer;

        Test.startTest();
            ApplicationFormItemDisplayCC controller = new ApplicationFormItemDisplayCC();
            controller.formItemRecord = item;
            controller.formItemAnswerRecord = answer;

            System.assert(controller.isAlternateAvailable == true);
        Test.stopTest();
    }
}