/*
 *  Author: Joel T. Jayoma
 *  Description:Test class for IntakeUnitsControllerExt
 *  -------------------------------------------------
 * HISTORY
 * - 25.FEB.2013	Cletz Cadiz		Modified all test methods
 */
@isTest
private class IntakeUnitsControllerExtTest {

    static testMethod void IntakeUnitsControllerExt_Init() {        
        
        ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		State__c state = new State__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Account acc = new Account();
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
       	state 	= TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
		acc = TestCoverageUtilityClass.createTrainingOrganisation();
		loc = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, acc.Id, parentLoc.Id, state.Id);
		program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
      	
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);  
              
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.searchText='Search Here';
        Intake.search();
        
        
        
    }
    
    static testMethod void IntakeUnitsControllerExt_Init2() {        
        
        ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
		State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Account acc = new Account();
        //unit = TestCoverageUtilityClass.createUnit();
        acc = TestCoverageUtilityClass.createTrainingOrganisation();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
       	state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
		loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
		program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
      	
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);      
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Unit Name';
        Intake.searchText='Sea';
        Intake.search();
        //Intake.addIntakeUnit();                
    }
    
     static testMethod void IntakeUnitsControllerExt_Init3() {        
        
        ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
		program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Unit Name';
        Intake.SelectedUnit='All';  
        Intake.searchText = 'greater than 5';
        Intake.search();                
    }
    
    static testMethod void IntakeUnitsControllerExt_Init4() {        
        
      	ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
        program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);     
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);   
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Unit';
        Intake.SelectedUnit='All';  
        Intake.searchText = '1';
        Intake.search();                
    }
    
    static testMethod void IntakeUnitsControllerExt_Init5() {        
       	
       	ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
		program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);    
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Unit name';
        Intake.SelectedUnit='None';  
        Intake.searchText = 'greater than 5';
        Intake.search();                
    }
    
     static testMethod void IntakeUnitsControllerExt_Init6() {        
        
       	ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
		State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
      	program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);        
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Unit name';
        Intake.SelectedUnit='Not None';  
        Intake.searchText = 'greater than 5';
        Intake.search();                
    }
    
    static testMethod void IntakeUnitsControllerExt_Init7() {        
        
		ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
      	program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='Not Unit name';
        Intake.SelectedUnit='All';  
        Intake.searchText = 'greater than 5';
        Intake.search();                
    }
    
    static testMethod void IntakeUnitsControllerExt_Init8() {        
        
        ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		state = TestCoverageUtilityClass.createState(country.Id);
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
      	program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);        
        
        Qualification_Unit__c qfc = new Qualification_Unit__c(Qualification__c=q.Id,unit__c=unit.Id, Unit_Type__c='Core');
        insert qfc;
        
        system.debug('@@Qualification_Unit__c'+[SELECT stage__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c]);
        system.debug('@@Qualification_Unit__c2'+[SELECT Id, Qualification__c, unit__c FROM Qualification_Unit__c]);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='test Unit';
        Intake.SelectedUnit='Core';  
        Intake.searchText = 'test Unit';
        Intake.search();                
    }
    
     static testMethod void IntakeUnitsControllerExt_Init9() {        
        ANZSCO__c anz = new ANZSCO__c();
		Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
		Locations__c parentLoc = new Locations__c();
		Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        unit = TestCoverageUtilityClass.createUnit();
        anz = TestCoverageUtilityClass.createANZSCO();
	    fld = TestCoverageUtilityClass.createFieldOfEducation();
       	q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
       	fundStream = TestCoverageUtilityClass.createFundingStream();
       	country = TestCoverageUtilityClass.createCountry();
		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
      	loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
      	program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);
        //insert i;
        
        Qualification_Unit__c qfc = new Qualification_Unit__c(Qualification__c=q.Id,unit__c=unit.Id, Unit_Type__c=null);
        insert qfc;
        
        system.debug('@@Qualification_Unit__c'+[SELECT stage__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c]);
        system.debug('@@Qualification_Unit__c2'+[SELECT Id, Qualification__c, unit__c FROM Qualification_Unit__c]);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        //con.reset();
        //List<String> l = [select name from Qualification__c];
        //con.addFields(new List<String>{'w','w'});     
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);
        //Intake.currPage = 1;
        //Intake.next();
        Intake.SelectedSearch='test Unit';
        Intake.SelectedUnit='None';  
        Intake.searchText = 'test Unit';
        Intake.search();  
        Intake.getItems();
        Intake.getSelected();        
        Intake.getItems2();
        Intake.next();
        //Intake.addIntakeUnit();
        List<String> sList = Intake.dynamicFields;     
        Intake.previous();
    }

    static testMethod void IntakeUnitsControllerExt_Init10() {        
        
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();         
        Qualification__c q = new Qualification__c();
        Unit__c unit = new Unit__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Intake__c i = new Intake__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        State__c state = new State__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        unit = TestCoverageUtilityClass.createUnit();
        unit.Unit_name__c = 'test12345';
        update unit;

        Unit__c parentUnit = TestCoverageUtilityClass.createUnit();
        parentUnit.Parent_UoC__c = unit.Id;
        update parentUnit;

        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        q = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        fundStream = TestCoverageUtilityClass.createFundingStream();
        country = TestCoverageUtilityClass.createCountry();
        state = TestCoverageUtilityClass.createState(country.Id);
        loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
        loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);  
        program = TestCoverageUtilityClass.createProgram(q.Id, loc.Id);
        i = TestCoverageUtilityClass.createIntake(q.Id,program.Id,loc.Id,fundStream.Id);        
        
        Qualification_Unit__c qfc = new Qualification_Unit__c(Qualification__c=q.Id,unit__c=unit.Id, Unit_Type__c=NULL, Unit_Record_Type__c = 'Unit Of Competency');
        insert qfc;

        system.debug('###$ has changed?:: ' + [SELECT stage__c,Stage__r.Name,Unit_Type__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :q.Id AND Unit_Type__c = null and Unit__r.Unit_Name__c like '%Search Here%' and Unit_Record_Type__c = 'Unit Of Competency' order by Unit__r.Name].size());            
      
        
        system.debug('@@Qualification_Unit__c'+[SELECT stage__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c]);
        system.debug('@@Qualification_Unit__c2'+[SELECT Id, Qualification__c, unit__c FROM Qualification_Unit__c]);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(i);
        
        IntakeUnitsControllerExt Intake = new IntakeUnitsControllerExt(sc);

        Intake.SelectedUnit = 'None';
        Intake.searchText = unit.Unit_name__c;
        Intake.SelectedSearch = 'Unit Name';

        Intake.search();
        Intake.previous();

        Intake.search();
        Intake.queryList[0].selected = true;
        Intake.addIntakeUnit();

        system.debug('###$ :: ' + [SELECT stage__c,Stage__r.Name,Unit_Type__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :q.Id AND Unit_Type__c = null and Unit__r.Unit_Name__c like '%Search Here%' and Unit_Record_Type__c = 'Unit Of Competency' order by Unit__r.Name].size());            
      
    }
}