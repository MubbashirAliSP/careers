/**
 * @description Enrolment unit Trigger Handler Test Methods
 * @author Janella Lauren Canlas
 * @date 07.NOV.2012
 * HISTORY
 * - 07.NOV.2014    Sairah Hadjinoor     -updated insertEnrolmentUnits method to cater supersession functionalities
 */

@isTest
private class EnrolmentUnitTriggerHandler_Test {
    /*
        Trigger Handler for Enrolment Units
    */

    static testMethod void insertEnrolmentUnits() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EnrolmentUnit_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp3 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Added By S.Hadjinoor 07.NOV.2014
        Unit__c unit2 = new Unit__c();
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                //Added By S.Hadjinoor 07.NOV.2014
                unit2 = TestCoverageUtilityClass.createUnit();
                
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                countryAU = TestCoverageUtilityClass.queryAustralia();
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType(); 
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Overseas_Country__c = null;
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                insert enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, null, loc.Id, lineItemUnit.Id);
                eUnit.Unit_Result__c = res.Id;
                update eUnit;
                
                uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                uhp2 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                uhp3 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit2.Id, state.Id);
                
                eUnit.Training_Delivery_Location__c = null;
                // added by S.Hadjinoor 07.NOV.2014
                eUnit.Unit__c = unit2.Id;
                
                eUnit.Start_Date__c = Date.TODAY() - 25;
                eUnit.End_Date__c = Date.TODAY() - 1;
                update eUnit;
                  // update start - S.Hadjinoor 07.NOV.2014
                  
                Enrolment_Unit__c eUnit2 = [SELECT Unit__c, Has_Been_Superseded__c, Superseded_1_EU_End_Date__c,Superseded_2_EU_End_Date__c from  Enrolment_Unit__c where Id =: eUnit.Id LIMIT 1];

                System.assertEquals(eUnit2.Has_Been_Superseded__c, true);
                System.assertEquals(eUnit2.Unit__c, unit2.Id);
                System.assert(eunit2.Superseded_1_EU_End_Date__c!=null);
                System.assert(eunit2.Superseded_2_EU_End_Date__c==null);

                eUnit2.Unit__c = unit.Id;
                update eUnit2;               
                System.assertEquals(eUnit2.Unit__c, unit.Id);

                Enrolment_Unit__c eUnit3 = [SELECT Unit__c, Has_Been_Superseded__c, Superseded_2_EU_End_Date__c,Superseded_3_EU_End_Date__c from  Enrolment_Unit__c where Id =: eUnit2.Id LIMIT 1];

                System.assertEquals(eUnit3.Unit__c, unit.Id);
                System.assert(eunit3.Superseded_2_EU_End_Date__c!=null);
                System.assert(eunit3.Superseded_3_EU_End_Date__c==null);

                eUnit3.Unit__c = unit2.Id;
                update eUnit3;
                System.assertEquals(eUnit3.Unit__c, unit2.Id);

                Enrolment_Unit__c eUnit4 = [SELECT Unit__c, Has_Been_Superseded__c, Superseded_3_EU_End_Date__c,Superseded_4_EU_End_Date__c from  Enrolment_Unit__c where Id =: eUnit3.Id LIMIT 1];

                System.assertEquals(eUnit4.Unit__c, unit2.Id);
                System.assert(eunit4.Superseded_3_EU_End_Date__c!=null);
                System.assert(eunit4.Superseded_4_EU_End_Date__c==null);

                eUnit4.Unit__c = unit.Id;
                update eUnit4;
                System.assertEquals(eUnit4.Unit__c, unit.Id);

                Enrolment_Unit__c eUnit5 = [SELECT Unit__c, Has_Been_Superseded__c, Superseded_4_EU_End_Date__c,Superseded_5_EU_End_Date__c from  Enrolment_Unit__c where Id =: eUnit4.Id LIMIT 1];

                System.assertEquals(eUnit5.Unit__c, unit.Id);
                System.assert(eunit5.Superseded_4_EU_End_Date__c!=null);
                System.assert(eunit5.Superseded_5_EU_End_Date__c==null);

                eUnit5.Unit__c = unit2.Id;
                update eUnit5;
                System.assertEquals(eUnit5.Unit__c, unit2.Id);
                
                // update end - S.Hadjinoor 07.NOV.2014
                
              //  euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eUnit.Id, qunit.Id, fld.Id); insert euos;
                
                /*eUnit.Start_Date__c = date.today() + 5;
                eUnit.End_Date__c = date.today() + 10;
                eUnit.Training_Delivery_Location__c = null;
                update eUnit; */
            test.stopTest();
        }
    }
    
    static testMethod void insertEnrolmentUnits2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EnrolmentUnit_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                countryAU = TestCoverageUtilityClass.queryAustralia();
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType(); 
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Overseas_Country__c = null;
                insert enrl;
                enrl.Referral_Source__c = 'iLearn';
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                update enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                
                uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                uhp2 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                
                euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eUnit.Id, qunit.Id, fld.Id);
                insert euos;
                
              //  euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eUnit.Id, qunit.Id, fld.Id); insert euos;
                
                /*eUnit.Start_Date__c = date.today() + 5;
                eUnit.End_Date__c = date.today() + 10;
                eUnit.Training_Delivery_Location__c = null;
                update eUnit; */
            test.stopTest();
        }
    }
    
}