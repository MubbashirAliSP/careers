/**
 * @description Central trigger handler for the `AssessmentAttempt__c` object
 * @author Biao Zhang
 * @date 11.NOV.2015
 * @history
 *       17.MAR.2016    Ranyel Maliwanag    - Removed logic to update Census counter 
 */
public without sharing class AssessmentAttemptTriggerHandler {
    public static void onBeforeInsert(List<AssessmentAttempt__c> records) {
        updateCensusReference(records);
    }

    public static void onBeforeUpdate(List<AssessmentAttempt__c> records, Map<Id, AssessmentAttempt__c> recordsMap) {       
        updateCensusReference(records);
    }

    public static void onAfterInsert(List<AssessmentAttempt__c> records) {
        //updateCensusCounter(records);
        updateLastAssessmentAttemptDate(records);
        CensusEligibilityController.EligibilityForStageMapper(records);
    }

    public static void onAfterUpdate(List<AssessmentAttempt__c> records, Map<Id, AssessmentAttempt__c> recordsMap) {
        //updateCensusCounter(records);

        List<AssessmentAttempt__c> forLastAssessmentAttemptDateUpdate = new List<AssessmentAttempt__c>();
        for (AssessmentAttempt__c record : records) {
            if ((record.AssessmentStatus__c != recordsMap.get(record.Id).AssessmentStatus__c) || 
                    (record.SubmittedDate__c != recordsMap.get(record.Id).SubmittedDate__c)) {
                forLastAssessmentAttemptDateUpdate.add(record);
            }
        }
        if (!forLastAssessmentAttemptDateUpdate.isEmpty()) {
            updateLastAssessmentAttemptDate(forLastAssessmentAttemptDateUpdate);
        }
    }

    public static void onAfterDelete(List<AssessmentAttempt__c> records) {
        //updateCensusCounter(records);
    }

    /**
     * @description Update the census reference for Assessment Attempt and update Census attempt counter field
     * @author Biao Zhang
     * @date 12.NOV.2015
     */
    public static void updateCensusReference(List<AssessmentAttempt__c> attempts) {
        //because census doesn't look up student, we need to manually map student to censuses       
        Map<Id, List<Census__c>> studentCensusMap = new Map<Id, List<Census__c>>();
        for(AssessmentAttempt__c attempt : attempts) {
            if(attempt.AccountId__c != null) {
                studentCensusMap.put(attempt.AccountId__c, new List<Census__c>());
            }
        }

        //populate the student-censuses mapping value
        List<Enrolment__c> enrolments = [Select 
                                            id, 
                                            Student__c, 
                                            (Select 
                                                Id, 
                                                StartDate__c,
                                                EndDate__c 
                                            from 
                                                Censuses__r)
                                        from 
                                            Enrolment__c
                                        where 
                                            Student__c in :studentCensusMap.keySet()
                                        ];
        for(Enrolment__c enrolment : enrolments) {
            if(enrolment.Censuses__r.size() > 0) {
                List<Census__c> censusOfStudent = studentCensusMap.get(enrolment.Student__c);
                censusOfStudent.addAll(enrolment.Censuses__r);
                studentCensusMap.put(enrolment.Student__c, censusOfStudent); 
            }           
        }

        //update census reference and counting attempts for each census
        for(AssessmentAttempt__c attempt : attempts) {
            if(attempt.AccountId__c != null) {
                List<Census__c> censuses = studentCensusMap.get(attempt.AccountId__c);
                for(Census__c census : censuses) {
                    if (attempt.SubmittedDate__c <= census.EndDate__c && attempt.SubmittedDate__c >= census.StartDate__c) {
                        attempt.Census__c = census.Id;
                        break;
                    }
                }
                //if(attempt.Census__c == null) {
                //    attempt.addError('Cannot find a census for the assessment submitted on ' + attempt.SubmittedDate__c.format());
                //}
            }
        }
    }

    /**
     * @description Update Census attempt counter field
     * @author Biao Zhang
     * @param (List<AssessmentAttempt__c>) attempts: the assessment attempts, must have census reference beforehand
     * @date 13.NOV.2015
     */
    //public static void updateCensusCounter(List<AssessmentAttempt__c> attempts) {
    //    List<Id> censusIds = new List<Id>();

    //    for(AssessmentAttempt__c attempt : attempts) {
    //        censusIds.add(attempt.Census__c);
    //    }

    //    List<Census__c> censuses = [Select
    //                                    AssessmentAttemptsCount__c,
    //                                    (Select
    //                                        Id
    //                                    from
    //                                        AssessmentAttempts__r)
    //                                from
    //                                    Census__c
    //                                where
    //                                    Id in :censusIds
    //                                ];

    //    for(Census__c census : censuses) {
    //        census.AssessmentAttemptsCount__c = census.AssessmentAttempts__r.size();
    //    }
        
    //    update censuses;
    //}


    /**
     * @description Updates the Last Assessment Attempt Date on the related Account records of the Assessment Attempt Records given
     * @author Ranyel Maliwanag
     * @date 22.JAN.2016
     * @history
     *      Biao Zhang 17.FEB.2016      check whether account id is null before adding account id into account id set
     */
     public static void updateLastAssessmentAttemptDate(List<AssessmentAttempt__c> attempts) {
        List<Account> accountsForUpdate = new List<Account>();
        Set<Id> accountIds = new Set<Id>();

        // Build the set of account Ids to avoid duplicate records for insertion
        for (AssessmentAttempt__c attempt : attempts) {
            if(attempt.AccountId__c != null) {
                accountIds.add(attempt.AccountId__c);
            }
        }

        // Update the Last Assessment Attempt Date for all collected accounts
        for (Id accountId : accountIds) {
            Account attemptAccount = new Account(
                    Id = accountId,
                    LastAssessmentAttemptDate__c = Datetime.now()
                );
            accountsForUpdate.add(attemptAccount);
        }
        update accountsForUpdate;
     }
}