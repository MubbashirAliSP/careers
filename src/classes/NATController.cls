public with sharing class NATController {

    public boolean deleteExistingFiles {get;set;}
    
    public boolean nswClaim {get;set;}
    
    public boolean displayPopup {get; set;}
    
    public boolean showClaimingPopUp {get;set;}
    
    public List<NatViewer>natFilesInfo {get;set;}

    public String state {get;set;}
    
    public String year {get;set;}
    
    public Set<String>ContractsCodes = new Set<String>();
    
    public String rto {get;set;}
    
    private String soql {get;set;}
    
    public List<SelectOption> states {get;set;}
    
    public List<SelectOption> years {get;set;}
    
    public List<SelectOption> rtos {get;set;}
    
    public Map<Id,Decimal> Unit_NominalHoursMap = new Map<Id, Decimal>();
    
    public Account dummyAccount {get;set;}
    
    public String pur_codes {get;set;} // used for pur. contracts query string
    
    public Set<String>tempPur_codes {get;set;} // used to reduce load on cpu time at generate screen
    
    /*Filter Fields */
    
    public List<SelectOption>subTypes {get;set;}
    public String subType {get;set;}
    
    public Boolean zipFiles {get;set;}
    
    public Boolean ReportAwardRecorded {get;set;}
    public Boolean SetClaimDate {get;set;}
    
    public transient Date ClaimDate {get;set;}
    
    public transient Boolean showClaimDate {get;set;} 
    
    public transient String fundingSourceSearch {get;set;}
    
    public transient String ContractCodeSearch {get;set;}
    public transient String IncludeActive {Get; set; }
    public static final List<String> exlStates =  new List<String>  {'Northern Territory', 'Tasmania', 'Australian Capital Territory'};
    
    public Set<ID> location;
    public Set<ID> parentUoC;
    public Set<ID> qualification;
    public Set<Id>student;
    
    public Account TrainingOrgToValidate {get;set;}
    
    public transient List<Locations__c> TrainingOrgLocationsToValidate = new List<Locations__c>();
    
    public transient List<Locations__c> TrainingOrgLocationsErrors {get;set;}
    
    public List<Locations__c> LocationsErrorsLimitedView {get;set;}
    
    public transient List<Qualification__c> QualificationsToValidate = new List<Qualification__c>();
    
    public transient List<Qualification__c> QualificationsErrors {get;set;}
    
    public List<Qualification__c> QualificationsErrorsLimitedView {get;set;}
    
    public transient List<Unit__c>UnitsOfComToValidate {get;set;}
    
    public transient List<Unit__c>UnitsOfComErrors {get;set;}
    
    public List<Unit__c>UnitsOfComErrorsLimitedView {get;set;}
    
    public transient List<Account>StudentsToValidate = new List<Account>();
    
    public transient List<Account>StudentsErrors {get;set;}
    
    public List<Account>StudentsErrorsLimitedView {get;set;}
    
    public List<Account>StudentsErrorsLimitedView100 {get; set;}
    
    public transient List<Account>StudentPostalErrors {get;set;}
    
    public List<Account>StudentPostalErrorsLimitedView {get;set;}
    
    public transient List<Enrolment__c>enrolmentsToValidate = new List<Enrolment__c>();
    
    public transient List<Enrolment__c>enrolmentsErrors {get;set;}
    
    public List<Enrolment__c>enrolmentErrorsLimitedView {get;set;}
    
    public List<PurchasingContractView> FilteredContracts {get;set;}
   
    transient public List<Enrolment_Unit__c>FilteredEU = new List<Enrolment_Unit__c>();
    
    public transient List<Enrolment_Unit__c>EUErrors {get;set;}
    
    public List<Enrolment_Unit__c>EUErrorsLimitedView {get;set;}
     
    public List<DisabilityObject>disabilities {get;set;}
    public List<PriorEducationalAchievmentObject>priorAchievements {get;set;}
    
    public String selectedFile {get;set;}
    
    public Boolean isValidated {get;set;} // need to check, if validation ran before, if not do not clear lists
    public Boolean exportDisallowed {get;set;}
    
    public String queryStringMain; // Contains query with all fields required to export NAT00120
    public String queryStringUnitEnrolment; // Contains query with only fields required to export NAT00060 and NAT00130
    public String queryStringE; // used for NAT80/85/90/100 due duplicate records produced
    public String queryStringStudents; //Contains Account query, required for NAT00080, NAT00085, NAT0090 and NAT00100
    
    
    public NATController() {
    
        deleteExistingFiles = true;
        nswClaim = false;
      
        isValidated = false;
        exportDisallowed = true;
        displayPopup = false;
        setClaimDate = false;
        
        state = '--Select State--';
        
      
        /*Grab RTOs */
        
        rtos = new List<SelectOption>();
        
        rtos.add(new SelectOption('--None--', '--None--'));
        //Add RTOs to Select list and display on the page
        for(Account a : [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Training Organisation'])
            rtos.add(new SelectOption(a.id,a.name));
          
             
        years = new List<SelectOption>();
        Integer yearInt = 2010;
        
            for(Integer i = 5; i >=0; i--) {
                
                years.add(new SelectOption(String.ValueOf(yearInt), String.ValueOf(yearInt)));
                yearInt +=1;
            }
          
        if(year == null || year == '')  
            year = String.ValueOf(date.today().year());
            
        filterStates();
        
        
        TrainingOrgLocationsToValidate = new List<Locations__c>();
        QualificationsToValidate = new List<Qualification__c>();
     
    }
    
    /*After RTO is selected, this method is called to retrieve relevant states */
    public pageReference filterStates() {
        
        Set<String> StateName = new Set<String>();
        
        states = new List<SelectOption>();
        
        if(rto == null)
            states.add(new SelectOption('--None','--None--'));
            
        else {
               
        for(Locations__c l : [Select Id, Child_State__c FROM Locations__c WHERE Training_Organisation__c = : rto AND Child_State__c not in :exlStates]) {
            if(l.Child_State__c != null)
                StateName.add(l.Child_State__c);
        }
            
         //Generate Picklist for the state
         states.add(new SelectOption('--Select State--', '--Select State--'));
         for(String s : StateName)
                states.add(new SelectOption(s, s));
       
        }
            
        return null;
     
        
    }
    
    public pageReference filterPurchasingContract() {
        List<String> fundingSourceSearchParams = fundingSourceSearch.split(' AND ');
        List<String> ContractCodeSearchParams = ContractCodeSearch.split(' AND ');
        
        String query = 'SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c ' +
            ' FROM Purchasing_Contracts__c ' +
            ' WHERE Training_Organisation__c = : rto AND Contract_State__c = : state';
        if(fundingSourceSearch !='' ) {
            query += ' AND State_Fund_Source__r.Code__c in :fundingSourceSearchParams ';
        }
        if(ContractCodeSearch != '') {
            query += ' AND (';
            boolean first = true;
            for ( String c : ContractCodeSearchParams ){
                if ( first )
                    first = false;
                else
                    query += ' OR ';
                query += 'Contract_Code__c LIKE \'%' + String.escapeSingleQuotes(c) + '%\''; 
            }
            query += ')';
        }
        if ( IncludeActive == 'active')
            query += ' AND Active__c = true';
        else if ( IncludeActive == 'inactive')
            query += ' AND Active__c = false';
        
        //remember what was selected...
        Map<Id, PurchasingContractView> selected = new Map<Id, PurchasingContractView>();
        for ( PurchasingContractView pcv : FilteredContracts){
            if ( pcv.selected ){
                selected.put(pcv.pc.id, pcv);
            }
        }

        //No Filtering
        FilteredContracts = new List<PurchasingContractView>();
        for ( Purchasing_Contracts__c pc : Database.query(query) ){
            FilteredContracts.add(new PurchasingContractView(pc, selected.containsKey(pc.id)));
            selected.remove(pc.id);
        }

        for ( PurchasingContractView pcv : selected.values()){
            FilteredContracts.add(pcv);
        }
        
        return null;
    }
    
    public pageReference continueButton() {
        
      String LibraryId;
        
      if(deleteExistingFiles) {
        
        if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        
        
        /*We will delete all files in content folder for the state */
        
         List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
         
         try {delete existingContent;}
         catch(Exception e) { ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Existing NAT Files Could Not Be Deleted');}    
          
      }
         
        
        
        if(state == '--None' || rto == '--None--') {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'RTO and State need to be selected');
            
            return null;
        }
        
        populateAvailableLists();
        
        if(state == 'Western Australia')
            return new PageReference('/apex/NatgeneratorWA?rto=' + rto);
                    
         return new PageReference('/apex/Natgenerator'); 
            
        
    }
    
    public pageReference SetClaimButton() {
        
        if(state == 'New South Wales') {
            
            String isClaim = '';
            
            if(nswClaim)
                isClaim = 'true';
            else
                isClaim = 'false';
        
        
            return new PageReference('/apex/NSWClaimingPurchasingContractFilter?state=' +state + '&rto=' + rto + '&year=' + year + '&claim=' + isClaim);
        }
           
        if(state == 'Western Australia')
            return new PageReference('/apex/WAClaiming?state=' +state + '&rto=' + rto);   
        return null;
         
    }
    public PageReference backButton(){
        if(ApexPages.currentPage().getParameters().get('state') == 'New South Wales')
            return new PageReference('/apex/NSWClaiming?state=' +ApexPages.currentPage().getParameters().get('state') + '&rto=' + ApexPages.currentPage().getParameters().get('rto')+ '&claim=' + ApexPages.currentPage().getParameters().get('claim'));
        else if(ApexPages.currentPage().getParameters().get('state') == 'Western Australia')
            return new PageReference('/apex/WAClaiming?state=' +ApexPages.currentPage().getParameters().get('state') + '&rto=' + ApexPages.currentPage().getParameters().get('rto')+ '&claim=' + ApexPages.currentPage().getParameters().get('claim'));
        else
            return new PageReference('/apex/Natgenerator'); 
         
    }
        
    public pageReference cancelButton() {
        
        return new PageReference('/apex/NATGeneratorStateSelection');
    }
    
    public pageReference cancelButtonHome() {
        
        return new PageReference('/home/home.jsp');
    }
    
    
    public pageReference nextValidateButton() {
        
        createFileStructure();
        
        
       
        return new PageReference('/apex/NATGeneratorValidation');
    }
    
    public pageReference viewErrors() {
        
        PageReference redirect = new PageReference('/apex/NATGeneratorErrorView');
        
        redirect.getParameters().put('fname',selectedFile);
        
        return redirect;
    }
   
    
    
    public void populateAvailableLists() {
             
            subTypes = new List<SelectOption>();    
                
            subTypes.add(new SelectOption('Partial Submission', 'Partial Submission'));
            subTypes.add(new SelectOption('End Of Year Submission', 'End Of Year Submission'));
            
        FilteredContracts = new List<PurchasingContractView>();
        
            
        for(Purchasing_Contracts__c c : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state]) 
            FilteredContracts.add(new PurchasingContractView(c, false));
        
    }
    
    public void buildPurchasingContractString() { //Creates purcontract string to be used for a query
         
         pur_codes = '';
         tempPur_codes = new Set<String>();
         
         for(PurchasingContractView p : FilteredContracts ) {
            
            if(p.selected) {
                pur_codes += '\'' + p.pc.Contract_Code__c + '\',';
                tempPur_codes.add(p.pc.Contract_Code__c);
               
            }
               
        }
        
        pur_codes = pur_codes.lastIndexOf(',') > 0 ? '(' + pur_codes.substring(0,pur_codes.lastIndexOf(',')) + ')' : pur_codes;
        
    }
    
    public void createStudentQueryString() {
        
        String studentIds = '';
        String recTypeName = 'Unit of Competency';
        
        for(Enrolment__c e : [Select Id, Student__c From Enrolment__c Where Id in (Select Enrolment__c From Enrolment_Unit__c WHERE Report_for_AVETMISS__c = true AND Purchasing_Contract_Identifier__c in : tempPur_codes AND (CALENDAR_YEAR(End_Date__c) = : Integer.valueof(year.trim()) OR CALENDAR_YEAR(Start_Date__c) = : Integer.valueof(year.trim())) AND Unit__r.RecordType.Name = : recTypeName)])
             studentIds += '\'' + e.Student__c + '\',';
        
       
     
        
        studentIds = studentIds.lastIndexOf(',') > 0 ? '(' + studentIds.substring(0,studentIds.lastIndexOf(',')) + ')' : studentIds;
        
        queryStringStudents =  'Select Student_Identifer__c,Reporting_Other_State_Identifier__c,PersonOtherPostalCode, Highest_School_Level_Comp_Identifier__c, Year_Highest_Education_Completed__c,'+
                                'Sex__c, PersonBirthdate, PersonMailingPostaLCode, PersonMailingState,Indigenous_Status_Identifier__c,'+
                                'Main_Language_Spoken_at_Home__r.Name, Main_Language_Spoken_at_Home_Identifier__c, Labour_Force_Status_Identifier__c,'+ 
                                'Country_of_Birth_Identifer__c, At_School_Flag__c,'+
                                'Client_Industry_of_Employment__r.Division__c,' + 
                                'Proficiency_in_Spoken_English_Identifier__c,Learner_Unique_Identifier__c,'+
                                'Has_prior_educational_achievement_s__c,Prior_Achievement_Flag__c,'+
                                'Prior_Achievement_Type_s__c,Prior_Education_Achievement_Recognition__c,Does_student_have_a_disability__c,Disability_Type__c,Disability_Flag_0__c,'+
                                'FirstName, LastName, PersonMailingStreet, Full_Student_Name__c,'+
                                'PersonMailingCity, State_Identifier__c, PersonHomePhone, Work_Phone__c, Address_building_property_name__c,Address_flat_unit_details__c,Address_street_number__c,Address_street_name__c,Postal_Delivery_Box__c,'+
                                'PersonMobilePhone, PersonEmail, Victorian_Student_Number__c, Salutation,Name_for_encryption_student__c,Age_Group__c,Unique_Student_Identifier__c,'+
                                'Postal_building_property_name__c,Postal_flat_unit_details__c,Postal_street_number__c,Postal_street_name__c,Postal_suburb_locality_or_town__c, '+
                                'Client_Industry_of_Employment__c, Client_Occupation_Identifier__c, Suburb_locality_or_town__c, USI_Status__c, Address_Post_Code__c' +
                                ' FROM Account WHERE Id in ' + studentIds;
    }
    
    public void createQueryString() {
      
        
        String recTypeName = 'Unit of Competency';
          
            queryStringMain = 'Select Id,' +
                            'Name,' +
                            'Enrolment__r.Qualification__c,'+
                            'Scheduled_Hours__c,'+
                            'Unit_of_Competency_Identifier__c,'+
                            'Training_Delivery_Location__c,'+
                            'Unit__r.RecordType.Name,'+
                            'Unit__r.Field_of_Education_Identifier__c,'+
                            'Unit__r.Unit_Flag__c,'+
                            'Unit__r.Vet_Non_Vet__c,'+
                            'Enrolment__r.Student__c,'+
                            'Enrolment__r.Student_Identifier__c,'+
                            'Delivery_Location_Identifier__c,'+
                            'Delivery_Mode_Identifier__c,'+
                            'AVETMISS_National_Outcome__c,'+
                            'Commencing_Course_Identifier__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c,'+
                            'Purchasing_Contract_Identifier__c,'+
                            'VET_in_Schools__c,'+
                            'Registration_Number_Identifier__c,'+
                            'Client_Identifier_New_Apprenticeships__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c,'+
                            'Enrolment__r.Qualification__r.Associated_Course_Identifier__c,'+
                            'Reportable_Tuition_Fee__c,'+
                            'Fee_Exemption_Identifier__c,'+
                            'Enrolment__r.Specific_Program_Identifier__c,'+
                            'Enrolment__r.Training_Contract_Identifier__c,'+
                            'Purchasing_Contract_Schedule_Identifier__c,'+
                            'Hours_Attended__c,'+
                            'Course_Commencement_Date__c,'+
                            'Eligibility_Exemption__c,'+
                            'VET_Fee_Help__c,'+
                            'ANZSIC_Code__c,'+
                            'Enrolment__c,' +
                            'Full_Time_Learning__c,'+
                            'Start_Date__c,'+
                            'End_Date__c,'+
                            'Study_Reason_Identifier__c,'+
                            'Unit__r.name,'+
                            'Qualification_Identifier__c,'+
                            'Has_Been_Superseded__c,' +
                            'Enrolment__r.Qualification__r.Parent_Qualification_Code__c,'+
                            'Enrolment__r.Qualification_Identifier__c,'+
                            'Enrolment__r.Start_Date__c,'+
                            'Enrolment__r.End_Date__c,'+
                            'Unit__r.Unit_Flag_Identifier__c,'+
                            'Unit__r.Unit_Name__c,'+
                            'Student_Identifier__c,'+ 
                            'Enrolment__r.Qualification_Hours__c,'+
                            'Line_Item_Qualification_Unit__r.Nominal_Hours__c,'+
                            'Enrolment__r.Qualification__r.Parent_Qualification_Id__c,'+
                            'Enrolment__r.VET_FEE_HELP_Indicator__c,'+
                            'Enrolment__r.Victorian_Commencement_Date__c,'+
                            'Enrolment__r.Delivery_Location_State__c,'+
                            'Training_Delivery_Location__r.Training_Organisation_ABN__c,' +    
                            'Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c,' + 
                            'Supersession_Count__c,' +  
                            'Superseded_1_EU_ID__c,' +
                            'Superseded_1_Unit_ID__c,' +
                            'Superseded_1_EU_End_Date__c,' +
                            'Superseded_2_EU_End_Date__c,' +
                            'Superseded_2_EU_ID__c,' +
                            'Superseded_2_Unit_ID__c,' +
                            'Superseded_3_EU_End_Date__c,' +
                            'Superseded_3_EU_ID__c,' +
                            'Superseded_3_Unit_ID__c,' +
                            'Superseded_4_EU_End_Date__c,' +
                            'Superseded_4_EU_ID__c,' +
                            'Superseded_4_Unit_ID__c,' +
                            'Superseded_5_EU_End_Date__c,' +
                            'Superseded_5_EU_ID__c,' +
                            'Superseded_5_Unit_ID__c,' +
                            'Superseded_6_EU_End_Date__c,' +
                            'Superseded_6_EU_ID__c,' +
                            'Superseded_6_Unit_ID__c,' +
                            'Victorian_Tuition_Rate__c,' +
                            'Client_Fees_Other__c' +        
                             
                      ' FROM Enrolment_Unit__c WHERE Report_for_AVETMISS__c = true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(year.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(year.trim()) +')' + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\'' + ' ORDER BY Student_Identifier__c';
                      
                      
              //This is reduced query to output for NAT60 and 130. We're using less fileds to preserve heap size 
              // have had to add supersession fields here due to vic avetmiss requirements
             
             if(state != 'Victoria') {
                  queryStringUnitEnrolment = 'Select Id,' +
                             'Line_Item_Qualification_Unit__r.Nominal_Hours__c,'+
                             'Enrolment_Unit__c.Unit__c,'+
                             'Enrolment__c' +
                           
                            ' FROM Enrolment_Unit__c WHERE Report_for_AVETMISS__c = true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(year.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(year.trim()) +')' + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\'' + ' ORDER BY Student_Identifier__c';
                 }
                 
              if(state == 'Victoria') {
                  queryStringUnitEnrolment = 'Select Id,' +
                             'Line_Item_Qualification_Unit__r.Nominal_Hours__c,'+
                             'Enrolment_Unit__c.Unit__c,'+
                             'Enrolment__c,' +
                             'Enrolment__r.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c,' +
                             'Supersession_Count__c,' +  
                             'Superseded_1_Unit_SFID__c,' +
                             'Superseded_2_Unit_SFID__c,' +
                             'Superseded_3_Unit_SFID__c,' +
                             'Superseded_4_Unit_SFID__c,' +
                             'Superseded_5_Unit_SFID__c,' +
                             'Superseded_6_Unit_SFID__c' + 
                           
                            ' FROM Enrolment_Unit__c WHERE Report_for_AVETMISS__c = true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(year.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(year.trim()) +')' + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\'' + ' ORDER BY Student_Identifier__c';
                 }
    }
    
   
    
    /*Creates pre validation file structure */
    public void createFileStructure() {
        
        
        buildPurchasingContractString();
        createStudentQueryString();
        
        natFilesInfo = new List<NatViewer>();
        disabilities = new List<DisabilityObject>();
        priorAchievements = new List<PriorEducationalAchievmentObject>();
        List<String>splitter = new List<String>();
        
        
        exportDisallowed = true;
      
         //ALL STATES EXCEPT NEW SOUTH WALES
        natFilesInfo.add(New NatViewer('NAT00010 - Training Organisation', 1,'Ready For Validation', 0,0,0));
        try {TrainingOrgToValidate = [SELECT Id,Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account WHERE id = : rto];
              } catch(Exception e) {natFilesInfo.set(0, new NatViewer('NAT00010 - Training Organisation', 0,'Ready For Validation', 0,0,0));}
                    
         natFilesInfo.add(New NatViewer('NAT00020 - Training Delivery Locations', 0,'Ready For Validation', 0,0,0));
         natFilesInfo.add(New NatViewer('NAT00030 - Programs', 0,'Ready For Validation', 0,0,0));           
         natFilesInfo.add(New NatViewer('NAT00060 - Subjects', 0,'Ready For Validation', 0,0,0));                   
         natFilesInfo.add(New NatViewer('NAT00080 - Client', 0,'Ready For Validation', 0,0,0));        
         natFilesInfo.add(New NatViewer('NAT00085 - Client Postal Details', 0,'Ready For Validation', 0,0,0));          
         natFilesInfo.add(New NatViewer('NAT00090 - Disability', 0,'Ready For Validation', 0,0,0));          
         natFilesInfo.add(New NatViewer('NAT00100 - Client Prior Educational Achievement', 0,'Ready For Validation', 0,0,0));                  
         natFilesInfo.add(New NatViewer('NAT00120 - Enrolment',0, 'Ready For Validation', 0,0,0));        
         natFilesInfo.add(New NatViewer('NAT00130 - Program Completed', 0,'Ready For Validation', 0,0,0)); 
               
    }
    
    public pageReference validate() {
        
        String fname =  ApexPages.currentPage().getParameters().get('fname');
        Integer maxErrorSize = 100;
        Integer Counter = 0;
        
        Integer errorCount = 0; //Need to dissallow exporting if there are any errors
        
        if(fname == 'NAT00010 - Training Organisation') {
            if(NatValidator.validateNAT00010(TrainingOrgToValidate).NAT_Error_Code__c == null)
                natFilesInfo.set(0, new NatViewer('NAT00010 - Training Organisation', 1,'Success', 0,0,Limits.getCpuTime()));
            else
                natFilesInfo.set(0, new NatViewer('NAT00010 - Training Organisation', 1,'Fail', 0,1,Limits.getCpuTime()));
            
        }
        
        if(fname == 'NAT00020 - Training Delivery Locations') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00020(tempPur_codes, year, state,rto);
            LocationsErrorsLimitedView = new List<Locations__c>();
            TrainingOrgLocationsErrors = newOutCome.dynamicRecords;
                
            
            if(newOutcome.fileCount == 0) {
                natFilesInfo.set(1, new NatViewer('NAT00020 - Training Delivery Locations', 0,'Fail', 0,1,Limits.getCpuTime()));
            
            } else {
                
                
                if(TrainingOrgLocationsErrors.size() == 0)
                    natFilesInfo.set(1, new NatViewer('NAT00020 - Training Delivery Locations', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
                else {
                    natFilesInfo.set(1, new NatViewer('NAT00020 - Training Delivery Locations', newOutcome.fileCount,'Fail', 0,TrainingOrgLocationsErrors.size(),newOutCome.CPUTimeInMilliseconds));
                    
                }
           }
           
           for(Locations__c l : TrainingOrgLocationsErrors) {
                if(Counter < maxErrorSize) {
                  LocationsErrorsLimitedView.add(l);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00030 - Programs') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00030(tempPur_codes, year, '');
            QualificationsErrorsLimitedView = new List<Qualification__c>();
            QualificationsErrors = newOutcome.dynamicRecords;
            
            if(newOutcome.fileCount == 0) {
                natFilesInfo.set(2, new NatViewer('NAT00030 - Programs', 0,'Fail', 0,1,Limits.getCpuTime()));
            } else {
            
             if(QualificationsErrors.size() == 0)
                natFilesInfo.set(2, new NatViewer('NAT00030 - Programs', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
             else {
                natFilesInfo.set(2, new NatViewer('NAT00030 - Programs', newOutcome.fileCount,'Fail', 0,QualificationsErrors.size(),newOutCome.CPUTimeInMilliseconds));
             }
            
            
            }
            
            for(Qualification__c q : QualificationsErrors) {
                if(Counter < maxErrorSize) {
                  QualificationsErrorsLimitedView.add(q);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00060 - Subjects') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00060(tempPur_codes, year, '');
            UnitsOfComErrorsLimitedView = new List<Unit__c>();
            UnitsOfComErrors = newOutcome.dynamicRecords;
            
            if(newOutcome.fileCount == 0) 
                natFilesInfo.set(3,New NatViewer('NAT00060 - Subjects', 0,'Fail', 0,1,Limits.getCpuTime()));
            
            else {
                 
                 UnitsOfComErrors = newOutcome.dynamicRecords;
                
                 if(UnitsOfComErrors.size() == 0)
                    natFilesInfo.set(3,New NatViewer('NAT00060 - Subjects', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));//change back to Competency jcanlas 26MAR2013
                 else {
                    natFilesInfo.set(3,New NatViewer('NAT00060 - Subjects', newOutcome.fileCount,'Fail', 0,UnitsOfComErrors.size(),newOutCome.CPUTimeInMilliseconds));//change back to Competency jcanlas 26MAR2013
                 }
                
             }
            
            for(Unit__c u : UnitsOfComErrors) {
                if(Counter < maxErrorSize) {
                  UnitsOfComErrorsLimitedView.add(u);
                  Counter ++;
                }
            }    
         
           
        }
        
        if(fname == 'NAT00080 - Client') {
            
            // NATValidateOutCome newOutcome = NatValidator.validateNAT00080(tempPur_codes, year, '');
            NATValidateOutCome newOutcome = NatValidator.validateNAT00080(tempPur_codes, year, state);
            StudentsErrorsLimitedView = new List<Account>();
            StudentsErrors = newOutcome.dynamicRecords;
        
            if(StudentsErrors.size() == 0)
                natFilesInfo.set(4,New NatViewer('NAT00080 - Client', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
            else {
                natFilesInfo.set(4,New NatViewer('NAT00080 - Client', newOutcome.fileCount,'Fail', 0,StudentsErrors.size(),newOutCome.CPUTimeInMilliseconds));
            }
            
            for(Account st : StudentsErrors) {
                if(Counter < maxErrorSize) {
                  StudentsErrorsLimitedView.add(st);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00085 - Client Postal Details') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00085(tempPur_codes, year, '');
            StudentPostalErrorsLimitedView = new List<Account>();
            StudentPostalErrors = newOutcome.dynamicRecords;
           
            if(StudentPostalErrors.size() == 0)
                natFilesInfo.set(5,New NatViewer('NAT00085 - Client Postal Details', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
            else {
                natFilesInfo.set(5,New NatViewer('NAT00085 - Client Postal Details', newOutcome.fileCount,'Fail', 0,StudentPostalErrors.size(),newOutCome.CPUTimeInMilliseconds));
            }
            
            for(Account st : StudentPostalErrors) {
                if(Counter < maxErrorSize) {
                  StudentPostalErrorsLimitedView.add(st);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00090 - Disability') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00090(tempPur_codes, year, '');
            
            natFilesInfo.set(6,New NatViewer('NAT00090 - Disability', newOutcome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
        }
            
        if(fname == 'NAT00100 - Client Prior Educational Achievement') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00100(tempPur_codes, year, state);
            StudentsErrorsLimitedView100 = new List<Account>();
            StudentsErrors = newOutcome.dynamicRecords;
                        
            if(StudentsErrors.size() == 0)
                natFilesInfo.set(7,New NatViewer('NAT00100 - Client Prior Educational Achievement', newOutCome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
            else {
                natFilesInfo.set(7,New NatViewer('NAT00100 - Client Prior Educational Achievement', newOutCome.fileCount,'Fail', 0,StudentsErrors.size(),newOutCome.CPUTimeInMilliseconds));
            }
            
            for(Account st : StudentsErrors) {
                if(Counter < maxErrorSize) {
                  StudentsErrorsLimitedView100.add(st);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00120 - Enrolment') {
            
             EUErrorsLimitedView = new List<Enrolment_Unit__c>();
                          
             NATValidateOutCome newOutcome = NatValidator.validateNAT00120(tempPur_codes, year, state);
             
             System.debug('@@' + newOutcome);
                              
                if(newOutCome.fileCount == 0)
                   natFilesInfo.set(8, new NatViewer('NAT00120 - Enrolment',0, 'Fail', 0,1,Limits.getCpuTime()));
                else {
                
                
                    EUErrors = newOutCome.dynamicRecords;
                
                    if(EUErrors.size() == 0)
                        natFilesInfo.set(8, new NatViewer('NAT00120 - Enrolment',newOutcome.fileCount, 'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
                    else {
                        natFilesInfo.set(8, new NatViewer('NAT00120 - Enrolment',newOutcome.fileCount, 'Fail', 0,EUErrors.size(),newOutCome.CPUTimeInMilliseconds));
                         
                    }
                 
            
                }
             
            for(Enrolment_Unit__c eu : EUErrors) {
                if(Counter < maxErrorSize) {
                  EUErrorsLimitedView.add(eu);
                  Counter ++;
                }
            }  
               
                
        }
        
        if(fname == 'NAT00130 - Program Completed') {
            
            enrolmentErrorsLimitedView = new List<Enrolment__c>();

            NATValidateOutCome newOutcome = NatValidator.validateNAT00130(tempPur_codes, year, state);
            
            enrolmentsErrors = newOutCome.dynamicRecords;
            
                if(enrolmentsErrors.size() == 0)
                    natFilesInfo.set(9,New NatViewer('NAT00130 - Program Completed', newOutCome.fileCount,'Success', 0,0,newOutCome.CPUTimeInMilliseconds));
                else 
                    natFilesInfo.set(9,New NatViewer('NAT00130 - Program Completed', newOutCome.fileCount,'Fail', 0,enrolmentsErrors.size(),newOutCome.CPUTimeInMilliseconds));
                    
            for(Enrolment__c e : enrolmentsErrors) {
                if(Counter < maxErrorSize) {
                  enrolmentErrorsLimitedView.add(e);
                  Counter ++;
                }
            }

        }
            
       
        return null;
         
    }
    
    public pageReference generate() {
       
      createQueryString();
      String LibraryId;
      
      if(ApexPages.currentPage().getParameters().get('state') != 'New South Wales') {
        
         if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
            
        /*  ***Claiming Disabled***
            if(setClaimDate)
              ClaimGenerator.claimStandard(FilteredEU, enrolmentsToValidate, TrainingOrgToValidate, State);         
          */
          
          Database.executeBatch(new NATGeneratorBatchSmall('NAT00010',queryStringMain,state, TrainingOrgToValidate.Id), 2000);//NAT00010 - Batch class will call itself for NAT20 and NAT30 upon completion of previous NAT
          Database.executeBatch(new NATGeneratorBatchUnit('NAT00060',queryStringUnitEnrolment,state)); //NAT0060 - Batch class will call itself for NAT130 upon completion of previous NAT
          Database.executeBatch(new NATGeneratorBatchStudents(queryStringStudents,state),200); //NAT00080, NAT00085, NAT00090, NAT00100
          Database.executeBatch(new NATGeneratorBatch(queryStringMain,state),2000); //NAT00120
         
          
      }
        return new PageReference('/apex/NATFilesGenerating');
        
    }
    
    
    
     public void closePopup() {
        displayPopup = false;
    }
 
    public PageReference showPopup() {
        if(setClaimDate) {
            displayPopup = true;
            return null;
        }
        else {
             displayPopup = false;
             createFileStructure();
        
            return new PageReference('/apex/NATGeneratorValidation');
        }
        
        
    }
    
}