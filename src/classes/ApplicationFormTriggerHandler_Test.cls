/**
 * @description Test class for `ApplicationFormTriggerHandler`
 * @author Ranyel Maliwanag
 * @date 25.AUG.2015
 * @history
 *       17.NOV.2015    Ranyel Maliwanag    Included Applications Portal Central Settings initialisation in triggersetup
 */
@isTest
public with sharing class ApplicationFormTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {
        ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.initialiseFormComponents();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();

        Account student = SASTestUtilities.createStudentAccount();
        insert student;

        GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        studentLogin.AccountId__c = student.Id;
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        portalSettings.EffectivityDate__c = Date.today().addDays(1);
        portalSettings.LoanApplicationFormCreationOffset__c = 0;
        insert portalSettings;

        ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;
    }

    static testMethod void populateEmailFieldTest() {
        GuestLogin__c studentLogin = [SELECT Id FROM GuestLogin__c WHERE Email__c = 'triddle@hogwarts.edu'];
        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Test.startTest();
            ApplicationForm__c appForm = new ApplicationForm__c(
                    StudentCredentialsId__c = studentLogin.Id,
                    StateId__c = stateQLD.Id
                );
            System.assert(String.isBlank(appForm.StudentEmail__c) && String.isNotBlank(appForm.StudentCredentialsId__c));
            insert appForm;

            String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
            queryString += 'WHERE Id = \'' + appForm.Id + '\'';
            appForm = Database.query(queryString);

            System.assert(String.isNotBlank(appForm.StudentEmail__c));
            System.assert(String.isNotBlank(appForm.StudentAccountId__c));
        Test.stopTest();
    }

    static testMethod void createFormServiceCases() {

        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );

        Test.startTest();
            appForm.Status__c = 'Submitted';
            update appForm;
        Test.stopTest();
    }

    static testMethod void createImmediateLoanForms() {

        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );

        Test.startTest();
            appForm.Status__c = 'Submitted';
            appForm.LLN_Score_1__c = '3';
            appForm.LLN_Score_3__c = '5';
            appForm.LLN_End_Date__c = Date.today();
            appForm.SubmissionDate__c = Date.today().addDays(-7);
            update appForm;

            queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
            queryString += 'WHERE Id = \'' + appForm.Id + '\'';
            appForm = Database.query(queryString);

            System.assert(String.isBlank(appForm.LinkedLoanFormId__c));
        Test.stopTest();
    }

    static testMethod void createImmediateLoanFormsNegative() {

        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );

        Test.startTest();
            System.assert(String.isBlank(appForm.LinkedLoanFormId__c));
            
            appForm.Status__c = 'Submitted';
            appForm.LLN_Score_1__c = '3';
            appForm.LLN_Score_3__c = '2';
            appForm.LLN_End_Date__c = Date.today();
            appForm.SubmissionDate__c = Date.today().addDays(-7);
            update appForm;

            queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
            queryString += 'WHERE Id = \'' + appForm.Id + '\'';
            appForm = Database.query(queryString);

            System.assert(String.isBlank(appForm.LinkedLoanFormId__c));
        Test.stopTest();
    }

    static testMethod void populateLoanFormDateTest1() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();

        Test.startTest();
            //test portal setting days offset && expected date to 0am next day
            portalSettings.LoanApplicationFormCreationOffset__c = 2;
            update portalSettings;

            appForm.SubmissionDate__c = DateTime.newInstance(2055, 04, 06, 18, 0, 0);
            appForm.Status__c = 'Submitted';
            update appForm;

            ApplicationForm__c updatedForm = [Select EarliestLoanFormDate__c, ExpectedLoanFormDate__c from ApplicationForm__c WHERE Id =: appForm.Id limit 1];
            system.assertEquals(updatedForm.EarliestLoanFormDate__c, DateTime.newInstance(2055, 04, 08, 18, 0, 0));
            system.assertEquals(updatedForm.ExpectedLoanFormDate__c, DateTime.newInstance(2055, 04, 09, 0, 0, 1));
        Test.stopTest();
    }

    static testMethod void populateLoanFormDateTest2() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();

        Test.startTest();
            //test portal setting minites delay && expected date to 12pm same day
            portalSettings.MinutesDelayed__c = 30;
            update portalSettings;

            appForm.SubmissionDate__c = DateTime.newInstance(2015, 11, 24, 0, 0, 0);
            appForm.Status__c = 'Submitted';
            update appForm;

            ApplicationForm__c updatedForm = [Select EarliestLoanFormDate__c, ExpectedLoanFormDate__c from ApplicationForm__c WHERE Id =: appForm.Id limit 1];
            system.assertEquals(updatedForm.EarliestLoanFormDate__c, DateTime.newInstance(2015, 11, 24, 0, 30, 0));
            system.assertEquals(updatedForm.ExpectedLoanFormDate__c, DateTime.newInstance(2015, 11, 24, 12, 0, 0));
        Test.stopTest();
    }

    static testMethod void populateLoanFormDateTest3() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'LastName'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormId__c = appForm.Id,
                ApplicationFormItemId__c = formItem.Id,
                Answer__c = 'Hello'
            );
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];

        List<Holiday__c> holidays = new List<Holiday__c> {
            new Holiday__c(Name='1', Date__c = Date.newInstance(2055, 04, 07), StateId__c = stateQLD.Id),
            new Holiday__c(Name='1', Date__c = Date.newInstance(2055, 04, 08), StateId__c = stateQLD.Id),
            new Holiday__c(Name='2', Date__c = Date.newInstance(2055, 04, 09), StateId__c = stateQLD.Id),
            new Holiday__c(Name='2', Date__c = Date.newInstance(2055, 04, 12), StateId__c = stateQLD.Id),
            new Holiday__c(Name='3', Date__c = Date.newInstance(2055, 04, 15), StateId__c = stateQLD.Id)
        };
        insert holidays;

        Test.startTest();
            //test business hours and consecutive holidays
            portalSettings.LoanApplicationFormCreationOffset__c = 2;
            update portalSettings;

            appForm.SubmissionDate__c = DateTime.newInstance(2055, 04, 07, 20, 0, 0);
            appForm.Status__c = 'Submitted';
            update appForm;

            ApplicationForm__c updatedForm = [Select EarliestLoanFormDate__c, ExpectedLoanFormDate__c from ApplicationForm__c WHERE Id =: appForm.Id limit 1];
            system.assertEquals(updatedForm.EarliestLoanFormDate__c, DateTime.newInstance(2055, 04, 16, 20, 0, 0));
            system.assertEquals(updatedForm.ExpectedLoanFormDate__c, DateTime.newInstance(2055, 04, 19, 0, 0, 1));
        Test.stopTest();
    }

    static testMethod void createLoanFormsTest() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'WHERE RecordType.Name = \'Enrolment\' AND LinkedLoanFormId__c = null';
        ApplicationForm__c enrolmentForm = Database.query(queryString);

        ApplicationFormItem__c encryptedItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'TFN'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = encryptedItem.Id,
                ApplicationFormId__c = enrolmentForm.Id,
                Answer__c = '123456782'
            );
        insert answer;

        Test.startTest();
            ApplicationForm__c loanForm = new ApplicationForm__c(
                    StudentCredentialsId__c = enrolmentForm.StudentCredentialsId__c,
                    StateId__c = enrolmentForm.StateId__c,
                    QualificationId__c = enrolmentForm.QualificationId__c,
                    LinkedEnrolmentFormId__c = enrolmentForm.Id,
                    RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId()
                );
            insert loanForm;

            String queryStringLN = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.LoanForm__c);
            LoanForm__c loanDetails = Database.query(queryStringLN);

            System.assert(String.isNotBlank(loanDetails.TaxFileNumber__c));
        Test.stopTest();
    }

    // Too Short TFN
    static testMethod void createLoanFormsTest1() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'WHERE RecordType.Name = \'Enrolment\' AND LinkedLoanFormId__c = null';
        ApplicationForm__c enrolmentForm = Database.query(queryString);

        ApplicationFormItem__c encryptedItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'TFN'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = encryptedItem.Id,
                ApplicationFormId__c = enrolmentForm.Id,
                Answer__c = '123'
            );
        insert answer;

        Test.startTest();
            ApplicationForm__c loanForm = new ApplicationForm__c(
                    StudentCredentialsId__c = enrolmentForm.StudentCredentialsId__c,
                    StateId__c = enrolmentForm.StateId__c,
                    QualificationId__c = enrolmentForm.QualificationId__c,
                    LinkedEnrolmentFormId__c = enrolmentForm.Id,
                    RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId()
                );
            insert loanForm;

            String queryStringLN = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.LoanForm__c);
            LoanForm__c loanDetails = Database.query(queryStringLN);

            System.assert(String.isBlank(loanDetails.TaxFileNumber__c));
        Test.stopTest();
    }

    // Too Long TFN
    static testMethod void createLoanFormsTest2() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'WHERE RecordType.Name = \'Enrolment\' AND LinkedLoanFormId__c = null';
        ApplicationForm__c enrolmentForm = Database.query(queryString);

        ApplicationFormItem__c encryptedItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'TFN'];
        ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                ApplicationFormItemId__c = encryptedItem.Id,
                ApplicationFormId__c = enrolmentForm.Id,
                Answer__c = '12345678901020938571923'
            );
        insert answer;

        Test.startTest();
            ApplicationForm__c loanForm = new ApplicationForm__c(
                    StudentCredentialsId__c = enrolmentForm.StudentCredentialsId__c,
                    StateId__c = enrolmentForm.StateId__c,
                    QualificationId__c = enrolmentForm.QualificationId__c,
                    LinkedEnrolmentFormId__c = enrolmentForm.Id,
                    RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId()
                );
            insert loanForm;

            String queryStringLN = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.LoanForm__c);
            LoanForm__c loanDetails = Database.query(queryStringLN);

            System.assert(String.isBlank(loanDetails.TaxFileNumber__c));
        Test.stopTest();
    }


    static testMethod void milestoneCreationTest() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'WHERE RecordType.Name = \'Enrolment\' AND LinkedLoanFormId__c = null';
        ApplicationForm__c enrolmentForm = Database.query(queryString);

        Service_Cases__c serviceCase = new Service_Cases__c(
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        insert serviceCase;

        ApplicationForm__c loanForm = new ApplicationForm__c(
                StudentCredentialsId__c = enrolmentForm.StudentCredentialsId__c,
                StateId__c = enrolmentForm.StateId__c,
                QualificationId__c = enrolmentForm.QualificationId__c,
                LinkedEnrolmentFormId__c = enrolmentForm.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId(),
                ServiceCaseId__c = serviceCase.Id
            );
        insert loanForm;

        Test.startTest();
            loanForm.Status__c = 'Submitted';
            update loanForm;

            loanForm = [SELECT Id, (SELECT Id FROM EnrolmentApplicationMilestones__r) FROM ApplicationForm__c WHERE Id = :loanForm.Id];
            System.assert(!loanForm.EnrolmentApplicationMilestones__r.isEmpty());
        Test.stopTest();
    }
}