/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchCalendar implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchCalendar() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPCAL_Calendar_Seq__c, IPCAL_Description__c, IPCAL_End_Date__c, IPCAL_Start_Date__c, IPC_Sync_on_next_update__c, Name, Id ';
        queryString += 'FROM InPlace_Calendar__c ';
        queryString += 'WHERE IPC_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<InPlace_Calendar__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (InPlace_Calendar__c record : records) {
            if (!exportedIdentifiers.contains(record.IPCAL_Calendar_Seq__c)) {
                InPlace__c forExport = new InPlace__c(
                        IPCalendar_CalendarCode__c = record.Name,
                        IPCalendar_CalendarSeqNum__c = record.IPCAL_Calendar_Seq__c,
                        IPCalendar_Description__c = record.IPCAL_Description__c,
                        IPCalendar_EndDate__c = record.IPCAL_End_Date__c,
                        IPCalendar_StartDate__c = record.IPCAL_Start_Date__c
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPCAL_Calendar_Seq__c);
            }
            record.IPC_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchDiscipline nextBatch = new InPlaceExportBatchDiscipline();
        Database.executeBatch(nextBatch);
    }
}