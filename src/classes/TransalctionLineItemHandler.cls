public class TransalctionLineItemHandler {
    
    public void onInsert(List<Transaction_Line_Item__c> TLIlist) {
        countTLI(TLIlist);
    }

    public void onUpdate(List<Transaction_Line_Item__c> TLIlist, List<Transaction_Line_Item__c> oldTLIList) {
        countTLI(TLIlist, oldTLIList);
    }

    private void countTLI(List<Transaction_Line_Item__c> TLIlist) {

        List<Transaction_Payment__c> transPayToUpdate = new List<Transaction_Payment__c>();

        Set<Id> transPaySet = new Set<Id>();

        for(Transaction_Line_Item__c tli : TLIlist) {
            transPaySet.add(tli.Transaction_Payment__c);
        }

        for(Transaction_Payment__c tp : [SELECT Id, (SELECT Id FROM Transaction_Line_Items__r) FROM Transaction_Payment__c WHERE id in: transPaySet]) {            
            tp.TLI_Count__c = tp.Transaction_Line_Items__r.size();
            transPayToUpdate.add(tp);
        }  

        update transPayToUpdate;
    }

    private void countTLI(List<Transaction_Line_Item__c> TLIlist, List<Transaction_Line_Item__c> oldTLIList) {

        List<Transaction_Payment__c> transPayToUpdate = new List<Transaction_Payment__c>();

        Set<Id> transPaySet = new Set<Id>();

        for(Transaction_Line_Item__c tli : TLIlist) {
            transPaySet.add(tli.Transaction_Payment__c);
        }

        for(Transaction_Line_Item__c otli : oldTLIList) {
            transPaySet.add(otli.Transaction_Payment__c);
        }

        for(Transaction_Payment__c tp : [SELECT Id, (SELECT Id FROM Transaction_Line_Items__r) FROM Transaction_Payment__c WHERE id in: transPaySet]) {            
            tp.TLI_Count__c = tp.Transaction_Line_Items__r.size();
            transPayToUpdate.add(tp);
        }  

        update transPayToUpdate;
    }
}