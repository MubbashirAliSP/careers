global class NATGeneratorBatch00120 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    private String libraryId;
    private String currentstate;
    global String body120;
    global integer fileCount = 0;
    global boolean heapSizeExceeded = false;
    private String LogId;
    global DateTime currentDT;
    global NAT_Validation_Log__c natLog;
  
    Map<Id,String>exMap = new Map<Id,String>();
  
     
    global NATGeneratorBatch00120(String state, String q, String LId) {
        
        query = q;
        currentstate = state;
        LogId = LId;
        
        natLog = [ SELECT Id, Content_Library_Id__c, Log_Run_At__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];

        currentDT = natLog.Log_Run_At__c;
        
        
        for(Fee_Exemption__c fee : [Select Id, Enrolment__c, Fee_Exemption_Type__r.Code__c From Fee_Exemption__c Order By createdDate]) {
            exMap.put(fee.Enrolment__c,fee.Fee_Exemption_Type__r.Code__c);
        }
        
        
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body120 = '';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        if (currentState == 'Queensland') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionQLD.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if (currentState == 'New South Wales') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionNSW.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if (currentState == 'New South Wales APL') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionNSWAPL.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if (currentState == 'Australian Capital Territory') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionACT.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if(currentState == 'Victoria') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionVIC.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if (currentState == 'South Australia') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionSA.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if (currentState == 'Western Australia') {
            List<Enrolment_Unit__c> euWA = new List<Enrolment_Unit__c>();
            List<Id> enrolUnitId = new List<Id>();
            List<Id> enrolId = new List<Id>();
            String censusDate = '';
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                euWa.add(eu);
                enrolUnitId.add(eu.Id);
                enrolId.add(eu.Enrolment__c);
            }
            
            List<Intake_Students__c> waIntakeStu = [Select Id, Intake__r.Name, Enrolment__c FROM Intake_Students__c WHERE Enrolment__c in :enrolId];
            
            Map<Id,String> roleIdMap = new Map<Id,String>();

            for(Intake_Students__c is :waIntakeStu) {
                roleIdMap.put(is.Enrolment__c, is.Intake__r.Name);
            }

            List<Enrolment_Unit_Of_Study__c> euosWA = [ SELECT ID, Enrolment_Unit__c, Census_Date__c FROM Enrolment_Unit_Of_Study__c WHERE Enrolment_Unit__c in :enrolUnitId ];
            
            Map<Id,Date> censusDateMap = new Map<Id,Date>();
            
            for(Enrolment_Unit_Of_Study__c euos :euosWA) {
                censusDateMap.put(euos.Enrolment_Unit__c, euos.Census_Date__c);
            }
                
            for(Enrolment_Unit__c eu :euWA) {
                if(censusDateMap.get(eu.Id) == null) {
                    censusDate = 'null,';
                }
                else {
                    censusDate = String.valueOf(censusDateMap.get(eu.Id)) + ',';
                }
                if (roleIdMap.get(eu.Enrolment__c) == null ) {
                    censusDate += 'null';
                }
                else{
                    censusDate += roleIdMap.get(eu.Enrolment__c);
                }
                body120 += NATConstructor.natFile(eu, NATDataDefinitionWA.nat00120(), censusDate);
                body120 += '\r\n';
            }
        }
        
        else if(currentState == 'Northern Territory') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionNT.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if(currentState == 'Tasmania') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionTAS.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
        
        else if(currentState == 'National') {
            for(SObject s :scope) { 
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                String feeExemption = exMap.get(eu.Enrolment__c) + ':' + currentState;
                body120 += NATConstructor.natFile(eu, NATDataDefinitionTAS.nat00120(), feeExemption);
                body120 += '\r\n';
            }
        }
    

    if(Limits.getHeapSize() > 10000000) { //10Mb aprox 70k records, limit is 12Mb but need some space for batch in progress
            heapSizeExceeded = true;
            fileCount++;
            Blob pBlob = Blob.valueof(body120);
            insert new ContentVersion(
                versionData =  pBlob,
                Title = 'NAT00120_' + fileCount ,
                PathOnClient = '/NAT00120_' + fileCount + '.txt',
                FirstPublishLocationId = natLog.Content_Library_ID__c
            );
            body120 = ''; 
        }
        
    }

    global void finish(Database.BatchableContext BC) {
        
        
        if(body120 == '') {
            body120 = ' ';
        }
        
        if(heapSizeExceeded) {
            fileCount++;
            Blob pBlob = Blob.valueof(body120);
            insert new ContentVersion(
                versionData =  pBlob,
                Title = 'NAT00120_' + fileCount ,
                PathOnClient = '/NAT00120_' + fileCount + '.txt',
                FirstPublishLocationId = natLog.Content_Library_ID__c
            );
            
            //windows .bat executable to combine files into one NAT00120.txt
            String batchFileS = 'TYPE NAT00120_*.txt >> NAT00120.txt' + '\r\n' + 'DEL NAT00120_*.txt' + '\r\n' + 'DEL Combine_NAT00120.bat';
            Blob batchFile = Blob.valueof(batchFileS);
            insert new ContentVersion(
                versionData =  batchFile,
                Title = 'Combine NAT00120',
                PathOnClient = '/Combine_NAT00120.bat',
                FirstPublishLocationId = natLog.Content_Library_ID__c
            );
        }
        
        else { //if heap not reached, output one file and no executable
            Blob pBlob = Blob.valueof(body120);
            insert new ContentVersion(
                versionData =  pBlob,
                Title = 'NAT00120',
                PathOnClient = '/NAT00120.txt',
                FirstPublishLocationId = natLog.Content_Library_ID__c
            );
        }
        
        Database.executeBatch(new NATGeneratorBatch00130( natLog.Validation_State__c, natLog.Query_Main__c, natLog.Id  ) );
    }
}