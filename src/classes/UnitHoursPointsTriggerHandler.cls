/** 
 * @description Unit Hours and Points Trigger Handler 
 * @author Janella Lauren Canlas
 * @date 05.DEC.2012
 *
 * HISTORY
 * - 05.DEC.2012    Janella Canlas - Created.
 * - 04.JUL.2013    Eu Cadag - Optimization of codes.
 */
public with sharing class UnitHoursPointsTriggerHandler {
    /*
        This method will update the Scheduled Hours of the related Enrolment Units
    */
    /*************************** OLD Codes Start here By Eu************************************************/
    /*public static void updateEnrolmentUnit(Set<Id> unitIds,List<Unit_Hours_and_Points__c> uhpList){
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
        //query Enrolment Unit records
        euList = [select id, Unit__c, Scheduled_Hours__c, Training_Delivery_Location__r.State_Lookup__c
                    From Enrolment_Unit__c where Unit__c IN: unitIds];
        //populate the Enrolment Unit Scheduled Hours           
        for(Unit_Hours_and_Points__c u : uhpList){
            for(Enrolment_Unit__c eu : euList){
                if(u.State__c == eu.Training_Delivery_Location__r.State_Lookup__c){
                    eu.Scheduled_Hours__c = u.Nominal_Hours__c;
                }
            }
        }
        //update the Enrolment Unit records
        try{update euList;}catch(Exception e){System.debug('Error: '+ e.getMessage());}
    }*/
    /*************************** OLD Codes End here************************************************/
    /*************************** Optimized Codes Start here By Eu************************************************/
    @Future
    public static void updateEnrolmentUnitLimit(Set<Id> unitIds,Set<Id> IdUnits){
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
        List<Unit_Hours_and_Points__c> RetrievediusList = new List<Unit_Hours_and_Points__c>();
        //query Enrolment Unit records
        euList = [select id, Unit__c, Scheduled_Hours__c, Training_Delivery_Location__r.State_Lookup__c
                    From Enrolment_Unit__c where Unit__c IN: unitIds];
        RetrievediusList = [SELECT ID, State__c, Nominal_Hours__c FROM Unit_Hours_and_Points__c WHERE ID IN: IdUnits];
        //populate the Enrolment Unit Scheduled Hours           
        for(Unit_Hours_and_Points__c u : RetrievediusList){
            for(Enrolment_Unit__c eu : euList){
                if(u.State__c == eu.Training_Delivery_Location__r.State_Lookup__c){
                    eu.Scheduled_Hours__c = u.Nominal_Hours__c;
                }
            }
        }
        //update the Enrolment Unit records
        try{update euList;}catch(Exception e){System.debug('Error: '+ e.getMessage());}
    }
    /*************************** Optimized Codes End here By Eu************************************************/
    /*
        This method will update the Intake Unit of Study Records related
    */
    /*************************** OLD Codes Start here By Eu************************************************/
    /*public static void updateIntakeUnitOfStudy(Set<Id> unitIds,List<Unit_Hours_and_Points__c> uhpList){
        List<Intake_Unit_of_Study__c> iusList = new List<Intake_Unit_of_Study__c>();
        //query IUoS records
        iusList = [select id, Unit__c, Scheduled_Hours__c, Unit__r.Parent_UoC__c from Intake_Unit_of_Study__c where Unit__r.Parent_UoC__c IN: unitIds];
        //populate IUoS Scheduled Hours
        for(Intake_Unit_of_Study__c i : iusList){
            for(Unit_Hours_and_Points__c u  : uhpList){
                if(i.Unit__r.Parent_UoC__c == u.Unit__c){
                    if(u.Institutional_Hours__c != null){
                        i.Scheduled_Hours__c = u.Institutional_Hours__c;
                    }
                    else{
                        i.Scheduled_Hours__c = u.Nominal_Hours__c;  
                    }
                }
            }
        }
        //update the IUoS records
        try{update iusList;}catch(Exception e){system.debug('MESSAGE: ' + e.getMessage());}
    }*/
    /*************************** OLD Codes End here By Eu************************************************/
  /*************************** Optimized Codes Start here By Eu************************************************/
  @Future
  public static void updateIntakeUnitOfStudyLimit(Set<Id> unitIds,Set<Id> IdUnits){
    List<Intake_Unit_of_Study__c> iusList = new List<Intake_Unit_of_Study__c>();
    List<Unit_Hours_and_Points__c> RetrievediusList = new List<Unit_Hours_and_Points__c>();
    //query IUoS records
    iusList = [select id, Unit__c, Scheduled_Hours__c, Unit__r.Parent_UoC__c from Intake_Unit_of_Study__c where Unit__r.Parent_UoC__c IN: unitIds];
    RetrievediusList = [SELECT ID, Unit__c, Institutional_Hours__c FROM Unit_Hours_and_Points__c WHERE ID IN: IdUnits];
    //populate IUoS Scheduled Hours
    for(Intake_Unit_of_Study__c i : iusList){
      for(Unit_Hours_and_Points__c u   : RetrievediusList){
        if(i.Unit__r.Parent_UoC__c == u.Unit__c){
          if(u.Institutional_Hours__c != null){
            i.Scheduled_Hours__c = u.Institutional_Hours__c;
          }
          else{
            i.Scheduled_Hours__c = u.Nominal_Hours__c;  
          }
        }
      }
    }
    //update the IUoS records
    try{update iusList;}catch(Exception e){system.debug('MESSAGE: ' + e.getMessage());}
  }
  /*************************** Optimized Codes End here By Eu************************************************/
}