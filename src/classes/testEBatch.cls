@isTest (SeeAllData=true)
public class testEBatch{
    static testMethod void testEBatch1(){
    
    string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
    User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        system.runas(u) {
        
           
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c(); 
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        
        //System.runAs(u) {
            
        Test.startTest();
        
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            state = TestCoverageUtilityClass.createState(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
            qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() + 365;
            enrl.Delivery_Location__c = loc.id;
            //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation start
            //enrl.Enrolment_Status__c = 'Completed'; 
            enrl.Enrolment_Status__c = 'Active (Commencement)';
            //MAM 04/08/2014 end 
            
            insert enrl;
                        
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            eus.Census_Date__c = system.today();
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;
            
      
    
        String query =  'Select Id, ' +
                                     'Student__r.Student_Identifer__c, ' +
                                     'Qualification__r.Award_based_on__r.Name, ' +
                                     'CHESSN2__c, ' +
                                     'Reporting_Year_Period__c, ' +
                                     'Variation_Reason_Code__r.Code__c, ' +
                                     'Name, ' +
                                     'Disability_Code__c, ' +
                                     'Basis_for_Admission__r.Code__c, ' +
                                     'Type_of_Attendance__r.Code__c, ' +
                                     'Student__r.Sex_Identifier__c, ' +
                                     'Aboriginal_and_Torres_Strait_Is_Status__r.Code__c, ' +
                                     'Student__r.Main_Language_Spoken_at_Home__r.Code__c, ' +
                                     'Field_of_Ed_of_prior_VET_Credit_RPL_code__c, ' +
                                     'Level_of_prior_study_for_credit_RPL_code__c, ' +
                                     'Student__r.Country_of_Birth__r.Country_Code__c, ' +
                                     'Type_of_provider_where_study_done_code__c, ' +
                                     'Credit_Offered_Value_code__c, ' +
                                     'Credit_status_Higher_Ed_provider_code__c, ' +
                                     'Reason_for_Study__r.Code__c, ' +
                                     'Labour_Force_Status__r.Code__c, ' +
                                     'Highest_Educational_Participation__r.Code__c, ' +
                                     'Credit_Used_Value_code__c, ' +
                                     'RPL_details_of_prior_study_code__c, ' +
                                     'Mailing_City__c, ' +
                                     'Student__r.PersonBirthDate, ' +
                                     'Location_code_of_perm_residence__c, ' +
                                     'Year_of_arrival_in_Australia__c, ' +
                                     'Location_code_of_term_residence__c, ' +
                                     'Commencing_location_code_of_perm_home__c, ' +
                                     'Overseas_Country__c, ' +
                                     'Mailing_Country__c, ' +
                                     'Does_student_have_a_disability__c, ' +
                                     'Indicate_Areas_of_Imparement__c, ' +
                                     'Requested_advice_on_support_services__c, ' +
                                     'Student__r.Arrival_Status_Identifer__c, ' +
                                     'Student__r.HEIMS_347_Year_of_arrival_in_Australia_1__c, ' +
                                     'Domestic_School_Leaver__c, ' +
                                     'Tax_File_Number__c, ' +
                                     'Student__r.PersonTitle, ' +
                                     'Student__r.FirstName, ' +
                                     'Student__r.LastName, ' +
                                     'Mailing_Street__c, ' +
                                     'Mailing_State_Provience__c, ' +
                                     'Mailing_Zip_Postal_Code__c, ' +
                                     'Other_Country__c, ' +
                                     'Name_of_suburb_town_locality__c, ' +
                                     'Post_code_of_Year_12_Perm_home_residence__c ' +                                    
                                     'FROM Enrolment__c WHERE Id = \'' + enrl.Id + '\'';
                              
                            
                              
        ExportAllEnrolment eBatch = new ExportAllEnrolment(query);
             
        //Call and Execute the batch job
        Database.executeBatch(eBatch);
        
        Test.stopTest();
        
      }
        

    }
}