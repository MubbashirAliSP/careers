/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for GenerateClassRoll_CC Class
Test Class:
History
19/12/2012     Created                        Sairah Hadjinoor
12/02/2014     Added New Test Method          Bayani Cruz
04/08/2014     Change Status to Active to pass Enrolment Validation - Michelle Magsarili
------------------------------------------------------------*/
@isTest(seeAllData=true)
private class GenerateClassRoll_CC_Test {

    static testMethod void testGenerateClassRoll_1() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
                    //insert u;
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Results__c res = new Results__c();
        Language__c lang = new Language__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            lang = TestCoverageUtilityClass.createLanguage();
            acc = TestCoverageUtilityClass.createStudent();
            acc2 = TestCoverageUtilityClass.createStudentEUOS(lang.Id);
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            insert enrl;
            enrl2 = TestCoverageUtilityClass.createEnrolment(acc2.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl2.Predominant_Delivery_Mode__c = del.Id;
            enrl2.Type_of_Attendance__c = cattend.Id;
            enrl2.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            insert enrl2;
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            system.debug('***INTAKE' + intake.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            //enrlUnit = [SELECT id from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id); insert enrlIntakeUnit;
            enrlIntakeUnit2 = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl2.Id, intakeUnit.Id, enrlUnit.Id); insert enrlIntakeUnit2;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id); insert staff;
            Staff_Member__c staf = [SELECT Id, Name, First_Name__c, Last_Name__c, Trainer__c, Trainer__r.Name from Staff_Member__c WHERE Intake__c =: intake.Id];
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id); insert clEnrolment;
            clEnrolment2 = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit2.Id, attend.Id); insert clEnrolment2;
            
            PageReference testPage = Page.GenerateClassRoll;
            testPage.getParameters().put('intakeId', intake.Id);
            Test.setCurrentPage(testPage);
            
            GenerateClassRoll_CC gen = new GenerateClassRoll_CC();
            gen.DateTo = '07/06/2013';
            gen.DateFrom = '07/01/2013';
            //gen.getIntakeRecord();
            //gen.getStaffPicklist();
            //gen.getClassList();
            //gen.getclsEnrolList();
            //gen.gettoDateTime();
            //gen.getfromDateTime();
            //gen.gettoDate();
            //gen.getfromDate();
            //gen.cancel();
            gen.generate();
            
            //delete clEnrolment;
            test.stopTest();
        }
    }
    static testMethod void testGenerateClassRoll_2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Enrolment__c enrl = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Results__c res = new Results__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            insert enrl;
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            //enrlUnit = [SELECT id from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id); insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id);
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            
            Classes__c clas2 = new Classes__c();
            clas2.Intake__c = intake.Id;
            clas2.Intake_Unit__c = intakeUnit.Id;
            clas2.Start_Date_Time__c = dateTime.newInstance(2013,2,12);
            clas2.End_Date_Time__c = dateTime.newInstance(2013,2,13);
            insert clas2;
            
            List<Classes__c >clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            
            clas[0].Intake_Unit__c = intakeUnit.Id;
            clas[0].Start_Date_Time__c = dateTime.newInstance(2013,2,11);
            clas[0].End_Date_Time__c = dateTime.newInstance(2013,2,13);
            update clas[0];
            
            PageReference testPage = Page.GenerateClassRoll;
            testPage.getParameters().put('intakeId', intake.Id);
            Test.setCurrentPage(testPage);
            
            GenerateClassRoll_CC gen = new GenerateClassRoll_CC();
            gen.DateTo = '13/02/2013';
            gen.DateFrom = '10/02/2013';
            //gen.fromDateTime = dateTime.newInstance(2013, 12, 02);
            List<Classes__c>classList = [Select Unit_Name__c, Unit_Code__c, Start_Date_Time__c, Room__c, Room_Address__c, Name,
                                            Intake__c, Intake_Unit__c, Id, End_Date_Time__c, Class_Configuration__c
                                            From Classes__c WHERE Intake__c =: intake.Id
                                            AND Start_Date_Time__c >=: date.newInstance(2013,2,12) AND End_Date_Time__c <=: date.newInstance(2013,2,13)
                                        ];
            system.debug('*****classList: ' + classList.size());                                        
            gen.getIntakeRecord();
            gen.getStaffPicklist();
            gen.getClassList();
            gen.getclsEnrolList();
            gen.gettoDateTime();
            gen.getfromDateTime();
            gen.gettoDate();
            gen.getfromDate();
            gen.cancel();
            gen.generate();
            
            test.stopTest();
        }
    }
    static testMethod void testGenerateClassRoll_3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Staff_Member__c staff2 = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Results__c res = new Results__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            //acc2 = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            insert enrl;
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
            system.debug('***INTAKE'+intake.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id); insert staff;
            staff2 = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id); insert staff2;
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            //enrlUnit = [SELECT id from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id); insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            system.debug('****clas: '+clas.size());
            integer i = 0;
            for(Classes__c c : clas){
                i = i + 5;
                c.Intake_Unit__c = intakeUnit.Id;
                c.Start_Date_Time__c = date.today().addDays(i+30);
                c.End_Date_Time__c = date.today().addMonths(i);
            }
            update clas;
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas[0].Id, enrlIntakeUnit.Id, attend.Id); insert clEnrolment;
            PageReference testPage = Page.GenerateClassRoll;
            testPage.getParameters().put('intakeId', intake.Id);
            Test.setCurrentPage(testPage);
            
            GenerateClassRoll_CC gen = new GenerateClassRoll_CC();
            gen.DateFrom = '02/06/2013';
            gen.DateTo = '04/01/2013';
            gen.fromDate = date.today();
            gen.toDate = date.today() - 10; 
            gen.getIntakeRecord();
            gen.getStaffPicklist();
            gen.getClassList();
            gen.getclsEnrolList();
            gen.gettoDateTime();
            gen.getfromDateTime();
            gen.gettoDate();
            gen.getfromDate();
            gen.cancel();
            gen.generate();
            
            test.stopTest();
        }
    }

    static testMethod void testGenerateClassRoll_4() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='Australia/Brisbane', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Staff_Member__c staff2 = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Results__c res = new Results__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            //acc2 = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            insert enrl;
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
            system.debug('***INTAKE'+intake.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id); insert staff;
            staff2 = TestCoverageUtilityClass.createStaffMember(intake.Id, u.Id); insert staff2;
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            //enrlUnit = [SELECT id from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id); insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            system.debug('****clas: '+clas.size());
            integer i = 0;
            for(Classes__c c : clas){
                i = i + 5;
                c.Intake_Unit__c = intakeUnit.Id;
                c.Start_Date_Time__c = date.today().addDays(i+30);
                c.End_Date_Time__c = date.today().addMonths(i);
            }
            update clas;
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas[0].Id, enrlIntakeUnit.Id, attend.Id); insert clEnrolment;
            PageReference testPage = Page.GenerateClassRoll;
            testPage.getParameters().put('intakeId', intake.Id);
            Test.setCurrentPage(testPage);
            /*
            GenerateClassRoll_CC gen = new GenerateClassRoll_CC();
            gen.DateFrom = '10/02/2014';
            gen.DateTo = '14/02/2014';
            gen.fromDateTime = datetime.now() - 1;
            gen.toDateTime = datetime.newInstanceGmt(2014, 2,11,20,30,0);
            System.debug('####' +gen.fromDatetime);
            System.debug('####' +gen.toDateTime.format('EEEE'));
            gen.generate();
            
            test.stopTest(); */
            
            GenerateClassRoll_CC gen = new GenerateClassRoll_CC();
            gen.DateFrom = '10/02/2014';
            gen.DateTo = '14/02/2014';
            gen.fromDate = date.today() - 2;
            gen.toDate = date.today() + 2; 
            gen.getIntakeRecord();
            gen.getStaffPicklist();
            gen.getClassList();
            gen.getclsEnrolList();
            gen.gettoDateTime();
            gen.getfromDateTime();
            gen.gettoDate();
            gen.getfromDate();
            gen.cancel();
            gen.generate();
            
            test.stopTest();
        }
    }
   
}