public with sharing class MyASMController {
    
    @AuraEnabled
    public static Account getMyAccountDetails() {
        Account a = [select FirstName,
                     LastName,
					 Learning_Tab__c,
                     PersonEmail,
                     PersonMobilePhone,
                     PersonMailingStreet,
                     PersonMailingCity,
                     PersonMailingPostalCode,
                     PersonMailingState,
                     PersonOtherStreet,
                     PersonOtherCity,
                     PersonOtherPostalCode,
                     PersonOtherState,
                     Lecturers__c,
                     MKP002__Photo_Id__c,
                     ASM_Course__c 
                     from Account where Id = '001O000000yc46M'];
        // a.Id = null;
        return a;
    }
    
    @AuraEnabled
    public static List<String> getFeedbackTypes() {
        return new String[]{'Please select', 'Technology Issues', 'Compliant', 'Feedback'};
    }
    
    @AuraEnabled
    public static boolean saveFeedback(String feedbackType, String subject, String message) {
        return true;
    }

}