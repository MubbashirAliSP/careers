/*BlackBoard User Integration Class only
 *Contains Save method, to create/update BB users using callouts
 *Contains Get method, to retrieve BB user primary key
 *Please Refer to method comments for more details
 *written by YG, CloudSherpas 1st August 2014
 */


global class BlackboardUserWS {

    /*Creates and Updates blackboard user in Blackboard */
    public static HTTPResponse saveUser(Blackboard_User__c bbuser) {

        List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
         
         String SessionId = log.get(0).Session_Id__c;
         
         Id logId = log.get(0).Id;
         
         String syncStatus = '';
         
         String Message = '';
         
         String userXML = BlackBoardXMLBuilder.buildUserXML(bbuser);
         
         String blackboardPrimaryKey = ''; 
         
         String SoapXMLBody ='';

          SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://user.ws.blackboard" xmlns:xsd="http://user.ws.blackboard/xsd">' +
      
          '  <soapenv:Header>'+
          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                '  <wsu:Timestamp wsu:Id="TS-24">'+
                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                    
                '  </wsu:Timestamp>'+
                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                '    <wsse:Username>session</wsse:Username>'+
                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                    
                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
    
        
        
      
        
        ' <soapenv:Body> ' +
          
          '  <user:saveUser>'+
          
                 userXML +
          
          '  </user:saveUser>' +
        
        
        ' </soapenv:Body> ' +
      
      '</soapenv:Envelope>';

        string SoapXML;
        SoapXML = SoapXMLBody;
        Integer ContentLength = 0;
        ContentLength = SoapXML.length();
            
        Http h = new Http();

        HttpRequest req = new HttpRequest();

        HttpResponse res = new HttpResponse();
      

        req.setMethod('POST');

        req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).User_WS_Endpoint__c); 

        req.setHeader('Content-type','text/xml');
        req.setHeader('Content-Length',ContentLength.format());

        
        req.setHeader('SoapAction','saveUser');
    
        req.setBody(SoapXML);        
      
        if(Test.isRunningTest())
            res.setBody('<return>_1234</return>');
        else
            res = h.send(req);

         

        XmlStreamReader reader = res.getXmlStreamReader();
          
        while(reader.hasNext()) {
               //  Start at the beginning of the book and make sure that it is a book
               if (reader.getEventType() == XmlTag.START_ELEMENT) {
                  if ('return' == reader.getLocalName()) {
                     blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                     syncStatus = 'Success';
                     Message = 'Record Synced';           
                     break;
                  }
                  
                  if('faultstring' == reader.getLocalName()) {
                    syncStatus = 'Fail';
                    Message = BlackboardUtility.parseResult(reader);
                    break;
                  }
               }
              reader.next(); 
        }
        


        if(SyncStatus == 'Success' && blackboardPrimaryKey != '') {
          
              bbuser.Blackboard_Primary_Key__c = blackboardPrimaryKey;
              bbuser.Date_of_Last_Sync__c = System.Now();
              bbuser.Changed_Since_Last_Sync__c  = false; 
              bbuser.Available_In_BlackBoard__c = bbuser.Available_After_Sync__c;
              
              
          }/*write back to master unit */
            try {
                update bbuser;
            } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
              
         
          
          BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bbuser.Id,syncStatus, 'SaveUser'); //create log event for each record processed

      return res;


    }
    
    /*Performs login, for get the get operation */
    WebService static void singleGetOperation(string recordId,String oType) {
        
        

        HttpResponse initResponse; //initialize response
        HttpResponse loginReponse; //login response

      
        String sessionId = '';
        String result='';

        String syncStatus = '';
        String Message = '';
    
        BlackboardcontextWS contextWS = new BlackboardcontextWS();

         if(!Test.isRunningTest()) {


          initResponse = contextWS.initialize();
    
        

    
        XmlStreamReader reader = initResponse.getXmlStreamReader();

         while(reader.hasNext()) {
             //  Start at the beginning of the book and make sure that it is a book
             if (reader.getEventType() == XmlTag.START_ELEMENT) {

                if ('return' == reader.getLocalName()) {
                   sessionId = BlackboardUtility.parseResult(reader);             
                   break;
                }
             }
                reader.next();

           }
           
        
       
           loginReponse = contextWS.login(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).userid__c, Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).password__c, 'YG','YG','', Integer.ValueOf(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).expectedLifeSeconds__c), sessionId);

         }
         
       
        if(oType == 'U')
          getSingleBlackboardUser(recordId, sessionId);

        if(oType == 'IR')
          getSingleInstitutionalRole(recordId, sessionId);
       

     }
    
   /*Makes callout to blackboard, returns primary key and sets it to Blackboard Role record 
    *which is a type of Institutional role
    *Login to blackboard is performed in singleGetOperation method */
   @future(callout=true)
    public static void getSingleInstitutionalRole(String bbrid, String sessionId) {

      Blackboard_Role__c bbrole = [Select name,Role_Id__c,Role_Validated__c From Blackboard_Role__c Where id = : bbrid LIMIT 1];

      String syncStatus = '';

      String SoapXMLBody ='';
    
      String Message = '';
    
      String blackboardPrimaryKey = '';

        SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://user.ws.blackboard" xmlns:xsd="http://user.ws.blackboard/xsd">' +
      
          '  <soapenv:Header>'+
          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                '  <wsu:Timestamp wsu:Id="TS-24">'+
                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                    
                '  </wsu:Timestamp>'+
                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                '    <wsse:Username>session</wsse:Username>'+
                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                    
                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+

                '<soapenv:Body>'+
                ' <user:getInstitutionRoles>'+
                '  </user:getInstitutionRoles>'+
                ' </soapenv:Body>'+
         ' </soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).User_WS_Endpoint__c); 
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getInstitutionRoles');
               req.setBody(SoapXML); 
                
               if(Test.isRunningTest())
                res.setBody('<return>_1234</return>');
               else
                 res = h.send(req);
            

               XmlStreamReader reader2 = res.getXmlStreamReader();

              

               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('roleName' == reader2.getLocalName()) {
                              if(bbrole.Role_Id__c  == BlackboardUtility.parseResult(reader2)){
                           syncStatus = 'Success';
                           Message = 'Record Synced';
                              
                           break;
                          }
                        }
                      
                    }
                    reader2.next();
               }


          if(syncStatus == 'Success') 
            bbrole.Role_Validated__c = true;
          else
              bbrole.Role_Validated__c = false;
            

          update bbrole; 
           
           
       BlackboardUtility.createBlackBoardLog(sessionId);
       List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
       BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbrole.Id,syncStatus, 'getInsitutionalRoles'); //create log event for each record processed

    }
    
     /*Makes callout to blackboard, returns primary key and sets it to Blackboard User record
      *Login to blackboard is performed in singleGetOperation method */
     @future(callout=true)
     public static void getSingleBlackboardUser(string bbuId, String sessionId) {
        
        
        Blackboard_User__c bbu = [Select name,Blackboard_username__c, Blackboard_Primary_Key__c From Blackboard_User__c Where id = : bbuId];
        String syncStatus = '';
        
        String Message = '';

        String SoapXMLBody ='';

        String blackboardPrimaryKey = '';
      
             SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://user.ws.blackboard" xmlns:xsd="http://user.ws.blackboard/xsd">' +
      
          '  <soapenv:Header>'+
          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                '  <wsu:Timestamp wsu:Id="TS-24">'+
                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                    
                '  </wsu:Timestamp>'+
                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                '    <wsse:Username>session</wsse:Username>'+
                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                    
                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                          
                        
                          
                '  <soapenv:Body>'+
                                '      <user:getUser>'+
                                '         <user:filter>'+
                                '           <xsd:available>True</xsd:available>'+
                                '           <xsd:filterType>6</xsd:filterType>'+
                                '           <xsd:name>'+bbu.Blackboard_Username__c +'</xsd:name>'+
                                '           </user:filter>'+
                                '      </user:getUser>'+
                                '   </soapenv:Body>'+
                        
                        '</soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).User_WS_Endpoint__c); 
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getUser');
               req.setBody(SoapXML); 
              

               if(Test.isRunningTest())
                  res.setBody('<id>_1234</id>');
                else
                  res = h.send(req);
              
               XmlStreamReader reader2 = res.getXmlStreamReader();
                
               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('id' == reader2.getLocalName()) {
                           blackboardPrimaryKey  = BlackboardUtility.parseResult(reader2);
                           syncStatus = 'Success';
                           Message = 'Record Synced';
                           break;
                        }
                        
                        if('faultstring' == reader2.getLocalName()) {
                          syncStatus = 'Fail';
                          Message = BlackboardUtility.parseResult(reader2);
                        
                          break;
                        }
                    }
                    reader2.next();
               }


          if(syncStatus == 'Success' && blackboardPrimaryKey != '') {
            bbu.Blackboard_Primary_Key__c = blackboardPrimaryKey;
            update bbu;

              
           }
           
          BlackboardUtility.createBlackBoardLog(sessionId);
          List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
           
          BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbu.Id,syncStatus, 'getUser'); //create log event for each record processed


     }
    
}