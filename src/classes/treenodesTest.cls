/**
 * @description
 * @author
 * @date
 * @history
 *       24.SEP.2015    Ranyel Maliwanag    Updated creation of Qualification Unit records to pass validation rule requirements
 */
@isTest(seeAlldata=true)
public class treenodesTest {
    private static List<Unit__c> unitList;
    private static void init(){
        Training_Package__c tp = new Training_Package__c(Name='Test1', Training_Package_Name__c = 'Training Package Name Test' );
        insert tp;
        
        Training_Package__c tp2 = new Training_Package__c(Name='Test2', Training_Package_Name__c = 'Training Package Name Test2' );
        insert tp2;
        
               
        
        Qualification__c masterObject = new Qualification__c(Name='TestCode1', Qualification_Name__c = 'Test Qualification', Description__c = 'TEST', Training_Package_Order__c=1, Training_Package__c = tp.Id);
        insert masterObject;
        
        unitList = new List<Unit__c>();
        for (Integer i = 0; i<19; i++){
            unitList.add(new Unit__c(Name = 'Test Unit ' + i));
        }
        
        insert unitList;

        for (Unit__c unit : unitList) {
            TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, null);
        }
        
        List<Unit__c> unitList2 = new List<Unit__c>();
        for (Integer i = 0; i<3; i++){
            unitList2.add(new Unit__c(Name = 'Test TP Unit ' + i));
        }
        insert unitList2;
        
        List<Training_Package_Unit__c> tuList = new List<Training_Package_Unit__c>();
        for (Integer i = 0; i<19; i++){
            for (Integer x = 0; x<3; x++){
                tuList.add(new Training_Package_Unit__c(Training_Package__c=tp2.Id, Unit_of_Competency__c=unitList2[x].Id));
            }
            
        }
        insert tuList; 
        Unit__c u2 = TestCoverageUtilityClass.createUnit();
        List<Qualification_Unit__c> quList = new List<Qualification_Unit__c>();
        for(Unit__c u: unitList){
            quList.add(new Qualification_Unit__c(Qualification__c = masterObject.Id, Unit__c = u.Id));
        }
        quList.add(new Qualification_Unit__c(Qualification__c = masterObject.Id, Unit__c = u2.Id, Unit_Type__c = 'Elective'));
        insert quList;
    }
    static testMethod void testPageView(){
        init();
        PageReference pageRef = Page.TrainingPackageViewer;
        Test.setCurrentPageReference(pageRef);
       
        treeNodes ctrl = new treeNodes();
        
        
        ctrl.getItems();
        ctrl.getItems2();
        ctrl.searchText = 'Test1';
        ctrl.SelectedSearch = 'Unit Name';
        ctrl.SelectedUnit = 'All';        
        ctrl.search();
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'None';
        ctrl.search();
        ctrl.searchText = 'Training';
        ctrl.SelectedSearch = 'Unit Name';
        ctrl.SelectedUnit = 'None';
        ctrl.search();        
        
        ctrl.searchText = '12';
        ctrl.SelectedSearch = 'Unit Name';
        ctrl.SelectedUnit = 'All';
        ctrl.search();        
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'All';
        ctrl.search();
        ctrl.SelectedSearch = 'Code';
        ctrl.searchText = '';
        ctrl.SelectedUnit = 'Core';
        ctrl.search();
        ctrl.query();
        ctrl.searchText = 'Test TP Unit ';
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'All';
        ctrl.search();
        ctrl.getmainnodes();
        
        ctrl.query2();
        ctrl.next();
        ctrl.previous();
    }
    static testMethod void testQUSearch(){
        init();
        PageReference pageRef = Page.TrainingPackageViewer;
        Test.setCurrentPageReference(pageRef);
       
        treeNodes ctrl = new treeNodes();
        ctrl.getItems();
        ctrl.getItems2();
        ctrl.pages = 1;
        ctrl.searchText = 'Test TP Unit 1';
        //ctrl.queryResult2 = new List<Training_Package_Unit__c>();
        //ctrl.queryResult = new List<Qualification_Unit__c>();
        
        
       
        
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'All';
        ctrl.search();
        //System.AssertEquals(19, ctrl.queryResult2.size());
        ctrl.getmainnodes();
        ctrl.query();
        //ctrl.query2();
        ctrl.next();
        ctrl.previous();
    }
    static testMethod void testQUSearch2(){
        init();
        PageReference pageRef = Page.TrainingPackageViewer;
        Test.setCurrentPageReference(pageRef);
       
        treeNodes ctrl = new treeNodes();
        ctrl.getItems();
        ctrl.getItems2();
        ctrl.pages = 1;
        
        ctrl.searchText = 'Test';
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'Elective';
        ctrl.queryResult = new List<Qualification_Unit__c>();
        if(ctrl.queryResult.size() < 0) {
            ctrl.queryResult2 = new List<Training_Package_Unit__c>();
            
            }
        ctrl.search();
        
        ctrl.searchText = 'Test TP Unit';
        
        //ctrl.queryResult = new List<Qualification_Unit__c>();
        
        
       
        
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'All';
        ctrl.search();
        //System.AssertEquals(57, ctrl.queryResult2.size());
        
        ctrl.getmainnodes();
        ctrl.query();
        //ctrl.query2();
        ctrl.next();
        ctrl.previous();
    }
    static testMethod void testTPUSearch(){
        init();
        PageReference pageRef = Page.TrainingPackageViewer;
        Test.setCurrentPageReference(pageRef);
       
        treeNodes ctrl = new treeNodes();
        ctrl.getItems();
        ctrl.getItems2();
        ctrl.pages = 1;
        ctrl.searchText = 'Test Unit 1';
        //ctrl.queryResult2 = new List<Training_Package_Unit__c>();
        //ctrl.queryResult = new List<Qualification_Unit__c>();
        
        
       
        
        ctrl.SelectedSearch = 'Code';
        ctrl.SelectedUnit = 'All';
        ctrl.search();
        System.AssertEquals(10, ctrl.queryResult.size());
        ctrl.getmainnodes();
        
        ctrl.query2();
        ctrl.next();
        ctrl.previous();
    }
}