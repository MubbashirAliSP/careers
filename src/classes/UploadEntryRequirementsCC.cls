/**
 * @description Custom controller for `UploadEntryRequirement` page
 * @author Biao Zhang
 * @date 24.FEB.2016
 */
public with sharing class UploadEntryRequirementsCC {
	// Reference variable for the Application Form Id that gets passed as a parameter to the page
	public Id formId {get; set;}

	// The actual application form record
	public ApplicationForm__c form {get; set;}

	// new entry requirement record for funding stream like C3G
	public EntryRequirement__c newEntryRequirement {get; set;}

	// new attachment for above entry requirement
	public Attachment document {get; set;}

	static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();

	/**
	 * @description Default constructor for the custom controller
	 * @author Biao Zhang
	 * @date 24.FEB.2016
	 */
	public UploadEntryRequirementsCC() {
		// Fetch the formId parameter passed to the page
		if (ApexPages.currentPage().getParameters().containsKey('formId')) {
			formId = ApexPages.currentPage().getParameters().get('formId');
		}

		// Query the actual form record
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, ', StudentCredentialsId__r.AccountId__c, FundingStreamTypeId__r.FundingStreamShortname__c ');
		queryString += 'WHERE Id = :formId';
		form = Database.query(queryString);		

		newEntryRequirement = new EntryRequirement__c(
			ApplicationFormId__c = form.Id,
			Status__c = 'Awaiting Upload',
			ServiceCaseId__c = form.ServiceCaseId__c
		);

		if(form.FundingStreamTypeId__r.FundingStreamShortname__c == 'C3G') {
			newEntryRequirement.RecordTypeId = c3gRTID;
		}	

		document = new Attachment(
			OwnerId = UserInfo.getUserId()
		);
 
	}

	/**
	 * @description redirect to new entry requirement page
	 * @author Biao Zhang
	 * @date 24.FEB.2016
	 */
	public PageReference upload() {
		if(document.name == null || document.body == null) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please choose a file'));
			return null;
		}

		newEntryRequirement.Status__c = 'Awaiting Verification';
		insert newEntryRequirement;
		
		document.ParentId = newEntryRequirement.Id;
		insert document;
		
		// Redirect the page as a whole to overcome large viewstate errors
		PageReference result = Page.EntryRequirements;
		result.getParameters().put('formId', form.Id);
		result.setRedirect(true);
		return result;
	}
}