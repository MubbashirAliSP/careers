@IsTest
public class EnrolmentHistoryTest{
    static ANZSCO__c anz = new ANZSCO__c();
    static Field_of_Education__c fld = new Field_of_Education__c();
    static Qualification__c qual = new Qualification__c();
    static Qualification__c qual2 = new Qualification__c();
    static Unit__c unit = new Unit__c();
    static Location_Loadings__c loading = new Location_Loadings__c(); 
    static Qualification_Unit__c qunit = new Qualification_Unit__c();
    static Qualification_Unit__c qunit2 = new Qualification_Unit__c();
    static Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
    static Funding_Source__c fSource = new Funding_Source__c();
    static Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
    static Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
    static Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
    static State__c state = new State__c();
    static Country__c country = new Country__c();
    static Country__c countryAU = new Country__c();
    static Locations__c parentLoc = new Locations__c();
    static Locations__c loc = new Locations__c();
    static Locations__c loc2 = new Locations__c();
    static Attendance_Types__c attend = new Attendance_Types__c();
    static Enrolment_Unit__c eu = new Enrolment_Unit__c();
    static Results__c res = new Results__c();
    static Unit__c un = new Unit__c();
    static Field_of_Education__c fe = new Field_of_Education__c();  
    static Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
    static Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
    static Account accTraining;
    
    private static void init(){
        accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        unit = TestCoverageUtilityClass.createUnit();
        qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
        country = TestCoverageUtilityClass.createCountry();  
        //countryAU = TestCoverageUtilityClass.queryAustralia();
        state = TestCoverageUtilityClass.createState(country.Id);
        loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
        pCon = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, accTraining.Id);
        conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
        lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
        lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
        parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
        loc = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id, accTraining.Id);
        del = TestCoverageUtilityClass.createDeliveryModeType();
        attend = TestCoverageUtilityClass.createAttendanceType();
    }
    private static Enrolment__c createEnrolment(Date StartDate, Date EndDate, String status){
        String stud = [select id from RecordType where Name='Student' AND SObjectType = 'Account'].Id; 
        Account student = new Account();
        student.RecordTypeId = stud;
        student.LastName = 'Test student Account';
        student.FirstName = 'First Name';
        student.PersonMailingCountry = 'AUSTRALIA';
        student.Student_Identifer__c = 'H89373';
        student.PersonBirthDate = system.today() - 5000;
        student.Year_Highest_Education_Completed__c = '2000';
        student.Proficiency_in_Spoken_English__c = 'Very Well';
        student.Salutation = 'Mr.';
        student.Learner_Unique_Identifier__c = '12ab34cd56';
        insert student; 
        
        Enrolment__c enrl = new Enrolment__c();
        enrl = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
        enrl.Predominant_Delivery_Mode__c = del.Id; 
        enrl.Type_of_Attendance__c = attend.Id;
        enrl.Start_Date__c = StartDate;
        enrl.End_Date__c = EndDate;
        enrl.Delivery_Location__c = loc.id;
        enrl.Enrolment_Status__c = status;
        //enrl.Overseas_Country__c = null;
        insert enrl;
        return enrl;
    }
    
    
    @IsTest
    public static void testBatchBasic() {
       init();
       //Enrolment__c enroBefore = createEnrolment(Date.newInstance(2013, 01, 01), Date.newInstance(2013, 12, 31), 'Active (Commencement)');
       Enrolment__c enroEndsBefore = createEnrolment(Date.newInstance(2013, 01, 01), Date.newInstance(2014, 01, 02), 'Active (Commencement)');
       Enrolment__c enroStartsBefore = createEnrolment(Date.newInstance(2014, 01, 02), Date.newInstance(2014, 01, 30), 'Active (Commencement)');
       Enrolment__c enroStartsOn = createEnrolment(Date.newInstance(2014, 01, 31), Date.newInstance(2015, 01, 30), 'Active (Commencement)');
       Enrolment__c enroAfter = createEnrolment(Date.newInstance(2014, 02, 01), Date.newInstance(2015, 01, 30), 'Active (Commencement)');
                
       Date StartDate = Date.newInstance(2014, 1, 1);
       Date EndDate = System.today() + 1;
       Test.StartTest();
           system.debug('**StartDate: ' + StartDate);
           system.debug('**EndDate: ' + EndDate);
           system.debug('**3rd Param: ' + Date.newInstance(2014, 2, 1));
           system.debug('**4th Param: ' + Date.newInstance(2010, 1, 1));
            EnrolmentHistoryBatch b = new EnrolmentHistoryBatch(StartDate, EndDate, Date.newInstance(2014, 2, 1), Date.newInstance(2010, 1, 1));
            ID batchprocessid = Database.executeBatch(b);
       Test.StopTest();

        Enrolment_History__c[] history = [ select id, Enrolment__c, Month_of_Enrolment__c, Is_Active__c from Enrolment_History__c ];
        system.assertEquals(17, history.size());
        Map<Id, Map<Date, boolean>> results = new Map<Id, Map<Date, boolean>>();
        for ( Enrolment_History__c e : history ){
            if ( !results.containsKey(e.Enrolment__c) )
                results.put(e.Enrolment__c, new Map<Date, boolean>());
            results.get(e.Enrolment__c).put(e.Month_of_Enrolment__c, e.Is_Active__c);
        }
        system.debug('**TEB endsBefore: ' + results.get(enroEndsBefore.id).size() + ' ,startsBefore: ' + results.get(enroStartsBefore.id).size() + ' ,startsOn: ' + results.get(enroStartsOn.id).size() + ' ,after: ' + results.get(enroAfter.id).size());
        system.assertEquals(13, results.get(enroEndsBefore.id).size());
        system.assertEquals(12, countActive(results.get(enroEndsBefore.id)));
        
        system.assertEquals(1, results.get(enroStartsBefore.id).size());
        system.assertEquals(0, countActive(results.get(enroStartsBefore.id)));
        
        system.assertEquals(2, results.get(enroStartsOn.id).size());
        system.assertEquals(2, countActive(results.get(enroStartsOn.id)));
        
        system.assertEquals(1, results.get(enroAfter.id).size());
        //system.assertEquals(1, countActive(results.get(enroAfter.id)));
   }
    
    
    @IsTest
    public static void testFieldHistory() {
       init();
       Enrolment__c enroEndsBefore = createEnrolment(Date.newInstance(2013, 01, 01), Date.newInstance(2014, 01, 02), 'Active (Commencement)');
       Map<Date, boolean> fh = new Map<Date, boolean>();
       fh.put(Date.newInstance(2013, 1, 2), true);
       fh.put(Date.newInstance(2013, 2, 2), false);
       fh.put(Date.newInstance(2013, 4, 30), true);
       fh.put(Date.newInstance(2013, 5, 1), false);
        
       Date StartDate = Date.newInstance(2014, 1, 1);
       Date EndDate = System.today() + 1;
       Test.StartTest();
            EnrolmentHistoryBatch b = new EnrolmentHistoryBatch(StartDate, EndDate, Date.newInstance(2014, 2, 1), Date.newInstance(2010, 1, 1));
            b.testHistory = new Map<Id, Map<Date, boolean>>();
            b.testHistory.put(enroEndsBefore.id, fh);
            ID batchprocessid = Database.executeBatch(b);
       Test.StopTest();

        Enrolment_History__c[] history = [ select id, Enrolment__c, Month_of_Enrolment__c, Is_Active__c from Enrolment_History__c ];
        system.assertEquals(13, history.size());
        Map<Id, Map<Date, boolean>> results = new Map<Id, Map<Date, boolean>>();
        for ( Enrolment_History__c e : history ){
            if ( !results.containsKey(e.Enrolment__c) )
                results.put(e.Enrolment__c, new Map<Date, boolean>());
            results.get(e.Enrolment__c).put(e.Month_of_Enrolment__c, e.Is_Active__c);
        }
         
        system.assertEquals(13, results.get(enroEndsBefore.id).size());
        system.assertEquals(10, countActive(results.get(enroEndsBefore.id)));
        
        
   }
   private static integer countActive(Map<Date, boolean> v){
        integer ret = 0;
        for ( boolean x : v.values() )
            ret += x ? 1 : 0;
        return ret;
   }
    
    @IsTest
    public static void testEnrolmentHistoryController() {
       init();
       Enrolment__c enroEndsBefore = createEnrolment(Date.newInstance(2013, 01, 01), Date.newInstance(2014, 01, 02), 'Active (Commencement)');
       
       EnrolmentHistoryController c = new EnrolmentHistoryController();
       c.init();
       c.limitId = enroEndsBefore.id;
       
       Test.StartTest();
        c.getIsBatchRunning();
        c.getBatchStatus();
        c.runNow();
        c.runBatchNow();
        c.schedule();
       Test.StopTest();
   }
   
   
    @IsTest
    public static void testEnrolementDeletedTrigger() {
       init();
       Enrolment__c enroEndsBefore = createEnrolment(Date.newInstance(2013, 01, 01), Date.newInstance(2014, 01, 02), 'Active (Commencement)');
       
       EnrolmentHistoryController c = new EnrolmentHistoryController();
       c.init();
       c.limitId = enroEndsBefore.id;
       
       Test.StartTest();
        c.getIsBatchRunning();
        c.getBatchStatus();
        c.runNow();
        c.runBatchNow();
        c.schedule();
       Test.StopTest();
       
       delete enroEndsBefore;
   }
    
    

}