/**
 * @description Custom component controller for `SEC_Dashboards` component
 * @author Ranyel Maliwanag
 * @date 08.FEB.2016
 */
public without sharing class SEC_DashboardsCC {
	// Custom controller attribute for determining the current trainer
	public Id currentTrainer {get; set;}

	// Custom controller attribute for determining the current trainer name
	public String currentTrainerName {get; set;}

	public String escapedCurrentTrainerName {
		get {
			return String.escapeSingleQuotes(currentTrainerName);
		}
	}
}