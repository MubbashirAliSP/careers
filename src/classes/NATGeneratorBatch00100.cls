global class NATGeneratorBatch00100 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    private String currentState;
    public String libraryId;
    global String body100;
    private String LogId;

    global NATGeneratorBatch00100(String state, String q, String LId) {
        
        query = q;
        currentState = state;
        logId = LId;
    
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body100 = '';    
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        if (currentState == 'Queensland') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionQLD.nat00100(), currentState);
                }
            }
        }
        
        else if (currentState == 'New South Wales') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionNSW.nat00100(), currentState);
                }
            }
        }
        
        else if (currentState == 'New South Wales APL') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionNSWAPL.nat00100(), currentState);
                }
            }
        }
        
        else if (currentState == 'Australian Capital Territory') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionACT.nat00100(), currentState);
                }
            }
        }
        
        else if(currentState == 'Victoria') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionVIC.nat00100(), currentState);
                }
            }
        }
        
        else if (currentState == 'South Australia') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionSA.nat00100(), currentState);
                }
            }
        }
        
        else if (currentState == 'Western Australia') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionWA.nat00100(), currentState);
                }
            }
        }
        
        else if(currentState == 'Northern Territory') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionNT.nat00100(), currentState);
                }
            }
        }
        
        else if(currentState == 'Tasmania') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionTAS.nat00100(), currentState);
                }
            }
        }  
        
        else if(currentState == 'National') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Prior_Achievement_Flag__c == 'Y') {
                    body100 += NATConstructor.natFile(a, NATDataDefinitionTAS.nat00100(), currentState);
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
        NAT_Validation_Log__c natLog = [ SELECT Id, Validation_State__c, Query_Main__c, Content_Library_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
        
        if(body100 == '') {
            body100 = ' ';
        }
        
        Blob pBlob100 = Blob.valueof(body100);
        insert new ContentVersion (
            versionData =  pBlob100,
            Title = 'NAT00100',
            PathOnClient = '/NAT00100.txt',
            FirstPublishLocationId = natLog.Content_Library_ID__c
        );
                
        Database.executeBatch(new NATGeneratorBatch00120( natLog.Validation_State__c, natLog.Query_Main__c , LogId  ) );
    }
}