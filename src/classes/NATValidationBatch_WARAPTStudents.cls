global class NATValidationBatch_WARAPTStudents implements Database.Batchable<sObject>,Database.Stateful {

   global String query;
   global NAT_Validation_Log__c log;
   global Integer studentRecordCount = 0;
    
    
    global NATValidationBatch_WARAPTStudents (String logId) {
        
         log = [ SELECT ID, RAPT_StudentQuery__c, Validation_State__c, Training_Organisation_Id__c, query_students__c, query_students_2__c, query_students_3__c, query_students_4__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
    
    }


     global database.querylocator start(Database.BatchableContext BC) {
     
        query = log.RAPT_StudentQuery__c;
         
        return Database.getQueryLocator(query);
            
    }
    
   
     global void execute(Database.BatchableContext BC,List<SObject> scope) {
          
          List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
          
          for(SObject s : scope) {
                
                
                
                Account student = (Account) s;
                studentRecordCount++;
                
                NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = student.Id, Nat_File_Name__c = 'WA RAPT Student',Parent_Record_Last_Modified_Date__c = student.LastModifiedDate);
                /*Student Id */
                if(student.Student_Identifer__c == null)
                    event.Error_Message__c = 'Student Identifier cannot be blank';
                
                /*Student First Name and Last Name, controller by validation rules 
                
                else if(student.FirstName == null || student.LastName == null)
                    event.Error_Message__c  = 'Student First Name and Last Name cannot be blank'; */
                    
                /*Student Birthdate */
                else if (student.PersonBirthdate == null)
                    event.Error_Message__c  = 'Student birthdate cannot be blank';
                
                /*Student Address */
                else if(student.PersonMailingStreet == null)
                    event.Error_Message__c  = 'Student Address Line 1 or Street must be populated';
                else if(student.PersonMailingCity == null)
                    event.Error_Message__c  = 'Student City cannot be blank';
                else if(student.PersonMailingPostalCode == null)
                    event.Error_Message__c  = 'Student PostalCode cannot be blank';
                else if(student.PersonMailingState == null)
                    event.Error_Message__c  = 'Student State cannot be blank';
                
                /*Country of Birth Identifier*/
                else if(student.Country_of_Birth_Identifer__c == null)
                    event.Error_Message__c  = 'Country Of Birth Identifier cannot be blank';
                
                /*Main Language spoken at home */
                else if(student.Main_Language_Spoken_at_Home__c == null)
                    event.Error_Message__c  = 'Main Language Spoken at Home cannot be blank';
                    
                /*Proficience in English */
                else if(student.Proficiency_in_Spoken_English_Identifier__c == null)
                    event.Error_Message__c  = 'Proficiency in Spoken English Identifier cannot be blank';
                
                    
                /*Indigenous Status */
                else if(student.Indigenous_status_identifier__c == null)
                    event.Error_Message__c  = 'Indigenous status identifier cannot be blank';
                    
                /*Highest School level completed */
                else if(student.Highest_School_Level_Comp_Identifier__c == null)
                    event.Error_Message__c  = 'Highest School Level Completed Identifier cannot be blank';
                else if(student.Highest_School_Level_Comp_Identifier__c == '02' && student.At_School_Flag__c != 'NO') 
                    event.Error_Message__c  = 'Highest School Level completed Identifier cannot be 02 if current at school is Y';
                else if((student.Highest_School_Level_Comp_Identifier__c == '09' || student.Highest_School_Level_Comp_Identifier__c == '10' || student.Highest_School_Level_Comp_Identifier__c == '11' || student.Highest_School_Level_Comp_Identifier__c == '12') && GlobalUtility.formatAgeFromDateOfBirth(student.PersonBirthDate) < 12)
                    event.Error_Message__c  = 'For Age Group < 12, Highest School Level Completed Identifier cannot be 10,11,12';
                
                    
                /*Year, Highest education completed */
                else if(student.Year_Highest_Education_Completed__c == null)
                    event.Error_Message__c = 'Year Highest Education Completed cannot be blank';
                    
                /*At School Flag */
                else if(student.At_School_Flag__c == null)
                    event.Error_Message__c  = 'At school flag must be populated';
                else if(student.At_School_Flag__c != 'N' && student.Labour_Force_Status_Identifier__c == '01')
                    event.Error_Message__c  = 'At school flag must be false if Labour Force Status is 01 - Full Time Employee';
                  
                /*Employment Status */
                else if(student.Labour_Force_Status_Identifier__c == null)
                    event.Error_Message__c  = 'Labour Force status Identifier cannot be blank';
                else if(student.Labour_Force_Status_Identifier__c == '01' && student.At_School_Flag__c == 'Y')
                    event.Error_Message__c  = 'Student Cannot be Full Time Employee and be at school';
                    
                /*Study Reason */
                else if(student.Study_Reason_Identifier__c == null)
                    event.Error_Message__c  = 'Study Reason Identifier cannot be blank';
                    
                    
                    
                if(event.Error_Message__c != null)
                    errorEventList.add(event);
          
          
          }
          
                     
           insert errorEventList; 
     
     
     }
     
     global void finish(Database.BatchableContext BC) {
         
        log.WA_RAPT_Student_Record_Count__c = studentRecordCount;
         
        update log;
        
        if(!Test.IsRunningTest())
            Database.executeBatch(new NATValidationBatch_WARAPTEnrolments( log.id ) );
     
     
     }
     
     /*private static NAT_Validation_Log__c createLog() {
        
        NAT_Validation_Log__c log = new NAT_Validation_Log__c();
        
        try { insert log; }
        catch(DMLException e) {}
        
        return log;
    
    } */

}