/**
 * @description Test class for `OnlineInboundEmailHandler`
 * @author Ranyel Maliwanag
 * @date 16.FEB.2016
 */
@isTest
public with sharing class OnlineInboundEmailHandler_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;
	}

	static testMethod void handlerTest() {
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
		envelope.toAddress = 'onlinerpl@careersaustralia.edu.au';

		List<OnlineEmailService__mdt> onlineEmailServiceMetas = [SELECT EmailName__c, QueueId__c, Type__c FROM OnlineEmailService__mdt];
		Set<String> emailNames = new Set<String>();
		for (OnlineEmailService__mdt meta : onlineEmailServiceMetas) {
			emailNames.add(meta.EmailName__c);
		}

		List<String> toAddresses = new List<String>();
		for (String emailName : emailNames) {
			toAddresses.add(emailName + '@careersaustralia.edu.au.test');
		}

		email.plainTextBody = '[[CASIS Student Email: person@casis.example.com]] This is the service case description';
		email.htmlBody = email.plainTextBody;
		email.fromAddress = 'testuser@careersaustralia.edu.au.test';
		email.subject = 'RPL test';
		email.toAddresses = toAddresses;

		OnlineInboundEmailHandler handler = new OnlineInboundEmailHandler();

		Test.startTest();
			Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);
		Test.stopTest();
	}

	static testMethod void handlerExceptionTest() {

		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
		envelope.toAddress = 'onlinerpl@careersaustralia.edu.au';

		List<OnlineEmailService__mdt> onlineEmailServiceMetas = [SELECT EmailName__c, QueueId__c, Type__c FROM OnlineEmailService__mdt];
		Set<String> emailNames = new Set<String>();
		for (OnlineEmailService__mdt meta : onlineEmailServiceMetas) {
			emailNames.add(meta.EmailName__c);
		}

		List<String> toAddresses = new List<String>();
		for (String emailName : emailNames) {
			toAddresses.add(emailName + '@careersaustralia.edu.au.test');
		}

		email.plainTextBody = '[[CASIS Student Email: nonexistent@casis.example.com]] This is the service case description';
		email.htmlBody = email.plainTextBody;
		email.fromAddress = 'testuser@careersaustralia.edu.au.test';
		email.subject = 'RPL test';
		email.toAddresses = toAddresses;

		OnlineInboundEmailHandler handler = new OnlineInboundEmailHandler();

		Test.startTest();
			Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);
		Test.stopTest();
	}
}