/**
 * @description Test Class for IntakeUnitOfStudyTriggerHandler 
 * @author Cletz Cadiz
 * @date 30.JAN.2013
 *
 */
@isTest(seeAlldata=true)
private class IntakeUnitOfStudyTriggerHandler_Test {

    static testMethod void testpopulateScheduledHours() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');	
                    
        ANZSCO__c anz = new ANZSCO__c();
	    Field_of_Education__c fld = new Field_of_Education__c();
	    State__c state = new State__c();
	    Country__c country = new Country__c();
	    Qualification__c qual = new Qualification__c();
		Intake__c intk = new Intake__c();
	    Unit__c unC = new Unit__c();
	    //Unit__c unS = new Unit__c();
	    Intake_Unit__c iu = new Intake_Unit__c();
	    //Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
	    Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Locations__c ploc = new Locations__c();
		Locations__c loc = new Locations__c();
		Tuition_Fee__c tuitionFee = new Tuition_Fee__c();
		Tuition_Fee__c tuitionFee2 = new Tuition_Fee__c();
		Intake_Unit_of_Study_Tuition_Fee__c iustf = new Intake_Unit_of_Study_Tuition_Fee__c();
		Location_Loadings__c loading = new Location_Loadings__c();
	    system.runAs(u){
	    	test.startTest();
	    		country = TestCoverageUtilityClass.createCountry();
	    		state 	= TestCoverageUtilityClass.createState(country.Id);
	    		loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
	    		ploc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
	    		qual 	= TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
	    		loc = TestCoverageUtilityClass.createLocation(country.Id,ploc.Id);
	    		unC  	= TestCoverageUtilityClass.createUnit();
	    		//unS 	= TestCoverageUtilityClass.createUnitofStudy();
	    		
	    		State__c st = new State__c();
	    		st.Code__c = '01';
	    		st.Country__c = country.Id;
	    		st.Short_Name__c = 'QLD';
	    		insert st;
	    		
	    		Id recrdType = [select Id from RecordType where Name = 'Unit of Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
	    		Unit__c unS = new Unit__c();
	    		unS.Name = 'UN Study';
	    		unS.RecordTypeId = recrdType;
	    		unS.Parent_UoC__c = unc.Id;
	    		insert unS;
	    		
	    		tuitionFee = TestCoverageUtilityClass.createTuitionFee(qual.Id, st.Id);
	    		tuitionFee2 = TestCoverageUtilityClass.createTuitionFee(qual.Id, st.Id);
	    		fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
	    		iu   	= TestCoverageUtilityClass.createIntakeUnit(intk.Id, unS.Id);
	    		
	    		//uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unS.Id, state.Id);
	    		Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
	    		uhp.Unit__c = unc.Id;
	    		uhp.Nominal_Hours__c = 1.5;
        		uhp.Points__c = 1;
        		uhp.State__c = st.Id;
        		insert uhp;
        		
        		Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
	    		uhp2.Unit__c = unc.Id;
	    		uhp2.Nominal_Hours__c = 1.5;
        		uhp2.Points__c = 1;
        		uhp2.State__c = st.Id;
        		uhp2.Institutional_Hours__c  = 2;
        		insert uhp2;
        		
        		Intake_Unit_of_Study__c ius = new Intake_Unit_of_Study__c();
	    		ius.Intake_Unit_of_Competency__c = iu.Id;
	    		ius.Unit__c = unS.Id; 
	    		insert ius;
        		
        		iustf = TestCoverageUtilityClass.createIntakeUnitofStudyTuitionFee(st.Id, ius.Id);
        		/*String x = ius.Unit__c;
        		if(x == uhp.Unit__c){
        			ius.Scheduled_Hours__c = uhp.Nominal_Hours__c;
        		}*/
        		ius.Scheduled_Hours__c = 40;
        		update ius;
	    	test.stopTest();
	    }
    }
}