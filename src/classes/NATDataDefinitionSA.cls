public class NATDataDefinitionSA {
    
    // NAT00010 - Account Object Fields
    public static final String NationalProviderNumber  = 'Account.National_Provider_Number__c';
    
     // NAT00130 Fields - Award Object
    public static final String ParchmentNumber = 'Award_ID_CASIS__c';
    public static final String DateOfIssue = 'Date_Issued__c';
    public static final String ReportableQualificationHours = 'SA_Reportable_Qualification_Hours__c';
    public static final String ProficiencyinSpokenEnglishIdentifier = 'Proficiency_in_Spoken_English_Identifier__c';
    
    //NAT00085 Fields - Account Object
    public static final String PostalBuildingPropertyName = 'Postal_building_property_name__c';
    public static final String PostalFlatUnitDetails = 'Postal_flat_unit_details__c';
    public static final String PostalStreetNumber = 'Postal_street_number__c';
    public static final String PostalStreetName = 'Postal_street_name__c';
    public static final String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static final String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static final String PostalPostCode = 'PersonOtherPostalCode';
    
    public static final String BlankField = 'BlankField';

    public static Map< Integer, NATDataElement > nat00010() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00010();
        positions.put(1, new NATDataElement(NationalProviderNumber, 10, true, 'string' ) );
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00020() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00020();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00030() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00030();
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, false, 'number') );

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00060() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00060();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00080() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00080();
        positions.put(15, new NATDataElement(ProficiencyinSpokenEnglishIdentifier, 1, false, 'string' ) ); 
       //below blank spaces added as requested
        positions.put(23, new NATDataElement(BlankField, 11, true, 'string' ) );
        positions.put(24, new NATDataElement(BlankField, 9, true, 'string' ) );

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00085() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00085();
        positions.put(5, new NATDataElement(PostalBuildingPropertyName, 50, true, 'string') );
        positions.put(6, new NATDataElement(PostalFlatUnitDetails, 30, true, 'string') );
        positions.put(7, new NATDataElement(PostalStreetNumber, 15, true, 'string') );
        positions.put(8, new NATDataElement(PostalStreetName, 70, true, 'string' ) ); 
        positions.put(9, new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ); 
        positions.put(10, new NATDataElement(PostalSuburbLocalityTown, 50, true, 'string' ) ); 
        positions.put(11, new NATDataElement(PostalPostCode, 4, true, 'string' ) ); 
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00090() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00090();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00100() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00100();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00120() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00120();
        return positions;
    }
    
    public static Map<Integer, NATDataElement> nat00130() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00130(); 
        positions.put(6, new NATDataElement(ParchmentNumber, 25, FALSE, 'string' ) );
        positions.put(7, new NATDataElement(DateOfIssue, 8, FALSE, 'date' ) );  
        return positions;
    }
        
}