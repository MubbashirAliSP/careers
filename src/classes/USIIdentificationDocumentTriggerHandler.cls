/**
 * @description USI Identification Document object Trigger 
 * @author      Sairah Hadjinoor
 * @Company     CloudSherpas
 * @date        23.OCT.2014
 *
 * HISTORY
 * - 23.OCT.2014    Sairah Hadjinoor      Created.
 * - 27.OCT.2014	Sairah Hadjinoor	  Updated to add updateDocumentSubmittedOnDelete method
 */


public with sharing class USIIdentificationDocumentTriggerHandler {
	
	//start onBeforeInsert Method
	public static void onBeforeInsert(List<USI_Identification_Document__c> newRecords) {
         
    }// End onBeforeInsert Method

    //start onAfterInsert Method
    public static void onAfterInsert(List<USI_Identification_Document__c> newRecords) {
    	Set<Id> accIdSet = new Set<Id>();
        for(USI_Identification_Document__c usi: newRecords){
        	//get all associated accounts to be updated
         	accIdSet.add(usi.Account__c);
        }

        if(!accIdSet.isEmpty()){
        	//call method for more readable codes
        	//just in case we would have more codes in the future
        	updateDocumentSubmitted(accIdSet);
        }
    }// End onAfterInsert Method

    
    
    //uncomment this just in case we need an before and after update event
    /*public static void onBeforeUpdate(List<USI_Identification_Document__c> oldRecords,List<USI_Identification_Document__c> newRecords, Map<Id,USI_Identification_Document__c> oldRecordsMap) {
         
    }
    // End onBeforeUpdate Method

    public static void onAfterUpdate(List<USI_Identification_Document__c> oldRecords,List<USI_Identification_Document__c> newRecords, Map<Id,USI_Identification_Document__c> oldRecordsMap) {
         
    }*/
    // End onAfterUpdate Method

    //uncomment this just in case we need a before and after delete event
    /*public static void onBeforeDelete(List<USI_Identification_Document__c> oldRecords,Map<Id,USI_Identification_Document__c> oldRecordsMap) {
         
    }// End onBeforeDelete Method
	*/
    public static void onAfterDelete(List<USI_Identification_Document__c> oldRecords,Map<Id,USI_Identification_Document__c> oldRecordsMap) {
    	
    	Set<Id> accIdSet = new Set<Id>();
        
        for(USI_Identification_Document__c usi: oldRecords){
        	//get all associated accounts to be updated
         	accIdSet.add(usi.Account__c);
        }

        if(!accIdSet.isEmpty()){
        	//call method for more readable codes
        	//just in case we would have more codes in the future
        	updateDocumentSubmittedOnDelete(accIdSet);
        }
    }
    // End onAfterDelete Method

    public static void updateDocumentSubmitted(Set<Id> accIdsSet){
    	/**
		 * @description When a USI document is added, this updates the field  USI_ID_Document_Submitted__c
		 *				of the associated account to true
		 * @author      Sairah Hadjinoor
		 * @Company     CloudSherpas
		 * @date        23.OCT.2014
		 *
		 * HISTORY
		 * - 23.OCT.2014    Sairah Hadjinoor      Created.
		**/
    	List<Account> accList = new List<Account>();
    	//query all accounts to be updated
    	for(Account acc: [Select USI_ID_Document_Submitted__c from Account where Id IN: accIdsSet]){
    		//set to true when USI document is added
    		acc.USI_ID_Document_Submitted__c = true;
    		accList.add(acc);
    	}
    	//check if account list is not empty
    	if(!accList.isEmpty()){
    		//if not update account list
    		try{
    			update accList;
    		}
    		//catch all unexpected errors
    		catch(Exception e){
    			system.debug('ERROR: '+ e.getMessage());
    		}
    	}
    }

    public static void updateDocumentSubmittedOnDelete(Set<Id> deletedUSIaccIdSet){
    	/**
		 * @description When a USI document is deleted, this updates the field  USI_ID_Document_Submitted__c
		 *				of the associated account to false
		 * @author      Sairah Hadjinoor
		 * @Company     CloudSherpas
		 * @date        27.OCT.2014
		 *
		 * HISTORY
		 * - 27.OCT.2014    Sairah Hadjinoor      Created.
		**/
    	List<Account> accList = new List<Account>();
    	//query all accounts to be updated
    	for(Account acc: [Select USI_ID_Document_Submitted__c from Account where Id IN: deletedUSIaccIdSet]){
    		//set to false when the USI is deleted
    		acc.USI_ID_Document_Submitted__c = false;
    		accList.add(acc);
    	}
    	//check if the account is not empty
    	if(!accList.isEmpty()){
    		//if not update account list
    		try{
    			update accList;
    		}
    		//catch all unexpected errors
    		catch(Exception e){
    			system.debug('ERROR: '+ e.getMessage());
    		}
    	}
    }
}