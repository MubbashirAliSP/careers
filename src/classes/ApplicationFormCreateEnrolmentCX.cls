/**
 * @description Custom controller extension for `ApplicationFormCreateEnrolment` page
 * @author Ranyel Maliwanag
 * @date 03.AUG.2015
 * @history
 *       12.OCT.2015    Ranyel Maliwanag    - Added tooling API callouts to fetch Custom Field Ids in preparation for the transition of background creation of enrolments to prepopulated forms.
 *		 01.MAR.2016	Ranyel Maliwanag	- Moved from custom settings to custom metadata for enrolment field mappings
 *		 20.JUN.2016	Ranyel Maliwanag	- Added handling of special characters for Enrolment field values which causes previous Enrolment mappings to spazz out, specifically the percent (%) character.
 */
public with sharing class ApplicationFormCreateEnrolmentCX {
	public ApplicationForm__c applicationForm {get; set;}
	public SObject enrolment {get; set;}

	public String debugger {get; set;}
	public String enrolmentUrl {get; set;}

	private Map<String, ApplicationFormItemAnswer__c> answersMap;
	private Map<String, ApplicationFormItem__c> itemsMap;

	/**
	 * @description Default controller extension constructor
	 * @author Ranyel Maliwanag
	 * @date 03.AUG.2015
	 * @param (ApexPages.StandardController) controller: The standard controller that will be extended by this class
	 */
	public ApplicationFormCreateEnrolmentCX(ApexPages.StandardController controller) {
		// Variable Initialisations
		String queryString;
		String subQueryString;
		Set<String> extrasSet = new Set<String>();
		String extras = ', ';
		Id applicationFormId = controller.getId();
		enrolmentUrl = '';
		applicationForm = (ApplicationForm__c) controller.getRecord();
		answersMap = new Map<String, ApplicationFormItemAnswer__c>();
		itemsMap = new Map<String, ApplicationFormItem__c>();
		Map<String, ApplicationsEnrolmentFieldMapping__c> mappingSettings = ApplicationsEnrolmentFieldMapping__c.getAll();
		List<ApplicationFormEnrolmentMapping__mdt> enrolmentMappings = [SELECT AdditionalFieldQuery__c, DropdownTranslations__c, FieldId__c, FundingStream__c, LookupTranslations__c, SourceField__c, SourceObject__c, TargetField__c, IsTranslateBooleanDropdowns__c, IsUseReadableAnswer__c FROM ApplicationFormEnrolmentMapping__mdt];

		Map<String, Schema.SObjectField> enrolmentFieldDescribe = Schema.SObjectType.Enrolment__c.fields.getMap();

		for (ApplicationsEnrolmentFieldMapping__c mapping : mappingSettings.values()) {
			if (mapping.SourceObject__c == 'ApplicationForm__c' && mapping.SourceField__c.contains('.')) {
				extrasSet.add(mapping.SourceField__c);
			}

			if (String.isNotBlank(mapping.AdditionalFieldQuery__c)) {
				extrasSet.add(mapping.AdditionalFieldQuery__c);
			}
		}
		// Ensure that Funding Stream Short name is queried
		extrasSet.add('FundingStreamTypeId__r.FundingStreamShortname__c');
		extras += String.join(new List<String>(extrasSet), ', ');

		// Build subQueryString for fetching related ApplicationFormItemAnswer__c records
		subQueryString = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationFormItemAnswer__c, ', ApplicationFormItemId__r.Name ');
		subQueryString = subQueryString.replace('ApplicationFormItemAnswer__c', 'ApplicationFormItemAnswers__r');
		subQueryString += ')';
		extras += subQueryString;

		// Query Application Form
		queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, extras);
		queryString += 'WHERE Id = :applicationFormId';
		applicationForm = Database.query(queryString);

		if (applicationForm != null) {
			// Build the answersMap variable to reference individual ApplicationFormItemAnswer__c records by Item Identifier
			for (ApplicationFormItemAnswer__c answer : applicationForm.ApplicationFormItemAnswers__r) {
				answersMap.put(answer.ApplicationFormItemId__r.Name, answer);
			}

			// Start building the enrolment URL
			enrolmentUrl = '/' + Schema.SObjectType.Enrolment__c.getKeyPrefix() + '/e?retUrl=';
			enrolment = Enrolment__c.SObjectType.newSObject();

			// Populate enrolment fields based on custom settings
			for (ApplicationFormEnrolmentMapping__mdt mapping : enrolmentMappings) {
				System.debug(mapping.FundingStream__c + ' ::: ' + mapping.TargetField__c + ' ::: ' + mapping.SourceObject__c + ' ::: ' + mapping.SourceField__c);
				// Check if funding stream of mapping is applicable to the form
				if (mapping.FundingStream__c.contains('All') || (String.isNotBlank(applicationForm.FundingStreamTypeId__r.FundingStreamShortname__c) && mapping.FundingStream__c.contains(applicationForm.FundingStreamTypeId__r.FundingStreamShortname__c))) {
					System.debug('::: Valid');
					// Condition: Enrolment mapping is applicable to all Funding Streams OR is for the Funding Stream of the current Application Form
					String enrolmentField = mapping.TargetField__c;

					Boolean isFieldDescribeAvailable = enrolmentFieldDescribe.containsKey(enrolmentField) && String.isNotBlank(mapping.FieldId__c);
					Boolean isReferenceField = isFieldDescribeAvailable && enrolmentFieldDescribe.get(enrolmentField).getDescribe().getType() == Schema.DisplayType.REFERENCE;

					if (isFieldDescribeAvailable) {
						enrolmentUrl += '&';
						if (isReferenceField) {
							enrolmentUrl += 'CF';
						}
						enrolmentUrl += mapping.FieldId__c;
						if (isReferenceField) {
							enrolmentUrl += '_lkid';
						}
						enrolmentUrl += '=';
					}

					if (mapping.SourceObject__c == 'ApplicationForm__c') {
						Object value = EnrolmentsSiteUtility.getReferenceFieldValue(applicationForm, mapping.SourceField__c);
						debugger += '<br />' + value + '<br />';
						//enrolment.put(enrolmentField, value);
						if (isFieldDescribeAvailable) {
							enrolmentUrl += String.isNotBlank(String.valueOf(value)) ? EncodingUtil.urlEncode(String.valueOf(value), 'utf-8') : '';

							if (isReferenceField) {
								enrolmentUrl += '&CF' + mapping.FieldId__c + '=' + EnrolmentsSiteUtility.getReferenceFieldValue(applicationForm, mapping.AdditionalFieldQuery__c);
							}
						}
					} else if (mapping.SourceObject__c == 'RecordType') {
						Id recordTypeId = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get(mapping.SourceField__c).getRecordTypeId();
						//enrolment.put('RecordTypeId', recordTypeId);
						enrolmentUrl += '&RecordType=' + recordTypeId;
					} else if (mapping.SourceObject__c == 'String') {
						String value = mapping.SourceField__c;
						//enrolment.put(mapping.Name, value);

						if (isFieldDescribeAvailable) {
							enrolmentUrl += String.isNotBlank(String.valueOf(value)) ? EncodingUtil.urlEncode(String.valueOf(value), 'utf-8') : '';
						}
					} else {
						for (String sourceField : mapping.SourceField__c.split(',')) {
							// Remove whitespaces
							sourceField = sourceField.trim();

							// Check if the sourceField is in the answers map
							if (answersMap.containsKey(sourceField)) {
								Object value = answersMap.get(sourceField).Answer__c;

								// Check if a non-null answer exists
								if (value != null) {
									// Boolean dropdowns handling
									if (mapping.IsTranslateBooleanDropdowns__c) {
										if (String.valueOf(value) == 'Yes') {
											value = true;
										} else if (String.valueOf(value) == 'No') {
											value = false;
										}
									}

									if (mapping.IsUseReadableAnswer__c && String.isNotBlank(answersMap.get(sourceField).ReadableAnswer__c)) {
										value = answersMap.get(sourceField).ReadableAnswer__c;
									}

									if (String.isNotBlank(mapping.DropdownTranslations__c)) {
										List<String> translations = mapping.DropdownTranslations__c.split('\r\n');
										// Mapping of <Form Answer> : <Record Value>
										Map<Object, String> answerTranslationMap = new Map<Object, String>();

										for (String translation : translations) {
											translation = translation.trim();
											String formAnswer = translation.split(':')[0].trim();
											String fieldValue = translation.split(':')[1].trim();
											answerTranslationMap.put(formAnswer, fieldValue);
										}

										if (answerTranslationMap.containsKey(value)) {
											value = answerTranslationMap.get(value);
										}
									}

									//dirty hard coded for mask C3G online delivery location to bowen hills
									if(sourceField == 'DeliveryLocationC3G' && applicationForm.Delivery_Mode__c == 'Online') {
										value = '';
										answersMap.get(sourceField).ReadableAnswer__c = 'Bowen Hills';
									}

									//enrolment.put(enrolmentField, value);
									if (isFieldDescribeAvailable) {
										enrolmentUrl += String.isNotBlank(String.valueOf(value)) ? EncodingUtil.urlEncode(String.valueOf(value), 'utf-8') : '';

										if (isReferenceField) {
											// Condition: The enrolment field being populated is a lookup
											// We need to have a user visible value (i.e. record name) added to the parameter
											String readableAnswer = answersMap.get(sourceField).ReadableAnswer__c;
											if (String.isBlank(readableAnswer)) {
												// Condition: A readable answer is not recorded on the answer object
												// Check if there is a translation piece in the custom setting:
												if (String.isNotBlank(mapping.LookupTranslations__c)) {
													List<String> lookupTranslations = mapping.LookupTranslations__c.split('\r\n');
													Map<Id, String> lookupTranslationMap = new Map<Id, String>();

													for (String translation : lookupTranslations) {
														Id recordId = translation.split(':')[0].trim();
														String recordName = translation.split(':')[1].trim();
														lookupTranslationMap.put(recordId, recordName);
													}

													if (lookupTranslationMap.containsKey((Id) value)) {
														readableAnswer = lookupTranslationMap.get((Id) value);
													}
												}
											}

											if (String.isNotBlank(readableAnswer)) {
												enrolmentUrl += '&CF' + mapping.FieldId__c + '=' + readableAnswer;
											}
										}
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * @description Page load action handler to redirect the user to the newly created enrolment form record opened in edit mode
	 * @author Ranyel Maliwanag
	 * @date 03.AUG.2015
	 * @return (PageReference): Expected to be the url for the newly created Enrolment__c record in edit mode and with return url to the same record in view mode upon submission
	 */
	public PageReference evaluateRedirects() {
		PageReference result = null;
		enrolmentUrl += '&nooverride=1';
		debugger = enrolmentUrl;
		result = new PageReference(enrolmentUrl);
		result.setRedirect(true);
		return result;
	}
}