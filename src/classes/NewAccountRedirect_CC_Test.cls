@isTest
private class NewAccountRedirect_CC_Test {

    static testMethod void NewAccountRedirect_CC_Test() {
    	Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
    	labor.Name = 'Not Specified';
    	labor.Code__c = '@@';
    	insert labor;
    	Country__c country = new Country__c();
    	country.Name = 'Not Specified';
    	country.Country_Code__c = '@@@@';
    	insert country;
    	Language__c language = new Language__c();
    	language.Name = 'Not Specified';
    	language.Code__c = '@@@@';
    	insert language;
    	
        PageReference testPage = Page.NewAccountRedirect;
        Account acct = new Account();
        Test.setCurrentPage(testPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(acct);
        NewAccountRedirect_CC acc = new NewAccountRedirect_CC(controller);
        acc.redirect();
    }
}