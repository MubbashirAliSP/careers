/**
 * @description Test class for `ProgressionSuspensionBatch` class
 * @author Ranyel Maliwanag
 * @date 19.NOV.2015
 */
@isTest
public with sharing class ProgressionSuspensionBatch_Test {
	@testSetup
	static void batchSetup() {
		List<Account> accounts = SASTestUtilities.bulkCreateStudentAccount(21);
		insert accounts;

		Qualification__c qual = SASTestUtilities.createQualification(null, null);
		insert qual;

		List<Enrolment__c> enrolments = new List<Enrolment__c>();
		for (Account student : accounts) {
			Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
			enrolment.Qualification__c = qual.Id;
			enrolments.add(enrolment);
		}
		insert enrolments;

		Stage__c stage = new Stage__c(Name = 'Stage 1');
		insert stage;

		Intake__c intake = SASTestUtilities.createIntake(qual);
		intake.Stage__c = stage.id;
		insert intake;

		List<Census__c> censuses = new List<Census__c>();
		Census__c previousCensus;
		for (Enrolment__c enrolment : enrolments) {
			Census__c census = new Census__c(
                EnrolmentId__c = enrolment.Id,
                IntakeId__c = intake.Id,
                CensusDate__c = Date.today().addDays(-1)
            );
            previousCensus = census;
			censuses.add(census);
		}
		previousCensus = new Census__c(
				EnrolmentId__c = previousCensus.EnrolmentId__c,
				IntakeId__c = previousCensus.IntakeId__c,
				CensusDate__c = previousCensus.CensusDate__c
			);
		censuses.add(previousCensus);
		insert censuses;
	}

	static testMethod void batchTest() {
		Test.startTest();
			ProgressionSuspensionBatch nextBatch = new ProgressionSuspensionBatch(null);
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(20, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from%'];
		System.assertEquals(20, enrolmentCount);
	}

	static testMethod void batchTest2() {
		List<Enrolment__c> enrolments = [SELECT Id FROM Enrolment__c];
		for (Enrolment__c enrolment : enrolments) {
			enrolment.IsVFHLoanFormSubmitted__c = true;
			enrolment.Eligible_for_C1__c = true;
			enrolment.AreAllEntryRequirementsMet__c = true;
		}
		update enrolments;

		Test.startTest();
			ProgressionSuspensionBatch nextBatch = new ProgressionSuspensionBatch(null);
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(20, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from%'];
		System.assertEquals(0, enrolmentCount);
	}

	static testMethod void batchTest3() {
		List<Enrolment__c> enrolments = [SELECT Id FROM Enrolment__c];
		for (Enrolment__c enrolment : enrolments) {
			enrolment.IsVFHLoanFormSubmitted__c = true;
			enrolment.Eligible_for_C1__c = true;
			enrolment.AreAllEntryRequirementsMet__c = false;
			enrolment.Form_Number__c = 'F-0001';
		}
		update enrolments;

		Test.startTest();
			ProgressionSuspensionBatch nextBatch = new ProgressionSuspensionBatch(null);
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(20, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from%'];
		System.assertEquals(0, enrolmentCount);
	}

	static testMethod void batchTest4() {
		List<Enrolment__c> enrolments = [SELECT Id FROM Enrolment__c];
		for (Enrolment__c enrolment : enrolments) {
			enrolment.IsVFHLoanFormSubmitted__c = false;
			enrolment.Eligible_for_C1__c = true;
			enrolment.AreAllEntryRequirementsMet__c = false;
			enrolment.Form_Number__c = '';
		}
		update enrolments;

		Test.startTest();
			ProgressionSuspensionBatch nextBatch = new ProgressionSuspensionBatch(null);
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(20, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from%'];
		System.assertEquals(0, enrolmentCount);
	}
}