/**
 * @description Central trigger handler for the `GuestLogin__c` object
 * @author Ranyel Maliwanag
 * @date 29.JUL.2015
 */
public with sharing class GuestLoginTriggerHandler {
    public static void onBeforeInsert(List<GuestLogin__c> guestLogins) {
        CentralTriggerSettings__c triggerSettings = CentralTriggerSettings__c.getInstance();

        if (triggerSettings.GuestLogin__c) {
            if (triggerSettings.GuestLogin_CreateAccounts__c) {
                List<GuestLogin__c> loginsWithoutAccounts = new List<GuestLogin__c>();
                for (GuestLogin__c record : guestLogins) {
                    if (String.isBlank(record.AccountId__c)) {
                        loginsWithoutAccounts.add(record);
                    }
                }
                createAccounts(loginsWithoutAccounts);
            }
        }

        updateUnencryptedPasswords(guestLogins);
    }


    public static void onBeforeUpdate(List<GuestLogin__c> records, Map<Id, GuestLogin__c> recordsMap) {
        updateUnencryptedPasswords(records);
    }


    /**
     * @description Creates linked `Account` records for `GuestLogin__c` records that have no associated account yet
     * @author Ranyel Maliwanag
     * @date 29.JUL.2015
     * @param (List<GuestLogin__c>) guestLogins: List of `GuestLogin__c` records that needs to have an associated account if none found yet
     */
    private static void createAccounts(List<GuestLogin__c> guestLogins) {
        // Collection of emails from `GuestLogin__c` records
        Set<String> emails = new Set<String>();
        // Mapping of <Email> : <Account Id>
        Map<String, Id> accountEmailMap = new Map<String, Id>();
        // Mapping of <Email> : <GuestLogin__c record>
        Map<String, GuestLogin__c> loginEmailMap = new Map<String, GuestLogin__c>();
        // Mapping of <State Short Name> : <State Id>
        Map<String, Id> stateMap = new Map<String, Id>();
        // List of Account records for insertion
        List<Account> accountsForInsert = new List<Account>();
        
        String queryString;
        Set<String> studentRecordTypes = new Set<String>{
            'Student',
            'Student Lead'
        };
        Id studentLeadRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student Lead').getRecordTypeId();

        // Collect list of emails and build a mapping of <Email> : <GuestLogin>
        for (GuestLogin__c login : guestLogins) {
            emails.add(login.Email__c);
            loginEmailMap.put(login.Email__c, login);
        }

        // Query list of accounts with the collected emails
        queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
        queryString += 'WHERE PersonEmail IN :emails AND RecordType.Name IN :studentRecordTypes';
        List<Account> accountRecords = Database.query(queryString);

        // Build a mapping of <Email> : <Account>
        for (Account accountRecord : accountRecords) {
            accountEmailMap.put(accountRecord.PersonEmail, accountRecord.Id);
        }

        // Build a mapping of <State Short Name> : <State ID>
        List<State__c> states = Database.query(EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.State__c));
        for(State__c state : states) {
            stateMap.put(state.Short_Name__c, state.Id);
        }

        for (GuestLogin__c login : guestLogins) {
            if (accountEmailMap.containsKey(login.Email__c)) {
                // Condition: An account already exists with this email
                // Update Account lookup to the matching one
                login.AccountId__c = accountEmailMap.get(login.Email__c);
            } else {
                Account accountForInsert = new Account(
                        FirstName = login.FirstName__c,
                        LastName = login.LastName__c,
                        PersonEmail = login.Email__c,
                        RecordTypeId = studentLeadRecordType,
                        Reporting_Billing_State__c = stateMap.get(login.AddressState__c)
                    );
                accountsForInsert.add(accountForInsert);
            }
        }

        // Bulk insert new Account records
        insert accountsForInsert;

        // Update mapping with newly inserted accounts
        for (Account accountRecord : accountsForInsert) {
            if (loginEmailMap.containsKey(accountRecord.PersonEmail)) {
                loginEmailMap.get(accountRecord.PersonEmail).AccountId__c = accountRecord.Id;
            }
        }
    }

    /**
     * @description Updates the unencrypted guest credential password
     * @author Ranyel Maliwanag
     * @date 25.AUG.2015
     */
    private static void updateUnencryptedPasswords(List<GuestLogin__c> records) {
        for (GuestLogin__c record : records) {
            record.PasswordUnencrypted__c = record.Password__c;
        }
    }
}