/**
 * @description Test class for `GuestLoginTriggerHandler` class
 * @author Ranyel Maliwanag
 * @date 28.AUG.2015
 */
@isTest
public with sharing class GuestLoginTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {

    }

    static testMethod void updateUnencryptedPasswordsTest() {
        ApplicationsTestUtilities.initialiseTriggerCustomSettings();
        ApplicationsTestUtilities.initialiseLogins();

        Test.startTest();
        	GuestLogin__c studentLogin = [SELECT Id, AccountId__c FROM GuestLogin__c WHERE RecordType.Name = 'Student'];
        	System.assert(String.isNotBlank(studentLogin.AccountId__c));
        Test.stopTest();
    }
}