/**
 */
@isTest
private class StaffIDPhoto_CC_Test {

    static testMethod void staffIdPhotoTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Staff_ID_Card__c staff = new Staff_ID_Card__c();
        Attachment attach = new Attachment();
        system.runAs(u){
            test.startTest();
            	staff = TestCoverageUtilityClass.createStaffIDCard(u.Id);
		    	attach.Name='Unit Test Attachment';
		    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
		    	attach.body=bodyBlob;
	            PageReference testPage = Page.StaffIDPhoto;
	            
	            //testPage.getParameters().put('id', staff.Id);
	            Test.setCurrentPage(testPage);
	            ApexPages.StandardController controller = new ApexPages.StandardController(staff);
	            StaffIDPhoto_CC staffId = new StaffIDPhoto_CC(controller);
	            staffId.attachment = attach;
	            staffId.upload();
	            staffId.deleteImage();
	            test.stopTest();
        }
    }
    static testMethod void staffIdPhotoTest2() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Staff_ID_Card__c staff = new Staff_ID_Card__c();
        system.runAs(u){
            test.startTest();
            	staff = TestCoverageUtilityClass.createStaffIDCard(u.Id);
	            PageReference testPage = Page.StaffIDPhoto;
	            
	            //testPage.getParameters().put('id', staff.Id);
	            Test.setCurrentPage(testPage);
	            ApexPages.StandardController controller = new ApexPages.StandardController(staff);
	            StaffIDPhoto_CC staffId = new StaffIDPhoto_CC(controller);
	            staffId.upload();
	            test.stopTest();
        }
    }
}