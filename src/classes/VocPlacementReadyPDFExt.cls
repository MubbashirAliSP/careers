/**
 * @description: VocPlacementReadyPDF Extension. Generates report data of to check completion of pre requisites
 * @created: 4/10/2015
 * @author: bayani.cruz@cloudsherpas.com
 */

public with sharing class VocPlacementReadyPDFExt { 

    private Map<Id, Map<String, String>> enrPPResMap = new Map<Id, Map<String, String>>();

    public final Intake__c intake {get; private set;}

    public List<StudentPreReqs> intakeStudPre {get; private set;}
    public Set<String> preReqSet {get; private set;}
    
    //list of exception columns
    public Set<String> preReqExceptionList;

    //wrapper class
    public class StudentPreReqs {
        public Intake_Students__c intakeStudent {get;set;}
        public Map<String, String> completeList {get;set;}

        StudentPreReqs(Intake_Students__c intakeStudent, Map<String, String> completeList) {
            this.intakeStudent = intakeStudent;
            this.completeList = completeList;
        }
    }
    
    //exception class
    public void setPreReqExceptionList(){
        preReqExceptionList = new Set<String>();
        preReqExceptionList.add('Hand Hygiene Certificate');
    }

    //constructor
    public VocPlacementReadyPDFExt(ApexPages.StandardController stdController) {
        this.intake = [SELECT Id, Name, Qualification_Name__c, Parent_Qualification_Code__c, 
        (SELECT Id, Student_Name_Conga__c, Enrolment__r.Name, Student__r.Suburb_locality_or_town__c FROM Intake_Students__r) FROM Intake__c WHERE Id =: (Id) stdController.getId()];
        
        setPreReqExceptionList();
        generateReportData();
    }

    private void generateReportData() {
        preReqSet = new Set<String>();
        enrPPResMap = new Map<Id, Map<String, String>>();

        for(Completed_Placement_Prerequisites__c cpp : [SELECT Id, Enrolment__c, Completed_Prerequisite__r.Name, Expiry_Date__c, Status__c FROM Completed_Placement_Prerequisites__c WHERE Enrolment__c in : getEnrolments()]) {
            //remove exceptions exception           
            if(!preReqExceptionList.contains(cpp.Completed_Prerequisite__r.Name)){
                //get all column names
                preReqSet.add(cpp.Completed_Prerequisite__r.Name);

                //define nested map values
                if(enrPPResMap.containsKey(cpp.Enrolment__c)) {
                    //if has expiry date value, use that instead of status
                    if(cpp.Expiry_Date__c != NULL) {
                        enrPPResMap.get(cpp.Enrolment__c).put(cpp.Completed_Prerequisite__r.Name, String.valueOf(cpp.Expiry_Date__c));
                    } else {
                        enrPPResMap.get(cpp.Enrolment__c).put(cpp.Completed_Prerequisite__r.Name, cpp.Status__c);
                    }
                } else {
                    //if has expiry date value, use that instead of status
                    if(cpp.Expiry_Date__c != NULL) {
                        enrPPResMap.put(cpp.Enrolment__c, new Map<String, String>{cpp.Completed_Prerequisite__r.Name => String.valueOf(cpp.Expiry_Date__c)});
                    } else {
                        enrPPResMap.put(cpp.Enrolment__c, new Map<String, String>{cpp.Completed_Prerequisite__r.Name => cpp.Status__c});            
                    }
                }
            }
        }

        intakeStudPre = new List<StudentPreReqs>();
        for(Intake_Students__c is : this.intake.Intake_Students__r) {
            //put the rest of preReq Columns
            for(String s : preReqSet) {
                if(enrPPResMap.containsKey(is.Enrolment__c)) {
                    if(!enrPPResMap.get(is.Enrolment__c).containsKey(s)) {
                        enrPPResMap.get(is.Enrolment__c).put(s, '');
                    }
                }               
            }     
            //define wrapper add to list       
            intakeStudpre.add(new StudentPreReqs(is, enrPPResMap.get(is.Enrolment__c)));
        }
    }

    //get all enrolments of IS
    private Set<Id> getEnrolments() {
        Set<Id> enrolmentSet = new Set<Id>();
        for(Intake_Students__c is : this.intake.Intake_Students__r) {
            enrolmentSet.add(is.Enrolment__c);
        } 

        return enrolmentSet;
    }
}