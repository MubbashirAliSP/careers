/**
 * @description Test class for `MetLeadConvertToPersonAccountButtonCX`
 * @author Biao Zhang
 * @date 07.MAR.2016
 */
 @isTest
public with sharing class METLeadConvertToPersonAccountCX_Test {
	@testSetup
	static void setup() {
		Id METLeadRecordTypeID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('MET Lead').getRecordTypeId();
		Lead l = new Lead();
		l.RecordTypeId = METLeadRecordTypeID;
		l.FirstName = 'test';
		l.LastName = 'test';
		l.Status = 'Open';
		l.I_am_a__c = 'Employer';
		l.Company = 'Company';
		l.Salutation = 'Mr';
		insert l;
	}

	static testMethod void controllerTest() {
		Lead l = [SELECT Company, Employer__c FROM Lead];

		Test.setCurrentPage(Page.METLeadConvertToPersonAccountButton);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(l);
			MetLeadConvertToPersonAccountButtonCX controller = new MetLeadConvertToPersonAccountButtonCX(stdController);

			PageReference result = controller.convertToPersonal();
			System.assertNotEquals(null, result);
		Test.stopTest();
	}
}