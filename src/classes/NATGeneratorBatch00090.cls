global class NATGeneratorBatch00090 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    public String libraryId;
    private String currentState;
    global String body90;
    private String LogId;

    global NATGeneratorBatch00090(String state, String q, String LId) {
        
        query = q;
        currentState = state;
        LogId = LId;
    
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body90 = '';  
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {

        if (currentState == 'Queensland') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionQLD.nat00090(), currentState);
                }
            }
        }
        
        else if (currentState == 'New South Wales') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionNSW.nat00090(), currentState);
                }
            } 
        }
        
        else if (currentState == 'New South Wales APL') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionNSWAPL.nat00090(), currentState);
                }
            } 
        }
        
        else if (currentState == 'Australian Capital Territory') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionACT.nat00090(), currentState);
                }
            } 
        }
        
        else if(currentState == 'Victoria') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionVIC.nat00090(), currentState);
                }
            }
        }
        
        else if (currentState == 'South Australia') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionSA.nat00090(), currentState);
                }
            } 
        }
        
        else if (currentState == 'Western Australia') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionWA.nat00090(), currentState);
                }
            } 
        }
        
        else if(currentState == 'Northern Territory') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionNT.nat00090(), currentState);
                }
            } 
        }
        
        else if(currentState == 'Tasmania') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionTAS.nat00090(), currentState);
                }
            } 
        } 
        
        else if(currentState == 'National') {
            for(SObject s :scope) { 
                Account a = (Account) s;
                if(a.Disability_Flag_0__c == 'Y') {
                    body90 += NATConstructor.natFile(a, NATDataDefinitionTAS.nat00090(), currentState);
                }
            } 
        } 
    }
    
    global void finish(Database.BatchableContext BC) {
        
        NAT_Validation_Log__c natLog = [ SELECT Id, Content_Library_Id__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Query_Students_3__c, Query_Students_4__c, Query_Students_5__c, Query_Students_6__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];

        
        if(body90 == '') {
            body90 = ' ';
        }
        
            Blob pBlob90 = Blob.valueof(body90);
            insert new ContentVersion (
                versionData =  pBlob90,
                Title = 'NAT00090',
                PathOnClient = '/NAT00090.txt',
                FirstPublishLocationId = natLog.Content_Library_ID__c
            );
        
       /* String queryStringStudents = '';
        
        List<Schema.FieldSetMember> fields = SObjectType.Nat_Validation_Log__c.FieldSets.Query_Students.getFields();
        
        for(Schema.FieldSetMember f : fields) { 
            queryStringStudents += natLog.get(f.getFieldPath());
        } */
        
        String queryStringStudents = '';
        
        if(natLog.query_students__c != null ) {
            queryStringStudents = natLog.query_students__c;
        }    
        if(natLog.query_students_2__c != null ) {
            queryStringStudents += natLog.query_students_2__c;
        }    
        if(natLog.query_students_3__c != null ) {
            queryStringStudents += natLog.query_students_3__c;
        }    
        if(natLog.query_students_4__c != null ) {
            queryStringStudents += natLog.query_students_4__c;
        }    
        if(natLog.query_students_5__c != null ) {
            queryStringStudents += natLog.query_students_5__c;
        }    
        if(natLog.query_students_6__c != null ) {
            queryStringStudents += natLog.query_students_6__c;
        }  
        
        Database.executeBatch(new NATGeneratorBatch00100( natLog.Validation_State__c, queryStringStudents, LogId  ) );
    }
}