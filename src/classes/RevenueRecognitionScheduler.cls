/**
 * @description Schedulable interface implementation for the Revenue Recognition batch
 * @author Unknown
 * @date Unknown
 * @history
 *       18.SEP.2015    Ranyel Maliwanag    Code cleanup
 */
public without sharing class RevenueRecognitionScheduler implements Schedulable {
    public RevenueRecognitionBatch batchProcess;

    public RevenueRecognitionScheduler() {
        batchProcess = new RevenueRecognitionBatch();
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(batchProcess);
    }
}