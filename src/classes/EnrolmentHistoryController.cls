public class EnrolmentHistoryController{
    private ID batchProcessId;
    public Opportunity StartDate {get;set;}
    public Opportunity EndDate {get;set;}
    public Opportunity FromDate {get;set;}
    public string limitId {get;set;}
    public integer batchSize { get; set; }
    
    public void init(){
        batchSize = 250;
        StartDate = new Opportunity(); //hack because we don't have inputDate in VF!
        EndDate = new Opportunity(); //hack because we don't have inputDate in VF!
        FromDate = new Opportunity(); //hack because we don't have inputDate in VF!
        StartDate.CloseDate = System.today();
        StartDate.CloseDate = startDate.CloseDate.toStartOfMonth().addMonths(-1);
        EndDate.CloseDate = System.today();
        FromDate.CloseDate = Date.newInstance(2013, 5, 1);
    }
  
    public void runBatchNow(){
        EnrolmentHistoryBatch b = new EnrolmentHistoryBatch(StartDate.CloseDate, EndDate.CloseDate+1, System.today().toStartOfMonth(), FromDate.CloseDate);
        b.limitId = limitId;
        
        batchProcessId = Database.executeBatch(b, batchSize); 
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch job started. You can see the progress here on on the Apex Jobs page'));
    }
    
    public void runNow(){
        //test only:
        EnrolmentHistoryBatch b = new EnrolmentHistoryBatch(StartDate.CloseDate, EndDate.CloseDate, System.today().toStartOfMonth(), FromDate.CloseDate);
        b.limitId = limitId;
        if ( limitId == null || (limitId.length() != 18 && limitId.length() != 15) ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Limit Id must be entered for non batch mode'));
            return;
        }
        List<sObject> scope = new List<sObject>();
        Database.QueryLocatorIterator it = b.start(null).iterator();
        while ( it.hasNext() )
            scope.add(it.next());
        b.execute(null, scope);
        //b.finish(null);
    }

    public void schedule(){
        try{
            Id id = System.schedule('Enrolment History Schedule', '0 0 0 1 * ?', 
                new EnrolmentHistoryScheduler(batchSize, FromDate.CloseDate));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Job scheduled as ' + id + '. You can manage your schedules from the Scheduled Jobs Page.'));
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
    }
    
    public boolean getIsBatchRunning(){
        return batchProcessId != null;
    }
    public AsyncApexJob getBatchStatus(){
        if ( batchProcessId != null ){
            AsyncApexJob ret = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:batchProcessId ];
            if ( ret.Status == 'Aborted' || ret.Status == 'Completed' || ret.Status == 'Failed'){
                batchProcessId = null;
            }
            return ret;
        }
        return null;
    }

}