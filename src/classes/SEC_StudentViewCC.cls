/**
 * @description Custom component controller for `SEC_StudentView` component
 * @author Ranyel Maliwanag
 * @date 15.JAN.2016
 */
public without sharing class SEC_StudentViewCC {
	// Reference attribute value to capture enrolmentId information
	public Id enrolmentId {
		get; 
		set {
			enrolmentId = value;
			// @TODO: Handle contact
			if (String.valueOf(enrolmentId.getSObjectType()) == 'Contact') {
				Contact cRecord = [SELECT Id, AccountId FROM Contact WHERE Id = :enrolmentId];
				enrolmentId = cRecord.AccountId;
			}
			if (String.valueOf(enrolmentId.getSObjectType()) == 'Account') {
				// Get enrolment Id
				List<Enrolment__c> enrolments = [SELECT Id, Enrolment_Status__c FROM Enrolment__c WHERE Student__c = :enrolmentId AND IsNotCurrentlyStudying__c = false ORDER BY CreatedDate DESC];
				if (enrolments.size() == 1) {
					enrolmentId = enrolments[0].Id;
				} else if (!enrolments.isEmpty()) {
					// Filter out active enrolments
					List<Enrolment__c> activeEnrolments = new List<Enrolment__c>();
					for (Enrolment__c record : enrolments) {
						if (record.Enrolment_Status__c.startsWithIgnoreCase('Active')) {
							activeEnrolments.add(record);
						}
					}

					if (activeEnrolments.size() == 1) {
						enrolmentId = activeEnrolments[0].Id;
					} else {
						enrolmentId = enrolments[0].Id;
					}
				}
			}
		}
	}

	// Reference variable for the Enrolment record to fetch details from
	public Enrolment__c enrolment {
		get {
			if (enrolment == null) {
				String enrolmentUnitsSubquery = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment_Unit__c);
				enrolmentUnitsSubquery = enrolmentUnitsSubquery.replace('Enrolment_Unit__c', 'Enrolment_Units__r');
				enrolmentUnitsSubquery += ')';

				String censusSubquery = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Census__c);
				censusSubquery = censusSubquery.replace(' Census__c', ' Censuses__r');
				censusSubquery += ')';

				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', Assigned_Trainer__r.Name, Assigned_Trainer__r.Title, Assigned_Trainer__r.Email, Assigned_Trainer__r.Phone' + enrolmentUnitsSubquery + censusSubquery);
				queryString += 'WHERE Id = :enrolmentId';
				enrolment = Database.query(queryString);
			}
			return enrolment;
		}
		set;
	}

	// Reference variable for the Account record related to the enrolment to fetch details from
	public Account student {
		get {
			if (student == null) {
				String serviceCaseSubquery = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Service_Cases__c);
		        serviceCaseSubquery = serviceCaseSubquery.replace('Service_Cases__c', 'Service_Cases__r');
		        serviceCaseSubquery += 'LIMIT 5';
		        serviceCaseSubquery += ')';

				String assessmentAttemptSubquery = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.AssessmentAttempt__c);
				assessmentAttemptSubquery = assessmentAttemptSubquery.replace('AssessmentAttempt__c', 'AssessmentAttempts__r');
				assessmentAttemptSubquery += 'LIMIT 5';
				assessmentAttemptSubquery += ')';

				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account, ', RecordType.Name' + serviceCaseSubquery + assessmentAttemptSubquery);
				queryString += 'WHERE Id = \'' + enrolment.Student__c + '\'';
				student = Database.query(queryString);
			}
			return student;
		}
		set;
	}

	public List<EnrolmentUnit> enrolmentUnits {
		get {
			if (enrolmentUnits == null) {
				enrolmentUnits = new List<EnrolmentUnit>();
				for (Enrolment_Unit__c enrolmentUnit : enrolment.Enrolment_Units__r) {
					enrolmentUnits.add(new EnrolmentUnit(enrolmentUnit));
				}
			}
			return enrolmentUnits;
		}
		set;
	}

	public Task taskForm {
		get {
			if (taskForm == null) {
				taskForm = new Task(
						WhoId = student.PersonContactId
					);
			}
			return taskForm;
		}
		set;
	}

	public Boolean isViewAlertModalVisible {get; set;}

	public Id currentAlertId {
		get;
		set {
			currentAlertId = value;
			if (String.isBlank(currentAlertId)) {
				currentAccountAlert = null;
			} else if (currentAlertId != null && alertMap.containsKey(currentAlertId)) {
				currentAccountAlert = alertMap.get(value);
			}
		}
	}


	public List<Event> accountAlerts {
		get {
			if (accountAlerts == null) {
				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Event);
				queryString += 'WHERE RecordType.Name = \'Account Alert\' AND WhatId = \'' + student.Id + '\'';
				accountAlerts = Database.query(queryString);

				for (Event alert : accountAlerts) {
					alertMap.put(alert.Id, alert);
				}
			}
			return accountAlerts;
		}
		set;
	}


	public List<Service_Cases__c> serviceCases {
		get {
			return student.Service_Cases__r;
		}
	}


	public List<AssessmentAttempt__c> assessmentAttempts {
		get {
			return student.AssessmentAttempts__r;
		}
	}

	public Task newTask {
		get {
			if (newTask == null) {
				newTask = new Task(
					OwnerId = UserInfo.getUserId(),
					RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
					WhoId = student.PersonContactId,
					ActivityDate = Date.today(),
					WhatId = enrolment.Id
				);
			}
			return newTask;
		}
		set;
	}

	public Task newCall {
		get {
			if (newCall == null) {
				newCall = new Task(
					OwnerId = UserInfo.getUserId(),
					RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
					Subject = 'Call',
					WhoId = student.PersonContactId,
					ActivityDate = Date.today(),
					WhatId = enrolment.Id,
					Status = 'Completed',
					Priority = 'Normal'
				);
			}
			return newCall;
		}
		set;
	}

	public Event newEvent {
		get {
			if (newEvent == null) {
				newEvent = new Event(
					OwnerId = UserInfo.getUserId(),
					RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Standard Event').getRecordTypeId(),
					WhoId = student.PersonContactId,
					WhatId = enrolment.Id
				);
			}
			return newEvent;
		}
		set;
	}

	public Date nextCensusDate {
		get {
			if (nextCensusDate == null) {
				for (Census__c census : enrolment.Censuses__r) {
					if (nextCensusDate == null || nextCensusDate < census.CensusDate__c) {
						nextCensusDate = census.CensusDate__c;
					}
				}
			}
			return nextCensusDate;
		}
		set;
	}


	public class EnrolmentUnit {
		public Enrolment_Unit__c record {get; set;}
		public String statusClass {
			get {
				statusClass = 'incomplete';
				if (record.AVETMISS_National_Outcome__c == '20') {
					statusClass = 'complete';
				} else if (record.Start_Date__c <= Date.today() && statusClass != '20') {
					statusClass = 'current';
				}
				return statusClass;
			}
			set;
		}

		public EnrolmentUnit(Enrolment_Unit__c enrolmentUnit) {
			this.record = enrolmentUnit;
		}
	}

	//public Map<Id, sObject> taskMap {
	//	get {
	//		if (taskMap == null) {
	//			taskMap = new Map<Id, sObject>();
	//		}
	//		return taskMap;
	//	}
	//	set;
	//}

	public Map<Id, Event> alertMap {
		get {
			if (alertMap == null) {
				alertMap = new Map<Id, Event>();
			}
			return alertMap;
		}
		set;
	}

	public Id taskDetailId {
		get; 
		set {
			taskDetailId = value;
			attachments = null;

			isTask = SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(taskDetailId) == 'Task' ? true : false;

			String queryString = '';
			if(isTask) {
				queryString += EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Task);
			} else {
				queryString += EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Event);
			}
			queryString += 'WHERE Id = :taskDetailId LIMIT 1';
			currentTask = Database.query(queryString);
		}
	}

	public sObject currentTask {get; set;}

	public boolean isTask {get; set;}

	public Event currentAccountAlert {
		get {
			if (currentAccountAlert == null) {
				currentAccountAlert = new Event(
						RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Account Alert').getRecordTypeId(),
						OwnerId = UserInfo.getUserId(),
						WhoId = student.PersonContactId,
						WhatId = student.Id,
						StartDateTime = Datetime.now(),
						EndDateTime = Datetime.now()
					);
			}
			return currentAccountAlert;
		}
		set;
	}

	public List<ActivityHistory> pastActivities {
		get {
			if (pastActivities == null) {
				//String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ActivityHistory);
				//queryString += 'WHERE IsClosed = true AND (WhoId = \'' + student.PersonContactId + '\' OR WhatId = \'' + enrolment.Id + '\' OR WhatId = \'' + student.Id + '\') ';
				//queryString += 'ORDER BY ActivityDate DESC NULLS LAST ';
				//queryString += 'LIMIT 20 ';
				//System.debug('queryString ::: ' + queryString);
				//pastActivities = Database.query(queryString);

				String extraQuery = ', (SELECT Subject, Id, OwnerId, ActivityDate, Description FROM ActivityHistories ORDER BY ActivityDate DESC NULLS LAST LIMIT 20) ';

				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account, extraQuery);
            	queryString += 'WHERE Id = \'' + student.Id + '\'';

            	Account student = Database.query(queryString);
            	pastActivities = student.ActivityHistories;

				//for (ActivityHistory record : pastActivities) {
				//	taskMap.put(record.Id, record);
				//}
			}
			return pastActivities;
		}
		set;
	}

	public List<OpenActivity> nextActivities {
		get {
			if (nextActivities == null) {
				//String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Task);
				//queryString += 'WHERE IsClosed = false AND (WhoId = \'' + student.PersonContactId + '\' OR WhatId = \'' + enrolment.Id + '\' OR WhatId = \'' + student.Id + '\') ';
				//queryString += 'ORDER BY ActivityDate ASC NULLS LAST ';
				////queryString += 'LIMIT 5 ';
				//nextActivities = Database.query(queryString);

				String extraQuery = ', (SELECT Subject, Id, OwnerId, ActivityDate, Description FROM OpenActivities ORDER BY ActivityDate ASC NULLS LAST) ';

				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account, extraQuery);
            	queryString += 'WHERE Id = \'' + student.Id + '\'';

            	Account student = Database.query(queryString);
            	nextActivities = student.OpenActivities;


				//for (OpenActivity record : nextActivities) {
				//	taskMap.put(record.Id, record);
				//}
			}
			return nextActivities;
		}
		set;
	}

	public String defaultCategory {get; set;}
	public Boolean isCategoryModalVisible {get; set;}
	public Boolean isTaskModalVisible {get; set;}
	public boolean isAccountAlertModalVisible {get; set;}

	public List<Attachment> attachments {
		get {
			if (attachments == null) {
				if (String.isNotBlank(taskDetailId)) {
					// Fetch attachments for the current task
					String queryString = 'SELECT Id, Name FROM Attachment ';
					queryString += 'WHERE ParentId = :taskDetailId';
					attachments = Database.query(queryString);
					attachments.add(new Attachment());
				} else {
					attachments = new List<Attachment>{new Attachment(),new Attachment(),new Attachment()};
				}
			}
			return attachments;
		}
		set;
	}

	public PageReference saveNewTask() {
		insert newTask;

		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (String.isBlank(record.Id) && record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = newTask.Id;
				validAttachments.add(record);
			}
		}
		insert validAttachments;

		newTask = null;
		pastActivities = null;
		nextActivities = null;
		attachments = null;
		return null;
	}

	public PageReference saveNewEvent() {
		insert newEvent;

		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (String.isBlank(record.Id) && record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = newEvent.Id;
				validAttachments.add(record);
			}
		}
		insert validAttachments;

		newEvent = null;
		pastActivities = null;
		nextActivities = null;
		attachments = null;
		return null;
	}

	public PageReference saveNewCall() {
		insert newCall;

		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (String.isBlank(record.Id) && record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = newCall.Id;
				validAttachments.add(record);
			}
		}
		insert validAttachments;

		newCall = null;
		pastActivities = null;
		nextActivities = null;
		attachments = null;
		return null;
	}

	public PageReference saveNewAccountAlert() {
		System.debug('Subject ::: ' + currentAccountAlert.Subject);
		insert currentAccountAlert;
		System.debug('Subject 2 ::: ' + currentAccountAlert.Subject);
		System.debug('currentAccountAlert ::: ' + currentAccountAlert.Id);
		accountAlerts = null;
		return null;
	}

	public PageReference saveCurrentTask() {
		update currentTask;
		pastActivities = null;
		nextActivities = null;
		return resetCategoryModal();
	}

	public PageReference saveAccountCategory() {
		update student;
		return resetCategoryModal();
	}

	public PageReference updateEnrolment() {
		update enrolment;
		return null;
	}


	public PageReference resetCategoryModal() {
		return null;
	}

	public PageReference deleteAlert() {
		delete currentAccountAlert;
		accountAlerts = null;
		return null;
	}


	public PageReference initialiseAccountCategory() {
		return null;
	}

	public Id attachmentIdForDelete {get; set;}
	public PageReference deleteAttachment() {
		if (String.isNotBlank(attachmentIdForDelete)) {
			delete new Attachment(Id = attachmentIdForDelete);
		}
		attachments = null;
		return null;
	}

	public PageReference uploadAttachment() {
		System.debug('taskDetailId ::: ' + taskDetailId);
		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (String.isBlank(record.Id) && record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = taskDetailId;
				validAttachments.add(record);
			}
		}

		insert validAttachments;
		attachments = null;
		return null;
	}
}