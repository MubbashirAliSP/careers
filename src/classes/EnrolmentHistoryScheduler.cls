global class EnrolmentHistoryScheduler implements Schedulable{
   private integer batchSize;
   private Date fromDate;
   
    public EnrolmentHistoryScheduler(integer batchSize, Date fromDate){
        this.batchSize = batchSize;
        this.fromDate = fromDate;
    }
    
   global void execute(SchedulableContext SC) {
       Date startDate = System.today().toStartOfMonth().addMonths(-1);
       Date EndDate = System.today().toStartOfMonth();
       EnrolmentHistoryBatch b = new EnrolmentHistoryBatch(startDate, System.today() + 1, EndDate, fromDate);
       Id batchProcessId = Database.executeBatch(b, batchSize);
   }
   
}