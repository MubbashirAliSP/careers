/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Controller extension class for WorkplaceVisit vf page that lets user select another unit to visit
Test Class:
History
09/01/2012     Created		Sairah Hadjinoor
------------------------------------------------------------*/
public with sharing class WorkPlaceVisit_CX {
	
	public Event thisEvent{get;set;}
	public Contact thisCon{get;set;}
	public String strStartDate{get;set;}
	public String strEndDate{get;set;}
	public Enrolment__c thisEnrolment{get;set;}
	public User thisUser{get;set;}
	public List<Enrolment_Unit__c> eUnitList{get;set;}
	public List<UnitWrapper> unitWrapperList{get;set;}
	public Enrolment_Unit__c insertUnit{get;set;}
	public String enrolUnitId{get;set;}
	public Link__c thisLink{get;set;}
	
	public WorkPlaceVisit_CX(ApexPages.StandardController controller){
		thisEvent = (Event) controller.getRecord();
		
		thisEvent = [Select WhatId,WhoId,Subject,RecordTypeId,RecordType.Name,OwnerId,Owner.Name,
					StartDateTime,EndDateTime,Unit_Delivered__c,Unit_Delivered_Link__c from Event where Id=: thisEvent.id];
		
		pullData();
	}
	
	public void pullData(){
		if(thisEvent.WhoId!=null){
			thisCon = [Select Name,Phone,Account.Employer__r.Name,Account.Employer__r.Phone from Contact where id=:thisEvent.WhoId];
		}
		strStartDate = thisEvent.StartDateTime.format();
		strEndDate = thisEvent.EndDateTime.format();
		
		if(thisEvent.WhatId!=null){
			thisEnrolment = [Select Name,Employer__r.Name,Employer__r.Phone from Enrolment__c where Id=: thisEvent.WhatId];
			if(thisEnrolment.id!=null){
				eUnitList= [Select Unit__r.Name,Unit_Name__c,Enrolment__c,Unit_Type__c,Purchasing_Contract_Identifier__c,Start_Date__c,
							End_Date__c,Delivery_Mode__r.Name,Training_Delivery_Location__c,Unit_Result__c from Enrolment_Unit__c  where Enrolment__c=:thisEnrolment.Id];
			}
		}
		
		unitWrapperList = new List<UnitWrapper>();
		for(Enrolment_Unit__c eUnit: eUnitList){
			UnitWrapper uWrap = new UnitWrapper(eUnit);
			unitWrapperList.add(uWrap);
		}
	}
	
	public class UnitWrapper{
		public Enrolment_Unit__c thisEnrolUnit{get;set;}
		public Boolean isSelected{get;set;}
		
		public UnitWrapper(Enrolment_Unit__c enrolUnit){
			thisEnrolUnit = enrolUnit;
			isSelected = false;
		}
	}
	
	public PageReference save(){
		insertUnit = [select Id,Name,Unit__r.Name from Enrolment_Unit__c where Id=:enrolUnitId];
		thisLink = [Select Id,Name,Url_link__c from Link__c where Name=:'Salesforce Link' LIMIT 1];
		
		thisEvent.Unit_Delivered__c = insertUnit.Unit__r.Name;
		thisEvent.Unit_Delivered_Link__c = thisLink.Url_link__c +'/'+ insertUnit.id;
		update thisEvent;
		
		PageReference pageRef = new PageReference('/'+thisEvent.id);
        pageRef.setredirect(true);
		
		return pageRef;
	}
	
	public PageReference cancel(){
		PageReference pageRef = new PageReference('/'+thisEvent.id);
        pageRef.setredirect(true);
		
		return pageRef;
	}

}