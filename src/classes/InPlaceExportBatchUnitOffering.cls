/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchUnitOffering implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchUnitOffering() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPUO_Calendar_Sequence_Number__r.IPCAL_Calendar_Seq__c, IPUO_Campus_Code__c, IPUO_InPlace_Unit_Offering__c, IPUO_Sync_on_next_update__c, IPUO_Unit_Code__c, IPUO_Unit_Offering_Code__c, IPUO_Unit_Version__c, Name, Id ';
        queryString += 'FROM Intake_Unit__c ';
        queryString += 'WHERE IPUO_InPlace_Unit_Offering__c = true AND IPUO_Sync_on_next_update__c = true';
        //queryString += ' AND IPUO_Campus_Code__c = \'IPC-0415\'';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Intake_Unit__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Intake_Unit__c record : records) {
            if (!exportedIdentifiers.contains(record.IPUO_Unit_Offering_Code__c)) {
                InPlace__c forExport = new InPlace__c(
                        IPUnitOffering_CalendarSeqNum__c = record.IPUO_Calendar_Sequence_Number__r.IPCAL_Calendar_Seq__c,
                        IPUnitOffering_CampusCode__c = record.IPUO_Campus_Code__c,
                        IPUnitOffering_UnitClass__c = '',
                        IPUnitOffering_UnitCode__c = record.IPUO_Unit_Code__c,
                        IPUnitOffering_UnitOfferingCode__c = record.IPUO_Unit_Offering_Code__c,
                        IPUnitOffering_UnitVersion__c = record.IPUO_Unit_Version__c
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPUO_Unit_Offering_Code__c);
            }
            
            record.IPUO_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchStudentUnitOffering nextBatch = new InPlaceExportBatchStudentUnitOffering();
        Database.executeBatch(nextBatch);
    }
}