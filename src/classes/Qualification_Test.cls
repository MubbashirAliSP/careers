@isTest
public class Qualification_Test {
    static Qualification__c masterObject;
    
    private static void init(){
        masterObject = new Qualification__c(Name='Test Qualification', Qualification_Name__c = 'TEST', Description__c = 'TEST', Training_Package_Order__c=1);
        insert masterObject;
        
        List<Unit__c> unitList = new List<Unit__c>();
        for (Integer i = 0; i<3; i++){
            unitList.add(new Unit__c(Name = 'Test Unit ' + i));
        }
        insert unitList;
        
        List<Qualification_Unit__c> quList = new List<Qualification_Unit__c>();
        for(Unit__c u: unitList){
            quList.add(new Qualification_Unit__c(Qualification__c = masterObject.Id, Unit__c = u.Id));
        }
        insert quList;
    }
    static testMethod void testPageView(){
    
         
        
        init();
        PageReference pageRef = Page.QualificationClone;
        Test.setCurrentPageReference(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
        
        QualificationCloneCtrl ctrl = new QualificationCloneCtrl(con);
        ctrl.allChecked = true;
        ctrl.CheckAll();
        System.assertEquals(true, ctrl.allQualificationUnits[0].selected);
        ctrl.saveNew();
        System.assertEquals(3, ctrl.allQualificationUnits.size());
    }
}