global class USI_UtilityClass {
    
    
    global static Account setUSIBatch(String usi, String studentId) {
        
        Account usiAccount = new Account(Id = studentId, Unique_Student_Identifier__c = usi, Validated_By_Webservice__c = true, Date_Of_Last_Sync__c = date.today() );
        return usiAccount;
    
    }
    
    global static Account verifyUSIBatch(String studentId) {
        
        Account usiAccount = new Account(Id = studentId, Validated_By_Webservice__c = true, Date_Of_Last_Sync__c = date.today() );
        return usiAccount;
    
    }
    
    global static Account createUsiFailureBatch(String studentId) {
        
        Account usiAccount = new Account(Id = studentId, invalidated_By_Webservice__c = true, Sync_Error__c = true, Date_Of_Last_Sync__c = date.today());
        return usiAccount;
    
    }
    
    global static Service_Cases__c createServiceCase(String studentId, String reason, String event) {
        
        RecordType rt = [ SELECT Id,Name FROM RecordType WHERE Name = 'USI Support Request' AND SobjectType = 'Service_Cases__c' ];
        
        Service_Cases__c svcCase = new Service_Cases__c(Account_Student_Name__c = studentId, Description__c = reason, RecordTypeId =  rt.Id ,USI_Issue_Type__c=event, Requires_USI_Assistance__c=true, Subject__c='USI Generation/Verification Issue');
        insert svcCase;
        return svcCase;
        
    }
    
     global static void closeServiceCase(String studentId) {
             
        RecordType rt = [ SELECT Id,Name FROM RecordType WHERE Name = 'USI Support Request' AND SobjectType = 'Service_Cases__c' ];
         
        List<Service_Cases__c> sCaseToClose =  [SELECT Id,Status__c,Description__c From Service_Cases__c Where Account_Student_Name__c = : studentId AND RecordTypeId = :rt.Id];
                
        for(Service_Cases__c s : sCaseToClose) {
            s.Status__c = 'Closed';
        }
        
        update sCaseToClose;
        
       
    }
        
}