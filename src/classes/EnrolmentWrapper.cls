/*
 * @description Wrapper class for enrolment object. Used for HEIMS generation.
 * @author D. Crisologo (CloudSherpas)
 * @date 19.JUN.2013
*/
global class EnrolmentWrapper {
        
    public Boolean selected {get;set;}
    public Enrolment__c enrolment {get;set;}
    
    public EnrolmentWrapper(Boolean sel, Enrolment__c e) {
        selected = sel;
        enrolment = e;
    }
    
}