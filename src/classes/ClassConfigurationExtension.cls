public with sharing class ClassConfigurationExtension {
    
    public final Class_Configuration__c classConfig;
    
    public String selectedIntakeUnit {get;set;}
    
    public List<SelectOption> intakeUnits {get;set;}
    
    

    public ClassConfigurationExtension(ApexPages.StandardController stdController) {
        
        Id recordtypeId = [Select Id From Recordtype Where DeveloperName = 'One_Off_Class'].Id;
        intakeUnits = new List<SelectOption>();
        this.classConfig = (Class_Configuration__c)stdController.getRecord();
        
        classConfig.Intake__c = ApexPages.currentPage().getParameters().get('intakeId');
        classConfig.RecordTypeId = recordtypeId;
        
        
        intakeUnits.add(new SelectOption('','--None--'));
        
        for(Intake_Unit__c iu : [Select id, Unit_of_Study__r.Name,Name, Unit_Name__c From Intake_Unit__c Where Intake__c = : ApexPages.currentPage().getParameters().get('intakeId')]) {
        
            intakeUnits.add(new SelectOption(iu.Id, iu.Unit_of_Study__r.Name  + ' ' + '-' + ' ' + iu.Unit_Name__c ));
        
        }
        
    }
    
    public pageReference saveCustom(){
    
        classConfig.intake_unit__c = selectedIntakeUnit;
        
        try {
        insert classConfig;
        
        } catch(Exception e){ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Class Configuration could not be saved'); return null;}
        
        return new pageReference('/' + ApexPages.currentPage().getParameters().get('intakeId'));
    }
    
    public pageReference cancelCustom(){
    
        return new pageReference('/' + ApexPages.currentPage().getParameters().get('intakeId'));
    }
    
}