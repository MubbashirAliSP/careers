global class NATGeneratorBatch00060_2 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global String currentState;
    global String body60;
    global String LogId;
    global Map<Id,Decimal> Unit_NominalHoursMap = new Map<Id,Decimal>();
    global String unitIds;

    global NATGeneratorBatch00060_2(String state, Map<Id,Decimal> unitMap, String LId ) {
        
        unitIds = '(';
        
        for(String s : unitMap.keySet() ){
            unitIds += '\'' + s + '\',';
        }
        
        unitIds = unitIds.substring(0, unitIds.length() -1 ) + ')';
            
        Unit_NominalHoursMap = unitMap;
        query = 'SELECT Id, Name, Unit_Name__c, Field_of_Education_Identifier__c, Vet_Non_Vet__c, ' +
            	'Unit_Flag__c FROM Unit__c WHERE ID in ' + unitIds;

        LogId = LId;
        currentState = state;
        
    }
        
    global database.querylocator start(Database.BatchableContext BC) {
       body60 = '';
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
         
        if (currentState == 'Queensland') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){                    
                    body60 += NATConstructor.natFile(un, NATDataDefinitionQLD.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if (currentState == 'New South Wales') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionNSW.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        else if (currentState == 'New South Wales APL') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionNSW.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if (currentState == 'Australian Capital Territory') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionACT.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if(currentState == 'Victoria') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionVIC.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if (currentState == 'South Australia') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionSA.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if (currentState == 'Western Australia') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionWA.nat00060(), String.valueOf(Unit_NominalHoursMap.get(un.id).round()));
                    body60 += '\r\n';
                }		
            }	
        }
        
        else if(currentState == 'Northern Territory') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionNT.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
        
        else if(currentState == 'Tasmania') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinitionTAS.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        } 
        else if(currentState == 'National') {
            for(SObject s : scope) {
                Unit__c un = (Unit__c) s;
                if(Unit_NominalHoursMap.containsKey(un.Id)){
                    body60 += NATConstructor.natFile(un, NATDataDefinition.nat00060(), currentState);
                    body60 += NATGeneratorFormat.formatNumber(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), 4);
                    body60 += '\r\n';
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        NAT_Validation_Log__c natLog = [ SELECT Id, Content_Library_Id__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Query_Students_3__c, Query_Students_4__c,Query_Students_5__c, Query_Students_6__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
        
        if(body60 == '') {
            body60 = ' ';
        }
        
        Blob pBlob60 = Blob.valueof(body60);
        insert new ContentVersion(
            versionData =  pBlob60,
            Title = 'NAT00060',
            PathOnClient = '/NAT00060.txt',
            FirstPublishLocationId = natLog.Content_Library_ID__c
        );
        
        /*String queryStringStudents = '';
        
        List<Schema.FieldSetMember> fields = SObjectType.Nat_Validation_Log__c.FieldSets.Query_Students.getFields();
        
        for(Schema.FieldSetMember f : fields) { 
            queryStringStudents += natLog.get(f.getFieldPath());
        } */
        
        String queryStringStudents = '';
        
        if(natLog.query_students__c != null ) {
            queryStringStudents = natLog.query_students__c;
        }    
        if(natLog.query_students_2__c != null ) {
            queryStringStudents += natLog.query_students_2__c;
        }    
        if(natLog.query_students_3__c != null ) {
            queryStringStudents += natLog.query_students_3__c;
        }    
        if(natLog.query_students_4__c != null ) {
            queryStringStudents += natLog.query_students_4__c;
        }    
        if(natLog.query_students_5__c != null ) {
            queryStringStudents += natLog.query_students_5__c;
        }    
        if(natLog.query_students_6__c != null ) {
            queryStringStudents += natLog.query_students_6__c;
        }  
        
        Database.executeBatch(new NATGeneratorBatch00080( natLog.Validation_State__c, queryStringStudents, LogId  ) );
    }
}