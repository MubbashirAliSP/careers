@RestResource(urlMapping='/saml-user-notify')
global class SamlUserCallbackService {
    @HttpGet
    global static String doGet() {
        return 'Saml user notify is online';
    }
    
    @HttpPost
    global static String doPost(String id, String status) {
        Account a = new Account(id=id);
        a.Community_Access__c = (status == 'SUCCESS')? 'Enabled': 'Failed';
        update a;
        return 'OK';
    }

}