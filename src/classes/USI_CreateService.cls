global class USI_CreateService {
    
    global static Integer reSyncCount = 0;
    
    global class usiCreateResponse {
        String USI;
        String message;
        String status;
    }
    
    global class usiVerifyResponse {
        String dateofbirth; 
        String firstname;
        String usi;
        String message;
        String lastname;
    }
    
    public class internalServerException extends Exception {
        //just for throwing exception
    }

    
    public static USI_Identification_Document__c getDocument (Id studentId) {
        
       USI_Identification_Document__c document = [ SELECT Id, Acquisition_Date__c, Card_Colour__c, Certificate_Number_Encrypted__c, Country_Of_Issue__c, 
                                                   Date_Printed__c,Document_Number_Encrypted__c, Expiry_Date_Encrypted__c, Immi_Card_Number_Encrypted__c,
                                                   Individual_Reference_Number_Encrypted__c, Licence_Number_Encrypted__c, Medicare_Card_Number_Encrypted__c,
                                                   Name_Line_1_Encrypted__c, Name_Line_2_Encrypted__c, Name_Line_3_Encrypted__c, Name_Line_4_Encrypted__c,
                                                   Passport_Number_Encrypted__c, Registration_Date__c, Registration_Number_Encrypted__c, Registration_State__c,
                                                   Registration_Year__c, State__c, Stock_Number_Encrypted__c, Account__c, Account__r.FirstName, RecordType.Name,
                                                   Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.Sex_Identifier__c,
                                                   Account__r.Country_of_Birth__r.Name,Account__r.Country_Studying_In__r.Name, Account__r.Preferred_Contact_Method__c,
                                                   Account__r.Address_Country__r.Name, Account__r.Address_building_property_name__c, Account__r.Address_street_number__c,
                                                   Account__r.Address_street_name__c, Account__r.Address_Post_Code__c, Account__r.Suburb_locality_or_town__c,
                                                   Account__r.Reporting_Billing_State__r.Name, Account__r.Town_City_Of_Birth__c, Account__r.PersonHomePhone, 
                                                   Account__r.PersonMobilePhone, Account__r.Student_Identifer__c, Account__r.National_Provider_Number__c
                                                   FROM USI_Identification_Document__c WHERE Account__c =: studentId ];
        
        return document;
    } 
    
    public static Enrolment__c getEnrolment (Id studentId) {

        Enrolment__c enrl = [ SELECT Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c,Delivery_Location__r.Training_Organisation__r.Name, Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c,Delivery_Location__r.Training_Organisation__r.ABN__c 
                              FROM Enrolment__c
                              WHERE Student__c = :studentId AND (
                                    Delivery_Location__r.Training_Organisation__r.Name = 'Careers Australia Education Institute Pty Ltd' OR
                                    Delivery_Location__r.Training_Organisation__r.Name = 'Careers Australia Institute Of Training'
                                    )
                             ORDER BY CreatedDate LIMIT 1 ];
        
        return enrl;
    }
    
    @future (callout=true)
    public static void createUSI(Id studentId, Id logId) {
        
        String sentBody;
        USI_Integration_Event__c event = new USI_Integration_Event__c();
        
        HTTPresponse createUSIres = new HTTPResponse();
        USI_Identification_Document__c document;
        Enrolment__c enrl;

        try {
            
            document = getDocument(studentId);
            enrl = getEnrolment(studentId);
            
            try {
                sentBody = JSON.serializePretty(document);
                createUSIres =  USI_WebService.createUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                event.JSON__c = sentBody;
                usiCreateResponse uro;
                if(createUSIres.getStatusCode() == 200 ) {
                    uro = (usiCreateResponse) JSON.deserialize(createUSIres.getBody(), usiCreateResponse.class); 
                    System.debug('uro : ' + uro);
                }
                if(createUSIres.getStatusCode() == 500 ) {
                    throw new internalServerException('Internal Server Error');
                }
                                
                //only retry with this error message
                if (uro.message == 'Internal USI Error Occured, Please Check Heroku logs for more details' ) {
                    //event = USI_LogManager.errorHandle(uro.message, studentId, logId, true);
                    event = USI_LogManager.errorHandle(uro.message, studentId, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, logId, true);
                    insert event;
                	createUSIres =  USI_WebService.createUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                    uro = (usiCreateResponse) JSON.deserialize(createUSIres.getBody(), usiCreateResponse.class); 
                    
                }

            if(uro.USI.length() ==10 && !uro.status.equalsIgnoreCase('failure')) {
                System.debug('failure');
                try {
                    setUSI(uro.USI, studentId,enrl);
                    event = USI_LogManager.createUSIEvent(logId, studentId, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, uro.message, true);
                    USI_UtilityClass.closeServiceCase(studentId);
                }
                catch (exception e) {
                    event = USI_LogManager.errorHandle(e.getMessage() + ' ' + e.getLineNumber(), studentId, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, logId, true);
                    event.JSON__c = sentBody;
                }
            }
            else {
                System.debug('not failure');
                event = USI_LogManager.errorHandle(uro.message, studentId, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, logId, true);
                event.JSON__c = sentBody;
                createUsiFailure(studentId);
            }
        }
            
            catch (exception e) {
                event = USI_LogManager.errorHandle(e.getMessage() + ' ' + e.getLineNumber(), studentId, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, logId, true);
                event.JSON__c = sentBody;
                System.debug('first catch');
            }
            
            event.Received_JSON__c = createUSIRes.getBody();
            
        }
        catch (exception e) {
            event = USI_LogManager.errorHandle('Student Must Have USI Identification Document Attached And Have At Least 1 Enrolment', studentId, null, null, logId, true);
            System.debug('first catch');
        }
        event.JSON__c = sentBody;
        insert event;
    }            
    
    @future (callout=true)
    public static void verifyUSI(Id studentId, Id logId) {
        
        String sentBody;
              
        USI_Integration_Event__c event = new USI_Integration_Event__c();
        HTTPresponse verifyUSIres = new HTTPResponse();
        Account student;
        Enrolment__c enrl;
        try {
            student = [ SELECT ID, FirstName, LastName, PersonBirthDate, Unique_student_identifier__c, Student_Identifer__c FROM Account WHERE Id =: studentId ];
            enrl = getEnrolment(studentId);
            
           
            try {                 
                sentBody = JSON.serializePretty(student);
                event.JSON__c = sentBody;
                verifyUSIres =  USI_WebService.verifyUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                usiVerifyResponse uro = (usiVerifyResponse) JSON.deserialize(verifyUSIres.getBody(), usiVerifyResponse.class); 
                
                if (uro.message == 'Internal USI Error Occured, Please Check Heroku logs for more details' ) {
                    event = USI_LogManager.errorHandle(uro.message, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
                    insert event;
                    
                    verifyUSIres =  USI_WebService.verifyUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                    uro = (usiVerifyResponse) JSON.deserialize(verifyUSIres.getBody(), usiVerifyResponse.class); 
                }
                
                if(uro.USI.equalsIgnoreCase('valid') && uro.Firstname.equalsIgnoreCase('match') && uro.Lastname.equalsIgnoreCase('match') && uro.DateOfBirth.equalsIgnoreCase('match') ) {
                    try {
                        verifyUSI(studentId, enrl);
                        event = USI_LogManager.createUSIEvent(logId, studentId, student.FirstName + ' ' + student.LastName, student.Student_Identifer__c, uro.message, true);
                        USI_UtilityClass.closeServiceCase(studentId);
                    }
                    catch (exception e) {
                        event = USI_LogManager.errorHandle(e.getMessage() + ' at line ' + String.valueOf(e.getLineNumber()), student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, studentId, logId, false);
                        event.JSON__c = sentBody;
                        createUsiFailure(studentId);
                    }
                }
                if(uro.USI.equalsIgnoreCase('invalid')) {
                    event = USI_LogManager.errorHandle('USI is Invalid, First Name: ' + uro.firstname + ', Last Name: ' + uro.lastname + ', DOB: ' + uro.dateofbirth, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
                    event.JSON__c = sentBody;
                    createUsiFailure(studentId);
                }
                
                //This is when usi is valid, but personal details are invalid
                if(uro.USI.equalsIgnoreCase('valid') && (uro.Firstname.equalsIgnoreCase('nomatch') || uro.Lastname.equalsIgnoreCase('nomatch') || uro.DateOfBirth.equalsIgnoreCase('nomatch') )) {
                    
                    String noMatchCode = '';
                    
                    if(uro.Firstname.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'Firstname does not match ';
                    if(uro.Lastname.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'Lastname does not match ';
                    if(uro.DateOfBirth.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'DOB does not match ';
                    
                    event = USI_LogManager.errorHandle(noMAtchCode + ',First Name: ' + uro.firstname + ', Last Name: ' + uro.lastname + ', DOB: ' + uro.dateofbirth, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
                    event.JSON__c = sentBody;
                    createUsiFailure(studentId);
                }
                
                if(uro.USI == '' || uro.USI == null) {
                    event = USI_LogManager.errorHandle(uro.message, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
                    event.JSON__c = sentBody;
                    createUsiFailure(studentId);
                }
            }

            catch (exception e) {
                event = USI_LogManager.errorHandle(e.getMessage(), studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
                event.JSON__c = sentBody;
                createUsiFailure(studentId);
            }
            event.Received_JSON__c = verifyUSIRes.getBody();
        }
        catch (exception e) {
            event = USI_LogManager.errorHandle(e.getMessage(), studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, logId, false);
        }
    
        insert event;
        
    }
    
    WebService static void singleCreateUsi(Id recid) {

        USI_Integration_Log__c usiLog = USI_LogManager.createUSILog();
        createUSI(recid, usiLog.Id);
          
    }
     
    WebService static void singleVerifyUsi(string recid) {
               
        USI_Integration_Log__c usiLog = USI_LogManager.createUSILog();
        verifyUSI(recid, usiLog.Id);

    }
    
    private static void setUSI(String usi, String studentId, Enrolment__c enrol) {
        
        Account usiAccount = new Account(Id = studentId, Unique_Student_Identifier__c = usi, Validated_By_Webservice__c = true,Sync_Error__c = false,Invalidated_By_Webservice__c = false, Date_Of_Last_Sync__c = date.today(), RTO_Used_For_USI_Creation__c = enrol.Delivery_Location__r.Training_Organisation__r.Name + ' ' + enrol.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c);
        //Account usiAccount = new Account(Id = studentId, Unique_Student_Identifier__c = usi, Validated_By_Webservice__c = true,Sync_Error__c = false,Invalidated_By_Webservice__c = false, Date_Of_Last_Sync__c = date.today(), RTO_Used_For_USI_Creation__c = 'test');
        System.debug(' enrol.Delivery_Location__r.Training_Organisation__r.Name  :' +  enrol.Delivery_Location__r.Training_Organisation__r.Name );
        update usiAccount;
    
    }
    
    private static void verifyUSI(String studentId, Enrolment__c enrol) {
        
        Account usiAccount = new Account(Id = studentId, Validated_By_Webservice__c = true,Invalidated_By_Webservice__c = false,Sync_Error__c = false, Date_Of_Last_Sync__c = date.today(), RTO_Used_For_USI_Creation__c = enrol.Delivery_Location__r.Training_Organisation__r.Name + ' ' + enrol.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c);
        update usiAccount;
    
    }
    
    private static void createUsiFailure(String studentId) {
        
        Account usiAccount = new Account(Id = studentId, invalidated_By_Webservice__c = true, Sync_Error__c = true, Date_Of_Last_Sync__c = date.today());
        update usiAccount;
    
    }
    

    global static Account createUsiFailureBatch(String studentId) {
        
        Account usiAccount = new Account(Id = studentId, invalidated_By_Webservice__c = true, Validated_By_Webservice__c = false,Sync_Error__c = true, Date_Of_Last_Sync__c = date.today());
        return usiAccount;
    
    }
}