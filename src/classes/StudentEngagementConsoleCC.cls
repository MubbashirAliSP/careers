/**
 * @description Custom controller for the Student Engagement Console
 * @author Ranyel Maliwanag
 * @date 06.JAN.2016
 */
public without sharing class StudentEngagementConsoleCC {
	// List of all visible tabs in SEC
	public List<SEC_Tab__mdt> tabs {get; set;}
	// Current active tab
	public SEC_Tab__mdt currentTab {get; set;}
	// List of all visible containers in the current tab
	public List<Container> containers {get; set;}
	// Reference map for List views per Container
	public Map<String, List<SEC_ListView__mdt>> containerListViews {get; set;}
	// Reference string that controls the current tab that's displayed on the SEC
	public String currentTabName {get; set;}
	// Reference Id for the current trainer accessing the SEC
	public Id currentTrainer {get; set;}
	// Reference Id for the current student enrolment that will be displayed on student view mode
	public Id studentId {
		get {
			if (studentId == null) {
				if (ApexPages.currentPage().getParameters().containsKey('eid') && String.isNotBlank(ApexPages.currentPage().getParameters().get('eid'))) {
					studentId = ApexPages.currentPage().getParameters().get('eid');
				}
			}
			return studentId;
		}
		set;
	}
	// Reference variable to determine whether or not the console is on student view mode
	public Boolean isStudentViewMode {
		get {
			return studentId != null;
		}
	}
	// List of all Trainers
	public transient List<User> trainers;
	// Reference map for accessing User records by Id
	public Map<Id, User> userMap {
		get {
			if (userMap == null) {
				userMap = new Map<Id, User>(trainers);
			}
			return userMap;
		}
		set;
	}
	public String currentTrainerName {
		get {
			if (currentTrainer == UserInfo.getUserId()) {
				currentTrainerName = UserInfo.getName();
			} else if (userMap.containsKey(currentTrainer)) {
				currentTrainerName = userMap.get(currentTrainer).Name;
			} else {
				currentTrainerName = null;
			}
			return currentTrainerName;
		}
		set;
	}

	// List of all trainers as select options
	public List<SelectOption> trainerOptions {
		get {
			if (trainerOptions == null) {
				trainerOptions = new List<SelectOption>();
				for (User trainer : trainers) {
					trainerOptions.add(new SelectOption(trainer.Id, trainer.Name));
				}
			}
			return trainerOptions;
		}
		set;
	}

	// Custom colour settings
	public List<SEC_Colour__mdt> colours {
		get {
			if (colours == null) {
				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_Colour__mdt);
				colours = Database.query(queryString);
			}
			return colours;
		}
		set;
	}

	// Private variable declarations for internal references
	private transient Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

	public Id viewAs {get; set;}


	/**
	 * @description Default custom controller constructor
	 * @author Ranyel Maliwanag
	 * @date 06.JAN.2016
	 */
	public StudentEngagementConsoleCC() {
		// Check if tabname is supplied as a parameter
		if (ApexPages.currentPage().getParameters().containsKey('tab')) {
			String tabName = ApexPages.currentPage().getParameters().get('tab');
			if (String.isNotBlank(tabName)) {
				currentTabName = tabName;
			}
		}
		// Fetch list of visible tabs in SEC
		tabs = [SELECT MasterLabel, DeveloperName, SEC_SortOrder__c FROM SEC_Tab__mdt ORDER BY SEC_SortOrder__c ASC];
		// Check if existing tabname is valid
		if (String.isNotBlank(currentTabName)) {
			Boolean isValid = false;
			for (SEC_Tab__mdt tab : tabs) {
				if (tab.DeveloperName == currentTabName) {
					isValid = true;
					break;
				}
			}

			if (!isValid) {
				currentTabName = null;
			}
		}
		// If the current tab name checks out fine, use it. Otherwise, use the first tab returned
		if (String.isBlank(currentTabName)) {
			// Assign the first tab as the current one
			currentTab = tabs.get(0);
			currentTabName = currentTab.DeveloperName;
		}

		// Check if currentTrainerParameter is passed
		if (ApexPages.currentPage().getParameters().containsKey('viewAs')) {
			viewAs = ApexPages.currentPage().getParameters().get('viewAs');
			currentTrainer = viewAs;
		} else {
			// Assign the current user as the initial trainer
			currentTrainer = UserInfo.getUserId();
		}

		// Initialise the tab contents
		loadTabContents();

		trainers = getTrainers();
		System.debug('trainers ::: ' + trainers.size());
	}

	public List<User> getTrainers() {
		return [SELECT Id, Name FROM User WHERE (UserRole.DeveloperName = 'Online_Trainer_User' AND IsActive = true) OR (Id = :UserInfo.getUserId()) ORDER BY Name ASC];
	}


	/**
	 * @description Initialises the view state based on the current tab selected. References the `currentTabName` class variable.
	 * @author Ranyel Maliwanag
	 * @date 06.JAN.2016
	 */
	public void loadTabContents() {
		System.debug('currentTabName ::: ' + currentTabName);
		// Fetch list of containers found on the current tab
		List<SEC_Container__mdt> containerMetas = [SELECT MasterLabel, DeveloperName, SEC_SortOrder__c, SEC_Tab__c FROM SEC_Container__mdt WHERE SEC_Tab__c = :currentTabName ORDER BY SEC_SortOrder__c ASC];
		
		// Build a set of container names that will be used to query the necessary list views
		containerListViews = new Map<String, List<SEC_ListView__mdt>>();
		for (SEC_Container__mdt container : containerMetas) {
			containerListViews.put(container.DeveloperName, new List<SEC_ListView__mdt>());
		}
		Set<String> containerNames = containerListViews.keySet();
		
		// Fetch list of list views based on visible containers on the current tab
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_ListView__mdt);
		queryString += 'WHERE SEC_Container__c IN :containerNames ORDER BY SEC_SortOrder__c ASC';
		List<SEC_ListView__mdt> listViews = Database.query(queryString);

		// Build reference map for listing of all list views per container
		for (SEC_ListView__mdt listView : listViews) {
			containerListViews.get(listView.SEC_Container__c).add(listView);
		}

		// Build list of easily-referenceable custom Container objects based on metadata
		containers = new List<Container>();
		for (SEC_Container__mdt containerMeta : containerMetas) {
			containers.add(new Container(containerMeta, containerListViews.get(containerMeta.DeveloperName)));
		}
	}


	/**
	 * @description Page action handler to reload page contents based on the clicked tab
	 * @author Ranyel Maliwanag
	 * @date 06.JAN.2016
	 */
	public PageReference switchTabs() {
		PageReference result = null;
		System.debug('selectedTrainer ::: ' + currentTrainer);
		result = Page.StudentEngagementConsole;
		if (studentId != null) {
			result.getParameters().put('eid', studentId);
		} else {
			result.getParameters().put('tab', currentTabName);
		}
		result.getParameters().put('viewAs', currentTrainer);
		result.setRedirect(true);
		return result;
	}


	/**
	 * @description Inner class to obfuscate the Container meta data for easier referencing on the visualforce page
	 * @author Ranyel Maliwanag
	 * @date 06.JAN.2016
	 */
	public class Container {
		public SEC_Container__mdt meta {get; set;}
		public List<SEC_ListView__mdt> listViews {get; set;}
		public Boolean isNotEmpty {get; set;}

		public Container(SEC_Container__mdt containerMeta, List<SEC_ListView__mdt> listViews) {
			this.meta = containerMeta;
			this.listViews = listViews;
			this.isNotEmpty = listViews != null && listViews.size() > 0;
		}
	}
}