/**
 * @description Schedulable interface implementation for the Stage Suspension Batch
 * @author Ranyel Maliwanag
 * @date 18.JAN.2016
 */
public without sharing class StageSuspensionScheduler implements Schedulable {
	public StageSuspensionBatch batchProcess;

	public StageSuspensionScheduler() {
		batchProcess = new StageSuspensionBatch();
	}

	public void execute(SchedulableContext sc) {
		Database.executeBatch(batchProcess);
	}
}