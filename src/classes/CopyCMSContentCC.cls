/**
 * @description Controller for `CopyCMSContent` page
 * @author Biao Zhang
 * @date 10.DEC.2015
 */

public with sharing class CopyCMSContentCC {
	//search strings
	public string fromSearchString {get; set;}
	public string toSearchString {get; set;}
	//
	public Qualification__c fromQualification {get; set;}
	public Qualification__c toQualification {get; set;}
	//
	public List<qualWrapper> fromQualifications {get; set;}
	public List<qualWrapper> toQualifications {get; set;}
	//flag for control visualforce page rendering
	public String stage {get; set;} 
	//the children objects to be copied
	public List<Website_Provider__c> providers {get; set;}
	public List<Website_Delivery_Mode__c> deliveries {get; set;}
	public List<Website_Disclaimer__c> disclaimers {get; set;}
	//Original Hidden from Website and Visible on Website values
	private Boolean originalVisibleOnWebsiteInSource {get; set;}
	private Boolean originalHiddenFromWebsiteInSource {get; set;}
	private Boolean originalVisibleOnWebsiteInTarget {get; set;}
	private Boolean originalHiddenFromWebsiteInTarget {get; set;}

	public CopyCMSContentCC() {
		fromQualifications = new List<qualWrapper>();
		toQualifications = new List<qualWrapper>();
		fromQualification = new Qualification__c();
		toQualification = new Qualification__c();
		stage = 'search';
		providers = new List<Website_Provider__c>();
		deliveries = new List<Website_Delivery_Mode__c>();
		disclaimers = new List<Website_Disclaimer__c>();

	}

	public void searchQualityFrom() {
		stage = 'search';
		fromQualifications.clear();
		
		string searchString = '%' + fromSearchString + '%';
		Qualification__c[] quals = [Select 
									Id,
									Qualification_Name__c,
									Name,
									Effective_Date__c,
									Eligible_for_Website__c
								From 
									Qualification__c 
								Where 
									Name like :searchString 
								Or 
									Qualification_Name__c like :searchString
								Limit 20];

		for(Qualification__c qual : quals) {
			fromQualifications.add(new qualWrapper(qual));
		}
	}

	public void searchQualityTo() {
		stage = 'search';
		toQualifications.clear();
		
		string searchString = '%' + toSearchString + '%';
		Qualification__c[] quals = [Select 
									Id,
									Qualification_Name__c,
									Name,
									Effective_Date__c,
									Eligible_for_Website__c
								From 
									Qualification__c 
								Where 
									Name like :searchString 
								Or 
									Qualification_Name__c like :searchString
								Limit 20];

		for(Qualification__c qual : quals) {
			toQualifications.add(new qualWrapper(qual));
		}
	}

	public void showOriginalQualifications() {
		fromQualification = getSelectedQual(fromQualifications);
		toQualification = getSelectedQual(toQualifications);

		Boolean hasError = false;
		if(fromQualification == null) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a qualification to copy from'));
			hasError = true;
		} 
		if(toQualification == null) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a qualification to copy to'));
			hasError = true;
		}
		if(hasError) return;

		stage = 'original';

		fromQualification = [Select 
								Id,
								website_title__c, 
								website_code__c, 
								Eligible_for_Website__c, 
								Visible_On_Website__c, 
								Hidden_from_website__c, 
								website_type__c, 
								website_description__c,
								website_aqf_code__c,
								website_aqf_title__c,
								website_pathway__c,
								website_requirements__c,
								website_sector__c,
								website_materials__c,
								website_accreditation__c,
								website_apprenticeship__c,
								website_traineeship__c,
								which_website__c,
								W_Course_Amount_All_States__c,
								W_Course_Amount_ACT__c,
								W_Course_Amount_NSW__c,
								W_Course_Amount_NT__c,
								W_Course_Amount_QLD__c,
								W_Course_Amount_SA__c,
								W_Course_Amount_TAS__c,
								W_Course_Amount_VIC__c,
								W_Course_Amount_WA__c,
								W_Course_Amount_MEA_member__c,
								W_Course_Amount_MEA_non_member__c,
								Website_Update_Required__c,
								(Select
									WP_RTO_Display_Name__c,
									RTO_Account__r.Name,
									WP_RTO_code__c,
									WP_CRICOS_Number__c
								From
									Website_Providers__r),
								(Select
									Website_duration_period__c,
									Website_duration_type__c,
									Website_Delivery_Mode_Locations__c,
									Website_Delivery_Mode__c,
									Requirements__c,
									Website_Delivery_Mode_Notes__c
								From
									Website_Delivery_Modes__r),
								(Select
									WDO_Disclaimer__c
								From
									Website_Disclaimers__r)
		 					From 
		 						Qualification__c 
		 					where 
		 						id =: fromQualification.Id limit 1];

		toQualification = [Select 
								website_title__c, 
								website_code__c, 
								Eligible_for_Website__c, 
								Visible_On_Website__c, 
								Hidden_from_website__c, 
								website_type__c, 
								website_description__c,
								website_aqf_code__c,
								website_aqf_title__c,
								website_pathway__c,
								website_requirements__c,
								website_sector__c,
								website_materials__c,
								website_accreditation__c,
								website_apprenticeship__c,
								website_traineeship__c,
								which_website__c,
								W_Course_Amount_All_States__c,
								W_Course_Amount_ACT__c,
								W_Course_Amount_NSW__c,
								W_Course_Amount_NT__c,
								W_Course_Amount_QLD__c,
								W_Course_Amount_SA__c,
								W_Course_Amount_TAS__c,
								W_Course_Amount_VIC__c,
								W_Course_Amount_WA__c,
								W_Course_Amount_MEA_member__c,
								W_Course_Amount_MEA_non_member__c,
								Website_Update_Required__c,
								(Select
									WP_RTO_Display_Name__c,
									RTO_Account__r.Name,
									WP_RTO_code__c,
									WP_CRICOS_Number__c
								From
									Website_Providers__r),
								(Select
									Website_duration_period__c,
									Website_duration_type__c,
									Website_Delivery_Mode_Locations__c,
									Website_Delivery_Mode__c,
									Requirements__c,
									Website_Delivery_Mode_Notes__c
								From
									Website_Delivery_Modes__r),
								(Select
									WDO_Disclaimer__c
								From
									Website_Disclaimers__r)
		 					From 
		 						Qualification__c 
		 					where 
		 						id =: toQualification.Id limit 1];

	}

	private Qualification__c getSelectedQual(List<qualWrapper> quals) {
		for(qualWrapper q : quals) {
			if(q.selected == true) {
				return q.qual;
			}
		}
		return null;
	}

	public void showUpdatedQualifications() {
		stage = 'confirm';
		toQualification.website_title__c = fromQualification.website_title__c;
		toQualification.website_description__c = fromQualification.website_description__c;
		toQualification.website_pathway__c = fromQualification.website_pathway__c;
		toQualification.website_requirements__c = fromQualification.website_requirements__c;
		toQualification.website_sector__c = fromQualification.website_sector__c;
		toQualification.website_materials__c = fromQualification.website_materials__c;
		toQualification.website_accreditation__c = fromQualification.website_accreditation__c;
		toQualification.which_website__c = fromQualification.which_website__c;
		toQualification.W_Course_Amount_All_States__c = fromQualification.W_Course_Amount_All_States__c;
		toQualification.W_Course_Amount_ACT__c = fromQualification.W_Course_Amount_ACT__c;
		toQualification.W_Course_Amount_NSW__c = fromQualification.W_Course_Amount_NSW__c;
		toQualification.W_Course_Amount_NT__c = fromQualification.W_Course_Amount_NT__c;
		toQualification.W_Course_Amount_QLD__c = fromQualification.W_Course_Amount_QLD__c;
		toQualification.W_Course_Amount_SA__c = fromQualification.W_Course_Amount_SA__c;
		toQualification.W_Course_Amount_TAS__c = fromQualification.W_Course_Amount_TAS__c;
		toQualification.W_Course_Amount_VIC__c = fromQualification.W_Course_Amount_VIC__c;
		toQualification.W_Course_Amount_WA__c = fromQualification.W_Course_Amount_WA__c;

		if(fromQualification.Website_Providers__r.size()>0) {
			providers.clear();
			for(Website_Provider__c provider : fromQualification.Website_Providers__r) {
				Website_Provider__c clone = provider.clone(false);
				clone.WP_Qualification__c = toQualification.Id;
				providers.add(clone);
			}
		}

		if(fromQualification.Website_Delivery_Modes__r.size()>0) {
			deliveries.clear();
			for(Website_Delivery_Mode__c mode : fromQualification.Website_Delivery_Modes__r) {
				Website_Delivery_Mode__c clone = mode.clone(false);
				clone.Qualification__c = toQualification.Id;
				deliveries.add(clone);
			}
		}

		if(fromQualification.Website_Disclaimers__r.size()>0) {
			disclaimers.clear();
			for(Website_Disclaimer__c disclaimer : fromQualification.Website_Disclaimers__r) {
				Website_Disclaimer__c clone = disclaimer.clone(false);
				clone.WD_Qualification__c = toQualification.Id;
				disclaimers.add(clone);
			}
		}

		originalHiddenFromWebsiteInSource = fromQualification.Hidden_from_website__c;
		originalVisibleOnWebsiteInSource = fromQualification.Visible_On_Website__c;
		originalHiddenFromWebsiteInTarget = toQualification.Hidden_from_website__c;
		originalVisibleOnWebsiteInTarget = toQualification.Visible_On_Website__c;
 	}

 	public void save() {
 		if((!originalHiddenFromWebsiteInSource && fromQualification.Hidden_from_website__c) || (!originalVisibleOnWebsiteInSource && fromQualification.Visible_On_Website__c)) {
 			fromQualification.Website_Update_Required__c = true;
 			update fromQualification;
 		}
 		if((!originalHiddenFromWebsiteInTarget && toQualification.Hidden_from_website__c) || (!originalVisibleOnWebsiteInTarget && toQualification.Visible_On_Website__c)) {
 			toQualification.Website_Update_Required__c = true;
 		}
 		update toQualification;
 		insert providers;
 		insert deliveries;
 		insert disclaimers;

 		stage = 'search';
 		fromQualifications.clear();
 		toQualifications.clear();

 		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Content successfully copied'));
 	}

	public class qualWrapper{
        public Qualification__c qual {get; set;}
        public Boolean selected {get; set;}
        
        public qualWrapper(Qualification__c qual){
            this.qual = qual;
            this.selected = false;
        }
    }
}