/**
 * @description Schedulable interface implementation for the InPlace batches
 * @author Ranyel Maliwanag
 * @date 22.JUL.2015
 */
public without sharing class InPlaceScheduler implements Schedulable {
    public InPlaceExportBatchStudentPrep batchProcess;
    public Integer batchSize;

    public InPlaceScheduler() {
        batchProcess = new InPlaceExportBatchStudentPrep();
        batchSize = 200;
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(batchProcess, batchSize);
    }
}