/**
 * @description Batch process for exporting student records and placement prerequisite details on an ad hoc InPlace export object to be exported for the SRS Importer
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 * @history
 *       28.SEP.2015    Ranyel Maliwanag    Changed criteria on main query to depend on the Qualification's InPlace Course status instead of just checking against a list of valid qualification names. Also removed the said list of valid qualification names to improve coverage
 */
public without sharing class InPlaceExportBatchStudent implements Database.Batchable<SObject> {
    public String queryString;
    // Mapping of <Enrolment Id> : <List of enrolment units>
    public Map<Id, List<Enrolment_Unit__c>> enrolmentUnitMap = new Map<Id, List<Enrolment_Unit__c>>();
    public Map<Id, Enrolment__c> studentEnrolmentMap = new Map<Id, Enrolment__c>();
    public Set<String> vocationalPlacementPrefixes = new Set<String>{
        'VP%',
        'VOC%'
    };
    public Date dateToday = Date.today();
    public InPlaceSettings__c settings;

    /**
     * @details Constructor for the batch process
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public InPlaceExportBatchStudent() {
        settings = InPlaceSettings__c.getInstance();
        // Query for Account Information
        queryString = 'SELECT Id, Student_Identifier__c, Enrolment_Status__c, Qualification_Name__c, IPS_DOB__c, IPS_Email__c, IPS_Foreign_Domestic_Status_Code__c, IPS_Gender__c, IPS_Given_Name__c, IPS_InPlace_Student__c, IPS_Other_Name__c, IPS_Phone_Home__c, IPS_Phone_Mobile__c, IPS_Student_Code__c, IPS_Surname__c, IPS_Sync_on_next_update__c, IPS_Title__c, Address_flat_unit_details__c, Address_street_name__c, Address_street_number__c, Name_of_suburb_town_locality__c, Mailing_State_Provience__c, Mailing_Zip_Postal_Code__c, Mailing_Country__c, Type_of_Attendance__c, Type_of_Attendance__r.Name, Student__r.Suburb_locality_or_town__c, Qualification__r.IPC_InPlace_Course__c, Qualification__r.InPlace_Discipline__r.IPD_Description__c, Student__r.Emergency_Contact_Name__c, Student__r.Relationship_to_Emergency_Contact__c, Student__r.Emergency_Contact_Phone_Number__c, Student__r.Emergency_Contact_Mobile_Number__c ';
        queryString += 'FROM Enrolment__c ';
        queryString += 'WHERE Enrolment_Status__c LIKE \'Active%\' AND IPS_InPlace_Student__c = true AND IPS_Sync_on_next_update__c = true AND Qualification__r.IPC_InPlace_Course__c = true ORDER BY Student_Identifier__c';
        // For debugging purposes
        //queryString += 'AND Id = \'0019000000P7K7B\'';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Enrolment__c> enrolments) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        // Iterate over enrolments
        for (Enrolment__c enrolment : enrolments) {
            // Set defaults for potential null-referencing fields
            String postalCode = String.isBlank(enrolment.Mailing_Zip_Postal_Code__c) ? '' : enrolment.Mailing_Zip_Postal_Code__c.left(10);
            String typeOfAttendance = (enrolment != null && enrolment.Type_of_Attendance__c != null && enrolment.Type_of_Attendance__r.Name != null) ? enrolment.Type_of_Attendance__r.Name.subString(0, 1) : 'F';
            InPlace__c studentAddress = new InPlace__c(
                    IPStudentAddress_StudentCode__c = enrolment.Student_Identifier__c,
                    IPStudentAddress_Unit__c = enrolment.Address_flat_unit_details__c,
                    IPStudentAddress_Street__c = enrolment.Address_street_number__c + ' ' + enrolment.Address_street_name__c,
                    IPStudentAddress_Suburb__c = enrolment.Student__r.Suburb_locality_or_town__c,
                    IPStudentAddress_State__c = enrolment.Mailing_State_Provience__c,
                    IPStudentAddress_Postcode__c = postalCode,
                    IPStudentAddress_Country__c = enrolment.Mailing_Country__c,
                    IPStudentAddress_Active__c = 'Y',
                    IPStudent_DeceasedFlag__c = 'N',
                    IPStudent_DOB__c = enrolment.IPS_DOB__c,
                    IPStudent_Email__c = enrolment.IPS_Email__c,
                    IPStudent_ForeignDomesticStatusCode__c = enrolment.IPS_Foreign_Domestic_Status_Code__c,
                    IPStudent_FullTimePartTime__c = typeOfAttendance,
                    IPStudent_Gender__c = enrolment.IPS_Gender__c,
                    IPStudent_GivenName__c = enrolment.IPS_Given_Name__c,
                    IPStudent_OtherName__c = enrolment.IPS_Other_Name__c,
                    IPStudent_PhoneHome__c = enrolment.IPS_Phone_Home__c,
                    IPStudent_PhoneMobile__c = enrolment.IPS_Phone_Mobile__c,
                    IPStudent_StudentCode__c = enrolment.IPS_Student_Code__c,
                    IPStudent_StudentEnrolmentCode__c = 'Enrolled',
                    IPStudent_Surname__c = enrolment.IPS_Surname__c,
                    IPStudent_Title__c = enrolment.IPS_Title__c,
                    IPStudent_Username__c = enrolment.IPS_Student_Code__c
                );

            String contactNameER = '';
            String relationshipER = '';
            String phoneER = '';
            String mobileER = '';

            if (enrolment.Qualification__r.InPlace_Discipline__r.IPD_Description__c == 'Health Care') {
                contactNameER = 'ERREF_1063';
                relationshipER = 'ERREF_1064';
                phoneER = 'ERREF_1081';
                mobileER = 'ERREF_1083';
            } else if (enrolment.Qualification__r.InPlace_Discipline__r.IPD_Description__c == 'Community Services Work') {
                contactNameER = 'ERREF_1066';
                relationshipER = 'ERREF_1067';
                phoneER = 'ERREF_1087';
                mobileER = 'ERREF_1091';
            } else if (enrolment.Qualification__r.InPlace_Discipline__r.IPD_Description__c == 'Nursing') {
                contactNameER = 'ERREF_1060';
                relationshipER = 'ERREF_1061';
                phoneER = 'ERREF_1085';
                mobileER = 'ERREF_1086';
            }

            if (!exportedIdentifiers.contains(enrolment.Student_Identifier__c)) {
                if (String.isNotBlank(contactNameER) && String.isNotBlank(enrolment.Student__r.Emergency_Contact_Name__c)) {
                    InPlace__c emergencyContactName = new InPlace__c(
                            //IPEntityRelationshipEntity_Action__c = 'APPEND',
                            IPEntityRelationshipEntity_AttrValue__c = enrolment.Student__r.Emergency_Contact_Name__c,
                            IPEntityRelationshipEntity_EntityCode__c = enrolment.Student_Identifier__c,
                            IPEntityRelationshipEntity_ErRef__c = contactNameER
                        );
                    exports.add(emergencyContactName);
                }
                if (String.isNotBlank(relationshipER) && String.isNotBlank(enrolment.Student__r.Relationship_to_Emergency_Contact__c)) {
                    InPlace__c relationshipToEmergencyContact = new InPlace__c(
                            //IPEntityRelationshipEntity_Action__c = 'APPEND',
                            IPEntityRelationshipEntity_AttrValue__c = enrolment.Student__r.Relationship_to_Emergency_Contact__c,
                            IPEntityRelationshipEntity_EntityCode__c = enrolment.Student_Identifier__c,
                            IPEntityRelationshipEntity_ErRef__c = relationshipER
                        );
                    exports.add(relationshipToEmergencyContact);
                }
                if (String.isNotBlank(phoneER) && String.isNotBlank(enrolment.Student__r.Emergency_Contact_Phone_Number__c)) {
                    InPlace__c phoneNumber = new InPlace__c(
                            //IPEntityRelationshipEntity_Action__c = 'APPEND',
                            IPEntityRelationshipEntity_AttrValue__c = enrolment.Student__r.Emergency_Contact_Phone_Number__c,
                            IPEntityRelationshipEntity_EntityCode__c = enrolment.Student_Identifier__c,
                            IPEntityRelationshipEntity_ErRef__c = phoneER
                        );
                    exports.add(phoneNumber);
                }            
                if (String.isNotBlank(mobileER) && String.isNotBlank(enrolment.Student__r.Emergency_Contact_Mobile_Number__c)) {
                    InPlace__c mobileNumber = new InPlace__c(
                            //IPEntityRelationshipEntity_Action__c = 'APPEND',
                            IPEntityRelationshipEntity_AttrValue__c = enrolment.Student__r.Emergency_Contact_Mobile_Number__c,
                            IPEntityRelationshipEntity_EntityCode__c = enrolment.Student_Identifier__c,
                            IPEntityRelationshipEntity_ErRef__c = mobileER
                        );
                    exports.add(mobileNumber);
                }
                
                exports.add(studentAddress);
                exportedIdentifiers.add(enrolment.Student_Identifier__c);
            }
            
            enrolment.IPS_Sync_on_next_update__c = false;
        }

        insert exports;

        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update enrolments;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchCalendar nextBatch = new InPlaceExportBatchCalendar();
        Database.executeBatch(nextBatch);
    }
}