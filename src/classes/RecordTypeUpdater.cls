global without sharing class RecordTypeUpdater {

    webService static void convertToStudentAccount(String personAccountId) {
        //personAccountId = '001N0000005zN6X';
        RecordType rc = [Select Id from RecordType where Name = 'Student' And SObjectType = 'Account'];
        Account acc = [Select RecordTypeId from Account where Id =: personAccountId];
        acc.RecordTypeId = rc.Id;
        update acc;
    }
}