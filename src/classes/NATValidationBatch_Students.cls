global without sharing class NATValidationBatch_Students implements Database.Batchable<sObject>,Database.Stateful {
    
    global String queryStringStudents;
    global NAT_Validation_Log__c log;
    global Integer nat80recCount = 0;
    global Integer nat90recCount = 0; 
    global Integer nat100recCount = 0; 
    global Set<NAT_Validation_Event__c> errorEventList = new Set<NAT_Validation_Event__c>();
    global List<Schema.FieldSetMember> fields;
    
    global class testException extends Exception {
        
    }

    
    global NATValidationBatch_Students(Id logId) {
        
       /* fields = SObjectType.Nat_Validation_Log__c.FieldSets.Query_Students.getFields();
        
        String fieldSet = '';
        
        for(Schema.FieldSetMember f : fields) { 
            fieldSet += f.getFieldPath() + ',';
        } */
        
       // String logQuery = 'SELECT ID, Validation_State__c, Training_Organisation_Id__c, ' + fieldSet + ' Collection_Year__c FROM NAT_Validation_Log__c WHERE ID = \'' + logId + '\''; 
                
    	log = [ SELECT ID, Validation_State__c, Training_Organisation_Id__c, query_students__c, query_students_2__c, query_students_3__c, query_students_4__c, query_students_5__c, query_students_6__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
    
        // log = Database.query(logQuery);
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        
        // will be replaced by field set stuff
        String queryStringStudents = '';
        
        if(log.query_students__c != null ) {
            queryStringStudents = log.query_students__c;
        }    
        if(log.query_students_2__c != null ) {
            queryStringStudents += log.query_students_2__c;
        }    
        if(log.query_students_3__c != null ) {
            queryStringStudents += log.query_students_3__c;
        }    
        if(log.query_students_4__c != null ) {
            queryStringStudents += log.query_students_4__c;
        }    
        if(log.query_students_5__c != null ) {
            queryStringStudents += log.query_students_5__c;
        }    
        if(log.query_students_6__c != null ) {
            queryStringStudents += log.query_students_6__c;
        }    
        
        
        /*
        for(Schema.FieldSetMember f : fields) { 
            if( log.get( f.getFieldPath() ) != null ) {
            	queryStringStudents += log.get(f.getFieldPath());
        	}
        } */
        
        //throw new testException(queryStringStudents);
 
        return Database.getQueryLocator(queryStringStudents);
       
            
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        List<String>splitter90 = new List<String>();
      	
        List<String>splitter100 = new List<String>();
        
        Set<String>otherLangCodes = new Set<String>{'1201','9700','9701','9702','9799'};
  

        try {
        	for(SObject so : scope) {
                nat80recCount++;
				Account s = (Account) so;
                
                NAT_Validation_Event__c event80 = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = s.Id, Nat_File_Name__c = 'NAT00080',Parent_Record_Last_Modified_Date__c = s.LastModifiedDate);
                NAT_Validation_Event__c event85 = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = s.Id, Nat_File_Name__c = 'NAT00085',Parent_Record_Last_Modified_Date__c = s.LastModifiedDate);
                NAT_Validation_Event__c event100 = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = s.Id, Nat_File_Name__c = 'NAT00100',Parent_Record_Last_Modified_Date__c = s.LastModifiedDate);

                //NAT80 Validation
                
                if(s.Student_Identifer__c == null)
                    event80.Error_Message__c = 'Student Identifier cannot be blank';
                    
                else if(s.Disability_Flag_0__c == 'Y' && s.Disability_Type__c == null)
                    event80.Error_Message__c = 'Disability Types must be populated when Disability Flag is YES';
                
                else if(s.Name_for_encryption_student__c == null)
                    event80.Error_Message__c = 'Name for encryption cannot be blank';
                    
                else if(s.Highest_School_Level_Comp_Identifier__c == null)
                    event80.Error_Message__c = 'Highest School Level Completed Identifier cannot be blank';
                    
                else if(s.PersonBirthdate == null) 
                    event80.Error_Message__c = 'Student Birthdate cannot be blank';                
                
                else if(s.Highest_School_Level_Comp_Identifier__c == '02' && s.At_School_Flag__c != 'N') 
                    event80.Error_Message__c = 'No – the student is not attending secondary school';     
                  
                else if(s.Address_building_property_name__c == null && s.Address_flat_unit_details__c == null && s.Address_street_number__c == null && s.Address_street_name__c == null)
                    event80.Error_Message__c = 'Address Cannot Be Blank';
                  
                else if((s.Highest_School_Level_Comp_Identifier__c == '09' || s.Highest_School_Level_Comp_Identifier__c == '10' || s.Highest_School_Level_Comp_Identifier__c == '11' || s.Highest_School_Level_Comp_Identifier__c == '12') && GlobalUtility.formatAgeFromDateOfBirth(s.PersonBirthDate) < 10)
                    event80.Error_Message__c = 'For Age Group < 10, Highest School Level Completed Identifier cannot be 09,10,11,12';
                    
                else if(s.Year_Highest_Education_Completed__c == null)
                    event80.Error_Message__c = 'Year High Education Completed cannot be blank';
                    
                else if( s.Year_Highest_Education_Completed__c != '@@@@' && Integer.valueof(s.Year_Highest_Education_Completed__c) < (s.PersonBirthdate).year() + 5 )
                    event80.Error_Message__c = 'Birthdate + 5 Years cannot be less than Year High Education Completed';
                
                else if (s.Year_Highest_Education_Completed__c != '@@@@' && (Integer.valueof(s.Year_Highest_Education_Completed__c) > s.PersonBirthdate.year() + 5 ) && (Integer.valueof(s.Year_Highest_Education_Completed__c) < s.PersonBirthdate.year() + 10 ) )
                    event80.Error_Message__c = 'Year High Education Completed needs to be consistent with Date Of Birth';
                
                else if(s.Sex__c == null)
                    event80.Error_Message__c = 'Gender cannot be blank';
                    
                else if(s.Labour_Force_Status_Identifier__c == null)
                    event80.Error_Message__c = 'Labour Status Identifier cannot be blank';
   
                else if(s.Proficiency_in_Spoken_English_Identifier__c == null && !(otherLangCodes.contains(s.Main_Language_Spoken_at_Home_Identifier__c)))
                    event80.Error_Message__c = 'Main Language spoken at home identifier must equal to 1201,9700,9701,9702,9799 when proficiency in spoken english identifier is blank';    
                
                else if(s.Proficiency_in_Spoken_English_Identifier__c != null && (otherLangCodes.contains(s.Main_Language_Spoken_at_Home_Identifier__c)))
                    event80.Error_Message__c = 'Main Language spoken at home identifier must not equal to 1201,9700,9701,9702,9799 when proficiency in spoken english identifier is populated';
                    
                else if(s.Learner_Unique_Identifier__c != null && s.Learner_Unique_Identifier__c.length() != 10)
                    event80.Error_Message__c = 'Learner Unit Identifier Must Be 10 Characters.';
                
                else if(s.USI_Status__c != 'VERIFIED' && log.Validation_State__c == 'Victoria') 
                    event80.Error_Message__c = 'This student has an un-validated or missing USI';
                
                else if(log.Validation_State__c == 'Victoria' && s.Prior_Achievement_Flag__c == 'Y' && s.Prior_Education_Achievement_Recognition__c == null)
                    event80.Error_Message__c = 'Prior Achievement Recognition must be populated when Prior Achievement flag is YES';  
                
                else if(log.Validation_State__c == 'Western Australia' && s.Address_Dwelling_Type_Identifier__c == null) 
                    event80.Error_Message__c = 'Address Dwelling Type Identifier is a mandatory data element for a street address and must not be left blank';
                
                else if(log.Validation_State__c == 'Western Australia' && s.At_School_Flag__c == 'Y' )
                    event80.Error_Message__c = 'Education Identifier must not be blank';
                
                else if(log.Validation_State__c == 'Victoria' && s.Labour_Force_Status_1__c != null) {
                    List<Labour_Force_Status_NAT__c> lfs = Labour_Force_Status_NAT__c.getall().values();
                    for(Labour_Force_Status_NAT__c l : lfs ) {
                        if(s.Labour_Force_Status_1__r.Code__c == l.Code__c) {
                            if(s.Client_Occupation_Identifier__c == null) {
                                event80.Error_Message__c = 'If the student is employed, Client Occupation Identifier must not be blank';
                            }
                            if(s.Client_Industry_of_Employment__c == null) {
                                event80.Error_Message__c = 'If the student is employed, Client Industry of Employment must not be blank';
                            }
                        }                   
                    }
                }
                
                if(event80.Error_Message__c != null && event80.Error_Message__c != '' ) {
                    errorEventList.add(event80);
                }
                
                
                //NAT85 Validation
                
                if(s.Salutation == null) 
                    event85.Error_Message__c = 'Title/Salutation cannot be blank';
                
                else if(s.State_Identifier__c == null)
                    event85.Error_Message__c = 'State Identifier Cannot Be Blank';
                
                if(event85.Error_Message__c != null && event85.Error_Message__c != '') {
                    errorEventList.add(event85);
                }
                
                //NAT90 Validation
                
                if(s.Disability_Flag_0__c == 'Y') {
                  splitter90 = s.Disability_Type__c.Split(';');
                    if(s.Disability_Type__c != null) {
                    	for(String sp90: splitter90) {
                        	nat90recCount++;
                   	 	}	  
                    }
                }
                
                //NAT100 Validation

                if(s.Prior_Achievement_Flag__c == 'Y') {
                    if(s.Prior_Achievement_Type_s__c != null) {
                        splitter100 = s.Prior_Achievement_Type_s__c.Split(';');
                    	for(String sp100: splitter100) {
                        	nat100recCount++;  
                    	}
                    }
                }
                
                if(log.Validation_State__c == 'Victoria' && s.Prior_Achievement_Flag__c == 'Y' && s.Prior_Education_Achievement_Recognition__c == null) {
                    event100.Error_Message__c = 'Prior Achievement Recognition must be populated when Prior Achievement flag is YES';        
                }
                
                if(event100.Error_Message__c != null && event100.Error_Message__c != '') {
                    errorEventList.add(event100);
                }

			}
        
		}	

        catch (exception e) {
            log.System_Message__c = String.valueOf(e);
        }
    }
       
    global void finish(Database.BatchableContext BC) {
        
        List<NAT_Validation_Event__c> errors = new List<NAT_Validation_Event__c>();
        
        for(NAT_Validation_Event__c ev : errorEventList) {
            errors.add(ev);
        }
        
        insert errors;
        
        log.NAT00080_Record_Count__c = nat80recCount;
        log.NAT00085_Record_Count__c = nat80recCount;
        log.NAT00090_Record_Count__c = nat90recCount;
        log.NAT00100_Record_Count__c = nat100recCount;
         
        update log;
        
        Database.executeBatch(new NATValidationBatch_EnrolmentUnits( log.id ) );


    }
}