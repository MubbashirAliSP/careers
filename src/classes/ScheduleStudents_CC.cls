/**
 * @description Controller for Schedule Students page 1
 * @author Janella Lauren Canlas
 * @date 06.DEC.2012
 *
 * HISTORY
 * - 06.DEC.2012    Janella Canlas - Created.
 * - 22.FEB.2012    Janella Canlas - add code for Class Enrolments without EIU
 * - 31.JUL.2014    Warjie Malibago - prevent user from going to next page if Outcome code is invalid.
 * 11.MAY.2015      Ranyel Maliwanag    - Added exception for enrolments with Melbourne as Delivery Location where Unit Results are expected to be null
 */
public with sharing class ScheduleStudents_CC {
    public Id intakeId;
    
    public String enrolmentId {get;set;}
    public String stud;
    public String searchField {get;set;}
    public String searchValue {get;set;}
    
    public List<Page1Wrapper> studentWrapperList {get;set;}
    
    public ScheduleStudents_CC(){
        //retrieves the intake ID passed from the button
        intakeId = ApexPages.currentPage().getParameters().get('intakeId');
        searchValue = '';
        enrolmentId = '';
        if(null == searchField){
            searchField = 'Name';
        }
        //call method to fill page table
        fillPage1Wrapper();
    }
    //query enrolment records based on the search value inputted bu user
    public List<Enrolment__c> getEnrolments(){
        List<Enrolment__c> temp = new List<Enrolment__c>();
        //this will hold the search value entered by user
        String newSearchText = '%'+searchValue+'%';
        system.debug('searchField****'+searchField);
        
        if(searchField == 'Name' && searchValue != ''){
            temp = [Select Student__c, Qualification_Name__c, Predominant_Delivery_Mode__c, Name, Id, 
                Enrolment_Status__c, Employer__c, Employer__r.Name, Student__r.Name, Student_Identifier__c,
                Predominant_Delivery_Mode__r.Name 
                From Enrolment__c
                Where Enrolment_Status__c like '%Active%'
                And Student__r.Name like: newSearchText
                order by Student__r.Name];
        }
        else if(searchField == 'Number'){
            temp = [Select Student__c, Qualification_Name__c, Predominant_Delivery_Mode__c, Name, Id, 
                Enrolment_Status__c, Employer__c, Student__r.Name, Student_Identifier__c,
                Predominant_Delivery_Mode__r.Name,Employer__r.Name  
                From Enrolment__c
                Where Enrolment_Status__c like '%Active%'
                And Student_Identifier__c like: newSearchText
                order by Student__r.Name];
        }
        System.debug('@@temp '+ temp);
        return temp;
    }
    
    //this will display the all the Students for the SearchPicklist Page1
    public List<SelectOption> getSearchPicklist() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Name','Student Name'));
        options.add(new SelectOption('Number','Student Number'));
        return options;
    } 
    //enrolment wrapper
    public class enrolmentListWrapper {
      public Enrolment__c cenrolments {get;set;}
      public Boolean selectedRadio {get;set;}
      
      public enrolmentListWrapper(Enrolment__c enrol, Boolean sel){
        cenrolments = enrol;
        selectedRadio = sel;
      }
    }
    //page 1 table wrapper
    public class Page1Wrapper {
        public String studentName {get;set;}
        public String studentIdentifier {get;set;}
        public List<enrolmentListWrapper> classEnrol {get;set;}
        
        public Page1Wrapper(String student, String identifier, List<enrolmentListWrapper> enrolList) {  
            studentName = student;
            studentIdentifier = identifier;
            classEnrol = enrolList;
        }
    }
    
    //this will fill page table
    public void fillPage1Wrapper() {
        studentWrapperList = new List<Page1Wrapper>();
        
        List<Enrolment__c> enrolList = new List<Enrolment__c>();
        List<Id> studIds = new List<Id>();
        
        Set<id> studIdscheck = new Set<Id>();
        
        Map<Id, List<Enrolment__c>> enrolMap = new Map<Id,List<Enrolment__c>>();
        Map<Id, List<enrolmentListWrapper>> classEnrolMap = new Map<Id, List<enrolmentListWrapper>>();
        Map<String, String> studentMap = new Map<String,String>();
        Map<String, String> studentIdMap = new Map<String,String>();
        
        Boolean slct = false;
        
        enrolList = getEnrolments();
        system.debug('***1. enrolList'+enrolList);
        //collect student identifiers
        for(Enrolment__c e : enrolList){
            if(!studIdscheck.contains(e.Student__c)){
                studIds.add(e.Student__c);
                studIdscheck.add(e.Student__c);
            }
        }
        system.debug('***2. studIds'+studIds);
        system.debug('***3. studIdscheck'+studIdscheck);
        //create map of student identifiers and corresponding enrolment list
        for(Id i : studIds){
            for(Enrolment__c e : enrolList){
                system.debug('***ENROLMENT'+e);
                if(i == e.Student__c){
                    if(enrolMap.containsKey(i)){
                        system.debug('***PASOK1***');
                        List<Enrolment__c> temp = enrolMap.get(i);
                        temp.add(e);
                        enrolMap.put(i,temp);
                    }
                    else{
                        enrolMap.put(i,new List<Enrolment__c>{e});
                    }
                }
            }
        }
        system.debug('***3. enrolMap'+enrolMap);
        //fill enrolment wrapper with unique enrolments
        for(Id s : studIds){
            List<Enrolment__c> tempClassList = new List<Enrolment__c>();
            tempClassList = enrolMap.get(s);            
            for(Enrolment__c e : tempClassList){
                if(s == e.Student__c){
                  if(classEnrolMap.containsKey(s)){
                        system.debug('***PASOK2***');
                        List<enrolmentListWrapper> tempList = classEnrolMap.get(s);
                        tempList.add(new enrolmentListWrapper(e,slct));
                        classEnrolMap.put(s,tempList);
                    }
                    else{
                        classEnrolMap.put(s,new List<enrolmentListWrapper>{new enrolmentListWrapper(e,slct)});
                    }
                }  
            }
        }
        system.debug('enrolMap***'+enrolMap);
        //create map of student identifier and enrolment student name
        for(Enrolment__c e : enrolList){
          for(Id s : studIds){
            if(s == e.Student__c){
              studentMap.put(s,e.Student__r.Name);
            }
          }
        }
        system.debug('studentMap***'+studentMap);
        //create map of student identifiers 
        for(Enrolment__c e : enrolList){
          for(Id s : studIds){
            if(s == e.Student__c){
              studentIdMap.put(s,e.Student_Identifier__c);
            }
          }
        }
        system.debug('studentMap***'+studentMap);
        system.debug('studIds***'+studIds);
        //loop to create page1 table
        for(Id s : studIds){
            //populate student wrapper list
            Page1Wrapper wc = new Page1Wrapper(studentMap.get(s),studentIdMap.get(s),classEnrolMap.get(s));
            studentWrapperList.add(wc);
        }
        system.debug('studentWrapperList***'+studentWrapperList);
    }
    //cancel method - redirect to intake record
    public PageReference cancel(){
        PageReference p = new PageReference('/'+intakeId);
        p.setredirect(true);
        return p;
    }
    //redirect to schedule students page 2, return error if there are no enrolment selected
    public PageReference next() {
        system.debug('^^^intakeId'+intakeId);
        if (enrolmentId == '') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please select an Enrolment'));
            return null;
        } else {
            //31JUL2014 WBM
            List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>([SELECT Id, Unit_Result__c,Training_Delivery_Location__r.Child_State__c, Unit_Result__r.National_Outcome_Code__c FROM Enrolment_Unit__c WHERE Enrolment__c =: enrolmentId]);
            Boolean proceed = true;

            Enrolment__c enrolment = [SELECT Delivery_Location_State__c FROM Enrolment__c WHERE Id = :enrolmentId];

            if (enrolment != null && enrolment.Delivery_Location_State__c != 'Victoria') {
                for (Enrolment_Unit__c eu : euList) {
                    if (eu.Unit_Result__c == NULL) {
                        proceed = false;
                        break;
                    }
                }
            }
            system.debug('**Proceed? ' + proceed);
            if (proceed) {
                PageReference pageRef = new PageReference('/apex/ScheduleStudentsPage2');
                pageRef.getParameters().put('enrolmentId',enrolmentId);
                pageRef.getParameters().put('intakeId',intakeId);
                pageRef.setRedirect(true);
                return pageRef;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please update all Enrolment Units with blank results, before scheduling this student.'));
                return null;
            }
            //WBM End
        }
        
    }
}