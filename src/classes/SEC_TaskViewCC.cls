/**
 * @description Custom component controller for `SEC_TaskView` component
 * @author Ranyel Maliwanag
 * @date 14.JAN.2016
 */ 
public without sharing class SEC_TaskViewCC {
	// To keep track of the current trainer
	public Id currentTrainer {get; set;}
	private Id oldTrainer {get; set;}

	public Map<String, String> listViewQueryMap {
		get {
			if (listViewQueryMap == null) {
				listViewQueryMap = new Map<String, String>{
					'Overdue' => 'ActivityDate < TODAY',
					'Today' => 'ActivityDate = TODAY',
					'Today + Overdue' => 'ActivityDate <= TODAY',
					'Tomorrow' => 'ActivityDate = TOMORROW',
					'Next 7 Days' => 'ActivityDate = NEXT_N_DAYS:7',
					'Next 7 Days + Overdue' => '(ActivityDate < TODAY OR ActivityDate = NEXT_N_DAYS:7)',
					'This Month' => 'ActivityDate = THIS_MONTH'
				};
			}
			return listViewQueryMap;
		}
		set;
	}

	public List<String> listViewQueryList {
		get {
			return new List<String>{
				'Overdue',
				'Today',
				'Today + Overdue',
				'Tomorrow',
				'Next 7 Days',
				'Next 7 Days + Overdue',
				'This Month'
			};
		}
	}

	public String currentListView {
		get {
			if (currentListView == null) {
				currentListView = 'All Open';
			}
			return currentListView;
		}
		set;
	}

	// List of all open tasks for the current user, sorted by due date
	public List<Task> myTasks {
		get {
			Boolean isChanged = currentTrainer != oldTrainer;
			oldTrainer = currentTrainer;
			if (myTasks == null || isChanged) {
				System.debug('currentTrainer Tasks reentry ::: ' + currentTrainer);
				String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Task, ', What.Name, Who.Name, Owner.Name ');
				queryString += 'WHERE OwnerId = :currentTrainer AND IsClosed = false ';

				if (currentListView != 'All Open' && listViewQueryMap.containsKey(currentListView)) {
					queryString += 'AND ' + listViewQueryMap.get(currentListView) + ' ';
				}

				queryString += 'ORDER BY ActivityDate ASC NULLS LAST ';
				queryString += 'LIMIT 20 ';
				myTasks = Database.query(queryString);
			}
			return myTasks;
		}
		set;
	}

	public Map<Id, Task> taskMap {
		get {
			if (taskMap == null) {
				taskMap = new Map<Id, Task>(myTasks);
			}
			return taskMap;
		}
		set;
	}

	// Reference variable for the current task record to show
	public Task currentTask {
		get {
			if (currentTaskId != null && taskMap.containsKey(currentTaskId)) {
				currentTask = taskMap.get(currentTaskId);
			}
			return currentTask;
		}
		set;
	}

	public Task newTask {
		get {
			if (newTask == null) {
				newTask = new Task(
						OwnerId = currentTrainer,
						ActivityDate = Date.today(),
						RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId()
					);
			}
			return newTask;
		}
		set;
	}

	public Id currentTaskId {
		get;
		set {
			currentTaskId = value;
			attachments = null;
		}
	}

	public Boolean displayCommentModal {get; set;}
	public Boolean isTaskEditMode {get; set;}

	// Attachments handling
	public List<Attachment> attachments {
		get {
			if (attachments == null) {
				System.debug('currentTaskId ::: ' + currentTaskId);
				if (String.isNotBlank(currentTaskId)) {
					// Fetch attachments for the current task
					String queryString = 'SELECT Id, Name FROM Attachment ';
					queryString += 'WHERE ParentId = :currentTaskId';
					attachments = Database.query(queryString);
					attachments.add(new Attachment());
				} else {
					attachments = new List<Attachment>{new Attachment(),new Attachment(),new Attachment()};
				}
			}
			return attachments;
		}
		set;
	}


	public PageReference selectCurrentTask() {
		return null;
	}

	public PageReference refreshTaskList() {
		myTasks = null;
		return null;
	}

	public PageReference deleteTask() {
		delete currentTask;
		myTasks = null;
		taskMap = null;
		currentTask = null;
		return null;
	}

	public PageReference createTask() {
		System.debug('::: creating Task');
		insert newTask;
		currentTask = newTask;

		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = newTask.Id;
				validAttachments.add(record);
			}
		}
		insert validAttachments;

		newTask = null;
		myTasks = null;
		attachments = null;
		PageReference result = ApexPages.currentPage();
		result.setRedirect(true);
		return result;
	}


	public PageReference saveTask() {
		update currentTask;
		return refreshTaskList();
	}

	public PageReference uploadAttachment() {
		List<Attachment> validAttachments = new List<Attachment>();
		for (Attachment record : attachments) {
			if (String.isBlank(record.Id) && record.Body != null && String.isNotBlank(record.Name)) {
				record.ParentId = currentTaskId;
				validAttachments.add(record);
			}
		}

		insert validAttachments;
		attachments = null;
		return null;
	}

	public Id attachmentIdForDelete {get; set;}
	public PageReference deleteAttachment() {
		if (String.isNotBlank(attachmentIdForDelete)) {
			delete new Attachment(Id = attachmentIdForDelete);
		}
		attachments = null;
		return null;
	}
}