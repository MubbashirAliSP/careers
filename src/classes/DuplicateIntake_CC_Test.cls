/**
 * @date 19.NOV.2013    W. Malibago    Added test method for changed intake date
 */
@isTest
private class DuplicateIntake_CC_Test {

    static testMethod void duplicateIntakeTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');    
                
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c(); 
        Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Intake__c intk = new Intake__c();
        Intake_Unit__c iu = new Intake_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Classes__c clas = new Classes__c();
        Staff_Member__c staff = new Staff_Member__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        Intake_Unit_of_Study__c iuos = new Intake_Unit_of_Study__c();
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry();           
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                insert enrl;
                
                acc.Study_Reason_Identifier__c = enrl.Study_Reason__c.substring(0,2);
                update acc;
                fundStream = TestCoverageUtilityClass.createFundingStream();
                program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
                intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
                staff = TestCoverageUtilityClass.createStaffMember(intk.Id, u.Id); insert staff;
                res = TestCoverageUtilityClass.createResult(state.Id);
                un = TestCoverageUtilityClass.createUnit();
                iu = TestCoverageUtilityClass.createIntakeUnit(intk.Id, un.Id);               
                classConfig = TestCoverageUtilityClass.createClassConfiguration(intk.Id, iu.Id);
                
                clas.Intake__c = intk.Id;
                clas.Intake_Unit__c = iu.Id;
                clas.Alternate_Class_Delivery_Option__c = 'Public Holiday';
                clas.Class_Configuration__c = classConfig.Id;
                clas.Start_Date_Time__c = Date.TODAY() - 25;
                clas.End_Date_Time__c = Date.TODAY() + 25;
                insert clas;
                
                iuos = TestCoverageUtilityClass.createIntakeUnitOfStudy(iu.Id, unit.Id);
                
                system.debug('classConfig.Id****'+classConfig.Id);
                PageReference testPage = Page.DuplicateIntake;
                testPage.getParameters().put('intakeId', intk.Id);
                system.debug('intk.Id****'+intk.Id);
                Test.setCurrentPage(testPage);
                
                DuplicateIntake_CC dup = new DuplicateIntake_CC();
                dup.intakeRecord = intk;
                dup.getIntakeRecord();
                //dup.getIntakeUnitList();
                dup.saveIntake();
                //dup.getClassConfigList();
                //dup.getClassList();
                //dup.getStaffMemberList();
                dup.cancel();
                
            test.stopTest();
        }
    }
    
    //WBM Start 11.19.2013
    static testMethod void duplicateIntakeTest2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');    
                
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c(); 
        Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Intake__c intk = new Intake__c();
        Intake_Unit__c iu = new Intake_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Classes__c clas = new Classes__c();
        Staff_Member__c staff = new Staff_Member__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        
        Class_Configuration__c classConfig2 = new Class_Configuration__c();
        Classes__c clas2 = new Classes__c();
        Intake__c intk2 = new Intake__c();
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry();           
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                insert enrl;
                
                acc.Study_Reason_Identifier__c = enrl.Study_Reason__c.substring(0,2);
                update acc;
                fundStream = TestCoverageUtilityClass.createFundingStream();
                program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
                intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);             
                staff = TestCoverageUtilityClass.createStaffMember(intk.Id, u.Id); insert staff;
                res = TestCoverageUtilityClass.createResult(state.Id);
                un = TestCoverageUtilityClass.createUnit();
                iu = TestCoverageUtilityClass.createIntakeUnit(intk.Id, un.Id);               
                classConfig = TestCoverageUtilityClass.createClassConfiguration(intk.Id, iu.Id);
                
                intk2 = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
                intk2.Start_Date__c = Date.TODAY()+1;
                intk2.End_Date__c = Date.TODAY()+60;
                update intk2;
                
                system.debug('classConfig.Id****'+classConfig.Id);
                PageReference testPage = Page.DuplicateIntake;
                testPage.getParameters().put('intakeId', intk.Id);
                system.debug('intk.Id****'+intk.Id);
                Test.setCurrentPage(testPage);
                
                DuplicateIntake_CC dup = new DuplicateIntake_CC();
                dup.intakeRecord = intk;
                dup.getIntakeRecord();
                //dup.getIntakeUnitList();
                dup.saveIntake();
                
                dup.intakeRecord = intk2;
                dup.getIntakeRecord();
                dup.saveIntake();
                //dup.getClassConfigList();
                //dup.getClassList();
                //dup.getStaffMemberList();
                dup.cancel();
                
            test.stopTest();
        }
    }
    //WBM End    
}