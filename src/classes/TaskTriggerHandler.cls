/**
 * @description: Trigger handler for the Task object
 * @author Ranyel Maliwanag
 * @date 12.MAY.2015
 * @history
 *       14.MAY.2015    Ranyel Maliwanag    - Added `beforeInsert` and `setDefaultContactTypeAndOutcome` methods
 *       04.JUN.2015    Ranyel Maliwanag    - Added `Status = Completed` condition for querying related tasks to a lead in `updateLeadTasksCount()`
 *       28.OCT.2015    Ranyel Maliwanag    - Removed logic that updates the related task status to `Contacted` or `Contact Attempted` based on `Contact Type`
 *       20.JAN.2016    Ranyel Maliwanag    - Added processing of Census Eligibility Items for after update operations
 *       04.MAR.2016    Ranyel Maliwanag    - Added logic to update open activity count on enrolment
 */
public without sharing class TaskTriggerHandler {
    /**
     * @description Handles all before insert operations for the Task object
     * @author Ranyel Maliwanag
     * @date 14.MAY.2015
     */
    public static void onBeforeInsert(List<Task> tasks) {
        setDefaultContactTypeAndOutcome(tasks);
    }


    /**
     * @description Handles all before update operations for the Task object
     * @author Ranyel Maliwanag
     * @date 06.JUL.2015
     */
    public static void onBeforeUpdate(List<Task> tasks, Map<Id, Task> oldTaskMap) {
    }


    /**
     * @description Handles all after insert operations for the Task object
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     */
    public static void onAfterInsert(List<Task> tasks) {
        updateLeadTasksCount(tasks);
        
        CensusEligibilityController.EligibilityForCensusMapper(tasks);

        updateLastSuccessfulContactDate(tasks);
        updateEnrolmentOpenActivityCount(tasks);
    }


    /**
     * @description Handles all after update operations for the Task object
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     * @history
     *       20.JAN.2016    Ranyel Maliwanag    - Added processing of Census Eligibility Items for after update operations
     */
    public static void onAfterUpdate(List<Task> tasks, Map<Id, Task> oldMap) {
        updateLeadTasksCount(tasks);

        List<Task> forEligibilityProcessing = new List<Task>();
        for (Task record : tasks) {
            if (record.Eligibility_Items__c != oldMap.get(record.Id).Eligibility_Items__c) {
                // Eligibility items have changed
                forEligibilityProcessing.add(record);
            }
        }
        if (!forEligibilityProcessing.isEmpty()) {
            CensusEligibilityController.EligibilityForCensusMapper(forEligibilityProcessing);
        }

        updateLastSuccessfulContactDate(tasks);
        updateEnrolmentOpenActivityCount(tasks);
    }


    /**
     * @description Handles all after delete operations for the Task object
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     */
    public static void onAfterDelete(List<Task> tasks) {
        updateLeadTasksCount(tasks);
        updateEnrolmentOpenActivityCount(tasks);
    }


    /**
     * @description Updates the running total of tasks open on the lead record
     * @author Ranyel Maliwanag
     * @date 12.MAY.2015
     * @history
     *       15.MAY.2015    Ranyel Maliwanag    - Updated logic to consider whether or not a lead has been successfully contacted and update the lead record using the appropriate status
     *       04.JUN.2015    Ranyel Maliwanag    - Added `Status = Completed` condition for querying related tasks to a lead in `updateLeadTasksCount()`
     *       28.OCT.2015    Ranyel Maliwanag    - Removed logic that updates the related task status to `Contacted` or `Contact Attempted` based on `Contact Type`
     *       04.NOV.2015    Biao Zhang          - Reverse the precede change
     */
    public static void updateLeadTasksCount(List<Task> tasks) {
        Set<Id> taskWhoIds = new Set<Id>();
        Map<Id, List<Task>> whoIdCountMap = new Map<Id, List<Task>>();
        for (Task taskRecord : tasks) {
            taskWhoIds.add(taskRecord.WhoId);
        }

        String queryString = 'SELECT Id, Status, RecordType.Name, (SELECT Id, Contact_Type__c FROM Tasks WHERE Status = \'Completed\') ';
        queryString += 'FROM Lead ';
        queryString += 'WHERE Id IN :taskWhoIds AND IsConverted = false';

        List<Lead> leads = Database.query(queryString);

        for (Lead leadRecord : leads) {
            leadRecord.Total_Tasks__c = leadRecord.Tasks.size();

            if (leadRecord.RecordType.Name == 'TeleSales Student Lead') {
                Boolean isContacted = false;

                // Iterate over tasks for the lead
                for (Task taskRecord : leadRecord.Tasks) {
                    // Look for contacted status
                    if (taskRecord.Contact_Type__c == 'Contacted') {
                        isContacted = true;
                        break;
                    } 
                }
                
                if (leadRecord.Tasks.size() > 0 && (leadRecord.Status == 'Open' || leadRecord.Status == 'Contact Attempted')) {
                    if (isContacted) {
                        leadRecord.Status = 'Contacted';
                    } else {
                        leadRecord.Status = 'Contact Attempted';
                    }
                }
            }
        }

        update leads;
    }


    /**
     * @description Sets the default Contact Type and Contact Outcome field values for tasks created when sending an email from a service case record
     * @author Ranyel Maliwanag
     * @date 14.MAY.2015
     */
    public static void setDefaultContactTypeAndOutcome(List<Task> tasks) {
        // Get prefix value for Service Case object
        String serviceCaseKeyPrefix = Service_Cases__c.SObjectType.GetDescribe().getKeyPrefix();

        // Iterate over task records
        for (Task taskRecord : tasks) {
            // Check if task is related to service case and subject begins with email
            if (taskRecord.WhatId != null && String.isNotEmpty(taskRecord.Subject) && taskRecord.WhatId.getSObjectType().getDescribe().getKeyPrefix() == serviceCaseKeyPrefix && taskRecord.Subject.startsWith('Email:')) {
                // Update Contact Type and Contact OUtcome fields
                System.debug('::: Matching task found');
                taskRecord.Contact_Type__c = 'Email';
                taskRecord.Contact_Outcome__c = 'Email Sent';
            }
        }
    }


    /**
     * @description Updates the `LastSuccessfulContactDate__c` field on Account to reflect a successful contact change
     * @author Ranyel Maliwanag
     * @date 25.JAN.2016
     */
    private static void updateLastSuccessfulContactDate(List<Task> tasks) {
        List<Task> filteredTasks = new List<Task>();
        Set<String> successfulCriteria = new Set<String> {
            'Phone Inbound - Successful',
            'Phone Outbound - Successful',
            //'Phone Outbound - Unsuccessful',
            'SMS',
            'Email',
            'Email - Successful'
        };

        for (Task record : tasks) {
            if (successfulCriteria.contains(record.Contact_Type__c)) {
                filteredTasks.add(record);
            }
        }

        if (!filteredTasks.isEmpty()) {
            List<Account> accountsForUpdate = new List<Account>();
            Set<Id> accountIds = new Set<Id>();
            Set<Id> enrolmentIds = new Set<Id>();

            // Build the set of account Ids to avoid duplicate records for insertion
            for (Task record : filteredTasks) {
                if (String.isNotBlank(record.WhatId) && String.valueOf(record.WhatId).startsWith('a1K')) {
                    enrolmentIds.add(record.WhatId);
                }
            }

            // Query Enrolments
            List<Enrolment__c> enrolments = [SELECT Id, Student__c FROM Enrolment__c WHERE Id IN :enrolmentIds];

            // Build list of account Ids
            for (Enrolment__c enrolment : enrolments) {
                accountIds.add(enrolment.Student__c);
            }

            // Update the Last Assessment Attempt Date for all collected accounts
            for (Id accountId : accountIds) {
                Account accountRecord = new Account(
                        Id = accountId,
                        LastSuccessfulContactDate__c = Datetime.now()
                    );
                accountsForUpdate.add(accountRecord);
            }
            update accountsForUpdate;
        }
    }


    /**
     * @description Updates the total open activity count for related enrolments. To be used in the SEC
     * @author Ranyel Maliwanag
     * @date 15.FEB.2016
     */
    private static void updateEnrolmentOpenActivityCount(List<Task> tasks) {
        Set<Id> enrolmentIds = new Set<Id>();

        System.debug('tasks ::: ' + tasks);

        for (Task record : tasks) {
            if (String.isNotBlank(record.WhatId) && String.valueOf(record.WhatId).startsWith('a1K')) {
                enrolmentIds.add(record.WhatId);
            }
        }

        List<Enrolment__c> enrolments = [SELECT Id, (SELECT Id FROM Tasks WHERE Status != 'Completed' AND RecordType.Name = 'Online Task') FROM Enrolment__c WHERE Id IN :enrolmentIds];

        System.debug('enrolments ::: ');

        for (Enrolment__c enrolment : enrolments) {
            enrolment.OpenActivityCount__c = enrolment.Tasks.size();
        }

        if (!enrolments.isEmpty()) {
            update enrolments;
        }
    }
}