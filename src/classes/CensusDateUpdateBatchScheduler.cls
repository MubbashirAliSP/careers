global class CensusDateUpdateBatchScheduler implements Schedulable {

   global void execute(SchedulableContext SC) {  
      
       CensusDateUpdateBatch b = new CensusDateUpdateBatch();
    
           
       if(!Test.IsRunningTest())
           database.executeBatch(b);
   }
   
}