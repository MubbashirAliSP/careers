/**
 * @description Schedulable interface implementation for the Lead Queue Assignment Batch
 * @author Ranyel Maliwanag
 * @date 15.MAY.2015
 */
public with sharing class LeadQueueAssignmentSchedulable implements Schedulable {
    public LeadQueueAssignmentBatch batchProcess;
    public Integer batchSize;

    public LeadQueueAssignmentSchedulable() {
        batchProcess = new LeadQueueAssignmentBatch();
        batchSize = 200;
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(batchProcess, batchSize);
    }
}