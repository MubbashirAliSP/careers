/**
 * @description Central trigger handler for the `Census__c` object
 * @author Ranyel Maliwanag
 * @date 27.NOV.2015
 */
public without sharing class CensusTriggerHandler {
	public static void onAfterUpdate(List<Census__c> records, Map<Id, Census__c> recordsMap) {
		List<Id> recordIdsForInvoiceGeneration = new List<Id>();
		for (Census__c record : records) {
			if (!recordsMap.get(record.Id).IsInvoiceGenerated__c && record.IsInvoiceGenerated__c && String.isNotBlank(record.StudentEmail__c)) {
				recordIdsForInvoiceGeneration.add(record.Id);
			}
		}

		if (!recordIdsForInvoiceGeneration.isEmpty()) {
			createInvoiceGenerationActivity(UserInfo.getSessionId(), recordIdsForInvoiceGeneration);
		}
	}


	/** 
	 * @description 
	 * @author Ranyel Maliwanag
	 * @date 27.NOV.2015
	 */
	@future(callout=true)
	public static void createInvoiceGenerationActivity(String sessionId, List<Id> recordIdsForInvoiceGeneration) {
		if (!recordIdsForInvoiceGeneration.isEmpty()) {
			HttpRequest req = new HttpRequest();
			req.setEndpoint('https://' + Url.getSalesforceBaseUrl().getHost() + '/services/apexrest/PDFGeneration/');
			req.setMethod('POST');
			req.setBody('{"recordIdList":' + JSON.serialize(recordIdsForInvoiceGeneration) + ', "sObjectType":"Census__c"}');
			req.setHeader('Authorization', 'Bearer ' + sessionId);
			req.setHeader('Content-Type', 'application/json');
			Http http = new Http();
			if (!Test.isRunningTest()) {
				HttpResponse res = http.send(req);
			}
		}
	}
}