global class CensusEligibilityNotificationScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
    CensusEligibilityNotificationBatch b = new CensusEligibilityNotificationBatch();
        database.executebatch(b, 200);
    }

}