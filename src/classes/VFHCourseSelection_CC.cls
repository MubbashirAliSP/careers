/*
 * @description Main Controller handler for the VFH Enrolment Form Controller Step 1-6.
 * @author D. Crisologo
 * @date 21.JUN.2013
 * @date 27.MAR.2014    W. Malibago -- added filter for query
 * @date 10.MAR.2015    P. Mendoza -- added filter for query
*/
public class VFHCourseSelection_CC {

    public boolean doubleDiplomaSelected {get; set;}
    public boolean singleDiplomaSelected {get; set;}
    public boolean deferAllVFHDebt {get; set;}
    
    public String selectedDoubleDiplomaId {get; set;}
    public String selectedSingleDiplomaQualification {get; set;}
    public List<SingleDiplomaQualifications> qualWrapper {get; set;}
    
    private String leadSource;
    
    private List<Double_Diploma_Mapping__c> ddMappingList;

    /*
     * @description Constructor for the VFH Course Selection controller. Initializes all necessary data / values. Pre-loads records.
     * @author D. Crisologo
     * @date 21.Jun.2013
     * @input:        None
     * @returns:      None
    */
    public VFHCourseSelection_CC() {

        this.ddMappingList = new List<Double_Diploma_Mapping__c>();
        this.qualWrapper = new List<SingleDiplomaQualifications>();

        this.doubleDiplomaSelected = false;
        this.singleDiplomaSelected = false;
        this.deferAllVFHDebt = true;
        this.selectedSingleDiplomaQualification = '';
        
        this.leadSource = ApexPages.currentPage().getParameters().get('LeadSource');
        
        for(Double_Diploma_Mapping__c ddm: [SELECT      Id, 
                                                        Double_Diploma_Name__c, 
                                                        Qualification1__c, 
                                                        Qualification2__c 
                                            FROM        Double_Diploma_Mapping__c 
                                            WHERE       Qualification1__r.Active__c = false
                                             AND        Qualification1__r.VFH_Approved__c = true
                                             AND        Qualification2__r.Active__c = false
                                             AND        Qualification2__r.VFH_Approved__c = true
                                             //PMM Start 10/03/2015
                                             AND        Available_for_Online_Enrolments__c = true
                                             //End
                                            ORDER BY    Double_Diploma_Name__c]) {
            ddMappingList.add(ddm);
        }
        
        for(Qualification__c qf: [SELECT     Id,
                                             Vocational_Training_Area__c, 
                                             Field_of_Education__r.Name, 
                                             Award_based_on__r.Qualification_Name__c, Online_Enrolment_Course_Name__c,
                                             (Select Delivery_Mode_Common_Name__c from Qualification_Delivery_Modes__r)
                                  FROM       Qualification__c
                                  WHERE      RecordType.DeveloperName = 'Tailored_Qualification'
                                   AND       Active__c = false
                                   AND       VFH_Approved__c = true
                                   //WBM Start 27/03/2014
                                   AND       Available_for_Online_Enrolments__c = true
                                   //WBM End
                                   AND       IsAccountSpecific__c = false
                                  ORDER BY   Online_Enrolment_Course_Name__c]) {
            system.debug('**ID: ' + qf.Id);                      
            qualWrapper.add(new SingleDiplomaQualifications(qf, qf.Qualification_Delivery_Modes__r));
        }

    }
    
    /*
     * @description checks if leadsource parameter is present. If not, redirect to unauthorized page.
     * @author D. Crisologo
     * @date 24.Jun.2013
     * @input:        None
     * @returns:      None
    */
    public PageReference validateParameters() {
        //if lead source is null, make the page unauthorized
        if(ApexPages.currentPage().getParameters().get('LeadSource')==null) {
            return Page.Unauthorized;
        } else {
            return null;
        }
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description Getter method that returns select options picklist for all diploma names
    * @param: none
    * @return List<SelectOption> - select list option that contains all double diploma names
    */ 
    public List<SelectOption> getDiplomaNames(){
        List<SelectOption> options = new List<SelectOption>();
        for(Double_Diploma_Mapping__c ddm: ddMappingList) {
            options.add(new SelectOption(ddm.Id,ddm.Double_Diploma_Name__c));
        }
        return options; 
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description sets the double diploma switch to true; sets the single diploma switch to false;
    * @param: none
    */ 
    public void doubleDiplomaClick() {
        this.doubleDiplomaSelected = true;
        this.singleDiplomaSelected = false;
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description sets the double diploma switch to false; sets the single diploma switch to true;
    * @param: none
    */ 
    public void singleDiplomaClick() {
        this.doubleDiplomaSelected = false;
        this.singleDiplomaSelected = true;
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description calls the form page, passing the two qualifications selected.
    * @param: none
    */ 
    public PageReference applyDoubleDiploma() {
        PageReference pr = Page.VFHEnrolmentForm1;
        
        pr.getParameters().put('DiplomaType', 'D');
        pr.getParameters().put('dd', this.selectedDoubleDiplomaId);
        pr.getParameters().put('LeadSource', this.leadSource);
        pr.setRedirect(true);
        
        return pr;
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description calls the form page, passing the selected qualification in the parameter
    * @param: none
    */ 
    public PageReference applySingleDiploma() {
        PageReference pr = Page.VFHEnrolmentForm1;
        
        pr.getParameters().put('DiplomaType', 'S');
        pr.getParameters().put('ql', this.selectedSingleDiplomaQualification);
        pr.getParameters().put('LeadSource', this.leadSource);
        pr.setRedirect(true);
        
        return pr;
    }
    
     /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 24.Jun.2013
    * @description Getter method that returns yes/no selection
    * @param 
    */ 
    public List<SelectOption> getYesNoOption() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('True','Yes')); 
        options.add(new SelectOption('False','No')); 
        return options; 
    }
    
    /**
    * @author Original: Dan Crisologo Cloud Sherpas
    * @date Original: 21.JUN.2013
    * @description wrapper class to be used for apex:table in course selection page
    * @param: none
    */ 
    public class SingleDiplomaQualifications {
    
        public Id qualificationId {get; set;}
        public String vocationalTraining {get; set;}
        public String fieldOfEducation {get; set;}
        public String courseName {get; set;}
        public String delivery {get; set;}
        
        public SingleDiplomaQualifications(Qualification__c q, List<Qualification_Delivery_Mode__c> deliveryModes) {
        
            this.qualificationId = q.Id;
            this.vocationalTraining = q.Vocational_Training_Area__c;
            this.fieldOfEducation = q.Field_of_Education__r.Name; 
            this.courseName = q.Online_Enrolment_Course_Name__c;
            
            String deliverModes = '';
            
            if(deliveryModes.size()>0) {
                for(Qualification_Delivery_Mode__c qdm: deliveryModes) {
                    deliverModes = deliverModes + qdm.Delivery_Mode_Common_Name__c + ', ';
                }
            } 
            
            if(deliverModes.length()>0) {
                deliverModes = deliverModes.substring(0, deliverModes.lastIndexOf(','));
            }
            
            this.delivery = deliverModes;
        }
                                             
    }

}