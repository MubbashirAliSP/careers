/**
 * @description Test class for `AccountAlertsInlineCX`
 * @author Ranyel Maliwanag
 * @date 04.MAR.2016
 */
@isTest
public with sharing class AccountAlertsInlineCX_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;
	}

	static testMethod void controllerTest1() {
		Account student = [SELECT Id FROM Account];

		Test.setCurrentPage(Page.AccountAlertsInline);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(student);
			AccountAlertsInlineCX controller = new AccountAlertsInlineCX(stdController);

			System.assert(controller.accountAlerts.isEmpty());
		Test.stopTest();
	}

	static testMethod void controllerTest2() {
		Account student = [SELECT Id FROM Account];
		Event alert = new Event(
				RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Account Alert').getRecordTypeId(),
				Subject = 'Okay to contact',
				ActivityDate = Date.today(),
				IsAllDayEvent = true,
				WhatId = student.Id
			);
		insert alert;

		Test.setCurrentPage(Page.AccountAlertsInline);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(student);
			AccountAlertsInlineCX controller = new AccountAlertsInlineCX(stdController);

			System.assert(!controller.accountAlerts.isEmpty());
		Test.stopTest();
	}
}