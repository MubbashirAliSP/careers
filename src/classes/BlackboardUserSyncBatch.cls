global with sharing class BlackboardUserSyncBatch  implements Database.Batchable<sObject>, Database.AllowsCallouts {

  global final String query = 'Select Available_After_Sync__c,Student_Identifier__c,create__c, update__c, deactivate__c, reactivate__c, Birthdate__c,Last_Name__c,First_Name__c,Email__c,Mobile_Phone__c,Blackboard_Primary_Key__c,Blackboard_Username__c, Blackboard_Password__c From Blackboard_User__c Where Sync_Required__c = True';
  
  
  global database.querylocator start(database.batchablecontext BC){
       return Database.getQueryLocator(query);
  }

   global void execute(Database.BatchableContext BC,List<SObject> scope){

        for(SObject s :scope) {
             
             Blackboard_User__c bu = (Blackboard_User__c) s;

             BlackboardUserWS.saveUser(bu);
     }


   }

   global void finish(Database.BatchableContext info){
       
      BlackboardCourseMembershipSyncBatch b1 = new  BlackboardCourseMembershipSyncBatch();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
   
   }
    
}