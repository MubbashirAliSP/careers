/**
 * @description Test class for EnrolmentIntakeHandler 
 * @author Gavi Grandea
 * @date 5.MAY.2014
 */
@isTest
private class EnrolmentIntakeHandler_Test {

    
    static testMethod void EnrolmentIntakeHandler_Test() {
        
        Set<Id> intakeIds = new Set<Id>();
        Set<Id> enrolmentIds = new Set<Id>();
        
        List<Intake__c> intakes = new List<Intake__c>([SELECT id, Name FROM Intake__c LIMIT 5]);
        List<Enrolment__c> enrolments = new List<Enrolment__c>([Select Id, Name FROM Enrolment__c  LIMIT 5]);
        
        for(Intake__c int2 :  intakes){
            intakeIds.add(int2.Id);
        }
        for(Enrolment__c enrl :  enrolments){
            enrolmentIds.add(enrl.Id);
        }
        
        //intakeIds.add('a1X90000000cB5MEAU');
        //enrolmentIds.add('a1X90000000cB5MEAU');
        
        
        
        EnrolmentIntakeHandler.linkEnrolmentIntakes(intakeIds, enrolmentIds);
        
        
        
    }

}