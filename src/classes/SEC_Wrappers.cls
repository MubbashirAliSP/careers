public with sharing class SEC_Wrappers {
	public class PicklistValue {
		@AuraEnabled
		public String label {get; set;}

		@AuraEnabled
		public String value {get; set;}

		@AuraEnabled
		public Boolean isActive {get; set;}

		@AuraEnabled
		public boolean isDefaultValue {get; set;}

		public PicklistValue(Schema.PicklistEntry entry) {
			label = entry.getLabel();
			value = entry.getValue();
			isActive = entry.isActive();
			isDefaultValue = entry.isDefaultValue();
		}
	}

	public class EnrolmentUnit {
		@AuraEnabled
		public String statusClass {get; set;}

		@AuraEnabled
		public Enrolment_Unit__c record {get; set;}

		public EnrolmentUnit(Enrolment_Unit__c euRecord) {
			record = euRecord;
			statusClass = 'incomplete';
			if (record.AVETMISS_National_Outcome__c == '20') {
				statusClass = 'complete';
			} else if (record.Start_Date__c <= Date.today() && statusClass != '20') {
				statusClass = 'current';
			}
		} 
	}
}