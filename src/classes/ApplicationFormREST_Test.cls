@isTest
private class ApplicationFormREST_Test {
	@testSetup
    static void triggerSetup() {
        ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.initialiseFormComponents();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();

        GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        portalSettings.EffectivityDate__c = Date.today().addDays(1);
        portalSettings.LoanApplicationFormCreationOffset__c = 0;
        insert portalSettings;

        ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id
            );
        insert appForm;
    }

	static testMethod void testApplicationFormREST() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c);
        queryString += 'LIMIT 1';
        ApplicationForm__c appForm = Database.query(queryString);
	   	   
	   	RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/ApplicationFormScoring/';  //Request URL
	   	req.httpMethod = 'POST';
	   	RestContext.request = req;
	   	RestContext.response= res;

	   	String resp = ApplicationFormREST.doPost(String.valueOf(appForm.Id), '', '', '', '', '2015-12-14 00:00:00', '2015-12-24 00:00:00'); 
	   	System.assertEquals(resp, 'Success');
	}
	
	
}