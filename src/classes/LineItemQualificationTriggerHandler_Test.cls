/**
 * @description     Test Class for LineItemQualificationTriggerHandler
 * @author          Janella Lauren Canlas
 * @Company         CloudSherpas
 * @date            03.DEC.2012
 *
 * HISTORY
 * - 05.FEB.2013    Janella Canlas      Created.
 * - 08.APR.2014    Michelle Magsarili - Change Status to Active to pass Enrolment Validation
 */
@isTest(seeAllData=true)
private class LineItemQualificationTriggerHandler_Test {
    
    static testMethod void testLIQ() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='LIQ_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Language__c language = new Language__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                state2 = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                uhp2 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
                
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType(); 
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Overseas_Country__c = null;
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                insert enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                update lineItemUnit;
            test.stopTest();
        }
    }
}