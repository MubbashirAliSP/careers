/**
 * @description Custom controller extension for displaying disclaimer information
 * @author Ranyel Maliwanag
 * @date 24.JUN.2015
 */
public with sharing class WebsiteDisclaimerInlineDisplayCX {
    public Website_Disclaimer__c activeRecord {get; set;}

    /** 
     * @description Standard controller constructor for this extension class
     * @author Ranyel Maliwanag
     * @date 24.JUN.2015
     */
	public WebsiteDisclaimerInlineDisplayCX(ApexPages.StandardController controller) {
        Id activeRecordId = controller.getId();
        activeRecord = [SELECT Id, WDO_Disclaimer__c, Website_Disclaimer_Option__r.WDO_State__c, Website_Disclaimer_Option__r.WDO_Funding_Type__c, Website_Disclaimer_Option__r.WDO_Disclaimer__c, RecordType.DeveloperName FROM Website_Disclaimer__c WHERE Id = :activeRecordId];
        if (activeRecord.RecordType.DeveloperName == 'Website_Disclaimer') {
            activeRecord = new Website_Disclaimer__c(
                    WDO_Disclaimer__c = activeRecord.Website_Disclaimer_Option__r.WDO_Disclaimer__c
                );
        }
	}
}