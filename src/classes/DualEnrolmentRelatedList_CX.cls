/**
 * @description Controller Extension for DualEnrolmentRelatedList page
 *              -This displays all related dual enrolment for an enrolment record
 * @author Sairah Hadjinoor
 * @date 04.DEC.2014
 */



public with sharing class DualEnrolmentRelatedList_CX {

    public Enrolment__c enrolment{get;set;}
    public List<Dual_Enrolment__c> dualEnrolmentList{get;set;} 

    public DualEnrolmentRelatedList_CX(ApexPages.StandardController controller) {
        this.enrolment = ( Enrolment__c ) controller.getRecord();
        this.enrolment = [Select Name from Enrolment__c where Id=: this.enrolment.id];
    }

    public List<Dual_Enrolment__c> getDualEnrolments(){
        dualEnrolmentList = [Select Name, Enrolment_1__c,Enrolment_1__r.Name,Enrolment_2__c,Enrolment_2__r.Name from Dual_Enrolment__c where Enrolment_1__c=:this.enrolment.Id OR
                            Enrolment_2__c=:this.enrolment.Id];
        return dualEnrolmentList;
    }
     
      public Pagereference deleteOpp()
     {
         String dualEnrol= ApexPages.currentPage().getParameters().get('dualEnrolmentId'); 
         
         Dual_Enrolment__c dual = [Select id from Dual_Enrolment__c where id =:dualEnrol limit 1];
         if(dual !=null && dual.id !=null){
          delete dual;
         }
        return new PageReference('/'+this.enrolment.Id);
     }
     public String getURLParam(){
        Schema.DescribeSObjectResult r = Dual_Enrolment__c.sObjectType.getDescribe();
        DualEnrolmentFieldIds__c dualEnrolmentFields = DualEnrolmentFieldIds__c.getInstance('Enrolment');
        String urlParam = '';
        urlParam = '/'+r.getKeyPrefix()+'/e?CF'+dualEnrolmentFields.Enrolment_1__c+'='+this.enrolment.Name+'&CF'+dualEnrolmentFields.Enrolment_1__c+'_lkid='+this.enrolment.Id+'&retURL=/'+this.enrolment.Id;
        
        return urlParam;
     }
}