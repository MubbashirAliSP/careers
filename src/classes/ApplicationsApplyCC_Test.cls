/**
 * @description Test class for `ApplicationsApplyCC` class
 * @author Ranyel Maliwanag
 * @date 14.JUL.2015
 */
@isTest
public with sharing class ApplicationsApplyCC_Test {
    @testSetup
    static void setupController() {
        Id studentLeadRTID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student Lead').getRecordTypeId();

        ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.initialiseFormComponents();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();

        GuestLogin__c guestLogin = ApplicationsTestUtilities.createGuestLogin();
        guestLogin.AddressState__c = 'QLD';
        insert guestLogin;

        Account student = new Account(
                RecordTypeId = studentLeadRTID,
                LastName = 'Record'
            );
        insert student;

        Opportunity opportunityRecord = new Opportunity(
                Name = 'Test Record',
                StageName = 'Prospecting',
                CloseDate = Date.today(),
                AccountId = student.Id
            );
        insert opportunityRecord;

        ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
        Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
        Qualification__c qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        Funding_Stream_Types__c fundingStream = TestCoverageUtilityClass.createFundingStream();
        PortalVisibility__c pv = ApplicationsTestUtilities.createPortalVisibility(fundingStream.Id, qual.Id);
        insert pv;
    }

    static testMethod void staffControllerTest() {
        Test.startTest();
            // Load initial page
            // Assertion: User is recognised as staff
            Test.setCurrentPage(Page.ApplicationsApply);
            ApplicationsApplyCC controller = new ApplicationsApplyCC();
            System.assert(controller.isStaff);

            // Assertion: Application Form null
            System.assertEquals(null, controller.applicationForm);
        Test.stopTest();
    }

    static testMethod void staffRedirectsTest() {
        Test.setCurrentPage(Page.ApplicationsApply);

        Test.startTest();
            ApplicationsApplyCC controller = new ApplicationsApplyCC();
            System.assert(controller.isStaff);

            Test.setCurrentPage(controller.evaluateRedirects());
            controller = new ApplicationsApplyCC();
            controller.evaluateRedirects();
        Test.stopTest();
    }


    static testMethod void staffOpportunityTest() {
        Opportunity opportunityRecord = [SELECT Id FROM Opportunity];

        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().getParameters().put('oid', opportunityRecord.Id);

        Test.startTest();
            ApplicationsApplyCC controller = new ApplicationsApplyCC();
            System.assert(controller.isStaff);

            Test.setCurrentPage(controller.evaluateRedirects());
            controller = new ApplicationsApplyCC();
            controller.evaluateRedirects();
        Test.stopTest();
    }

    static testMethod void newStudentRegistrationTest1() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        Test.startTest();
            System.runAs(guestUser) {
                // Initial assertions: User is not authenticated
                Test.setCurrentPage(Page.ApplicationsApply);
                ApplicationsApplyCC controller = new ApplicationsApplyCC();
                System.assert(!controller.isAuthenticated);

                // User enters incomplete details
                // Assertion: Saving will fail and an error message will be displayed
                controller.studentCredentials.Email__c = 'triddle@hogwarts.edu';
                controller.studentCredentials.FirstName__c = 'Tom';
                controller.studentCredentials.LastName__c = 'Riddle';
                controller.studentCredentials.AddressState__c = 'QLD';
                controller.startApplication();
                System.assertEquals(null, controller.studentCredentials.Id);
                System.assert(ApexPages.hasMessages());
            }
        Test.stopTest();
    }


    static testMethod void newStudentRegistrationTest2() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        Test.startTest();
            System.runAs(guestUser) {
                // Initial assertions: User is not authenticated
                Test.setCurrentPage(Page.ApplicationsApply);
                ApplicationsApplyCC controller = new ApplicationsApplyCC();
                System.assert(!controller.isAuthenticated);

                // User enters complete details
                // Assertion: Guest login will be created
                controller.studentCredentials.Email__c = 'adumbledore@hogwarts.edu';
                controller.studentCredentials.FirstName__c = 'Albus';
                controller.studentCredentials.LastName__c = 'Dumbledore';
                controller.studentCredentials.AddressState__c = 'QLD';
                controller.studentCredentials.Password__c = 'TheElderWand';
                controller.studentCredentials.Phone__c = '123';
                Test.setCurrentPage(controller.startApplication());
                //controller = new ApplicationsApplyCC();
                System.assertNotEquals(null, controller.studentCredentials.Id);

                // Assertion: User will be authenticated
                System.assert(controller.isAuthenticated);
                // Assertion: A FormDisplay object is generated for the user to fill in the details
                //System.assertNotEquals(null, controller.form);
            }
        Test.stopTest();
    }
    


    static testMethod void submitFormFailTest() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);

        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        ApplicationsApplyCC controller = new ApplicationsApplyCC();
        Test.startTest();
            // Submit form
            PageReference result = controller.saveForm();
            // Assertion: Returned page reference will be null
            System.assertEquals(null, result);
            System.assert(ApexPages.hasMessages());
        Test.stopTest();
    }


    static testMethod void applicationStep1Test() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);

        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        Test.startTest();
            System.runAs(guestUser) {
                ApplicationsApplyCC controller = new ApplicationsApplyCC();

                // Assertion: Step 1 is rendered
                System.assert(controller.isApplicationTypeSelectionRendered);

                // Assertion: CourseSelect is not null
                System.assertNotEquals(null, controller.courseSelect);

                controller.proceedWithSelection();
            }
        Test.stopTest();
    }


    static testMethod void applicationStep2Test() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = guestLogin.Id,
                IsMainApplicationForm__c = true,
                StateId__c = stateQLD.Id
            );
        insert form;

        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        ApexPages.currentPage().getParameters().put('formId', form.Id);

        Test.startTest();
            System.runAs(guestUser) {
                ApplicationsApplyCC controller = new ApplicationsApplyCC();
                controller.evaluateRedirects();

                System.assertEquals('ca-enrolment-form', controller.formStyleClass);
            }
        Test.stopTest();
    }


    static testMethod void applicationLoanTest() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);

        Id loanRTID = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId();

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = guestLogin.Id,
                IsMainApplicationForm__c = true,
                StateId__c = stateQLD.Id,
                RecordTypeId = loanRTID
            );
        insert form;

        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        ApexPages.currentPage().getParameters().put('formId', form.Id);

        Test.startTest();
            System.runAs(guestUser) {
                ApplicationsApplyCC controller = new ApplicationsApplyCC();

                // Assertion: Rendering is loan form
                System.assertEquals('ca-loan-form', controller.formStyleClass);

                // Assertion: Loan form is not null
                System.assertNotEquals(null, controller.loanForm);
            }
        Test.stopTest();
    }


    static testMethod void applicationLoanTest2() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);

        Id loanRTID = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId();

        LoanForm__c loanForm = new LoanForm__c(
                ApprovedTrainingProvider__c = 'CAG',
                FamilyName__c = guestLogin.LastName__c,
                GivenNames__c = guestLogin.FirstName__c,
                VETCourse__c = 'Herbology'
            );
        insert loanForm;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = guestLogin.Id,
                IsMainApplicationForm__c = true,
                StateId__c = stateQLD.Id,
                RecordTypeId = loanRTID,
                LoanFormId__c = loanForm.Id
            );
        insert form;

        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        ApexPages.currentPage().getParameters().put('formId', form.Id);
        Test.startTest();
            System.runAs(guestUser) {
                ApplicationsApplyCC controller = new ApplicationsApplyCC();

                // Assertion: Rendering is loan form
                System.assertEquals('ca-loan-form', controller.formStyleClass);

                // Assertion: Loan form is not null
                System.assertNotEquals(null, controller.loanForm);
            }
        Test.stopTest();
    }


    static testMethod void debugFlagTest() {
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().getParameters().put('debug', 'true');
        Test.startTest();
            ApplicationsApplyCC controller = new ApplicationsApplyCC();

            // Assertion: Debug flag is activated
            System.assert(controller.isDebug);
        Test.stopTest();
    }


    static testMethod void currentPageTest() {
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().getParameters().put('page', '2');
        Test.startTest();
            ApplicationsApplyCC controller = new ApplicationsApplyCC();

            // Assertion: Current page is set to 2
            System.assertEquals(2, controller.currentPage);
        Test.stopTest();
    }


    static testMethod void coverageOnly() {
        User guestUser = [SELECT Id FROM User WHERE Profile.Name = 'Applications Profile' LIMIT 1];
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        GuestLogin__c guestLogin = Database.query(queryString);
                
        // Initial page setup
        Test.setCurrentPage(Page.ApplicationsApply);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', guestLogin.Id, null, -1, true)});
        Test.startTest();
            System.runAs(guestUser) {
                ApplicationsApplyCC controller = new ApplicationsApplyCC();
                controller.evaluateRedirects();
                controller.generateRandomString(8);
                controller.returnToOpportunity();

                controller.studentEmail = 'someone@somenoe.com';
                //controller.ipAddress = '12345';


                controller.searchCredentials();

                controller.courseSelect.courseType = 'diploma';
                controller.courseSelect.residentState = 'QLD';
                controller.courseSelect.fundingType = 'STATEFUNDING';

                PortalVisibility__c pv = [select id from PortalVisibility__c];
                controller.courseSelect.courseOrPortalVisibilityId = pv.Id;


                System.debug(controller.courseSelect.availableDualQualifications);
                System.debug(controller.formStyleClass);
                System.debug(controller.isPrint);
                System.debug(controller.renderAs);
                System.debug(controller.isSubmitPage);
                System.debug(controller.isDebug);
                System.debug(controller.staffCredentials);
                System.debug(controller.isApplicationFormRendered);
                System.debug(controller.isStudentCredentialsSearchRendered);
                System.debug(controller.courseSelect.residentStates);
                System.debug(controller.courseSelect.fundingTypes);
                System.debug(controller.courseSelect.courseTypes);
                System.debug(controller.courseSelect.courseTypeToCodeMap);
                System.debug(controller.courseSelect.availableCertificateQualifications);
                System.debug(controller.courseSelect.courseTypeToCodeMap);
                //system.debug(controller.courseSelect.);
            }
        Test.stopTest();
    }

}