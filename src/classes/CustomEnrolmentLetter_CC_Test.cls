/**
 * @author         Clarence Dalan Jr
 * @date           19 August, 2015
 * @description    Test class for CustomEnrolmentLetter_CC class.
 *
 * @history        Clarence Dalan Jr  19 August, 2015    Created
*/

@isTest
public class CustomEnrolmentLetter_CC_Test {

    static testMethod void validateCustomEnrolmentLetterNormalSave () {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Variation_Reason_Code__c variation = new Variation_Reason_Code__c();
        Delivery_Strategy__c strategy = new Delivery_Strategy__c();
        Results__c res = new Results__c();
        Results__c resNat = new Results__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        
        PageReference customEnrolmentLetter = Page.CustomEnrolmentLetter;
        Enrolment_Letters__c enrolmentLetterController = new Enrolment_Letters__c();
        Id recordTypeId = Schema.SObjectType.Enrolment_Letters__c.getRecordTypeInfosByName().get('INT - Intervention Strategy').getRecordTypeId();
        User trainerEducator = new User(Alias = 'trainEdu', Email='trainerEducator@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Educator', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='trainerEducator@testorg.com');
        
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                qual2 = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                
                unit2 = TestCoverageUtilityClass.createUnit();
                unit2.Parent_UoC__c = unit.Id;
                update unit2;
                
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2.Don_t_auto_add_this_unit_to_enrolments__c = true;
                update qunit2;

                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                state.Name = 'Queensland';
                update state;
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual2.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                
                del = TestCoverageUtilityClass.createDeliveryModeType();
                strategy = TestCoverageUtilityClass.createDeliveryStrategy(del.Id);
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Delivery_Strategy__c = strategy.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005'; 
                enrl.Other_Zip_Postal_Code__c = '4005'; 
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Mailing_State_Provience__c = 'Queensland';  
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;  
                insert enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                eu.Start_Date__c = Date.TODAY() - 25;
                eu.End_Date__c = Date.TODAY() - 1;
                update eu;
        }
                insert new Conga_Query__c(Name='Campus Info');
                insert new Conga_Query__c(Name='Enrolment Letters Information');
                insert new Conga_Query__c(Name='RTO Information');
                insert new Conga_Query__c(Name='Placeholder');
                insert new Conga_Query__c(Name='Student Information');
                insert new Conga_Template__c(Name='INT - Intervention Strategy');
                
                enrolmentLetterController.Enrolment__c = enrl.Id;
                enrolmentLetterController.Trainer_Educator__c = trainerEducator.Id;
                enrolmentLetterController.RecordTypeId = recordTypeId;
                insert enrolmentLetterController;
                
                customEnrolmentLetter.getParameters().put('CF00N90000004xMeo_lkid', enrl.Id);
                customEnrolmentLetter.getParameters().put('RecordType',recordTypeId);
                Test.setCurrentPage(customEnrolmentLetter);
                
                ApexPages.StandardController standardController = new ApexPages.StandardController(enrolmentLetterController);
                CustomEnrolmentLetter_CC customEnrolmentLetterController = new CustomEnrolmentLetter_CC(standardController);
                
                CustomEnrolmentLetter_CC.wEUList wEuListVal = new CustomEnrolmentLetter_CC.wEUList(eu);
                wEuListVal.selected = true;
                
                customEnrolmentLetterController.euList = new List<CustomEnrolmentLetter_CC.wEUList>{wEuListVal};
                PageReference saveReference = customEnrolmentLetterController.save();
                
                
                ApexPages.Message[] pageMessages = ApexPages.getMessages();
                
                System.assert(pageMessages.size() == 0, 'An error occured.');
                
                test.stopTest();
    }
    
    static testMethod void validateCustomEnrolmentLetterEmptyListSave () {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Variation_Reason_Code__c variation = new Variation_Reason_Code__c();
        Delivery_Strategy__c strategy = new Delivery_Strategy__c();
        Results__c res = new Results__c();
        Results__c resNat = new Results__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        
        PageReference customEnrolmentLetter = Page.CustomEnrolmentLetter;
        Enrolment_Letters__c enrolmentLetterController = new Enrolment_Letters__c();
        Id recordTypeId = Schema.SObjectType.Enrolment_Letters__c.getRecordTypeInfosByName().get('INT - Intervention Strategy').getRecordTypeId();
        User trainerEducator = new User(Alias = 'trainEdu', Email='trainerEducator@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Educator', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='trainerEducator@testorg.com');
        
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                qual2 = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                
                unit2 = TestCoverageUtilityClass.createUnit();
                unit2.Parent_UoC__c = unit.Id;
                update unit2;
                
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2.Don_t_auto_add_this_unit_to_enrolments__c = true;
                update qunit2;

                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                state.Name = 'Queensland';
                update state;
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual2.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                
                del = TestCoverageUtilityClass.createDeliveryModeType();
                strategy = TestCoverageUtilityClass.createDeliveryStrategy(del.Id);
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Delivery_Strategy__c = strategy.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005'; 
                enrl.Other_Zip_Postal_Code__c = '4005'; 
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Mailing_State_Provience__c = 'Queensland';  
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;     
                insert enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                eu.Start_Date__c = Date.TODAY() - 25;
                eu.End_Date__c = Date.TODAY() - 1;
                update eu;
        }
                enrolmentLetterController.Enrolment__c = enrl.Id;
                enrolmentLetterController.Trainer_Educator__c = trainerEducator.Id;
                enrolmentLetterController.RecordTypeId = recordTypeId;
                insert enrolmentLetterController;
                
                customEnrolmentLetter.getParameters().put('CF00N90000004xMeo_lkid', enrl.Id);
                customEnrolmentLetter.getParameters().put('RecordType',recordTypeId);
                Test.setCurrentPage(customEnrolmentLetter);
                
                ApexPages.StandardController standardController = new ApexPages.StandardController(enrolmentLetterController);
                CustomEnrolmentLetter_CC customEnrolmentLetterController = new CustomEnrolmentLetter_CC(standardController);
                
                customEnrolmentLetterController.save();
                
                ApexPages.Message[] pageMessages = ApexPages.getMessages();
                
                System.assert(pageMessages.size() != 0, 'An Error Message was expected');
                
                Boolean messageFound = false;

                for(ApexPages.Message message : pageMessages) {
                    if(message.getDetail() == 'Error: Please select at least 1 EU.'
                    && message.getSeverity() == ApexPages.Severity.ERROR) {
                        messageFound = true;        
                    }
                }
                
                System.assert(messageFound, 'The error message was not found');
                
                test.stopTest();
    }
    
    static testMethod void validateNormalCancel() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Variation_Reason_Code__c variation = new Variation_Reason_Code__c();
        Delivery_Strategy__c strategy = new Delivery_Strategy__c();
        Results__c res = new Results__c();
        Results__c resNat = new Results__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        
        PageReference customEnrolmentLetter = Page.CustomEnrolmentLetter;
        Enrolment_Letters__c enrolmentLetterController = new Enrolment_Letters__c();
        Id recordTypeId = Schema.SObjectType.Enrolment_Letters__c.getRecordTypeInfosByName().get('INT - Intervention Strategy').getRecordTypeId();
        User trainerEducator = new User(Alias = 'trainEdu', Email='trainerEducator@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Educator', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='trainerEducator@testorg.com');
        
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                qual2 = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                
                unit2 = TestCoverageUtilityClass.createUnit();
                unit2.Parent_UoC__c = unit.Id;
                update unit2;
                
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2.Don_t_auto_add_this_unit_to_enrolments__c = true;
                update qunit2;

                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                state.Name = 'Queensland';
                update state;
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual2.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                
                del = TestCoverageUtilityClass.createDeliveryModeType();
                strategy = TestCoverageUtilityClass.createDeliveryStrategy(del.Id);
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Delivery_Strategy__c = strategy.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005'; 
                enrl.Other_Zip_Postal_Code__c = '4005'; 
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Mailing_State_Provience__c = 'Queensland';  
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;     
                insert enrl;
                
                res = TestCoverageUtilityClass.createResult(state.Id);
                eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                eu.Start_Date__c = Date.TODAY() - 25;
                eu.End_Date__c = Date.TODAY() - 1;
                update eu;
        }
                enrolmentLetterController.Enrolment__c = enrl.Id;
                enrolmentLetterController.Trainer_Educator__c = trainerEducator.Id;
                enrolmentLetterController.RecordTypeId = recordTypeId;
                insert enrolmentLetterController;
                
                customEnrolmentLetter.getParameters().put('CF00N90000004xMeo_lkid', enrl.Id);
                customEnrolmentLetter.getParameters().put('RecordType',recordTypeId);
                Test.setCurrentPage(customEnrolmentLetter);
                
                ApexPages.StandardController standardController = new ApexPages.StandardController(enrolmentLetterController);
                CustomEnrolmentLetter_CC customEnrolmentLetterController = new CustomEnrolmentLetter_CC(standardController);
                
                PageReference cancelPage = customEnrolmentLetterController.cancel();
                String correctUrl = '/'+enrl.Id;
                System.assert(cancelPage.getUrl().equals(correctUrl), 'Cancel did not work correctly, user was redirected to '+ cancelPage.getUrl() + ' and not to '+correctUrl);
                
                test.stopTest();
    }
}