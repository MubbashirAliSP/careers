/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchUnit implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchUnit() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPU_Description__c, IPU_End_Date__c, IPU_InPlace_Discipline__r.Name, IPU_InPlace_faculty__r.Name, InPlace_Unit__c, InPlace_Unit_Code__c, IPU_Start_Date__c, IPU_Sync_on_next_update__c, IPU_Unit_Version__c, Name, (SELECT Id, Version_Number__c, InPlace_Discipline__r.Name FROM InPlaceUnitVersions__r) Id ';
        queryString += 'FROM Unit__c ';
        queryString += 'WHERE InPlace_Unit__c = true AND IPU_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Unit__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Unit__c record : records) {
            if (!exportedIdentifiers.contains(record.InPlace_Unit_Code__c)) {
                InPlace__c forExport = new InPlace__c(
                        IPUnit_UnitCode__c = record.InPlace_Unit_Code__c
                    );
                for (InPlace_Unit_Version__c unitVersion : record.InPlaceUnitVersions__r) {
                    InPlace__c uvExport = new InPlace__c(
                            IPUnitVersion_Description__c = record.IPU_Description__c,
                            IPUnitVersion_DisciplineCode__c = record.IPU_InPlace_Discipline__r.Name,
                            IPUnitVersion_EndDate__c = record.IPU_End_Date__c,
                            IPUnitVersion_FacultyCode__c = record.IPU_InPlace_faculty__r.Name,
                            IPUnitVersion_StartDate__c = record.IPU_Start_Date__c,
                            IPUnitVersion_UnitCode__c = record.InPlace_Unit_Code__c,
                            IPUnitVersion_UnitVersion__c = record.IPU_Unit_Version__c
                        );
                    exports.add(uvExport);
                }
                exports.add(forExport);
                exportedIdentifiers.add(record.InPlace_Unit_Code__c);
            }
            
            record.IPU_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchUnitOffering nextBatch = new InPlaceExportBatchUnitOffering();
        Database.executeBatch(nextBatch);
    }
}