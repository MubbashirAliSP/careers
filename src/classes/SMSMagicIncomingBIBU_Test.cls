@isTest
private class SMSMagicIncomingBIBU_Test{

    static testMethod void triggerForOnlyMobileNumber(){
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student';
        sacct.FirstName = 'First Name';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.Student_Identifer__c = 'H0001';
        sacct.PersonMobilePhone = '61847292111';
        sacct.Year_Highest_Education_Completed__c = '2001';
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.Salutation = 'Mr.';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        insert sacct;

        smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
        incomingSMS.smagicinteract__external_field__c = 'test';
        incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
        incomingSMS.smagicinteract__Mobile_Number__c = sacct.PersonMobilePhone;
        incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 2';

        insert incomingSMS;

        List<smagicinteract__Incoming_SMS__c> incomingList = [select Id, Account__c, smagicinteract__Mobile_Number__c from smagicinteract__Incoming_SMS__c where smagicinteract__Mobile_Number__c =: sacct.PersonMobilePhone];
        for(smagicinteract__Incoming_SMS__c c : incomingList){
            incomingList[0].Account__c = sacct.Id;
        }

        system.assertEquals(incomingList[0].Account__c,sacct.Id);
    }
}