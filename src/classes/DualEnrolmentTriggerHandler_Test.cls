@isTest
private class DualEnrolmentTriggerHandler_Test {
	
	@isTest static void createDualEnrolment() {
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Variation_Reason_Code__c variation = new Variation_Reason_Code__c();
        Delivery_Strategy__c strategy = new Delivery_Strategy__c();
        Results__c res = new Results__c();
        Results__c resNat = new Results__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Credit_status_Higher_Ed_provider_code__c credStatus = new Credit_status_Higher_Ed_provider_code__c();
        Credit_status_Higher_Ed_provider_code__c credStatus2 = new Credit_status_Higher_Ed_provider_code__c();
        Field_of_Education__c fld2 = new Field_of_Education__c();
        Dual_Enrolment__c newDualEnrolment = new Dual_Enrolment__c();
        
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                qual2 = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                
                unit2 = TestCoverageUtilityClass.createUnit();
                unit2.Parent_UoC__c = unit.Id;
                update unit2;
                
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);                
                qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                qunit2.Don_t_auto_add_this_unit_to_enrolments__c = true;
                update qunit2;
                
                qual.RecordTypeId = '01290000000kNntAAE'; 
                update qual;

                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual2.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                strategy = TestCoverageUtilityClass.createDeliveryStrategy(del.Id);
                attend = TestCoverageUtilityClass.createAttendanceType();
                
                credStatus = TestCoverageUtilityClass.createCreditStatus();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Delivery_Strategy__c = strategy.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005';   
                enrl.Other_Zip_Postal_Code__c = '4005'; 
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation     
                enrl.Mailing_State_Provience__c = 'Queensland';    
                
                //24JUL2014 WBM  
                enrl.Credit_Offered_Value_full_time_load__c = 0.26;
                enrl.Credit_Used_Value_full_time_load__c = 0.26;
                enrl.Field_of_Ed_of_prior_VET_Credit_RPL__c = fld.Id;
                enrl.Credit_status_Higher_Ed_provider__c = credStatus.Id;
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                insert enrl;
                enrl2 = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl2.Overseas_Country__c = null;
                enrl2.Type_of_Attendance__c = attend.Id;
                enrl2.Predominant_Delivery_Mode__c = del.Id;
                enrl2.Delivery_Strategy__c = strategy.Id;
                enrl2.Mailing_Country__c = 'AUSTRALIA';
                enrl2.Mailing_Zip_Postal_Code__c = '4005';   
                enrl2.Other_Zip_Postal_Code__c = '4005'; 
                enrl2.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation     
                enrl2.Mailing_State_Provience__c = 'Queensland';    
            test.stopTest();
                
                //24JUL2014 WBM  
                enrl2.Credit_Offered_Value_full_time_load__c = 0.26;
                enrl2.Credit_Used_Value_full_time_load__c = 0.26;
                enrl2.Field_of_Ed_of_prior_VET_Credit_RPL__c = fld.Id;
                enrl2.Credit_status_Higher_Ed_provider__c = credStatus.Id;
                enrl2.Start_Date__c = Date.TODAY() - 50;
                enrl2.End_Date__c = Date.TODAY() + 50;
                insert enrl2;

                newDualEnrolment = TestCoverageUtilityClass.createDualEnrolment(enrl,enrl2);
                delete newDualEnrolment;
                
            
		}
	
	}
}