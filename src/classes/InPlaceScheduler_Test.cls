/**
 * @description Test class for `InPlaceScheduler`
 * @author Ranyel Maliwanag
 * @date 22.JUL.2015
 */
@isTest
public with sharing class InPlaceScheduler_Test {
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('InPlace Schedule Test', schedule, new InPlaceScheduler());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}