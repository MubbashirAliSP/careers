@isTest
public class NATValidationBatchALL_Test {
    
    public static Account stu = NATTestDataFactory.createStudent();
    public static Country__c ctry = NATTestDataFactory.createCountry();
    public static ANZSCO__c anz = NATTestDataFactory.createANZSCO();
    public static Unit__c unt = NATTestDataFactory.createUnit();
    public static Account tOrg = NATTestDataFactory.createTrainingOrganisation();
    public static Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();
    
    public static State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
    public static Results__c res = NATTestDataFactory.createResult(sta.Id);
    public static Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
    public static Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
    public static Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
    public static Locations__c loc2 = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);


    public static Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);   
    public static Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
    public static Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
    public static Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
    public static Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
    public static Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
    public static Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
    public static Enrolment__c enrl = NATTestDataFactory.createEnrolment(stu.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
    public static Enrolment_Unit__c enu1 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);
    public static Enrolment_Unit__c enu2 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);
    
    @isTest (seeAllData=true) public static void testValQLD() {
        
        //string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = '00590000000HR2zAAG' ];
        
        System.runAs(u) {
                    
            String state = 'Queensland';
            String year = '2016';
            
            loc.Delivery_Location_Identifier__c = '232323';
            
            update loc;
                    
            enu1.Report_for_AVETMISS__c = true;
            enu1.Unit_Result__c = res.Id;
            enu1.End_Date__c = date.today() - 1;
            enu2.Report_for_AVETMISS__c = true;
            enu2.End_Date__c = date.today() - 1;
            enu2.Unit_Result__c = res.Id;
            enu1.Training_Delivery_Location__c = loc.id;
            enu2.Training_Delivery_Location__c = loc2.id;
            enu1.Scheduled_Hours__c = 0.0;
    
            update enu1;
            update enu2;
        
            stu.Reporting_Billing_State__c = sta.Id;
            
            stu.Address_building_property_name__c = 'property';
            
            stu.Year_Highest_Education_Completed__c = '@@@@';
            
            update stu;        
                    
            List<String> pconCode = new List<String>{pcon.Contract_Code__c};
    
            List<String> pconId = new List<String>{pcon.Id};
            
            database.executeBatch( new NATValidationBatch_Initial(tOrg.Id, state, year, pconCode, pconId, true, false) );
        }
    
	}
    
    @isTest (seeAllData=true) public static void testValNT() {
        
        //string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = '00590000000HR2zAAG' ];
        
        System.runAs(u) {
                    
            String state = 'Northern Territory';
            String year = '2016';
            
            loc.Delivery_Location_Identifier__c = '232323';
            
            update loc;
                    
            enu1.Report_for_AVETMISS__c = true;
            enu1.Unit_Result__c = res.Id;
            enu1.End_Date__c = date.today() - 1;
            enu2.Report_for_AVETMISS__c = true;
            enu2.End_Date__c = date.today() - 1;
            enu2.Unit_Result__c = res.Id;
            enu1.Training_Delivery_Location__c = loc.id;
            enu2.Training_Delivery_Location__c = loc2.id;
            enu1.Scheduled_Hours__c = 0.0;
    
            update enu1;
            update enu2;
        
            stu.Reporting_Billing_State__c = sta.Id;
            
            stu.Address_building_property_name__c = 'property';
            
            stu.Year_Highest_Education_Completed__c = '@@@@';
            
            update stu;        
                    
            List<String> pconCode = new List<String>{pcon.Contract_Code__c};
    
            List<String> pconId = new List<String>{pcon.Id};
            
            database.executeBatch( new NATValidationBatch_Initial(tOrg.Id, state, year, pconCode, pconId, true, false) );
        }
    
	}
    
    @isTest (seeAllData=true) public static void testValWA() {
        
         //string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = '00590000000HR2zAAG' ];
        
        System.runAs(u) {

            
            String state = 'Western Australia';
            String year = '2016';
            
            loc.Delivery_Location_Identifier__c = '232323';
            
            update loc;
                    
            enu1.Report_for_AVETMISS__c = true;
            enu1.Unit_Result__c = res.Id;
            enu1.End_Date__c = date.today() - 1;
            enu2.Report_for_AVETMISS__c = true;
            enu2.End_Date__c = date.today() - 1;
            enu2.Unit_Result__c = res.Id;
            enu1.Training_Delivery_Location__c = loc.id;
            enu2.Training_Delivery_Location__c = loc2.id;
            enu1.Scheduled_Hours__c = 0.0;
    
            update enu1;
            update enu2;
        
            stu.Reporting_Billing_State__c = sta.Id;
            
            stu.Address_building_property_name__c = 'property';
            
            stu.Year_Highest_Education_Completed__c = '@@@@';
            
            update stu;        
                    
            List<String> pconCode = new List<String>{pcon.Contract_Code__c};
    
            List<String> pconId = new List<String>{pcon.Id};
            
            database.executeBatch( new NATValidationBatch_Initial(tOrg.Id, state, year, pconCode, pconId, true, false) );
        
        }
        
	}
    
    @isTest (seeAllData=true) public static void testValWARAPT() {
        
         //string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = '00590000000HR2zAAG' ];
        
        System.runAs(u) {

                    
            String state = 'Western Australia';
            String year = '2016';
            
            loc.Delivery_Location_Identifier__c = '232323';
            
            update loc;
                    
            enu1.Report_for_AVETMISS__c = true;
            enu1.Unit_Result__c = res.Id;
            enu1.End_Date__c = date.today() - 1;
            enu2.Report_for_AVETMISS__c = true;
            enu2.End_Date__c = date.today() - 1;
            enu2.Unit_Result__c = res.Id;
            enu1.Training_Delivery_Location__c = loc.id;
            enu2.Training_Delivery_Location__c = loc2.id;
            enu1.Scheduled_Hours__c = 0.0;
    
            update enu1;
            update enu2;
        
            stu.Reporting_Billing_State__c = sta.Id;
            
            stu.Address_building_property_name__c = 'property';
            
            stu.Year_Highest_Education_Completed__c = '@@@@';
            
            update stu;        
                    
            List<String> pconCode = new List<String>{pcon.Contract_Code__c};
    
            List<String> pconId = new List<String>{pcon.Id};
            
            database.executeBatch( new NATValidationBatch_Initial(tOrg.Id, state, year, pconCode, pconId, true, true) );

        }
	}
    
       @isTest (seeAllData=true) public static void testValNSW() {
        		
           
        //string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = '00590000000HR2zAAG' ];
        
        System.runAs(u) {
        
            String state = 'New South Wales';
            String year = '2016';
            
            loc.Delivery_Location_Identifier__c = '232323';
            
            update loc;
                    
            enu1.Report_for_AVETMISS__c = true;
            enu1.Unit_Result__c = res.Id;
            enu1.End_Date__c = date.today() - 1;
            enu2.Report_for_AVETMISS__c = true;
            enu2.End_Date__c = date.today() - 1;
            enu2.Unit_Result__c = res.Id;
            enu1.Training_Delivery_Location__c = loc.id;
            enu2.Training_Delivery_Location__c = loc2.id;
            enu1.Scheduled_Hours__c = 0.0;
    
            update enu1;
            update enu2;
        
            stu.Reporting_Billing_State__c = sta.Id;
            
            stu.Address_building_property_name__c = 'property';
            
            stu.Year_Highest_Education_Completed__c = '@@@@';
            
            update stu;        
                    
            List<String> pconCode = new List<String>{pcon.Contract_Code__c};
    
            List<String> pconId = new List<String>{pcon.Id};
                
                List<String> enrlId = new List<String>{enrl.Id};
            
            database.executeBatch( new NATValidationBatch_InitialNSW(tOrg.Id, enrlId, true) );
            
        }
    
	}
}