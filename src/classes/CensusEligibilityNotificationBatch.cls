/*16 days before census, a trainer will be sent a task for non-eligible students (Students without an Eligibility Record for the Census) */

/**
 * @description n/a
 * @author Yuri Gribanov
 * @date n/a
 * @history
 *       07.JAN.2016    Ranyel Maliwanag    - Added `IsEligiblityNotificationSent__c` field to the query criteria
 *       13.JAN.2016    Biao Zhang          - Added Due date(ActivityDate) for the task
 * 		 02.JUN.2016	Ranyel Maliwanag	- Fixed the issue where Task creation fails if the Enrolment's Assigned Trainer is inactive and defaults it to the current running user. Reference case: 00088005
 *		 10.JUN.2016	Ranyel Maliwanag	- Fixed the issue where batch tries to update a deleted Task. Reference case: 00089713
 */
public without sharing class CensusEligibilityNotificationBatch implements Database.Batchable<sObject>,Database.Stateful {
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		Date dt = Date.today().addDays(+16);
		return Database.getQueryLocator('Select Id, EnrolmentId__c, EnrolmentId__r.Student__r.PersonContactId, EnrolmentId__r.Assigned_Trainer__c, EnrolmentId__r.Assigned_Trainer__r.IsActive, CensusTaskId__c, Stage__c, CensusDate__c FROM Census__c WHERE EligibleforCensus__c = false AND CensusDate__c <= : dt AND CensusDate__c >= TODAY AND EnrolmentId__r.Enrolment_Status__c LIKE \'Active%\'');
	}


	public void execute(Database.BatchableContext BC,List<SObject> scope) {
		Database.DMLOptions dm = new Database.DMLOptions();
		dm.EmailHeader.triggerUserEmail = true;

		Map<Census__c, Task> censusTaskMap = new Map<Census__c, Task>();

		// Build a list of all Task Ids
		Set<Id> allTaskIds = new Set<Id>();
		for (SObject item : scope) {
			Census__c record = (Census__c) item;
			allTaskIds.add(record.CensusTaskId__c);
		}

		// Build a list of filtered non-deleted Task Ids
		Set<Id> taskIds = new Set<Id>();
		for (Task record : [SELECT Id FROM Task WHERE Id IN :allTaskIds]) {
			taskIds.add(record.Id);
		}

		
		for (SObject s : scope) {
			Census__c cen = (Census__c) s;

			String censusNumber = String.isNotBlank(cen.Stage__c) ? 'C' + cen.Stage__c.right(1) : '';
			Integer daysBetween = Date.today().daysBetween(cen.CensusDate__c);
			String subject = 'Approaching ' + censusNumber + ' ' + daysBetween + ' days until ' + censusNumber;
			String description = 'Student is approaching Census and has not Commenced an Introduction or a Lesson. This student will be suspended if an activity is not recorded by Census date.';

			if (daysBetween <= 4) {
				String days = daysBetween == 1 ? ' day ' : ' days ';
				subject = 'Urgent ' + daysBetween + days + 'until ' + censusNumber;
				description = 'Student is approaching Census and has not Commenced an Introduction or a Lesson for this Census period. Due to this the student will be suspended if no activity has been recorded at the Census date. Please take necessary actions to confirm the student is still planning on studying.';
			} else if (daysBetween <= 10) {
				subject = 'Important ' + daysBetween + ' days until ' + censusNumber;
				description = 'Student is approaching Census and has not Commenced an Introduction or a Lesson. This student will be suspended if an activity is not recorded by Census date. Please contact student to establish the status.';
			}

			Task t = new Task(
				Id = taskIds.contains(cen.CensusTaskId__c) ? cen.CensusTaskId__c : null,
				RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
				WhatId = cen.EnrolmentId__c,
				WhoId = cen.EnrolmentId__r.Student__r.PersonContactId,
				OwnerId = String.isNotBlank(cen.EnrolmentId__r.Assigned_Trainer__c) && cen.EnrolmentId__r.Assigned_Trainer__r.IsActive ? cen.EnrolmentId__r.Assigned_Trainer__c : UserInfo.getUserId(),
				Subject = subject,
				Status = 'Not Started',
				Priority = 'High',
				Description = description,
				ActivityDate = Date.today()
			);

			censusTaskMap.put(cen, t);
		}

		upsert censusTaskMap.values();
		for (Census__c record : censusTaskMap.keySet()) {
			record.CensusTaskId__c = censusTaskMap.get(record).Id;
		}
		update new List<Census__c>(censusTaskMap.keySet());
	}
	

	public void finish(Database.BatchableContext BC) {
	}
}