/*This test class is used to create all relevant data for
 *blackboard integration, please refer to specific methods
 *for details.
 *Created By Yuri Gribanov - CloudSherpas 04/06/2014
 */

@istest 
public class BlackboardTestDataFactory {

    //Create Country record
    public static Country__c insertCountry(String CountryName, String CountryCode){

        Country__c c = new Country__c(

          Country_Code__c = CountryCode ,
          Name = CountryName

        );
        
        insert c;

        return c;
    }

     public static State__c insertState(Id countryId, String stateName, String stateShortName, String stateCode){

        State__c newState = new State__c(

          Name = stateName,
          Short_Name__c = stateShortName,
          Code__c = stateCode,
          Country__c = countryId




        );
        
        insert newState;

        return newState;
    }

    public static Unit__c insertUnitOfCompetency(String bbcourseId) {

            Id recrdTypeId = [select Id from RecordType where Name = 'Unit of Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c un = new Unit__c(

            Name = 'BSBADM506A',
            Unit_Name__c = 'Manage business document design and development',
            Include_In_CCQI_Export__c = true,
            Blackboard_Master_Course__c = bbcourseId,
            RecordTypeId = recrdTypeId
        );
        
        insert un;

        return un;

    }

     public static Field_of_Education__c insertFieldOfEducation() {

        Id recrdTypeId = [select Id from RecordType where DeveloperName = 'Division' And SObjectType='Field_of_Education__c' LIMIT 1].Id;

        Field_of_Education__c fld = new Field_of_Education__c(

            RecordTypeId = recrdTypeId,
            Name = 'Test Field of Education'

        );

        insert fld;

        return fld;

    }

     
    public static Qualification__c insertQualification(String fldId, String recStatus, Boolean availableForOnlineEnrol, Boolean DoNotAddQualUnits, Boolean isActive, Boolean RequiresLLNAssessmentforEnrolment, Id bbroleId){

        Id recrdTypeId = [select Id from RecordType where DeveloperName = 'Qualification' And SObjectType = 'Qualification__c' LIMIT 1].Id;

        Qualification__c qual = new Qualification__c(

           RecordTypeId = recrdTypeId,
           Name = 'Test Qualification',
           Qualification_Name__c = 'Diploma of Business',
           Available_for_Online_Enrolments__c = availableForOnlineEnrol,
           DoNotAddQualUnits__c = DoNotAddQualUnits,
           Include_in_CCQI_export__c = True,
           Requires_LLN_Assessment_for_Enrolment__c = RequiresLLNAssessmentforEnrolment,
           Active__c = isActive,
           Vet_Non_Vet__c = True,
           VFH_Approved__c = True,
           Field_of_Education__c = fldId,
           Recognition_Status__c = recStatus,
           Blackboard_Institutional_Role__c = bbroleId
        );
             
        insert qual;

        return qual;
    }

    public static Qualification_Unit__c insertQualificationUnit(String qualId, String unitId, String unitType){

        Qualification_Unit__c qu = new Qualification_Unit__c(

            Qualification__c = qualId,
            Unit__c = unitId,
            Unit_Type__c = unitType,
            Don_t_auto_add_this_unit_to_enrolments__c = False,
            Inactive__c = False
        );

        insert qu;

        return qu;
    }

  
   
     public static Intake__c insertIntake(String qualId, String programId, String fundingStreamId, String contextId ){
       
        Intake__c i = new Intake__c(

          Start_Date__c = date.today(),
          End_Date__c = date.today() + 30,
          Qualification__c = qualId,
          Program__c = programId,
          Funding_Stream__c = fundingStreamId,
          Qualification_Context__c = contextId 


        );
             
        insert i;

        return i;
    }

    public static Intake_Unit__c insertIntakeUnit(String intakeId, Date startdate, Date enddate, String unitId, Id bbtemplatecourseId) {

      Intake_Unit__c iu = new Intake_Unit__c(

        Intake__c = intakeId,
        Unit_Start_Date__c = startdate,
        Unit_End_Date__c = enddate,
        Unit_of_Study__c = unitId,
        Blackboard_Template_Course__c = bbtemplatecourseId


 
      );


      insert iu;

      return iu;
    }

    public static Funding_Stream_Types__c insertFundingStream(){

        Funding_Stream_Types__c f = new Funding_Stream_Types__c(

          Name = 'Funding Stream'
        );
        
        insert f;

        return f;
    }
   
    public static Program__c insertProgram(String qualid){

        Program__c p = new Program__c(Qualification__c = qualid

        );


        insert p;

        return p;
    }

    public static Context__c insertContext() {

       Context__c c = new Context__c(

        Context_Full_Name__c = 'Construction(Online)',
        Name = 'Construction(Online)',
        Context_Title__c = 'Construction'


        );

       insert c;

       return c;

    }

    public static Qualification_Context__c insertQualificationContext(String contextId, String qualId) {

      Qualification_Context__c qc = new Qualification_Context__c(

        Context__c = contextId,
        Qualification__c = qualId

        );

      insert qc;

      return qc;


    }

    public static Qualification_Support_Item__c insertQualificationSupportItem(String bbcourseId, String qualId) {

      Qualification_Support_Item__c qsi = new Qualification_Support_Item__c(

        Blackboard_Course_ID__c  = bbcourseId,
        Qualification__c = qualId

        );

      insert qsi;

      return qsi;


    }

    public static VFH_Controller__c insertVFHController(){
        VFH_Controller__c v = new VFH_Controller__c(

           
        );
       
        insert v;

        return v;
    }

    //Create t2 and t3, require BB courseReference
    public static Blackboard_Course__c insertBlackboardCourse(String bbcmastereference,String bbctemplatereference, String unitcode, String RecTypId, Boolean isAvailable, Boolean changeSinceLastSync, String contextId, String intakeunitId, Boolean sysAttempted, Boolean StudentAccessApproved, String bbkey) {

     Id recTypeId = [Select Id From RecordType Where Id =: RecTypId].Id;

      Blackboard_Course__c bbcourse = new Blackboard_Course__c(

          RecordTypeId = recTypeId,
          Blackboard_Course_Name__c = 'BSBADM502B Manage meetings (MASTER)',
          Available_in_Blackboard__c = isAvailable,
          Changed_Since_Last_Sync__c = changeSinceLastSync,
          Context__c = contextId,
          Intake_Unit__c = intakeunitId,
          Master_Course__c = bbcmastereference,
          SIS_Sync_Attempted__c = sysAttempted,
          Student_Access_Approved__c = StudentAccessApproved,
          Unit_Name__c = 'Manage Meetings',
          Unit_Code__c = unitcode,
          Template_Course__c = bbctemplatereference,
          Blackboard_Primary_Key__c = bbkey
         

      );

      insert bbcourse;

      return bbcourse;

    }
    //create t1, t4,t5 No bbcourse reference required
    public static Blackboard_Course__c insertBlackboardCourse(String RecTypId, String unitcode, Boolean isAvailable, Boolean changeSinceLastSync, String contextId, String intakeunitId, Boolean sysAttempted, Boolean StudentAccessApproved, String bbkey) {

      Id recTypeId = [Select Id From RecordType Where Id =: RecTypId].Id;
      
       Blackboard_Course__c bbcourse = new Blackboard_Course__c( 

          RecordTypeId = recTypeId,
          Blackboard_Course_Name__c = 'BSBADM502B Manage meetings (MASTER)',
          Available_in_Blackboard__c = isAvailable,
          Changed_Since_Last_Sync__c = changeSinceLastSync,
          Context__c = contextId,
          Intake_Unit__c = intakeunitId,
          SIS_Sync_Attempted__c = sysAttempted,
          Student_Access_Approved__c = StudentAccessApproved,
          Unit_Code__c = unitcode,
          Unit_Name__c = 'Manage Meetings',
          Blackboard_Primary_Key__c = bbkey
          
         
         

      );

       insert bbcourse;

       return bbcourse;

    }


    //Create Enrolment Record
    public static Enrolment__c insertEnrolment(Id studentId,Id qualId, Id countryId){

         Enrolment__c enrolment = new Enrolment__c(
         Student__c = studentId,
         Qualification__c = qualId,
         //Line_Item_Qualification__c = liqId,
         Enrolment_Status__c = 'Completed',
         Start_Date__c = date.today()- 100, 
         End_Date__c = date.today(),
         //Delivery_Location__c = locId,
         Overseas_Country__c = countryId,
         Study_Reason__c = '01 - To get a job',
         Blackboard_Required__c = true

        );


        insert enrolment;

        return enrolment;
        
    }

    //Create Enrolment Unit
    public static Enrolment_Unit__c insertEnrolmentUnit(Id unitId, Id enrolId, Id resultId, Date sdate, Date edate, Id lineItemId){

        Enrolment_Unit__c enrolment_unit = new Enrolment_Unit__c(


                Unit__c = unitId,
                Enrolment__c = enrolId,
                Start_Date__c = sdate,
                End_Date__c = edate,
                Unit_Result__c = resultId,
                Line_Item_Qualification_Unit__c = lineItemId
               
        );

          insert enrolment_unit;

          return enrolment_unit;
    }
        public static Enrolment_Unit__c insertEnrolmentUnit2(Id unitId, Id enrolId, Date sdate, Date edate, Id lineItemId){

        Enrolment_Unit__c enrolment_unit = new Enrolment_Unit__c(


                Unit__c = unitId,
                Enrolment__c = enrolId,
                Start_Date__c = sdate,
                End_Date__c = edate,
                Unit_Result__c = null,
                Line_Item_Qualification_Unit__c = lineItemId
               
        );

          insert enrolment_unit;

          return enrolment_unit;
    }

     //create Account Record (Student)
    public static Account insertStudent(String fname, String lname){
        Id recrdTypeId = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 

        Account student = new Account(
              RecordTypeId = recrdTypeId,
              LastName = lname,
              FirstName = fname,
              PersonMailingCountry = 'AUSTRALIA',
              PersonMailingPostalCode = '4005',
              PersonMailingState = 'Queensland',
              PersonMobilePhone = '61847292111',
              Student_Identifer__c = 'H89373',
              PersonBirthDate = system.today() - 5000,
              Year_Highest_Education_Completed__c = '2000',
              Proficiency_in_Spoken_English__c = '1 - Very well',
              Sex__c = 'F - Female',
              Salutation = 'Mr.',
              Learner_Unique_Identifier__c = '12ab34cd56'

        );

        insert student;

        return student;
    }

    public static Blackboard_User__c insertBlackboardUser(Id AccountId, String bbprimarykey, String bbusername) {

       Blackboard_User__c bbuser = new Blackboard_User__c(

          Account__c = AccountId,
          Blackboard_Primary_Key__c = bbprimarykey,
          Blackboard_Username__c = bbusername


      );

       insert bbuser;

       return bbuser;
    }

    //create Line Item Qualification record
    public static Line_Item_Qualifications__c insertLineItemQualification(Id qualId,Id contractLineItemId){
        Line_Item_Qualifications__c  line_item= new Line_Item_Qualifications__c(

          Qualification__c = qualId,
          Training_Catalogue_Item_No__c = '0001',
          Contract_Line_Item__c = contractLineItemId


        );
        
        insert line_item;

        return line_item;
    }

    
    public static Contract_Line_Items__c insertContractLineItem(Id contractId){
        Contract_Line_Items__c c = new Contract_Line_Items__c(

          Line_Item_Number__c = '001',
          Purchasing_Contract__c = contractId

        );
        
        insert c;

        return c;
    } 

   
    public static Line_Item_Qualification_Units__c insertLineItemQualificationUnit(Id lineItemId, Id qualUnitId){
        
        Line_Item_Qualification_Units__c  lineItemQualUnit = new Line_Item_Qualification_Units__c(

          Qualification_Unit__c = qualUnitId,
          Line_Item_Qualification__c = lineItemId
        );
       
        insert lineItemQualUnit;

        return lineItemQualUnit;
    }

     public static Purchasing_Contracts__c insertPurchasingContract(){
      //  Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;

        Purchasing_Contracts__c purchasing_contract = new Purchasing_Contracts__c(

          Contract_Name__c = 'test Contract',
          Contract_Type__c = 'Apprenticeship Funding',
          Contract_Code__c = '1234567890'

        );
       
      
        insert purchasing_contract;

        return purchasing_contract;
    }

    public static Completion_Status_Type__c insertCompletionStatusType(String nname, String code) {

      Completion_Status_Type__c completion_statustype = new Completion_Status_Type__c(

        name = nname,
        Code__c = code,
        Coding_Notes__c = 'test note'

      );

      insert completion_statustype;

      return completion_statustype;
    }

    public static Intake_Students__c insertIntakeStudent(Id IntakeId, Id EnrolmentId) {

        Intake_Students__c intake_student = new Intake_Students__c(

            Intake__c = IntakeId,
            Enrolment__c = EnrolmentId

        );

        insert intake_student;

        return intake_student;


    }

    public static Blackboard_Course_Membership__c insertBlackboardCourseMembership(Id recordtypeId, Id courseId, String pkey, Id bburId, Id euId, Id isId, Id iuId, Id iuurId, Id eiuId, Boolean availableinBB) {

      Blackboard_Course_Membership__c bbcm = new Blackboard_Course_Membership__c(

          RecordtypeId = recordtypeId,
          Blackboard_Course__c = courseId,
          Blackboard_Primary_Key__c = pkey,
          Blackboard_User_Role__c = bburId,
          Enrolment_Unit__c =  euId,
          Intake_Student__c = isId,
          Intake_Unit__c = iuId,
          Intake_Unit_User_Role__c = iuurId,
          Enrolment_Intake_Unit__c = eiuId,
          Available_in_Blackboard__c = availableinBB



      );

      insert bbcm;

      return bbcm;

    }

    public static Blackboard_User_Role__c insertBlackboardUserRole(Id bbuserId, Id bbroleId, Id recTypeId) {

      Blackboard_User_Role__c bbuserrole = new Blackboard_User_Role__c(

          Blackboard_User__c = bbuserId,
          Blackboard_Role__c = bbroleId,
          RecordTypeId = recTypeId


      );

      insert bbuserrole;

      return bbuserrole;

    }

    public static Blackboard_Role__c insertBlackboardRole(Id recTypeId) {

      Blackboard_Role__c bbrole = new Blackboard_Role__c (

        Blackboard_Role_Name__c = 'Blackboard Role',
        Role_Id__c = 'Role Id',
        RecordtypeId = recTypeId



      );

      insert bbrole;

      return bbrole;

    }

    public static Intake_Unit_User_Role__c insertIntakeUnitUserRole(Id burId, Id iuId) {

      Intake_Unit_User_Role__c iuur = new Intake_Unit_User_Role__c(

        Blackboard_User_Role__c = burId,
        Intake_Unit__c = iuId


      );

      insert iuur;

      return iuur;


    }

    public static Enrolment_Intake_Unit__c insertEnrolmentIntakeUnit(Id euId, Id Eid, Id iuId) {

      Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c(

        Enrolment__c = Eid,
        Enrolment_Unit__c = euId,
        Intake_Unit__c = iuId


        );

      insert eiu;

      return eiu;
    }
    
    public static Results__c insertResult(Id recTypeId, Id stateId, String resCode, Id natOutcomeId) {
        
        Results__c result = new Results__c(
        
            RecordTypeId = recTypeId,
            Name = 'Result Name',
            State__c = stateId,
            Result_Code__c = resCode,
            National_Outcome__c = natOutcomeId
        
        );      
        insert result;      
        return result;
    }



    public static void createCustomSettings() {

      Blackboard_Configuration__c bb_sandboxCS = new Blackboard_Configuration__c(Name= 'WS_Config Sandbox',
        is_Active__c = true,
        Context_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Context.WS',
        Course_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Course.WS',
        expectedLifeSeconds__c = 86400,
        Password__c = 'scottga',
        SIS_Batch_Size__c = 200,
        SIS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com/webapps/bb-data-integration-ss-xml-BBLEARN/endpoint',
        SIS_End_Point_Password__c = 'test',
        SIS_End_Point_Username__c = '0df63dd9-959b-4e79-acde-5f3894f56f13',
        UserId__c = 'scottga',
        User_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/User.WS',
        Course_Membership_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/CourseMembership.WS'
      );


      insert bb_sandboxCS;
      
      
      Blackboard_Configuration__c bb_prodCS = new Blackboard_Configuration__c(Name= 'WS_Config Production',
        is_Active__c = true,
        Context_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Context.WS',
        Course_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Course.WS',
        expectedLifeSeconds__c = 86400,
        Password__c = 'scottga',
        SIS_Batch_Size__c = 200,
        SIS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com/webapps/bb-data-integration-ss-xml-BBLEARN/endpoint',
        SIS_End_Point_Password__c = 'test',
        SIS_End_Point_Username__c = '0df63dd9-959b-4e79-acde-5f3894f56f13',
        UserId__c = 'scottga',
        User_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/User.WS',
        Course_Membership_WS_Endpoint__c = 'https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/CourseMembership.WS'
      );


      insert bb_prodCS;
    }
    
}