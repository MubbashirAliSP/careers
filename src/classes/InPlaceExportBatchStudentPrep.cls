/**
 * @description Batch class for preparing 
 * @author Ranyel Maliwanag
 * @date 22.JUN.2015
 * @history
 *       28.SEP.2015    Ranyel Maliwanag    Changed evaluation of enrolment qualification to base on the qualification's Discipline instead of just the Qualification name. This is to accommodate qualifications that are named differently / has typos
 */
public without sharing class InPlaceExportBatchStudentPrep implements Database.Batchable<SObject> {
    public String queryString;
    public Set<String> vocationalPlacementPrefixes = new Set<String>{
        'VP%',
        'VOC%'
    };
    public Date dateToday = Date.today();
    
    public InPlaceExportBatchStudentPrep() {
        queryString = 'SELECT Id, Qualification_Name__c, Qualification__r.InPlace_Discipline__r.IPD_Description__c, Campus__r.Name, ';
        queryString += '(SELECT Id, Enrolment__r.Parent_Qualification_Code__c, Student_Identifier__c, Unit_Code__c, Unit_Name__c, Intake_Name__c, Unit_Training_Start_Date__c, IPSUO_InPlace_Student_Unit_Offering__c FROM Enrolment_Intake_Units__r WHERE Unit_Code__c LIKE :vocationalPlacementPrefixes AND Unit_Training_End_Date__c >= :dateToday) ';
        queryString += 'FROM Enrolment__c ';
        queryString += 'WHERE Enrolment_Status__c LIKE \'Active%\' AND Qualification__r.IPC_InPlace_Course__c = true ';
        //queryString += 'AND Id = \'a1K90000009bD7y\'';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 22.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 22.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Enrolment__c> records) {
        List<Enrolment__c> recordsForUpdate = new List<Enrolment__c>();
        // Mapping of <Enrolment Intake Unit Id> : <Class Enrolment List>
        Map<Id, List<Class_Enrolment__c>> enrolmentClassesMap = new Map<Id, List<Class_Enrolment__c>>();

        // Get list of Enrolment Intake Unit Ids
        Set<Id> enrolmentIntakeUnitIds = new Set<Id>();
        for (Enrolment__c record : records) {
            System.debug('scheduled units size ::: ' + record.Enrolment_Intake_Units__r.size());
            for (Enrolment_Intake_Unit__c scheduledUnit : record.Enrolment_Intake_Units__r) {
                enrolmentClassesMap.put(scheduledUnit.Id, new List<Class_Enrolment__c>());
            }
        }
        enrolmentIntakeUnitIds = enrolmentClassesMap.keySet();
        System.debug('enrolmentIntakeUnitIds ::: ' + enrolmentIntakeUnitIds);

        // Fetch Class enrolments
        List<Class_Enrolment__c> classEnrolments = [SELECT Id, Class_Start_Date_Time__c, Unit_Code__c, Enrolment_Intake_Unit__c FROM Class_Enrolment__c WHERE Unit_Code__c LIKE :vocationalPlacementPrefixes AND Class_Start_Date_Time__c >= :dateToday AND Enrolment_Intake_Unit__c IN :enrolmentIntakeUnitIds];
        System.debug('Class enrolments ::: ' + classEnrolments);

        // Build map
        for (Class_Enrolment__c classEnrolment : classEnrolments) {
            enrolmentClassesMap.get(classEnrolment.Enrolment_Intake_Unit__c).add(classEnrolment);
        }

        System.debug('enrolment classes map ::: ' + enrolmentClassesMap);


        for (Enrolment__c record : records) {
            Boolean hasScheduledUnits = record.Enrolment_Intake_Units__r != null && record.Enrolment_Intake_Units__r.size() > 0;
            if (hasScheduledUnits && record.Qualification__r.InPlace_Discipline__r.IPD_Description__c == 'Nursing') {
                // Condition: Enrolment is for Diploma of Nursing
                // Go over scheduled units and see if there is a placement starting in 10 weeks' time
                for (Enrolment_Intake_Unit__c scheduledUnit : record.Enrolment_Intake_Units__r) {
                    Boolean matchFound = false;
                    for (Class_Enrolment__c scheduledClass : enrolmentClassesMap.get(scheduledUnit.Id)) {
                        if (scheduledClass.Class_Start_Date_Time__c != null && scheduledClass.Class_Start_Date_Time__c <= Date.today().addDays(70)) {
                            record.IPS_InPlace_Student__c = true;
                            record.IPS_Sync_on_next_update__c = true;
                            matchFound = true;
                            recordsForUpdate.add(record);
                            break;
                        }
                    }
                    if (matchFound) {
                        break;
                    }
                }
            } else if (hasScheduledUnits && record.Qualification__r.InPlace_Discipline__r.IPD_Description__c == 'Health Care') {
                // Condition: Enrolment is for Aged Care
                // Go over scheduled units and see if there is a placement starting in 3 weeks' time
                for (Enrolment_Intake_Unit__c scheduledUnit : record.Enrolment_Intake_Units__r) {
                    Boolean matchFound = false;
                    for (Class_Enrolment__c scheduledClass : enrolmentClassesMap.get(scheduledUnit.Id)) {
                        if (scheduledClass.Class_Start_Date_Time__c != null && scheduledClass.Class_Start_Date_Time__c <= Date.today().addDays(42)) {
                            record.IPS_InPlace_Student__c = true;
                            record.IPS_Sync_on_next_update__c = true;
                            matchFound = true;
                            recordsForUpdate.add(record);
                            break;
                        }
                    }
                    if (matchFound) {
                        break;
                    }
                }
            }
        }

        update recordsForUpdate;
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 22.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchStudent nextBatch = new InPlaceExportBatchStudent();
        Database.executeBatch(nextBatch);
    }
}