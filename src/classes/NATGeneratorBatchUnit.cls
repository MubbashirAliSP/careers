/**
 * @description Batch Class for NAT00060 and NAT00130
 * @author 
 * @date 30.JUL.2013
 *
 * HISTORY
 * -30.JUL.2013    Created.
 * -25.SEP.2013    Michelle Magsarili restructure code to perform multiple batch process for different files to prevent duplicate processing
 * -24.OCT.2013    Michelle Maagarili resturcture code to avoid Apex Heaps Limits
 */
global class NATGeneratorBatchUnit implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String functionquery;
    global final String query;
    private String currentState;
    public String libraryId; 
    global String body60;
    global String body130;
    global String NATFile; 
    public Map<Id,Decimal>Unit_NominalHoursMap;
    
    global NATGeneratorBatchUnit(String q, String state) {
        query = q;
        
        currentState = state;
        
        if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        
        
        /*We will delete all files in content folder for the state */
        
        //List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
        
        
    }
    
    global NATGeneratorBatchUnit(String NATFileName, String q, String state) {
        //query = q;
        functionquery = q;
        currentState = state;
        NATFile = NATFileName.toUpperCase();
        
        if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        
        
        /*We will delete all files in content folder for the state */
        
        //List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
        
        Set<Id>unitIds = new Set<Id>();
        Set<Id>enrolmentIds = new Set<Id>();
        List<String>splitter = new List<String>();
        Unit_NominalHoursMap = new Map<Id,Decimal>();
        
        if(NATFile.equals('NAT00060')){
            List<sObject> EUList = Database.query(q);
            if(!EUList.isEmpty()){
                for(SObject s :EUList) {
                     
                     Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                       
                       //unitIds.add(eu.Unit__c);
                       
                       enrolmentIds.add(eu.Enrolment__c);
                       
                       List<Id> unitName = new List<Id>();
                      
                      if (state == 'Victoria') {
	                    for ( integer i = 1 ; i <= eu.Supersession_Count__c ; i++ ) {
	             		    String supersededUnitId = 'Superseded_' + i + '_Unit_SFID__c';  
	             		   		unitName.add(String.valueOf(eu.get(supersededUnitId)));
	                      	
                  		}
	                       
                       if(unitName.size() > 0) {
	                       		
                       		List<Unit__c> supersededUnits = [SELECT Id, Name FROM Unit__c WHERE Id IN :unitName];
                   			for (Unit__c u : supersededUnits) {
	                   				Unit_NominalHoursMap.put(u.Id, 0.0);
                   					}
                       		}
                      }
                       
                       
                       if (eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null) {
                       			Unit_NominalHoursMap.put(eu.Unit__c, 0.0);
                       }
                       
                       
                       else {
                       			Unit_NominalHoursMap.put(eu.Unit__c, eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c);
                       }                    
                
                }
                
            }
            
            query = 'SELECT Id, Name, Unit_Name__c, Field_of_Education_Identifier__c, ' + 
                    'Vet_Non_Vet__c, Unit_Flag__c '+
                    'FROM Unit__c ' +
                    'LIMIT 65000';
        }
        
        else{
            query = functionquery;
        }
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
       body60 = '';
       body130 = '';   
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
             
        if(NATFile.equals('NAT00060')){
            for (SObject s :scope) {
                    Unit__c un = (Unit__c) s;
                    
                    if(Unit_NominalHoursMap.containsKey(un.Id)){
                        body60 += NATGeneratorUtility.formatOrPadTxtFile('C', NAT00060__c.getInstance('01').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Name, NAT00060__c.getInstance('02').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Unit_Name__c, NAT00060__c.getInstance('03').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Field_of_Education_Identifier__c, NAT00060__c.getInstance('04').Length__c,'');
                         
                        if (un.Vet_Non_Vet__c) { 
                            body60 += NATGeneratorUtility.formatOrPadTxtFile('Y', NAT00060__c.getInstance('05').Length__c,'');
                        } else {
                            body60 += NATGeneratorUtility.formatOrPadTxtFile('N', NAT00060__c.getInstance('05').Length__c,'');
                        }
                       
                        body60 +=NATGeneratorUtility.formatHours(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), NAT00060__c.getInstance('06').Length__c).substringBefore('.') + '\r\n';
                   }
            }
        }
        else{
            //NAT00130
            Set<Id>enrolmentIds = new Set<Id>();
            Set<Id>awardQuals = new Set<Id>();
            
            for (SObject s :scope) {
                Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                enrolmentIds.add(eu.Enrolment__c);
                /*if(eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null)
                    Unit_NominalHoursMap.put(eu.Unit__c, 0.0);
                else
                    Unit_NominalHoursMap.put(eu.Unit__c, eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c);*/
            }
            
            
            //Do not output if no awards issued
            for(Awards__c award : [Select Enrolment__c, Year_Program_Completed__c,Qualification_Issued__c From Awards__c WHERE RecordType.DeveloperName = 'approved_award' AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' And Enrolment__c in : enrolmentIds ]){
                   awardQuals.add(award.Enrolment__c);
            }
            
            if (currentstate == 'Victoria') { // query for victoria, gets all enrolments regardless of if an award has been issued
            	
            	// if(!awardQuals.IsEmpty()) {
                
                	for(Enrolment__c enrl: [SELECT id, 
                                               NAT_Error_Code__c, 
                                               Training_Organisation_Identifier__c,
                                               Parent_Qualification_Code__c, 
                                               Student_Identifier__c, Start_Date__c,
                                               Victorian_Commencement_Date__c,
                                               Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c,

                                               (SELECT 
                                                Year_Program_Completed__c,
                                                Qualification_Issued__c 

                                                FROM Awards__r WHERE RecordType.Name = 'approved award' AND 
                                                                     Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' LIMIT 1)

                                            FROM Enrolment__c 
                                            WHERE id in : enrolmentIds]){
                                                
                   	body130 += NATGeneratorUtility.formatHours(String.valueOf(enrl.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c), NAT00130__c.getInstance('01').Length__c) +
					NATGeneratorUtility.formatOrPadTxtFile(enrl.Parent_Qualification_Code__c, NAT00130__c.getInstance('02').Length__c,'') +
                    NATGeneratorUtility.formatOrPadTxtFile(enrl.Student_Identifier__c, NAT00130__c.getInstance('03').Length__c,'');
                    
                    if(enrl.awards__r.size() == 0) { // if enrolment has no award, output @@@@ for year completed and N for qualification issued in NAT130 file
                    	
                    	 body130 += '@@@@';
                         body130 += 'N';
                    	
                    }
                    
                    else { // otherwise, outout data from award record, year completed and qualification issues
	                    for(Awards__c award : enrl.awards__r ) {
	                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Year_Program_Completed__c, NAT00130__c.getInstance('04').Length__c,'');
	                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Qualification_Issued__c, NAT00130__c.getInstance('05').Length__c,'');
	                        //Add Qualification Issued Flag
	                    }
                    }

                    body130 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(enrl.Victorian_Commencement_Date__c)), NAT00130__c.getInstance('06').Length__c,'');

                    body130 += '\r\n';
            
                //}
            }
            	
            	
            }
           
            if (currentstate != 'Victoria') { // seperate query for rest of aus, gets only enrolments with an award
            	
	            if(!awardQuals.IsEmpty()){
	                for(Enrolment__c enrl: [SELECT id, 
	                                               NAT_Error_Code__c, 
	                                               Training_Organisation_Identifier__c,
	                                               Parent_Qualification_Code__c, 
	                                               Student_Identifier__c, Start_Date__c,
	                                               Victorian_Commencement_Date__c,
	
	                                               (Select 
	                                                Year_Program_Completed__c,
	                                                Qualification_Issued__c 
	
	                                                From Awards__r WHERE RecordType.Name = 'approved award' AND 
	                                                                     Award_Type__c = 'Certificate' AND 
	                                                                     Qualification_Issued__c = 'Y' LIMIT 1)
	
	                                            FROM Enrolment__c 
	                                            WHERE id in : awardQuals]){
	                            
	                    body130 += NATGeneratorUtility.formatOrPadTxtFile(enrl.Training_Organisation_Identifier__c, NAT00130__c.getInstance('01').Length__c,'')+
	                    NATGeneratorUtility.formatOrPadTxtFile(enrl.Parent_Qualification_Code__c, NAT00130__c.getInstance('02').Length__c,'')+
	                    NATGeneratorUtility.formatOrPadTxtFile(enrl.Student_Identifier__c, NAT00130__c.getInstance('03').Length__c,'');
	                    
	                    for(Awards__c award : enrl.awards__r ) {
	                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Year_Program_Completed__c, NAT00130__c.getInstance('04').Length__c,'');
	                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Qualification_Issued__c, NAT00130__c.getInstance('05').Length__c,'');
	                        //Add Qualification Issued Flag
	                    }
	
	                    body130 += '\r\n';
	            
	                	}
	            	}
	            }
	        }
        
    }

    global void finish(Database.BatchableContext BC) {
        
        if(NATFile.equals('NAT00060')){
             Blob pBlob60 = Blob.valueof(body60);
                insert new ContentVersion(
                    versionData =  pBlob60,
                    Title = 'NAT00060',
                    PathOnClient = '/NAT00060.txt',
                    FirstPublishLocationId = libraryId); 
                    Database.executeBatch(new NATGeneratorBatchUnit('NAT00130',functionquery,currentstate)); //NAT00130
         }
         else { //NAT00130 
            
            //MAM 10/25/2013 Remove Duplicates for NAT 130 start 
            String fBody130 = NATGeneratorUtility.RemoveDuplicates(body130);
            if(fBody130 == '')
                fBody130 = ' ';
            
            Blob pBlob130 = Blob.valueof(fBody130);
            insert new ContentVersion(
                versionData =  pBlob130,
                Title = 'NAT00130',
                PathOnClient = '/NAT00130.txt',
                FirstPublishLocationId = libraryId); 
            //MAM 10/25/2013 end
         }
        
    }
}