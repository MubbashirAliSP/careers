/**
 * @description Teset class for `ApplicationFormItemAnswerTriggerHandler`
 * @author Ranyel Maliwanag
 * @date 28.AUG.2015
 */
@isTest
public with sharing class AppFormItemAnswerTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {
        ApplicationsTestUtilities.initialiseEnrolmentApplication();
    }

    static testMethod void testCase() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE QuestionType__c = 'Encrypted'];

        Test.startTest();
            ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                    ApplicationFormId__c = form.Id,
                    ApplicationFormItemId__c = formItem.Id,
                    Answer__c = 'Hello'
                );
            insert answer;

            update answer;
        Test.stopTest();
    }

    static testMethod void testCase2() {
        ApplicationForm__c form = [SELECT Id FROM ApplicationForm__c];
        ApplicationFormItem__c formItem = [SELECT Id FROM ApplicationFormItem__c WHERE Name = 'SupplyingYear12Cert'];

        Test.startTest();
            ApplicationFormItemAnswer__c answer = new ApplicationFormItemAnswer__c(
                    ApplicationFormId__c = form.Id,
                    ApplicationFormItemId__c = formItem.Id,
                    Answer__c = 'Yes'
                );
            insert answer;
        Test.stopTest();
    }
}