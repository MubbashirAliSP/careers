/**
 * @description Controller for Mark Class Roll page
 * @author Janella Lauren Canlas
 * @date 13.NOV.2012
 *
 * HISTORY
 * - 13.NOV.2012    Janella Canlas - Created.
 * - 18.AUG.2015    Miguel de Guia - added International_Student__c
 */
public with sharing class MarkClassRoll_CC {
    //instantiate variables
    public List<ClassRollWrapper> lstEnroment {get;set;}
    public Id classId;
    public String InternationalStudent {get;set;}
    
    public MarkClassRoll_CC(){
        //retrieve Id of current class record
        classId = ApexPages.currentPage().getParameters().get('classId');
        system.debug('classID'+classId);
        fillHelperClass();
    }
    //retrieve the class record using the Id retrieved
    public Classes__c getClassRecord(){
        Classes__c cls = new Classes__c();
        cls = [SELECT Id, Unit_Name__c, Unit_Code__c, Intake_Unit__r.Name, Start_Date_Time__c, End_Date_Time__c 
                FROM Classes__c WHERE Id =: classId];
        return cls;
    }
    //retrieve related Class Staff to the Class record
    public List<Class_Staff_Member__c> getClassStaff(){
        List<Class_Staff_Member__c> cls = new List<Class_Staff_Member__c>();
        cls = [SELECT Id, Class__c, Staff_Member_Name__c 
                FROM Class_Staff_Member__c WHERE Class__c =: classId];
        return cls;
    }
    
    public class ClassRollWrapper{
        public Class_Enrolment__c enrolment {get;set;}
        //public Boolean am {get;set;}
        //public Boolean pm {get;set;}
    
        public ClassRollWrapper(Class_Enrolment__c passEnrolment){
            enrolment = passEnrolment;
            //am = false;
            //pm = false;
        }
    }
    //this will display the Attendance Type object records as Picklist values
    public List<SelectOption> getAttendancePicklist() {
        List<SelectOption> options = new List<SelectOption>();                       
        options.add(new SelectOption('', ''));   
        for (Class_Attendance_Type__c iu : [SELECT Id, Name, Code__c from Class_Attendance_Type__c]) {
            options.add(new SelectOption(iu.Id, iu.Name));
        }  
        return options;
    }
    //this is used to list down the Class Enrolment records
    public void fillHelperClass(){
        lstEnroment = new List<ClassRollWrapper>();
        List<Class_Enrolment__c> enrolList = new List<Class_Enrolment__c>();
        enrolList = [SELECT Id, Name, Student_Identifier__c, Attendance_Type__c, Attendance_Code__c, Student_Name__c, International_Student__c, ClassRollNote__c FROM Class_Enrolment__c WHERE Class__c =: classId ORDER BY Student_Name__c ASC]; // added Enrolment__r.Student__r.International_Student__c JMDG 18 AUG 2015
        system.debug('enrolList'+enrolList);
        for(Class_Enrolment__c e: enrolList){
            ClassRollWrapper crw = new ClassRollWrapper(e);
            lstEnroment.add(crw);
        }
        system.debug('lstEnroment'+lstEnroment); 
    }
    //this will update the enrolment records and redirect to the Class record
    public PageReference updateEnrolment(){
        List<Class_Enrolment__c> classList = new List<Class_Enrolment__c>();
        for(ClassRollWrapper w : lstEnroment){          
                classList.add(w.enrolment);
        }
        try{
            update classList;
            PageReference pref = new PageReference('/'+classId);
            pref.setRedirect(true);
            return pref;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,e.getMessage()));return null;
        }   
    }
    //this will go back to the class record
    public PageReference cancel(){
        PageReference p = new PageReference('/'+classId);
        p.setredirect(true);
        return p;
    }
    
}