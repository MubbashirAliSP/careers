public class NATDataDefinitionQLD {
   
       
    public static final String TrainingOrganisationIdentifier = 'Enrolment__r.Training_Organisation_Identifier__c';
    public static final String ParentQualificationCode = 'Enrolment__r.Parent_Qualification_Code__c';
    public static final String StudentIdentifier = 'Enrolment__r.Student_Identifier__c';
    public static final String YearProgramCompleted = 'Year_Program_Completed__c';
    public static final String IssuedFlag = 'Qualification_Issued__c';
    public static final String ReportableQualificationHours = 'QLD_Reportable_Qualification_Hours__c';
    public static final String LearnerUniqueIdentifier = 'Learner_Unique_Identifier__c';
    public static final String FullTimeLearning = 'Full_Time_Learning__c';
    
    
    public static Map< Integer, NATDataElement > nat00010() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00010();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00020() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00020();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00030() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00030();
        
        //replace standard AVETMISS field
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, FALSE, 'number' ) );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00060() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00060();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00080() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00080();
        positions.put(23, new NATDataElement(LearnerUniqueIdentifier, 11, false, 'string' ) );

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00085() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00085();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00090() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00090();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00100() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00100();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00120() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00120();
        positions.put(25, new NATDataElement(FullTimeLearning, 1, false, 'string') );
        return positions;
    }
    
    public static Map<Integer, NATDataElement> nat00130() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(TrainingOrganisationIdentifier, 10, true, 'string') );
        positions.put(2, new NATDataElement(ParentQualificationCode, 10, true, 'string') );
        positions.put(3, new NATDataElement(StudentIdentifier, 10, true, 'string') );
        positions.put(4, new NATDataElement(YearProgramCompleted, 4, false, 'number') );
        positions.put(5, new NATDataElement(IssuedFlag, 1, false, 'string') );
        
        return positions;
    }
        
}