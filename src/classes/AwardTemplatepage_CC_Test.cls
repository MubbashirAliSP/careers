/**
 * @description AwardTemplatepage_CC Test Coverage
 * @author Cletz Cadiz
 * @date 30.JAN.2013
 *
 * HISTORY
 * - 30.JAN.2013    Cletz Cadiz     Created.
 * - 04.MAR.2013    Janella Canlas      Removed seeAllData=true tag
 * - 02.AUG.2013    Michelle Magsarili  Increased Test class to 95%
 */
@isTest
private class AwardTemplatepage_CC_Test {

    static testMethod void testAwardTemp() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
                    
        
        system.runAs(u){
            test.startTest();           
                Awards__c awrd = new Awards__c();
                awrd.Award_Template_ID__c = 'ABC';
                insert awrd;
                
                APXTConga4__Conga_Template__c cTemp = new APXTConga4__Conga_Template__c();
                cTemp.APXTConga4__Name__c = 'Temp Name';
                cTemp.APXTConga4__Template_Type__c = 'Award';
                insert cTemp;
                
                Pagereference testPage = Page.AwardTemplatePage;
                testPage.getParameters().put('awardId', awrd.Id);
                Test.setCurrentPage(testPage);
                
                AwardTemplatePage_CC atpCC = new AwardTemplatePage_CC();
                
                atpCC.getAwardRecord();
                atpCC.getAwardTemplates();
                atpcc.fillPageWrapper();
                
                // get the list of ApexPages.Message
                List<ApexPages.Message> msgList = ApexPages.getMessages();
                // or loop over the messages
                for(ApexPages.Message msg :  ApexPages.getMessages()){
                    System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity()); 
                }
                atpCC.save();
                atpCC.cancel(); 
            test.stopTest();
        }
    }
    
    static testMethod void testAwardTemp2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
                    
        
        system.runAs(u){
            test.startTest();           
                Awards__c awrd = new Awards__c();
                awrd.Award_Template_ID__c = 'ABC';
                insert awrd;
                
                APXTConga4__Conga_Template__c cTemp = new APXTConga4__Conga_Template__c();
                cTemp.APXTConga4__Name__c = 'Temp Name';
                cTemp.APXTConga4__Template_Type__c = 'Award';
                insert cTemp;
                
                //MAM 08/02/2013 start 
                //Insert a 2 more Award and Award Templates where AwardTemplateId__c is filled up
                List<APXTConga4__Conga_Template__c> lstAwardCT = new List<APXTConga4__Conga_Template__c>();
                
                For(integer i=0; i<2; i++){
                    
                    APXTConga4__Conga_Template__c cTemp1 = new APXTConga4__Conga_Template__c();
                    cTemp1.APXTConga4__Name__c = 'Temp Name '  + String.ValueOf(i);
                    cTemp1.APXTConga4__Template_Type__c = 'Award ' + String.ValueOf(i);
                    cTemp1.AwardTemplateId__c = i;
                    lstAwardCT.add(cTemp1);
                    
                }
                
                insert lstAwardCT;
                
                //call page parameters
                /*
                Pagereference testPage = Page.AwardTemplatePage;
                testPage.getParameters().put('awardId', awrd.Id);
                Test.setCurrentPage(testPage);
                */
                
                Test.setCurrentPageReference(new PageReference('Page.AwardTemplatePage')); 
                System.currentPageReference().getParameters().put('awardId', awrd.Id);
                System.currentPageReference().getParameters().put('type', 'award');
                //MAM 08/02/2013 end
                
                AwardTemplatePage_CC atpCC = new AwardTemplatePage_CC();
                atpCC.templateId = '';    //Save Blank to display Error Message
                atpCC.save();
                atpCC.templateId = cTemp.id;
                atpCC.save();
            test.stopTest();
        }
    }
    
    static testMethod void testAwardTemp3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u){
            test.startTest();
                //Create Award Record
                Awards__c awrd = new Awards__c();
                awrd.Award_Template_ID__c = 'ABC';
                insert awrd;
                
                APXTConga4__Conga_Template__c cTemp = new APXTConga4__Conga_Template__c();
                cTemp.APXTConga4__Name__c = 'Temp Name';
                cTemp.APXTConga4__Template_Type__c = 'Award';
                insert cTemp;
                
                //Insert a 2 more Award and Award Templates where AwardTemplateId__c is filled up
                List<APXTConga4__Conga_Template__c> lstAwardCT = new List<APXTConga4__Conga_Template__c>();
                
                For(integer i=0; i<2; i++){
                    
                    APXTConga4__Conga_Template__c cTemp1 = new APXTConga4__Conga_Template__c();
                    cTemp1.APXTConga4__Name__c = 'Temp Name '  + String.ValueOf(i);
                    cTemp1.APXTConga4__Template_Type__c = 'Award';
                    cTemp1.AwardTemplateId__c = i;
                    lstAwardCT.add(cTemp1);
                    
                }
                
                insert lstAwardCT;
                
                //call page parameters
                Test.setCurrentPageReference(new PageReference('Page.AwardTemplatePage')); 
                System.currentPageReference().getParameters().put('awardId', awrd.Id);
                System.currentPageReference().getParameters().put('type', 'award');
                
                AwardTemplatePage_CC atpCC = new AwardTemplatePage_CC();
                atpCC.templateId = lstAwardCT.get(0).id;
                atpCC.save();
            test.stopTest();
        }
    }
    
    static testMethod void testAwardTemp4() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u){
            test.startTest();
                //Create Award - Academic Transcript Record
                Awards__c awrd = new Awards__c();
                awrd.Award_Template_ID__c = 'ABC';
                insert awrd;
                
                APXTConga4__Conga_Template__c cTemp = new APXTConga4__Conga_Template__c();
                cTemp.APXTConga4__Name__c = 'Temp Name';
                cTemp.APXTConga4__Template_Type__c = 'Academic Transcript';
                insert cTemp;
                
                //Insert a 2 more Award and Award Templates where AwardTemplateId__c is filled up
                List<APXTConga4__Conga_Template__c> lstAwardCT = new List<APXTConga4__Conga_Template__c>();
                
                For(integer i=0; i<2; i++){
                    
                    APXTConga4__Conga_Template__c cTemp1 = new APXTConga4__Conga_Template__c();
                    cTemp1.APXTConga4__Name__c = 'Temp Name '  + String.ValueOf(i);
                    cTemp1.APXTConga4__Template_Type__c = 'Award';
                    cTemp1.AwardTemplateId__c = i;
                    lstAwardCT.add(cTemp1);
                    
                }
                
                insert lstAwardCT;
                
                //call page parameters
                Test.setCurrentPageReference(new PageReference('Page.AwardTemplatePage')); 
                System.currentPageReference().getParameters().put('awardId', awrd.Id);
                
                AwardTemplatePage_CC atpCC = new AwardTemplatePage_CC();
                atpCC.templateId = lstAwardCT.get(0).id;
                atpCC.save();
            test.stopTest();
        }
    }
}