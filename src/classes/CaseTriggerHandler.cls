/**
 * @description Central trigger handler for the Case object
 * @author Ranyel Maliwanag
 * @date 04.AUG.2015
 * @history
 *       19.NOV.2015    Biao Zhang          - Update Account complaint type from the value of case type
 *       17.MAR.2016    Ranyel Maliwanag    - Moved and optimised logic from `SetUserField` trigger to populate the `Submitted_By_User__c` field on case insert
 */
public without sharing class CaseTriggerHandler {
    static Map<String, Schema.RecordTypeInfo> caseRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    static Set<Id> sasRecordTypes = new Set<Id>{
        caseRecordTypes.get('Application Issues').getRecordTypeId(),
        caseRecordTypes.get('Microsoft SharePoint').getRecordTypeId(),
        caseRecordTypes.get('Reports/Dashboards').getRecordTypeId(),
        caseRecordTypes.get('Uncategorized Cases').getRecordTypeId(),
        caseRecordTypes.get('User Management').getRecordTypeId()
    };

    public static void onBeforeInsert(List<Case> records) {
        List<Case> withSuppliedEmails = new List<Case>();
        for (Case record : records) {
            if (String.isNotBlank(record.SuppliedEmail)) {
                withSuppliedEmails.add(record);
            }
        }
        setAccountDetails(records);
        populateSubmittedByUserField(withSuppliedEmails);
    }

    public static void onBeforeUpdate(List<Case> records, Map<Id, Case> recordsMap) {
        setAccountDetails(records);

        List<Case> withSuppliedEmails = new List<Case>();
        for (Case record : records) {
            if (String.isNotBlank(record.SuppliedEmail) && record.SuppliedEmail != recordsMap.get(record.Id).SuppliedEmail) {
                withSuppliedEmails.add(record);
            }
        }
        populateSubmittedByUserField(withSuppliedEmails);
    }

    public static void onAfterInsert(List<Case> records) {
        updateOutstandingCount(records);
        updateAccountComplaintType(records);
    }

    public static void onAfterUpdate(List<Case> records, Map<Id, Case> recordsMap) {
        updateOutstandingCount(records);
        updateAccountComplaintType(records);

        List<Case> casesForNotification = new List<Case>();
        for (Case record : records) {
            if (String.isNotBlank(record.RecordTypeId) && sasRecordTypes.contains(record.RecordTypeId) && record.Status == 'Closed' && record.Status != recordsMap.get(record.Id).Status) {
                casesForNotification.add(record);
            }
        }
        notifyImproperCaseClose(casesForNotification);
    }

    public static void onAfterDelete(List<Case> records) {
        updateOutstandingCount(records);

        List<Case> casesForNotification = new List<Case>();
        for (Case record : records) {
            if (String.isNotBlank(record.RecordTypeId) && sasRecordTypes.contains(record.RecordTypeId)) {
                casesForNotification.add(record);
            }
        }
        if (!casesForNotification.isEmpty()) {
            notifyCaseDeletion(casesForNotification);
        }
    }


    /**
     * @description Determines the appropriate Account lookup record for the case, given a student identifier
     * @author Ranyel Maliwanag
     * @date 04.AUG.2015
     */
    private static void setAccountDetails(List<Case> records) {
        String queryString;
        List<String> studentIdentifiers = new List<String>();

        for (Case record : records) {
            if (String.isBlank(record.AccountId) && String.isNotBlank(record.Student_Identifier__c)) {
                studentIdentifiers.add(record.Student_Identifier__c);
            }
        }

        queryString = 'SELECT Id, Student_Identifer__c FROM Account WHERE Student_Identifer__c IN :studentIdentifiers';
        List<Account> accounts = Database.query(queryString);

        Map<String, Account> accountsMap = new Map<String, Account>();
        for (Account accountRecord : accounts) {
            accountsMap.put(accountRecord.Student_Identifer__c, accountRecord);
        }

        for (Case record : records) {
            if (String.isBlank(record.AccountId) && String.isNotBlank(record.Student_Identifier__c) && accountsMap.containsKey(record.Student_Identifier__c)) {
                record.AccountId = accountsMap.get(record.Student_Identifier__c).Id;
            }
        }
    }


    /**
     * @description Updates the outstanding complaints count on the related Account records
     * @author Ranyel Maliwanag
     * @date 04.AUG.2015
     * @param (List<Case>) records: The Case records whose associated Account records needs updating for the outstanding complaints count
     */
    private static void updateOutstandingCount(List<Case> records) {
        Set<Id> accountIds = new Set<Id>();
        String queryString;

        for (Case record : records) {
            accountIds.add(record.AccountId);
        }

        queryString = 'SELECT Id, (SELECT Id, IsClosed FROM Cases WHERE RecordType.Name = \'Student Feedback\') FROM Account WHERE Id IN :accountIds';
        List<Account> results = Database.query(queryString);

        for (Account accountRecord : results) {
            Integer totalCaseCount = accountRecord.Cases.size();
            Integer outstandingCaseCount = 0;
            for (Case caseRecord : accountRecord.Cases) {
                if (!caseRecord.IsClosed) {
                    outstandingCaseCount++;
                }
            }

            accountRecord.Total_Outstanding_Complaint__c = outstandingCaseCount;
            accountRecord.Total_Complaints__c = totalCaseCount;
        } 

        update results;
    }

    /**
     * @description Updates the related account 'Complaint Type' 
     * @author Biao Zhang
     * @date 19.Nov.2015
     * @param (List<Case>) records: The Case records whose associated Account records needs to be updated 
     */
    private static void updateAccountComplaintType(List<Case> records) {
        Set<Id> accountIds = new Set<Id>();
        Map<String, Integer> severityMap = new Map<String, Integer>{
            'Do Not Contact' => 1,
            'High' => 2,
            'Medium' => 3,
            'Low' => 4
        };

        for (Case record : records) {
            if(record.IsClosed == false) {
                accountIds.add(record.AccountId);
            }
        }

        List<Account> accounts = [SELECT
                                Id,
                                Complaint_Type__c,
                                Complaint_Severity__c,
                                (SELECT
                                    Id,
                                    Complaint_Severity__c,
                                    Type
                                FROM
                                    Cases
                                WHERE
                                    RecordType.Name = 'Student Feedback'
                                AND
                                    IsClosed = false
                                Order by
                                    CreatedDate ASC)
                            FROM
                                Account
                            WHERE
                                Id in :accountIds
                            ];

        for (Account account : accounts) {
            String highestSeverity = account.Complaint_Severity__c;
            for (Case ca : account.Cases) {
                account.Complaint_Type__c = ca.Type;

                if (String.isNotBlank(ca.Complaint_Severity__c)) {
                    if (String.isBlank(highestSeverity)) {
                        highestSeverity = ca.Complaint_Severity__c;
                    } else {
                        if (severityMap.containsKey(ca.Complaint_Severity__c) && severityMap.containsKey(highestSeverity) && severityMap.get(ca.Complaint_Severity__c) < severityMap.get(highestSeverity)) {
                            highestSeverity = ca.Complaint_Severity__c;
                        }
                    }
                }
            }

            if (String.isNotBlank(highestSeverity)) {
                account.Complaint_Severity__c = highestSeverity;
            }
        } 
        update accounts;
    }


    /**
     * @description Sends a notification to the designated user everytime a case gets deleted
     * @author Ranyel Maliwanag
     * @ate 16.MAR.2016
     */
    private static void notifyCaseDeletion(List<Case> records) {
        String emailBody = 'Hi Troy, the following cases have been deleted in CASIS: \n\n';
        for (Case record : records) {
            emailBody += 'Case Id: ' + record.Id + '\n';
            emailBody += 'Case Number: ' + record.CaseNumber + '\n';
            emailBody += 'Owner: ' + record.OwnerId + '\n';
            emailBody += 'Subject: ' + record.Subject + '\n';
            emailBody += 'Created Date: ' + record.CreatedDate + '\n';
            emailBody += 'Deleted By: ' + UserInfo.getName() + '\n';
            emailBody += 'Deleted Date: ' + Datetime.now() + '\n';
            emailBody += '\n';
        }

        try {
            String userEmail = 'troy.collins@careersaustralia.edu.au';
            String[] toAddresses = new String[] {userEmail};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('Case Deletion Notification');
            mail.setPlainTextBody(emailBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
        } catch (Exception e) {
            // Silent error
        }
    }


    /**
     * @description Populates the `Submitted_By_User__c` field on the case based on the `SuppliedEmail`. Original logic transferred from `SetUserField` trigger.
     * @author Ranyel Maliwanag
     * @date 17.MAR.2016
     */
    private static void populateSubmittedByUserField(List<Case> records) {
        Set<String> emailSet = new Set<String>();
        for (Case record : records) {
            emailSet.add(record.SuppliedEmail);
        }

        Map<String, Id> userEmailMap = new Map<String, Id>();
        for (User record : [SELECT Id, Email FROM User WHERE Email IN :emailSet AND IsActive = true]) {
            userEmailMap.put(record.Email, record.Id);
        }

        for (Case record : records) {
            if (userEmailMap.containsKey(record.SuppliedEmail)) {
                record.Submitted_By_User__c = userEmailMap.get(record.SuppliedEmail);
                System.debug('::: Record updated : ' + record.Submitted_By_User__c);
            }
        }
    }


    /**
     * @description Sends a notification to the designated user everytime a case gets closed without replies
     * @author Ranyel Maliwanag
     * @date 22.MAR.2016
     */
    private static void notifyImproperCaseClose(List<Case> records) {
        Set<Id> caseIds = new Set<Id>();
        for (Case record : records) {
            caseIds.add(record.Id);
        }

        // Query Cases and Email Messages
        List<Case> finalCases = new List<Case>();
        List<Case> filteredRecords = [SELECT Id, CaseNumber, OwnerId, Subject, CreatedDate, (SELECT Id FROM EmailMessages) FROM Case WHERE Id IN :caseIds];
        for (Case record : filteredRecords) {
            if (record.EmailMessages.size() <= 1) {
                finalCases.add(record);
            }
        }

        if (!finalCases.isEmpty()) {
            String emailBody = 'Hi Troy, the following cases have been closed in CASIS without any replies sent to the user: \n\n';
            for (Case record : records) {
                emailBody += 'Case Id: ' + record.Id + '\n';
                emailBody += 'Case Number: ' + record.CaseNumber + '\n';
                emailBody += 'Owner: ' + record.OwnerId + '\n';
                emailBody += 'Subject: ' + record.Subject + '\n';
                emailBody += 'Created Date: ' + record.CreatedDate + '\n';
                emailBody += 'Deleted By: ' + UserInfo.getName() + '\n';
                emailBody += 'Deleted Date: ' + Datetime.now() + '\n';
                emailBody += '\n';
            }

            try {
                String userEmail = 'troy.collins@careersaustralia.edu.au';
                String[] toAddresses = new String[] {userEmail};
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddresses);
                mail.setSubject('Improper Case Close Notification');
                mail.setPlainTextBody(emailBody);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
            } catch (Exception e) {
                // Silent error
            }
        }
    }
}