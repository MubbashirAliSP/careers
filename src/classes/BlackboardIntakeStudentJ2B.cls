/*JOB2b Staging table loader job
 *This class is resposible for loading creating and loading 
 *Blackboard Course Memberships
 *Course memberships are created from Intake Students, however
 *before we can create course memberships we need to create Blackboard Users and
 *Blackboard User Role for each BBCM created if there is no Blackboard User and Blackboard User Role
 *previously created.
 *This job is designed for T4(organisation) and T5(support material) type courses
 *Please Refer to ERD for further details about relationships
 *Created By Yuri Gribanov - CloudSherpas 15th July 2014
 */

 /*****BATCH SIZE 1 ONLY ******/

global with sharing class BlackboardIntakeStudentJ2B implements Database.Batchable<sObject> {

    global String query;

    global BlackboardIntakeStudentJ2B() {
    /*Initial query to be passed into query locator performed against Enrolment Intake Units*/
        query = 'Select Id,Enrolment__r.Qualification__c,Enrolment__r.Student__c From Intake_Students__c Where Intake__r.Qualification__r.Blackboard_Institutional_Role__c != null And Blackboard_Course_Membership_Created__c = false And Enrolment__r.Blackboard_Required__c = true';
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        Set<Id>qualIds = new Set<Id>();
        List<Blackboard_User__c> bbuserList = new List<Blackboard_User__c>();
        List<Blackboard_User_Role__c>bbuserroleList = new List<Blackboard_User_Role__c>();
        List<Blackboard_Course_Membership__c> bbcmList = new List<Blackboard_Course_Membership__c>();
        List<Intake_Students__c> IntakeStudentsToUpdate= new List<Intake_Students__c>();
        Map<Id,Id>userRolesInDBMap = new Map<Id,Id>();
        Set<Id>accIds = new Set<Id>();
        Set<Id>IsIds = new Set<Id>();
        //Recordtypes for BBCM
        List<RecordType>bbcmRecTypes = New List<RecordType>([Select Id,DeveloperName From RecordType Where DeveloperName = 'Student_T4' OR DeveloperName = 'Student_T5' ORDER By DeveloperName]);
        //RecordTypes for User And User Role
        List<RecordType>userAnduserRoleRecTypes = new List<RecordType>([Select Id, DeveloperName, SObjectType From RecordType Where DeveloperName = 'Student' And (SObjectType = 'Blackboard_User__c' OR SObjectType = 'Blackboard_User_Role__c')  ORDER BY SObjectType Desc]);
        List<Qualification_Support_Item__c> qsiList = new List<Qualification_Support_Item__c>();
        List<Blackboard_Role__c> bbRole = new List<Blackboard_Role__c>([Select Id From Blackboard_Role__c Where Blackboard_Role_Name__c = 'Student' AND RecordType.DeveloperName = 'Course_Role']);
        
        
        
        for(SObject s :scope) {

             Intake_Students__c is = (Intake_Students__c) s;
             
             qualIds.add(is.Enrolment__r.Qualification__c);

             accIds.add(is.Enrolment__r.Student__c);

             IsIds.add(is.Id);


             //If Blackboard User Doesn't Exist - Create
              if([Select Id From Blackboard_User__c Where Account__c = : is.Enrolment__r.Student__c].size() == 0) {

                Blackboard_User__c bbuser = new Blackboard_User__c(Account__c = is.Enrolment__r.Student__c,
                                                                   Enable_Blackboard_User__c = true,
                                                                   RecordTypeId = userAnduserRoleRecTypes.get(0).Id);

                bbuserList.add(bbuser);

             }
            
        }

        //Insert new Blackboard Users Here
        insert bbuserList;

        if(bbuserList.size() > 0) {

          /*Create New Blackboard User Roles here for just created new users */
          for(Blackboard_User__c bbuser : bbuserList) {

            Blackboard_User_Role__c bbuserrole = new Blackboard_User_Role__c(RecordTypeId = userAnduserRoleRecTypes.get(1).Id,
                                                                             Blackboard_User__c = bbuser.Id,
                                                                             Blackboard_Role__c = bbRole.get(0).Id);

                bbuserroleList.add(bbuserrole);
          }
          //Insert New Blackboard User Roles here
          insert bbuserroleList;

        }
         
         
         for(Blackboard_User_Role__c bur : [Select Id,Blackboard_User__r.Account__c From Blackboard_User_Role__c Where Blackboard_User__r.Account__c in : accIds LIMIT 1])
               userRolesInDBMap.put(bur.Blackboard_User__r.Account__c, bur.Id);

         
         
        /*We create BBCM for each Qualification support item attached to intake student */
        for(Intake_Students__c is : [Select Id,Enrolment__r.Student__c From Intake_Students__c Where id in : IsIds]) {

            for(Qualification_Support_Item__c qsi : [Select Id,Blackboard_Course_ID__c, Blackboard_Course_Id__r.RecordType.DeveloperName From Qualification_Support_Item__c Where Qualification__c in : qualIds]) {
                
                String RecType = '';
                
                //Need to grab correct recordtype based on Blackboard Course
                if(qsi.Blackboard_Course_Id__r.RecordType.DeveloperName == 'T4_Organisation')
                    RecType = bbcmRecTypes.get(0).Id;
                else
                    RecType = bbcmRecTypes.get(1).Id;

                //construct new BBCM here
                Blackboard_Course_Membership__c bbcm = new Blackboard_Course_Membership__c(

                   Blackboard_Course__c = qsi.Blackboard_Course_ID__c,
                   Intake_Student__c = is.Id,
                   Blackboard_User_Role__c = userRolesInDBMap.get(is.Enrolment__r.Student__c),
                   RecordTypeId = RecType
                   
                   
                );

                bbcmList.add(bbcm);
                
               

               

            }
                  //set the flag to true for processed Enrolment intake unit
                is.Blackboard_Course_Membership_Created__c = true;
                IntakeStudentsToUpdate.add(is);

        }

        insert bbcmList;
        
        update IntakeStudentsToUpdate;


    }

    global void finish(Database.BatchableContext BC) {
    
      BlackboardIntakeUnitUserRoleJ2C b1 = new BlackboardIntakeUnitUserRoleJ2C();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }

}