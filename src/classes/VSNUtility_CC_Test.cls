/**
 * @description Test Class for IntakeUnitOfStudyTriggerHandler 
 * @author Cletz Cadiz
 * @date 26.FEB.2013
 *
 * HISTORY
 * - 26.FEB.2013    Cletz Cadiz          Created.
 * - 08.APR.2014    Michelle Magsarili   called in TestCoverageUtilityClass.createNationalResult()
 */
@isTest
private class VSNUtility_CC_Test {
    @testSetup
    static void setup() {
        NAT_Test_User__c setting = new NAT_Test_User__c(
                ID__c = '00590000000HR2zAAG',
                Name = 'NAT Test User ID',
                Username__c = 'casis-admin@cloudsherpas.com'
            );
        insert setting;
    }



    static testMethod void testVSNUtility() {
    
        string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
    User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        system.runas(u) {
        
        
        /* Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = [Select Id, Name from User where LastName = 'Canlas' or UserName ='jcanlas@cloudsherpas.com']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test', Part_of_Support_Group__c=TRUE);            
        //RecordType recId = [Select Id,Name from RecordType where Name=:'VSN Library'];   
        
        */         
        
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //system.runAs(u){
            test.startTest();
                res = TestCoverageUtilityClass.createNationalResult(); //MAM 04/08/2014 call in TestCoverageUtilityClass.createNationalResult()
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                countryAU = TestCoverageUtilityClass.queryAustralia();
                state = TestCoverageUtilityClass.createState(country.Id); 
                
                system.runAs(u){//to satisfy same owner validation
                    fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                    pCon = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, accTraining.Id);
                    conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
                    lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                    lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                    loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                    parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                    loc = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id, accTraining.Id); 
                    loc2 = TestCoverageUtilityClass.createLocationWithParentTR(countryAU.Id, accTraining.Id, parentLoc.Id, state.Id);
                    system.debug('@@1. loc2.Training_Organisation__c' + loc2.Training_Organisation__c);
                    del = TestCoverageUtilityClass.createDeliveryModeType(); 
                    attend = TestCoverageUtilityClass.createAttendanceType();
                    
                    enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id);
                    enrl.Predominant_Delivery_Mode__c = del.Id; 
                    enrl.Type_of_Attendance__c = attend.Id;
                    enrl.Overseas_Country__c = null;
                    enrl.Enrolment_Status__c ='Active (Commencement)';
                    enrl.Delivery_Location__c = loc.id;

                    insert enrl;
                    system.debug('@@1. enrl.Training_Organisation__c' + enrl.Training_Organisation__c);
                    enrl.Start_Date__c = date.newInstance(2013, 1, 1);
                    enrl.End_Date__c = date.newInstance(2013, 2, 3);
                    update enrl;
                }

                Pagereference testPage = Page.VSNUtility;
                testPage.getParameters().put('enrl', enrl.Id);
                Test.setCurrentPage(testPage);
                
                VSNUtility_CC vsnCC = new VSNUtility_CC();
                
                try
                {
                    vsnCC.Month = null;
                    vsnCC.Year = null;
                    vsnCC.generateEnrolmentList();
                }
                catch(Exception e)
                {
                    Boolean expectedExceptionThrown =  e.getMessage().contains('Please select Month and Year.') ? true : false;
                    System.AssertEquals(expectedExceptionThrown, true);
                } 
                try
                {
                    vsnCC.Month = '1';
                    vsnCC.Year = '2013';
                    vsnCC.rto = null;
                    vsnCC.generateEnrolmentList();
                }
                catch(Exception e)
                {
                    Boolean expectedExceptionThrown =  e.getMessage().contains('Please select RTO.') ? true : false;
                    System.AssertEquals(expectedExceptionThrown, true);
                } 
                
                vsnCC.Month = '1';
                vsnCC.Year = '2013';
                String rto = '';
                rto = [select Id, Training_Organisation__c from Enrolment__c where Id =: enrl.Id].Training_Organisation__c;
                vsnCC.rto = rto;
                system.debug('@@rto val' + vsnCC.rto);
                vsnCC.getmonthPicklist();
                vsnCC.getyearPicklist();
                vsnCC.getRTOPicklist();
                vsnCC.generateEnrolmentList();
                //List<VSNUtility_CC.pageWrapper> wrapperList = new List<VSNUtility_CC.pageWrapper>();
                //VSNUtility_CC.pageWrapper wrapper = new VSNUtility_CC.pageWrapper(enrl, 'F', UserInfo.getUsername());
                //wrapperList.add(wrapper);
                //vsnCC.pageWrapperList = wrapperList;
                vsnCC.exportData();
                vsnCC.generateEnrolmentFile();
                
             test.stopTest();
        }
    }
}