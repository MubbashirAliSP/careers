/*Controls creation of claims */

public with sharing class ClaimGenerator {
	
	//Standard claiming, applies to QLD, VIC, SA
	public static void claimStandard(List<Enrolment_Unit__c> enrolment_units, List<Enrolment__c> enrolments, Account TrainingOrg, String State)  {
		/*1. Create New Claim
		 *2. Insert Enrolment Claim Records
		 *3. Insert Enrolment Uniy claims and relate to Enrolment Claim, Enrolment Unit and claim
		
		
		List<Enrolment_Claims__c>EnrlClaimsToInsert = new List<Enrolment_Claims__c>();
		List<Enrolment_Unit_Claim__c> EnrlUnitClaimsToInsert = new List<Enrolment_Unit_Claim__c>();
		
		//Step 1
		//Decimal nextClaimNumber = NatGeneratorUtility.nextClaimNumber(State);
		Claim_Number__c claim = new Claim_Number__c(Training_Organisation__c = TrainingOrg.id, 
													State__c = State
													//Claim_Counter__c = nextClaimNumber,
													//Claim_Number__c = State + ' - ' + Integer.valueOf(nextClaimNumber)); 
													);
		insert claim;
		
		//Step2
		for(Enrolment__c enrl :enrolments ) {
		
			Enrolment_Claims__c enrlClaim = new Enrolment_Claims__c(Enrolment__c = enrl.id,Claim_Number__c = claim.id);
			EnrlClaimsToInsert.add(enrlClaim);
			
		}
		
		insert EnrlClaimsToInsert;
		
		//step3
		
		Map<Id,Id>EnrolmentClaimsMap = new Map<Id,Id>();
		
		for(Enrolment_Claims__c ec : [Select id, Enrolment__c, Claim_Number__c FROM Enrolment_Claims__c WHERE Claim_Number__c = : claim.id])
			EnrolmentClaimsMap.put(ec.Enrolment__c, ec.id);
			
		for(Enrolment_Unit__c eu : enrolment_units) {
			
			if(EnrolmentClaimsMap.containsKey(eu.Enrolment__c)) {
				Enrolment_Unit_Claim__c EnrlUnitClaim = new Enrolment_Unit_Claim__c(Claim_Number__c = claim.id,
																				    Enrolment_Claim__c = EnrolmentClaimsMap.get(eu.Enrolment__c),
																				    Enrolment_Unit__c = eu.id);
				EnrlUnitClaimsToInsert.add(EnrlUnitClaim);
			}
			
			
			
		}
		
		insert EnrlUnitClaimsToInsert;
		
		*/
		
	}
	
	
	//Applies to NSW only
	public static Id claimNSW(List<Enrolment__c> enrolments, String state, Id TrainingOrgId, Date claimDate, String CT) {
		
		List<Enrolment_Claims__c>EnrlClaimsToInsert = new List<Enrolment_Claims__c>();
				
		//Step 1
		//Decimal nextClaimNumber = NatGeneratorUtility.nextClaimNumber(State);
		Claim_Number__c claim = new Claim_Number__c(Training_Organisation__c = TrainingOrgId, 
													State__c = State,
													//Claim_Counter__c = nextClaimNumber,
													//Claim_Number__c = State + ' - ' + Integer.valueOf(nextClaimNumber),
													Claim_Date__c = claimDate);
													
	    insert claim;
	    
	    
	    //Step2
		for(Enrolment__c enrl :enrolments ) {
		
			Enrolment_Claims__c enrlClaim = new Enrolment_Claims__c(Enrolment__c = enrl.id,Claim_Number__c = claim.id, Claim_Type__c = CT, of_Enrolment_Completed__c = enrl.of_Enrolment_Completed__c);
			EnrlClaimsToInsert.add(enrlClaim);
			
		}
		
		insert EnrlClaimsToInsert;
		
		return claim.id;
	    
	    
		
	}
	

}