/**
 * @description N/A
 * @author N/A
 * @date N/A
 * @history
 *       01.FEB.2016    Ranyel Maliwanag    - Added addtional fields for the Enrolment query for the state of Victoria. Reference Case: 00038985
 *                                          - Added try-catch block on the email send call on `finish()` to let normal execution flow for sandboxes that have email deliverability turned off
 */
global class NATGeneratorBatch00130 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    private String currentState;
    public String libraryId; 
    global String body130;
    private Set<String> body130Set = new Set<String>();
    global String natLogId;
    global DateTime currentDT;
    global NAT_Validation_Log__c natLog;

    public Map<Id,Decimal>Unit_NominalHoursMap;
    
    
    global NATGeneratorBatch00130(String state, String q, String logId) {
        query = q;
        currentState = state;   
        natLogId = logId;
        Set<Id>unitIds = new Set<Id>();
        Set<Id>enrolmentIds = new Set<Id>();
        List<String>splitter = new List<String>();
        Unit_NominalHoursMap = new Map<Id,Decimal>();
        
        natLog = [ SELECT Id, Content_Library_Id__c, Log_Run_At__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :natLogId ];

        currentDT = natLog.Log_Run_At__c;
        
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
       body130 = '';   
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
    
        Set<Id>enrolmentIds = new Set<Id>();
        Set<Id>awardQuals = new Set<Id>();
        Set<Id>hasAward = new Set<Id>();
            
        for (SObject s :scope) {
            Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
            enrolmentIds.add(eu.Enrolment__c);
        }
            
        List<Awards__c> awardsNonVic = [ SELECT ID, Education_Training_International_Flag__c, Year_Program_Completed__c, Qualification_Issued__c, Enrolment__r.Training_Organisation_Identifier__c,
                          Enrolment__r.Student_Identifier__c, Enrolment__r.Start_Date__c, Award_ID_CASIS__c, Date_Issued__c, Enrolment__r.Parent_Qualification_Code__c  FROM Awards__c WHERE RecordType.Name = 'approved award' 
                          AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' AND Enrolment__c in :enrolmentIds ];
        
        List<Enrolment__c> enrolVic = [ SELECT ID, Training_Organisation_Identifier__c, Student_Identifier__c, Start_Date__c, Parent_Qualification_Code__c FROM Enrolment__c WHERE ID IN :enrolmentIds];
        
            if (currentState == 'Queensland') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionQLD.nat00130(), currentState));

                    //body130 += NATConstructor.natFile(aw, NATDataDefinitionQLD.nat00130(), currentState);
                    //body130 += '\r\n';
                }
            }
            
            else if (currentState == 'New South Wales') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){     
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionNSW.nat00130(), currentState));
                }
            }
        
        	else if (currentState == 'New South Wales APL') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){     
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionNSWAPL.nat00130(), currentState));
                }
            }
            
            
            else if (currentState == 'Australian Capital Territory') {
                 libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files ACT' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){        
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionACT.nat00130(), currentState));

                    //body130 += NATConstructor.natFile(aw, NATDataDefinitionACT.nat00130(), currentState);
                    //body130 += '\r\n';
                }
            }
            
                        
            else if (currentState == 'South Australia') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;                    
                if(awardsNonVic != null) {
                    for(Awards__c aw: awardsNonVic){  
                        body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionSA.nat00130(), currentState));
               		} 
                }
            }
            
            
            else if (currentState == 'Western Australia') {
                     libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files WA' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){       
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionWA.nat00130(), currentState));
                }
            }
            
            
            else if(currentState == 'Northern Territory') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NT' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){ 
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionNT.nat00130(), currentState));
                }
            }
            
        
            else if(currentState == 'Tasmania') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files TAS' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){   
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionTAS.nat00130(), currentState));
                }
            }
        
        	else if(currentState == 'National') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files National' LIMIT 1].Id;                    
                for(Awards__c aw: awardsNonVic){   
                    body130Set.add(NATConstructor.natFile(aw, NATDataDefinition.nat00130(), currentState));
                }
            }
            
        	else if (currentstate == 'Victoria') {
            	libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id; 
            	Set<Id> enrolWithAward = new Set<Id>();
            
            	for(Enrolment__c enrl: [SELECT Id, Training_Organisation_Identifier__c,Parent_Qualification_Code__c, Student_Identifier__c, Start_Date__c,
                                           Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c, Victorian_Commencement_Date__c, End_Date__c, Qualification__r.Award_based_on__r.VTGPUSH__c, Line_Item_Qualification__r.State_Fund_Source_Identifier__c,
                                           (SELECT Year_Program_Completed__c, Qualification_Issued__c,Enrolment__r.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c, Enrolment__r.Victorian_Commencement_Date__c, Enrolment__r.Parent_Qualification_Code__c, Enrolment__r.Student_Identifier__c
                                           FROM Awards__r WHERE RecordType.Name = 'approved award' AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' LIMIT 1)
                                           FROM Enrolment__c WHERE id in : enrolmentIds]){
 
                    for(Awards__c aw : enrl.awards__r ) {
                         body130Set.add(NATConstructor.natFile(aw, NATDataDefinitionVIC.nat00130(), currentState));

                         //body130 += NATConstructor.natFile(aw, NATDataDefinitionVIC.nat00130(), currentState);
                         //body130 += '\r\n';   
                         enrolWithAward.add(enrl.Id);
                    }
                         
        	}
            
            for(Enrolment__c enrl: [SELECT Id, Training_Organisation_Identifier__c,Parent_Qualification_Code__c, Student_Identifier__c, Start_Date__c,
                                           Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c, Victorian_Commencement_Date__c, End_Date__c, Qualification__r.Award_based_on__r.VTGPUSH__c, Line_Item_Qualification__r.State_Fund_Source_Identifier__c,
                                           (SELECT Year_Program_Completed__c, Qualification_Issued__c,Enrolment__r.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c, Enrolment__r.Victorian_Commencement_Date__c, Enrolment__r.Parent_Qualification_Code__c, Enrolment__r.Student_Identifier__c
                                           FROM Awards__r WHERE RecordType.Name = 'approved award' AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' LIMIT 1)
                                           FROM Enrolment__c WHERE id in : enrolmentIds]){
 
            	if(!enrolWithAward.contains(enrl.Id) ) {
                    
                    body130Set.add(NATConstructor.natFile(enrl, NATDataDefinitionVIC.nat00130noAwards(), currentState));

           			//body130 += NATConstructor.natFile(enrl, NATDataDefinitionVIC.nat00130noAwards(), currentState);
                    //body130 += '\r\n';   

                }
                        
            }             
        	
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        
        if(body130Set.size() > 0){
            for (string s : body130Set) {
                body130 += s;
                body130 += '\r\n';
            }
        }
		
        if(body130 == '') {
            body130 = ' ';
        }        

        Blob pBlob130 = Blob.valueof(body130);
        insert new ContentVersion(
            versionData =  pBlob130,
            Title = 'NAT00130',
            PathOnClient = '/NAT00130.txt',
            FirstPublishLocationId = natLog.Content_Library_ID__c
        ); 
        
        try {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
            mail.setToAddresses(toAddresses);  
            mail.setSubject('NAT Generation is Complete');  
            mail.setPlainTextbody('NAT Generation completed, please click on the URL to check the files. ' + URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/#search');
            mail.setHTMLBody('NAT Generation completed, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/#search' + '">click here</a> to check the files.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        } catch (Exception e) {
            // silent exception
        }
        
        
    }
}