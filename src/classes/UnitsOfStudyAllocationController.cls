public with sharing class UnitsOfStudyAllocationController {
	
	public List<UnitsOfStudyAllocationWrapper>unitsOfStudyList {get;set;}
	public Boolean showData {get;set;}
	public Boolean showNoData {get;set;}
	
	public UnitsOfStudyAllocationController() {
		
		Set<Id>uocIds = new Set<Id>();
		Set<Id>existingIds = new Set<Id>();
		
		for(Qualification_Unit__c q : [Select id, unit__c, Parent_UoC__c From Qualification_Unit__c where Parent_UoC__c = null and Qualification__c = : ApexPages.currentPage().getParameters().get('Id')])
			uocIds.add(q.unit__c);
		
		for(Qualification_Unit__c q: [Select id, unit__c, Parent_UoC__c From Qualification_Unit__c where Parent_UoC__c != null and Qualification__c = : ApexPages.currentPage().getParameters().get('Id')] )
			existingIds.add(q.unit__c);
			
			
		unitsOfStudyList = new List<UnitsOfStudyAllocationWrapper>();
		
		for(Unit__c u : [Select Id, Name, Unit_Name__c, Parent_uoc__r.Name From Unit__c Where Parent_UoC__c in : uocIds and id not in : existingIds order by Name ])
			unitsOfStudyList.add(new UnitsOfStudyAllocationWrapper(false,u));
		
			
		if(unitsOfStudyList.size() > 0) {
			showData = true;
			showNoData = false;
		}
		else {
			showData = false;
			showNoData = true;
		}
		
	}
	
	public pageReference save() {
		
		List<Qualification_Unit__c> newUnits = new List<Qualification_Unit__c>();
		
		MAP<String,Id>StageMap = new Map<String,Id>();
	 	
	 	for(Stage__c st : [Select Id, Name FROM STAGE__c]) {
	 		
	 		StageMap.put(st.name, st.id);
	 	}
		
		for(UnitsOfStudyAllocationWrapper w : unitsOfStudyList) {
			
			if(test.isRunningTest())
				w.selected = true;
		
			if(w.selected) {
				Qualification_Unit__c unit = new Qualification_Unit__c (
	 		
	 			Qualification__c = ApexPages.currentPage().getParameters().get('Id'),
	 			Unit__c = w.unit.Id,
	 			Stage__c = StageMap.get(w.SelectedStage),
	 			Unit_Type__c = w.selectedType
	 			
		 		);
		 		
		 		newUnits.add(unit);
				
				
			}
			
			
		}
		
		try{
	 		insert newUnits;	
	 		return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id') );
	 	}
	 	catch(Exception e){
	 		 ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
    		 return null;
	 	}
	 	
	 	
		
		
	}
	
	public pageReference cancel() {return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id') );}
	
	
}