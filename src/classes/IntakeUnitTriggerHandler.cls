/**
 * @description Intke Unit Trigger Handler
 * @author Janella Lauren Canlas
 * @date 14.DEC.2012
 *
 * HISTORY
 * - 14.DEC.2012	Janella Canlas - Created.
 * - 17.JAN.2013	Janella Canlas - renamed from IntakeUnitBIBU_CC to IntakeUnitTriggerHandler
 */
public with sharing class IntakeUnitTriggerHandler {
	/*
		This method will populate the Intake Unit Date fields depending on the related Intakes
	*/
	public static void populateDate(List<Intake_Unit__c> iuList){
		Set<Id> intIds = new Set<Id>();
		List<Intake__c> intList = new List<Intake__c>();
		//collect intake ids
		for(Intake_Unit__c i : iuList){
			intIds.add(i.Intake__c);
		}
		//query intake details
		intList = [select Id, Start_Date__c, End_Date__c from Intake__c where Id IN: intIds];
		//loop to populate intake unit start and end dates
		for(Intake_Unit__c iu : iuList){
			for(Intake__c intk : intList){
				if(iu.Intake__c == intk.Id){
					iu.Unit_Start_Date__c = intk.Start_Date__c;
					iu.Unit_End_Date__c = intk.End_Date__c;
				}
			}
		}
		
	}
}