/**
 * @description Test class for VFHCourseSelection_CC
 * @author Dan Crisologo
 * @date 5.JUL.2013
 */
@isTest
private class VFHCourseSelection_CC_Test {

    //Test Single Diploma Qualifications
    static testMethod void testVFHCourseSelection_SingleDiploma() {
    
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c tailoredQual = new Qualification__c();

        test.startTest();
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        tailoredQual = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
        
        Delivery_Mode_Types__c dmt = new Delivery_Mode_Types__c(Type__c = 'VFH/HEIMS', Common_Name__c='testDeliveryMode', Code__c = '12345');
        insert dmt;
        
        Qualification_Delivery_Mode__c qdm = new Qualification_Delivery_Mode__c(Qualification__c = tailoredQual.Id, Delivery_Mode__c=dmt.Id);
        insert qdm;
        
        VFHCourseSelection_CC vfhCS = new VFHCourseSelection_CC();
        vfhCS.getYesNoOption();
        vfhCS.singleDiplomaClick();
        vfhCS.applySingleDiploma();
        vfhCS.validateParameters();
        test.stopTest();
        
    }  
    
    static testMethod void testQualWrapper(){
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c tailoredQual = new Qualification__c();

        test.startTest();
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        tailoredQual = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);  
        
        Delivery_Mode_Types__c dmt = new Delivery_Mode_Types__c(Type__c = 'VFH/HEIMS', Common_Name__c='testDeliveryMode', Code__c = '12345');
        insert dmt;
        
        List<Qualification_Delivery_Mode__c> qdmList = new List<Qualification_Delivery_Mode__c>();
        
        for(Integer i=0; i<=5; i++){
            Qualification_Delivery_Mode__c qdm = new Qualification_Delivery_Mode__c(Qualification__c = tailoredQual.Id, Delivery_Mode__c=dmt.Id);
            qdmList.add(qdm);
        }
        insert qdmList;       
        system.debug('**QDM: ' + qdmList.size() + ' ,Qual: ' + qual);
          
        VFHCourseSelection_CC vfhCS = new VFHCourseSelection_CC();
        VFHCourseSelection_CC.SingleDiplomaQualifications qualWrap = new VFHCourseSelection_CC.SingleDiplomaQualifications(qual, qdmList);
        test.stopTest();
    }
    
    //Test Single Diploma Qualifications
    static testMethod void testVFHCourseSelection_DoubleDiploma() {   
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c tailoredQual = new Qualification__c();

        test.startTest();
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        tailoredQual = TestCoverageUtilityClass.createTailoredQualification(anz.Id, fld.Id);
        
        Delivery_Mode_Types__c dmt = new Delivery_Mode_Types__c(Type__c = 'VFH/HEIMS', Common_Name__c='testDeliveryMode', Code__c = '12345');
        insert dmt;
        
        Qualification_Delivery_Mode__c qdm = new Qualification_Delivery_Mode__c(Qualification__c = tailoredQual.Id, Delivery_Mode__c=dmt.Id);
        insert qdm;
        
        //create double diploma
        Double_Diploma_Mapping__c ddMap = new Double_Diploma_Mapping__c(Double_Diploma_Name__c = 'test', Qualification1__c=tailoredQual.id, Qualification2__c=tailoredQual.id);
        insert ddMap;

        Test.setCurrentPageReference(Page.VFHCourseSelection); 
        System.currentPageReference().getParameters().put('LeadSource', 'Acquire');
        
        VFHCourseSelection_CC vfhCS = new VFHCourseSelection_CC();
        vfhCS.getYesNoOption();
        vfhCS.getDiplomaNames();
        vfhCS.doubleDiplomaClick();
        vfhCS.applyDoubleDiploma();
        vfhCS.validateParameters();
        test.stopTest();
        
    }   
}