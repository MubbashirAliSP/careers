public class treenodes {

    public List<SelectOption> items3 { get; set; }
    public String SelectedView { get; set; }  
    public String SelectedUnit { get; set; }
    public String SelectedSearch { get; set; }
    public String ref{get;set;}
    public Boolean pRender{get;set;}
    
    public Boolean nRender{get;set;}
    public String pageNo{get;set;}
    public Integer recordCount{get;set;}
    public Integer pages{get;set;}
    private Integer offsetVal;
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Unit Name','Unit Name'));
            options.add(new SelectOption('Code','Code'));
            
            return options;
    }
    public List<SelectOption> getItems2() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All','All'));            
            options.add(new SelectOption('Core','Core'));
            options.add(new SelectOption('Elective','Elective'));
            options.add(new SelectOption('None','None'));
            return options;
    }
    




    public String searchText { get; set; }




    public PageReference search() {
        ref='';
        if((searchText.length() <5) && (SelectedSearch == 'Unit Name')){
            ref = 'Please enter at least 5 characters for Unit Name';
        } 
        else if(searchText.length() <2){
            ref = 'Please enter at least 2 characters for Unit Code';
        }
        else {
        transient String strLikeVal = '%' + searchText + '%';
        transient String qryString;
        if(SelectedSearch == 'Unit Name'){
            if(SelectedUnit == 'All'){
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit__r.Unit_Name__c like : strLikeVal order by Unit__r.Name limit 5000';
            }
            else if(SelectedUnit == 'None'){
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit_Type__c = null and Unit__r.Unit_Name__c like : strLikeVal order by Unit__r.Name limit 5000';
            }
            else {
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit_Type__c = : SelectedUnit and Unit__r.Unit_Name__c like : strLikeVal order by Unit__r.Name limit 5000';
            }
        }
        else {
            if(SelectedUnit == 'All'){
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit__r.Name like : strLikeVal order by Unit__r.Name limit 5000';
            }
            else if(SelectedUnit == 'None'){
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit_Type__c = null and Unit__r.Name like : strLikeVal order by Unit__r.Name limit 5000';
            }
            else {
                qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Unit_Type__c = : SelectedUnit and Unit__r.Name like : strLikeVal order by Unit__r.Name limit 5000';
            }
        }
        //queryResult = new List<Qualification_Unit__c>();
        queryResult = Database.query(qryString) ;
        queryList = new List<queryResultSet>();
        for(Qualification_Unit__c qu: queryResult){
            queryList.add(new queryResultSet(qu.Unit__r.Name, qu.Unit__r.Unit_Name__c, qu.Unit__r.Id));
        }
        if(SelectedSearch == 'Unit Name'){
            if(SelectedUnit == 'All'){
                qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Unit_of_Competency__r.Unit_Name__c like : strLikeVal order by Unit_of_Competency__r.Name limit 5000';
            }
            if(SelectedUnit == 'None'){
                qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Unit_of_Competency__r.Unit_Name__c like : strLikeVal order by Unit_of_Competency__r.Name limit 5000';
            }
            
        }
        else {
            if(SelectedUnit == 'All'){
                qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Unit_of_Competency__r.Name like : strLikeVal order by Unit_of_Competency__r.Name limit 5000';
            }
            if(SelectedUnit == 'None'){
                qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Unit_of_Competency__r.Name like : strLikeVal order by Unit_of_Competency__r.Name limit 5000';
            }
            
        }
        //qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Training_Package__c = : parentID order by Unit_of_Competency__r.Unit_Name__c';
        queryResult2 = new List<Training_Package_Unit__c>();
        system.debug('@queryResult.size()'+queryResult.size());
        system.debug('@queryResult'+queryResult);
        if(queryResult.size() < 0) 
            queryResult2 = Database.query(qryString);
        //if(queryResult2
        for(Training_Package_Unit__c qu: queryResult2){
            queryList.add(new queryResultSet(qu.Unit_of_Competency__r.Name, qu.Unit_of_Competency__r.Unit_Name__c, qu.Unit_of_Competency__r.Id));
        }
        List<queryResultSet> tempList = queryList;
        recordCount = tempList.size();
        System.debug('TEST::' + tempList);
        offsetVal=0;
        pages = recordCount/20;
        
        if (Math.mod(recordCount,20)>0) {
                pages++;
                
        }
        
        
        
        mapPages = new Map<Integer, List<queryResultSet>>();
        //offsetVal = 20;
        
        Integer numRec = 20;
        for(Integer i = 1; i<=pages; i++){
            if(i == pages) { 
                if(Math.mod(recordCount,20) > 0)
                numRec = Math.mod(recordCount,20);
                else
                numRec = 20;
            }
            List<queryResultSet> qr = new List<queryResultSet>();
            
                for(Integer j = offsetVal; j < offsetVal + numRec; j++){
                    
                    qr.add(tempList[j]);
                }
            
            
            mapPages.put(i, qr);
            offsetVal += 20;
        }
        
        currPage = 1;
        displayTable(currPage);
        }
        return null;
    }


    public Transient List<Qualification_Unit__c> queryResult { get; set; }
    public Transient List<Training_Package_Unit__c> queryResult2 { get; set; }
    public Transient List<queryResultSet> queryList { get; set; }
    public Map<Integer, List<queryResultSet>> mapPages ;
    public void query() {
        Id parentID = System.currentPageReference().getParameters().get('quid');
        
        system.debug('@parentID'+parentID);
        String qryString = 'SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Id = : parentID or Qualification__r.Id = : parentID or Qualification__r.Training_Package__c = : parentID order by Unit__r.Name';
        system.debug('@@@>>'+[SELECT Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c]);
        queryResult = Database.query(qryString) ; 
        //return null;
        queryList = new List<queryResultSet>();
        system.debug('@queryResult@'+queryResult);
        for(Qualification_Unit__c qu: queryResult){
           
            queryList.add(new queryResultSet(qu.Unit__r.Name, qu.Unit__r.Unit_Name__c, qu.Unit__r.Id));
        }
        
        qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Training_Package__c = : parentID order by Unit_of_Competency__r.Name';
        queryResult2 = Database.query(qryString) ;
        
        for(Training_Package_Unit__c qu: queryResult2){
            queryList.add(new queryResultSet(qu.Unit_of_Competency__r.Name, qu.Unit_of_Competency__r.Unit_Name__c, qu.Unit_of_Competency__r.Id));
        }
        List<queryResultSet> tempList = queryList;
        recordCount = tempList.size();
        System.debug('TEST::' + tempList);
        offsetVal=0;
        pages = recordCount/20;
        
        if (Math.mod(recordCount,20)>0) {
                pages++;
                
        }
        
        mapPages = new Map<Integer, List<queryResultSet>>();
        //offsetVal = 20;
        
        Integer numRec = 20;
        for(Integer i = 1; i<=pages; i++){
            if(i == pages) { 
                if(Math.mod(recordCount,20) > 0)
                numRec = Math.mod(recordCount,20);
                else
                numRec = 20;
            }
            List<queryResultSet> qr = new List<queryResultSet>();
            
                for(Integer j = offsetVal; j < offsetVal + numRec; j++){
                    
                    qr.add(tempList[j]);
                }
            
            
            mapPages.put(i, qr);
            offsetVal += 20;
        }
        
        currPage = 1;
        displayTable(currPage);
        
    }
    private void displayTable(Integer pNum){
        pageNo = 'Page ' + pNum + ' of ' + pages;
        queryList = mapPages.get(pNum);
        if((pNum == 1 && pages == 1)||(pages < 1)){
            pRender = false;
            nRender = false;
            if(pages < 1) pageNo = '';
        }
        else {
        if(pNum <= 1){ 
            pRender = false;
            nRender = true;
        } else {
            pRender = true;
        }     
        if(pNum >= pages) { 
            nRender = false;
            pRender = true;
        }
        }  
    }
    private Integer currPage{get;set;}
    public void previous(){
        
        
        
        currPage --;
        pageNo = 'Page ' + currPage;
        displayTable(currPage);
        
    }
    public void next(){
        
        
        currPage ++;
        pageNo = 'Page ' + currPage;
        displayTable(currPage);
            
    }
    public void query2() {
        Id parentID = System.currentPageReference().getParameters().get('quid');
        
        System.debug('TEST:::' + parentID);
        String qryString = 'SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c where Training_Package__c = : parentID order by Unit_of_Competency__r.Name limit 1400'; //limit 1100';
        queryResult2 = Database.query(qryString) ; 
        //return null;
        queryList = new List<queryResultSet>();
        for(Training_Package_Unit__c qu: queryResult2){
            queryList.add(new queryResultSet(qu.Unit_of_Competency__r.Name, qu.Unit_of_Competency__r.Unit_Name__c, qu.Unit_of_Competency__r.Id));
        }
        List<queryResultSet> tempList = queryList;
        recordCount = tempList.size();
        
        offsetVal=0;
        pages = recordCount/20;
        
        if (Math.mod(recordCount,20)>0) {
                pages++;
                
        }
        
        mapPages = new Map<Integer, List<queryResultSet>>();
        //offsetVal = 20;
        
        Integer numRec = 20;
        for(Integer i = 1; i<=pages; i++){
            if(i == pages) { 
                if(Math.mod(recordCount,20) > 0)
                numRec = Math.mod(recordCount,20);
                else
                numRec = 20;
            }
            List<queryResultSet> qr = new List<queryResultSet>();
            
                for(Integer j = offsetVal; j < offsetVal + numRec; j++){
                    
                    qr.add(tempList[j]);
                }
            
            
            mapPages.put(i, qr);
            offsetVal += 20;
        }
        
        currPage = 1;
        displayTable(currPage);
    }
    
    public treenodes() {
    
        //queryResult2 = Database.query('SELECT Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name FROM Training_Package_Unit__c order by Unit_of_Competency__r.Unit_Name__c') ;
    }



/* Wrapper class to contain the nodes and their children */
public class cNodes
{

 public List<Qualification__c> parent {get; set;}
 public Training_Package__c gparent {get;set;}

 public cNodes(Training_Package__c gp, List<Qualification__c> p)
 {
     parent = p;
     gparent = gp;
 }


}

 

Public List<cNodes> hierarchy;

Private List<Training_Package__c> tempparent = [Select Id,Name, Training_Package_Name__c from Training_Package__c order by Name];

Public List<cNodes> getmainnodes()
{
    hierarchy = new List<cNodes>();

    
    
    
    if(tempparent.size() > 0){
        Set<Id> pIds = new Set<Id>();
        items3 = new List<SelectOption>();
        Map<Id, Qualification__c> mapTree = new Map<Id, Qualification__c>();
        for(Training_Package__c tp : tempparent){
            pIds.add(tp.Id);
            items3.add(new SelectOption(tp.Training_Package_Name__c, tp.Training_Package_Name__c));
        }
        //List<Training_Package_Unit__c> tempchildren2 = [Select Training_Package__c, Unit_of_Competency__r.Name, Unit_of_Competency__r.Unit_Name__c from Training_Package_Unit__c where Training_Package__c in :pIds];
        List<Qualification__c> tempchildren = [Select q.Training_Package__c, q.Name, q.Id, q.Qualification_Name__c From Qualification__c q where q.Training_Package__c in :pIds];
        //[Select q.Training_Package__c, q.Name, q.Id, q.Qualification_Name__c, (Select Id, Name, Unit__r.Unit_Name__c, Unit__r.Name From Qualification_Units__r) From Qualification__c q where q.Training_Package__c in :pIds]
        
         for (Integer i =0; i< tempparent.size(); i++)
         {
            List<Qualification__c> qualList = new List<Qualification__c>();
            
            for(Integer j = 0; j< tempchildren.size(); j++){
                if(tempparent[i].Id == tempchildren[j].Training_Package__c){
                    qualList.add(tempchildren[j]);
                    
                } 
                  
                // hierarchy.add(new cNodes(tempparent[i],tempchildren[j]));
               // mapTree.put(tempparent[i], tempchildren[j])
             }
             
             hierarchy.add(new cNodes(tempparent[i],qualList));
             //mapTree.put(tempparent[i], )
         //List<Qualification__c> tempchildren = [Select q.Name, q.Id, q.Qualification_Name__c, (Select Id, Name, Unit__r.Unit_Name__c From Qualification_Units__r) From Qualification__c q where q.Training_Package__c = :tempparent[i].Id];
         //hierarchy.add(new cNodes(tempparent[i],tempchildren));
         } 
    }
      
    return hierarchy;
}   
public class queryResultSet
{
    public String cd {get;set;}
    public String un {get;set;}
    public String sfid {get;set;}
    public queryResultSet(String a, String b, Id c)
    {
        cd = a;
        un = b;
        sfid = c;
    }
}
}