public with sharing class BlackboardCreateMasterCourseController {

	private final Id master_course_rec_typeId = [Select Id From Recordtype Where DeveloperName = 'T1_Master_Course' AND sObjectType= 'Blackboard_Course__c'].Id;

	public List<Unit__c>unitsFromDB {get; private set;}

	public String searchString {get;set;}

	public boolean resultsblock {get;set;}
	public boolean noresultsblock {get;set;}

	public String temp_courseIdURL {get;set;}
	
	public BlackboardCreateMasterCourseController() {

		 resultsblock = false;
		 noresultsblock = false;
		
	}

	public pageReference search() {

		unitsFromDB = [Select Id, Name, Unit_Name__c From Unit__c WHERE name = : searchString];

		if(unitsFromDB.size() > 0) {
			resultsblock = true;
			noresultsblock = false;
		}
		else {
			noresultsblock = true;
			resultsblock = false;
		}

		return null;
	}

	public pageReference confirm() {

		Blackboard_Course__c master_course = new Blackboard_Course__c(
				RecordTypeId = master_course_rec_typeId,
				Unit_Code__c = unitsFromDB.get(0).Name,
				Unit_Name__c = unitsFromDB.get(0).Unit_Name__c

			);

		try {
			insert master_course;

			for(Unit__c unit : unitsFromDB)
				unit.Blackboard_Master_Course__c = master_course.Id;


			update unitsFromDB;
		} catch(Exception e){

			 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
			return null;
		}

		temp_courseIdURL = '/' + master_course.Id;
		
		return new PageReference('/apex/BlackBoardCreateMasterCourse2');
	}

	public pageReference cancel() {
		return new PageReference('/apex/BlackBoardCourseControl');
	}
}