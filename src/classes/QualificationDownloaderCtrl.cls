public with sharing class QualificationDownloaderCtrl{

    public Integer pageSize = 20;
    public Integer pageNumber = 0;
    private List<TC> tcCache;
    
    public Integer getPageNumber() { return pageNumber; }

    public class Change{
        public String changeType{get;set;}
        public String oldValue{get;set;}
        public String newValue{get;set;}
    }
    public class TC {
        private boolean ivalue = false;
        public boolean isUpgrade { get; set; }
        public boolean i{
            get{
                return ivalue && (isUpgrade == null || !isUpgrade);
            }
            set{
                ivalue = value;
            }
        }
        public Training_Component__c p{get;set;}
        public List<Change> changes{get;set;}
        
        public string className {
            get{
                if( p.Training_Package__c != null){
                    if ( p.imported__c == p.updated__c )
                        return 'imported';
                    else if ( p.updated__c >= system.today() - 30 )
                        return 'changed';
                    return 'linked';
                }
                
                return '';
            }
        }
    }

    public PageReference nextPage(){
        pageNumber++;
        tcCache = null;
        return null;
    }

    public PageReference previousPage(){
        pageNumber--;
        tcCache = null;
        return null;
    }
    
    public List<TC> qualifications {
        get{
            if ( tcCache == null ){
                Integer offset = pageNumber * pageSize;
                Training_Component__c[] r = [ 
                    select 
                        Code__c,
                        Title__c,
                        Updated__c,
                        Qualification__c,
                        Parent_Code__c
                    from Training_Component__c
                    Where Component_Type__c = 'Qualification'
                    Order By Code__c
                    LIMIT :pageSize
                    OFFSET :offset
                ];
                List<TC> ret = new List<TC>();
                for (Training_Component__c c : r ){
                    if ( c.Qualification__c == null ) continue;
                    TC p = new TC();
                    p.p = c;
                    ret.add(p);
                }
                for (Training_Component__c c : r ){
                    if ( c.Qualification__c != null ) continue;
                    TC p = new TC();
                    p.p = c;
                    ret.add(p);
                }
                tcCache = ret;
            }
            return tcCache;
        }    
    }
    
    public PageReference importQualifications(){
        List<Qualification__c> qq = new List<Qualification__c>();
        List<Training_Component__c> toUpdate = new List<Training_Component__c>();
        
        //fetch parents
        Set<String> codes = new Set<String>();
        for ( TC tp : qualifications ){
            if ( tp.i && tp.p.Parent_Code__c != null ){
                codes.add(tp.p.Parent_Code__c);
            }
        }
        Map<String, Training_Package__c> parents = new Map<String, Training_Package__c>();
        for ( Training_Package__c tp : [ Select Id, Name From Training_Package__c Where Name in :codes ] ){
            parents.put(tp.Name, tp);
        }
        
        
        for ( TC tp : qualifications ){
            if ( tp.i ){
                system.assert( tp.p.Qualification__c == null, 'updating qualifications is not supported yet');
                
                Qualification__c q = new Qualification__c();
                q.name = tp.p.Code__c;
                q.Training_Package__c = parents.get(tp.p.Parent_Code__c).id;
                q.Qualification_Name__c = tp.p.Title__c;
                qq.add(q);
                
                tp.p.Qualification__c = q.id;
                tp.p.Qualification__r = q;
                toUpdate.add(tp.p);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, tp.p.Code__c + ' was created'));
            }
        }
        insert qq;
        update toUpdate;
        
        tcCache = null;
        return null;
    }
    
    

}