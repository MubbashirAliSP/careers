/**
 * @description Test class for `SEC_ListViewCC`
 * @author Ranyel Maliwanag
 * @date 03.MAR.2016
 */
@isTest
public with sharing class SEC_ListViewCC_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.Assigned_Trainer__c = UserInfo.getUserId();
		insert enrolment;
	}

	static testMethod void accessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_Container__mdt);
		queryString += 'WHERE DeveloperName = \'MyStudents_General\'';
		SEC_Container__mdt container = Database.query(queryString);

		queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_ListView__mdt);
		queryString += 'WHERE SEC_Container__c = \'' + container.DeveloperName + '\'';
		List<SEC_ListView__mdt> listViews = Database.query(queryString);
		Id currentListViewId;
		for (SEC_ListView__mdt listView : listViews) {
			if (listView.DeveloperName == 'MyStudents_General_MyStudents') {
				currentListViewId = listView.Id;
			}
		}
		Test.setCurrentPage(Page.StudentEngagementConsole);
		ApexPages.currentPage().getParameters().put('lid', currentListViewId);
		SEC_ListViewCC controller = new SEC_ListViewCC();


		Test.startTest();
			controller.currentTrainer = UserInfo.getUserId();
			controller.container = container;
			controller.listViews = listViews;

			System.debug(controller.oldTab);
			System.debug(controller.isMultipleListViews);
			System.debug(controller.objectName);
			System.debug(controller.fieldsetMembers);
			System.debug(controller.newListView);
			System.debug('CLVR ::: ' + controller.currentListViewRecords);
			System.debug(controller.recordsMap);
			System.debug(controller.resultSize);
			System.debug(controller.currentPageRecords);
			System.debug(controller.isCategorised);
			System.debug(controller.recordCategories);
			System.debug(controller.newTask);
			System.debug(controller.taskWhatId);
			System.debug(controller.taskWhoId);
			System.debug(controller.accountId);
			System.debug(controller.defaultCategory);
			System.debug(controller.isCallModalVisible);
			System.debug(controller.isCategoryModalVisible);
			System.debug(controller.accountForCategorisation);
			System.debug(controller.referenceEnrolment);
			System.debug(controller.enrolmentId);
			System.debug(controller.debugger);
			System.debug(controller.sortingField);
			System.debug(controller.sortAscending);
			System.debug(controller.sortedList);
			System.debug(controller.currentPageUrl);
		Test.stopTest();
	}


	static testMethod void methodsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_Container__mdt);
		queryString += 'WHERE DeveloperName = \'MyStudents_General\'';
		SEC_Container__mdt container = Database.query(queryString);

		queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_ListView__mdt);
		queryString += 'WHERE SEC_Container__c = \'' + container.DeveloperName + '\'';
		List<SEC_ListView__mdt> listViews = Database.query(queryString);
		SEC_ListViewCC controller = new SEC_ListViewCC();

		Account student = [SELECT Id FROM Account];

		Test.startTest();
			controller.container = container;
			controller.listViews = listViews;
			controller.accountId = student.Id;

			controller.initialiseAccountCategory();
			controller.initialiseLogCall();
			controller.switchListViews();
			controller.resetCategoryModal();
			controller.sortBy();
		Test.stopTest();
	}
}