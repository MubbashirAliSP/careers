global class NATGeneratorBatchEUCollection implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String functionquery;
    global final String query;
    global String currentState;
    global String libraryId; 
    global String body60;
    global String LogId;
    global Map<Id,Decimal> unitNomHrsMap = new Map<Id,Decimal>();
    global Set<Id> qualSet = new Set<Id>();
    global Set<Id> locSet = new Set<Id>();
    global Set<Id> euSet = new Set<Id>();
    global set<Id> enrlSet = new Set<Id>();
    global DateTime currentDT;
    global NAT_Validation_Log__c natLog;

    global NATGeneratorBatchEUCollection(String state, String q, String LId ) {
        query = q;
        LogId = LId;
        currentState = state;
        
        natLog = [ SELECT Id, Location_Ids__c, Delete_Existing_NAT_files__c, Log_Run_At__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];

        currentDT = natLog.Log_Run_At__c;
    }
        
    global database.querylocator start(Database.BatchableContext BC) {
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        for(SObject s : scope) {
            Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
            
            euSet.add(eu.Id);
            
            enrlSet.add(eu.Enrolment__c);
            
            qualSet.add(eu.Enrolment__r.Qualification__r.Parent_Qualification_Id__c);
            
            locSet.add(eu.Training_Delivery_Location__c);

            List<Id> unitName = new List<Id>();
            if (currentState == 'Victoria') {
                for ( integer i = 1 ; i <= eu.Supersession_Count__c ; i++ ) {
                    String supersededUnitId = 'Superseded_' + i + '_Unit_SFID__c';  
                    unitName.add(String.valueOf(eu.get(supersededUnitId)));
                }
                if(unitName.size() > 0) {
                    List<Unit__c> supersededUnits = [SELECT Id, Name FROM Unit__c WHERE Id IN :unitName];
                    for (Unit__c u : supersededUnits) {
                        unitNomHrsMap.put(u.Id, 0.0);
                    }
                }
            }

            if (eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null) {
                unitNomHrsMap.put(eu.Unit__c, 0.0);
            }
            else {
                unitNomHrsMap.put(eu.Unit__c, eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c);
            }                    
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        
        natLog.Location_Ids__c = String.valueOf(qualSet);
        
        update natLog;
        if(currentState != 'New South Wales APL') {
            Database.executeBatch(new NATGeneratorBatch00020_2( currentState, locSet, LogId  ) );
        	Database.executeBatch(new NATGeneratorBatch00030_2( currentState, qualSet, LogId  ) );  
        }
        
        Database.executeBatch(new NATGeneratorBatch00060_2( currentState, unitNomHrsMap, LogId  ) );

    }
}