/**
 * @description Dual Enrolment object Trigger Handler
 * @author      Sairah Hadjinoor
 * @Company     CloudSherpas
 * @date        01.DEC.2014
 *
 * HISTORY
 * - 01.DEC.2014    Sairah Hadjinoor      Created.
 */
public with sharing class DualEnrolmentTriggerHandler {

	//start onBeforeInsert Method
	//uncomment this just in case we need an before insert event
	/*public static void onBeforeInsert(List<Dual_Enrolment__c> newRecords) {
         
    }// End onBeforeInsert Method*/

    //start onAfterInsert Method
    public static void onAfterInsert(List<Dual_Enrolment__c> newRecords) {
    	Set<Id> enrolmentIdSet = new Set<Id>();
        for(Dual_Enrolment__c dual: newRecords){
        	//get all associated enrolments to be updated
        	if(dual.Enrolment_1__c!=null){
        		enrolmentIdSet.add(dual.Enrolment_1__c);
        	}
        	if(dual.Enrolment_2__c!=null){
	        	enrolmentIdSet.add(dual.Enrolment_2__c);
	        }
        }
        if(!enrolmentIdSet.isEmpty()){
        	updateEnrolment(enrolmentIdSet); 
        }
    }// End onAfterInsert Method

    
    
    //uncomment this just in case we need an before  update event
    /*public static void onBeforeUpdate(List<USI_Identification_Document__c> oldRecords,List<USI_Identification_Document__c> newRecords, Map<Id,USI_Identification_Document__c> oldRecordsMap) {
         
    }
    // End onBeforeUpdate Method
	*/
	//uncomment this just in case we need a after update event
	/*
    public static void onAfterUpdate(List<Dual_Enrolment__c> oldRecords,List<Dual_Enrolment__c> newRecords, Map<Id,Dual_Enrolment__c> oldRecordsMap) {
        
    }*/
    // End onAfterUpdate Method

    //uncomment this just in case we need a before delete event
    /*public static void onBeforeDelete(List<USI_Identification_Document__c> oldRecords,Map<Id,USI_Identification_Document__c> oldRecordsMap) {
         
    }// End onBeforeDelete Method
	*/
    public static void onAfterDelete(List<Dual_Enrolment__c> oldRecords,Map<Id,Dual_Enrolment__c> oldRecordsMap) {
    	Set<Id> enrolmentIdSet = new Set<Id>();
    	for(Dual_Enrolment__c dual: oldRecords){
        	//get all associated enrolments to be updated
        	if(dual.Enrolment_1__c!=null){
        		enrolmentIdSet.add(dual.Enrolment_1__c);
        	}
        	if(dual.Enrolment_2__c!=null){
	        	enrolmentIdSet.add(dual.Enrolment_2__c);
	        }
        }
        if(!enrolmentIdSet.isEmpty()){
        	updateLinkedEnrolment(enrolmentIdSet);
        }
    }
    // End onAfterDelete Method

    public static void updateEnrolment(Set<Id> enrolmentIdsSet){
    	/**
		 * @description When an Dual Enrolment is added or updated,associated Enrolment's 
		 *				'Linked Enrolment' field must be checked
		 * @author      Sairah Hadjinoor
		 * @Company     CloudSherpas
		 * @date        01.DEC.2014
		 *
		 * HISTORY
		 * - 01.DEC.2014    Sairah Hadjinoor      Created.
		**/
		List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
		for(Enrolment__c enrol: [Select Id,Linked_Enrolment__c from Enrolment__c where Id IN: enrolmentIdsSet]){
			if(!enrol.Linked_Enrolment__c){
				enrol.Linked_Enrolment__c = true;
				enrolmentList.add(enrol);
			}
		}

		if(!enrolmentList.isEmpty()){
			update enrolmentList;
		}
    }
    public static void updateLinkedEnrolment(Set<Id> enrolmentIdsSet){
    	/**
		 * @description When an Dual Enrolment is deleted,associated Enrolment's 
		 *				'Linked Enrolment' field must be set to false
		 * @author      Sairah Hadjinoor
		 * @Company     CloudSherpas
		 * @date        01.DEC.2014
		 *
		 * HISTORY
		 * - 01.DEC.2014    Sairah Hadjinoor      Created.
		**/
		List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
		for(Enrolment__c enrol: [Select Id,Linked_Enrolment__c from Enrolment__c where Id IN: enrolmentIdsSet]){
			if(enrol.Linked_Enrolment__c){
				enrol.Linked_Enrolment__c = false;
				enrolmentList.add(enrol);
			}
		}

		if(!enrolmentList.isEmpty()){
			update enrolmentList;
		}
    }
}