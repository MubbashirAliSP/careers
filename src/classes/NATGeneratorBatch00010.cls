/*
*HISTORY
*JUAN MIGUEL DE GUIA 7/31/2015 ADDED TRY CATCH TO CATCH ERROR DURING TEST CASES
*/
global class NATGeneratorBatch00010 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global String body10;
    
    private String currentState;
    private String libraryId;
    private String logId;
    
   // global NATGeneratorBatch00010(String state, String tId) {
    
    global NATGeneratorBatch00010(String state, String tId, String LId) {
        currentState = state;
        
        /*query = 'SELECT Id, AVETMISS_Organisation_Name__c, Training_Org_Identifier__c, Training_Org_Type_Identifier__c, BillingStreet, National_Provider_Number__c, ' + 
                'BillingCity, BillingPostalCode, BillingState, State_Identifier__c, Name, Software_Product_Name__c,Software_Vendor_Email_Address__c ' + 
                'FROM Account */
        
        Query  = 'SELECT Phone,FirstName,LastName, MailingStreet,MailingState, MailingPostalCode,MailingCountry,MailingCity,Id, Fax,Email,' +
                 'Account.AVETMISS_Organisation_Name__c, Account.Training_Org_Type_Identifier__c, Account.Training_Org_Identifier__c, ' +
                 'Account.BillingCity, Account.BillingPostalCode, Account.BillingState, Account.State_Identifier__c, Account.Name,' +
                 'Account.Software_Product_Name__c, Account.Software_Vendor_Email_Address__c, Account.BillingStreet, Account.National_Provider_Number__c ' +
                 'FROM Contact WHERE Account.Id =\'' + tId + '\' LIMIT 1'; 
                 
        logId = LId;
    }

    global database.querylocator start(Database.BatchableContext BC)
    {
        body10 = '';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        system.debug(scope);
            
                if (currentState == 'Queensland') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionQLD.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if (currentState == 'New South Wales') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionNSW.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if (currentState == 'Australian Capital Territory') {
                    try {
                        libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files ACT' LIMIT 1].Id;
                        for(SObject s :scope) {
                            body10 += NATConstructor.natFile(s, NATDataDefinitionACT.nat00010(), currentState);
                            body10 += '\r\n';
                        }
                    }
                    catch(System.QueryException e){
                        System.debug('Error!!');
                    }
                    
                }
                else if(currentState == 'Victoria') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionVIC.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if (currentState == 'South Australia') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionSA.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if (currentState == 'Western Australia') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files WA' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionWA.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if(currentState == 'Northern Territory') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NT' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionNT.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
                else if(currentState == 'Tasmania') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files TAS' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinitionTAS.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
        	else if(currentState == 'National') {
                    libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files National' LIMIT 1].Id;
                    for(SObject s :scope) {
                        body10 += NATConstructor.natFile(s, NATDataDefinition.nat00010(), currentState);
                        body10 += '\r\n';
                    }
                }
            } 
       
    global void finish(Database.BatchableContext BC) {
        
        if(body10 == '') {
            body10 = ' ';
        }
        
        Blob pBlob10 = Blob.valueof(body10);
            insert new ContentVersion(
                versionData =  pBlob10,
                Title = 'NAT00010',
                PathOnClient = '/NAT00010.txt',
                FirstPublishLocationId = libraryId
            );
        
         NAT_Validation_Log__c natLog = [ SELECT Id, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
            
         //Database.executeBatch(new NATGeneratorBatch00020( natLog.Validation_State__c, natLog.Training_Organisation_Id__c, natLog.Query_Main__c, LogId ) ); 
        Database.executeBatch(new NATGeneratorBatchEUCollection( natLog.Validation_State__c, natLog.Query_Main__c, LogId ) ); 

    }
}