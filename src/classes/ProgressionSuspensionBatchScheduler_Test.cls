/**
 * @description Test class for `ProgressionSuspensionBatchScheduler` class
 * @author Biao Zhang
 * @date 25.NOV.2015
 */
@istest
public with sharing class ProgressionSuspensionBatchScheduler_Test {
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Progression Suspension Batch Scheduler Test', schedule, new ProgressionSuspensionBatchScheduler());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}