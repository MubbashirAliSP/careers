/**
 * @description 	Awards Object Trigger handler
 * @author 			N/A
 * @date 			N/A
 * @history
 * 		20.JAN.2016		Biao Zhang			Create Award Units when Award is approved
 */
 public with sharing class AwardsTriggerHandler {
 	/**
     * @description Handles after update operations for the Award object
     * @author Biao Zhang
     * @date 20.JAN.2016
     */
    public static void onAfterUpdate(List<Awards__c> awards, Map<Id, Awards__c> oldAwardMap) {
    	Id draftRTID = Schema.SObjectType.Awards__c.getRecordTypeInfosByName().get('Draft Award').getRecordTypeId();
        Id approvedRTID = Schema.SObjectType.Awards__c.getRecordTypeInfosByName().get('Approved Award').getRecordTypeId();

        List<Awards__c> processAwards = new List<Awards__c>();
    	for(Awards__c award : awards) {
    		if(award.RecordTypeId != oldAwardMap.get(award.Id).RecordTypeId && award.RecordTypeId == approvedRTID) {
    			processAwards.add(award);
    		}
    	}

    	createAwardUnit(processAwards);
    }

	public static void populateReportForAvetmiss(List<Awards__c> awardList){
		Set<Id> enrolmentIds = new Set<Id>();
		List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
		for(Awards__c a : awardList){
			enrolmentIds.add(a.Enrolment__c);
		}
		enrolmentList = [SELECT id, RecordType.Name FROM Enrolment__c WHERE Id IN: enrolmentIds];
		for(Awards__c a : awardList){
			for(Enrolment__c e : enrolmentList){
				if(a.Enrolment__c == e.Id){
					if(e.RecordType.Name == 'Dual Funded Enrolment' || e.RecordType.Name == 'Standard Enrolment'){
						a.Collect_for_AVETMISS__c = true;
					}
				}
			}
		}
	}
	/*
		This method will avoid 2 certificate awards on Enrolment
	*/
	public static void avoidDuplicateCertificate(List<Awards__c> awardList){
		Set<Id> eIds = new Set<Id>();
		List<Awards__c> awards = new List<Awards__c>();
		Map<Id, Awards__c> awardsMap = new Map<Id, Awards__c>();
		for(Awards__c a : awardList){
			eIds.add(a.Enrolment__c);
		}
		awards = [select Id, Enrolment__c FROM Awards__c where Enrolment__c IN: eIds and Award_Type__c = 'Certificate'];
		for(Awards__c a : awards){
				awardsMap.put(a.Enrolment__c, a);
		}
		for(Awards__c a : awardList){
			if(awardsMap.containsKey(a.Enrolment__c) && a.Award_Type__c == 'Certificate'){
				a.addError('You cannot create 2 Award Certificates for this Enrolment.');
			}
		}
	}

	/**
     * @description create award units
     * @author Biao Zhang
     * @date 20.JAN.2016
     */
	public static void createAwardUnit(List<Awards__c> awards) {
		Map<Id, List<Enrolment_Unit__c>> enrolmentToEnrolmentUnitMapping = new Map<Id, List<Enrolment_Unit__c>>();
		for(Awards__c award : awards) {
			enrolmentToEnrolmentUnitMapping.put(award.Enrolment__c, new List<Enrolment_Unit__c>());
		}

		List<Enrolment_Unit__c> eus = [Select 
											Id, 
											Enrolment__c 
										from 
											Enrolment_Unit__c 
										where 
											Enrolment__c in: enrolmentToEnrolmentUnitMapping.keySet() 
										and 
											(AVETMISS_National_Outcome__c = '20' or AVETMISS_National_Outcome__c = '51' or AVETMISS_National_Outcome__c = '60')
										];

		for(Enrolment_Unit__c eu : eus) {
			enrolmentToEnrolmentUnitMapping.get(eu.Enrolment__c).add(eu);
		}

		List<AwardUnit__c> awardUnits = new List<AwardUnit__c>();
		for(Awards__c award : awards) {
			for(Enrolment_Unit__c eu : enrolmentToEnrolmentUnitMapping.get(award.Enrolment__c)) {
				AwardUnit__c au = new AwardUnit__c(
					EnrolmentUnitId__c = eu.Id,
					AwardId__c = award.Id
				);
				awardUnits.add(au);
			}
		}

		insert awardUnits;
	}
}