/**
 * @description Schedulable interface implementation for the CensusInvoiceGeneration batch
 * @author Ranyel Maliwanag
 * @date 16.NOV.2015
 */
public without sharing class CensusInvoiceGenerationScheduler implements Schedulable {
	public CensusInvoiceGenerationBatch batchProcess;

    public CensusInvoiceGenerationScheduler() {
        batchProcess = new CensusInvoiceGenerationBatch();
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new CensusInvoiceGenerationBatch());
    }
}