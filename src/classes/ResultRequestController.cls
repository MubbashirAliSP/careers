/*Result request allows users to submit a result request
 *that needs to be verified and approved before Enrolment Unit or
 *Enrolment Unit Of Study gets resulted
 *in other words results request is an object that holds result requests
 *that needs to be approved, instead of directly resulting EU or EUOS
 *There is additional functionality that bypasses Result Request object to create
 *new result request. Intead, result request is requested from Enrolment, which bypasses
 *the first page of the process
 *created by Yuri Gribanov - CloudSherpas 31 July 2014 */

public with sharing class ResultRequestController {

	public String searchString {get;set;}
    public Boolean searchActive {get;set;}

    public List<Enrolment__c> enrolments {get;set;}

    public String selEnrolmentId {get;set;}
    public String enrlState {get;set;}

    public List<Results__c> resultsList {get; private set;}
    public List<Completion_Status_Type__c> completionTypesList {get; private set;}

    public List<InnerWrapper> euList {get;set;}
    public List<InnerWrapper> euosList {get;set;}


	public ResultRequestController() {

		euList = new List<InnerWrapper>();
		euosList = new List<InnerWrapper>();
		
	}

	/*search enrolments */
	public pageReference dosearch() {

		enrolments = new List<Enrolment__c>();

		enrolments = [Select Student__r.Name, 
							 Id,
		                     Name, 
		                     Student__r.Student_Identifer__c,
		                     Student__r.PersonBirthDate, 
		                     Qualification__r.Name,
		                     Qualification__r.Id, 
		                     Start_Date__c,
		                     End_Date__c,
		                     Student__r.Id,
		                     Delivery_Location__r.Child_State__c

		              
		              From Enrolment__c 

		              Where Student__r.Name LIKE : searchString
		                            OR Name = : searchString
		                            OR Student__r.Student_Identifer__c = : searchString];

		return null;
	}

	/*Once enrolment has been selected, we need to populate
	 *Enrolment Unit list and Enrolment Unit Of Study list
	 *Also we are using Result codes based on the state of the enrolment
	 *If this function is initiated from Enrolment record, we just pass
	 *Enrolment and Enrolment State directly */

	public pageReference next() {


			resultsList = [Select 
									Name, 
									State__r.Name ,
									Inactive__c, 
									Result_Code__c,
									National_Outcome_Code__c,
									Use_Information__c 

									From Results__c 

									Where Inactive__c = false AND state__r.Name = : ApexPages.currentPage().getParameters().get('Estate')

									ORDER BY Result_Code__c, Name];


			completionTypesList = [Select Name, ID, Code__c From Completion_Status_Type__c ORDER BY Code__c];

			/*Create SelectOption List per State/Territory for results */

			List<SelectOption>qldResults = new List<SelectOption>();
			List<SelectOption>nswResults = new List<SelectOption>();
			List<SelectOption>vicResults = new List<SelectOption>();
			List<SelectOption>saResults = new List<SelectOption>();
			List<SelectOption>waResults = new List<SelectOption>();
			List<SelectOption>actResults = new List<SelectOption>();
			List<SelectOption>ntResults = new List<SelectOption>();
			List<SelectOption>euosResults = new List<SelectOption>();

				for(Completion_Status_Type__c results : [Select Name, Code__c FROM Completion_Status_Type__c ORDER BY Code__c LIMIT 999]) { //Limit number of results returned 
					EUOSResults.add(new SelectOption(results.Id,results.Name +' - '+results.Code__c ));
					}

			Map<String, List<SelectOption>> resultsMap = new Map<String, List<SelectOption>>();
			
			for(Results__c results : [Select 
											 Name, 
											 State__r.Name, 
											 Result_Code__c 

									  From Results__c 

								      Where Inactive__c = false

									  ORDER BY Result_Code__c 

									  LIMIT 999]) { //Limit number of results returned 

					if(results.State__r.Name == 'Queensland')
						qldResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c ));
					if(results.State__r.Name == 'New South Wales')
						nswResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));
					if(results.State__r.Name == 'Victoria')
						vicResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));
					if(results.State__r.Name == 'South Australia')
						saResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));
					if(results.State__r.Name == 'Western Australia')
						waResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));
					if(results.State__r.Name == 'Australian Capital Territory')
						actResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));
					if(results.State__r.Name == 'Northern Territory')
						ntResults.add(new SelectOption(results.Id, results.Name +' - '+results.Result_Code__c));



			}

			resultsMap.put('Queensland', qldResults);
			resultsMap.put('New South Wales', nswResults);
			resultsMap.put('Victoria', vicResults);
			resultsMap.put('South Australia', saResults);
			resultsMap.put('Western Australia', waResults);
			resultsMap.put('Australian Capital Territory', actResults);
			resultsMap.put('Northern Territory', ntResults);
			
			for(Enrolment_Unit__c eu : [Select 

										Name, 
										Enrolment__c,
				                        Unit__r.Unit_Name__c,
				                        Unit_Name__c,
				                        Unit_Result__c,
				                        Unit__r.Name,
				                        Start_Date__c, 
				                        End_Date__c, 
				                        Delivery_Mode__r.Name, 
				                        Delivery_Mode__c,
				                        Training_Delivery_Location__r.Name,
				                        Training_Delivery_Location__r.Parent_Location__r.State_LookUp__r.Name,
				                        Unit_Result__r.Result_Code__c

				                        From Enrolment_Unit__c 

				                        Where (Unit_Result__r.Result_Code__c = '70' OR Unit_Result__r.Result_Code__c = '90' OR Unit_Result__c = null) AND Enrolment__c = : ApexPages.currentPage().getParameters().get('EId')]) {

				


				euList.add(New InnerWrapper(false,eu,resultsMap.get(eu.Training_Delivery_Location__r.Parent_Location__r.State_LookUp__r.Name)));

				
			}

			for(Enrolment_Unit_Of_Study__c euos : [Select 
													
													Name,
													Unit_Name__c,
													Enrolment_Unit__c,
													Result__c,
													Parent_UOC__c,
													Census_Date__c, 
													Total_amount_charged__c,
													Amount_paid_upfront__c,
													Unit_of_study_HELP_Debt_UI__c,
													Student_Status_Code__c,
													Unit_of_Study_Code__c,
													Result__r.Code__c,
													Enrolment_Unit__r.Enrolment__c

												   	From Enrolment_Unit_Of_Study__c 

													Where (Result__r.Code__c != '3' AND Result__r.Code__c != '5') AND Enrolment_Unit__r.Enrolment__c = : ApexPages.currentPage().getParameters().get('EId')])

				euosList.add(New InnerWrapper(false,euos,euosResults));

			return new PageReference('/apex/ResultRequest2');
		

		
	}

	public pageReference cancel1() {

		return new PageReference('/apex/ResultRequestTab');
	}

	public pageReference back1() {
		return new pageReference('apex/ResultRequest');
	}

	/*Controls save button
	 *calls RR insert method
	*/
	public pageReference save() {

		try {

			if(insertResultRequests().size() == 0) {
				apexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must select at least 1 record for approval'));
				return null;
			}

			insertResultRequests();
			

		}
		catch(Exception e) {
			apexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
			return null;

		}

		return new PageReference('/apex/ResultRequestTab');

	}

	/*save and submit */
	public pageReference submit() {



		try {
			//Insert result request records
			List<Result_Request__c>resultRequestList = insertResultRequests();

			if(resultRequestList.size() == 0) {
				apexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must select at least 1 record for approval'));
				return null;
			}
			//Send result request records for approval
			for(Result_Request__c rr : resultRequestList)
				submitForApproval(rr.Id);
		} catch(Exception e) {
			apexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
			return null;
		}

		return new PageReference('/apex/ResultRequestTab');
		
	}

	/*This method is used to send multiple Result Request records
	 *into the approval process.
	 *RecordId is passed for processing
	 */
	public void submitForApproval(Id RecordId) {

		 // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(recordId);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        
        
	}

	/*inserts RR into the database */
	public List<Result_Request__c> insertResultRequests() {

		List<Result_Request__c> resultRequestList = new List<Result_Request__c>();

		for(InnerWrapper w : euList) {

			if(w.sel) {

			Result_Request__c rr = new Result_Request__c(

				Enrolment_Unit__c = w.dynamicObject.Id,
				Enrolment_Unit_Result__c = (Id) w.dynamicObject.get('Unit_Result__c'),
				Enrolment_Record__c = (Id) w.dynamicObject.get('Enrolment__c'),
				End_Date_For_Approval__c = w.EuTemp.End_Date__c

			);

			resultRequestList.add(rr);

		   }
		}

		for(InnerWrapper w : euosList) {

			if(w.sel) {

			Result_Request__c rr = new Result_Request__c(

				Enrolment_Unit_of_Study__c = w.dynamicObject.Id,
				Enrolment_Unit__c = (Id) w.dynamicObject.get('enrolment_unit__c'),
				Enrolment_Unit_of_Study_Result__c = (Id) w.dynamicObject.get('Result__c'),
				Enrolment_Record__c = (Id) w.dynamicObject.get('Enrolment_Unit__r.Enrolment__c')


				

			

			);

			resultRequestList.add(rr);

		   }
		}
		   //Insert result request records
		   insert resultRequestList;

		   return resultRequestList;
			
	}

	
	/*Wrapper class using dynamic object */
	public class InnerWrapper {

		public boolean sel {get;set;}
		public SObject dynamicObject {get;set;}
		public List<SelectOption> results {get;set;}
		public Enrolment_Unit__c euTemp {get;set;}

		public InnerWrapper(boolean selected, SObject o, List<SelectOption> res) {

			sel = selected;
			dynamicObject = o;
			results = res;

			if(o.getSObjectType().getDescribe().getName() == 'Enrolment_Unit__c') {

				euTemp = (Enrolment_Unit__c)o;


			}

		}
	}
}