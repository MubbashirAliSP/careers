/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for FundingResourceTriggerHandler Class and FundingSourceAIAU Trigger
Test Class:
History
19/12/2012     	Created     Sairah Hadjinoor
05/03/2013		Updated		Janella Canlas
------------------------------------------------------------*/
@isTest
private class FundingResourceTriggerHandlerTest {

    /*
    	Test FundingSourceTriggerHandler
    */
    static testMethod void testFundingSourceTriggerHandler() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='FS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Purchasing_Contracts__c pCon2 = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Funding_Source__c fSource2 = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Contract_Line_Items__c conLine2= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit3 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            	acc = TestCoverageUtilityClass.createStudent();
	            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
	            anz = TestCoverageUtilityClass.createANZSCO();
	            fld = TestCoverageUtilityClass.createFieldOfEducation();
	            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
	            unit = TestCoverageUtilityClass.createUnit();
	            unit2 = TestCoverageUtilityClass.createUnit();
	            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
	            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
	            country = TestCoverageUtilityClass.createCountry(); 
	            countryAU = TestCoverageUtilityClass.queryAustralia();
	            state = TestCoverageUtilityClass.createState(country.Id); 
		        state2.Name = 'Queensland';
		        state2.Short_Name__c = 'QLD';
		        state2.Code__c = '01';
		        state2.Country__c = countryAU.Id;
		        insert state2;
	            //state2 = TestCoverageUtilityClass.createState(countryAU.Id); 
	            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
	            Id fRecType = [select Id from RecordType where Name=:'State Funding Source' and sObjectType = 'Funding_Source__c'].Id;
	            fSource2.RecordTypeId = fRecType;
	            fSource2.Fund_Source_Name__c = 'test Source';
		        fSource2.Code__c = '23341';
		        fSource2.State__c = state2.Id;
		        insert fSource2; 
	            
	            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource2.Id);
	            pCon.State_Fund_Source__c = fSource.Id;
	            update pCon;
	            //pCon2 = TestCoverageUtilityClass.createPurchasingContract(fSource2.Id);
	            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
	            //conLine2 = TestCoverageUtilityClass.createContractLineItem(pCon2.Id);  
	            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
	            //lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine2.Id);
	            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
	            //lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem2.id, qunit2.id);
	            lineItemUnit3 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit2.id);
	            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
	            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
	            loc = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
	            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
	            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id);
            	uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
	            uhp2 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
	            fSource.Fund_Source_Name__c = 'test source name';
	            fSource.State__c = state2.id;
	            update fSource;
	            fSource2.Fund_Source_Name__c = 'test source name';
	            fSource2.State__c = state.id;
	            update fSource2;
            
            test.stopTest();
        }
    }
    static testMethod void testFundingSourceTriggerHandler2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='FS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Purchasing_Contracts__c pCon2 = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Funding_Source__c fSource2 = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Contract_Line_Items__c conLine2= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit2 = new Line_Item_Qualification_Units__c ();
        Line_Item_Qualification_Units__c lineItemUnit3 = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            	acc = TestCoverageUtilityClass.createStudent();
	            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
	            anz = TestCoverageUtilityClass.createANZSCO();
	            fld = TestCoverageUtilityClass.createFieldOfEducation();
	            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
	            unit = TestCoverageUtilityClass.createUnit();
	            unit2 = TestCoverageUtilityClass.createUnit();
	            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
	            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
	            country = TestCoverageUtilityClass.createCountry(); 
	            countryAU = TestCoverageUtilityClass.queryAustralia();
	            state = TestCoverageUtilityClass.createState(country.Id); 
		        state2.Name = 'Queensland';
		        state2.Short_Name__c = 'QLD';
		        state2.Code__c = '01';
		        state2.Country__c = countryAU.Id;
		        insert state2;
	            //state2 = TestCoverageUtilityClass.createState(countryAU.Id); 
	            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
	            Id fRecType = [select Id from RecordType where Name=:'State Funding Source' and sObjectType = 'Funding_Source__c'].Id;
	            fSource2.RecordTypeId = fRecType;
	            fSource2.Fund_Source_Name__c = 'test Source';
		        fSource2.Code__c = '23341';
		        fSource2.State__c = state2.Id;
		        insert fSource2; 
	            
	            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource2.Id);
	            //pCon2 = TestCoverageUtilityClass.createPurchasingContract(fSource2.Id);
	            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
	            //conLine2 = TestCoverageUtilityClass.createContractLineItem(pCon2.Id);  
	            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
	            //lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine2.Id);
	            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
	            //lineItemUnit2 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem2.id, qunit2.id);
	            lineItemUnit3 = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit2.id);
	            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
	            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
	            loc = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
	            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
	            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id);
            	uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
	            uhp2 = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
	            fSource.Fund_Source_Name__c = 'test source name';
	            fSource.State__c = state2.id;
	            update fSource;
	            fSource2.Fund_Source_Name__c = 'test source name';
	            fSource2.State__c = state.id;
	            update fSource2;
            
            test.stopTest();
        }
    }
}