/**
 * @description Controller for Edit Result And Avetmiss page
 * @author Janella Lauren Canlas
 * @date 03.DEC.2012
 *
 * HISTORY 
 * - 03.DEC.2012    Janella Canlas  - Created.
 * - 15.FEB.2013    Michi Magsarili - updated to add new validation: not to allow end date to be greater than the date TODAY if
 *                                    National Outcome Code is not equal to 70 or 90
 * - 20.MAR.2013    Janella Canlas  - update to display error if Enrolment is cancelled/withdrawn and there are Enrolment Units not populated
 * - 26.MAY.2014    Scott Gassmann  - Updated existing error for populating Hours attened when Withdrawn Not Started as there was a bug in logic. 
 * - 15.SEP.2014    Warjie Malibago - Bypass VR if result is Never Commenced ( no participation) (WA Only)
 * - 20.OCT.2014    Bayani Cruz     - Implemented a compareTo to sort WrapperClass
 * - 06.APR.2015    Kim Saclag      - Added ORDER BY on the query for Location object.
 * - 03.AUG.2015    Michi Magsarili - Additional Logic for data submission -  if the result is Withdrawn/Not Started no "hours attended' are required. 
 */
global with sharing class EditResultsAndAvetmiss_CC {
    
    public String location{get;set;}
    public String idToRerender {get;set;}
    
    public integer index {get;set;}
    
    public List<WrapperClass> euWrapList {get;set;}
    
    public Map<Id,Results__c> resultMap = new Map<Id, Results__c>();
    
    public Id enrolmentId;
    public Id stateId {get;set;}
    
    public EditResultsAndAvetmiss_CC(){
        idToRerender = '';
        //retrieves the enrolment ID passed from the button
        enrolmentId = ApexPages.currentPage().getParameters().get('enrolmentId');
        fillHelperClass();
    }
    /*
    public List<SelectOption> getResultsPicklist() {
        
        List<SelectOption> options = new List<SelectOption>();                       
        options.add(new SelectOption('', ''));   
        for (Results__c r : [SELECT Id, Name,National_Outcome__c,National_Outcome_Code__c, State__c, State__r.Name from Results__c where State__c =: stateId]) {
            options.add(new SelectOption(r.Id, r.Name + ' '+ r.State__r.Name));
            
        }  
        return options;
    }
    */
    // Wrapper Class
    public class WrapperClass implements Comparable {

        public Enrolment_Unit__c eu {get;set;}
        public List<SelectOption> res {get;set;}
        public List<SelectOption> loc {get;set;}
        
        /* BAC 20.10.2014 */
        public String unitName;
        /* BAC 20.10.2014 */
       
        public WrapperClass(Enrolment_Unit__c passEURecord, List<SelectOption> results, List<SelectOption> locations) {
            eu = passEURecord;
            res = results;
            loc = locations;
        /* BAC 20.10.2014 */    
            unitName = eu.Unit__r.Name;
        }
          
        public Integer compareTo(Object compareTo) {
           return unitName.CompareTo(((WrapperClass)compareTo).eu.Unit__r.Name);
        }
        
         /* BAC 20.10.2014 */
    }
    
    //class to fill page table
    public void fillHelperClass() {
        //re-instantiate wrapper class list
        euWrapList = new List<WrapperClass>();
        
        Set<Id> resultIds = new Set<Id>();
        Set<String> torgIds = new Set<String>();
        
        List<Results__c> resList = new List<Results__c>();
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
        List<Locations__c> locList = new List<Locations__c>();
        
        Map<String, List<Locations__c>> locMap = new Map<String, List<Locations__c>>();
        Map<Id,List<Results__c>> optMap = new Map<Id,List<Results__c>>();
        
        //query enrolment unit
        //added Unit_Name__c 06/24/2013
        euList = [Select Variation_Reason_Code__c, Unit__c, Unit__r.Name,Unit__r.Unit_Name__c,Unit_Result__c,Unit_Result__r.Name, Unit_Record_Type__c,
                    Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c, Unit_Result__r.National_Outcome_Code__c,
                    Training_Delivery_Location__r.Parent_Location__r.State_Lookup__r.Name,Unit_Name__c, Training_Delivery_Location__c, 
                    Training_Delivery_Location__r.State_Lookup__c, Start_Date__c, 
                    RecordTypeId, Purchasing_Contract_Schedule_Identifier__c, Purchasing_Contract_Identifier__c, 
                    Name, Line_Item_Qualification_Unit__c, Line_Item_Qualification_Unit__r.Name, Last_Modified_Unit_Date__c,Id, Enrolment__c, End_Date__c, 
                    Delivery_Mode__c, Delivery_Mode_Identifier__c, Delivery_Location_Identifier__c, Completion_Status__c, 
                    AVETMISS_National_Outcome__c, Enrolment__r.Delivery_Location__r.Training_Organisation__c ,
                    Enrolment__r.Delivery_Location__r.Training_Organisation__r.BillingState,Scheduled_Hours__c,
                    Hours_Attended__c,Report_for_AVETMISS__c, Enrolment__r.Start_Date__c, Enrolment__r.End_Date__c,
                    Enrolment__r.Enrolment_Status__c, Percentage_Result__c
                    From Enrolment_Unit__c
                    Where Enrolment__c =: enrolmentId ORDER BY Training_Delivery_Location__c ASC]; 
        
        //collect needed ids            
        for(Enrolment_Unit__c e : euList){
            if(e.Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c != null){
                resultIds.add(e.Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c);
            }
            torgIds.add(e.Enrolment__r.Delivery_Location__r.Training_Organisation__c);
        }
        system.debug('torgIds***'+torgIds);
        //query locations
        //KS Start 4/6/2015
        locList = [select id, name, State_Lookup__c, State_Lookup__r.Name, State_Lookup__r.Short_Name__c,Training_Organisation__c from Locations__c where Training_Organisation__c IN: torgIds and RecordType.Name ='Training Delivery Location' ORDER BY Name];
        //KS End
        system.debug('locList***'+locList);
        
        //create map of list of locations per training organisation
        for(String t : torgids){
            for(Locations__c l : locList){
                if(t == l.Training_Organisation__c){
                    if(locMap.containsKey(t)){
                        List<Locations__c> tempList = locMap.get(t);
                        tempList.add(l);
                        locMap.put(t,tempList);
                    }
                    else{
                        locMap.put(t,new List<Locations__c>{l});
                    }
                }
            }
        }
        system.debug('locMap***'+locMap);
        
        system.debug('resultIds***' + resultIds);
        //query results
        resList = [SELECT Id, Name,National_Outcome__c,Result_Code__c,National_Outcome_Code__c, State__c, State__r.Name 
                    from Results__c 
                    where State__c IN: resultIds
                    order by Name];
        system.debug('resList***' + resList);
        
        //create map of list of results per result
        for(Enrolment_Unit__c e : euList){
            for(Results__c r : resList){
                system.debug('r.State__r.Name***' + r.State__r.Name + ' '+ r.State__c);
                system.debug('Training_Delivery_Location__r.Parent_Location__r.State_Lookup__r.Name***' + e.Training_Delivery_Location__r.Parent_Location__r.State_Lookup__r.Name + ' '+e.Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c);
                if(r.State__c == e.Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c){
                    if(optMap.containsKey(e.Id)){
                        List<Results__c> tempList = optMap.get(e.Id);
                        tempList.add(r);
                        optMap.put(e.Id,tempList);
                    }
                    else{
                        optMap.put(e.Id,new List<Results__c>{r});
                    }
                }
            }
        }
        system.debug('optMap***' + optMap);         
        for(Enrolment_Unit__c eu : euList){
            List<SelectOption> options = new List<SelectOption>();                       
            options.add(new SelectOption('', '- None -'));   
            if(optMap.get(eu.Id)!=null){
                resultMap = new Map<Id, Results__c>();
                for (Results__c r : optMap.get(eu.Id)) {
                    resultMap.put(r.Id,r);
                    options.add(new SelectOption(r.Id, r.Name +' - '+ r.Result_Code__c));
                }  
            }
            List<SelectOption> locs = new List<SelectOption>();
            locs.add(new SelectOption('', '- None -')); 
            if(locMap.get(eu.Enrolment__r.Delivery_Location__r.Training_Organisation__c) != null){
                for (Locations__c l : locMap.get(eu.Enrolment__r.Delivery_Location__r.Training_Organisation__c)) {
                    locs.add(new SelectOption(l.Id, l.Name));
                }
            }       
            //populate wrapper class list
            WrapperClass wc = new WrapperClass(eu,options,locs);
            euWrapList.add(wc);
            
        }
        
        //BAC 20.10.2014
        euWrapList.sort();
        system.debug('euWrapList***'+euWrapList);
    }
    
    //method to update enrolment units
    public PageReference updateUnits(){
        String natOutCode = '';
        String unitName = '';
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
       
        for(WrapperClass w : euWrapList){
            string enrolmentStart = w.eu.Enrolment__r.Start_Date__c.day() +'/'+ w.eu.Enrolment__r.Start_Date__c.month() +'/' + w.eu.Enrolment__r.Start_Date__c.year();
            string enrolmentEnd = w.eu.Enrolment__r.End_Date__c.day() +'/'+ w.eu.Enrolment__r.End_Date__c.month() +'/' + w.eu.Enrolment__r.End_Date__c.year();
            
            if(resultMap.get(w.eu.Unit_Result__c) != null){
                
                natOutCode = resultMap.get(w.eu.Unit_Result__c).National_Outcome_Code__c;
               
                unitName = resultMap.get(w.eu.Unit_Result__c).Name;
                //jcanlas 21MAR2013 start - if enrolment is cancelled, result should not be 70/90
                if(w.eu.Enrolment__r.Enrolment_Status__c == 'Withdrawn' || w.eu.Enrolment__r.Enrolment_Status__c == 'Cancelled'){
                    if(natOutCode == '70' || natOutCode == '90'){
                        
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'This enrolment is '+ w.eu.Enrolment__r.Enrolment_Status__c +'. The national outcome code for '+ w.eu.Unit__r.Name +' must not be 70 or 90.'));
                        return null;
                    }
                }
                //jcanlas 21MAR2013 end
            }
            
            system.debug('w.eu.Unit_Result__r.National_Outcome_Code__c: '+w.eu.Unit_Result__r.National_Outcome_Code__c);
            
            //force population of Hours Attended if National Outcome Code is 40 and Unit Result Name is not 'Withdrawn - not started
            if((w.eu.Hours_Attended__c == 0.0 || w.eu.Hours_Attended__c == null) && natOutCode == '40' && !unitname.containsIgnoreCase('not started') && !unitname.contains('Never Commenced ( no participation) (WA Only)')){ //15.SEP.2014 WBM //MAM 03.AUG.2015
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please populate Hours Attended on Unit ' + w.eu.Unit__r.Name));
                return null;
            }
            //else if((w.eu.Hours_Attended__c != 0.0 && w.eu.Hours_Attended__c != null) && natOutCode == '40' && unitname.contains('Withdrawn - not started')){
              //  ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,w.eu.Unit__r.Name + ' is Withdrawn. You cannot modify Hours Attended.'));
               // return null;
            //}
            else if(w.eu.Start_Date__c < w.eu.Enrolment__r.Start_Date__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Enrolment Unit for '+ w.eu.Unit__r.Name + ' Start Date must be within ' +enrolmentStart+'-'+enrolmentEnd+'.'));
                return null;
            }
            else if(w.eu.End_Date__c > w.eu.Enrolment__r.End_Date__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Enrolment Unit for '+ w.eu.Unit__r.Name + ' End Date must be within ' +enrolmentStart+'-'+enrolmentEnd+'.'));
                return null;
            }
            //mmagsarili 02/15/2013 start
            else if(natOutCode == '' && (natOutCode != '70' && natOutCode != '90')){
                
                if( w.eu.End_Date__c > Date.TODAY()){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Enrolment Unit for '+ w.eu.Unit__r.Name + ': End Date must not be greater than or equal TODAY.'));
                    return null;
                }
                //jcanlas 20MAR2013 start
                else if(w.eu.Start_Date__c > w.eu.End_Date__c){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Enrolment Unit for '+ w.eu.Unit__r.Name + ': Start Date must always be earlier than End Date.'));
                    return null;
                }
                //jcanlas 20MAR2013 end
                else{ 
                    euList.add(w.eu);
                }
            }
            //mmagsarili 02/15/2013 end
            //jcanlas 20MAR2013 start
            else if(w.eu.Unit_Result__c == null){
                if(w.eu.Enrolment__r.Enrolment_Status__c == 'Withdrawn' || w.eu.Enrolment__r.Enrolment_Status__c == 'Cancelled'){
                    
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'This enrolment is '+ w.eu.Enrolment__r.Enrolment_Status__c +'. Please populate Result for all Units.'));
                    return null;
                }
                else{
                    euList.add(w.eu);
                }
            }
            //jcanlas 20MAR2013 end
            else{
                euList.add(w.eu);
            }
        }
       
        //update enrolment unit records
        try{
            update euList;
            deleteClassEnrolments(euList);
            PageReference pref = new PageReference('/'+enrolmentId);
            pref.setRedirect(true);
            return pref;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: ' + e.getMessage()));return null;}
    }
    //cancel method, redirect to enrolment record
    public PageReference cancel(){
        PageReference p = new PageReference('/'+enrolmentId);
        p.setredirect(true);
        return p;
    }
    public static List<Results__c> resList {get;set;}
    //javascript remoting method
    @RemoteAction
    global static List<Results__c> getResultOptions(String locationId){ 
        resList = new List<Results__c>();
        set<Id> stateIds = new Set<Id>();
        system.debug('**Remote Loc: ' + locationId);
        for(Locations__c l : [select State_Lookup__c, Parent_Location__r.State_Lookup__c, Parent_Location__r.State_Lookup__r.Name from Locations__c where Id=:locationId]){
            if(l.Parent_Location__r.State_Lookup__c != null){
                stateIds.add(l.Parent_Location__r.State_Lookup__c);
                system.debug('**l.Parent_Location__r.State_Lookup__r.Name: '+l.Parent_Location__r.State_Lookup__r.Name + ' ,Id: ' + l.Id);
            }
        }
        system.debug('**Remote State: ' + stateIds.size());
        resList = [SELECT Id, Name,National_Outcome__c, Result_Code__c, State__c, State__r.Name 
                    from Results__c 
                    where State__c IN: stateIds
                    order by Name];
       
        system.debug('**Remote Result: ' + resList.size());
        return resList;
    }
    //Used to re-render Results picklist upon changing of Delivery Location
    public void rerenderResults(){
        resList = new List<Results__c>();
        set<Id> stateIds = new Set<Id>();
        for(Locations__c l : [select State_Lookup__c, Parent_Location__r.State_Lookup__c, Parent_Location__r.State_Lookup__r.Name from Locations__c where Id=:location]){
            if(l.Parent_Location__r.State_Lookup__c != null){
                stateIds.add(l.Parent_Location__r.State_Lookup__c);
                system.debug('**State Id: ' + l.Parent_Location__r.State_Lookup__c);
            }
        }
        
        system.debug('**Location Id: ' + location + ' ,State: ' + stateIds.size());
        
        //mmagsarili 02/15/2013 add National_Outcome_Code__c
        resList = [SELECT Id, Name,National_Outcome__c, National_Outcome_Code__c, Result_Code__c, State__c, State__r.Name 
                    from Results__c 
                    where State__c IN: stateIds
                    order by Name];
        system.debug('**Res List: ' + resList.size());            
        
        List<SelectOption> options = new List<SelectOption>();                       
            options.add(new SelectOption('', '- None -'));   
            for (Results__c r : resList) {
                resultMap.put(r.Id,r); //mmagsarili 02/15/2013 put entries to resultmap for NatCode value
                options.add(new SelectOption(r.Id, r.Name +' - '+ r.Result_Code__c));
            }  
        euWrapList[index].res = options;
        
    }
    //delete class enrolments and enrolment intake units (if needed)
    public void deleteClassEnrolments(List<Enrolment_Unit__c> euList){
        List<Enrolment_Unit__c> cancelledEUList = new List<Enrolment_Unit__c>();
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        List<Class_Enrolment__c> cenrol = new List<Class_Enrolment__c>();
        List<Class_Enrolment__c> remainingCenrol = new List<Class_Enrolment__c>();
        List<Enrolment_Intake_Unit__c> eiuForDelete = new List<Enrolment_Intake_Unit__c>();
        Set<Id> euIds = new Set<Id>();
        Set<Id> cancelledEUIds = new Set<Id>();
        Set<Id> eiuIds = new Set<Id>();
        Map<Id,Class_Enrolment__c> cenrolMap = new Map<Id, Class_Enrolment__c>();
        for(Enrolment_Unit__c e : euList){
            euIds.add(e.Id);
        }
        system.debug('1. euids : ' + euIds);
        cancelledEUList = [select Id, Unit_Result__r.Name  from Enrolment_Unit__c where Id IN: euIds and (Unit_Result__r.Name LIKE '%Withdrawn%' OR Unit_Result__r.Name LIKE '%Cancelled%')];
        system.debug('2. cancelledEUList : ' + cancelledEUList);
        if(cancelledEUList.size() > 0){
            for(Enrolment_Unit__c e : cancelledEUList){
                cancelledEUIds.add(e.Id);
            }
            cenrol = [select Id, Enrolment_Intake_Unit__c from Class_Enrolment__c where Enrolment_Intake_Unit__r.Enrolment_Unit__c IN: cancelledEUIds AND Class_Start_Date_Time__c > TODAY];
            if(cenrol.size() > 0){
                for(Class_Enrolment__c c : cenrol){
                    eiuIds.add(c.Enrolment_Intake_Unit__c);
                }
                try{
                    delete cenrol;
                    remainingCenrol = [select Id, Enrolment_Intake_Unit__c from Class_Enrolment__c where Enrolment_Intake_Unit__c IN: eiuIds];
                    eiuList = [select Id from Enrolment_Intake_Unit__c where id IN: eiuIds];
                    if(remainingCenrol.size() > 0){
                        for(Class_Enrolment__c c : remainingCenrol){
                            cenrolMap.put(c.Enrolment_Intake_Unit__c, c);
                        }
                    }
                    for(Enrolment_Intake_Unit__c e : eiuList){
                        if(!cenrolMap.containsKey(e.Id)){
                            eiuForDelete.add(e);
                        }
                    }
                    if(eiuForDelete.size() > 0){
                        system.debug('pasok eiuForDelete?');
                        delete eiuForDelete;
                    }
                }
                catch(Exception e){
                    //display error if there is an exception
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'ERROR removing Class Enrolments: ' + e.getMessage()));
                }
            }
        }
    }
}