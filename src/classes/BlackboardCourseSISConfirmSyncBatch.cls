global with sharing class BlackboardCourseSISConfirmSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	global final String query = 'Select SIS_Sync_Attempted__c, RecordType.DeveloperName,Id,Course_Description__c, Name, Template_Course__c,Template_Course__r.Name, create__c,Blackboard_Course_Name__c,Available_After_Sync__c,Get_T3_PK_Attempted__c From Blackboard_Course__c Where Get_T3_PK_Attempted__c = True AND RecordType.DeveloperName = \'T3_Intake_Course\'';
	
	global BlackboardCourseSISConfirmSyncBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		 for(SObject s :scope) {
             
             Blackboard_Course__c bbc = (Blackboard_Course__c) s;

             BlackboardCourseWS.getCourse(bbc);

     }
	
	}
	
	global void finish(Database.BatchableContext BC) {

		BlackboardCourseSISSyncBatch b1 = new BlackboardCourseSISSyncBatch();
		if(!Test.isRunningTest())
        	database.executebatch(b1,200);
		
	}
	
}