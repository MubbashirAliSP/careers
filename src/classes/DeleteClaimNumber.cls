global with sharing class DeleteClaimNumber {
	WebService static String deleteClaimNumberRec (Claim_Number__c claim,  String command) { 

	    List<Enrolment_Claims__c> eClaims = [
	        SELECT Id
	        FROM Enrolment_Claims__c
	        WHERE Claim_Number__c = : claim.Id
	    ];
		
		List<Enrolment_Unit_Claim__c> euClaims = [
	        SELECT Id
	        FROM Enrolment_Unit_Claim__c
	        WHERE Claim_Number__c = : claim.Id
	    ];
	    
	    command = 'This Claim Number will now be deleted.\n\n You will be redirected to the Claim Numbers page.';
	    try{
	    	if(euClaims.size() > 0){
	    		delete euClaims;
	    	}
	    	if(eClaims.size() > 0){
	    		delete eClaims;
	    	}
		    delete claim;}catch(Exception e){system.debug('ERROR: ' + e.getMessage());
	    }

	    return command;                 
	}
}