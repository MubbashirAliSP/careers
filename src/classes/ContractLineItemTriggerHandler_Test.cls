/**
 
 */
@isTest
private class ContractLineItemTriggerHandler_Test {

    static testMethod void testCLI() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='CLI_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        system.runAs(u){
            test.startTest();
	            acc = TestCoverageUtilityClass.createStudent();
	            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
	            anz = TestCoverageUtilityClass.createANZSCO();
	            fld = TestCoverageUtilityClass.createFieldOfEducation();
	            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
	            unit = TestCoverageUtilityClass.createUnit();
	            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
	            country = TestCoverageUtilityClass.createCountry(); 
	            state = TestCoverageUtilityClass.createState(country.Id); 
	            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
	            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
	            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
	            update conLine;
            test.stopTest();
        }
    }    
}