/**
 * @description Staff Member Trigger Handler 
 * @author Janella Lauren Canlas
 * @date 05.NOV.2012
 *
 * HISTORY
 * - 05.NOV.2012	Janella Canlas - Created.
 */
public with sharing class StaffMemberTriggerHandler {
	/*
		This method will insert class staff members based on the staff members passed by the trigger
	*/
	public static void insertClassStaffMember(List<Staff_Member__c> staffList){
		//declare variables
		Set<Id> staffIds = new Set<Id>();
		Set<Id> intakeIds = new Set<Id>();
		
		List<Class_Staff_Member__c> csmList = new List<Class_Staff_Member__c>();
		
		//Map<Id,List<Classes__c>> classMap = new Map<Id,List<Classes__c>>();
		
		//retrieve intake ids from the staff member list passed by the trigger
		for(Staff_Member__c sm : staffList){
			intakeIds.add(sm.Intake__c);
		}
		//query class records
		List<Classes__c> classList = [select id,Intake__c from Classes__c where Intake__c IN: intakeIds];
		//create new class staff member records
		for(Staff_Member__c s : staffList){
			if(s.Add_staff_member_to_all_classes__c == true){
				for(Classes__c c : classList){
					if(s.Intake__c == c.Intake__c){
						Class_Staff_Member__c csm = new Class_Staff_Member__c();
						csm.Intake_Staff_Member__c = s.Id;
						csm.Class__c = c.Id;
						csmList.add(csm);
					}
				}
			}
		}
		system.debug('classList'+classList);
		//insert class staff members
		try{
			insert csmList;}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());
		}
	}
	/*
		This method will delete Class Staff Members when the same Intake Staff Members are deleted
	*/
	public static void deleteClassStaffMember(List<Staff_Member__c> staffList){
		Set<Id> intakeIds = new Set<Id>();
		Set<Id> classIds = new Set<Id>();
		Set<Id> staffIds = new Set<Id>();
		List<Class_Staff_Member__c> csmToDel = new List<Class_Staff_Member__c>();
		//collect staff and intake ids
		for(Staff_Member__c sm : staffList){
			intakeIds.add(sm.Intake__c);
			staffIds.add(sm.Id);
		}
		//query classes
		List<Classes__c> classList = [select id, Intake__c from Classes__c where Intake__c IN: intakeIds];
		//collect class ids
		for(Classes__c c : classList){
			classIds.add(c.Id);
		}
		//query class staff members
		List<Class_Staff_Member__c> csmList = [select Id, Intake_Staff_Member__c, Class__c from Class_Staff_Member__c where Intake_Staff_Member__c IN: staffIds AND Class__c IN: classIds];
		//add the class staff member to the list to be deleted
		for(Staff_Member__c s : staffList){
			for(Class_Staff_Member__c csm : csmList){
				if(csm.Intake_Staff_Member__c == s.Id){
					csmToDel.add(csm);
				}
			}	
		}
		system.debug('csmToDel: '+csmToDel);
		//delete class staff member
		try{
			delete csmToDel;}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());
		}
	}
}