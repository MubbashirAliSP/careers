/*
 * @description    Trigger handler for UserTrigger
 * @date           31.JULY.2014
 * @author         Warjie Malibago
*/
public with sharing class UserTriggerHandler{   
    public static Boolean updateUserLicenseType(String oldLicense, String userId){
        Boolean proceed;
        List<User> userList = new List<User>([SELECT Id, User_License_Type__c, Profile.UserLicense.Name FROM User WHERE Id =: userId]);
        
        system.debug('**New: ' + userList[0].Profile.UserLicense.Name + ' ,Old: ' + oldLicense);
        if(userList[0].Profile.UserLicense.Name != oldLicense){
            userList[0].User_License_Type__c = userList[0].Profile.UserLicense.Name;
            update userList;
        }
        return proceed = false;
    }
}