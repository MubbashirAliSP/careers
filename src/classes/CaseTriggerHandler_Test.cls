/**
 * @description Test class for `CaseTriggerHandler` class
 * @author Ranyel Maliwanag
 * @date 04.AUG.2015
 */
@isTest
public with sharing class CaseTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {
        Account studentAccount = SASTestUtilities.createStudentAccount();
        insert studentAccount;
    }

    @testSetup
    static void userSetup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User testUser = SASTestUtilities.createUser();
        testUser.ProfileId = p.id;
        testUser.Email = 'olympia@careersaustralia.edu.au.test';
        testUser.Username = 'olympia@careersaustralia.edu.au.test';
        insert testUser;
    }

    static testMethod void updateOutstandingCountTest1() {
        Account studentAccount = [SELECT Id, Total_Outstanding_Complaint__c FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Id studentFeedbackRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Student Feedback').getRecordTypeId();

        Test.startTest();
            // Initial assertions
            // Assertion: Student Account exists
            // Assertion: Total Outstanding Complaints is 0
            System.assertNotEquals(null, studentAccount);
            System.assertEquals(null, studentAccount.Total_Outstanding_Complaint__c);

            // Create a new case of type student complaint
            Case complaintCase = new Case(
                    AccountId = studentAccount.Id,
                    RecordTypeId = studentFeedbackRecordType,
                    Status = 'New'
                );
            insert complaintCase;

            complaintCase = [SELECT Id, IsClosed FROM Case WHERE Id = :complaintCase.Id];
            System.assert(!complaintCase.IsClosed);

            studentAccount = [SELECT Id, Total_Outstanding_Complaint__c FROM Account WHERE Id = :studentAccount.Id];
            // Assertion: Total Outstanding Complaints is 1
            System.assertEquals(1, studentAccount.Total_Outstanding_Complaint__c);

            delete complaintCase;

            studentAccount = [SELECT Id, Total_Outstanding_Complaint__c FROM Account WHERE Id = :studentAccount.Id];
            // Assertion: Total Outstanding Complaints is 1
            System.assertEquals(0, studentAccount.Total_Outstanding_Complaint__c);            
        Test.stopTest();
    }


    static testMethod void updateTotalCaseCountTest() {
        Account studentAccount = [SELECT Id, Total_Outstanding_Complaint__c, Total_Complaints__c FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Id studentFeedbackRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Student Feedback').getRecordTypeId();

        Test.startTest();
            // Initial assertions
            // Assertion: Student account exists
            // Assertion: Total Complaints is 0
            // Assertion: Total Outstanding Complaints is 0
            System.assertNotEquals(null, studentAccount);
            System.assertEquals(null, studentAccount.Total_Outstanding_Complaint__c);
            System.assertEquals(null, studentAccount.Total_Complaints__c);

            // Create a new case of type student complaint
            Case complaintCase = new Case(
                    AccountId = studentAccount.Id,
                    RecordTypeId = studentFeedbackRecordType,
                    Status = 'New'
                );
            insert complaintCase;

            complaintCase = [SELECT Id, IsClosed FROM Case WHERE Id = :complaintCase.Id];
            System.assert(!complaintCase.IsClosed);

            studentAccount = [SELECT Id, Total_Complaints__c, Total_Outstanding_Complaint__c FROM Account WHERE Id = :studentAccount.Id];
            // Assertion: Total Outstanding Complaints is 1
            System.assertEquals(1, studentAccount.Total_Outstanding_Complaint__c);
            // Assertion: Total Complaints is 1

            complaintCase.Status = 'Closed';
            update complaintCase;

            studentAccount = [SELECT Id, Total_Complaints__c, Total_Outstanding_Complaint__c FROM Account WHERE Id = :studentAccount.Id];
            // Assertion: Total Outstanding Complaints is 0
            System.assertEquals(0, studentAccount.Total_Outstanding_Complaint__c);
            // Assertion: Total Complaints is 1
            System.assertEquals(1, studentAccount.Total_Complaints__c);

            delete complaintCase;

            studentAccount = [SELECT Id, Total_Complaints__c, Total_Outstanding_Complaint__c FROM Account WHERE Id = :studentAccount.Id];
            // Assertion: Total Outstanding Complaints is 0
            System.assertEquals(0, studentAccount.Total_Outstanding_Complaint__c);
            // Assertion: Total Complaints is 0
            System.assertEquals(0, studentAccount.Total_Complaints__c);
        Test.stopTest();
    }

    static testMethod void setAccountDetailsTest() {
        Account studentAccount = [SELECT Id, Total_Outstanding_Complaint__c FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Id studentFeedbackRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Student Feedback').getRecordTypeId();

        Test.startTest();
            // Initial assertions
            // Assertion: Student Account exists
            // Assertion: Total Outstanding Complaints is 0
            System.assertNotEquals(null, studentAccount);
            System.assertEquals(null, studentAccount.Total_Outstanding_Complaint__c);

            Case complaintCase = new Case(
                    RecordTypeId = studentFeedbackRecordType,
                    Student_Identifier__c = 'CX1230'
                );
            insert complaintCase;

            complaintCase = [SELECT Id, AccountId FROM Case WHERE Id = :complaintCase.Id];
            // Assertion: AccountId field gets populated based on the provided student identifier
            System.assert(String.isNotBlank(complaintCase.AccountId));
            System.assertEquals(studentAccount.Id, complaintCase.AccountId);
        Test.stopTest();
    }

    static testMethod void notifyCaseDeletionTest() {
        Case sasIssue = new Case(
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Uncategorized Cases').getRecordTypeId()
            );
        insert sasIssue;

        Test.startTest();
            delete sasIssue;
        Test.stopTest();
    }

    static testMethod void notifyImproperCaseClose() {
        Case sasIssue = new Case(
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Uncategorized Cases').getRecordTypeId()
            );
        insert sasIssue;

        Test.startTest();
            sasIssue.Status = 'Closed';
            update sasIssue;
        Test.stopTest();
    }
}