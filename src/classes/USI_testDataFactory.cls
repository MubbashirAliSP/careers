@isTest
public class USI_testDataFactory {
    
    public static USI_Identification_Document__c createUSIDocument(Id studentId) {
        
        USI_Identification_Document__c doc = new USI_Identification_Document__c();
        
        doc.Account__c = studentId;
        doc.Document_Number_Encrypted__c = 'X111111';
        doc.Registration_State__c = 'QLD';
        doc.Registration_Date__c = date.today();
        doc.Registration_Number_Encrypted__c = 'X111111';
        doc.Registration_Year__c = string.valueOf(date.today().year());
        
        return doc;
    }
    
    public static Country__c createCountry(){
        Country__c c = new Country__c();
        c.Country_Code__c = '1123';
        c.Name = 'Country Name';
        
        return c;
    }
    
    public static ANZSCO__c createANZSCO(){
        ANZSCO__c anz = new ANZSCO__c();
        anz.Name = 'Test ANZSCO';
        
        return anz;
    }
    
    public static Account createStudent(){
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student Account';
        sacct.FirstName = 'First Name';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.PersonMailingPostalCode = '4005';
        sacct.PersonMailingState = 'Queensland';
        sacct.PersonMobilePhone = '61847292111';
        sacct.Student_Identifer__c = 'H89373';
        sacct.PersonBirthDate = system.today() - 5000;
        sacct.Year_Highest_Education_Completed__c = '2000';
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.Sex__c = 'F - Female';
        sacct.Salutation = 'Mr.';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        sacct.Client_Occupation_Identifier__c = '1 - Manager';
        
        return sacct;
    }
    
    public static Account createTrainingOrganisation(){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Training_Organisation' And SObjectType = 'Account' LIMIT 1].Id;
        Account sacct = new Account();
        sacct.RecordTypeId = recrdType;
        sacct.Name = 'training';
        sacct.BillingCountry = 'NEW ZEALAND';
        sacct.Training_Org_Identifier__c = 'test';
        sacct.Training_Org_Type__c = 'test';
        sacct.Client_Occupation_Identifier__c = '1 - Manager';
        
        return sacct;
    }
    
    public static Field_of_Education__c createFieldOfEducation(){
        Field_of_Education__c fld = new Field_of_Education__c();
        Id recrdType = [select Id from RecordType where DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        fld.RecordTypeId = recrdType;
        fld.Name = 'Test Field of Education';
        
        return fld;
    }
    
    public static State__c createStateQLD(Id country){
        State__c newState = new State__c();
        newState.Name = 'Queensland';
        newState.Country__c = Country;
        newState.Short_Name__c = 'QLD';
        newState.Code__c = '03';
        
        return newState;
    }
    
    public static Results__c createResult(Id state){
        Id recType = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c newRes = new Results__c();
        newRes.RecordTypeId = recType;
        newRes.Name = 'test results';
        newRes.State__c = state; 
        
        return newRes;
    }
    
    public static Location_Loadings__c createLocationLoadings(Id state){
        Location_Loadings__c l = new Location_Loadings__c();
        l.Name = 'loading name';
        l.State__c = state;
        l.Price_Factor__c = 50;
        
        return l;
    }
    
    public static Locations__c createParentLocation(Id country, Id state, Id loading){
        Id recrdtype = [select id from RecordType where DeveloperName='Parent_Location' And SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.Name = 'name';
        l.Country__c = country;
        l.State_Lookup__c = state;
        l.Location_Loading__c = loading;
        l.RecordTypeId = recrdtype;
        //l = [select id, Country__c from Locations__c where Country__c != null and RecordType.Name ='Parent Location' LIMIT 1];
        
        return l;
    }
    
    public static Locations__c createLocation(Id countryId, Id pl, Id tOrg){
        Id recrdtype = [select id from RecordType where DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.RecordTypeId = recrdtype;
        l.Name = 'Sample Room';
        l.Country__c = countryId;
        l.Parent_Location__c = pl;
        l.Training_Organisation__c = tOrg;
        
        return l;
    }
    
    public static Funding_Source__c createFundingSource(Id state){
        RecordType recId = [Select Id,Name from RecordType where DeveloperName = 'State_Funding_Source' and sObjectType = 'Funding_Source__c'];
        Funding_Source__c f = new Funding_Source__c();
        f.RecordTypeId = recId.Id;
        f.Fund_Source_Name__c = 'test Source';
        f.Code__c = '2334';
        f.State__c = state;
        
        return f;
    }
    
    public static Qualification__c createQualification(String anzId, String fldId){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Qualification'  And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = recrdType;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anzId;
        qual.Field_of_Education__c = fldId;
        qual.Recognition_Status__c = '14 - Other Courses';     
        
        return qual;
    }
    
    public static Qualification_Unit__c createQualificationUnit(String qualId, String unitId){
        Qualification_Unit__c qu = new Qualification_Unit__c();
        qu.Qualification__c = qualId;
        qu.Unit__c = unitId;
        qu.Unit_Type__c = 'Core';
        
        return qu;
    }
    
    public static Purchasing_Contracts__c createPurchasingContractWithTOState(Id fSource, Id trainingOrg, Id state){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = fSource;
        p.Contract_Code__c = '1234567890';
        p.Training_Organisation__c = trainingOrg;
        p.Contract_State_Lookup__c = state;
        
        return p;
    }  
    
    public static Contract_Line_Items__c createContractLineItem(Id pCon){
        Contract_Line_Items__c c = new Contract_Line_Items__c();
        c.Line_Item_Number__c = '001';
        c.Purchasing_Contract__c = pCon;
        
        return c;
    }
    
    public static Line_Item_Qualifications__c createLineItemQualificationFS(Id qualification, Id con, Id fundSource){
        Line_Item_Qualifications__c  l= new Line_Item_Qualifications__c();
        l.Qualification__c = qualification;
        l.Training_Catalogue_Item_No__c = '0001';
        l.Contract_Line_Item__c = con;
        l.State_Fund_Source__c = fundSource;
        
        return l;
    }
    
    public static Line_Item_Qualification_Units__c createLineItemQualificationUnit(Id lineItem, Id qualUnit){
        Line_Item_Qualification_Units__c  l= new Line_Item_Qualification_Units__c();
        l.Qualification_Unit__c = qualUnit;
        l.Line_Item_Qualification__c = lineItem;
        
        return l;
    }
    
    public static Enrolment__c createEnrolment(Id student,Id qual, id liq, id loc, Id country){
        Enrolment__c e = new Enrolment__c();
        e.Student__c = student;
        e.Qualification__c = qual;
        e.Line_Item_Qualification__c = liq;
        //e.Enrolment_Status__c = 'Active (Commencement)';
        e.Enrolment_Status__c = 'Completed';
        e.Start_Date__c = date.today()+5;
        e.End_Date__c = date.today()+10;
        e.Delivery_Location__c = loc;
        e.Overseas_Country__c = country;
        e.Study_Reason__c = '01 - To get a job';
        e.Victorian_Commencement_Date__c = e.start_date__c+5;
        
        return e;
    }
    
    public static USI_Integration_Log__c createUSILog() {
        
        USI_Integration_Log__c log = new USI_Integration_Log__c();
        
        return log;
        
    }
    
    public static Unit__c createUnit() {
        Id recrdType = [select Id from RecordType where DeveloperName = 'Unit_of_Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c un = new Unit__c();
        un.Name = 'Test Unit';
        un.RecordTypeId = recrdType;
        un.Include_In_CCQI_Export__c = true;
        
        return un;
    }
    
    public static Unit_Hours_and_Points__c createUnitHoursAndPoints(Id unit, Id state) {
        Unit_Hours_and_Points__c  u= new Unit_Hours_and_Points__c();
        u.Unit__c = unit;
        u.State__c = state;
        u.Nominal_Hours__c = 1.5;
        u.Points__c = 1;
        
        return u;
    }
    
}