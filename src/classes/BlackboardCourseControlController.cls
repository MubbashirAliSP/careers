public class BlackboardCourseControlController {
    
    public String keyprefix= '';
    
    public String description1 {get; private set;}
    public String description2 {get; private set;}
    public String description4 {get; private set;}
    public String description5 {get; private set;}

    private final List<RecordType> recordtypes = new List<RecordType>([Select Id,DeveloperName,Description From RecordType Where SObjectType = 'Blackboard_Course__c' ORDER BY DeveloperName]);
    
    public BlackboardCourseControlController() {
        
        Schema.DescribeSObjectResult r = Blackboard_Course__c.sObjectType.getDescribe();
        keyprefix = r.getKeyPrefix();
        
        description1 = recordtypes.get(0).Description;
        description2 = recordtypes.get(1).Description;
        description4 = recordtypes.get(3).Description;
        description5 = recordtypes.get(4).Description;
        
    
    }
    
    public pageReference createType1() {

        return new pageReference('/apex/BlackboardCreateMasterCourse');
    }
    
    public pageReference createType2() {
        
        return new pageReference('/' + keyprefix + '/e?RecordType=' + recordtypes.get(1).Id);
    
    }
    
    public pageReference createType4() {
        return new pageReference('/' + keyprefix + '/e?RecordType=' + recordtypes.get(3).Id);
    
    }
    
    public pageReference createType5() {
    
        return new pageReference('/' + keyprefix + '/e?RecordType=' + recordtypes.get(4).Id);
    
    }

}