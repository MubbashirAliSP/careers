public with sharing class SEC_ListViewWrapper {
	@AuraEnabled
	public SEC_ListView__mdt meta {get; set;}

    @AuraEnabled
	public List<SEC_FieldSet> fieldsetMembers {get; set;}
    
    @AuraEnabled
    public String sObjectName {get; set;}
    
    public List<Schema.FieldSetMember> fieldsetMembersAuto {
		get {
			if (fieldsetMembersAuto == null) {
				Schema.FieldSet fieldSet = null;
				// Validate that the SObject name configured in the metadata is valid
				if (Schema.getGlobalDescribe().containsKey(meta.SEC_SObjectName__c)) {
					// Validate that the Fieldset name exists for the configured SObject in the meta
					if (Schema.getGlobalDescribe().get(meta.SEC_SObjectName__c).getDescribe().fieldSets.getMap().containsKey(meta.SEC_FieldsetName__c)) {
						// Fetch the actual Fieldset record if it exists
						fieldSet = Schema.getGlobalDescribe().get(meta.SEC_SObjectName__c).getDescribe().fieldSets.getMap().get(meta.SEC_FieldsetName__c);
					}
				}
				if (fieldSet != null) {
					fieldsetMembersAuto = fieldSet.getFields();
				}
			}
			return fieldsetMembersAuto;
		}
		set;
	}

	public class SEC_FieldSet {
        @AuraEnabled
        public String label {get; set;}
        
        @AuraEnabled
        public String fieldPath {get; set;}
        
        @AuraEnabled
        public String fieldType {get; set;}
	}
    
    public Boolean isCategorised {
		get {
			isCategorised = String.isNotBlank(meta.SEC_Categorisation__c);
			return isCategorised;
		}
		set;
	}
}