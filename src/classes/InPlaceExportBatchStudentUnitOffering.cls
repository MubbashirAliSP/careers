/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchStudentUnitOffering implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchStudentUnitOffering() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPSUO_Active__c, IPSUO_Course_Code__c, IPSUO_Course_Enrolment_Code__c, IPSUO_Course_Version__c, IPSUO_InPlace_Unit_Offering_a__c, IPSUO_Result__c, IPSUO_Student_Code__c, IPSUO_Sync_on_next_update__c, IPSUO_Unit_Offering_Code__c, IPSUO_Unit_Offering_Enrolment_Code__c, IPSUO_Year__c, Name, Id ';
        queryString += 'FROM Enrolment_Intake_Unit__c ';
        queryString += 'WHERE IPSUO_InPlace_Unit_Offering_a__c = true AND IPSUO_Sync_on_next_update__c = true ORDER BY IPSUO_Unit_Offering_Code__c';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Enrolment_Intake_Unit__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Enrolment_Intake_Unit__c record : records) {
            if (!exportedIdentifiers.contains(record.IPSUO_Student_Code__c + record.IPSUO_Unit_Offering_Code__c)) {
                String isActive = (String.isNotBlank(record.IPSUO_Active__c)) ? record.IPSUO_Active__c : 'Y';
                InPlace__c forExport = new InPlace__c(
                        IPStudentUnitOffering_Active__c = isActive,
                        IPStudentUnitOffering_CECode__c = record.IPSUO_Course_Enrolment_Code__c,
                        IPStudentUnitOffering_CourseCode__c = record.IPSUO_Course_Code__c,
                        IPStudentUnitOffering_CourseVersion__c = record.IPSUO_Course_Version__c,
                        IPStudentUnitOffering_Result__c = record.IPSUO_Result__c,
                        IPStudentUnitOffering_StudentCode__c = record.IPSUO_Student_Code__c,
                        IPStudentUnitOffering_UnitOfferingCode__c = record.IPSUO_Unit_Offering_Code__c,
                        IPStudentUnitOffering_UOEnrolCode__c = record.IPSUO_Unit_Offering_Enrolment_Code__c,
                        IPStudentUnitOffering_Year__c = record.IPSUO_Year__c
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPSUO_Student_Code__c + record.IPSUO_Unit_Offering_Code__c);
            }

            record.IPSUO_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchPlacementPrerequisite nextBatch = new InPlaceExportBatchPlacementPrerequisite();
        Database.executeBatch(nextBatch);
    }
}