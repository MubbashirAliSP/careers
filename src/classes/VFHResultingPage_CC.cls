/**
 * @description Unit Hours and Points Trigger Handler 
 * @author Janella Lauren Canlas
 * @date 10.JAN.2013
 * @history
 *       10.JAN.2013    Janella Canlas      - Created.
 *       16.JAN.2013    Janella Canlas      - Recoded from EnrolmentUnitResultingPage_CC
 *       18.JAN.2013    Janella Canlas      - Added code for EUoS Result Picklist
 *       23.JAN.2013    Janella Canlas      - Added code for EUos Variation Reason Picklist
 *       19.MAR.2014    Bayani Cruz         - Added code for Hours Attended
 *       20.APR.2016    Ranyel Maliwanag    - Fixed handling of Recredit/Revision EUOS records
 */
public with sharing class VFHResultingPage_CC {
    public List<Results__c> resList {get;set;}
    public List<Enrolment_Unit_of_Study__c> euos {get;set;}
    public List<EnrolmentUnitOfStudyWrapper> euosWrapper {get; set;}
    public Enrolment_Unit__c euRecord {get;set;}
    public String location{get;set;}
    public Integer index {get;set;}
    public Id enrolmentId;
    public String enrolmentUnitId {get;set;}
    public String locationId {get;set;}
    public String record {get;set;}
    
    // Picklists
    public List<SelectOption> unitPickList{get;set;}
    public List<SelectOption> purConPickList{get;set;}
    public List<SelectOption> delModePickList{get;set;}
    public List<SelectOption> locationPickList{get;set;}
    public List<SelectOption> resultsPickList{get;set;}
    public List<Results__c> resultsList {get;set;}

    public String hoursFocus {get; set;}

    public Boolean isAllEUOSSelected {
        get {
            isAllEUOSSelected = true;
            for (EnrolmentUnitOfStudyWrapper item : euosWrapper) {
                if (!item.selected) {
                    isAllEUOSSelected = false;
                    break;
                }
            }
            return isAllEUOSSelected;
        }
        set;
    }
    
    
    /**
     * @description Default custom page controller constructor
     */
    public VFHResultingPage_CC() {
        // Retrieves the enrolment ID passed from the button
        enrolmentId = ApexPages.currentPage().getParameters().get('enrolmentId');

        euosWrapper = new List<EnrolmentUnitOfStudyWrapper>();
    }
    
    
    /**
     * @description Compose Enrolment Unit Picklist
     */
    public List<SelectOption> getEUPicklist() {
        List<SelectOption> options = new List<SelectOption>(); 
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
        // Query enrolment units
        euList = [SELECT Id, Unit__r.Name, Unit_Name__c FROM Enrolment_Unit__c WHERE Enrolment__c =: enrolmentId ORDER BY Unit__r.Name];                      
        // Populate picklist
        options.add(new SelectOption('', ''));   
        for (Enrolment_Unit__c e : euList) {
            options.add(new SelectOption(e.Id, e.Unit__r.Name + ' - '+ e.Unit_Name__c));
        }  
        return options;
    }
    
    
    /**
     * @description Fill Enrolment Unit fields
     */
    public void fillEUFields(){
        euRecord = new Enrolment_Unit__c();
        euos = new List<Enrolment_Unit_of_Study__c>();
        // Query Enrolment Unit record
        euRecord = [SELECT Delivery_Location_Identifier__c, Delivery_Mode__c, Delivery_Mode_Identifier__c, End_Date__c, Enrolment__c, Enrolment__r.Delivery_Location__r.Training_Organisation__c, Purchasing_Contract_Schedule_Identifier__c, Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c, Training_Delivery_Location__r.Parent_Location__r.State_Lookup__r.Name, Id,Name,Start_Date__c, Training_Delivery_Location__c, Training_Delivery_Location__r.Name,Unit__c, Unit__r.Name,Unit_Name__c, Unit_Record_Type__c, Unit_Result__c,Unit_Result__r.Name,Unit_Type__c, Unit_of_Competency_Identifier__c, VET_in_Schools__c, Variation_Reason_Code__c, Enrolment__r.Contract_Code__c, Percentage_Result__c, Hours_Attended__c, Scheduled_Hours__c, Enrolment__r.RecordTypeId, Enrolment__r.RecordType.Name, Report_for_AVETMISS__c FROM Enrolment_Unit__c WHERE Id =: enrolmentUnitId];

        // Query enrolment units based on the enrolment id                        
        List<Enrolment_Unit__c> euLst = [SELECT Unit__c,Enrolment__c, Training_Delivery_Location__r.Parent_Location__r.State_Lookup__c, Enrolment__r.Delivery_Location__r.Training_Organisation__c, Unit__r.Name, Enrolment__r.Contract_Code__c,Percentage_Result__c, Hours_Attended__c, Scheduled_Hours__c FROM Enrolment_Unit__c WHERE Enrolment__c=:enrolmentId];
        resultsPickList = new List<SelectOption>();
        
        // Query related units
        unitPickList = new List<SelectOption>();
        unitPickList.add(new SelectOption('', '- None -')); 
        for(Enrolment_Unit__c eu : euLst){
            unitPickList.add(new SelectOption(eu.Unit__c,eu.Unit__r.Name));
        }                       
        
        // Delivery mode picklist
        delModePickList = new List<SelectOption>();
        delModePickList.add(new SelectOption('', '- None -'));
        for(Delivery_Mode_Types__c dm: [Select Id,Name from Delivery_Mode_Types__c order by Name]){
            delModePickList.add(new SelectOption(dm.Id,dm.Name));
        }   
        
        // Location picklist
        locationPickList = new List<SelectOption>();
        locationPickList.add(new SelectOption('', '- None -'));
        String torg = euRecord.Enrolment__r.Delivery_Location__r.Training_Organisation__c;
        for(Locations__c loc: [Select Id,Name from Locations__c where RecordType.Name ='Training Delivery Location' and Training_Organisation__c =: torg ORDER BY Name]){
            locationPickList.add(new SelectOption(loc.Id,loc.Name));
        }

        refreshResults();
        
        euos = geteuosList();
        for (Enrolment_Unit_of_Study__c record : euos) {
            euosWrapper.add(new EnrolmentUnitOfStudyWrapper(String.isBlank(record.Recredit_Revision__c) ? true : false, record));
        }
    }
    

    /**
     * @description Refresh results based on the Training Delivery Location
     */
    public void refreshResults(){
        resultsPickList = new List<SelectOption>();               
        resultsPickList.add(new SelectOption('', '- None -'));
        locationId = euRecord.Training_Delivery_Location__c;
        record = euRecord.Enrolment__r.RecordType.Name;
        resList = new List<Results__c>();
        set<Id> stateIds = new Set<Id>();
        
        // Collect state ids
        for(Locations__c l : [select State_Lookup__c, Parent_Location__r.State_Lookup__c, Parent_Location__r.State_Lookup__r.Name from Locations__c where Id=:locationId]){
            if(l.Parent_Location__r.State_Lookup__c != null){
                stateIds.add(l.Parent_Location__r.State_Lookup__c);
                system.debug('**l.Parent_Location__r.State_Lookup__r.Name'+l.Parent_Location__r.State_Lookup__r.Name);
            }
        }
        
        // Query results by state ids
        resList = [SELECT Id, Name,National_Outcome__c, Result_Code__c, State__c, State__r.Name, National_Outcome_Code__c
                    from Results__c 
                    where State__c IN: stateIds AND Inactive__c = false order by Result_Code__c];
        
        // Populate picklist
        for (Results__c r : resList) {
            //BAC to filter out the incorrect selection for SC-94754
            if(r.Name == 'Comptent-High Distinction') continue;
            resultsPickList.add(new SelectOption(r.Id, r.Result_Code__c +' - '+r.Name));
        }  
        
    }
    

    // Query related EUoS records
    public List<Enrolment_Unit_of_Study__c> geteuosList(){
        List<Enrolment_Unit_of_Study__c> temp = new List<Enrolment_Unit_of_Study__c>();
        temp = [SELECT  Enrolment_Unit__c, 
                        Enrolment_Unit__r.Unit__r.Name,
                        Enrolment_Unit__r.Purchasing_Contract_Identifier__c,
                        Enrolment_Unit__r.Start_Date__c,
                        Enrolment_Unit__r.End_Date__c,  
                        Enrolment_Unit__r.Delivery_Mode__r.Name,
                        Enrolment_Unit__r.Delivery_Mode_Identifier__c,
                        Enrolment_Unit__r.Training_Delivery_Location__r.Name,
                        Enrolment_Unit__r.Unit_Result__r.Name,
                        Variation_Reason__c,
                        Name,
                        Unit_of_Study_Code__c,
                        Census_Date__c,
                        Result__c,
                        Recredit_Revision__c
                        FROM Enrolment_Unit_of_Study__c
                        WHERE Enrolment_Unit__c =: enrolmentUnitId
                        ORDER BY Name ASC];
        return temp;
    } 
    
    // Compose EUOS Results Picklist
    public List<SelectOption> geteuosResultPicklist() {
        List<SelectOption> options = new List<SelectOption>(); 
        List<Completion_Status_Type__c> cstList = new List<Completion_Status_Type__c>();
        //query enrolment units
        cstList = [SELECT Id, Name FROM Completion_Status_Type__c];                      
        //populate picklist
        options.add(new SelectOption('', ''));   
        for (Completion_Status_Type__c e : cstList) {
            options.add(new SelectOption(e.Id, e.Name));
        }  
        return options;
    }
    
    // Compose EUOS Variation Reason Picklist
    public List<SelectOption> geteuosVarReasonPicklist() {
        List<SelectOption> options = new List<SelectOption>(); 
        List<Variation_Reason_Code__c> vrList = new List<Variation_Reason_Code__c>();
        //query variation reasons
        vrList = [SELECT Id, Code__c, Name FROM Variation_Reason_Code__c];                      
        //populate picklist
        options.add(new SelectOption('', ''));   
        for (Variation_Reason_Code__c e : vrList) {
            options.add(new SelectOption(e.Id, e.Name));
        }  
        return options;
    }
    
    // Cancel method
    public PageReference cancel(){
        PageReference p = new PageReference('/'+enrolmentId);
        p.setredirect(true);
        return p;
    }

    //update the enrolment unit record
    public PageReference save(){
        PageReference p = new PageReference('/'+enrolmentId);
        p.setredirect(true);

        /*************************************
        * @since 19.MAR.2014
        * validation start
        * should have a hours attended 
        * if result is withdrawn/discontinued
        * @bayani.cruz@cloudhserpas.com
        **************************************/
        
        Map<String, String> resultOptions = new Map<String, String>();

        for(SelectOption so : resultsPickList) {
            resultOptions.put(so.getLabel(), so.getValue());
        }
        
        final string RESULT_WITHDRAWN_DISCONTINUED = '40 - Withdrawn/discontinued';
        final string ENTER_HOURS_ATTNEDED = 'You must enter hours attended';

        if( (euRecord.Hours_Attended__c == NULL || euRecord.Hours_Attended__c <= 0.00) && euRecord.Unit_Result__c == resultOptions.get(RESULT_WITHDRAWN_DISCONTINUED)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Hours Attended: ' + ENTER_HOURS_ATTNEDED));
            euRecord.Hours_Attended__c.addError(ENTER_HOURS_ATTNEDED);
            hoursFocus = 'HoursFocus';              //call HoursFocus js method to focus on field

            return null;
        }
        /***validation end***/
        
        try{
            update euos;
            update euRecord;
            return p;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Error: '+e.getMessage()));
            return null;
        }
    }
}