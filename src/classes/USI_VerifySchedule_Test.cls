/**
 * @description Test Class for class USI_VerifySchedule 
 * @author Jerome Almanza
 * @date 13.OCT.2015
 *
 * HISTORY
 * - 13.OCT.2015    Jerome Almanza  - Created.
 */

@isTest
private class USI_VerifySchedule_Test {
				
	@isTest 
    static void USI_VerifyScheduleTest() {
    		//List<Account> students = new List<Account>();
    		//insert students;

            Test.startTest(); 
            String jobId = System.schedule('USI_VerifySchedule', '0 0 4 * * ?', new USI_VerifySchedule()); 
            Test.stopTest();
//check the schedule details 
		List<CronTrigger> cronJob = [SELECT Id, CronExpression,                  
											TimesTriggered, NextFireTime 
									FROM CronTrigger 
									WHERE (Id = :jobId)]; 
		if (!cronJob.isEmpty()) { 
			CronTrigger schedJob = cronJob.get(0); 
			System.assert(schedJob.CronExpression.equals('0 0 4 * * ?')); 
			System.assert(schedJob.TimesTriggered == 0); 
			System.assert(schedJob.NextFireTime.hour() == 4); } 
	}
}