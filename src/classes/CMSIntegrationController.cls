/*This class manages integration between CMS staging and CMS live
 *using jitterbit as a middle layer
 *All callouts will be made from this class
 *This class will also response from Jitterbit and CMS
 *For JSON generation please refer to CMSIntegrationQueryBuilder.class
 *Created By Yuri Gribanov 26-10-2015
 */

public class CMSIntegrationController {
   
   //Access Custom Setting for connection details
   private final static CMS_Integration_Settings__c cmsConfig = CMS_Integration_Settings__c.getInstance('Default');
   private static List<CMS_Integration_Event__c> integrationEvents = new List<CMS_Integration_Event__c>();
       
    /*Create response message */
    private static HttpRequest createRequest(String body, Boolean isLiveImport) {
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
      
        
        //Set correct endpoint
        if(isLiveImport)
           req.setEndpoint(cmsConfig.CMS_JitterBit_Live_Endpoint__c);
        else
           req.setEndpoint(cmsConfig.CMS_JitterBit_Staging_Endpoint__c);           
        
        req.setBody(body);
        
        return req;
        
    }
    
          
     //Post courses JSON to jitterbit endpoint
     public static HttpResponse importCourses(String logId, Set<Id> qualIds, Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateCourseJSON(qualIds), LI);
        
        Http http = new http();
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            
        }
        
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
        
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
        
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Courses', Status__c = JSONResponse.status, Message__c = JSONResponse.message,Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
         
        return res;
        
    }    
   
     public static HTTPResponse importDeliveryModes(String logId, Set<Id> qualIds,Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateDeliveryModesJSON(qualIds), LI);
       
        Http http = new http();
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            
        }        
        
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}'); 
         
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
               
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Delivery Modes', Status__c = JSONResponse.status, Message__c = JSONResponse.message,Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
         
        return res;
       
    }
    
    //post providers to jitterbit endpoint
    public static HTTPresponse importProviders(String logId, Set<Id> qualIds, Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateProviderJSON(qualIds), LI);
        
        Http http = new http();
        
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            
        }        
         
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
       
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
               
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Providers',Status__c = JSONResponse.status, Message__c = JSONResponse.message, Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
        
        return res;
      }
    
     //post qualification units to jitterbit endpoint
     public static httpResponse importUnits(String logId, Set<Id> qualIds, Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateUnitsJSON(qualIds), LI);
        Http http = new http();
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            
        }
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
         
         
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
        
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Units', Status__c = JSONResponse.status, Message__c = JSONResponse.message,Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
        
        return res;
    }
    
    //post disclaimeroptions to jitterbit endpoint
    public static httpResponse importDisclaimerOptions(String logId, Set<Id> discIds, Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateDesclaimerOptionsJSON(discIds), LI);
        Http http = new http();
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            
        }        
       
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
        
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
        
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Disclaimer Options', Status__c = JSONResponse.status, Message__c = JSONResponse.message, Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
        
        return res;
    }
    
    
    
    //post course disclaimers to jitterbit endpoint
    public static httpResponse importCourseDisclaimers(String logId, Set<Id> qualIds, Boolean LI) {
        
        httpRequest req = createRequest(CMSIntegrationQueryBuilder.generateCourseDisclaimers(qualIds), LI);
        Http http = new http();
        HttpResponse res = new HttpResponse();
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            system.debug('ERRROR' + e.getMessage());
        }
        
       
        //Must preset value, another json deserialize fails
        if(Test.isRunningTest())
            res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
        
        CMSResponse JSONresponse = (CMSResponse) JSON.deserialize(res.getBody(), CMSResponse.class);
        
        CMS_Integration_Event__c event = new CMS_Integration_Event__c(CMS_Integration_Log__c = LogId, SObject_Type__c = 'Course Disclaimers', Status__c = JSONResponse.status, Message__c = JSONResponse.message,Sent_JSON__c = req.getBody());
        
        integrationEvents.add(event);
        
        return res;
    }
    
    
    /*This method makes a callout to Jitterbit */
     @future(callout=true) public static void post(String logId, Set<Id> qualIds, Boolean isliveImport) {
       
       CMS_Integration_Log__c log = [Select Id, Integration_Message__c From CMS_Integration_Log__c Where id = : logId];
         
         
       log.Integration_Message__c = 'Success';
         
       try {importCourses(log.Id, qualIds, isliveImport );}        
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Courses ' + e.getMessage() ); }         
    
       try {importDeliveryModes(log.Id, qualIds, isLiveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Delivery Modes ' + e.getMessage() );} 
         
       try {importProviders(log.Id, qualIds, isliveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Providers ' + e.getMessage() );} 
         
       try {importUnits(log.Id, qualIds, isliveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Units ' + e.getMessage() );} 
         
       try {importDisclaimerOptions(log.Id, null, isliveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Disclaimer Options' + e.getMessage() );} 
         
       try {importCourseDisclaimers(log.Id, qualIds, isliveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Course Disclaimers ' + e.getMessage() );} 
      
             
       try {insert integrationEvents;} catch (Exception e) {log.Integration_Message__c = 'Integration Events could be inserted' + e.getMessage();}
       
       update log;
         
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        	String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
       		mail.setToAddresses(toAddresses);  
        	mail.setSubject('Website Course Integration Complete');  
        	mail.setPlainTextbody('CMS Sync complete, please see Integration log for more details ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id);
        	mail.setHTMLBody('CMS Sync complete, please see Integration log for more details, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id + '">click here</a> to check the log. <br>'
							);
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
     @future(callout=true) public static void importSingleDisclaimerOption(String logId,Set<Id> disclaimerIds, Boolean isLiveImport) {
        
       CMS_Integration_Log__c log = [Select Id, Integration_Message__c From CMS_Integration_Log__c Where id = : logId];
         
       log.Integration_Message__c = 'Success';
        
       try {importDisclaimerOptions(log.Id, disclaimerIds, isliveImport);}
       catch (Exception e) { (log.Integration_Message__c = 'Critial Error While Processing Disclaimer Options ' + e.getMessage() );}
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        	String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
       		mail.setToAddresses(toAddresses);  
        	mail.setSubject('Website Course Integration Complete');  
        	mail.setPlainTextbody('CMS Sync complete, please see Integration log for more details ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id);
        	mail.setHTMLBody('CMS Sync complete, please see Integration log for more details, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id + '">click here</a> to check the log. <br>'
							);
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       try {insert integrationEvents;} catch (Exception e) {log.Integration_Message__c = 'Integration Events could be inserted' + e.getMessage();}
        
        update log;
    }
    
    
    @future public static void resetUpdateRequiredFlagQual(Set<Id> qualIds) {
        List<Qualification__c> qualstoreset = new List<Qualification__c>();
        
            for(Qualification__c q : [Select Website_Live_Upsert_Required__c,Website_Update_Required__c from Qualification__c where id in :qualids And Website_Update_Required__c = true]) {
                q.Website_Update_Required__c = false;
                q.Website_Live_Upsert_Required__c = false;
                qualstoreset.add(q);
                
            }
        
        update qualstoreset;
            
        
    }
    
    @future public static void resetUpdateRequiredFlagWD(Set<Id> wdIds) {
        List<Website_Disclaimer__c> wdsToReset = new List<Website_Disclaimer__c>();
        
            for(Website_Disclaimer__c wd : [Select Website_Live_Upsert_Required__c,Website_Update_Required__c from Website_Disclaimer__c where id in :wdIds And Website_Update_Required__c = true]) {
                wd.Website_Update_Required__c = false;
                wd.Website_Live_Upsert_Required__c = false;
                wdsToReset.add(wd);
                
            }
        
        update wdsToReset;
            
        
    }
    
    /*handle disclaimer trigger data */
    
    public static void handleDisclaimers(List<Website_Disclaimer__c> disclaimers, Map<id,Website_Disclaimer__c> olddisclaimers) {
        
        set<Id>wdIdsStaging = new Set<Id>();
        set<Id>wdIdsLive = new Set<Id>();
        
        for(Website_Disclaimer__c ws : disclaimers) {
            
            //Visible on website flag has been ticked, this will trigger initial integration
            if(olddisclaimers.get(ws.Id).Website_Update_Required__c == false && ws.Website_Update_Required__c == true)
            	wdIdsStaging.add(ws.Id);
            
            if(olddisclaimers.get(ws.Id).Website_Live_Upsert_Required__c == false && ws.Website_Live_Upsert_Required__c == true)
                wdIdsLive.add(ws.Id);
        }        
        
        
        if(wdIdsStaging.size() > 0) {
            
            CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
            insert log;
            
            //Tested outside of this trigger
            if(!Test.isRunningTest()) {
                importSingleDisclaimerOption(Log.Id,wdIdsStaging, false);
                resetUpdateRequiredFlagWD(wdIdsStaging);
            }
        }
    
        if(wdIdsLive.size() > 0) {
            
            CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Live', Integration_Message__c = 'Processing' );
            insert log;
            
            //Tested outside of this trigger
            if(!Test.isRunningTest()) {
                importSingleDisclaimerOption(Log.Id,wdIdsLive, false);
                resetUpdateRequiredFlagWD(wdIdsLive);
            }
        }
        
        
        
    }
    
   
        
  
    /*This class is used to deserialize reponse from Jitterbit/CMS */
    private class CMSResponse {
        String status; 
        String message;
        
    }

}