/**
 * @description Controller for RemoveFromClasses page
 * @author Janella Lauren Canlas
 * @date 18.MAR.2013
 * @history
 * 		 18.MAR.2013	Janella Canlas		- Created.
 * 		 19.MAR.2013	Janella Canlas		- Fix code to delete Empty Enrolment Intake Units
 * 		 20.MAR.2013	Janella Calnas		- Update EIU deletion code
 * 		 16.OCT.2013	Warjie Malibago		- Add code that will delete student in Intake if he has no more class enrolment in the intake
 * 		 06.NOV.2013	Warjie Malibago		- Add code that will remove EUOS's Census Date if student is removed from Scheduled Units and Intake
 * 		 25.NOV.2013	Warjie Malibago		- Make Total Amount Charged to null if student is removed in all classes.
 * 		 10.DEC.2013	Michelle Magsarili	- Additional Filter 'Student Name' on List Class Enrolment query under removeEnrolment method
 * 		 02.APR.2014	Warjie Malibago		- display class enrolments in order (by date)
 * 		 11.AUG.2014	Warjie Malibago		- display only CEs without Attendance
 * 		 28.APR.2015	Deepak Moudekar		- check all CEs checkbox
 *		 21.MAR.2016	Ranyel Maliwanag	- Added the removal of Census records upon unscheduling
 *     04.APR.2016  Ranyel Maliwanag  - Fixed bug with unscheduling from a specific intake but all the other census records gets removed
 */
public with sharing class RemoveFromClasses_CC {  
  public Id enrolmentId;
  public List<Class_Enrolment__c> classEnrolments {get;set;}
  public List<WrapperClass> pageWrapperList{get;set;}
  public Set<Id> classIds;
  public Id intakeUnit {get;set;}
  public Id intake {get;set;}
  public List<Classes__c> classes;
  public Boolean choseIntakeUnit {get;set;}
  public Boolean choseAll {get;set;}
  
  public RemoveFromClasses_CC(){
    enrolmentId = ApexPages.currentPage().getParameters().get('enrolmentId');//retrieves the intake ID passed from the button
    choseIntakeUnit = false;
    choseAll =false;
    
    fillPageWrapper();
  }
  
  public Enrolment__c getenrolmentRec(){
    Enrolment__c enrol = [select Id, Student__r.Name, RecordType.Name from Enrolment__c where Id =: enrolmentId];
    return enrol;
  }
  //intake unit picklist above the pageblocktable
  public List<SelectOption> getIntakeUnitPicklist() {
    List<Classes__c> tempClass = new List<Classes__c>();
    List<SelectOption> options = new List<SelectOption>();
    Set<String> iuIds = new Set<String>();
    tempClass = classes;

    //collect intake unit ids
    for(Classes__c c : tempClass){
      iuIds.add(c.Intake_Unit__c);
    }
        //populate picklist
        options.add(new SelectOption('', ''));   
        for (Intake_Unit__c r : [SELECT Id, Name,Unit_of_Study__r.Name, Unit_Name__c from Intake_Unit__c where Id IN: iuIds]) {
            options.add(new SelectOption(r.Id, r.Unit_of_Study__r.Name + ' - '+ r.Unit_Name__c + ' - ' + r.Name));
        }  
        return options;
    }
    //intake picklist
    public List<SelectOption> getIntakePicklist() {
    List<Classes__c> tempClass = new List<Classes__c>();
    List<SelectOption> options = new List<SelectOption>();
    Set<String> iuIds = new Set<String>();
    tempClass = classes;

    //collect intake unit ids
    for(Classes__c c : tempClass){
      iuIds.add(c.Intake__c);
    }
        //populate picklist
        options.add(new SelectOption('', ''));   
        for (Intake__c i : [SELECT Id, Name from Intake__c where Id IN: iuIds]) {
            options.add(new SelectOption(i.Id, i.Name));
        }  
        return options;
    }
    //wrapper class of Class Enrolment Record and Checkbox value
  public class WrapperClass {
      public Class_Enrolment__c c {get;set;}
      public Boolean check {get;set;}
      public WrapperClass(Class_Enrolment__c passClassRecord, Boolean check) {   
        c = passClassRecord;
        this.check = check;
      }
    }
    //fill page table
    public void fillPageWrapper(){
      system.debug('**choseIntakeUnit:'+choseIntakeUnit);
      system.debug('***choseAll***'+choseAll);
      pageWrapperList = new List<WrapperClass>();
      classIds = new Set<Id>();
      classes = new List<Classes__c>();
      List<Class_Enrolment__c> cenrol = new List<Class_Enrolment__c>();
      //query class enrolments related to the Enrolment Id
      cenrol = [select Id, Name, Class_End_Date_Time__c, Class_Start_Date_Time__c,Enrolment_Intake_Unit__r.Intake_Unit__r.Census_Date__c,
            Unit_Code__c, Unit_Name__c, Class__c, Class__r.Intake_Unit__c, Class__r.Intake__c, Enrolment_Intake_Unit__c
            from Class_Enrolment__c where (Enrolment__c =: enrolmentId OR Enrolment_Intake_Unit__r.Enrolment__c =: enrolmentId)
            AND Attendance_Type__c = NULL
            ORDER BY Class_Start_Date_Time__c ASC]; //11AUG2014 WBM         
      for(Class_Enrolment__c c : cenrol){
        Boolean ch = false;
        //if intake unit checkbox is chosen, compare with intake unit Id
        if(choseIntakeUnit){
          if(intakeUnit != null){
            if(c.Class__r.Intake_Unit__c == intakeUnit){
              ch = true;
            }
          }
        }
        else
        if(choseAll){
              //select all class
               ch = true;
        }
        else{
          //else compare with intake unit
          if(intake != null){
            if(c.Class__r.Intake__c == intake){
              ch = true;
            }
          }
        }
        WrapperClass wc = new WrapperClass(c,ch);
        pageWrapperList.add(wc);
        //for Intake and Intake Unit picklists
         classIds.add(c.Class__c);
      }
      //for Intake and Intake Unit picklists
      classes = [select Id, Intake_Unit__c, Intake__c from Classes__c where Id IN: classIds];
    }
    //return to the enrolment without any changes
    public PageReference cancel(){
      PageReference p = new PageReference('/'+enrolmentId);
      p.setredirect(true);
      return p;
    }
    //this will delete class enrolments chosen,and eiu records if there are no class enrolment records related to it
    public PageReference removeEnrolment(){
      List<Class_Enrolment__c> remainingCenrol = new List<Class_Enrolment__c>();
      List<Class_Enrolment__c> cenrolDelete = new List<Class_Enrolment__c>();
      List<Enrolment_Intake_Unit__c> eiuForDelete = new List<Enrolment_Intake_Unit__c>();
      Set<Id> eiuIds = new Set<Id>();  
      Map<Id,Class_Enrolment__c> classMap = new Map<Id,Class_Enrolment__c>();
      Set<Id> classEnrolmentIdsDEL = new Set<Id>();
      
      //WBM Start 10.16.2013
      Set<Id> selIntakeId = new Set<Id>();
      Set<Id> unselIntakeId = new Set<Id>();
      Set<String> studName = new Set<String>();
      Set<Id> intakeForDel = new Set<Id>();
      List<Intake_Students__c> student = new List<Intake_Students__c>([SELECT Student__r.Name FROM Intake_Students__c WHERE Enrolment__c =: enrolmentId]);
      List<Class_Enrolment__c> unselCE = new List<Class_Enrolment__c>();
      //WBM End
      
      //redirect to enrolment record if process is successful
      /*
      PageReference p = new PageReference('/'+enrolmentId);
      */
      
      //11.AUG.2014 WBM
      PageReference p = new PageReference('/'+enrolmentId);
      if(getenrolmentRec().RecordType.Name == 'Dual Funding Enrolment' || getenrolmentRec().RecordType.Name == 'VET Fee Help Enrolment'){
          p = new PageReference('/apex/EnrolmentUnitList?eId=' + enrolmentId);
      }
      p.setredirect(true);
      
      for(WrapperClass w : pageWrapperList){
        //delete if checked
        if(w.check){
          cenrolDelete.add(w.c);
          eiuIds.add(w.c.Enrolment_Intake_Unit__c);
          classEnrolmentIdsDEL.add(w.c.Id);
          selIntakeId.add(w.c.Class__r.Intake__c);
        }
        else{
            unselIntakeId.add(w.c.Class__r.Intake__c);
            unselCE.add(w.c);
        }
      }
      try{       
        if(selIntakeId.size() > 0){ //if there's a selection
                delete cenrolDelete;
            
            //WBM Start 10.16.2013
            for(Intake_Students__c is : student){
                studName.add(is.Student__r.Name);
            }
            String status;
            if(unselCe.size() > 0){ //if there's CE left
                for(Class_Enrolment__c ce : unselCe){
                    if(!selIntakeId.contains(ce.Class__r.Intake__c)){
                        intakeForDel.add(ce.Class__r.Intake__c);
                    }
                    else{
                        status = 'NOT OK';
                    }
                }
                
                if(intakeForDel.size() > 0){
                    List<Intake_Students__c> iStud = new List<Intake_Students__c>([SELECT Id, Name FROM Intake_Students__c WHERE Intake__c NOT IN: intakeForDel AND Student__r.Name =: studName]);
                    List<Census__c> censusesForDelete = [SELECT Id FROM Census__c WHERE IntakeId__c NOT IN :intakeForDel AND EnrolmentId__c = :enrolmentId];
                    System.debug('Censuses for Delete 1 ::: ' + censusesForDelete);
                    List<Intake_Students__c> iStudForDelete = new List<Intake_Students__c>();
                    for(Intake_Students__c is : iStud){
                        iStudForDelete.add(is);
                    }
                    
                    if(iStudForDelete.size() > 0){
                        if(status != 'NOT OK'){
                            delete iStudForDelete;
                            delete censusesForDelete;
                        }
                    }
                }                
            }
            else if(unselCe.size() < 1){ //if no CE left
                for(ID i : selIntakeId){
                    intakeForDel.add(i);
                }
                
                if(intakeForDel.size() > 0){
                    List<Intake_Students__c> iStud = new List<Intake_Students__c>([SELECT Id, Name FROM Intake_Students__c WHERE Intake__c IN: intakeForDel AND Student__r.Name =: studName]);
                    List<Census__c> censusesForDelete = [SELECT Id FROM Census__c WHERE IntakeId__c IN :intakeForDel AND EnrolmentId__c = :enrolmentId];
                    System.debug('Censuses for Delete 2 ::: ' + censusesForDelete);
                    List<Intake_Students__c> iStudForDelete = new List<Intake_Students__c>();
                    for(Intake_Students__c is : iStud){
                        iStudForDelete.add(is);
                    }
                    
                    if(iStudForDelete.size() > 0){
                        delete iStudForDelete;
                        delete censusesForDelete;
                    }
                }                
            }
            
            //WBM End
            
          
            
            //MAM 12/10/2013 start Fix Query to avoild Too many Query rows limit
            //remainingCenrol = [select Id, Enrolment_Intake_Unit__c from Class_Enrolment__c where Enrolment_Intake_Unit__c IN: eiuIds AND Id not in:classEnrolmentIdsDEL];
            //Narrow retrieved record by Name 
            remainingCenrol = [select Id, Enrolment_Intake_Unit__c from Class_Enrolment__c 
                               where Enrolment_Intake_Unit__c IN: eiuIds AND Id not in:classEnrolmentIdsDEL AND
                                     Student_Name__c LIKE : '%' + getenrolmentRec().Student__r.Name + '%'];
            
           
            //query eiu records
            List<Enrolment_Intake_Unit__c> eiuList = [select Id, Enrolment_Unit__c from Enrolment_Intake_Unit__c where id IN: eiuIds];
            Set<Id>setEuIds9 = new Set<Id>();
            
            /*start null out Census Date and Total Amount charged
             *02/06/2014 This class was updated by Yuri Gribanov, Cloud Sherpas Australia. 
             This piece of code sets a NULL value for the Census_Date__c field and Total_Amount_Charged__c field on an Enrolment Unit of  Study ONLY IF certain conditions are met.
             These conditions are required because to clear out these values is quite a significant process to complete in terms of the impact to Careers Australia so it must be done
             with care. The Conditions to set these two fields to NULL are: 

             1. For the Class Enrolment Record being deleted the Census Date on the related Intake Unit must be equal to the Census Date on the Enrolment Unit of Study
             2. For the Class Enrolment Record being deleted the Unit_Code__c must be equal to the Parent_Uoc__c on the Enrolment Unit of Study 
             3. For the Classs Enrolment Record being deleted the Census Date on the Enrolment Unit of Study must be greater then "TODAY"
             4. Find the number of Class Enrolment Records for the Enrolment Unit that the Enrolment Unit of Study relates to and if there ensure there are 0 

             NOTE: ONLY IF ALL OF THE FOUR CONDITIONS ABOVE ARE TRUE WILL THE CENSUS DATE AND TOTAL AMOUNT CHARGED FIELDS ON THE ENROLMENT UNIT OF STUDY BECOME NULL 

             If you have any questions regarding this piece of code please contact either, Scott Gassmann or Yuri Gribanov from Cloud Sherpas or Beth Collins from Careers Australia
            
             */
             List<Enrolment_Unit_of_Study__c> euosForUpdate9 = new List<Enrolment_Unit_of_Study__c>();
             for(Enrolment_Intake_Unit__c eiu : [SELECT Id, Enrolment_Unit__c,(Select id From Class_Enrolments__r) FROM Enrolment_Intake_Unit__c WHERE Id IN: eiuList]){
                     if(eiu.Class_Enrolments__r.size() == 0)
                        setEuIds9.add(eiu.Enrolment_Unit__c);
             }
             
        
             
             for(Enrolment_Unit_of_Study__c euos : [SELECT Id,Parent_Uoc__c, Census_Date__c, Total_amount_charged__c, Loan_Fee__c FROM Enrolment_Unit_of_Study__c WHERE Enrolment_Unit__c IN: setEuIds9]){
                  for(Class_Enrolment__c c : cenrolDelete) {
                     
                     if(euos.Census_Date__c == c.Enrolment_Intake_Unit__r.Intake_Unit__r.Census_Date__c &&
                        euos.Parent_Uoc__c ==  c.Unit_Code__c &&
                        euos.Census_Date__c > System.Today()) {
                        
                     system.debug('@@@@in3');
                        euos.Census_Date__c = null;
                        euos.Total_amount_charged__c = null;
                        euos.Loan_Fee__c = null;
                        euosForUpdate9.add(euos);
                    }
                }
             }
             update euosForUpdate9;
             
             /*end null out Census Date and Total Amount charged */
            
            
            
            if(remainingCenrol.size() > 0){
              //create map of enrolment intkae units and class enrolments
              for(Class_Enrolment__c c : remainingCenrol){
                classMap.put(c.Enrolment_Intake_Unit__c,c);
              }  
              //if an eiu is not on the map, it means there are no class enrolment records related to it
              for(Enrolment_Intake_Unit__c e : eiuList){
                if(!classMap.containsKey(e.Id)){
                  eiuForDelete.add(e);
                }
              }
              
              if(eiuForDelete.size() > 0) {delete eiuForDelete;}
              //if there are eiu for deletion, delete.
             
            }
            //if there are no remaining class enrolments related to the EIU ids, delete eiu records
            else{
                //WBM Start 11.05.2013
                 delete eiuList;
            }              
        }
        return p;
      }
      catch(Exception e){
        //display error if there is an exception
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'ERROR removing Class Enrolments: ' + e.getMessage()));
        return null;
      }
    }
}