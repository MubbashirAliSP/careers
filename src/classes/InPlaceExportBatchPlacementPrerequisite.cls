/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 19.JUN.2015
 */
public without sharing class InPlaceExportBatchPlacementPrerequisite implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchPlacementPrerequisite() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPE_Attribute_Value__c, IPE_Entity_Code__c, IPE_Er_Ref__c, IPE_Expiry_Date__c, IPE_InPlace_Entity__c, IPE_Sync_on_next_update__c, Name, Id ';
        queryString += 'FROM Completed_Placement_Prerequisites__c ';
        queryString += 'WHERE IPE_InPlace_Entity__c = true AND IPE_Sync_on_next_update__c = true ORDER BY IPE_Entity_Code__c';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 19.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 19.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Completed_Placement_Prerequisites__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Completed_Placement_Prerequisites__c record : records) {
            if (!exportedIdentifiers.contains(record.IPE_Entity_Code__c + record.IPE_Er_Ref__c)) {
                InPlace__c forExport = new InPlace__c(
                        // IPEntityRelationshipEntity_Action__c = 'APPEND',
                        IPEntityRelationshipEntity_AttrValue__c = 'Completed',
                        IPEntityRelationshipEntity_EntityCode__c = record.IPE_Entity_Code__c,
                        IPEntityRelationshipEntity_ErRef__c = record.IPE_Er_Ref__c,
                        IPEntityRelationshipEntity_ExpiryDate__c = record.IPE_Expiry_Date__c
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPE_Entity_Code__c + record.IPE_Er_Ref__c);
            }
            
            record.IPE_Sync_on_next_update__c = false;
        }

        insert exports;

        if (settings.ToggleSyncFlagOnExport__c) {
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 19.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
    }
}