/*This class handles Web Service course object component for Blackboard Integration
 *It will retrieve Blackboard_course__c records that are only
 *T2/T3(get,update)/T4/T5 
 *SIS t3 insert is handled by BlackBoardCourseSISSyncBatch.class
 *create by Yuri Gribanov 30th May 2014 - CloudSherpas
*/



global with sharing class BlackboardCourseSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {


  global final String query = 'Select Available_After_Sync__c,Get_T3_PK_Attempted__c, Course_Description__c, SIS_Sync_Attempted__c, Date_of_Last_Sync__c, Blackboard_Primary_Key__c, RecordType.DeveloperName,Id, Name, create__c,deactivate__c,Reactivate__c,Update__c, Blackboard_Course_Name__c, Available_In_BlackBoard__c From Blackboard_Course__c Where Sync_Required__c = True';
  
  
  global database.querylocator start(database.batchablecontext BC){
       return Database.getQueryLocator(query);
  }
  
  
  global void execute(Database.BatchableContext BC,List<SObject> scope){
     
     //go through blackboard course one by one and route to correct WS method
     for(SObject s :scope) {
             
             Blackboard_Course__c bbc = (Blackboard_Course__c) s;

             //Type2 AND Type5
             if(bbc.RecordType.DeveloperName == 'T2_Template_Course' || bbc.RecordType.DeveloperName == 'T5_Support_Materials') {
                BlackboardCourseWS.saveCourse(bbc);

             }

             //type3
             if(bbc.RecordType.DeveloperName == 'T3_Intake_Course') {

                if(bbc.SIS_Sync_Attempted__c)
                   BlackboardCourseWS.getCourse(bbc);

                else {
                    if(bbc.Update__c || bbc.Reactivate__c || bbc.Deactivate__c)
                       BlackboardCourseWS.saveCourse(bbc);
                }
             }

             //Type4
             if(bbc.RecordType.DeveloperName == 'T4_Organisation') {

                //create org
                if(bbc.create__c)
                  BlackboardCourseWS.createOrg(bbc);

                else {
                  BlackboardCourseWS.updateOrg(bbc);
                }

             }

     }
    
   
  }
  
  global void finish(Database.BatchableContext info){
  
      BlackboardScheduleUnitJ2A b1 = new BlackboardScheduleUnitJ2A();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
  
      
  
  }
  
  

}