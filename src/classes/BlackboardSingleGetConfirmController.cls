public with sharing class BlackboardSingleGetConfirmController {

    public BlackboardSingleGetConfirmController() {
        
    }

    /*will perform get operation here */
    public pageReference action() {

        if(ApexPages.currentPage().getParameters().get('ObjectType') == 'Blackboard_Course__c') {
            BlackboardCourseWS.singleGetOperation(ApexPages.currentPage().getParameters().get('Id'));
        }

        return null;

    }
}