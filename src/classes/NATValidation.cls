public class NATValidation {
    
    private static final Integer max_query_length = 131072;
    
    private List<Nat_Validation_Event__c> eventList = new List<Nat_Validation_Event__c>();
    
    private static NAT_Validation_Log__c createLog(String state, Id trainingOrgId, String queryStudents, String querymain, String year, Integer NAT00120RecCount, Boolean deleteOldFiles) {
        
        NAT_Validation_Log__c log;
        
        if(queryStudents.length() > max_query_length)
             log = new NAT_Validation_Log__c(Validation_State__c = state, Training_Organisation_Id__c = trainingOrgId, query_students__c = queryStudents.Substring(0,max_query_length), query_students_2__c =  queryStudents.Substring(max_query_length), query_main__c = querymain, Collection_Year__c = year, NAT00120_Record_Count__c = NAT00120RecCount, Delete_Existing_NAT_Files__c = deleteOldFiles);
        
        else { 
        
             log = new NAT_Validation_Log__c(Validation_State__c = state, Training_Organisation_Id__c = trainingOrgId, query_students__c = queryStudents,query_students_2__c = '', query_main__c = querymain, Collection_Year__c = year,NAT00120_Record_Count__c = NAT00120RecCount, Delete_Existing_NAT_Files__c = deleteOldFiles);
        }
        insert log;
       
       /*try { insert log; }
        catch(DMLException e) {} */
        
        return log;
    
    }

     public static NAT_Validation_Log__c createLog() {
        
        NAT_Validation_Log__c log = new NAT_Validation_Log__c();
        
        try { insert log; }
        catch(DMLException e) {}
        
        return log;
    
    }
    
    //to confirm the end of process
    private static void finish(String logId) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('NAT Validation is Complete');  
        mail.setPlainTextbody('NAT Validation completed, please click on the URL to check the log. ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + logId);
        mail.setHTMLBody('NAT Validation completed, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + logId + '">click here</a> to check the log.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    
    }
    
    //This method will be called to perform validation
    @future public static void validate(String querymain, String querystudents, String queryshort, String state, String year,Id tOrgId,Integer NAT00120RecCount, Boolean deleteOldFiles) {
        
        String system_message = 'No System Errors';
        //create log
        NAT_Validation_Log__c log = createLog(state, tOrgId,queryStudents, querymain + ' ORDER BY Student_Identifier__c', year, NAT00120RecCount, deleteOldFiles);

        try { //NAT00020
            if(state != 'New South Wales') {
                log.NAT00020_Record_Count__c =  NatValidationMethods.validateNAT00020(log.Id, queryshort);
            }
            else {
               log.NAT00020_Record_Count__c = 0; 
            }
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00020 :' + e.getMessage();
        }
        
        try { //NAT00030
            if(state != 'New South Wales') {
                log.NAT00030_Record_Count__c =  NatValidationMethods.validateNAT00030(log.Id, queryshort);
            }
            else {
               log.NAT00030_Record_Count__c = 0; 
            }
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00030 :' + e.getMessage();
        }
        
        try { //NAT00060
            log.NAT00060_Record_Count__c =  NatValidationMethods.validateNAT00060(log.Id, queryshort);
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00060 :' + e.getMessage();
        }
        
        try { //NAT00080 and NAT00085
            log.NAT00080_Record_Count__c =  NatValidationMethods.validateNAT00080(log.Id, querystudents); 
            log.NAT00085_Record_Count__c =  log.NAT00080_Record_Count__c;
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00080 :' + e.getMessage();
        }        
        try{ //NAT00090
            log.NAT00090_Record_Count__c =  NatValidationMethods.validateNAT00090(log.Id, querystudents);
        }  
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00090 :' + e.getMessage();
        }
        
        try{ //NAT00100
            log.NAT00100_Record_Count__c =  NatValidationMethods.validateNAT00100(log.Id, querystudents);
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00100 :' + e.getMessage();
        }
        
        try{ //NAT00120
             NatValidationMethods.validateNAT00120(log.Id, querymain);
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00120 :' + e.getMessage();
        }
        
        try{ //NAT00130
            log.NAT00130_Record_Count__c = NatValidationMethods.validateNAT00130(log.Id, queryshort);
        } 
        catch(exception e) {
            log.System_Message__c = 'Error Has Occured While Validating NAT00130 :' + e.getMessage();
        }
        
        
       // try {NatValidationMethods.validateNAT00020(log.Id, querymain);} catch(exception e){}
       // try {NatValidationMethods.validateNAT00030(log.Id, query30);} catch(exception e){}
      //  try {NatValidationMethods.validateNAT00060(log.Id, query60);} catch(exception e){}
     //   try {NatValidationMethods.validateNAT00080(log.Id, querystudents);} catch(exception e){}
     //   try {NatValidationMethods.validateNAT00120(log.Id, querymain);} catch(exception e){}
       // try {NatValidationMethods.validateNAT00130(log.Id, query130);} catch(exception e){}

      
        log.Cpu_Time_Used__c = Limits.getCpuTime();
        if(log.System_message__c == null)
           log.System_Message__c =  'No System Errors';


        update log;

        finish(log.Id);
       
    
    }



}