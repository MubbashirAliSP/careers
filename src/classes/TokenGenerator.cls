public with sharing class TokenGenerator {
	
	final List<String> chars = new List<String>{'A','B', 'C', 'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	
	
	
	/*method to generate random token */
		public String generateToken() {
			
			
			String token ='';
		
		
			for(Integer i = 0; i < 32; i++) {
				
				Integer l;
				
				l = Math.Round((Math.random()) * 65);
				
				token = token + chars.get(l);
				
			}
			
			
			return token;
		} 
		
		
		public List<String> generateTokens(Integer numTokens) {
			
			List<String> tokens = new List<String>();
			
		
			 for(Integer j=0; j < numTokens; j++) {
				  String token = '';
					
				  for(Integer i = 0; i < 32; i++) {
				
						Integer l;
				
						l = Math.Round((Math.random()) * 61);
				
						token = token + chars.get(l);
				
					}
					
					
				    tokens.add(token);
				
			   }
			
			
			return tokens;
			
			
		}
	
	
}