public with sharing class GlobalUtility {
	
	
	public static boolean isNumeric(String str){
			Pattern p = Pattern.compile('^[0-9]+$');
			Matcher MyMatcher = p.matcher(str);
			return MyMatcher.matches();
	}
	
    public static Integer formatAgeFromDateOfBirth(Date birthdate) {
    	
    	if(birthdate == null)
    		return 0;
    	else
    	  return Integer.valueOf(System.Today().daysBetween(birthdate) / -365.2425);
    	
    	
    }
    
    public static Integer calculateYearsBetweenDates(Date StartDate, Date EndDate) {
    	return Integer.ValueOf(StartDate.daysBetween(EndDate) / 365.2425) + 1;
    	
    }
    
    public static String stateNameConvert(String state) {
    	
    	if(state == 'NSW')
    		return 'New South Wales';
    		
    	if(state == 'QLD')
    		return 'Queensland';
    	
    	if(state == 'VIC')
    		return 'Victoria';
    	
    	if(state == 'SA')
    		return 'South Australia';
    		
    	if(state == 'WA')
    		return 'Western Australia';
    	
    	if(state == 'NT')
    		return 'Northern Territories';
    	
    	if(state == 'ACT')
    		return 'Australian Capital Territory';
    	
    	if(state == 'TAS')
    		return 'Tasmania';
    		
    	return null;
    }
    
    public static String formatStudentId(String stId) {
    	
    	if(stId == null)
    		return '';
    	
    	String returnString = '';
    	
    	returnString = stId;
    	
    	returnString.replace('-','');
    	returnString.replace(' ', '');
    	
    	return returnString;
    	
    }
	
	

}