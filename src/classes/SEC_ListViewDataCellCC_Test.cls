/**
 * @description Test class for `SEC_ListViewDataCellCC`
 * @author Ranyel Maliwanag
 * @date 16.FEB.2016
 */ 
@isTest
public with sharing class SEC_ListViewDataCellCC_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		insert enrolment;
	}

	@testSetup
	static void setupUser() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User testUser = SASTestUtilities.createUser();
		testUser.ProfileId = p.id;
		testUser.Email = 'olympia@careersaustralia.edu.au.test';
		testUser.Username = 'olympia@careersaustralia.edu.au.test';
		insert testUser;
	}

	static testMethod void settersTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
		Account student = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = student;
			controller.fieldPath = 'Name';
			controller.fieldType = 'string';

			System.assertNotEquals(null, controller.fieldValue);
		Test.stopTest();
	}

	static testMethod void dateAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
		Account student = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = student;
			controller.fieldPath = 'CreatedDate';
			controller.fieldType = 'date';

			System.assertNotEquals(null, controller.dateValue);
		Test.stopTest();
	}

	static testMethod void dateTimeAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
		Account student = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = student;
			controller.fieldPath = 'CreatedDate';
			controller.fieldType = 'datetime';

			System.assertNotEquals(null, controller.datetimeString);
		Test.stopTest();
	}

	static testMethod void referenceAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', Student__r.Name');
		Enrolment__c enrolment = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = enrolment;
			controller.fieldPath = 'Student__c';
			controller.fieldType = 'reference';

			System.assertNotEquals(null, controller.fieldValue);
			System.assertNotEquals(null, controller.referenceName);
			System.assertNotEquals(null, controller.studentIdReference);
		Test.stopTest();
	}

	static testMethod void resultRequestAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', Student__r.Name');
		Enrolment__c enrolment = Database.query(queryString);

		Result_Request__c request = new Result_Request__c(
				Enrolment_Record__c = enrolment.Id
			);
		insert request;

		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = request;
			controller.fieldPath = 'Enrolment_Record__c.Student_Name__c';
			controller.fieldType = 'string';

			System.assertNotEquals(null, controller.resultRequestReference);
		Test.stopTest();
	}

	static testMethod void viewAsAccessorsTest() {
		User testUser = [SELECT Id FROM User WHERE Username = 'olympia@careersaustralia.edu.au.test'];
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.viewAs = testUser.Id;

			System.assertNotEquals(null, controller.trainerId);
		Test.stopTest();
	}


	static testMethod void booleanAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', Student__r.Name');
		Enrolment__c enrolment = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = enrolment;
			controller.fieldPath = 'IsVFHLoanFormSubmitted__c';
			controller.fieldType = 'boolean';

			System.assertNotEquals(null, controller.booleanValue);
		Test.stopTest();
	}


	static testMethod void idAccessorsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', Student__r.Name');
		Enrolment__c enrolment = Database.query(queryString);
		Test.startTest();
			SEC_ListViewDataCellCC controller = new SEC_ListViewDataCellCC();
			controller.record = enrolment;

			System.assertNotEquals(null, controller.recordId);
		Test.stopTest();
	}
}