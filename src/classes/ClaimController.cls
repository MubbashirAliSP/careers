/*Controls claiming for all states, Follow comments for each state for more info */

public with sharing class ClaimController {
    
    public List<Enrolment_Unit__c>FilteredEU {get;set;}
    
  
    
    public Claim_Number__c dummyClaimNumber {get;set;}
    
    public String selectedFile {get;set;}
    public Boolean includeUnclaimed {get;set;}
    public Boolean activeEnrl {get;set;}
    
    private final String state = ApexPages.currentPage().getParameters().get('state');
    private final Id rto = ApexPages.currentPage().getParameters().get('rto');
    private final String year = ApexPages.currentPage().getParameters().get('year');
    private final String isNSWClaim = ApexPages.currentPage().getParameters().get('claim');
    

    public String fundingSourceSearch {get;set;}
    
    public String ContractCodeSearch {get;set;}
    
    public String claimType {get;set;}
    
      
    public List<NSWEnrolmentStudent>NSWEnrolmentStudentList {get;set;}
   
    
    public List<Enrolment__c> FilteredEnrolments {get;set;}
    
    public List<PurchasingContractView> FilteredContracts {get;set;}
    
    public List<SelectOption>claimTypes  {get;set;}
    
    public List<SelectOption>NSWOptions {get;set;}
    
    public List<SelectOption>claimList {get;set;}
    
    private Id claimId;
    
    /*NAT CONTROLLER VARIABLES*/
    
    public List<DisabilityObject>disabilities {get;set;}
    public List<PriorEducationalAchievmentObject>priorAchievements {get;set;}
    public List<NatViewer>natFilesInfo {get;set;}
    
    public Set<Id>EnrolmentIds = new Set<Id>();
    public Set<String>ContractsCodes = new Set<String>();
    
    public transient List<Unit__c>UnitsOfComErrors {get;set;}
    public List<Unit__c>UnitsOfComErrorsLimitedView {get;set;}
    public List<Account>StudentsErrorsLimitedView {get;set;}
    public List<Account>StudentPostalErrorsLimitedView {get;set;}
    public List<Enrolment__c>enrolmentErrorsLimitedView {get;set;}
    public List<Enrolment_Unit__c>EUErrorsLimitedView {get;set;}
    public transient List<Account>StudentsErrors {get;set;}
    public transient List<Account>StudentPostalErrors {get;set;}
    public transient List<Enrolment__c>enrolmentsErrors {get;set;}
    public transient List<Enrolment_Unit__c>EUErrors {get;set;}
    /*END NAT CONTROLLER VARIABLES */
    

    
    public ClaimController () {
        
         claimTypes = new List<SelectOption>();
         claimTypes.add(new SelectOption('Commencement Claim', 'Commencement Claim'));
         claimTypes.add(new SelectOption('Midway Claim', 'Midway Claim'));
         claimTypes.add(new SelectOption('Final Claim', 'Final Claim'));
        
         dummyClaimNumber = new Claim_Number__c();
         
         
         activeEnrl = false;
         
         FilteredContracts = new List<PurchasingContractView>();
         NSWEnrolmentStudentList = new List<NSWEnrolmentStudent>();
        
            
        for(Purchasing_Contracts__c c : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state]) 
            FilteredContracts.add(new PurchasingContractView(c, false));
        
    }
    
    //do claiming here
    public void doClaim() {
        
        if(state == 'New South Wales') 
            claimId = ClaimGenerator.claimNSW(FilteredEnrolments, state,rto,dummyClaimNumber.Claim_Date__c,claimType );
        
    }
    
  
    
    public PageReference NSWNextButton() {
        FilteredEnrolments = new List<Enrolment__c>();
        
        for(NSWEnrolmentStudent es : NSWEnrolmentStudentList) {
        
                if(es.selected) {
                    FilteredEnrolments.add(es.Enrl);
                    EnrolmentIds.add(es.Enrl.Id);
                }
                
         }
        
         if(EnrolmentIds.size() == 0) {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'You Must Select At Least 1 Student'));
             return null;
         }
            
            
            
            
        
        
        
        
        
       
              
        
         /*Do Claim and return no normal validation screen */
         if(isNSWClaim == 'false')
            doClaim();
            
         createFileStructureNSW();
            
         return new PageReference('/apex/NatGeneratorValidationNSW');
         
    }
    
    public PageReference NSWBackButton() {return new PageReference('/apex/NSWClaimingPurchasingContractFilter');}
        
        
    
    
    
  

    
        /*Search NSW Enrolment, Students and Quals and populate results into NSWEnrolmentStudentList */
        
        public PageReference NSWSearch() {
            
            NSWEnrolmentStudentList = new List<NSWEnrolmentStudent>();
            
            dummyClaimNumber = new Claim_Number__c();
            
            Map<Id,Enrolment_Claims__c>ecMap  = new Map<Id,Enrolment_Claims__c>();
            
            ContractsCodes = new Set<String>();
                  //Selected Contract codes
                for(PurchasingContractView p : FilteredContracts ) {
                        if(test.isRunningTest())
                            p.selected = true;
                            
                        if(p.selected) 
                          ContractsCodes.add(p.pc.Contract_Code__c);
                          
                }         
                    
            //Need to filter based on commencement type
            
            
                    
            List<Enrolment__c> Enrols  = new List<Enrolment__c>([Select id, Name,
                                            Student__r.FirstName,
                                            Student__r.LastName,
                                            Student__r.PersonBirthDate,
                                            Student__r.Full_Student_Name__c,
                                            Student__r.Student_Identifer__c,
                                            Student__r.IsPersonAccount,
                                            Qualification__r.Name,
                                            Contract_Code__c,
                                            No_of_Enrolment_Claims__c,
                                            of_Enrolment_Completed__c
                                            
                                            FROM Enrolment__c
                                            WHERE Delivery_Location_State__c = : state AND
                                                  NSW_Claiming_Allowed__c = 'Yes' AND
                                                  Contract_Code__c in :ContractsCodes AND
                                                  No_of_Enrolment_Claims__c in : NATGeneratorUtility.claimCodes(claimType)
                                                  LIMIT 500
                                                  ]);
            
            
            for(Enrolment_Claims__c ec : [Select Id, Enrolment__c,of_Enrolment_Completed__c,createdDate FROM Enrolment_Claims__c WHERE Enrolment__c in : Enrols order by createdDate]) 
                ecMap.put(ec.Enrolment__c,ec);
         
            
            for(Enrolment__c e : Enrols) {
                if(e.No_of_Enrolment_Claims__c == 0)
                    NSWEnrolmentStudentList.add(new NSWEnrolmentStudent(false,null, e));
                else {
                    if(e.of_Enrolment_Completed__c > ecMap.get(e.Id).of_Enrolment_Completed__c)
                        NSWEnrolmentStudentList.add(new NSWEnrolmentStudent(false,Date.ValueOf(ecMap.get(e.Id).CreatedDate), e));
                }
                    
                 
                
            }                       
        
            return null;
            
        }
     
      
      public pageReference cancelButton() {return new PageReference('/apex/NATGeneratorStateSelection'); }
      public PageReference NextToClaiming() {return new PageReference('/apex/NSWClaiming?state=' +state + '&rto=' + rto); }
        
    
    
    
      
      
      public pageReference filterPurchasingContract() {
        //No Filtering
        FilteredContracts = new List<PurchasingContractView>();
        
        
        if(fundingSourceSearch == '' && ContractCodeSearch == '') {
             for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state] )
               FilteredContracts.add(new PurchasingContractView(pc, false));
          
            
        //Filtering applies
        } else {
            
                
                
                if(fundingSourceSearch !='' && ContractCodeSearch != '') {
                     ContractCodeSearch = '%' + ContractCodeSearch + '%';
                     List<String> searchParams = new List<String>();
                     searchParams = fundingSourceSearch.split(' AND ');
                
                    for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE State_Fund_Source__r.Code__c in : searchParams AND Contract_Code__c like : ContractCodeSearch AND Training_Organisation__c = : rto AND Contract_State__c = : state])
                        FilteredContracts.add(new PurchasingContractView(pc, false));
                }
                
                else if(fundingSourceSearch !='' && ContractCodeSearch == ''){
                    
                    List<String> searchParams = new List<String>();
               
                    searchParams = fundingSourceSearch.split(' AND ');
                    
                    for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE State_Fund_Source__r.Code__c in : searchParams AND Training_Organisation__c = : rto AND Contract_State__c = : state])
                        FilteredContracts.add(new PurchasingContractView(pc, false));
                    
                    
                }
                
                else if(fundingSourceSearch =='' && ContractCodeSearch != ''){
                    ContractCodeSearch = '%' + ContractCodeSearch + '%';
                    for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE Contract_Code__c like : ContractCodeSearch AND Training_Organisation__c = : rto AND Contract_State__c = : state])
                        FilteredContracts.add(new PurchasingContractView(pc, false));
                    
                    
                }
        
        
        }
        
       
        
        return null;
    }
     
    
    /*NAT LOGIC HERE - MOVED FROM NAT GENERATOR TO
     *WORK BETTER WITH GOVERNOR LIMITS
     *By YG, CloudSherpas 25/11/2013
     */
     
     
      public void createFileStructureNSW() {
    
         natFilesInfo = new List<NatViewer>();
         disabilities = new List<DisabilityObject>();
         priorAchievements = new List<PriorEducationalAchievmentObject>();
        
        
         natFilesInfo.add(New NatViewer('NAT00060 - Subjects', 0,'Ready For Validation', 0,0,0));                   
         natFilesInfo.add(New NatViewer('NAT00080 - Client', 0,'Ready For Validation', 0,0,0));        
         natFilesInfo.add(New NatViewer('NAT00085 - Client Postal Details', 0,'Ready For Validation', 0,0,0));          
         natFilesInfo.add(New NatViewer('NAT00090 - Disability', 0,'Ready For Validation', 0,0,0));          
         natFilesInfo.add(New NatViewer('NAT00100 - Client Prior Educational Achievement', 0,'Ready For Validation', 0,0,0));                  
         natFilesInfo.add(New NatViewer('NAT00120 - Enrolment',0, 'Ready For Validation', 0,0,0));        
         natFilesInfo.add(New NatViewer('NAT00130 - Program Completed', 0,'Ready For Validation', 0,0,0)); 
        
     }
    
     public pageReference generateNSWNatFiles() {
     
        NatGenerator.generateNAT00060_NSW(ContractsCodes, year,EnrolmentIds);
        NatGenerator.generateNAT0008090100_NSW(ContractsCodes, year,EnrolmentIds);
        NatGenerator.generateNAT00120_NSW(ContractsCodes, year,EnrolmentIds);
        NatGenerator.generateNAT00130_NSW(ContractsCodes, year,EnrolmentIds);
        
        return new PageReference('/apex/NATFilesGenerating');
      
     }
      
     public PageReference validateNSWNatFiles() {
        
        String fname =  ApexPages.currentPage().getParameters().get('fname');
        Integer maxErrorSize = 100;
        Integer Counter = 0;
     
        if(fname == 'NAT00060 - Subjects') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00060_NSW(ContractsCodes, year,EnrolmentIds);
            UnitsOfComErrorsLimitedView = new List<Unit__c>();
            UnitsOfComErrors = newOutcome.dynamicRecords;
            
            if(newOutcome.fileCount == 0) 
                natFilesInfo.set(0,New NatViewer('NAT00060 - Subjects', 0,'Fail', 0,1,0));
            
            else {
                 
                 UnitsOfComErrors = newOutcome.dynamicRecords;
                
                 if(UnitsOfComErrors.size() == 0)
                    natFilesInfo.set(0,New NatViewer('NAT00060 - Subjects', newOutcome.fileCount,'Success', 0,0,Limits.getCpuTime()));
                 else {
                    natFilesInfo.set(0,New NatViewer('NAT00060 - Subjects', newOutcome.fileCount,'Fail', 0,UnitsOfComErrors.size(),Limits.getCpuTime()));
                 }
                
             }
            
            for(Unit__c u : UnitsOfComErrors) {
                if(Counter < maxErrorSize) {
                  UnitsOfComErrorsLimitedView.add(u);
                  Counter ++;
                }
            }    
         
           
        }
        
        
        if(fname == 'NAT00080 - Client') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00080_NSW(ContractsCodes, year,EnrolmentIds);
            StudentsErrorsLimitedView = new List<Account>();
            StudentsErrors = newOutcome.dynamicRecords;
        
            if(StudentsErrors.size() == 0)
                natFilesInfo.set(1,New NatViewer('NAT00080 - Client', newOutcome.fileCount,'Success', 0,0,Limits.getCpuTime()));
            else {
                natFilesInfo.set(1,New NatViewer('NAT00080 - Client', newOutcome.fileCount,'Fail', 0,StudentsErrors.size(),Limits.getCpuTime()));
            }
            
            for(Account st : StudentsErrors) {
                if(Counter < maxErrorSize) {
                  StudentsErrorsLimitedView.add(st);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00085 - Client Postal Details') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00085_NSW(ContractsCodes, year,EnrolmentIds);
            StudentPostalErrorsLimitedView = new List<Account>();
            StudentPostalErrors = newOutcome.dynamicRecords;
           
            if(StudentPostalErrors.size() == 0)
                natFilesInfo.set(2,New NatViewer('NAT00085 - Client Postal Details', newOutcome.fileCount,'Success', 0,0,Limits.getCpuTime()));
            else {
                natFilesInfo.set(2,New NatViewer('NAT00085 - Client Postal Details', newOutcome.fileCount,'Fail', 0,StudentPostalErrors.size(),Limits.getCpuTime()));
            }
            
            for(Account st : StudentPostalErrors) {
                if(Counter < maxErrorSize) {
                  StudentPostalErrorsLimitedView.add(st);
                  Counter ++;
                }
            }
            
        }
        
        if(fname == 'NAT00090 - Disability') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00090_NSW(ContractsCodes, year,EnrolmentIds);
            
            natFilesInfo.set(3,New NatViewer('NAT00090 - Disability', newOutcome.fileCount,'Success', 0,0,Limits.getCpuTime()));
        }
            
        if(fname == 'NAT00100 - Client Prior Educational Achievement') {
            
            NATValidateOutCome newOutcome = NatValidator.validateNAT00100_NSW(ContractsCodes, year,EnrolmentIds);
            
            natFilesInfo.set(4,New NatViewer('NAT00100 - Client Prior Educational Achievement', newOutCome.fileCount,'Success', 0,0,Limits.getCpuTime()));
        }
        
        if(fname == 'NAT00120 - Enrolment') {
            
             EUErrorsLimitedView = new List<Enrolment_Unit__c>();
                NATValidateOutCome newOutcome = NatValidator.validateNAT00120_NSW(ContractsCodes, year,EnrolmentIds);
                
                if(newOutCome.fileCount == 0)
                   natFilesInfo.set(5, new NatViewer('NAT00120 - Enrolment',0, 'Fail', 0,1,Limits.getCpuTime()));
                else {
                
                
                    EUErrors = newOutCome.dynamicRecords;
                
                    if(EUErrors.size() == 0)
                        natFilesInfo.set(5, new NatViewer('NAT00120 - Enrolment',newOutcome.fileCount, 'Success', 0,0,Limits.getCpuTime()));
                    else {
                        natFilesInfo.set(5, new NatViewer('NAT00120 - Enrolment',newOutcome.fileCount, 'Fail', 0,EUErrors.size(),Limits.getCpuTime()));
                    }
                 
            
                  }
                  
            for(Enrolment_Unit__c eu : EUErrors) {
                if(Counter < maxErrorSize) {
                  EUErrorsLimitedView.add(eu);
                  Counter ++;
                }
            }
               
                
        }
        
        if(fname == 'NAT00130 - Program Completed') {
            
            enrolmentErrorsLimitedView = new List<Enrolment__c>();
            NATValidateOutCome newOutcome = NatValidator.validateNAT00130_NSW(ContractsCodes, year,EnrolmentIds);
            
            enrolmentsErrors = newOutCome.dynamicRecords;
            
                if(enrolmentsErrors.size() == 0)
                    natFilesInfo.set(6,New NatViewer('NAT00130 - Program Completed', newOutCome.fileCount,'Success', 0,0,Limits.getCpuTime()));
                else 
                    natFilesInfo.set(6,New NatViewer('NAT00130 - Program Completed', newOutCome.fileCount,'Fail', 0,enrolmentsErrors.size(),Limits.getCpuTime()));
                    
            for(Enrolment__c e : enrolmentErrorsLimitedView) {
                if(Counter < maxErrorSize) {
                  enrolmentErrorsLimitedView.add(e);
                  Counter ++;
                }
            }
            
        }
        
        return null;
     
     }
     
      public pageReference viewErrors() {
        
        PageReference redirect = new PageReference('/apex/NATGeneratorErrorViewNSW');
        
        redirect.getParameters().put('fname',selectedFile);
        
        return redirect;
    }
        
        public class NSWEnrolmentStudent {
            
            public boolean selected {get;set;}
            public Date lastClaim {get;set;}
            public Enrolment__c Enrl {get;set;}
            
            public NSWEnrolmentStudent(Boolean sel, Date lclaimDate, Enrolment__c enrolment ) {
                
                selected = sel;
                lastClaim = lclaimDate;
                Enrl = enrolment;
            }
            
        }
        
     
}