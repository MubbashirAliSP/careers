/* This class generates NAT files for NSW only
 * from claim Controller

 */
public with sharing class NATGenerator {
    
    private String currentState;
    public String libraryId;
    
    //need to set Library here
    public NATGenerator(String state) {
        
        
        
        
    }
    
    
    @future public static void generateNAT00060_NSW(Set<String>purchasingContractIds, String year,Set<Id>enrolmentIds) {
              
              String body60 = '';
              
              Map<Id,Decimal>Unit_NominalHoursMap = new Map<Id,Decimal>();
              
              String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
              
              for(Enrolment_Unit__c eu : [Select Unit__c, Line_Item_Qualification_Unit__r.Nominal_Hours__c From Enrolment_Unit__c Where Enrolment__c in : enrolmentIds ]) {
                    if(eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null)
                        Unit_NominalHoursMap.put(eu.Unit__c, 0.0);
                       else
                        Unit_NominalHoursMap.put(eu.Unit__c, eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c); 
              }
              
              
              
              
              
              for(Unit__c un : [Select Id,name,Unit_Name__c,Field_of_Education_Identifier__c,Vet_Non_Vet__c,Unit_Flag__c From Unit__c
                         WHERE Id in (Select Unit__c FROM Enrolment_Unit__c WHERE Enrolment__c in : enrolmentIds AND Purchasing_Contract_Identifier__c in : purchasingContractIds AND (CALENDAR_YEAR(End_Date__c) = : Integer.valueof(year.trim()) OR CALENDAR_YEAR(Start_Date__c) = : Integer.valueof(year.trim())) AND Unit__r.RecordType.Name = 'Unit of Competency') ]) {
                         
                
                    body60 += NATGeneratorUtility.formatOrPadTxtFile('C', NAT00060__c.getInstance('01').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Name, NAT00060__c.getInstance('02').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Unit_Name__c, NAT00060__c.getInstance('03').Length__c,'')+
                                NATGeneratorUtility.formatOrPadTxtFile(un.Field_of_Education_Identifier__c, NAT00060__c.getInstance('04').Length__c,'');
                         
                        if (un.Vet_Non_Vet__c) { 
                            body60 += NATGeneratorUtility.formatOrPadTxtFile('Y', NAT00060__c.getInstance('05').Length__c,'');
                        } else {
                            body60 += NATGeneratorUtility.formatOrPadTxtFile('N', NAT00060__c.getInstance('05').Length__c,'');
                        }
                       
                       body60 +=NATGeneratorUtility.formatHours(String.valueOf(Unit_NominalHoursMap.get(un.id).round()), NAT00060__c.getInstance('06').Length__c).substringBefore('.') + '\r\n';
                         
               }
               
               if(body60 == '')
                body60 = 'No Data, check configuration';
               
               Blob pBlob60 = Blob.valueof(body60);
               insert new ContentVersion(
               versionData =  pBlob60,
               Title = 'NAT00060',
               PathOnClient = '/NAT00060.txt',
               FirstPublishLocationId = libraryId); 
    
    
    }
    
     @future public static void generateNAT0008090100_NSW(Set<String>purchasingContractIds, String year,Set<Id>enrolmentIds) {
            
             List<String>splitter = new List<String>();
             String Salutation = '';
             String MailingAddress = '';
             String body80 = '';
             String body85 = '';
             String body90 = '';
             String body100 = '';
             String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
            
            
            for(Account st :[SELECT Student_Identifer__c, Highest_School_Level_Comp_Identifier__c, Year_Highest_Education_Completed__c, 
                            Sex__c, PersonBirthdate, PersonMailingPostaLCode, PersonMailingState,Indigenous_Status_Identifier__c, 
                            Main_Language_Spoken_at_Home__r.Name, Main_Language_Spoken_at_Home_Identifier__c, Labour_Force_Status_Identifier__c,  
                            Country_of_Birth_Identifer__c, At_School_Flag__c, 
                            Proficiency_in_Spoken_English_Identifier__c,Learner_Unique_Identifier__c, 
                            Has_prior_educational_achievement_s__c,Prior_Achievement_Flag__c,
                            Prior_Achievement_Type_s__c,Does_student_have_a_disability__c,Disability_Type__c,Disability_Flag_0__c, 
                            FirstName, LastName, PersonMailingStreet, Full_Student_Name__c,
                            PersonMailingCity, State_Identifier__c, PersonHomePhone, Work_Phone__c, Address_building_property_name__c,Address_flat_unit_details__c,Address_street_number__c,Address_street_name__c,Postal_Delivery_Box__c,
                            PersonMobilePhone, PersonEmail, Victorian_Student_Number__c, Salutation,Name_for_encryption_student__c,Age_Group__c,Unique_Student_Identifier__c
                            FROM Account WHERE id in (Select student__c From Enrolment__c Where Id in :enrolmentIds )] ) {
                 
                 
                  body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00080__c.getInstance('01').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Full_Student_Name__c, NAT00080__c.getInstance('02').Length__c,'')+
                            NATGeneratorUtility.newFormatTxtFile(st.Highest_School_Level_Comp_Identifier__c, NAT00080__c.getInstance('03').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Year_Highest_Education_Completed__c, NAT00080__c.getInstance('04').Length__c)+   
                            NATGeneratorUtility.formatOrPadTxtFile(st.Sex__c, NAT00080__c.getInstance('05').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(st.PersonBirthDate)), NAT00080__c.getInstance('06').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.PersonMailingPostalCode, NAT00080__c.getInstance('07').Length__c,'')+
                            NATGeneratorUtility.newFormatTxtFile(st.Indigenous_Status_Identifier__c, NAT00080__c.getInstance('08').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Main_Language_Spoken_at_Home_Identifier__c, NAT00080__c.getInstance('09').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Labour_Force_Status_Identifier__c, NAT00080__c.getInstance('10').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Country_of_Birth_Identifer__c, NAT00080__c.getInstance('11').Length__c)+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Disability_Flag_0__c, NAT00080__c.getInstance('12').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Prior_Achievement_Flag__c, NAT00080__c.getInstance('13').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.At_School_Flag__c, NAT00080__c.getInstance('14').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Proficiency_in_Spoken_English_Identifier__c, NAT00080__c.getInstance('15').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.PersonMailingCity, NAT00080__c.getInstance('16').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Unique_Student_Identifier__c, NAT00080__c.getInstance('17').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.State_Identifier__c, NAT00080__c.getInstance('18').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_building_property_name__c, NAT00080__c.getInstance('19').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_flat_unit_details__c, NAT00080__c.getInstance('20').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_number__c, NAT00080__c.getInstance('21').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_name__c, NAT00080__c.getInstance('22').Length__c,'');
         
             
            //NAT85
            Salutation = !String.IsEmpty(st.Salutation) ? st.Salutation.Replace('.', '') : '';
            MailingAddress = '';
            if(!String.IsEmpty(st.PersonMailingStreet)){
                MailingAddress =  st.PersonMailingStreet.Replace('\r\n', ' ');
                MailingAddress =  MailingAddress .Replace('\n', ' '); // To remove all instances of newlines and carriage returns
            }
            
            body85 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00085__c.getInstance('01').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(Salutation, NAT00085__c.getInstance('02').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.FirstName, NAT00085__c.getInstance('03').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.LastName, NAT00085__c.getInstance('04').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Address_building_property_name__c, NAT00085__c.getInstance('05').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Address_flat_unit_details__c, NAT00085__c.getInstance('06').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_number__c, NAT00085__c.getInstance('07').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_name__c, NAT00085__c.getInstance('08').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Postal_Delivery_Box__c, NAT00085__c.getInstance('09').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonMailingCity, NAT00085__c.getInstance('10').Length__c,'')+
                        NATGeneratorUtility.newformatTxtFile(st.PersonMailingPostalCode, NAT00085__c.getInstance('11').Length__c)+
                        NATGeneratorUtility.formatOrPadTxtFile(st.State_Identifier__c, NAT00085__c.getInstance('12').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonHomePhone, NAT00085__c.getInstance('13').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Work_Phone__c, NAT00085__c.getInstance('14').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonMobilePhone, NAT00085__c.getInstance('15').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonEmail, NAT00085__c.getInstance('16').Length__c,'')+                        
                        '\r\n';
                   
            if(st.Disability_Flag_0__c == 'Y') {
                splitter = st.Disability_Type__c.Split(';');
                for(String sp: splitter) {
                     body90 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00090__c.getInstance('01').Length__c,'')+
                               NATGeneratorUtility.formatOrPadTxtFile(sp.substring(0,2), NAT00090__c.getInstance('02').Length__c,'')+
                            '\r\n';
                }
                
            }
            
            if(st.Prior_Achievement_Flag__c == 'Y') {
                splitter = st.Prior_Achievement_Type_s__c.Split(';');
                for(String sp: splitter) {                        
                     body100 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT000100__c.getInstance('01').Length__c,'')+
                             NATGeneratorUtility.formatOrPadTxtFile(sp.substring(0,3), NAT000100__c.getInstance('02').Length__c,'')+
                             '\r\n';
                }
            }
               
                            
         }
         
        /*Insert Into Content */
        
        if(body80 == '')
            body80 = 'No Data, check configuration';
            
        if(body85 == '')
            body85 = 'No Data, check configuration';
            
       
        Blob pBlob80 = Blob.valueof(body80);
        insert new ContentVersion(
            versionData =  pBlob80,
            Title = 'NAT00080',
            PathOnClient = '/NAT00080.txt',
            FirstPublishLocationId = libraryId);
        
        Blob pBlob85 = Blob.valueof(body85);
        insert new ContentVersion(
           versionData =  pBlob85,
           Title = 'NAT00085',
           PathOnClient = '/NAT00085.txt',
           FirstPublishLocationId = libraryId); 
        
        if(body90 == '')
            body90 = ' ';  
        
        Blob pBlob90 = Blob.valueof(body90);
        insert new ContentVersion(
            versionData =  pBlob90,
            Title = 'NAT00090',
            PathOnClient = '/NAT00090.txt',
            FirstPublishLocationId = libraryId); 
        
        if(body100 == '')
            body100 = ' ';      
        
        Blob pBlob100 = Blob.valueof(body100);
        insert new ContentVersion(
            versionData =  pBlob100,
            Title = 'NAT00100',
            PathOnClient = '/NAT00100.txt',
            FirstPublishLocationId = libraryId);  
     
     
     }
     
     @future public static void generateNAT00120_NSW(Set<String>purchasingContractIds, String year,Set<Id>enrolmentIds) {
             
             String body120 = '';
             String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
             String NationalOutComeNSW = ''; // This is needed to convert formula field into blank, when identifier is 70
             
             
             map<Id,String>exMap = new Map<Id,String>();
             
             for(Fee_Exemption__c fee : [Select Id, Enrolment__c, Fee_Exemption_Type__r.Code__c From Fee_Exemption__c WHERE Enrolment__c in : enrolmentIds Order By createdDate])
                 exMap.put(fee.Enrolment__c,fee.Fee_Exemption_Type__r.Code__c);
             
             for(Enrolment_Unit__c eu : [Select Id,
                             Name,
                             Enrolment__r.Qualification__c,
                             Scheduled_Hours__c,
                             Unit_of_Competency_Identifier__c,
                             Training_Delivery_Location__c,
                             Unit__r.RecordType.Name,
                             Unit__r.Field_of_Education_Identifier__c,
                             Unit__r.Unit_Flag__c,
                             Unit__r.Vet_Non_Vet__c,
                             Enrolment__r.Student__c,
                             Enrolment__r.Student_Identifier__c,
                             Delivery_Location_Identifier__c,
                             Delivery_Mode_Identifier__c,
                             AVETMISS_National_Outcome__c,
                             Commencing_Course_Identifier__c,
                             Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c,
                             Purchasing_Contract_Identifier__c,
                             VET_in_Schools__c,
                             Registration_Number_Identifier__c,
                             Client_Identifier_New_Apprenticeships__c,
                             Client_Tuition_Fee__c,
                             Enrolment__r.Specific_Program_Identifier__c,
                             Enrolment__r.Training_Contract_Identifier__c,
                             Purchasing_Contract_Schedule_Identifier__c,
                             Hours_Attended__c,
                             Course_Commencement_Date__c,
                             Eligibility_Exemption__c,
                             VET_Fee_Help__c,
                             ANZSIC_Code__c,
                             Full_Time_Learning__c,
                             Start_Date__c,
                             End_Date__c,
                             Study_Reason_Identifier__c,
                             Unit__r.name,
                             Qualification_Identifier__c,
                             Enrolment__r.Qualification_Identifier__c,
                             Enrolment__r.Start_Date__c,
                             Enrolment__r.End_Date__c,
                             Unit__r.Unit_Flag_Identifier__c,
                             Unit__r.Unit_Name__c,
                             Student_Identifier__c,
                             Line_Item_Qualification_Unit__r.Nominal_Hours__c,
                             Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c,
                             Enrolment__r.Training_Plan_Developed__c,
                             Enrolment__r.Booking_ID__c,
                             Course_Site_ID__c,
                             Enrolment__r.Qualification__r.Associated_Course_Identifier__c
                          
                             
                      FROM Enrolment_Unit__c WHERE Purchasing_Contract_Identifier__c in : purchasingContractIds AND Enrolment__c in : enrolmentIds AND (CALENDAR_YEAR(End_Date__c) = : Integer.valueof(year.trim()) OR CALENDAR_YEAR(Start_Date__c) = : Integer.valueof(year.trim())) AND Unit__r.RecordType.Name = 'Unit of Competency']) {
                      
                                            
                      if(eu.AVETMISS_National_Outcome__c == '70')
                         NationalOutComeNSW = '';
                      else
                        NationalOutComeNSW = eu.AVETMISS_National_Outcome__c;
            
                     body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Delivery_Location_Identifier__c, NAT00120__c.getInstance('01').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Student_Identifier__c, NAT00120__c.getInstance('02').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Unit_of_Competency_Identifier__c, NAT00120__c.getInstance('03').Length__c,'') +     
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Qualification_Identifier__c, NAT00120__c.getInstance('04').Length__c,'') +       
                     NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Start_Date__c)), NAT00120__c.getInstance('05').Length__c,'') +    
                     NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.End_Date__c)), NAT00120__c.getInstance('06').Length__c,'') +      
                     NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Delivery_Mode_Identifier__c), NAT00120__c.getInstance('07').Length__c,'') +   
                     NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(NationalOutComeNSW), NAT00120__c.getInstance('08').Length__c,'') +     
                     NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Scheduled_Hours__c)), NAT00120__c.getInstance('09').Length__c) +    
                     NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c), NAT00120__c.getInstance('10').Length__c,'') +     
                     NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Commencing_Course_Identifier__c), NAT00120__c.getInstance('11').Length__c,'') +     
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Training_Contract_Identifier__c, NAT00120__c.getInstance('12').Length__c,'') +     
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Client_Identifier_New_Apprenticeships__c, NAT00120__c.getInstance('13').Length__c,'') +      
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Study_Reason_Identifier__c, NAT00120__c.getInstance('14').Length__c,'') +      
                     NATGeneratorUtility.formatOrPadTxtFile(eu.VET_in_Schools__c, NAT00120__c.getInstance('15').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Specific_Program_Identifier__c, NAT00120__c.getInstance('23').Length__c,'') +  
                     NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('16').Length__c,'') +       
                     NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('17').Length__c,'') +      
                     NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf('0000')), NAT00120__c.getInstance('18').Length__c) +     
                     NATGeneratorUtility.formatOrPadTxtFile('N', NAT00120__c.getInstance('19').Length__c,'') +      
                     NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('20').Length__c,'') +      
                     NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('21').Length__c,'')+
                     NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Hours_Attended__c)), NAT00120__c.getInstance('22').Length__c)+
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Qualification__r.Associated_Course_Identifier__c, NAT00120__c.getInstance('27').Length__c,'')+
                     NATGeneratorUtility.formatOrPadTxtFileNSW(eu.Enrolment__r.Booking_ID__c, NAT00120__c.getInstance('24').Length__c)+
                     NATGeneratorUtility.formatOrPadTxtFileNSW(eu.Course_Site_ID__c, NAT00120__c.getInstance('25').Length__c)+                   
                     NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Training_Plan_Developed__c, NAT00120__c.getInstance('26').Length__c,'');
                      
                     body120 += '\r\n';
              }
            
            if(body120 == '')
                body120 = 'No Data, check configuration';
            
            Blob pBlob120 = Blob.valueof(body120);
            insert new ContentVersion(
            versionData =  pBlob120,
            Title = 'NAT00120',
            PathOnClient = '/NAT00120.txt',
            FirstPublishLocationId = libraryId);
     
     
     }
     
     @future public static void generateNAT00130_NSW(Set<String>purchasingContractIds, String year,Set<Id>enrolmentIds) {
         
         String body130 = '';
         String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
         Set<Id>awardQuals = new Set<Id>();
         
         for(Awards__c award : [Select Enrolment__c, Year_Program_Completed__c,Qualification_Issued__c From Awards__c WHERE RecordType.DeveloperName = 'approved_award' AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' And Enrolment__c in : enrolmentIds ]){
                   awardQuals.add(award.Enrolment__c);
         }
         
          if(!awardQuals.IsEmpty()){
                for(Enrolment__c enrl: [SELECT id, NAT_Error_Code__c, Training_Organisation_Identifier__c,
                                            Parent_Qualification_Code__c, Student_Identifier__c, 
                                            (Select Year_Program_Completed__c,Qualification_Issued__c 
                                            From Awards__r WHERE RecordType.Name = 'approved award' AND
                                            Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' LIMIT 1)
                                            FROM Enrolment__c 
                                            WHERE id in : awardQuals]){
                            
                    body130 += NATGeneratorUtility.formatOrPadTxtFile(enrl.Training_Organisation_Identifier__c, NAT00130__c.getInstance('01').Length__c,'')+
                    NATGeneratorUtility.formatOrPadTxtFile(enrl.Parent_Qualification_Code__c, NAT00130__c.getInstance('02').Length__c,'')+
                    NATGeneratorUtility.formatOrPadTxtFile(enrl.Student_Identifier__c, NAT00130__c.getInstance('03').Length__c,'');
                    
                    for(Awards__c award : enrl.awards__r ) {
                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Year_Program_Completed__c, NAT00130__c.getInstance('04').Length__c,'');
                         body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Qualification_Issued__c, NAT00130__c.getInstance('05').Length__c,'');
                        //Add Qualification Issued Flag
                    }
                    
                    body130 += '\r\n';
            
                }
            }
         
          if(body130 == '')
            body130 = ' ';
          Blob pBlob130 = Blob.valueof(body130);
          
            insert new ContentVersion(
            versionData =  pBlob130,
            Title = 'NAT00130',
            PathOnClient = '/NAT00130.txt',
            FirstPublishLocationId = libraryId);
     
     }
   
    
}