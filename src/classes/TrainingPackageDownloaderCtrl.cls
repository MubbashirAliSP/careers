public class TrainingPackageDownloaderCtrl {
    public Integer pageSize = 20;
    public Integer pageNumber = 0;
    private List<TC> tcCache;

    public Integer getPageNumber() { return pageNumber; }

    public class TC {
        private boolean ivalue = false;
        public boolean isUpgrade { get; set; }
        /*public boolean i{
            get{
                return ivalue && (isUpgrade == null || !isUpgrade);
            }
            set{
                ivalue = value;
            }
        }*/
        public Training_Component__c p{get;set;}
        
        public string className {
            get{
                if( p.Training_Package__c != null){
                    if ( p.imported__c == p.updated__c )
                        return 'imported';
                    else if ( p.updated__c >= system.today() - 30 )
                        return 'changed';
                    return 'linked';
                }
                
                return '';
            }
        }
    }
    
    public PageReference nextPage(){
        pageNumber++;
        init();
        return null;
    }

    public PageReference previousPage(){
        pageNumber--;
        init();
        return null;
    }

    public String tgaUpdated{
        get{
            Training_Component__c[] newest = [ select LastModifiedDate From Training_Component__c Order By LastModifiedDate LIMIT 1 ];
            if ( newest.size() > 0 ){
                return newest[0].LastModifiedDate.formatLong();
            }
            return null;
        }
    }
    
    public void init(){
        Integer offset = pageNumber * pageSize;
        Training_Component__c[] r = [ 
            select 
                Code__c,
                Title__c,
                Updated__c,
                Training_Package__c,
                Parent_Code__c,
                Component_Type__c,
                Imported__c,
                ImportTest__c
            from Training_Component__c
            Where Component_Type__c = 'TrainingPackage'
            Order By Code__c
            LIMIT :pageSize
            OFFSET :offset
        ];
        system.debug('r****'+r);
        List<TC> ret = new List<TC>();
        List<Training_Component__c> toAutoImport = new List<Training_Component__c>();
        integer autoImport = 0;
        for (Training_Component__c c : r ){
            TC p = new TC();
            p.p = c;
            
            //'auto-import' components which have no actual changes...
            if ( c.Imported__c != c.Updated__c && c.ImportTest__c != c.Updated__c && autoImport++ < 15 ){
                UnitsChangesReportCtrl uc = new UnitsChangesReportCtrl();
                uc.parentCode = c.Code__c;

                if ( uc.units.size() == 0 ){
                    c.Imported__c = c.Updated__c;
                }
                c.ImportTest__c = c.Updated__c;
                toAutoImport.add(c);
            }
            
            
            ret.add(p);
        }
        if ( toAutoImport.size() > 0 ){
            update toAutoImport;
            //system.assert(false, [ Select Imported__c, ImportTest__c from  Training_Component__c where id in :toAutoImport ]);
        }
        tcCache = ret;
        system.debug('tcCache'+tcCache);
    }
    public List<TC> trainingPackages {
        get{
            system.debug('2tcCache'+tcCache);
            return tcCache;
        }    
    }

    
    /*
    private string getBaseUnitCode(String code){
        if ( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.countMatches(code.substring(code.length()-1)) > 0 )
            return code.substring(0, code.length()-1);
        return null;
    }
    
    public PageReference importTPs(){
        //all TC to update with new data
        Map<String, Training_Component__c> toUpdate = new Map<String, Training_Component__c>();
        
        //create TP if it doesn't exist
        List<Training_Package__c> toUpdateTP = new List<Training_Package__c>();
        for ( TC tp : trainingPackages ){
            if ( tp.i ){
                if ( tp.p.Training_Package__c == null ){
                    Training_Package__c newtp = new Training_Package__c();
                    newtp.Training_Package_Name__c = tp.p.title__c;
                    newtp.name = tp.p.code__c;
                    toUpdateTP.add(newtp);
                }
             }
         }
         insert toUpdateTP;
         //link new TPs to TC
         for ( Training_Package__c newtp : toUpdateTP ){
             for ( TC tp : trainingPackages ){
               if ( tp.i && tp.p.code__c == newtp.name && tp.p.Training_Package__c != newtp.id ){
                  tp.p.Training_Package__c = newtp.id;
                  toUpdate.put(tp.p.Code__c, tp.p);
               }
             }
         }
         
         //fetch components, and link units
         Map<String, Map<String, Training_Component__c>> componentsByTP = new Map<String, Map<String, Training_Component__c>>();
         List<Unit__c> toUpdateUnit = new List<Unit__c>();
         for ( TC tp : trainingPackages ){
            if ( tp.i ){
                 Map<String, Unit__c> units = new Map<String, Unit__c>();
        
                 //fetch TC unit components
                 Map<String, Training_Component__c> components = new Map<String, Training_Component__c>();
                 componentsByTP.put(tp.p.code__c, components);
                 
                 for ( Training_Component__c component : [ 
                    select 
                        Id,
                        Code__c,
                        Fields_of_Education__c,
                        Title__c,
                        Unit__c,
                        Training_Package__c,
                        Training_Package_Unit__c,
                        Component_Type__c
                    From Training_Component__c
                    Where
                        Parent_Code__c = :tp.p.Code__c
                        and
                        Component_Type__c = 'Unit'
                     ]){
                     components.put(component.Code__c, component);
                 }
                 
                 //fetch units
                 for ( Unit__c unit : [ 
                    select 
                        Id,
                        Name
                    From Unit__c
                    Where
                        Name in :components.keySet()
                     ]){
                     units.put(unit.Name, unit);
                 }
                 
                 //create and update units
                 for ( Training_Component__c component : components.values() ){
                     //create or update the unit
                     Unit__c unit = units.get(component.Code__c);
                     if ( unit == null ){
                         unit = new Unit__c();
                         toUpdate.put(component.Code__c, component);
                     }else if ( component.Unit__c != unit.id ){
                         component.unit__c = unit.id;
                         toUpdate.put(component.Code__c, component);
                     }
                     if ( convert(component, unit, false) )
                         toUpdateUnit.add(unit);
                 }
            }
        }
        //insert units and link them up to the training_components
        upsert toUpdateUnit;
        for ( Unit__c newunit : toUpdateUnit ){
            Training_Component__c tc = toUpdate.get(newunit.name);
            system.assert(tc!=null, newunit.name + ' does not exist');
            tc.Unit__c = newunit.id;
        }
        
        
        //now handle the training package units
        List<Training_Package_Unit__c> toUpdateTPU = new List<Training_Package_Unit__c>();
        for ( TC tp : trainingPackages ){
            if ( tp.i ){
                 Map<String, Training_Package_Unit__c> tpunits = new Map<String, Training_Package_Unit__c>();
                 Map<String, Training_Component__c> components = componentsByTP.get(tp.p.code__c);
                 
                 //fetch training package units
                 for ( Training_Package_Unit__c tpunit : [ 
                    select 
                        Id,
                        National_Code__c
                    From Training_Package_Unit__c
                    Where
                         Training_Package__c = :tp.p.Training_Package__c
                     ]){
                     tpunits.put(tpunit.National_Code__c, tpunit);
                 }
                 
                 
                 
                 //create and update units and tp unit
                 for ( Training_Component__c component : components.values() ){
                     Training_Package_Unit__c tpunit = tpunits.get(component.code__c);
                     if ( tpunit == null ){
                         tpunit = new Training_Package_Unit__c();
                         tpunit.Training_Package__c = tp.p.Training_Package__c;
                         tpunit.Unit_of_Competency__c = component.Unit__c;
                         toUpdateTPU.add(tpunit);
                         toUpdate.put(component.Code__c, component);
                     }else if ( component.Training_Package_Unit__c != tpunit.id ){
                         toUpdate.put(component.Code__c, component);
                         component.Training_Package_Unit__c = tpunit.id;
                     }
                 }
             }
        }
        //update TPUs with new units
        upsert toUpdateTPU;
        for ( Training_Package_Unit__c newtpu : toUpdateTPU ){
            for ( Training_Component__c tc: toUpdate.values() ){
                if ( newtpu.Unit_of_Competency__c == tc.Unit__c ){
                    tc.Training_Package_Unit__c = newtpu.id;
                }
            }
        }
        
        
        
        //check that we have all required data
        for ( Training_Component__c c : toUpdate.values() ){
            if ( c.Component_Type__c == 'Unit' ){
                system.assert(c.Unit__c != null);
                system.assert(c.Training_Package_Unit__c != null, c);
            }else if ( c.Component_Type__c == 'TrainingPackage' ){
                system.assert(c.Training_Package__c != null);
            }else{
                system.assert(false, 'Invalid Component Type: ' + c.Component_Type__c);
            }
        }
        upsert toUpdate.values();
        
        //mark TPU components as imported
        for ( Training_Component__c c : toUpdate.values() ){
            if ( c.Component_Type__c == 'TrainingPackage' ){
                markComponentImported(c.code__c);
            }
        }
        
        tcCache = null;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, toUpdateTP.size() + ' new training packages were added with ' + toUpdateUnit.size() + ' units and ' + toUpdateTPU.size() + ' training package units.'));
        return null;
    }
    */
    
}