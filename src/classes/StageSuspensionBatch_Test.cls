/**
 * @description Test class for `StageSuspensionBatch` class
 * @author Ranyel Maliwanag
 * @date 15.DEC.2015
 */
@isTest
public with sharing class StageSuspensionBatch_Test {
	@testSetup
	static void batchSetup() {
		List<Account> accounts = SASTestUtilities.bulkCreateStudentAccount(201);
		insert accounts;

		Qualification__c qual = SASTestUtilities.createQualification(null, null);
		insert qual;

		List<Enrolment__c> enrolments = new List<Enrolment__c>();
		for (Account student : accounts) {
			Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
			enrolment.Qualification__c = qual.Id;
			enrolments.add(enrolment);
		}
		insert enrolments;

		Stage__c stage = new Stage__c(Name = 'Stage 1');
		insert stage;

		Intake__c intake = SASTestUtilities.createIntake(qual);
		intake.Stage__c = stage.id;
		intake.Start_Date__c = Date.today().addDays(-7);
		intake.End_Date__c = Date.today().addDays(-1);
		insert intake;

		List<Census__c> censuses = new List<Census__c>();
		for (Enrolment__c enrolment : enrolments) {
			Census__c census = new Census__c(
                EnrolmentId__c = enrolment.Id,
                IntakeId__c = intake.Id,
                CensusDate__c = Date.today().addDays(-2)
            );
			censuses.add(census);
		}
		insert censuses;
	}

	static testMethod void batchTest() {
		Test.startTest();
			StageSuspensionBatch nextBatch = new StageSuspensionBatch();
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(200, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from S%'];
		System.assertEquals(200, enrolmentCount);
	}

	static testMethod void batchExceptionTest() {
		Stage__c stage3 = new Stage__c(Name = 'Stage 3');
		insert stage3;

		Intake__c intake = [SELECT Id FROM Intake__c];
		intake.Stage__c = stage3.Id;
		update intake;

		Test.startTest();
			StageSuspensionBatch nextBatch = new StageSuspensionBatch();
			Database.executeBatch(nextBatch);
		Test.stopTest();

		Integer enrolmentCount = [SELECT count() FROM Enrolment__c];
		System.assertEquals(200, enrolmentCount);

		enrolmentCount = [SELECT count() FROM Enrolment__c WHERE Enrolment_Status__c LIKE 'Suspended from S%'];
		System.assertEquals(0, enrolmentCount);
	}
}