/**
 * @description Custom component controller for `SEC_ListViewDataCell` component
 * @author Ranyel Maliwanag
 * @date 12.JAN.2016
 */
public without sharing class SEC_ListViewDataCellCC {
	// The base SObject record where data will be fetched from for this data table cell
	public SObject record {get; set;}

	// The field path that will be used to fetch the data from the given record
	public String fieldPath {get; set;}

	// Determines how the data will be presented on the data table cell component
	public String fieldType {get; set;}

	public Date dateValue {
		get {
			if (fieldType == 'date' && String.isNotBlank(fieldValue)) {
				dateValue = Date.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, fieldPath));
			}
			return dateValue;
		}
		set;
	}

	public Datetime datetimeValue {
		get {
			if (fieldType == 'datetime' && String.isNotBlank(fieldValue)) {
				datetimeValue = Datetime.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, fieldPath));
			}
			return datetimeValue;
		}
		set;
	}

	public String datetimeString {
		get {
			if (datetimeValue != null) {
				datetimeString = datetimeValue.format();
			}
			return datetimeString;
		}
		set;
	}

	public String referenceName {
		get {
			if (fieldType == 'reference' && String.isNotBlank(fieldValue) && fieldPath.endsWith('__c')) {
				String arbitraryPath = fieldPath.replace('__c', '__r') + '.Name';
				referenceName = String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, arbitraryPath));
			}
			return referenceName;
		}
		set;
	}

	public String fieldValue {
		get {
			return EnrolmentsSiteUtility.getReferenceFieldValue(record, fieldPath) != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, fieldPath)) : '';
		}
	}

	public String resultRequestReference {
		get {
			return EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Enrolment_Record__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Enrolment_Record__c')) : '';
		}
	}

	public String studentIdReference {
		get {
			return EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__c')) : '';
		}
	}

	public Boolean booleanValue {
		get {
			return Boolean.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, fieldPath));
		}
	}

	public Id viewAs {get; set;}

	public Id trainerId {
		get {
			if (viewAs != UserInfo.getUserId()) {
				trainerId = viewAs;
			}
			return trainerId;
		}
		set;
	}

	public Id recordId {
		get {
			return record.Id;
		}
	}
}