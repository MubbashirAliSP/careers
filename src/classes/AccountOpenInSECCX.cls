/**
 * @description Custom butotn handler for `AccountOpenInSEC` page that opens up the active enrolment of the student in the Student Engagement Console
 * @author Ranyel Maliwanag
 * @date 26.FEB.2016
 */
public with sharing class AccountOpenInSECCX {
	public Account student {get; set;}

	public AccountOpenInSECCX(ApexPages.StandardController controller) {
		Id recordId = controller.getId();
		student = [SELECT Id, (SELECT Id FROM Enrolment1__r WHERE IsNotCurrentlyStudying__c = false) FROM Account WHERE Id = :recordId];
	}

	public PageReference evaluateRedirects() {
		PageReference result = null;
		Enrolment__c activeEnrolment;
		for (Enrolment__c enrolment : student.Enrolment1__r) {
			activeEnrolment = enrolment;
		}

		if (activeEnrolment != null) {
			result = Page.StudentEngagementConsole;
			result.getParameters().put('eid', activeEnrolment.Id);
			result.setRedirect(true);
		}
		return result;
	}
}