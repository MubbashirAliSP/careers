/*  Author: Paolo Mendoza
    Date: 4/23/2015
    Description: Mass Assign Transaction Line Item to a Transaction Payment
                 test class for MassUpdateTLIController
*/


@isTest
private class MassUpdateTLIControllerTest{

    private static TestMethod void MassUpdateTLIControllerTest(){
        Account acc = new Account();
           
       
        
        Language__c l = new Language__c();
        l.Name = 'Test Language';
        l.Code__c = '111';
        insert l;
        
        acc = TestCoverageUtilityClass.createStudentEUOS(l.id);
        
        Account_Transaction__c accTrans = new Account_Transaction__c();
        accTrans.payer__c = acc.id;
        insert accTrans;
        
        Transaction_Payment__c trans = new Transaction_Payment__c();
        trans.Account_Transaction__c = accTrans.id;
        trans.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Credit Note'].Id;
        insert trans;
        
        Transaction_Line_item__c transLineItem = new Transaction_Line_item__c();
        transLineItem.Account_Transaction__c = accTrans.id;
        insert transLineItem;
        
        Test.startTest();

        Transaction_Line_item__c transLineItem2 = new Transaction_Line_item__c();
        transLineItem2.Account_Transaction__c = accTrans.id;
        transLineItem2.Transaction_Payment__c = trans.Id;
        insert transLineItem2;
        
        PageReference pageRef = Page.MassUpdateTLI;
        pageRef.getParameters().put('id', String.valueOf(trans.Id));
        Test.setCurrentPage(pageRef);        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(trans);
        MassUpdateTLIController MU = new MassUpdateTLIController(sc);
        system.debug(MU.tliWrapList);
        for(MassUpdateTLIController.TLIWrapperCls wtli : MU.tliWrapList) {
            wtli.isSelected = true;
        }
        system.debug(MU.tliWrapList);

        MU.displaySelectedTLI();

        Test.stopTest();
        
        }
        


}