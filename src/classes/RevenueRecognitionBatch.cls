/**
 * @description     Batch job that creates revenue reconciliation summaries for qualified enrolments
 * @author          Michael Wheeler
 * @Company         CloudSherpas
 * @date            23.MAR.2015
 * @history
 *       21.JUL.2015    Ranyel Maliwanag    Added new fields for ERS generation
 *       07.SEP.2015    Ranyel Maliwanag    Added override for August 2015 missed records. Force archive date to 31 August 2015
 *       08.SEP.2015    Ranyel Maliwanag    Reverted back batch criteria
 */ 

global class RevenueRecognitionBatch implements Database.Batchable<sObject> {
    // Variable initialisations
    public String query;
    public String stageFields = '';
    public Id currentERSRecordTypeId;
    public Id historicalERSRecordTypeId;
    public Boolean isSuccess = true;
    public Date baselineDate = Date.newInstance(2015, 7, 1);
    
    /**
     * @description Default class contructor
     * @author N/A
     * @date N/A
     */
    global RevenueRecognitionBatch() {
        // Fetch record type Id information
        currentERSRecordTypeId = Schema.SObjectType.Enrolment_Revenue_Summary__c.getRecordTypeInfosByName().get('Current Summary').getRecordTypeId();
        historicalERSRecordTypeId = Schema.SObjectType.Enrolment_Revenue_Summary__c.getRecordTypeInfosByName().get('Historical Summary').getRecordTypeId();
        
        // Fetch census date fields off the census dates fieldset
        List<Schema.FieldSetMember> fields = SObjectType.Enrolment_Revenue_Summary__c.FieldSets.Census_Dates.getFields(); 
        
        // Build extra fields for query constructor
        for(Schema.FieldSetMember f : fields) {
           stageFields += String.valueOf(f.getFieldPath()) + ','; 
        }
        
        // Build query for batch
        query = 'SELECT Id,' + stageFields + ' Enrolment__c, Tech_One_Contract_Code__c, Historical_Tech_One_Contract_Code__c, Parent_Qualification_Code__c, Historical_Parent_Qualification_Code__c, Enrolment_Start_Date__c, Historical_Enrolment_Start_Date__c, Unit_of_Competency_Delivery_Mode__c, Historical_Unit_of_Study_Delivery_Mode__c, Delivery_Location__c, Historical_Delivery_Location__c, Campus__c, Historical_Campus__c, Type__c, Historical_Type__c, Contract_Code__c, Historical_Contract_Code__c, Historical_Campus_Code__c, Campus_Code__c, Qualification__c, Historical_Qualification__c, Contract_Start_Date__c, Historical_Contract_Start_Date__c, Contract_End_Date__c, Historical_Contract_End_Date__c, Last_Historical_Record_Creation_Date__c, Reconciliation_Revenue__c,Total_Course_Revenue__c, Enrolment__r.Enrolment_Status__c,Revenue_Received__c, Enrolment__r.End_Date__c, RecordTypeId, Enrolment_Status__c,Enrolment_End_Date__c, Cumulative_Revenue_Recognised__c, Historical_Revenue_Recognised__c, Monthly_Revenue_Recognised__c, Historical_Monthly_Revenue_Recognised__c, Enrolment__r.Auto_Include_In_Revenue_Summary_Job__c,Enrolment__r.Revenue_Received_Not_Recognised_Is_Not_0__c,Enrolment__r.Enrolment_Included_In_Rev_Summary_Job__c, Enrolment__r.Override_Auto_Rev_Summary_Generation__c FROM Enrolment_Revenue_Summary__c WHERE RecordTypeId = :currentERSRecordTypeId AND Enrolment__c IN (SELECT Id FROM Enrolment__c WHERE Enrolment_Included_In_Rev_Summary_Job__c = true AND Start_Date__c >= :baselineDate AND Start_Date__c <= THIS_MONTH)';
    }
    

    global Database.QueryLocator start(Database.BatchableContext BC) {  
        return Database.getQueryLocator(query);  
    }


    global void execute(Database.BatchableContext BC, List<Enrolment_Revenue_Summary__c> listRevenue) {
        // Variable initialisations
        List<Enrolment_Revenue_Summary__c> activeERS = new List<Enrolment_Revenue_Summary__c>();
        List<Enrolment_Revenue_Summary__c> inactiveERS = new List<Enrolment_Revenue_Summary__c>();
        List<Enrolment_Revenue_Summary__c> toUpsertERSList = new List<Enrolment_Revenue_Summary__c>();
        
        // Start iterating over records in batch
        for (Enrolment_Revenue_Summary__c ers: listRevenue) {
            if ((ers.Enrolment__r.Enrolment_Status__c == 'Cancelled' || ers.Enrolment__r.Enrolment_Status__c == 'Completed') && ers.Reconciliation_Revenue__c <> 0) {
                // Condition: Related enrolment reord has status of cancelled or completed and current revenue summary has reconciliation revenue value
                ers.Historical_Monthly_Revenue_Recognised__c = ers.Reconciliation_Revenue__c;
            } else {
                ers.Historical_Monthly_Revenue_Recognised__c = ers.Monthly_Revenue_Recognised__c;
            }
            ers.Historical_Revenue_Recognised__c = ers.Cumulative_Revenue_Recognised__c + ers.Monthly_Revenue_Recognised__c;
            ers.Last_Historical_Record_Creation_Date__c = date.today();

            Enrolment_Revenue_Summary__c historicalERS = ers.clone(false, true);
            historicalERS.RecordTypeId = historicalERSRecordTypeId;
            historicalERS.Monthly_Historical_Summary__c = TRUE;
            historicalERS.HistoricalTotalUpfrontPayment__c = ers.TotalUpfrontPayment__c;
            historicalERS.Historical_Total_Course_Revenue__c = ers.Total_Course_Revenue__c;
            historicalERS.Historical_Revenue_Received__c = ers.Revenue_Received__c;
			historicalERS.Historical_Enrolment_Status__c = ers.Enrolment_Status__c;
         	historicalERS.Historical_Enrolment_End_Date__c = ers.Enrolment_End_Date__c;
            historicalERS.Historical_Tech_One_Contract_Code__c = ers.Tech_One_Contract_Code__c;
            historicalERS.Historical_Parent_Qualification_Code__c = ers.Parent_Qualification_Code__c;
            historicalERS.Historical_Enrolment_Start_Date__c = ers.Enrolment_Start_Date__c;
            historicalERS.Historical_Unit_of_Study_Delivery_Mode__c = ers.Unit_of_Competency_Delivery_Mode__c;
            historicalERS.Historical_Delivery_Location__c = ers.Delivery_Location__c;
            historicalERS.Historical_Campus__c = ers.Campus__c;
            historicalERS.Historical_Type__c = ers.Type__c;
            historicalERS.Historical_Contract_Code__c = ers.Contract_Code__c;
            historicalERS.Historical_Qualification__c = ers.Qualification__c;
            historicalERS.Historical_Contract_Start_Date__c = ers.Contract_Start_Date__c;
            historicalERS.Historical_Contract_End_Date__c = ers.Contract_End_Date__c;
            historicalERS.Historical_Campus_Code__c = ers.Campus_Code__c;
            historicalERS.Monthly_Historical_Summary__c = TRUE;
            historicalERS.Auto_Include_In_Revenue_Summary_Job__c = ers.Enrolment__r.Auto_Include_In_Revenue_Summary_Job__c;
			historicalERS.Override_Auto_Rev_Summary_Job_Flag__c = ers.Enrolment__r.Override_Auto_Rev_Summary_Generation__c;
            historicalERS.Revenue_Received_Not_Recognised_Is_Not_0__c = ers.Enrolment__r.Revenue_Received_Not_Recognised_Is_Not_0__c;
			historicalERS.Enrolment_Included_In_Rev_Summary_Job__c = ers.Enrolment__r.Enrolment_Included_In_Rev_Summary_Job__c;

            historicalERS.Current_Revenue_Summary__c = ers.Id;
            historicalERS.Archive_Date__c = Date.today();

            toUpsertERSList.add(ers);
            toUpsertERSList.add(historicalERS);
        }

        System.debug('toUpsertERSList ::: ' + toUpsertERSList.size() + ' ::: ' + toUpsertERSList);

        if (toUpsertERSList.size() != 0) {
            //try {
                upsert toUpsertERSList;
                
                if (!isSuccess) {
                    isSuccess = false;
                } else {
                    isSuccess = true;
                }
            //} catch (exception e) {
            //    System.debug('Batch exception ::: ' + e.getMessage());
            //    if(isSuccess) {
            //        isSuccess = false;
            //    }
            //}
        }
    }
        
    global void finish(Database.BatchableContext BC){
        List<RevenueRecognitionSetting__c> rrs = RevenueRecognitionSetting__c.getall().values();

        if (isSuccess) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {rrs[0].Notification_Email__c};  
            mail.setToAddresses(toAddresses);  
            mail.setSubject('Revenue Recognition Batch Job Complete');  
            mail.setPlainTextbody('The revenue recognition batch job has completed successfully');  
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        if (!isSuccess) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] { rrs[0].Notification_Email__c};  
            mail.setToAddresses(toAddresses);  
            mail.setSubject('Revenue Recognition Batch Job Failed');  
            mail.setPlainTextbody('The revenue recognition batch job has failed');  
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        }
    }
}