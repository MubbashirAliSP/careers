/**
 * @description Test class for `ApplicationsConfirmCC` class
 * @author Ranyel Maliwanag
 * @date 17.SEP.2015
 */
@isTest
public with sharing class ApplicationsConfirmCC_Test {
    @testSetup
    static void controllerSetup() {
        ApplicationsTestUtilities.initialiseEnrolmentApplication();
    }

    static testMethod void testConstructorUnauthenticated(){
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        queryString += 'WHERE RecordType.Name = \'Student\'';
        GuestLogin__c studentLogin = Database.query(queryString);

        Test.setCurrentPage(Page.ApplicationsConfirm);
        ApexPages.currentPage().getParameters().put('confirm', studentLogin.Id);

        Test.startTest();
            ApplicationsConfirmCC controller = new ApplicationsConfirmCC();
            controller.evaluateParameters();
        Test.stopTest();
    }


    static testMethod void testConstructorAuthenticated(){
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        queryString += 'WHERE RecordType.Name = \'Student\'';
        GuestLogin__c studentLogin = Database.query(queryString);

        Test.setCurrentPage(Page.ApplicationsConfirm);
        ApexPages.currentPage().getParameters().put('confirm', studentLogin.Id);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', studentLogin.Id, null, -1, true)});

        Test.startTest();
            ApplicationsConfirmCC controller = new ApplicationsConfirmCC();
            controller.evaluateParameters();
        Test.stopTest();
    }


    static testMethod void testConstructorAuthenticatedNegative(){
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        queryString += 'WHERE RecordType.Name = \'Student\'';
        GuestLogin__c studentLogin = Database.query(queryString);
        String loginId = '005' + String.valueOf(studentLogin.Id).substring(3);

        Test.setCurrentPage(Page.ApplicationsConfirm);
        ApexPages.currentPage().getParameters().put('confirm', loginId);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', studentLogin.Id, null, -1, true)});

        Test.startTest();
            ApplicationsConfirmCC controller = new ApplicationsConfirmCC();
            controller.evaluateParameters();
        Test.stopTest();
    }


    static testMethod void testConstructorAuthenticatedNegative2(){
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
        queryString += 'WHERE RecordType.Name = \'Student\'';
        GuestLogin__c studentLogin = Database.query(queryString);
        String loginId = '005' + String.valueOf(studentLogin.Id).substring(3);

        Test.setCurrentPage(Page.ApplicationsConfirm);
        ApexPages.currentPage().getParameters().put('confirm', loginId);
        ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', loginId, null, -1, true)});

        Test.startTest();
            ApplicationsConfirmCC controller = new ApplicationsConfirmCC();
            controller.evaluateParameters();
        Test.stopTest();
    }
}