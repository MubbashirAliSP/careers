@istest
public class TestClassConfigurationExtension {

    @istest static void testOneOffClassConfiguration() {
    
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
                    
        
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit2 = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment3 = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Intake_Students__c iStud = new Intake_Students__c();
        Intake_Students__c iStud2 = new Intake_Students__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Class_Attendance_Type__c cattend = new Class_Attendance_Type__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        Language__c language = new Language__c();
        system.runAs(u){
            test.startTest();
            language = TestCoverageUtilityClass.createLanguage();
            acc = TestCoverageUtilityClass.createStudent();
            acc2 = TestCoverageUtilityClass.createStudentEUOS(language.Id);
            system.debug('****acc.Student_Identifer__c'+acc.Student_Identifer__c);
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            attend = TestCoverageUtilityClass.createAttendanceType();
            
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            
            insert enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            
            enrl2 = TestCoverageUtilityClass.createEnrolment(acc2.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl2.Predominant_Delivery_Mode__c = del.Id;
            enrl2.Type_of_Attendance__c = attend.Id;
            insert enrl2;
            
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            
            PageReference pageRef = Page.ClassConfiguration;
            Test.setCurrentPageReference(pageRef); 
            
            pageRef.getParameters().put('intakeId', intake.Id);
            
            Class_Configuration__c classConfig9 = new Class_Configuration__c();
            
            ApexPages.StandardController sc = new ApexPages.StandardController(classConfig9);
            
            ClassConfigurationExtension ole = new ClassConfigurationExtension(sc);
            
            ole.savecustom();
            ole.cancelcustom();
            
            }
        
        
    }

}