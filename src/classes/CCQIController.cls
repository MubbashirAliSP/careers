public with sharing class CCQIController {
    
    public static final List<String> ResultCodes =  new List<String>  {'20', '51', '81'};
    
    public String year {get;set;}
    
    public String rto {get;set;}
    
    public List<SelectOption> years {get;set;}
    
    public List<SelectOption> rtos {get;set;}
    
    public List<SelectOption> states {get;set;}
    
    public String state {get;set;}
    
    public List<Account> TrainingOrgList {get;set;}
    public List<Qualification__c> QualList {get;set;}
    public List<Enrolment_Unit__c>EUList {get;set;}
    
    public List<CCQIWrapperQualifications> Qualification_Enrolments {get;set;} 
    public List<CCQIWrapperUnits> Enrolment_Units {get;set;}
     
    
    
    
    
    public CCQIController() {
        
        List<Account>RTOsList = New List<Account>([SELECT Id,Training_Org_Identifier__c,Offshore_Indicator_Flag__c, Name FROM Account WHERE RecordType.Name = 'Training Organisation']);
        
        
        rtos = new List<SelectOption>();
        
        states = new List<SelectOption>();
        
        //Add RTOs to Select list and display on the page
        for(Account a : RTOsList)
            rtos.add(new SelectOption(a.id,a.name));
            
        years = new List<SelectOption>();
        Integer yearInt = 2000;
        
            for(Integer i = 20; i >=0; i--) {
                
                years.add(new SelectOption(String.ValueOf(yearInt), String.ValueOf(yearInt)));
                yearInt +=1;
            }
          
        if(year == null || year == '')  
            year = String.ValueOf(date.today().year());
            
        
        states.add(new SelectOption('NSW','NSW'));
        states.add(new SelectOption('QLD','QLD'));
        states.add(new SelectOption('WA','WA'));
        states.add(new SelectOption('SA','SA'));
        states.add(new SelectOption('VIC','VIC'));
        states.add(new SelectOption('NSW','NSW'));
        states.add(new SelectOption('NT','NT'));
        states.add(new SelectOption('ACT','ACT'));
    }
    
    public void generateData() {

        Qualification_Enrolments = new List<CCQIWrapperQualifications>();
        Enrolment_Units = new List<CCQIWrapperUnits>();
        
        TrainingOrgList = new List<Account>([Select Id,Training_Org_Identifier__c,Offshore_Indicator_Flag__c, Name From Account Where Id = : rto]);
        
        /*original code
        QualList = new List<Qualification__c>();
        
        Set<Id> qualId = new Set<Id>();
        Map<Id, List<Enrolment__c>> qualEnrolmentMap = new Map<Id, List<Enrolment__c>>();
        
        for(Qualification__c q: [Select Id, Name, Qualification_Name__c FROM Qualification__c WHERE Include_in_CCQI_export__c = true limit 2000]) {
            QualList.add(q);
            qualId.add(q.Id);
        }              
        */
        
        //new 08.AUG.2014 WBM
        QualList = new List<Qualification__c>([Select Id, Name, Qualification_Name__c FROM Qualification__c WHERE Include_in_CCQI_export__c = true limit 2000]);
       
        Set<Id> qualId = new Set<Id>();
        Map<Id, List<Enrolment__c>> qualEnrolmentMap = new Map<Id, List<Enrolment__c>>();
        
        for(Qualification__c q: qualList){
            qualId.add(q.Id);
        }
        //end
        
        //QualList = new List<Qualification__c>();
        for (Enrolment__c e: [Select Qualification_Award_based_on__c, Id,Start_Date__c,End_Date__c From Enrolment__c WHERE Delivery_Location_State__c = : GlobalUtility.stateNameConvert(state) AND Qualification_Award_based_on__c IN: qualId limit 2000]) {
            if(qualEnrolmentMap.containsKey(e.Qualification_Award_based_on__c)) {
                qualEnrolmentMap.get(e.Qualification_Award_based_on__c).add(e);
            } else {
                qualEnrolmentMap.put(e.Qualification_Award_based_on__c, new List<Enrolment__c>{e});
            }
        }
        
        EUList = new List<Enrolment_Unit__c>();
        
        for(Enrolment_Unit__c eu: [Select Id,Unit__r.Unit_Name__c, Unit__r.Name, Name,Enrolment__r.Start_Date__c,Enrolment__r.End_Date__c From Enrolment_Unit__c WHERE Unit__r.Include_in_CCQI_export__c = true AND RecordType.Name = 'Unit of Competency' AND AVETMISS_National_Outcome__c in : ResultCodes AND Enrolment__r.Delivery_Location_State__c = : GlobalUtility.stateNameConvert(state) limit 2000]) {
            EUList.add(eu);
        }
        
        for(Qualification__c q : QualList) {
            integer e_count = 0;
            integer c_count = 0;
            
            if(qualEnrolmentMap.containsKey(q.Id)){
              for(Enrolment__c e : qualEnrolmentMap.get(q.Id)) {
                 if(String.ValueOf(e.Start_Date__c.year()) >= year && String.ValueOf(e.End_Date__c.year()) != year )
                    e_count ++;
                 
                 if(String.ValueOf(e.End_Date__c.year()) == year) {
                    e_count ++;
                    c_count ++;
                 }
              }
            }
            
             if(e_count != 0 || c_count != 0)
                Qualification_Enrolments.add(new CCQIWrapperQualifications(q,e_count,c_count));
        }

        for(Enrolment_Unit__c eu : EUList) {
            
            integer e_count = 0;
            integer c_count = 0;
            
            if(String.ValueOf(eu.Enrolment__r.Start_Date__c.year()) == year && String.ValueOf(eu.Enrolment__r.End_Date__c.year()) != year )
                 e_count ++;
            
            if(String.ValueOf(eu.Enrolment__r.End_Date__c.year()) == year) {
                 e_count ++;
                 c_count ++;
            }

            Enrolment_Units.add(new CCQIWrapperUnits(eu,e_count,c_count));
            
        }

    }
    
    public pageReference exportData() {
            
        Id libraryId;
        
        String queryString = '';
        
        try {libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'CCQI' LIMIT 1].Id;
            
            List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
        
                if(existingContent.size() > 0)
                    delete existingContent;
                
        
        }
        catch(Exception e){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CCQI Library not found or you do not have permission!'));}
        
        
        CCQIGenerator.exportCCQIFiles(libraryId, state, Year, TrainingOrgList.get(0), Qualification_Enrolments,Enrolment_Units);  
        
        return new PageReference('/sfc/#search?');
    }
    
    
    public pageReference next() {generateData(); return new pageReference('/apex/CCQIGeneration2');}
    
    public pageReference cancel() {return new pageReference('/home/home.jsp');}
    
   
    
 
        
        
    
}