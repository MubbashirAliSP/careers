/**
 * @description Test class for `LeadQueueAssignmentSchedulable` class
 * @author Ranyel Maliwanag
 * @date 18.SEP.2015
 */
@isTest
public with sharing class LeadQueueAssignmentSchedulable_Test {
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Lead Queue Assignment Test', schedule, new LeadQueueAssignmentSchedulable());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}