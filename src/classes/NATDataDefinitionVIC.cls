/**
 * @description N/A
 * @author N/A
 * @date N/A
 * @history
 *       01.FEB.2016    Ranyel Maliwanag    - Added position 33 for NAT00120. Reference Case: 00038985
 *                                          - Added positions 7 and 8 for NAT00130. Reference Case: 00038985
 */
public class NATDataDefinitionVIC {
   
    // NAT00010 - Account Object Fields
    public static final String NationalProviderNumber  = 'Account.National_Provider_Number__c';
    public static final String SoftwareProductName = 'Account.Software_Product_Name__c';
    public static final String SoftwareVendorEmailAddress = 'Account.Software_Vendor_Email_Address__c';
    
    // NAT00020 - Location__c Object Fields
    public static final String TrainingOrganisationNationalProviderNumber = 'Training_Organisation__r.National_Provider_Number__c';
    public static final String ChildAddressbuildingpropertyname = 'Child_Address_building_property_name__c';
    public static final String ChildAddressflatunitdetails = 'Child_Address_flat_unit_details__c';
    public static final String ChildAddressstreetnumber = 'Child_Address_street_number__c';
    public static final String ChildAddressstreetname = 'Child_Address_street_name__c';
    
    // NAT00030 - Qualification__c Object Fields
    public static final String ReportableQualificationHours = 'VIC_Reportable_Qualification_Hours__c';
    
    // NAT00080 - Account Object
    public static final String StatisticalAreaLevel1Identifier='';
    public static final String StatisticalAreaLevel2Identifier='';
    public static final String VictorianStudentNumber='Victorian_Student_Number__c';
    public static final String ClientIndustryOfEmployment='Client_Industry_of_Employment__r.Division__c';
    public static final String ClientOccupationIdentifier='Client_Occupation_Identifier__c';
        
    //NAT00100 - Account Object
    public static final String PriorEducationAchievementRecognition='Prior_Education_Achievement_Recognition__c';
    public static final String PostalBuildingPropertyName = 'Postal_building_property_name__c';
    public static final String PostalFlatUnitDetails = 'Postal_flat_unit_details__c';
    public static final String PostalStreetNumber = 'Postal_street_number__c';
    public static final String PostalStreetName = 'Postal_street_name__c';
    public static final String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static final String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static final String PostalPostCode = 'PersonOtherPostalCode';
    
    //NAT00120 Enrolment Unit
    public static final String VictorianTuitionRate='Victorian_Tuition_Rate__c';
    public static final String HoursAttended='Hours_Attended__c';
    public static final String VictorianCommencementDate='Enrolment__r.Victorian_Commencement_Date__c';
    public static final String EligibilityExemption='Eligibility_Exemption__c';
    public static final String ANZSICCode='ANZSIC_Code__c';
    public static final String Name='Name';
    public static final String VictorianClientFeesOther='Client_Fees_Other__c';
    //NAT000120 Relatioship Fields
    public static final String EnrolmentStartDate='Enrolment__r.Start_Date__c';
    public static final String VETFeeHelpIndicator='Enrolment__r.VET_FEE_HELP_Indicator__c';
    public static final String SubcontractedTrainingOrganisation = 'Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c';
    public static final String TrainingOrganisationABN='Training_Delivery_Location__r.Training_Organisation_ABN__c';
    public static final String FundingEligibilityKey = 'Enrolment__r.FundingEligibilityKey__c';
    
    // NAT00130 Fields - Enrolment Object
    public static final String TrainingOrganisationIdentifier = 'Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c';
    public static final String IssuedFlag = 'IssuedFlag';
    public static final String YearProgramCompleted = 'YearProgramCompleted';
    public static final String YearProgramCompletedE = 'Year_Program_Completed__c';
    public static final String ParentQualificationCodeE = 'Parent_Qualification_Code__c';
    public static final String StudentIdentifierE = 'Student_Identifier__c';
    public static final String VictorianCommencementDateE ='Victorian_Commencement_Date__c';
    public static final String ProgramSupervisedTrainingActivityDate = 'End_Date__c';
    public static final String ProgramSupervisedHours = 'Qualification__r.Award_based_on__r.VTGPUSH__c';

    //Utility Fields
    public static final String LastFirst = 'LastFirst';
    public static final String BlankField = 'BlankField';
    
    
    public static Map< Integer, NATDataElement > nat00010() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00010();
        
        //replace standard AVETMISS field
        positions.put(1, new NATDataElement(NationalProviderNumber, 10, true, 'string' ) );
        positions.put(9, new NATDataElement(LastFirst, 60, true, 'string') );

        
        //add below the line fields
        positions.put(13, new NATDataElement(SoftwareProductName, 20, true, 'string') );
        positions.put(14, new NATDataElement(SoftwareVendorEmailAddress, 80, true, 'string') );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00020() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00020();
        
        //replace standard AVETMISS field
        positions.put(1, new NATDataElement(TrainingOrganisationNationalProviderNumber, 10, true, 'number' ) );
        
        //add below the line fields
        positions.put(8, new NATDataElement(ChildAddressbuildingpropertyname, 50, FALSE, 'string') );
        positions.put(9, new NATDataElement(ChildAddressflatunitdetails, 30, FALSE, 'string') );
        positions.put(10, new NATDataElement(ChildAddressstreetnumber, 15, FALSE, 'string') );
        positions.put(11, new NATDataElement(ChildAddressstreetname, 70, FALSE, 'string') );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00030() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00030();
        
        //replace standard AVETMISS field
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, FALSE, 'number' ) );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00060() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00060();
        return positions;
    }
   
       public static Map< Integer, NATDataElement > nat00080() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00080();
        
        //replace standard AVETMISS field           
        positions.put(23, new NATDataElement(BlankField, 11, true, 'string' ) );
        positions.put(24, new NATDataElement(BlankField, 9, true, 'string' ) );
        positions.put(25, new NATDataElement(VictorianStudentNumber, 9, FALSE, 'string' ) );
        positions.put(26, new NATDataElement(ClientIndustryOfEmployment, 1, true, 'string' ) );
        positions.put(27, new NATDataElement(ClientOccupationIdentifier, 1, FALSE, 'string' ) );
               
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00085() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00085();
        
        positions.put(5, new NATDataElement(PostalBuildingPropertyName, 50, true, 'string') );
        positions.put(6, new NATDataElement(PostalFlatUnitDetails, 30, true, 'string') );
        positions.put(7, new NATDataElement(PostalStreetNumber, 15, true, 'string') );
        positions.put(8, new NATDataElement(PostalStreetName, 70, true, 'string' ) ); 
        positions.put(9, new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ); 
        positions.put(10, new NATDataElement(PostalSuburbLocalityTown, 50, true, 'string' ) ); 
        positions.put(11, new NATDataElement(PostalPostCode, 4, true, 'string' ) );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00090() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00090();
        return positions;
    }
        
     
    public static Map< Integer, NATDataElement > nat00100() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00100();
        
        //replace standard AVETMISS field
        positions.put(2, new NATDataElement(PriorEducationAchievementRecognition, 3, true, 'number' ) );
       // positions.put(3, new NATDataElement(PriorEducationAchievementRecognition, 1, true, 'string' ) );
        
       
        return positions;
    }
    
    /**
     * @description N/A
     * @author N/A
     * @date N/A
     * @history
     *       01.FEB.2016    Ranyel Maliwanag    Added Position 33: Funding Eligibility Key. Reference Case: 00038985
     */
    public static Map< Integer, NATDataElement > nat00120() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00120();
        
        positions.put(19, new NATDataElement(VictorianTuitionRate, 4, false, 'number' ) );
        positions.put(23, new NATDataElement(HoursAttended, 4, true, 'number' ) );
        positions.put(25, new NATDataElement(VictorianCommencementDate, 8, true, 'date' ) ); // why is it commented
        positions.put(26, new NATDataElement(EligibilityExemption, 1, false, 'string' ) );
        positions.put(27, new NATDataElement(VETFeeHelpIndicator, 1, true, 'string' ) );
        positions.put(28, new NATDataElement(ANZSICCode, 2, false, 'string' ) );
        positions.put(29, new NATDataElement(EnrolmentStartDate, 8, true, 'date' ) );
        positions.put(30, new NATDataElement(Name, 50, false, 'string' ) ); 
        positions.put(31, new NATDataElement(VictorianClientFeesOther, 5, false, 'number' ) );
        positions.put(32, new NATDataElement(SubcontractedTrainingOrganisation, 11, true, 'string' ) );
        positions.put(33, new NATDataElement(FundingEligibilityKey, 10, true, 'string'));
        
        /* replace standard AVETMISS field
        positions.put(18, new NATDataElement(VictorianTuitionRate, 4, false, 'number' ) );
        positions.put(22, new NATDataElement(HoursAttended, 4, true, 'number' ) );
        //positions.put(24, new NATDataElement(VictorianCommencementDate, 8, false, 'date' ) );
        positions.put(25, new NATDataElement(EligibilityExemption, 1, false, 'string' ) );
        positions.put(26, new NATDataElement(VETFeeHelpIndicator, 1, true, 'string' ) );
        positions.put(27, new NATDataElement(ANZSICCode, 2, false, 'number' ) );
        positions.put(28, new NATDataElement(EnrolmentStartDate, 8, true, 'date' ) );
        positions.put(29, new NATDataElement(Name, 50, false, 'string' ) ); 
        positions.put(30, new NATDataElement(VictorianClientFeesOther, 5, true, 'number' ) );
        positions.put(31, new NATDataElement(SubcontractedTrainingOrganisation, 11, true, 'number' ) ); */  
       
        return positions;
    }
    

    /**
     * @description N/A
     * @author N/A
     * @date N/A
     * @history
     *       01.FEB.2016    Ranyel Maliwanag    - Added position 7: Program Supervised Training Activity Date, and position 8: Program Unique Supervised Hours. Reference Case: 00038985
     */
    public static Map< Integer, NATDataElement > nat00130() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00130();
        
        //replace standard AVETMISS field
        positions.put(1, new NATDataElement(TrainingOrganisationIdentifier, 10, true, 'string') );  
        positions.put(4, new NATDataElement(YearProgramCompletedE, 4, false, 'string') );
        positions.put(6, new NATDataElement(VictorianCommencementDate, 8, true, 'date') );
        positions.put(7, new NATDataElement(ProgramSupervisedTrainingActivityDate, 8, true, 'date'));
        positions.put(8, new NATDataElement(ProgramSupervisedHours, 5, true, 'number'));
  
        return positions;
    }
    
        public static Map< Integer, NATDataElement > nat00130noAwards() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00130();
        
        //replace standard AVETMISS field
        positions.put(1, new NATDataElement(TrainingOrganisationIdentifier, 10, true, 'string') );
        positions.put(2, new NATDataElement(ParentQualificationCodeE, 10, false, 'string') );
        positions.put(3, new NATDataElement(StudentIdentifierE, 10, false, 'string') );
        positions.put(4, new NATDataElement(YearProgramCompleted, 4, true, 'string') );
        positions.put(5, new NATDataElement(IssuedFlag, 1, true, 'string') );
        positions.put(6, new NATDataElement(VictorianCommencementDateE, 8, false, 'date') );
        positions.put(7, new NATDataElement(ProgramSupervisedTrainingActivityDate, 8, true, 'date'));
        positions.put(8, new NATDataElement(ProgramSupervisedHours, 5, true, 'number'));

  
        return positions;
    }
}