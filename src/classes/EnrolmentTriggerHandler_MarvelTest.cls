/**
 * @description Test class for Enrolment Trigger Handler. Part of Project Marvel: Epic Code Cleanup
 * @author Ranyel Maliwanag
 * @date 11.MAY.2016
 */
@isTest
public with sharing class EnrolmentTriggerHandler_MarvelTest {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;
	}


	/**
	 * @description Tests the creation of Inplace Exports
	 * @author Ranyel Maliwanag
	 * @date 11.MAY.2016
	 */
	static testMethod void createInplaceExportTest() {
		Account student = [SElECT Id FROM Account];
		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.IPS_InPlace_Student__c = true;
		insert enrolment;

		Test.startTest();
			enrolment.Enrolment_Status__c = 'Suspended';
			update enrolment;
		Test.stopTest();
	}


	/**
	 * @description Tests the creation of Census Eligibility History
	 * @author Ranyel Maliwanag
	 * @date 12.MAY.2016
	 */
	static testMethod void recordEligibilityChangeTest() {
		Account student = [SELECT Id FROM Account];
		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
        enrolment.Assigned_Trainer__c = UserInfo.getUserId();
        insert enrolment;

        Test.startTest();
        	enrolment = [SELECT Id, Eligible_for_C1__c, (SELECT Id FROM CensusEligibilityHistory__r) FROM Enrolment__c WHERE Id = :enrolment.Id];

        	System.assert(!enrolment.Eligible_for_C1__c);
        	System.assert(enrolment.CensusEligibilityHistory__r.isEmpty());
        	
        	enrolment.Eligible_for_C1__c = true;
        	update enrolment;

        	enrolment = [SELECT Id, Eligible_for_C1__c, (SELECT Id FROM CensusEligibilityHistory__r) FROM Enrolment__c WHERE Id = :enrolment.Id];

        	System.assert(enrolment.Eligible_for_C1__c);
        	System.assert(!enrolment.CensusEligibilityHistory__r.isEmpty());
        Test.stopTest();
	}


	/**
	 * @method populateStudyReasonField
	 * @scenario Enrolment record gets created with `Study_Reason__c` field populated as `StudyReasons` and `Reason_for_Study__c` field populated with a proper value
	 * @acceptanceCrtieria `Study_Reason__c` field value should not be `StudyReasons`
	 * @acceptanceCriteria `Study_Reason__c` field should pull values from the `Reason_for_Study__c`
	 * @date 20.JUN.2016
	 */
	static testMethod void populateStudyReasonFieldTest() {
		Study_Reason_Type__c reasonForStudy = SASTestUtilities.createStudyReasonType();
		insert reasonForStudy;

		Account student = [SELECT Id FROM Account];
		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.RecordTypeId = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get('Dual Funding Enrolment').getRecordTypeId();
		enrolment.Study_Reason__c = 'StudyReasons';
		enrolment.Reason_for_Study__c = reasonForStudy.Id;

		Test.startTest();
			insert enrolment;

			enrolment = [SELECT Id, Reason_for_Study__c, Reason_for_Study__r.Name, Reason_for_Study__r.Code__c, Study_Reason__c FROM Enrolment__c WHERE Id = :enrolment.Id];
			System.assertNotEquals('StudyReasons', enrolment.Study_Reason__c);
			System.assertEquals(enrolment.Reason_for_Study__r.Code__c + ' - ' + enrolment.Reason_for_Study__r.Name, enrolment.Study_Reason__c);
		Test.stopTest();
	}


	/**
	 * @method populateStudyReasonField
	 * @scenario Enrolment record gets created with `Study_Reason__c` field populated as `StudyReasons` and `Reason_for_Study__c` gets updated
	 * @acceptanceCrtieria `Study_Reason__c` field value should not be `StudyReasons`
	 * @acceptanceCriteria `Study_Reason__c` field should pull values from the `Reason_for_Study__c`
	 * @date 20.JUN.2016
	 */
	static testMethod void populateStudyReasonFieldTest2() {
		Study_Reason_Type__c reasonForStudy = SASTestUtilities.createStudyReasonType();
		insert reasonForStudy;

		Account student = [SELECT Id FROM Account];
		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.RecordTypeId = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get('Dual Funding Enrolment').getRecordTypeId();
		enrolment.Study_Reason__c = 'StudyReasons';
		insert enrolment;
		

		Test.startTest();
			enrolment.Reason_for_Study__c = reasonForStudy.Id;
			update enrolment;

			enrolment = [SELECT Id, Reason_for_Study__c, Reason_for_Study__r.Name, Reason_for_Study__r.Code__c, Study_Reason__c FROM Enrolment__c WHERE Id = :enrolment.Id];
			System.assertNotEquals('StudyReasons', enrolment.Study_Reason__c);
			System.assertEquals(enrolment.Reason_for_Study__r.Code__c + ' - ' + enrolment.Reason_for_Study__r.Name, enrolment.Study_Reason__c);
		Test.stopTest();
	}
}