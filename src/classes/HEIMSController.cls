/**
 * @history
 *       02.SEP.2015    Ranyel Bryan    - Updated criteria for `VCC/VCU` display
 *       06.JAN.2016    Ranyel Bryan    - Added Unique Student Identifier to the filteredEnrolmentsQuery string of generateHEIMSData()
 *       22.JUN.2016    Biao Zhang      - Add Enrolment Contract Start Date
 */
public with sharing class HEIMSController {
    
    public String display {get;set;} //show correct data table or tab
    
    public Claim_Number__c dummyClaimNumber {get;set;} // date picker
    
    public Boolean includePreviouslyExported {get;set;}
    public Boolean includeNotExported {get;set;}
    public Boolean flagExported {get;set;}
    public Boolean canGeneration {get;set;}
    
    public List<SelectOption>files {get;set;}
    
    //public List<Enrolment_Unit_of_Study__c> filteredEnrolUnitsOfStudy {get;set;}
    //public List<Enrolment__c>filteredEnrolments {get;set;} //VCC only
 
    public String rto {get;set;}
    public List<SelectOption> rtos {get;set;}
    
    public Account TrainingOrg {get;set;}
    public HEIMS_Submissions__c SubmissionRecord {get;set;}
    
    public transient List<EnrolmentUnitOfStudyWrapper> datalist {get;set;}
    public transient List<EnrolmentUnitOfStudyWrapper> datalist2 {get;set;} //for tabbed file
    public transient List<EnrolmentWrapper> datalistEnrolments {get;set;} // required for Enrolments
    
    //public EnrolmentUnitOfStudyWrapper w {get;set;}
    //public EnrolmentWrapper w2 {get;set;}
    
    public String executeSearchString {get; set;}
    private final String searchString = 'HEIMS';
    private final String path = '/sfc/#search';
    
    EnrolmentUnitOfStudyIterable enrolmentUnitOfStudyIterator;
    EnrolmentIterable enrolmentIterator;
    
    String enrolmentUnitOfStudyQuery;
    Date dtStart1;
    Date dtEnd1;
    String filteredEnrolmentsQuery;
    Set<Id>relatedEnrolmentIds= new Set<Id>();

    public HEIMSController() {
        
        /*Grab RTOs */
        Id toRecTypeId = [Select Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Training_Organisation' LIMIT 1].Id;
        List<Account>RTOsList = New List<Account>([SELECT Id, Name FROM Account WHERE RecordTypeID = :toRecTypeId AND WebsiteOnlyAccount__c = false]);

        rtos = new List<SelectOption>();
        
        //Add RTOs to Select list and display on the page
        for(Account a : RTOsList)
            rtos.add(new SelectOption(a.id,a.name));
        
        //Add file types here
        files = new List<SelectOption>();
        
        files.add(new SelectOption('VLL/VEN', 'VLL/VEN'));
        files.add(new SelectOption('VDU', 'VDU'));
        files.add(new SelectOption('VCC/VCU','VCC/VCU'));
        //files.add(new SelectOption('CHESSN', 'CHESSN')); DISABLED
        //files.add(new SelectOption('VCO', 'VCO')); DISABLED
        //files.add(new SelectOption('VER', 'VER')); DISABLED
        //files.add(new SelectOption('VSR', 'VSR')); DISABLED

        dummyClaimNumber = new Claim_Number__c();
        
        flagExported = false;

    }
    
    public PageReference NextButton1() {
        
        trainingOrg = [Select Id, Name,HEIMS_Provider_Number__c From Account WHERE id = : rto];
        SubmissionRecord = new HEIMS_Submissions__c ();
        
        insert SubmissionRecord;
        SubmissionRecord = [Select Id, Name FROM HEIMS_Submissions__c WHERE Id = :SubmissionRecord.Id];
        //requery submissionRecord
        
        return new PageReference('/apex/HEIMSGeneration2');
    }
    
    public PageReference NextButton2() {
        
        if(Display == '' || Display == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select Type Of Export'));
            return null;
        }
        
        if(Display != 'VLL/VEN' && flagExported == true ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Flag As Exported Can Only Be Set FOR VLL/VEN files'));
            return null;
            
        }
        
        generateHEIMSData();
        
        return new PageReference('/apex/HEIMSGeneration3');
    }
    
    public PageReference CancelButton() {
        return new PageReference('/apex/HEIMSGeneration');
    }
    
    public PageReference refreshData() {
        
        generateHEIMSData();
        
        return null;
        
    }
    
    public PageReference backButton() {
        return new PageReference('/HIEMSGeneration2');
    }
    
    public void generateHEIMSData() {

        datalist = new List<EnrolmentUnitOfStudyWrapper>();
        datalist2 = new List<EnrolmentUnitOfStudyWrapper>();
        dataListEnrolments = new List<EnrolmentWrapper>();
        //filteredEnrolUnitsOfStudy = new List<Enrolment_Unit_of_Study__c>();
        //filteredEnrolments = new List<Enrolment__c>();
        
        //Set<Id>relatedEnrolmentIds= new Set<Id>();

        dtStart1 = dummyClaimNumber.Report_Start_Date__c;
        dtEnd1 = dummyClaimNumber.Report_End_Date__c;

        //Enrolment Units Of Study query
        
        enrolmentUnitOfStudyQuery = 'Select Id, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Student_Identifer__c, '+
                                        'Tax_File_Number__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonBirthDate, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Sex_Identifier__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Qualification__r.Award_based_on__r.Name, '+
                                        'Enrolment_Unit__r.Enrolment__r.Qualification__r.Award_based_on__r.Qualification_Name__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Qualification__r.Award_based_on__r.Field_of_Education_Identifier__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Qualification__r.Course_Load__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonTitle, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Salutation, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.LastName, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.FirstName, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Middle_Name__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Full_Student_Name__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Course_Commencement_Date__c, '+
                                        //MAM 09/26/2013 start query student mailing address
                                        /*'Enrolment_Unit__r.Enrolment__r.Mailing_Street__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_City__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_State_Provience__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_Zip_Postal_Code__c, '+
                                        'Postal_Address__c, '+ */
                                        //MAM 01/20/2014 start query enrolment residential and postal address
                                        /*'Enrolment_Unit__r.Enrolment__r.Student__r.PersonMailingStreet, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonMailingCity, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonMailingState, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonMailingPostalCode, '+
                                        //MAM 11/25/2013 add Student Postal Address
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonOtherStreet, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonOtherCity, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonOtherState, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonOtherPostalCode, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.PersonOtherCountry, '+*/
                                        //MAM 11/25/2013 end
                                        //MAM 09/26/2013 end
                                        //Residential Address
                                        'Enrolment_Unit__r.Enrolment__r.Address_building_property_name__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Address_flat_unit_details__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Address_street_number__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Address_street_name__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_City__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_State_Provience__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_Zip_Postal_Code__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Mailing_Country__c, '+
                                        //Postal Address
                                        'Enrolment_Unit__r.Enrolment__r.Other_Street__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Other_City__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Other_State_Provience__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Other_Zip_Postal_Code__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Other_Country__c, '+
                                        //MAM 01/20/2014 end
                                        //MAM 10/11/2013 change CHESSN call to CHESSN2__c
                                        //'CHESSN__c, '+
                                        //'Enrolment_Unit__r.Enrolment__r.Student__r.CHESSN__c ' +
                                        'CHESSN2__c, '+ 
                                        //MAM 10/11/2013
                                        'Citizenship_Resident_Indicator__c, '+
                                        'Country_of_Birth_Code__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Former_Surname__c, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Country_of_permanent_residence_lookup__r.Name, '+
                                        'Enrolment_Unit__r.Enrolment__r.Student__r.Country_of_permanent_residence_lookup__r.Country_Code__c, '+
                                        'Reporting_Year_Period__c, '+
                                        'Variation_Reason__r.Code__c, '+
                                        'Course_of_Study_Type_Code__c, '+
                                        'Unit_of_Study_Code__c, '+
                                        'Academic_Organisation_Unit_Code__c, '+
                                        'Census_Date__c, '+
                                        'Result__c, '+
                                        'Result__r.Code__c, '+ //MAM 04/11/2014 new Result Field
                                        'Location_code_of_term_residence__c, '+
                                        'Location_code_of_perm_home_residence__c, '+
                                        'Basis_for_Admission_Code__c, '+
                                        'Type_of_Attendance__r.Code__c, '+
                                        'Year_of_Arrival_in_Australia_Code__c, '+
                                        'Language_Spoken_at_Home_Code__c, '+
                                        'Credit_Offered_Value_Indicator__c, '+
                                        'Credit_status_Higher_Ed_provider_ID__c, '+
                                        'Disability_Code__c, '+
                                        'Name_of_suburb_town_locality__c, '+
                                        'Highest_participation_code__c, '+
                                        'Credit_Used_Value_Indicator__c, '+
                                        'RPL_details_of_prior_study__r.Code__c, '+
                                        'Field_of_Ed_of_prior_VET_Credit_RPL_Code__c, '+
                                        'Level_of_prior_study_for_RPL_Indicator__c, '+
                                        'Type_of_provider_where_study_undertaken__r.Code__c, '+
                                        'Reason_for_Study__r.Code__c, '+
                                        'Labour_Force_Status__c, '+
                                        'Start_Date__c, '+
                                        'Discipline_code__c, '+
                                        'Mode_of_Attendance_Indicator__c, '+
                                        'EFTSL__c, '+
                                        'Student_Status_Code_Indicator__c, '+
                                        'Total_amount_charged__c, '+
                                        'Amount_paid_upfront__c, '+
                                        'Loan_Fee_Code__c, '+
                                        'Unit_of_study_HELP_Debt_UI__c, '+
                                        'Recognition_of_Prior_Learning_Status__r.Code__c, '+
                                        'Campus_Location_Code__c, '+
                                        'Post_code_of_Year_12_Perm_home_residence__c, '+
                                        'Aboriginal_and_Torres_Strait_Code__c, '+
                                        'High_School_State__r.Short_Name__c, '+
                                        'Attended_Grade_12__r.HEIMS_Report_Value__c, '+
                                        'Domestic_School_Leaver__c, '+
                                        'Year_12_Student_Number__c, '+
                                        'Year_12_School_Name__c, '+
                                        'The_year_in_which_Year_12_was_attended__c, '+
                                        'Name, '+
                                        'Enrolment_Unit__r.Enrolment__r.Enrolment_Status__c, '+
                                        'Enrolment_Unit__r.Enrolment__c, '+
                                        'Course_of_Study_commencement_date__c, '+
                                        'Unit_of_Study_Census_Date__c, '+
                                        'Unit_of_study_HELP_Debt__c, ' + 
            							'CensusId__c ' +
                                 'FROM Enrolment_Unit_of_Study__c ';
        enrolmentUnitOfStudyQuery += 'WHERE Census_Date__c >=: dtStart1 AND Census_Date__c <=: dtEnd1 AND ';
        
        enrolmentUnitOfStudyQuery += 'Enrolment_Unit__r.Unit_Result__r.Name != \'Credit Transfer\' AND ' + //MAM 09/13/2013 add Filter to check CCT Units (Enrolment Unit - Unit Result field)
                                 'HEIMS_Exported__c =: includePreviouslyExported AND '+
                                 'Include_in_HEIMS_reporting__c = \'Yes\' ';
        
        //if(display == 'VCC/VCU') {
        //    enrolmentUnitOfStudyQuery = enrolmentUnitOfStudyQuery + 'AND Enrolment_Unit__r.Enrolment__r.Enrolment_Status__c = \'Completed\'';
        //}
        
        enrolmentUnitOfStudyQuery = enrolmentUnitOfStudyQuery + 'LIMIT 1000';  //MAM 09/20/2013 use limits to reduce heap size
        enrolmentUnitOfStudyIterator = new EnrolmentUnitOfStudyIterable(enrolmentUnitOfStudyQuery, dtStart1, dtEnd1, includePreviouslyExported);
        enrolmentUnitOfStudyIterator.setPageSize = 100;
                                    
        //Filter enrolment units for study for enrolment query  
        relatedEnrolmentIds = enrolmentUnitOfStudyIterator.relatedEnrolmentIds;

        if(display == 'VLL/VEN'||display == 'VCC/VCU') {
            system.debug('**THIS');
            datalist2 = enrolmentUnitOfStudyIterator.next();
            system.debug('**Datalist: ' + datalist2.size());
        } else {
            datalist = enrolmentUnitOfStudyIterator.next();
        }

        //enrolment query
        filteredEnrolmentsQuery = 'Select Id, ' +
                                     'Student__r.Student_Identifer__c, ' +
                                     'Qualification__r.Award_based_on__r.Name, ' +
                                     //MAM 01/20/2014 start query enrolment residential and postal address
                                     //MAM 09/26/2013 start query student mailing address
                                     /*'Student__r.PersonMailingStreet, '+
                                     'Student__r.PersonMailingCity, '+
                                     'Student__r.PersonMailingState, '+
                                     'Student__r.PersonMailingPostalCode, '+
                                     'Student__r.PersonMailingCountry, '+*/
                                     //MAM 09/26/2013 end
                                     //MAM 11/25/2013 add Student Postal Address
                                     /*'Student__r.PersonOtherStreet, '+
                                     'Student__r.PersonOtherCity, '+
                                     'Student__r.PersonOtherState, '+
                                     'Student__r.PersonOtherPostalCode, '+
                                     'Student__r.PersonOtherCountry, '+*/
                                     //MAM 11/25/2013 end
                                     //Residential Address
                                     'Address_building_property_name__c, '+
                                     'Address_flat_unit_details__c, '+
                                     'Address_street_number__c, '+
                                     'Address_street_name__c, '+
                                     'Mailing_City__c, '+
                                     'Mailing_State_Provience__c, '+
                                     'Mailing_Zip_Postal_Code__c, '+
                                     'Mailing_Country__c, '+
                                     //Postal Address
                                     'Other_Street__c, '+
                                     'Other_City__c, '+
                                     'Other_State_Provience__c, '+
                                     'Other_Zip_Postal_Code__c, '+
                                     'Other_Country__c, '+
                                     //MAM 01/20/2014 end
                                     //MAM 10/11/2013 change CHESSN call to CHESSN2__c
                                     //'CHESSN__c, ' +
                                     //'Student__r.CHESSN__c, ' +
                                     'CHESSN2__c, ' +
                                     //MAM 10/11/2013 end
                                     'Reporting_Year_Period__c, ' +
                                     'Variation_Reason_Code__r.Code__c, ' +
                                     'Name, ' +
                                     'Disability_Code__c, ' +
                                     'Basis_for_Admission__r.Code__c, ' +
                                     'Type_of_Attendance__r.Code__c, ' +
                                     'Student__r.Sex_Identifier__c, ' +
                                     'Aboriginal_and_Torres_Strait_Is_Status__r.Code__c, ' +
                                     'Student__r.Main_Language_Spoken_at_Home__r.Code__c, ' +
                                     'Field_of_Ed_of_prior_VET_Credit_RPL_code__c, ' +
                                     'Level_of_prior_study_for_credit_RPL_code__c, ' +
                                     'Student__r.Country_of_Birth__r.Country_Code__c, ' +
                                     'Type_of_provider_where_study_done_code__c, ' +
                                     'Credit_Offered_Value_code__c, ' +
                                     'Credit_status_Higher_Ed_provider_code__c, ' +
                                     'Reason_for_Study__r.Code__c, ' +
                                     'Labour_Force_Status__r.Code__c, ' +
                                     'Highest_Educational_Participation__r.Code__c, ' +
                                     'Credit_Used_Value_code__c, ' +
                                     'RPL_details_of_prior_study_code__c, ' +
                                     'Student__r.PersonBirthDate, ' +
                                     'Location_code_of_perm_residence__c, ' +
                                     'Year_of_arrival_in_Australia__c, ' +
                                     'Location_code_of_term_residence__c, ' +
                                     'Commencing_location_code_of_perm_home__c, ' +
                                     'Overseas_Country__c, ' +
                                     //MAM 09/26/2013 comment out to lessen the number of queries - use Student Address instead
                                     /*'Other_Country__c, ' +
                                     'Mailing_City__c, ' +
                                     'Mailing_Country__c, ' +
                                     'Mailing_Street__c, ' +
                                     'Mailing_State_Provience__c, ' +
                                     'Mailing_Zip_Postal_Code__c, ' +
                                     */
                                     //MAM 09/26/2013 end
                                     'Does_student_have_a_disability__c, ' +
                                     'Indicate_Areas_of_Imparement__c, ' +
                                     'Requested_advice_on_support_services__c, ' +
                                     'Student__r.Arrival_Status_Identifer__c, ' +
                                     'Student__r.HEIMS_347_Year_of_arrival_in_Australia_1__c, ' +
                                     'Domestic_School_Leaver__c, ' +
                                     'Tax_File_Number__c, ' +
                                     'Student__r.PersonTitle, ' +
                                     'Student__r.Salutation, ' +
                                     'Student__r.FirstName, ' +
                                     'Student__r.LastName, ' +
                                     'Student__r.Middle_Name__c,' +
                                     'Student__r.Full_Student_Name__c, '+ //MAM 04/11/2014
                                     'Name_of_suburb_town_locality__c, ' +
                                     'Post_code_of_Year_12_Perm_home_residence__c, ' +     
                                     'Year_of_last_participation__c, ' +
                                     'Student__r.Year_of_Arrival_in_Australia__c, ' +
                                     'Student__r.Country_of_Birth__r.Name, ' +
                                     'Field_of_Ed_of_prior_VET_Credit_RPL__c, ' +
                                     'Credit_Offered_Value_full_time_load__c, ' +
                                     'Credit_Used_Value_full_time_load__c ,' +
                                     'Student__c, ' +
                                     'Student__r.Sex__c, ' +
                                     'Credit_status_Higher_Ed_provider__c, ' +
                                     'Credit_status_Higher_Ed_provider__r.Code__c, ' +
                                     'Field_of_Ed_of_prior_VET_Credit_RPL__r.FoE_Code__c, ' +
                                     'Student__r.Unique_student_identifier__c ' +
                              'FROM Enrolment__c ' +
                              'WHERE (RecordType.DeveloperName = \'VET_Fee_Help_Enrolement\' OR ' +
                                     'RecordType.DeveloperName = \'Dual_Funding_Enrolment\') AND ';
        if (display == 'VCC/VCU') {
            // Look at end dates
            filteredEnrolmentsQuery += 'End_Date__c >=: dtStart1 AND End_Date__c <=: dtEnd1 AND Enrolment_Status__c = \'Completed\'';
        } else {
            filteredEnrolmentsQuery += 'Id in : relatedEnrolmentIds ';
        }
        filteredEnrolmentsQuery += ' LIMIT 1000'; //MAM 09/20/2013 use limits to reduce heap size
        
        enrolmentIterator = new EnrolmentIterable(filteredEnrolmentsQuery, relatedEnrolmentIds, dtStart1, dtEnd1);
        enrolmentIterator.setPageSize = 100;
        dataListEnrolments = enrolmentIterator.next();

    }
    
    public pageReference exportFile() {
        //MAM 09/20/2013 remove heaps in queries before exporting start
        enrolmentUnitOfStudyQuery = enrolmentUnitOfStudyQuery.replace('LIMIT 1000','');
        
        //Perform another enrolmentUnitOfStudyIterator  run to get ALL Enrolment Records for HEIMS Reporting
        enrolmentUnitOfStudyIterator = new EnrolmentUnitOfStudyIterable(enrolmentUnitOfStudyQuery, dtStart1, dtEnd1, includePreviouslyExported);
        relatedEnrolmentIds = enrolmentUnitOfStudyIterator.relatedEnrolmentIds;
        
        filteredEnrolmentsQuery = filteredEnrolmentsQuery.replace('LIMIT 1000','');
        //MAM 09/20/2013 end
        
        if(!Test.isRunningTest()){
            ExportAllEUOS batch = new ExportAllEUOS(display,flagExported,SubmissionRecord,dummyClaimNumber,TrainingOrg,dtStart1,dtEnd1,includePreviouslyExported,enrolmentUnitOfStudyQuery,canGeneration);
            Database.executeBatch(batch,2000);
        }
        
        if(!Test.isRunningTest()){
            ExportAllEnrolment batch2 = new ExportAllEnrolment(display,flagExported,SubmissionRecord,dummyClaimNumber,TrainingOrg,relatedEnrolmentIds,filteredEnrolmentsQuery,dtEnd1,dtStart1);
            Database.executeBatch(batch2,2000);
        }
        
        return new PageReference('/sfc/#search?');
    }
    
    public void nextEnrolmentUnitOfStudy() {
        if(display == 'VLL/VEN'||display == 'VCC/VCU') {
            datalist2 = enrolmentUnitOfStudyIterator.next();
        } else {
            datalist = enrolmentUnitOfStudyIterator.next();
        }
        
    }
    
    public void previousEnrolmentUnitOfStudy() {
        if(display == 'VLL/VEN'||display == 'VCC/VCU') {
            datalist2 = enrolmentUnitOfStudyIterator.previous();
        } else {
            datalist = enrolmentUnitOfStudyIterator.previous();
        }
    }
    
    public void nextEnrolment() {
        dataListEnrolments = enrolmentIterator.next();
    }

    public void previousEnrolment() {
        dataListEnrolments = enrolmentIterator.previous();
    }
    
    public Boolean hasNextEnrolmentUnitOfStudy {
        get{
            return enrolmentUnitOfStudyIterator.hasNext();
        }
        set;
    }
    
    public Boolean hasPreviousEnrolmentUnitOfStudy {
        get{
            return enrolmentUnitOfStudyIterator.hasPrevious();
        }
        set;
    }
    
    public Boolean hasNextEnrolment {
        get{
            return enrolmentIterator.hasNext();
        }
        set;
    }
    
    public Boolean hasPreviousEnrolment {
        get{
            return enrolmentIterator.hasPrevious();
        }
        set;
    }
    
}