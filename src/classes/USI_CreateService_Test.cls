@isTest
public class USI_CreateService_Test {
    /*
public static Account student = NATTestDataFactory.createStudent();
public static USI_Identification_Document__c usiDoc = NATTestDataFactory.createUSIDocument(student.Id);
public static Country__c ctry = NATTestDataFactory.createCountry();
public static ANZSCO__c anz = NATTestDataFactory.createANZSCO();
public static Unit__c unt = NATTestDataFactory.createUnit();
public static Account tOrg = NATTestDataFactory.createTrainingOrganisation();
public static Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();
public static USI_Integration_Log__c usiLog = USI_LogManager.createUSILog();

public static State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
public static Results__c res = NATTestDataFactory.createResult(sta.Id);
public static Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
public static Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
public static Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);

public static Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);   
public static Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
public static Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
public static Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
public static Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
public static Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
public static Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
public static Enrolment__c enrl = NATTestDataFactory.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
public static Enrolment_Unit__c enu1 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);
public static Enrolment_Unit__c enu2 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);   
*/
    
    public class usiCreateResponse {
        String USI;
        String message;
        String status;
    }
    
    public class usiVerifyResponse {
        String dateofbirth; 
        String firstname;
        String usi;
        String message;
        String lastname;
    }
    
    @testSetup
    static void insertCustomSetting() {
        
        USI_Configuration__c usiConfig = new USI_Configuration__c();
        
        usiConfig.Name = 'Default';
        usiConfig.Timeout__c = 60000;
        usiConfig.VerifyUSI_endpoint__c = '/verifyUSI';
        usiConfig.CreateUSI_endpoint__c = '/createUSI';
        
        insert usiConfig;        
        
    }
    /*
public static testMethod void insertStuff() {

test.startTest();

Account student = USI_testDataFactory.createStudent();
insert student;
USI_Identification_Document__c usiDoc = USI_testDataFactory.createUSIDocument(student.Id);
insert usiDoc;
Country__c ctry = USI_testDataFactory.createCountry();
insert ctry;
ANZSCO__c anz = USI_testDataFactory.createANZSCO();
insert anz;
Unit__c unt = USI_testDataFactory.createUnit();
insert unt;
Account tOrg = USI_testDataFactory.createTrainingOrganisation();
insert tOrg;
Field_of_Education__c foe = USI_testDataFactory.createFieldOfEducation();
insert foe;
USI_Integration_Log__c usiLog = USI_testDataFactory.createUSILog();
insert usiLog;

State__c sta = USI_testDataFactory.createStateQLD(ctry.Id);
insert sta;
Results__c res = USI_testDataFactory.createResult(sta.Id);
insert res;
Location_Loadings__c lload = USI_testDataFactory.createLocationLoadings(sta.Id);
insert lload;
Locations__c ploc = USI_testDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
insert ploc;
Locations__c loc = USI_testDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
insert loc;

Funding_Source__c fsrc = USI_testDataFactory.createFundingSource(sta.Id);  
insert fsrc;
Qualification__c qual = USI_testDataFactory.createQualification(anz.Id, foe.Id);
insert qual;
Qualification_Unit__c qualu = USI_testDataFactory.createQualificationUnit(qual.Id, unt.Id);
insert qualu;
Purchasing_Contracts__c pcon = USI_testDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id); 
insert pcon;
Contract_Line_Items__c cli = USI_testDataFactory.createContractLineItem(pcon.id);
insert cli;
Line_Item_Qualifications__c liq = USI_testDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
insert liq;
Enrolment__c enrl = USI_testDataFactory.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
insert enrl;

test.stopTest();
} */
    
    
    public static testMethod void testCreateUsiCallout() {
        
        /*
        test.startTest();

        
        Account student = USI_testDataFactory.createStudent();
        insert student;
        USI_Identification_Document__c usiDoc = USI_testDataFactory.createUSIDocument(student.Id);
        insert usiDoc;
        Country__c ctry = USI_testDataFactory.createCountry();
        insert ctry;
        ANZSCO__c anz = USI_testDataFactory.createANZSCO();
        insert anz;
        Unit__c unt = USI_testDataFactory.createUnit();
        insert unt;
        Unit_Hours_and_Points__c uhp = USI_testDataFactory.createUnitHoursAndPoints(unt.Id, null);
        insert uhp;        
        Account tOrg = USI_testDataFactory.createTrainingOrganisation();
        insert tOrg;
        Field_of_Education__c foe = USI_testDataFactory.createFieldOfEducation();
        insert foe;
        USI_Integration_Log__c usiLog = USI_testDataFactory.createUSILog();
        insert usiLog;
        
        State__c sta = USI_testDataFactory.createStateQLD(ctry.Id);
        insert sta;
        Results__c res = USI_testDataFactory.createResult(sta.Id);
        insert res;
        Location_Loadings__c lload = USI_testDataFactory.createLocationLoadings(sta.Id);
        insert lload;
        Locations__c ploc = USI_testDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
        insert ploc;
        Locations__c loc = USI_testDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
        insert loc;
        
        Funding_Source__c fsrc = USI_testDataFactory.createFundingSource(sta.Id);  
        insert fsrc;
        Qualification__c qual = USI_testDataFactory.createQualification(anz.Id, foe.Id);
        insert qual;
        Qualification_Unit__c qualu = USI_testDataFactory.createQualificationUnit(qual.Id, unt.Id);
        insert qualu;
        Purchasing_Contracts__c pcon = USI_testDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id); 
        insert pcon;
        Contract_Line_Items__c cli = USI_testDataFactory.createContractLineItem(pcon.id);
        insert cli;
        Line_Item_Qualifications__c liq = USI_testDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
        insert liq;
        Enrolment__c enrl = USI_testDataFactory.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
        insert enrl;
        */
       
    
        Country__c ctry = new Country__c();
        ctry.Country_Code__c = '1123';
        ctry.Name = 'Country Name';
        
        insert ctry;
        
        State__c state = new State__c();
        state.Name = 'Queensland';
        state.Country__c = ctry.Id;
        state.Short_Name__c = 'QLD';
        state.Code__c = '03';
        
        insert state;
    
        ANZSCO__c anz = new ANZSCO__c();
        anz.Name = 'Test ANZSCO';
        
        insert anz;
        
        Id rtypeunit = [select Id from RecordType where DeveloperName = 'Unit_of_Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c unit = new Unit__c();
        unit.Name = 'Test Unit';
        unit.RecordTypeId = rtypeunit;
        unit.Include_In_CCQI_Export__c = true;
        
        insert unit;
        
        Unit_Hours_and_Points__c  uhp = new Unit_Hours_and_Points__c();
        uhp.Unit__c = unit.Id;
        uhp.State__c = state.Id;
        uhp.Nominal_Hours__c = 1.5;
        uhp.Points__c = 1;
        
        insert uhp;
    
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account student = new Account();
        student.RecordTypeId = stud;
        student.LastName = 'Test student Account';
        student.FirstName = 'First Name';
        student.PersonMailingCountry = 'AUSTRALIA';
        student.PersonMailingPostalCode = '4005';
        student.PersonMailingState = 'Queensland';
        student.PersonMobilePhone = '61847292111';
        student.Student_Identifer__c = 'H89373';
        student.PersonBirthDate = system.today() - 5000;
        student.Year_Highest_Education_Completed__c = '2000';
        student.Proficiency_in_Spoken_English__c = '1 - Very well';
        student.Sex__c = 'F - Female';
        student.Salutation = 'Mr.';
        student.Learner_Unique_Identifier__c = '12ab34cd56';
        student.Client_Occupation_Identifier__c = '1 - Manager';
        
        insert student;
    
        Id rtypetorg = [select Id from RecordType where DeveloperName = 'Training_Organisation' And SObjectType = 'Account' LIMIT 1].Id;
        Account torgact = new Account();
        torgact.RecordTypeId = rtypetorg;
        torgact.Name = 'training';
        torgact.BillingCountry = 'NEW ZEALAND';
        torgact.Training_Org_Identifier__c = 'test';
        torgact.Training_Org_Type__c = 'test';
        torgact.Client_Occupation_Identifier__c = '1 - Manager';
        
        insert torgact;
    
        Field_of_Education__c foe = new Field_of_Education__c();
        Id rtypefoe = [select Id from RecordType where DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        foe.RecordTypeId = rtypefoe;
        foe.Name = 'Test Field of Education';
        
        insert foe;
    
    
        Id rtyperes = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c res = new Results__c();
        res.RecordTypeId = rtyperes;
        res.Name = 'test results';
        res.State__c = state.Id; 
        
        insert res;
    
        Location_Loadings__c lloa = new Location_Loadings__c();
        lloa.Name = 'loading name';
        lloa.State__c = state.Id;
        lloa.Price_Factor__c = 50;
        
        insert lloa;
    
        Id rtypepl = [select id from RecordType where DeveloperName='Parent_Location' And SObjectType = 'Locations__c'].Id;
        Locations__c pl = new Locations__c();
        pl.Name = 'name';
        pl.Country__c = ctry.Id;
        pl.State_Lookup__c = state.Id;
        pl.Location_Loading__c = lloa.Id;
        pl.RecordTypeId = rtypepl;
        
        insert pl;
    
        Id rtypeloc = [select id from RecordType where DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c loc = new Locations__c();
        loc.RecordTypeId = rtypeloc;
        loc.Name = 'Sample Room';
        loc.Country__c = ctry.Id;
        loc.Parent_Location__c = pl.Id;
        loc.Training_Organisation__c = torgact.Id;
        
        insert loc;
    
        RecordType rtypefsrc = [Select Id,Name from RecordType where DeveloperName = 'State_Funding_Source' and sObjectType = 'Funding_Source__c'];
        Funding_Source__c fsrc = new Funding_Source__c();
        fsrc.RecordTypeId = rtypefsrc.Id;
        fsrc.Fund_Source_Name__c = 'test Source';
        fsrc.Code__c = '2334';
        fsrc.State__c = state.Id;
        
        insert fsrc;
    
        Id rtypequal = [select Id from RecordType where DeveloperName = 'Qualification'  And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = rtypequal;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anz.Id;
        qual.Field_of_Education__c = foe.Id;
        qual.Recognition_Status__c = '14 - Other Courses';     
        
        insert qual;
        
        Id rtypepcon = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        pCon.Contract_Name__c = 'test Contract';
        pCon.Contract_Type__c = 'Apprenticeship Funding';
        pCon.RecordTypeId = rtypepcon;
        pCon.State_Fund_Source__c = fsrc.Id;
        pCon.Contract_Code__c = '1234567890';
        pCon.Training_Organisation__c = torgact.Id;
        pCon.Contract_State_Lookup__c = state.Id;
        
        insert pCon;
    
        Contract_Line_Items__c cli = new Contract_Line_Items__c();
        cli.Line_Item_Number__c = '001';
        cli.Purchasing_Contract__c = pCon.Id;
        
        insert cli;
    
        Line_Item_Qualifications__c  liq = new Line_Item_Qualifications__c();
        liq.Qualification__c = qual.Id;
        liq.Training_Catalogue_Item_No__c = '0001';
        liq.Contract_Line_Item__c = cli.Id;
        liq.State_Fund_Source__c = fsrc.Id;
        
        insert liq;
        
        Qualification_Unit__c qu = new Qualification_Unit__c();
        qu.Qualification__c = qual.Id;
        qu.Unit__c = unit.Id;
        qu.Unit_Type__c = 'Core';
        
        insert qu;
    
        Line_Item_Qualification_Units__c  liqu = new Line_Item_Qualification_Units__c();
        liqu.Qualification_Unit__c = qu.Id;
        liqu.Line_Item_Qualification__c = liq.Id;
        
        insert liqu;
    
        Enrolment__c enrl = new Enrolment__c();
        enrl.Student__c = student.Id;
        enrl.Qualification__c = qual.Id;
        enrl.Line_Item_Qualification__c = liq.Id;
        enrl.Enrolment_Status__c = 'Completed';
        enrl.Start_Date__c = date.today()+5;
        enrl.End_Date__c = date.today()+10;
        enrl.Delivery_Location__c = loc.Id;
        enrl.Overseas_Country__c = ctry.Id;
        enrl.Study_Reason__c = '01 - To get a job';
        enrl.Victorian_Commencement_Date__c = enrl.start_date__c+5;
        
        insert enrl;
    
        USI_Integration_Log__c usiLog = new USI_Integration_Log__c();
        
        insert usiLog;
        
        USI_Identification_Document__c doc = new USI_Identification_Document__c();
        
        doc.Account__c = student.Id;
        doc.Document_Number_Encrypted__c = 'X111111';
        doc.Registration_State__c = 'QLD';
        doc.Registration_Date__c = date.today();
        doc.Registration_Number_Encrypted__c = 'X111111';
        doc.Registration_Year__c = string.valueOf(date.today().year());
        
        insert doc;
        

        usiCreateResponse ucr = new usiCreateResponse();
        ucr.USI = 'X123Y456ZV';
        ucr.message = 'Success'; 
        ucr.status = 'Success';
        
        
        String mockBody = JSON.serialize(ucr);
        USI_MockWebService fakeResponse = new USI_MockWebService(200, 'Complete', mockBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        test.startTest();
        
        //USI_CreateService.createUSI(student.Id, usiLog.Id);
        USI_CreateService.singleCreateUsi(student.Id);
        
        test.stopTest();
        
        
        List<Account> checkAccounts = [Select Id, Unique_Student_Identifier__c FROM Account WHERE ID = :student.Id];
        
        system.debug(checkAccounts[0]);
        
        List<USI_Integration_Event__c > events = [ Select Id, Event_Status__c, Message__c, JSON__c, Received_JSON__c FROM USI_Integration_Event__c  ];
        
        system.debug(events[0]);
        
        Enrolment__c enrl2 = [ SELECT Delivery_Location__r.Training_Organisation__r.Name, Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c,Delivery_Location__r.Training_Organisation__r.ABN__c 
                              FROM Enrolment__c WHERE Student__c = :student.Id ORDER BY CreatedDate LIMIT 1 ];
        system.debug(enrl2);
        
        USI_Identification_Document__c document = [ SELECT Id, Acquisition_Date__c, Card_Colour__c, Certificate_Number_Encrypted__c, Country_Of_Issue__c, 
                                                   Date_Printed__c,Document_Number_Encrypted__c, Expiry_Date_Encrypted__c, Immi_Card_Number_Encrypted__c,
                                                   Individual_Reference_Number_Encrypted__c, Licence_Number_Encrypted__c, Medicare_Card_Number_Encrypted__c,
                                                   Name_Line_1_Encrypted__c, Name_Line_2_Encrypted__c, Name_Line_3_Encrypted__c, Name_Line_4_Encrypted__c,
                                                   Passport_Number_Encrypted__c, Registration_Date__c, Registration_Number_Encrypted__c, Registration_State__c,
                                                   Registration_Year__c, State__c, Stock_Number_Encrypted__c, Account__c, Account__r.FirstName, RecordType.Name,
                                                   Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.Sex_Identifier__c,
                                                   Account__r.Country_of_Birth__r.Name,Account__r.Country_Studying_In__r.Name, Account__r.Preferred_Contact_Method__c,
                                                   Account__r.Address_Country__r.Name, Account__r.Address_building_property_name__c, Account__r.Address_street_number__c,
                                                   Account__r.Address_street_name__c, Account__r.Address_Post_Code__c, Account__r.Suburb_locality_or_town__c,
                                                   Account__r.Reporting_Billing_State__r.Name, Account__r.Town_City_Of_Birth__c, Account__r.PersonHomePhone, 
                                                   Account__r.PersonMobilePhone
                                                   FROM USI_Identification_Document__c WHERE Account__c =: student.Id ];
        system.debug(document);
        
        for(Account stu : checkAccounts) {
            //system.assert(stu.Unique_Student_Identifier__c == 'X123Y456ZV');
        }
        
        
        
    }
    
    public static testMethod void testVerifyUsiCallout() {
        
         
        
        Country__c ctry = new Country__c();
        ctry.Country_Code__c = '1123';
        ctry.Name = 'Country Name';
        
        insert ctry;
        
        State__c state = new State__c();
        state.Name = 'Queensland';
        state.Country__c = ctry.Id;
        state.Short_Name__c = 'QLD';
        state.Code__c = '03';
        
        insert state;
    
        ANZSCO__c anz = new ANZSCO__c();
        anz.Name = 'Test ANZSCO';
        
        insert anz;
        
        Id rtypeunit = [select Id from RecordType where DeveloperName = 'Unit_of_Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c unit = new Unit__c();
        unit.Name = 'Test Unit';
        unit.RecordTypeId = rtypeunit;
        unit.Include_In_CCQI_Export__c = true;
        
        insert unit;
        
        Unit_Hours_and_Points__c  uhp = new Unit_Hours_and_Points__c();
        uhp.Unit__c = unit.Id;
        uhp.State__c = state.Id;
        uhp.Nominal_Hours__c = 1.5;
        uhp.Points__c = 1;
        
        insert uhp;
    
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account student = new Account();
        student.RecordTypeId = stud;
        student.LastName = 'Test student Account';
        student.FirstName = 'First Name';
        student.PersonMailingCountry = 'AUSTRALIA';
        student.PersonMailingPostalCode = '4005';
        student.PersonMailingState = 'Queensland';
        student.PersonMobilePhone = '61847292111';
        student.Student_Identifer__c = 'H89373';
        student.PersonBirthDate = system.today() - 5000;
        student.Year_Highest_Education_Completed__c = '2000';
        student.Proficiency_in_Spoken_English__c = '1 - Very well';
        student.Sex__c = 'F - Female';
        student.Salutation = 'Mr.';
        student.Unique_student_identifier__c = 'X123Y456ZV';
        student.Learner_Unique_Identifier__c = '12ab34cd56';
        student.Client_Occupation_Identifier__c = '1 - Manager';
        
        insert student;
    
        Id rtypetorg = [select Id from RecordType where DeveloperName = 'Training_Organisation' And SObjectType = 'Account' LIMIT 1].Id;
        Account torgact = new Account();
        torgact.RecordTypeId = rtypetorg;
        torgact.Name = 'training';
        torgact.BillingCountry = 'NEW ZEALAND';
        torgact.Training_Org_Identifier__c = 'test';
        torgact.Training_Org_Type__c = 'test';
        torgact.Client_Occupation_Identifier__c = '1 - Manager';
        
        insert torgact;
    
        Field_of_Education__c foe = new Field_of_Education__c();
        Id rtypefoe = [select Id from RecordType where DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        foe.RecordTypeId = rtypefoe;
        foe.Name = 'Test Field of Education';
        
        insert foe;
    
    
        Id rtyperes = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c res = new Results__c();
        res.RecordTypeId = rtyperes;
        res.Name = 'test results';
        res.State__c = state.Id; 
        
        insert res;
    
        Location_Loadings__c lloa = new Location_Loadings__c();
        lloa.Name = 'loading name';
        lloa.State__c = state.Id;
        lloa.Price_Factor__c = 50;
        
        insert lloa;
    
        Id rtypepl = [select id from RecordType where DeveloperName='Parent_Location' And SObjectType = 'Locations__c'].Id;
        Locations__c pl = new Locations__c();
        pl.Name = 'name';
        pl.Country__c = ctry.Id;
        pl.State_Lookup__c = state.Id;
        pl.Location_Loading__c = lloa.Id;
        pl.RecordTypeId = rtypepl;
        
        insert pl;
    
        Id rtypeloc = [select id from RecordType where DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c loc = new Locations__c();
        loc.RecordTypeId = rtypeloc;
        loc.Name = 'Sample Room';
        loc.Country__c = ctry.Id;
        loc.Parent_Location__c = pl.Id;
        loc.Training_Organisation__c = torgact.Id;
        
        insert loc;
    
        RecordType rtypefsrc = [Select Id,Name from RecordType where DeveloperName = 'State_Funding_Source' and sObjectType = 'Funding_Source__c'];
        Funding_Source__c fsrc = new Funding_Source__c();
        fsrc.RecordTypeId = rtypefsrc.Id;
        fsrc.Fund_Source_Name__c = 'test Source';
        fsrc.Code__c = '2334';
        fsrc.State__c = state.Id;
        
        insert fsrc;
    
        Id rtypequal = [select Id from RecordType where DeveloperName = 'Qualification'  And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = rtypequal;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anz.Id;
        qual.Field_of_Education__c = foe.Id;
        qual.Recognition_Status__c = '14 - Other Courses';     
        
        insert qual;
        
        Id rtypepcon = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        pCon.Contract_Name__c = 'test Contract';
        pCon.Contract_Type__c = 'Apprenticeship Funding';
        pCon.RecordTypeId = rtypepcon;
        pCon.State_Fund_Source__c = fsrc.Id;
        pCon.Contract_Code__c = '1234567890';
        pCon.Training_Organisation__c = torgact.Id;
        pCon.Contract_State_Lookup__c = state.Id;
        
        insert pCon;
    
        Contract_Line_Items__c cli = new Contract_Line_Items__c();
        cli.Line_Item_Number__c = '001';
        cli.Purchasing_Contract__c = pCon.Id;
        
        insert cli;
    
        Line_Item_Qualifications__c  liq = new Line_Item_Qualifications__c();
        liq.Qualification__c = qual.Id;
        liq.Training_Catalogue_Item_No__c = '0001';
        liq.Contract_Line_Item__c = cli.Id;
        liq.State_Fund_Source__c = fsrc.Id;
        
        insert liq;
        
        Qualification_Unit__c qu = new Qualification_Unit__c();
        qu.Qualification__c = qual.Id;
        qu.Unit__c = unit.Id;
        qu.Unit_Type__c = 'Core';
        
        insert qu;
    
        Line_Item_Qualification_Units__c  liqu = new Line_Item_Qualification_Units__c();
        liqu.Qualification_Unit__c = qu.Id;
        liqu.Line_Item_Qualification__c = liq.Id;
        
        insert liqu;
    
        Enrolment__c enrl = new Enrolment__c();
        enrl.Student__c = student.Id;
        enrl.Qualification__c = qual.Id;
        enrl.Line_Item_Qualification__c = liq.Id;
        enrl.Enrolment_Status__c = 'Completed';
        enrl.Start_Date__c = date.today()+5;
        enrl.End_Date__c = date.today()+10;
        enrl.Delivery_Location__c = loc.Id;
        enrl.Overseas_Country__c = ctry.Id;
        enrl.Study_Reason__c = '01 - To get a job';
        enrl.Victorian_Commencement_Date__c = enrl.start_date__c+5;
        
        insert enrl;
    
        USI_Integration_Log__c usiLog = new USI_Integration_Log__c();
        
        insert usiLog;
        

        
    
        usiVerifyResponse uvr = new usiVerifyResponse();
        uvr.dateofbirth = String.valueOf(student.PersonBirthdate);
        uvr.firstname = student.FirstName;
        uvr.usi = 'X123Y456ZV';
        uvr.message = 'Success';
        uvr.lastname = student.LastName;

        
        String mockBody = JSON.serialize(uvr);
        USI_MockWebService fakeResponse = new USI_MockWebService(200, 'Complete', mockBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        test.startTest();
        
        USI_CreateService.verifyUSI(student.Id, usiLog.Id);
        
        test.stopTest();
        
        List<Account> checkAccounts = [Select Id, Unique_Student_Identifier__c FROM Account WHERE ID = :student.Id];
        
        
        for(Account stu : checkAccounts) {
            //system.assert(stu.Unique_Student_Identifier__c == 'X123Y456ZV');
        }
        
        
        
    }
    
    
}