@isTest
public class NATConstructor_Test {
    
    //No custom logic tests

    @isTest static void testStringConstructor() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('Name', 10, false, 'string') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, Name FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
        @isTest static void testNumberConstructor() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('Year_Highest_Education_Completed__c', 2, false, 'number') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, Year_Highest_Education_Completed__c FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
    @isTest static void testDateConstructor() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('PersonBirthDate', 2, false, 'date') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, PersonBirthDate FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
    @isTest static void testCheckboxConstructor() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('PersonDoNotCall', 2, false, 'boolean') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        stu1.PersonDoNotCall = true;
        update stu1;
        
        Account stu2 = NATTestDataFactory.createStudent();
        stu2.PersonDoNotCall = false;
        update stu2;
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, PersonDoNotCall FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
	system.assertEquals(' YN', output);
        
    }
    
   @isTest static void testStringConstructorNull() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('NAT_Error_Code__c', 10, false, 'string') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, NAT_Error_Code__c FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
        @isTest static void testNumberConstructorNull() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('Total_Outstanding_Complaint__c', 2, false, 'number') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, Total_Outstanding_Complaint__c FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
    @isTest static void testDateConstructorNull() {
        
        Map<Integer, NATDataElement> nde = new Map<Integer, NATDataElement>();
        
        nde.put(1, new NATDataElement('Visa_Expiry_Date__c', 2, false, 'date') );
 
    	Account stu1 = NATTestDataFactory.createStudent();
        Account stu2 = NATTestDataFactory.createStudent();
        
        String state = 'Queensland';
        
        String output = ' ';
        
        for(Account a : [SELECT ID, Visa_Expiry_Date__c FROM Account]) {
            Sobject s = a;
        	output += NATConstructor.natFile(s, nde, state);
        }
        
        system.debug(output);
    }
    
}