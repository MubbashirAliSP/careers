global class ExportAllEnrolment implements Database.Batchable<sObject>, Database.Stateful{

    global final String query;
    global final String display;
    global final Boolean flagExported;
    global final HEIMS_Submissions__c SubmissionRecord;
    global final Claim_Number__c dummyClaimNumber;
    global final Account trainingOrg;
    global final Set<Id> relatedEnrolmentIds;
    global final Date dtEnd1;
    global final Date dtStart1;
    public Id libraryId;
    global String body;
    
    global ExportAllEnrolment(String display, Boolean flagExported, HEIMS_Submissions__c SubmissionRecord, Claim_Number__c dummyClaimNumber, Account trainingOrg,Set<Id> relatedEnrolmentIds, String query, Date dtEnd1, Date dtStart1){        
        this.display=display;
        this.flagExported=flagExported;
        this.SubmissionRecord=SubmissionRecord;
        this.dummyClaimNumber=dummyClaimNumber;
        this.trainingOrg=trainingOrg;
        this.relatedEnrolmentIds=relatedEnrolmentIds;
        this.query=query;
        this.dtEnd1=dtEnd1;
        this.dtStart1=dtStart1;
    }
    global ExportAllEnrolment(String query){
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        body = '';
        system.debug('##start');
        system.debug('##query: '+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Enrolment__c> scope){        
        system.debug('##execute');
        
        //Id libraryId;
            
        libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'HEIMS' LIMIT 1].Id;
        List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId AND (Title = 'VDU' OR Title = 'VEN')];
        
        //Database.DeleteResult[] updateResult;
        if(existingContent.size() > 0){
            delete existingContent;
        }
        
        /*if(existingContent.size() > 0){
            system.debug('%%existingContent: '+existingContent);
            updateResult = Database.delete(existingContent, false);
            //delete existingContent;
        }*/
        
        Enrolment__c[] finalE = new list<Enrolment__c>();
                
        for(sObject s : scope){
            finalE.add((Enrolment__c)s);
        }
        
        if(display == 'VDU'){
            body += HEIMSGenerator.generateVDUFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
            //HEIMSGenerator.generateVDUFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
        }
        
        if(display == 'VLL/VEN') {
            body += HEIMSGenerator.generateVENFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
            //HEIMSGenerator.generateVENFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
        }
        
        if(display == 'VCC/VCU') {
            body += HEIMSGenerator.generateVCCFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
            //HEIMSGenerator.generateVCCFile(finalE, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
        }
        if(!Test.isRunningTest()){
            SubmissionRecord.Census_Start_Date__c = dummyClaimNumber.Report_Start_Date__c;
            SubmissionRecord.Census_End_Date__c = dummyClaimNumber.Report_End_Date__c;
            SubmissionRecord.Flag_as_Exported__c = flagExported;
            SubmissionRecord.Type_of_Export__c = Display;
            SubmissionRecord.Provider_Code__c = trainingOrg.HEIMS_Provider_Number__c;
        
            update SubmissionRecord;
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        //Write All contents in 1 file
        //Set File Name Values
        System.debug('**FINISH ExportAllEnrolment**');
        Id libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'HEIMS' LIMIT 1].Id;
        date myDate = dtEnd1;
        
        if(Test.IsRunningTest()){
            myDate = System.Date.Today();
        }
    
        String reportingYear = String.valueOf(myDate.year());
        system.debug('@@reportingYear: '+reportingYear);
        String period = '';
        Integer month = myDate.month();
        if(month<=6){
            system.debug('@@period: '+1);
            period = '1';
        }else if(month>6){
            system.debug('@@period: '+2);
            period = '2';
        } 
        
        if(!String.IsEmpty(body)){
        
            Blob pBlob = Blob.valueof(body);
            
            if(display == 'VDU'){
                insert new ContentVersion(
                     versionData =  pBlob,
                     Title = '7253VDU'+reportingYear+'.'+period+'00001',
                     PathOnClient = '/7253VDU'+reportingYear+'.'+period+'00001.txt',
                     FirstPublishLocationId = libraryId);
            }
            
            if(display == 'VLL/VEN') {
                insert new ContentVersion(
                     versionData =  pBlob,
                     Title = '7253VEN'+reportingYear+'.'+period+'00001',
                     PathOnClient = '/7253VEN'+reportingYear+'.'+period+'00001.txt',
                     FirstPublishLocationId = libraryId);
            }
            
            if(display == 'VCC/VCU') {
                insert new ContentVersion(
                     versionData =  pBlob,
                     Title = '7253VCC'+reportingYear+'.'+period+'00001',
                     PathOnClient = '/7253VCC'+reportingYear+'.'+period+'00001.txt',
                     FirstPublishLocationId = libraryId);
            }
            
        }
        
        //Send Email
        try {
            String userEmail = UserInfo.getUserEmail();
            String[] toAddresses = new String[] {userEmail};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('Export All Enrolments');
            mail.setPlainTextBody('Export All Enrolment has finished');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception e) {

        }
    }

}