global class ExportAllEUOS implements Database.Batchable<sObject>, Database.Stateful{

    global final String query;
    global final String display;
    global final Boolean flagExported;
    global final Boolean canGeneration;
    global final HEIMS_Submissions__c SubmissionRecord;
    global final Claim_Number__c dummyClaimNumber;
    global final Account trainingOrg;
    global final Date dtStart1;
    global final Date dtEnd1;
    global final Boolean includePreviouslyExported;
    global String body;
    
    global ExportAllEUOS(String display, Boolean flagExported, HEIMS_Submissions__c SubmissionRecord, Claim_Number__c dummyClaimNumber, Account trainingOrg, Date dtStart1, Date dtEnd1, Boolean includePreviouslyExported, String query, Boolean isCangeneration){      
        this.display=display;
        this.canGeneration = isCangeneration;
        this.flagExported=flagExported;
        this.SubmissionRecord=SubmissionRecord;
        this.dummyClaimNumber=dummyClaimNumber;
        this.trainingOrg=trainingOrg;
        this.dtStart1=dtStart1;
        this.dtEnd1=dtEnd1;
        this.includePreviouslyExported=includePreviouslyExported;
        this.query=query;
    }
    
    global ExportAllEUOS(String query){
        this.query=query;
        this.canGeneration = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        body = '';
        system.debug('##start');
        system.debug('##query: '+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Enrolment_Unit_of_Study__c> scope){
        system.debug('##execute');
        
        List<Census__c>censusList = new List<Census__c>();
        Set<Id> censusIDs = new Set<Id>();
        
        Id libraryId;
            
        libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'HEIMS' LIMIT 1].Id;
        List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId AND (Title = 'VLL' OR Title = 'VCU')];
        
        //Database.DeleteResult[] updateResult;
        if(existingContent.size() > 0){
            delete existingContent;
        }
        
        /*if(existingContent.size() > 0){
            system.debug('%%existingContent: '+existingContent);
            updateResult = Database.delete(existingContent, false);
            //delete existingContent;
        }*/
        
        //List<Enrolment_Unit_of_Study__c> finalEUOS = new List<Enrolment_Unit_of_Study__c>();
        //List<Enrolment_Unit_of_Study__c> finalEUOS2 = new List<Enrolment_Unit_of_Study__c>();
        
        Enrolment_Unit_of_Study__c[] finalEUOS2 = new list<Enrolment_Unit_of_Study__c>();
        Enrolment_Unit_of_Study__c[] finalEUOS = new list<Enrolment_Unit_of_Study__c>();
        for(sObject s : scope){
            
            Enrolment_Unit_of_Study__c eous = (Enrolment_Unit_of_Study__c)s;           
            
            if(display == 'VLL/VEN'||display == 'VCC/VCU'){
                finalEUOS2.add(eous);
                
                //Store Census ID 
                if(canGeneration)
                	censusIDs.add(eous.CensusId__c);
                
            } else{
                finalEUOS.add(eous);
                //Store Census ID 
                if(canGeneration)
                	censusIDs.add(eous.CensusId__c);
            }
        }
        
        if(display == 'VLL/VEN') {
            body += HEIMSGenerator.generateVLLFile(finalEUOS2, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
            //HEIMSGenerator.generateVLLFile(finalEUOS2, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
        }
        
        if(display == 'VCC/VCU') {
            body += HEIMSGenerator.generateVCUFile(finalEUOS2, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
            //HEIMSGenerator.generateVCUFile(finalEUOS2, flagExported, SubmissionRecord, '', libraryId, dtEnd1);
        }
        if(!Test.isRunningTest()){
            SubmissionRecord.Census_Start_Date__c = dummyClaimNumber.Report_Start_Date__c;
            SubmissionRecord.Census_End_Date__c = dummyClaimNumber.Report_End_Date__c;
            SubmissionRecord.Flag_as_Exported__c = flagExported;
            SubmissionRecord.Type_of_Export__c = Display;
            SubmissionRecord.Provider_Code__c = trainingOrg.HEIMS_Provider_Number__c;
            
        
            update SubmissionRecord;
        }
        
        //Set Can Generated Checkbox on census object
        
        if(censusIDs.size() > 0) {
            
            censusList = [Select IsCANGenerated__c From Census__c where Id in : censusIDs And CensusDate__c < : System.Today()];
            
            for(Census__c c : censusList)
                c.IsCANGenerated__c = true;
            
            update censusList;
            
        }
        
        
    }
    
    global void finish(Database.BatchableContext BC){
        //Write All contents in 1 file
        //Set File Name Values
        System.debug('**FINISH ExportAllEUOS**');
        Id libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'HEIMS' LIMIT 1].Id;
        date myDate = dtEnd1;
        
        if(Test.IsRunningTest()){
            myDate = System.Date.Today();
        }
    
        String reportingYear = String.valueOf(myDate.year());
        system.debug('@@reportingYear: '+reportingYear);
        String period = '';
        Integer month = myDate.month();
        if(month<=6){
            system.debug('@@period: '+1);
            period = '1';
        }else if(month>6){
            system.debug('@@period: '+2);
            period = '2';
        } 
        
        if(!String.IsEmpty(body)){
        
            Blob pBlob = Blob.valueof(body);
            
            if(display == 'VLL/VEN') {
                insert new ContentVersion(
                     versionData =  pBlob,
                     Title = '7253VLL'+reportingYear+'.'+period+'00001',
                     PathOnClient = '/7253VLL'+reportingYear+'.'+period+'00001.txt',
                     FirstPublishLocationId = libraryId);
            }
            
            if(display == 'VCC/VCU') {
                insert new ContentVersion(
                     versionData =  pBlob,
                     Title = '7253VCU'+reportingYear+'.'+period+'00001',
                     PathOnClient = '/7253VCU'+reportingYear+'.'+period+'00001.txt',
                     FirstPublishLocationId = libraryId);
            }
            
        }
        
        //Send Email
        try {
            String userEmail = UserInfo.getUserEmail();
            String[] toAddresses = new String[] {userEmail};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('Export All Enrolment Unit of Study');
            mail.setPlainTextBody('Export All Enrolment Unit of Study has finished');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception e) {

        }
    }

}