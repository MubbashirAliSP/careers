/**
 * @description Test class for `EntryRequirementERNolongerRequiredCX`
 * @author Biao Zhang
 * @date 20.JUN.2016
 */
@isTest
private class ERNolongerRequiredCX_Test {
	static Id llnERRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	static Id formSCRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();

	@testSetup
	static void setup() {
		ApplicationsTestUtilities.initialiseCountryAndStates();

		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        studentLogin.AccountId__c = student.Id;
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		insert enrolment;

		Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
		serviceCase.Enrolment__c = enrolment.Id;
		serviceCase.Account_Student_Name__c = student.Id;
		serviceCase.RecordTypeId = formSCRTID;
		serviceCase.ParentQualificationName__c = 'test qualification';
		insert serviceCase;

		ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;

		EntryRequirement__c requirement = new EntryRequirement__c(
				ApplicationFormId__c = appForm.Id,
				EnrolmentId__c = enrolment.Id,
				ServiceCaseId__c = serviceCase.Id,
				RecordTypeId = llnERRTID,
				Status__c = 'LLN - Pending'
			);
		insert requirement;

	}

	/**
     * @method testRedirect
     * @scenario User click 'ER No Longer Required' button
     * @acceptanceCriteria *
     * @date 20.JUN.2016
     */
	static testMethod void testRedirect() {
		String queryStringER = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		EntryRequirement__c record = Database.query(queryStringER);
		Test.setCurrentPage(Page.EntryRequirementERNoLongerRequired);

		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(record);
            EntryRequirementERNolongerRequiredCX controller = new EntryRequirementERNolongerRequiredCX(stdController);

            PageReference result = controller.evaluateRedirects();
		Test.stopTest();

		List<EntryRequirement__c> requirements = Database.query(queryStringER);
		System.assert(requirements.size() == 1);
		System.assert(requirements[0].Status__c == 'Closed');
		System.assert(requirements[0].Optional__c == true);
	}
}