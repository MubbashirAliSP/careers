global class USI_BatchCollect implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts {

	global String query;   
    global Set<String> studentIds = new Set<String>();
    global Map<Id, Enrolment__c> studentEnrolmentMap = new Map<Id, Enrolment__c>();
    global boolean isCreate;


    global USI_BatchCollect(Boolean create) {
        
        isCreate = create; 
        
        List<Account> students = new List<Account>();
        
        if(isCreate) {
            students = [SELECT Id FROM Account
								  WHERE ((Require_USI_Creation__c = true
                                            AND USI_ID_Document_Submitted__c =true
                                            AND sync_error__c = false)
                                            OR ReSync__c = true)
                                            AND Validated_by_WebService__c = false
                                            AND manually_verified__c = false
                                            and (Recordtype.DeveloperName = 'PersonAccount' AND Recordtype.SObjectType = 'Account')];
        }
        else {
            students = [SELECT Id FROM Account WHERE Unique_Student_Identifier__c != null AND Validated_by_WebService__c = false AND manually_verified__c = false AND (Recordtype.DeveloperName = 'PersonAccount' AND Recordtype.SObjectType = 'Account') LIMIT 1000]; //];
        }
        
        for(Account s : students) {
            studentIds.add(s.Id);   
        }
        
        query = 'SELECT Id, Student__c, Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c, Delivery_Location__r.Training_Organisation__r.Name, Delivery_Location__r.Training_Organisation__r.ABN__c, Student__r.Training_Org_Identifier__c FROM Enrolment__c WHERE  Student__c IN :studentIds';
       

    }

    global database.querylocator start(Database.BatchableContext BC) {
         
        return Database.getQueryLocator(query);
    
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        for(SObject s : scope) {
            Enrolment__c enrl = (Enrolment__c) s;
            
            if( studentIds.contains(enrl.Student__c) ) {
                
                if(enrl.Delivery_Location__r.Training_Organisation__r.Name == 'Careers Australia Education Institute Pty Ltd' || enrl.Delivery_Location__r.Training_Organisation__r.Name == 'Careers Australia Institute Of Training')
                   studentEnrolmentMap.put(enrl.Student__c, enrl);
            }
    	
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        if(isCreate) {
            Database.executeBatch(new USI_BatchCreate( studentEnrolmentMap ), 1 );
        }
        else {
            Database.executeBatch(new USI_BatchVerify( studentEnrolmentMap ), 1 );
        }
        
    }
}