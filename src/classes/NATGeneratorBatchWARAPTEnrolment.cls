global class NATGeneratorBatchWARAPTEnrolment implements Database.Batchable<sObject>,Database.Stateful {

    global String query = '';
    global String body = '';
    private String LogId;
    public String libraryId;
    Map<Id,String>exMap = new Map<Id,String>();
    
    public NATGeneratorBatchWARAPTEnrolment(String q, String LId) {
     
     query = q;
     LogId = LId;
     
      for(Fee_Exemption__c fee : [Select Id, Enrolment__c, Fee_Exemption_Type__r.Code__c From Fee_Exemption__c Order By createdDate]) {
            exMap.put(fee.Enrolment__c,fee.Fee_Exemption_Type__r.Code__c);
      }
    
    }

    
     global database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    
     global void execute(Database.BatchableContext BC,List<SObject> scope) {
     
           set<Id>enrolmentIds = new Set<Id>();
           Set<Id>enrolmentIdsWithAward = new Set<Id>();
     
           for(SObject s :scope) {
          
               Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
               
               enrolmentIds.add(eu.Enrolment__c);
               
           }
     
          List<Awards__c> awardsNonVic = [ SELECT ID, Enrolment__c, Education_Training_International_Flag__c, Year_Program_Completed__c, Qualification_Issued__c, Enrolment__r.Training_Organisation_Identifier__c,
                          Enrolment__r.Student_Identifier__c, Enrolment__r.Start_Date__c, Award_ID_CASIS__c, Date_Issued__c, Enrolment__r.Parent_Qualification_Code__c  FROM Awards__c WHERE RecordType.Name = 'approved award' 
                          AND Award_Type__c = 'Certificate' AND Qualification_Issued__c = 'Y' AND Enrolment__c in :enrolmentIds ];
                          
          //Collect Enrolment Ids that do have award
          
          for(Awards__c a : awardsNonVic)
             enrolmentIdsWithAward.add(a.Enrolment__c);
          
         
          for(SObject s :scope) {
          
               Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
              
               body += eu.Purchasing_Contract_Identifier__c + '\t';
                     
               for(Enrolment_Intake_Unit__c eiu : eu.Enrolment_Intake_Units__r) {
                     if(eiu.Intake_Name__c == null)
                        body += '' + '\t';
                     else
                        body += eiu.Intake_Name__c + '\t';
                        
                }
               
               if(eu.Unit_Code__c == null)
                  body += '' + '\t';
               else   
                  body += eu.Unit_Code__c + '\t';
               
               if(eu.Unit_Name__c == null)
                  body += '' + '\t';
               else     
                  body += eu.Unit_Name__c + '\t';
               
               if(eu.Scheduled_Hours__c == null)
                  body += '' + '\t';
               else   
                  body += eu.Scheduled_Hours__c + '\t';
                  
               if(eu.Enrolment__r.Delivery_Strategy__r.Code__c == null) 
                  body += '' + '\t';
               else 
                  body += eu.Enrolment__r.Delivery_Strategy__r.Code__c + '\t';
               
               if(exMap.containsKey(eu.Enrolment__c)) //this is related list of enrolment(get values by querying on Fee emexption), check avetmiss query
                 body += exMap.get(eu.Enrolment__c) + '\t';                
               else
                  body += '' + '\t';
               
               if(eu.Unit_Result__r.Result_Code__c == null)
                  body += '' + '\t';
               else      
                  body += eu.Unit_Result__r.Result_Code__c + '\t';
                     
               body += formatDate(eu.Start_Date__c) + '\t';
                     
               body += formatDate(eu.End_Date__c) + '\t';
               
               if(eu.Training_Delivery_Location__r.Child_Suburb__c == null)
                  body += '' + '\t';
               else     
                  body += eu.Training_Delivery_Location__r.Child_Suburb__c + '\t';
               
               if(eu.Training_Delivery_Location__r.Child_Postcode__c == null)
                  body += '' + '\t';
               else     
                  body += eu.Training_Delivery_Location__r.Child_Postcode__c + '\t';
               
               if(eu.Enrolment__r.Student__r.TRS_Number__c == null)
                  body += '' + '\t';
               else
                  body += eu.Enrolment__r.Student__r.TRS_Number__c + '\t';   
               
               if(eu.Enrolment__r.Student__r.Student_Identifer__c == null)
                   body += '' + '\t';
               else    
                   body += eu.Enrolment__r.Student__r.Student_Identifer__c + '\t';
                     
               body += eu.Enrolment__r.Student__r.LastName + '\t';
                     
               body += eu.Enrolment__r.Student__r.FirstName + '\t';
               
               if(eu.Enrolment__r.Student__r.PersonBirthDate == null)
                   body += '' + '\t';
               else
                   body += formatDate(eu.Enrolment__r.Student__r.PersonBirthDate) + '\t';
               
               if(enrolmentIdsWithAward.contains(eu.Enrolment__c))
                  body += 'Y' + '\t';
               else     
                  body += 'N' + '\t';
               
               body += Integer.valueOf(eu.Reportable_Tuition_Fee__c) + '\t';
               
               if(eu.Client_Fees_Other__c == null)
                  body += '0' + '\t';
               else
                  body += Integer.valueOf(eu.Client_Fees_Other__c)  + '\t';
               
               body += Integer.valueOf(eu.Total_Amount_Charged__c) + '\t';
               
               if(eu.Enrolment__r.Student__r.Unique_student_identifier__c == null)
                  body += '' + '\t';
               else   
                 body += eu.Enrolment__r.Student__r.Unique_student_identifier__c  + '\t';
                     
               body += '\r\n';
        }
     
     
     }

     global void finish(Database.BatchableContext BC) {
     
         NAT_Validation_Log__c natLog = [ SELECT Id, RAPT_StudentQuery__c,RAPT_EnrolmentQuery__c,Content_Library_Id__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Query_Students_3__c, Query_Students_4__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
          
            if(body == '')
            body = ' ';
         
         Blob pBlob = Blob.valueof(body);
                    
         insert new ContentVersion(
         versionData =  pBlob,
         Title = 'enrolment_' + System.today(),
         PathOnClient = '/enrolment_' + System.today() + '.txt',
         FirstPublishLocationId = natLog.Content_Library_ID__c);
         
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
         mail.setToAddresses(toAddresses);  
         mail.setSubject('NAT Generation is Complete');  
         mail.setPlainTextbody('WA RAPT Generation completed, please click on the URL to check the files. ' + URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/#search');
         mail.setHTMLBody('WA RAPT completed, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/#search' + '">click here</a> to check the files.');
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
     
     
     }
     
     private String formatDate(Date bday) {
        
        Datetime yourDate = bday;
        String dateOutput = yourDate.format('dd/MM/yyyy');
        
        return dateOutput;
    
    }

}