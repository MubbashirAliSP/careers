/**
 * @description Central trigger handler for the `ApplicationFormItemAnswer__c` object
 * @author Ranyel Maliwanag
 * @date 07.AUG.2015
 * @history
 *		 22.FEB.2016	Ranyel Maliwanag	- Added support for mapping of Answers directly to Application Forms
 */
public without sharing class ApplicationFormItemAnswerTriggerHandler {
	/**
	 * @description Before Insert trigger event handler definnition
	 * @author Ranyel Maliwanag
	 * @date 07.AUG.2015
	 */
	public static void onBeforeInsert(List<ApplicationFormItemAnswer__c> records) {
		processEncryptedAnswers(records);
	}


	/**
	 * @description Before Update trigger event handler definnition
	 * @author Ranyel Maliwanag
	 * @date 07.AUG.2015
	 */
	public static void onBeforeUpdate(List<ApplicationFormItemAnswer__c> records, Map<Id, ApplicationFormItemAnswer__c> recordsMap) {
		List<ApplicationFormItemAnswer__c> encryptedAnswers = new List<ApplicationFormItemAnswer__c>();

		for (ApplicationFormItemAnswer__c record : records) {
			if (record.IsEncrypted__c && record.Answer__c != recordsMap.get(record.Id).Answer__c) {
				// Condition: Answer has changed
				encryptedAnswers.add(record);
			}
		}
		processEncryptedAnswers(encryptedAnswers);
	}

	/**
	 * @dsecription After Insert trigger event definition
	 * @author Ranyel Maliwanag
	 * @date 23.FEB.2016
	 */
	public static void onAfterInsert(List<ApplicationFormItemAnswer__c> records) {
		processMapping(records);
	}


	/**
	 * @description After update trigger event definition
	 * @author Ranyel Maliwanag
	 * @date 23.FEB.2016
	 */
	public static void onAfterUpdate(List<ApplicationFormItemAnswer__c> records, Map<Id, ApplicationFormItemAnswer__c> recordsMap) {
		processMapping(records);
	}


	/**
	 * @description Stores the encrypted answer on the encrypted field for those that should be encrypted and stores a masked value on the accessible answer field
	 * @author Ranyel Maliwanag
	 * @date 07.AUG.2015
	 */
	private static void processEncryptedAnswers(List<ApplicationFormItemAnswer__c> answers) {
		for (ApplicationFormItemAnswer__c answer : answers) {
			if (answer.IsEncrypted__c && answer.Answer__c != null) {
				answer.EncryptedAnswer__c = answer.Answer__c;
				answer.Answer__c = EnrolmentsSiteUtility.maskAnswer(answer.Answer__c);
			}
		}
	}


	/**
	 * @description Processing mappings to Application Forms based on Item Answers
	 * @author Ranyel Maliwanag
	 * @date 23.FEB.2016
	 */
	private static void processMapping(List<ApplicationFormItemAnswer__c> records) {
        System.debug('::: processMapping');
		// Get the mapping metadata
		List<ApplicationFormAnswerMapping__mdt> mappingMetas = [SELECT ApplicationFormField__c, ItemIdentifier__c, IsTranslateBooleanValues__c FROM ApplicationFormAnswerMapping__mdt];

		// Mapping of <Application form field> : <Associated meta>
		Map<String, ApplicationFormAnswerMapping__mdt> fieldMetas = new Map<String, ApplicationFormAnswerMapping__mdt>();

		// Mapping of <Item Identifier> : <List of Fields dependant on it>
		Map<String, List<String>> dependencyMap = new Map<String, List<String>>();
		for (ApplicationFormAnswerMapping__mdt meta : mappingMetas) {
			fieldMetas.put(meta.ApplicationFormField__c, meta);

			if (dependencyMap.containsKey(meta.ItemIdentifier__c)) {
				dependencyMap.get(meta.ItemIdentifier__c).add(meta.ApplicationFormField__c);
			} else {
				dependencyMap.put(meta.ItemIdentifier__c, new List<String>{meta.ApplicationFormField__c});
			}
		}

		// Mapping of <Item record Id> : <Item Identifier>
		Map<Id, String> itemIdentifierMap = new Map<Id, String>();

		// Mapping of <Form Id> : <Application Form>
		// For avoiding duplicates
		Map<Id, ApplicationForm__c> formMap = new Map<Id, ApplicationForm__c>();

		// Go over answer record
		for (ApplicationFormItemAnswer__c record : records) {
            System.debug('Identifier ::: ' + record.ItemIdentifier__c);
			// Check if item is mapped into a field or two
			if (dependencyMap.containsKey(record.ItemIdentifier__c)) {
                System.debug('::: Hit!');
				ApplicationForm__c form = new ApplicationForm__c(
						Id = record.ApplicationFormId__c
					);
				// Get the Application Form that will be updated
				if (formMap.containsKey(record.ApplicationFormId__c)) {
					form = formMap.get(record.ApplicationFormId__c);
				}

				// Populate the value for each field mapped
				for (String field : dependencyMap.get(record.ItemIdentifier__c)) {
					// Fetch the meta for more mapping information
					ApplicationFormAnswerMapping__mdt meta = fieldMetas.get(field);
					Object answer = record.Answer__c;

					if (meta.IsTranslateBooleanValues__c) {
						if (String.valueOf(answer) == 'Yes') {
							answer = true;
						} else if (String.valueOf(answer) == 'No') {
							answer = false;
						}
					}
                    System.debug('Answer ::: ' + answer);
					form.put(field, answer);
				}
				formMap.put(form.Id, form);
			}
		}

        System.debug('values ::: ' + formMap.values());

		update formMap.values();
	}
}