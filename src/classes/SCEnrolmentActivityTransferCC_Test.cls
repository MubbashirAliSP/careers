/**
 * @description Test class for `ServiceCaseEnrolmentActivityTransfer_CC` class
 * @author Ranyel Maliwanag
 * @date 18.SEP.2015
 */
@isTest
public with sharing class SCEnrolmentActivityTransferCC_Test {
    @testSetup
    static void controllerSetup() {
        Account student = SASTestUtilities.createStudentAccount();
        insert student;

        Account student2 = SASTestUtilities.createStudentAccount();
        student2.Student_Identifer__c = 'CX1231';
        insert student2;

        List<Enrolment__c> enrolmentsForInsert = new List<Enrolment__c>{
            SASTestUtilities.createEnrolment(student),
            SASTestUtilities.createEnrolment(student2),
            SASTestUtilities.createEnrolment(student)
        };
        insert enrolmentsForInsert;

        Service_Cases__c serviceCase = new Service_Cases__c(
                Account_Student_Name__c = student.Id,
                Enrolment__c = enrolmentsForInsert[0].Id
            );
        insert serviceCase;

        Task enrolmentTask = new Task(
                WhatId = enrolmentsForInsert[0].Id
            );
        insert enrolmentTask;
    }


    static testMethod void transferTasksNegativeTest() {
        // Query objects
        Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
        List<Enrolment__c> enrolments = [SELECT Id, Name FROM Enrolment__c];

        // Set current page
        Test.setCurrentPage(Page.ServiceCaseEnrolmentActivityTransfer);

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(serviceCase);
            ServiceCaseEnrolmentActivityTransfer_CC controller = new ServiceCaseEnrolmentActivityTransfer_CC(stdController);
            controller.targetEnrolmentNumber = enrolments[1].Name;
            controller.validateEnrolmentNumber();
            controller.transferTasks();
        Test.stopTest();
    }


    static testMethod void transferTasksTest() {
        // Query objects
        Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
        List<Enrolment__c> enrolments = [SELECT Id, Name FROM Enrolment__c];

        // Set current page
        Test.setCurrentPage(Page.ServiceCaseEnrolmentActivityTransfer);

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(serviceCase);
            ServiceCaseEnrolmentActivityTransfer_CC controller = new ServiceCaseEnrolmentActivityTransfer_CC(stdController);
            controller.targetEnrolmentNumber = enrolments[0].Name;
            controller.validateEnrolmentNumber();
            controller.transferTasks();
        Test.stopTest();
    }


    static testMethod void transferTasksNegativeTest2() {
        // Query objects
        Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
        List<Enrolment__c> enrolments = [SELECT Id, Name FROM Enrolment__c];

        // Set current page
        Test.setCurrentPage(Page.ServiceCaseEnrolmentActivityTransfer);

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(serviceCase);
            ServiceCaseEnrolmentActivityTransfer_CC controller = new ServiceCaseEnrolmentActivityTransfer_CC(stdController);
            controller.targetEnrolmentNumber = '12345';
            controller.validateEnrolmentNumber();
            controller.transferTasks();
        Test.stopTest();
    }


    static testMethod void transferTasksNegativeTest3() {
        // Query objects
        Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
        List<Enrolment__c> enrolments = [SELECT Id, Name FROM Enrolment__c];
        List<Task> taskRecords = [SELECT Id FROM Task];
        delete taskRecords;

        // Set current page
        Test.setCurrentPage(Page.ServiceCaseEnrolmentActivityTransfer);

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(serviceCase);
            ServiceCaseEnrolmentActivityTransfer_CC controller = new ServiceCaseEnrolmentActivityTransfer_CC(stdController);
            controller.targetEnrolmentNumber = enrolments[2].Id;
            controller.validateEnrolmentNumber();
            controller.transferTasks();
        Test.stopTest();
    }
}