/**
 */
@isTest
private class ChangePurchasingContractPage1_CC_Test {
    
    public static Purchasing_Contracts__c pc = new Purchasing_Contracts__c();
    public static Purchasing_Contracts__c pc2 = new Purchasing_Contracts__c();
    public static Line_Item_Qualifications__c liq = new Line_Item_Qualifications__c();
    public static Line_Item_Qualifications__c liq2 = new Line_Item_Qualifications__c();
    public static Line_Item_Qualifications__c liq3 = new Line_Item_Qualifications__c();
    public static Enrolment_Unit__c eu = new Enrolment_Unit__c();
    public static Enrolment_Unit__c eu2 = new Enrolment_Unit__c();
    public static Line_Item_Qualification_Units__c liqu = new Line_Item_Qualification_Units__c();
    public static Line_Item_Qualification_Units__c liqu2 = new Line_Item_Qualification_Units__c();
    public static Line_Item_Qualification_Units__c liqu3 = new Line_Item_Qualification_Units__c();
    static testMethod void setupPages() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');

        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Unit__c unit2 = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Contract_Line_Items__c cli = new Contract_Line_Items__c();
        Contract_Line_Items__c cli2 = new Contract_Line_Items__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Qualification_Unit__c qu = new Qualification_Unit__c();
        Qualification_Unit__c qu2 = new Qualification_Unit__c();
        Unit__c un = new Unit__c();
        Results__c res = new Results__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        system.runAs(u){           
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                
                Id recrdType = [select Id from RecordType where DeveloperName = 'Training_Organisation' AND SObjectType = 'Account' LIMIT 1].Id;
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.Name = 'training';
                sacct.BillingCountry = 'NEW ZEALAND';
                sacct.Training_Org_Identifier__c = 'test';
                sacct.Training_Org_Type__c = 'test';
                insert sacct;
                
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                //countryAU = TestCoverageUtilityClass.queryAustralia();
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, sacct.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
                loc = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id, sacct.Id); 
                //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                enrl.Type_of_Attendance__c = attend.Id;
                //enrl.Overseas_Country__c = null;
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                insert enrl;
                
                //pc.RecordTypeId = rt;*/
                
                Country__c c = new Country__c();
                c.Country_Code__c = '4565';
                c.Name = 'Country Name';
                insert c;
                
                State__c newState = new State__c();
                newState.Name = 'Victoria';
                newState.Country__c = c.Id;
                newState.Short_Name__c = 'VIC';
                newState.Code__c = '02';
                insert newState;
                
                Funding_Source__c f = new Funding_Source__c();
                Id fRecType = [select Id from RecordType where Name=:'State Funding Source' and sObjectType = 'Funding_Source__c'].Id;
                f.RecordTypeId = fRecType;
                f.Fund_Source_Name__c = 'test Source';
                f.Code__c = '5642';
                f.State__c = newState.Id;
                insert f;
                
                
                
                Account clientAcc = new Account();
                clientAcc.Name = 'Client';
                
                insert clientAcc;
                
                Id rt = [select Id from RecordType where Name='AHC Funded' and SObjectType ='Purchasing_Contracts__c'].Id;
                pc = new Purchasing_Contracts__c();
                pc.Contract_Type__c = 'SIF';
                pc.Contract_Code__c = 'Code';
                pc.Commencement_Date__c = date.today() + 10;
                pc.Contract_Manager__c = u.Id;
                pc.Contract_Name__c = 'Contact Name';
                pc.Completion_Date__c = date.today();
                pc.RecordTypeId = rt;
                pc.State_Fund_Source__c = f.Id;
                pc.Training_Organisation__c = sacct.Id;
                pc.Active__c = false;
                insert pc;
                pc2 = new Purchasing_Contracts__c();
                pc2.Contract_Type__c = 'SIF';
                pc2.Contract_Code__c = 'Code2';
                pc2.Commencement_Date__c = date.today() + 10;
                pc2.Contract_Manager__c = u.Id;
                pc2.Contract_Name__c = 'Contact Name';
                pc2.Completion_Date__c = date.today();
                pc2.RecordTypeId = rt;
                pc2.State_Fund_Source__c = f.Id;
                pc2.Training_Organisation__c = sacct.Id;
                pc2.Active__c = true;
                insert pc2;
                
                Id urecrdType = [select Id from RecordType where Name = 'Unit of Study' and sObjectType ='Unit__c' LIMIT 1].Id;
                unit2.Name = 'Test Unit 123';
                unit2.RecordTypeId = urecrdType;
                unit2.Parent_UoC__c = unit.Id;
                insert unit2;
                
                un = TestCoverageUtilityClass.createUnit();
                res = TestCoverageUtilityClass.createResult(state.Id);
                cli = TestCoverageUtilityClass.createContractLineItem(pc.Id);
                cli2 = TestCoverageUtilityClass.createContractLineItem(pc2.Id);
                qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, un.Id);
                qu2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
                liq2 = TestCoverageUtilityClass.createLineItemQualification(qual.Id, cli2.Id);   
                liq = TestCoverageUtilityClass.createLineItemQualification(qual.Id, cli.Id);
                liq3 = TestCoverageUtilityClass.createLineItemQualification(qual.Id, cli.Id);   
                liqu = TestCoverageUtilityClass.createLineItemQualificationUnit(liq.Id, qunit.Id);
                liqu2 = TestCoverageUtilityClass.createLineItemQualificationUnit(liq2.Id, qunit.Id);
                liqu3 = TestCoverageUtilityClass.createLineItemQualificationUnit(liq3.Id, qu2.Id);
                
                eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id,liqu.Id);
                //eu.Line_Item_Qualification_Unit__c = liqu.Id;  
                //update eu;
                eu2 = TestCoverageUtilityClass.createEnrolmentUnit(unit2.Id, enrl.Id, res.Id, loc.Id,liqu3.Id);
                //eu.Line_Item_Qualification_Unit__c = liqu.Id;
                //update eu2;
                //liq = TestCoverageUtilityClass.createContractLineItem(p.Id);             
        }
           
    }
    
     static testMethod void testPage1(){
        setupPages();
        test.startTest();
        ChangePurchasingContractPage1_CC cpCC = new ChangePurchasingContractPage1_CC();
        
        Pagereference testPage = Page.ChangePurchasingContractPage1;
        testPage.getParameters().put('contractId', pc.Id);
        Test.setCurrentPage(testPage);
        cpCC = new ChangePurchasingContractPage1_CC();
        cpCC.getContractRecord();
        cpCC.oldliqId = liq.Id;
        cpCC.getOldContract();

        cpCC.codeFilter = 'a';
        cpCC.fsCodeFilter = 'a';
        cpCC.getContractList();
        
        cpCC.codeFilter = '';
        cpCC.fsCodeFilter = '';
        cpCC.getContractList();
        
        cpCC.fsCodeFilter = 'F2 AND F3';
        cpCC.trainingOrg = null;
        cpCC.getContractList();
        
        cpCC.codeFilter = 'a';
        cpCC.fsCodeFilter = '';
        cpCC.trainingOrg = 'a';
        cpCC.getContractList();
        
        cpCC.fsCodeFilter = 'F2 AND F3';
        cpCC.trainingOrg = 'aaa';
        cpCC.getContractList();
        
        cpCC.fsCodeFilter = 'F2';
        cpCC.trainingOrg = 'aaa';
        cpCC.getContractList();
        
        cpCC.getliqList();
        cpCC.getEnrolmentUnits();
        cpCC.fillPage1Wrapper();
        cpCC.nextToPage2();
        try
        {
            cpCC.oldliqId = '';
            cpCC.nextToPage2();
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Please select a Qualification.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
        
        cpCC.cancel();
        
        cpCC.getTrainingOrgPicklist();
        test.stopTest();
     } 
     static testMethod void testPage2(){
        setupPages();
        test.startTest();
        ChangePurchasingContractPage1_CC cpCC = new ChangePurchasingContractPage1_CC();
        
        Pagereference testPage2 = Page.ChangePurchasingContractPage2;
        testPage2.getParameters().put('contractId', pc.Id);
        Test.setCurrentPage(testPage2);
        cpCC = new ChangePurchasingContractPage1_CC();
        cpCC.getContractRecord();
        cpCC.oldliqId = liq.Id;
        cpCC.getOldContract();
        cpCC.getContractList();
        cpCC.getliqList();
        cpCC.getEnrolmentUnits();
        cpcc.page2SelectAll = true;
        cpCC.fillPage2Wrapper();
        cpCC.nextToPage3();
        try
        {
            cpcc.page2SelectAll = false;
            cpCC.fillPage2Wrapper();
            cpCC.nextToPage3();
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Please select Enrolment Unit(s).') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
        cpCC.backToPage1();
        cpCC.cancel();
        
        cpCC.getTrainingOrgPicklist();
        test.stopTest();
     } 
     static testMethod void testPage3(){
        setupPages();
        test.startTest();
        //Map<String,Enrolment_Unit__c> oldMap = new Map<String,Enrolment_Unit__c>();
        //oldMap.put(eu.Id,eu);
        ChangePurchasingContractPage1_CC cpCC = new ChangePurchasingContractPage1_CC();
        
        Pagereference testPage3 = Page.ChangePurchasingContractPage3;
        testPage3.getParameters().put('contractId', pc.Id);
        Test.setCurrentPage(testPage3);
        cpCC = new ChangePurchasingContractPage1_CC();
        cpcc.page2SelectAll = true;
        //List<ChangePurchasingContractPage1_CC.page2Wrapper> wrapperList = new List<ChangePurchasingContractPage1_CC.page2Wrapper>();
        //List<ChangePurchasingContractPage1_CC.page3Wrapper> wrapperList3 = new List<ChangePurchasingContractPage1_CC.page3Wrapper>();
        //ChangePurchasingContractPage1_CC.page2Wrapper wrapper = new ChangePurchasingContractPage1_CC.page2Wrapper(eu,true);
        //ChangePurchasingContractPage1_CC.page3Wrapper wrapper3 = new ChangePurchasingContractPage1_CC.page3Wrapper(liq, 'No');
        //wrapperList3.add(wrapper3);
        //wrapperList.add(wrapper);
        //cpCC.oldConMap = oldMap;
        //cpCC.page2WrapperList = wrapperList; 
        //cpCC.page3WrapperList = wrapperList3;
        cpCC.oldliqId = liq.Id;
        cpCC.getContractRecord();
        cpCC.getOldContract();
        cpCC.codeFilter = 'co';
        cpCC.getContractList();
        cpCC.getliqList();
        cpCC.getEnrolmentUnits();
        try
        {
            //cpCC.euUpdateList.add(eu2);
            cpCC.fillPage2Wrapper();
            cpCC.newLiqId = liq3.Id;
            cpCC.fillPage3Wrapper();
            cpCC.submitChanges();
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('The Qualification you have chosen is invalid.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
        cpCC.fillPage2Wrapper();
        cpCC.newLiqId = liq2.Id;
        cpCC.fillPage3Wrapper();
        cpCC.submitChanges();
        cpCC.getUpdatedEuList();
        cpCC.resultCode = 'AND';
        cpCC.getEnrolmentUnits();
        cpCC.resultCode = 'a';
        cpCC.backToPage2(); 
        cpCC.cancel();
        
        
        test.stopTest();
     } 
     static testMethod void testPage4(){
        setupPages();
        List<Enrolment_Unit__c> euUpdate = new List<Enrolment_Unit__c>();
        euUpdate = [select Id, Enrolment__r.Student__r.Name,End_Date__c, Enrolment__r.Student__r.Student_Identifer__c, Student_Identifier__c, Enrolment__c, Enrolment__r.Name, Name, 
                        Unit_Name__c,Unit__r.Name, Unit_Result__r.Result_Code__c, Purchasing_Contract_Identifier__c, Line_Item_Qualification_Unit__r.Name, Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.Name 
                     FROM Enrolment_Unit__c
                     WHERE Id =: eu.Id];
        system.debug('euUpdate****'+euUpdate);
        test.startTest();
        Map<String,Enrolment_Unit__c> oldMap = new Map<String,Enrolment_Unit__c>();
        oldMap.put(eu.Id,eu);
        ChangePurchasingContractPage1_CC cpCC = new ChangePurchasingContractPage1_CC();
        
        Pagereference testPage4 = Page.ChangePurchasingContractPage4;
        testPage4.getParameters().put('contractId', pc.Id);
        Test.setCurrentPage(testPage4);
        List<ChangePurchasingContractPage1_CC.page2Wrapper> wrapperList = new List<ChangePurchasingContractPage1_CC.page2Wrapper>();
        List<ChangePurchasingContractPage1_CC.page3Wrapper> wrapperList3 = new List<ChangePurchasingContractPage1_CC.page3Wrapper>();
        ChangePurchasingContractPage1_CC.page2Wrapper wrapper = new ChangePurchasingContractPage1_CC.page2Wrapper(eu,true);
        ChangePurchasingContractPage1_CC.page3Wrapper wrapper3 = new ChangePurchasingContractPage1_CC.page3Wrapper(liq, 'No');
        wrapperList3.add(wrapper3);
        wrapperList.add(wrapper);
        cpCC.oldConMap = oldMap;
        system.debug('DEBUG***********'+cpCC.oldConMap.get(eu.Id));
        cpCC.page2WrapperList = wrapperList; 
        cpCC = new ChangePurchasingContractPage1_CC();
        cpCC.getContractRecord();
        cpCC.oldliqId = liq.Id;
        cpCC.newLiqId = liq2.Id;
        cpCC.euUpdateList = euUpdate;
        cpCC.getUpdatedEuList();
        cpCC.getOldContract();
        cpCC.getContractList();
        cpCC.getliqList();
        //cpCC.getEnrolmentUnits();
        cpCC.fillPage4Wrapper();
        cpCC.oldLiquId = liqu.Id;
        cpCC.undoRecId = eu.Id;
        cpCC.undo();
        cpCC.getTrainingOrgPicklist();
        test.stopTest();
     }
}