/**
 * @author         Warjie Malibago
 * @date           April 13, 2015
 * @description    This will allow user to select EUs for Intervention Strategy. Controller for CustomEnrolmentLetter VF page
 *
 * @history        Warjie Malibago  ARIL.13.2015    Created
 *                 Nick Guia        AUG.17.2015     Code optimization/bugfix
*/
public class CustomEnrolmentLetter_CC{
    
    public RecordType recType {get; set;}
    public Enrolment_Letters__c enrLet {get; set;}
    public List<wEUList> euList {get; set;}
    public String urlString {get; set;}
    public Boolean shouldOpenConga {get; set;}

    private String enrolmentId;
    private Enrolment_Letters__c interventionStrategy;

    //Constants for Conga Query/Template Id in Custom Settings
    private static final String CAMPUS_INFO = 'Campus Info';
    private static final String ENROLMENT_LETTERS_INFO = 'Enrolment Letters Information';
    private static final String RTO_INFO = 'RTO Information';
    private static final String STUDENT_INFO = 'Student Information';
    private static final String INTERVENTION_STRATEGY = 'INT - Intervention Strategy';
    private static final String PLACE_HOLDER = 'Placeholder';
    
    public CustomEnrolmentLetter_CC(ApexPages.StandardController controller){
        //get enrolment Id from URL
        enrolmentId = ApexPages.currentPage().getParameters().get('CF00N90000004xMeo_lkid');
        this.recType = [SELECT Name FROM RecordType WHERE Id=: ApexPages.currentPage().getParameters().get('RecordType')];
        this.enrLet = (Enrolment_Letters__c)controller.getRecord();   
    } 
    
    public List<wEUList> getEnrolmentUnits(){
        if(euList == null){
            euList = new List<wEUList>();
            for(Enrolment_Unit__c eu: [SELECT Id, Name, Enrolment__c, Unit__r.Name, Unit_Result__r.Name FROM Enrolment_Unit__c WHERE Enrolment__c =: enrolmentId]){
                euList.add(new wEUList(eu));
            }
        } 
        return euList;        
    } 
    
    public PageReference save(){
        List<Enrolment_Unit__c> selectedEU = new List<Enrolment_Unit__c>();
        
        for(wEUList eu: getEnrolmentUnits()){
            if(eu.selected == true){
                selectedEU.add(eu.eul);
            }
        }
        
        if(selectedEU.isEmpty()){
            shouldOpenConga = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: Please select at least 1 EU.'));
            return null;
        } else if(!selectedEU.isEmpty()){
            
            try {
                //insert intervention strategy
                interventionStrategy = new Enrolment_Letters__c(
                                        Enrolment__c = enrolmentId,
                                        RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType'),
                                        Trainer_Educator__c = this.enrLet.Trainer_Educator__c);
                insert interventionStrategy;

                //create palceholder
                List<Intervention_Letter_Placeholder__c> placeholder = new List<Intervention_Letter_Placeholder__c>();
                for(Enrolment_Unit__c eu: selectedEU){
                    Intervention_Letter_Placeholder__c p = new Intervention_Letter_Placeholder__c(
                                                                Enrolment__c = eu.Enrolment__c,
                                                                Enrolment_Unit__c = eu.Id,
                                                                Enrolment_Letter__c = interventionStrategy.Id);
                    placeholder.add(p);
                }
                insert placeholder;

                //collect placeholder Ids
                Set<Id> placeholderIds = new Map<Id, SObject>(placeholder).keySet();
                
                //requery related fields
                Enrolment_Letters__c enrolmentLetter = [SELECT Id, Enrolment__c, Enrolment__r.Id,Enrolment__r.Name, Name,Employer_Account_ID__c, 
                                        RecordType.Name, Trainer_Educator__c FROM Enrolment_Letters__c WHERE id = :interventionStrategy.id LIMIT 1];
                
                //build URL-START

                urlString = '';
                //server url
                String serverURL = System.URL.getSalesforceBaseUrl().getHost();
                serverURL = serverURL.replace('visual.force','salesforce');
                serverURL = 'https://' + serverURL.substring(2) + '/services/Soap/u/21.0/' + (UserInfo.getOrganizationId() + '').left(15);
                urlString = '&ServerUrl=' + serverURL;
                //enrolment Id
                urlString += '&Id=' + enrolmentId;
                //pdf string
                urlString += '&DefaultPDF=1';
                //query url
                urlString += '&QueryId=[RTO]'+Conga_Query__c.getValues(RTO_INFO).Id__c+'?pv0='+enrolmentLetter.Enrolment__r.Name;
                urlString += '&QueryId=[LettersStudent]'+Conga_Query__c.getValues(STUDENT_INFO).Id__c+'?pv0='+enrolmentLetter.Enrolment__r.Name;
                urlString += '&QueryId=[LettersEnrolment]'+Conga_Query__c.getValues(ENROLMENT_LETTERS_INFO).Id__c+'?pv0='+enrolmentLetter.Id;
                urlString += '&QueryId=[Campus]'+Conga_Query__c.getValues(CAMPUS_INFO).Id__c+'?pv0='+enrolmentLetter.Enrolment__c;
                urlString+='&QueryId=[Placeholder]'+Conga_Query__c.getValues(PLACE_HOLDER).Id__c+'?pv0='+(interventionStrategy.Id + '').left(15)+'~pv1='+(enrolmentId + '').left(15);
                //conga template
                urlString+='&TemplateID='+Conga_Template__c.getValues(INTERVENTION_STRATEGY).Id__c;
                //OFN
                urlString+='&OFN='+enrolmentLetter.Enrolment__r.Name+'+-+'+enrolmentLetter.RecordType.Name;
                urlString+='&DS7=1';
                   
                //ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,'Placeholder created.'));
                shouldOpenConga = true;
            } catch(Exception e){
                shouldOpenConga = false;
                System.debug('An error occurred while saving enrolment letter and generating conga : ' + e);
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An error occurred while trying to save Intervention Strategy. ERROR: ' + e));
            }       
        }
        return null;
    }
    
    public PageReference cancel(){
        String url = '/'+ enrolmentId;
        return new PageReference(url);
    }
    
    public class wEUList{
        public Enrolment_Unit__c eul {get; set;}
        public Boolean selected {get; set;}
        
        public wEUList(Enrolment_Unit__c eu){
            eul = eu;
            selected = false;
        }
    }
}