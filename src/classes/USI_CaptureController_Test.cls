@istest public class USI_CaptureController_Test {
    
    public static USI_CaptureController controller = new USI_CaptureController();
    
    public static Account stu = NATTestDataFactory.createStudent();
    public static Country__c ctry = NATTestDataFactory.createCountry();

    @testSetup
    static void controllerSetup() {
        ApplicationsTestUtilities.initialiseCountryAndStates();
    }
    
    @istest static void updateStudent() {
        
        stu.FirstName = 'John';
        stu.LastName = 'Kennedy';
        stu.PersonBirthdate = date.newInstance(1917, 05, 29);
        update stu;
        
    }
    
    
    @istest static void USI_CaptureControllerTestPositive() {
        
        updateStudent();
        
        controller.rerender();
        
        controller.initTermsPage(); //INIT Student
        controller.lookupStudent(stu.Id); //Lookup Student
        
        system.assert(controller.student.Id != null); //ensure student isnt null
        
        PageReference termsPage = Page.USI_TermsPage;
        termsPage.getParameters().put('data', stu.Id);
        Test.setCurrentPage(termsPage);
        
        controller.verifyFirstName = 'John';
        controller.verifySurname = 'Kennedy';
        controller.verifyDOB = date.newInstance(1917, 05, 29); //verify details
        controller.ackConsent = true;
        controller.agreeTerms = true;

        controller.gotoInitialDetails(); //goto initial details vfp
        
        //system.debug(test)
        
        controller.gotoInitialDetailsVFH(); //goto intiial details vfh vfp
        
        controller.gotoTermsPage();
        
        controller.student.Middle_Name__c = 'F';
        
        controller.getContactMethods();
        
        controller.getCountries();
        
        controller.getGender();
        
        controller.gotoConfirmDetails();
        
        controller.gotoDocumentCapture();
                
        for(SelectOption s : controller.getDocType() ) {
            if(s.getLabel() == 'Passport') {
                controller.DocTypeValue = s.getValue();
            }
        }
        
        controller.getCapturedDocumentState();
        
        controller.capturedDocument.Document_Number_Encrypted__c = 'X111111';
        
        controller.gotoUSIComplete();
      
    }
    
      @istest static void USI_CaptureControllerTestNegativeVerify() {
        
        updateStudent();
   		
       	controller.initTermsPage(); //INIT Student
        controller.lookupStudent(stu.Id); //Lookup Student
        
        system.assert(controller.student.Id != null); //ensure student isnt null
        
        PageReference termsPage = Page.USI_TermsPage;
        termsPage.getParameters().put('data', stu.Id);
        Test.setCurrentPage(termsPage);
        
        controller.verifyFirstName = 'Richard';
        controller.verifySurname = 'Nixon';
        controller.verifyDOB = date.newInstance(1917, 05, 29); //verify details
        controller.ackConsent = false;
        controller.agreeTerms = true;

        controller.gotoInitialDetails(); //goto initial details vfp
          
      }
        
	@istest static void USI_CaptureControllerTestPositiveVFH() {
        
        updateStudent();

        PageReference termsPageVFH = Page.USI_TermsPageVFH;
        termsPageVFH.getParameters().put('data', stu.Id);
        Test.setCurrentPage(termsPageVFH);
        controller.initialTermsVFHLookup();
        
        //system.assert(controller.student.FirstName == 'John');
        
    }
    
    @istest static void USI_CaptureControllerTestNegativeVFH() {
        
        updateStudent();

        PageReference termsPageVFH = Page.USI_TermsPageVFH;
        termsPageVFH.getParameters().put('data', stu.Id);
        Test.setCurrentPage(termsPageVFH);
        
        controller.ackConsent = false;
        controller.agreeTerms = false;
        
        controller.initialTermsVFHLookup();
        
        controller.gotoInitialDetailsVFH();
        
        //system.assert(controller.student.FirstName == 'John');
        
    }
    
    @istest static void USI_CaptureControllerTestPositiveValidate() {
        
        updateStudent();

        PageReference reValidation = Page.USI_ReValidation;
        reValidation.getParameters().put('data', stu.Id);
        Test.setCurrentPage(reValidation);
        
        controller.reValidationLookup();
        
        controller.gotoConfirmReValidation();
        
        controller.getStates();
        
        controller.student.Unique_Student_Identifier__c = 'X1234V9876';
        
        controller.gotoSaveReValidation();
        
        Account valStu = [ Select Unique_Student_Identifier__c FROM Account WHERE Id = :stu.Id ];
            
        system.assert(valStu.Unique_student_identifier__c == 'X1234V9876');
        
                
    }
    
    @istest static void USI_CaptureControllerTestNegativeValidate() {
        
        updateStudent();

        PageReference reValidation = Page.USI_ReValidation;
        reValidation.getParameters().put('data', stu.Id);
        Test.setCurrentPage(reValidation);
        
        controller.reValidationLookup();
        
        controller.gotoConfirmReValidation();
        
        controller.student.Unique_Student_Identifier__c = '1';
        
        controller.gotoSaveReValidation();
        
                
    }
    

}