/*Exports NAT120 */

global class NATGeneratorBatch implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    private String libraryId;
    private String currentstate;
    global String body120;
  	global integer natFileNumber = 0;
    global integer natFileName = 0;
    
    Map<Id,String>exMap;
  
     
    global NATGeneratorBatch(String q, String state) {
        
        query = q;
        currentstate = state;
         
        
        /*Set Correct Library Id */
         if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
             
        exMap = new Map<Id,String>();
         for(Fee_Exemption__c fee : [Select Id, Enrolment__c, Fee_Exemption_Type__r.Code__c From Fee_Exemption__c Order By createdDate])
             exMap.put(fee.Enrolment__c,fee.Fee_Exemption_Type__r.Code__c);
        
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body120 = '';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        //Need to exemption identifier via batch
        
   
        
         for(SObject s :scope) {
             
             Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
        
           if(!eu.Has_Been_Superseded__c) {
            
             body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Delivery_Location_Identifier__c, NAT00120__c.getInstance('01').Length__c,'') +
             NATGeneratorUtility.formatOrPadTxtFile(eu.Student_Identifier__c, NAT00120__c.getInstance('02').Length__c,'') +
             NATGeneratorUtility.formatOrPadTxtFile(eu.Unit_of_Competency_Identifier__c, NAT00120__c.getInstance('03').Length__c,'') +     
             NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Qualification__r.Parent_Qualification_Code__c, NAT00120__c.getInstance('04').Length__c,'') +     
             NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Start_Date__c)), NAT00120__c.getInstance('05').Length__c,'') + 
             NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.End_Date__c)), NAT00120__c.getInstance('06').Length__c,'') +
             NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Delivery_Mode_Identifier__c), NAT00120__c.getInstance('07').Length__c,'') +   
             NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.AVETMISS_National_Outcome__c), NAT00120__c.getInstance('08').Length__c,'') +     
             NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Scheduled_Hours__c)), NAT00120__c.getInstance('09').Length__c) +    
             NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c), NAT00120__c.getInstance('10').Length__c,'') +     
             NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Commencing_Course_Identifier__c), NAT00120__c.getInstance('11').Length__c,'') +     
             NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Training_Contract_Identifier__c, NAT00120__c.getInstance('12').Length__c,'') +     
             NATGeneratorUtility.formatOrPadTxtFile(eu.Client_Identifier_New_Apprenticeships__c, NAT00120__c.getInstance('13').Length__c,'') +      
             NATGeneratorUtility.formatOrPadTxtFile(eu.Study_Reason_Identifier__c, NAT00120__c.getInstance('14').Length__c,'') +      
             NATGeneratorUtility.formatOrPadTxtFile(eu.VET_in_Schools__c, NAT00120__c.getInstance('15').Length__c,'') +
             NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Specific_Program_Identifier__c, NAT00120__c.getInstance('23').Length__c,'') +  
             NATGeneratorUtility.formatOrPadTxtFile(eu.AVETMISS_National_Outcome__c, NAT00120__c.getInstance('16').Length__c,'') +       
             NATGeneratorUtility.formatOrPadTxtFile(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c, NAT00120__c.getInstance('17').Length__c,'');     
             
             if (currentState == 'Victoria' && eu.Victorian_Tuition_Rate__c != null) {
                 body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Victorian_Tuition_Rate__c))), NAT00120__c.getInstance('34').Length__c);
             }    
             
             else {
                 body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Reportable_Tuition_Fee__c))), NAT00120__c.getInstance('18').Length__c);
             }    

             body120 += NATGeneratorUtility.formatOrPadTxtFile(exMap.get(eu.Enrolment__c), NAT00120__c.getInstance('19').Length__c,'');      
             
             //TODO: this needs to be an integer only for victoria
             
                 if( currentState == 'Victoria' && eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c == 'F') {
                     body120 += NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('20').Length__c,'');

                 }
               else {
                  body120 +=  NATGeneratorUtility.formatOrPadTxtFile(eu.Purchasing_Contract_Identifier__c, NAT00120__c.getInstance('20').Length__c,'');     
               }
             
             body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Purchasing_Contract_Schedule_Identifier__c, NAT00120__c.getInstance('21').Length__c,'');
             
             if (currentState == 'Victoria' && eu.AVETMISS_National_Outcome__c != '40') {
                 body120 += '    ';
             }
             
             else {
                 body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Hours_Attended__c)), NAT00120__c.getInstance('22').Length__c);
             }
             
             body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Qualification__r.Associated_Course_Identifier__c, NAT00120__c.getInstance('27').Length__c,'');

             if (currentState == 'Victoria') {
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Enrolment__r.Victorian_Commencement_Date__c)), NAT00120__c.getInstance('28').Length__c,'');
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Eligibility_Exemption__c, NAT00120__c.getInstance('29').Length__c,'');
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.VET_FEE_HELP_Indicator__c, NAT00120__c.getInstance('30').Length__c,'');
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.ANZSIC_Code__c, NAT00120__c.getInstance('31').Length__c,''); 
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Enrolment__r.Start_Date__c)), NAT00120__c.getInstance('32').Length__c,'');
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Name, NAT00120__c.getInstance('33').Length__c,''); 
               /*  
                 if (eu.Client_Fees_Other__c != null) {
                    body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Client_Fees_Other__c))), NAT00120__c.getInstance('34').Length__c); 
                 }
                 else {
                    body120 += NATGeneratorUtility.formatHours(String.valueOf(0), NAT00120__c.getInstance('34').Length__c);
                 }

                  */  
                 if (eu.Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c != null) {
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c, NAT00120__c.getInstance('35').Length__c,''); 
                 }
                 else { 
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Training_Delivery_Location__r.Training_Organisation_ABN__c, NAT00120__c.getInstance('35').Length__c,''); 
                  }
              
              }
           
           body120 += '\r\n';
           
           }
                  
             
             if(eu.Has_Been_Superseded__c && currentState == 'Victoria' ) {
                
                for ( integer i = 1 ; i <= eu.Supersession_Count__c ; i++ ) {
                    
                String supersededEndDate = 'Superseded_' + i + '_EU_End_Date__c';
                String supersededUnitId = 'Superseded_' + i + '_Unit_ID__c';
                String supersededEUId = 'Superseded_' + i + '_EU_ID__c';
                
                    
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Delivery_Location_Identifier__c, NAT00120__c.getInstance('01').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Student_Identifier__c, NAT00120__c.getInstance('02').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.get(supersededUnitId)), NAT00120__c.getInstance('03').Length__c,'') + 
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Qualification__r.Parent_Qualification_Code__c, NAT00120__c.getInstance('04').Length__c,'') +       
                 NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Start_Date__c)), NAT00120__c.getInstance('05').Length__c,'');   
                
                // end date below
                 if (String.valueOf(eu.get(supersededEndDate)) != null) {
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.get(supersededEndDate)).substring(0,10)), NAT00120__c.getInstance('06').Length__c,''); 
                 }
                 else {    
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.End_Date__c)), NAT00120__c.getInstance('06').Length__c,'');
                 }
                  
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Delivery_Mode_Identifier__c), NAT00120__c.getInstance('07').Length__c,'') +   
                 NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.AVETMISS_National_Outcome__c), NAT00120__c.getInstance('08').Length__c,'') +     
                 NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Scheduled_Hours__c)), NAT00120__c.getInstance('09').Length__c) +    
                 NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c), NAT00120__c.getInstance('10').Length__c,'') +     
                 NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.Commencing_Course_Identifier__c), NAT00120__c.getInstance('11').Length__c,'') +     
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Training_Contract_Identifier__c, NAT00120__c.getInstance('12').Length__c,'') +     
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Client_Identifier_New_Apprenticeships__c, NAT00120__c.getInstance('13').Length__c,'') +      
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Study_Reason_Identifier__c, NAT00120__c.getInstance('14').Length__c,'') +      
                 NATGeneratorUtility.formatOrPadTxtFile(eu.VET_in_Schools__c, NAT00120__c.getInstance('15').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Specific_Program_Identifier__c, NAT00120__c.getInstance('23').Length__c,'') +  
                 NATGeneratorUtility.formatOrPadTxtFile(eu.AVETMISS_National_Outcome__c, NAT00120__c.getInstance('16').Length__c,'') +       
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c, NAT00120__c.getInstance('17').Length__c,'');      
                
                if (eu.Victorian_Tuition_Rate__c != null) {
                    body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Victorian_Tuition_Rate__c))), NAT00120__c.getInstance('18').Length__c);
                }    
             
                else {
                    body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Reportable_Tuition_Fee__c))), NAT00120__c.getInstance('18').Length__c);
                 }    
                
                 body120 += NATGeneratorUtility.formatOrPadTxtFile(exMap.get(eu.Enrolment__c), NAT00120__c.getInstance('19').Length__c,'');   
                
                if( currentState == 'Victoria' && eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c == 'F') {
                 	body120 += NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('20').Length__c,'');    
                 }
               	else {
                	body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Purchasing_Contract_Identifier__c, NAT00120__c.getInstance('20').Length__c,'');
               	}                 
                
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Purchasing_Contract_Schedule_Identifier__c, NAT00120__c.getInstance('21').Length__c,'');
                 
                 if (eu.AVETMISS_National_Outcome__c != '40') {
                     body120 += '    ';
                 }
             
                 else {
                     body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(eu.Hours_Attended__c)), NAT00120__c.getInstance('22').Length__c);
                 }
                 

                 body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Qualification__r.Associated_Course_Identifier__c, NAT00120__c.getInstance('27').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Enrolment__r.Victorian_Commencement_Date__c)), NAT00120__c.getInstance('28').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Eligibility_Exemption__c, NAT00120__c.getInstance('29').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.VET_FEE_HELP_Indicator__c, NAT00120__c.getInstance('30').Length__c,'') +
                 NATGeneratorUtility.formatOrPadTxtFile(eu.ANZSIC_Code__c, NAT00120__c.getInstance('31').Length__c,'') +   
                 NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(eu.Start_Date__c)), NAT00120__c.getInstance('32').Length__c,'') +      
                 NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(eu.get(supersededEUId)), NAT00120__c.getInstance('33').Length__c,''); 
              /*                   
                 if (eu.Client_Fees_Other__c != null) {
                    body120 += NATGeneratorUtility.formatHours(String.valueOf(Integer.valueOf(Math.round(eu.Client_Fees_Other__c))), NAT00120__c.getInstance('34').Length__c); 
                 }
                 else {
                     body120 += NATGeneratorUtility.formatOrPadTxtFile('', NAT00120__c.getInstance('34').Length__c); 
                 }
                    */ 
                 if (eu.Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c != null) {
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c, NAT00120__c.getInstance('35').Length__c,''); 
                 }
                 else { 
                    body120 += NATGeneratorUtility.formatOrPadTxtFile(eu.Training_Delivery_Location__r.Training_Organisation_ABN__c, NAT00120__c.getInstance('35').Length__c,''); 
                  }
                  
                 body120 += '\r\n';
                  
                 
                 }
                      
                    
                    
                    
                }
             
             
             }

        natFileNumber++;

        
        if(currentState == 'Queensland') {
            
            Integer remainder = math.mod(natFileNumber, 10);
            
            if(remainder == 0) {   
                natFileName++;
                Blob pBlob = Blob.valueof(body120);
                insert new ContentVersion(
                    versionData =  pBlob,
                    Title = 'NAT00120_' + natFileName,
                    PathOnClient = '/NAT00120_' + natFileName + '.txt',
                    FirstPublishLocationId = libraryId );
                
                body120 = '';
        	}
        }
    }    
      
    global void finish(Database.BatchableContext BC) {
        
        
        if(currentState != 'Queensland') {
        
        Blob pBlob = Blob.valueof(body120);
        
        insert new ContentVersion(
                versionData =  pBlob,
                Title = 'NAT00120',
                PathOnClient = '/NAT00120.txt',
                FirstPublishLocationId = libraryId);
            
        }
        
        if(currentState == 'Queensland') {
            
            if(body120 != '') {
                natFileName++;
                Blob pBlob = Blob.valueof(body120);
        
            	insert new ContentVersion(
                    versionData =  pBlob,
                    Title = 'NAT00120_' + natFileName,
                    PathOnClient = '/NAT00120_' + natFileName + '.txt',
                    FirstPublishLocationId = libraryId);
               
           	}   
            
            String batchFileS = 'TYPE NAT00120_*.txt >> NAT00120.txt' + '\r\n' + 'DEL NAT00120_*.txt' + '\r\n' + 'DEL combine.bat';
            Blob batchFile = Blob.valueof(batchFileS);
            
       	 	insert new ContentVersion(
                versionData =  batchFile,
                Title = 'combine',
                PathOnClient = '/combine.bat',
                FirstPublishLocationId = libraryId);
            
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('NAT Generation is Complete');  
        mail.setPlainTextbody('NAT Generation completed, please click on the URL retrieve your files. https://salesforce.com/sfc/#search');  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        
    }
  

}