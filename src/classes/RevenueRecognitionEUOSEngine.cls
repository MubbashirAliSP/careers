public class RevenueRecognitionEUOSEngine {
    
  /* This method will populate Stage upon creation and update - Neha Batra, 24/03/2015 */
   
    public static boolean runME = true;
    
    public static void populateStage(Set<Id> eUnitSet, List<Enrolment_Unit_of_Study__c> euosList){
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        Map<Id,String> eiuMapStage = new Map<Id,String>(); 
        //query enrolment intake units with the same enrolment unit
        eiuList = [select Id, Enrolment_Unit__c, Intake_Unit__r.Stage__r.Name
                    from Enrolment_Intake_Unit__c 
                    where Enrolment_Unit__c IN: eUnitSet];
        for(Enrolment_Intake_Unit__c e : eiuList){       
            eiuMapStage.put(e.Enrolment_Unit__c,e.Intake_Unit__r.Stage__r.Name);
        }           
        for(Enrolment_Unit_of_Study__c euos : euosList){
            if(eiuMapStage.containsKey(euos.Enrolment_Unit__c) && euos.Stage__c == null)
            {    
                   euos.Stage__c=eiuMapStage.get(euos.Enrolment_Unit__c);
            }
        }
    }
    
    //public static void generateERS(List<Enrolment_Unit_Of_Study__c> inEUOS) {
    @future public static void generateERS(List<Id> inEuosId) {   
        
        date firstOfMonth = date.newInstance(date.today().year(), date.today().month(), 01);

        List<Enrolment_Unit_of_Study__c> inEUOS = [ SELECT Id, Enrolment_Unit__r.Enrolment__c FROM Enrolment_Unit_Of_Study__c WHERE Stage__c != null AND  Enrolment_Unit__r.Enrolment__r.End_Date__c >= :firstOfMonth AND Id IN :inEuosId ];

        system.debug('x' + inEUOS);
        
        Set<Id> inEnrolmentId = new Set<Id>();
        
        List<Enrolment_Revenue_Summary__c> historicUpdate = new List<Enrolment_Revenue_Summary__c>();
        
        Database.SaveResult[] insertList = new Database.SaveResult[]{};
        
        Boolean historicFlag = false;

        for(Enrolment_Unit_Of_Study__c e : inEuos) {
           /* if(!inEnrolmentId.contains(String.valueOf(e.getSobject('Enrolment_Unit__r').get('Enrolment__c')))) {
                
                Date inDate = Date.valueOf( e.getSObject('Enrolment_Unit__r').getSobject('Enrolment__r').get('End_Date__c') );
                
                if(e.Stage__c != null && inDate >= firstOfMonth) {
                    inEnrolmentId.add(String.valueOf(e.getSobject('Enrolment_Unit__r').get('Enrolment__c')));
                } */

            
            if(!inEnrolmentId.contains(e.Enrolment_Unit__r.Enrolment__c)) {
                inEnrolmentId.add(e.Enrolment_Unit__r.Enrolment__c);
            }
        }
        
        String currentERSRecordTypeId = Schema.SObjectType.Enrolment_Revenue_Summary__c.getRecordTypeInfosByName().get('Current Summary').getRecordTypeId();
        String historicalERSRecordTypeId = Schema.SObjectType.Enrolment_Revenue_Summary__c.getRecordTypeInfosByName().get('Historical Summary').getRecordTypeId();

        List<Schema.FieldSetMember> fields = SObjectType.Enrolment_Revenue_Summary__c.FieldSets.Census_Dates.getFields(); 
    
        String existingERSQueryS = 'SELECT Id, Cumulative_Revenue_Recognised__c, Monthly_Revenue_Recognised__c, Historical_Revenue_Recognised__c, Historical_Monthly_Revenue_Recognised__c, Enrolment__r.Auto_Include_In_Revenue_Summary_Job__c,Enrolment__r.Enrolment_Included_In_Rev_Summary_Job__c,Enrolment__r.Revenue_Received_Not_Recognised_Is_Not_0__c,Enrolment__r.Override_Auto_Rev_Summary_Generation__c,Total_Course_Revenue__c,Enrolment_Status__c, Enrolment_End_Date__c, Revenue_Received__c, '; 
        String existingERSQueryE = 'Enrolment__c FROM Enrolment_Revenue_Summary__c WHERE Enrolment__c IN : inEnrolmentId AND RecordTypeId = :currentERSRecordTypeId';
        String stageFields = '';
        
        List<Enrolment_Revenue_Summary__c> ersInsert = new List<Enrolment_Revenue_Summary__c>();
        List<Enrolment_Revenue_Summary__c> ersUpdate = new List<Enrolment_Revenue_Summary__c>();
        List<Enrolment_Revenue_Summary__c> ersHistInsert = new List<Enrolment_Revenue_Summary__c>();
                
        AggregateResult[] groupedResults = [SELECT Enrolment_Unit__r.Enrolment__c, Stage__c, min(Census_date__c) MinDate
                                            FROM Enrolment_Unit_Of_Study__c
                                            WHERE Stage__c != null AND Census_date__c!= null AND Enrolment_Unit__r.Enrolment__c IN : inEnrolmentId
                                            GROUP BY Stage__c, Enrolment_Unit__r.Enrolment__c ];
                            
        for(Schema.FieldSetMember f : fields) {
           stageFields += String.valueOf(f.getFieldPath()) + ','; 
        }
        
        List<Enrolment_Revenue_Summary__c> existingERS = Database.query(existingERSQueryS + stageFields + existingERSQueryE );
          
        Map<Id, Id> ersMap = new Map<Id,Id>();
        
        for(Enrolment_Revenue_Summary__c ersm : existingERS) {
            ersMap.put(ersm.Enrolment__c, ersm.Id);         
        }
                    
 
            
            
            for( Enrolment_Revenue_Summary__c eers : existingERS ) {
                        
                Enrolment_Revenue_Summary__c updateERS = new Enrolment_Revenue_Summary__c();
                   Boolean requireUpdate = false;
                                
                
                for (AggregateResult ar : groupedResults)  {   
                    if( String.valueOf(ar.get('Enrolment__c') ) == eers.Enrolment__c  && existingERS.size() > 0 ) {
                        for(Schema.FieldSetMember f : fields) {                                    
                            String fieldname = String.valueOf(f.getFieldPath()).replace('Census_Date_Stage_', 'Stage ').replace('__c', '');                                    
                            if (fieldname == ar.get('Stage__c') ) { 
                                if( eers.get( f.getFieldPath() ) == null ) {     
                                    updateERS.put(f.getFieldPath(), ar.get('MinDate'));
                                    updateERS.Id = eers.Id;
                                    requireUpdate = true;             
                                }
                                      
                                if( eers.get( f.getFieldPath() ) != null ) {
                                        
                                    date existingDate = date.valueOf( eers.get( f.getFieldPath() ) );                                           
                                    date compareDate = date.valueOf( ar.get('MinDate') );
                                 
                                    if( existingDate != compareDate )  {            
                                        updateERS.put(f.getFieldPath(), ar.get('MinDate'));
                                        updateERS.Id = eers.Id;
                                        requireUpdate = true;              
                                    }
                                }
                            }
                        }
                    }
                }
                
                if( requireUpdate ) {      
                    ersUpdate.add(updateERS);
                }   
            }

            for(Id i : inEnrolmentId) {
                                                                
                Enrolment_Revenue_Summary__c ersNew = new Enrolment_Revenue_Summary__c();
                
                if(!ersMap.containsKey(i)) {
                    ersNew.RecordTypeId = currentERSRecordTypeId;
                    ersNew.Enrolment__c = i;
                                        
                    for (AggregateResult ars : groupedResults)  {
                     if( String.valueOf(ars.get('Enrolment__c') ) == i)
                     {
                        
                            system.debug(ars);

                              for(Schema.FieldSetMember f : fields) {
                                    
                                    String fieldname = String.valueOf(f.getFieldPath()).replace('Census_Date_Stage_', 'Stage ').replace('__c', '');
                                    
                                    if (fieldname == ars.get('Stage__c')) {
                                        
                                        ersNew.put(f.getFieldPath(), ars.get('MinDate') );
                                        
                                    } 
                                }
                            }
                        }
                    ersInsert.add(ersNew);
                        }
                
                }
                
            
        if (ersInsert.size() > 0) {
           insertList = Database.insert(ersInsert, false);
        }
        
        if (ersUpdate.size() > 0) {
            Update ersUpdate;
        }
            
    List<Id> savedERSIds = new List<Id>();
        
    for (Database.SaveResult sr : insertList) {
        
        if (sr.isSuccess()) {
            
            savedERSIds.add(sr.getId());
            
        }
    }
        for (Enrolment_Revenue_Summary__c ersup : ersUpdate) {        
            savedERSIds.add(ersup.Id);
    }
     
        
        List<Enrolment_Revenue_Summary__c> insertHistoric = Database.query(existingERSQueryS + stageFields + ' Enrolment__c FROM Enrolment_Revenue_Summary__c WHERE RecordTypeId = :currentERSRecordTypeId AND Id IN : savedERSIds'  );
       
        if(insertHistoric.size() > 0) {

            For(Enrolment_Revenue_Summary__c ershi : insertHistoric) {
                
                historicUpdate.add(ershi);
                 
            }
        }
                    
        for (Enrolment_Revenue_Summary__c ersh : historicUpdate ) {
                        
            Enrolment_Revenue_Summary__c ersHistoric = new Enrolment_Revenue_Summary__c();

            ersHistoric = ersh.clone(false, true);

            ersHistoric.RecordTypeId = historicalERSRecordTypeId;
            ersHistoric.Current_Revenue_Summary__c = ersh.Id;
            ersHistoric.Archive_Date__c = Date.today();
            ersHistoric.Enrolment__c = ersh.Enrolment__c;       
            ersHistoric.Historical_Revenue_Recognised__c = ersh.Cumulative_Revenue_Recognised__c;
            ersHistoric.Historical_Monthly_Revenue_Recognised__c = ersh.Monthly_Revenue_Recognised__c;
            ersHistoric.Historical_Enrolment_Status__c = ersh.Enrolment_Status__c;
            ersHistoric.Historical_Enrolment_End_Date__c = ersh.Enrolment_End_Date__c;
            ersHistoric.Historical_Total_Course_Revenue__c = ersh.Total_Course_Revenue__c;
            ersHistoric.Historical_Revenue_Received__c = ersh.Revenue_Received__c;
            ersHistoric.Auto_Include_In_Revenue_Summary_Job__c=ersh.Enrolment__r.Auto_Include_In_Revenue_Summary_Job__c;
            ersHistoric.Override_Auto_Rev_Summary_Job_Flag__c=ersh.Enrolment__r.Override_Auto_Rev_Summary_Generation__c;
            ersHistoric.Revenue_Received_Not_Recognised_Is_Not_0__c=ersh.Enrolment__r.Revenue_Received_Not_Recognised_Is_Not_0__c;
            ersHistoric.Enrolment_Included_In_Rev_Summary_Job__c=ersh.Enrolment__r.Enrolment_Included_In_Rev_Summary_Job__c;

            ersHistInsert.add(ersHistoric);

            
        }
            
        if (ersHistInsert.size() > 0) {
            Insert ersHistInsert ;
        }
    }
}