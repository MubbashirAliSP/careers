public class AssessmentAttemptController {
    
    public List<AssessmentAttemptWrapper>students {get;set;}
    
    public AssessmentAttemptController() {
         
         students = new List<AssessmentAttemptWrapper>();
         constructItemList();
        
    }
    
    /*Will build list of items combining intake student data and blank fields */
    private void constructItemList() {
        
        for(Intake_Students__c is : [Select Student__r.Student_Identifer__c, Student__r.Name, Student__c From Intake_Students__c Where Intake__c = : ApexPages.currentPage().getParameters().get('id')]) {
            
            AssessmentAttemptWrapper atw = new AssessmentAttemptWrapper(false,is, new AssessmentAttempt__c());
			students.add(atw);           
        }
        
    }
    
    /*Inserts new Assessment Attempt records */
    public pageReference submit() {
        List<AssessmentAttempt__c>attemptsToInsert = new List<AssessmentAttempt__c>();

        for(AssessmentAttemptWrapper aws : students) {
            
            //We temporary set BB Assessment Recordid to unique student Id, workflow will set from autonumber after
            if(aws.selected) {
                if(aws.assesAttempt.BBCourseId__c == null || aws.assesAttempt.SubmittedDate__c== null || aws.assesAttempt.BBAssessmentId__c == null) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Fill In All Mandatory Fields'));
                    return null;
                }
                
                AssessmentAttempt__c attempt = new AssessmentAttempt__c(BBAssessmentRecordId__c = aws.intakeStudent.Student__c + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'))
,AccountId__c = aws.intakeStudent.Student__c, BBCourseId__c = aws.assesAttempt.BBCourseId__c, SubmittedDate__c = aws.assesAttempt.SubmittedDate__c, BBAssessmentId__c = aws.assesAttempt.BBAssessmentId__c);
                
                attemptsToInsert.add(attempt);
                
            }
            
        }
        
        //In case no records are selected, display error and stay on the pagee
        if(attemptsToInsert.size() == 0) {
            
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select At Least 1 Student'));
            
             return null;
            
        }
        
        try {
               Database.SaveResult[] srList = Database.insert(attemptsToInsert, true);
                
                
                for (Database.SaveResult sr : srList) {
                    
                    if (!sr.isSuccess()) {
                        
                        for(Database.Error err : sr.getErrors()) {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, err.getStatusCode() + ': ' + err.getMessage()));
                        }
                          
                    }
                    
                }
        
        } catch(Exception e) {
            system.debug('@@@@' + e.getMessage());
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); 
            return null; 
        }
        
        return new pageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        
        
    }
    
    //Go Back To Intake
    public pageReference cancel() {
        
        return new pageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        
    }    
  
    /*Wrapper class implements listview for selection and creation of assessment attempts */
    public class AssessmentAttemptWrapper {
        
        public boolean selected {get;set;}       
        public Intake_Students__c intakeStudent {get;private set;}
        public AssessmentAttempt__c assesAttempt {get;set;}
        
        
        public AssessmentAttemptWrapper(Boolean sel, Intake_Students__c is,AssessmentAttempt__c attempt) {
            
            selected = sel;
            intakeStudent = is;
            assesAttempt = attempt;
        }
        
    }

}