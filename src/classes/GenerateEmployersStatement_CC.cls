/**
 * @description Controller for Generate Employer Statement page
 * @author Warjie Malibago
 * @date 30.JAN.2014
 *
 * HISTORY
 * - 30.JAN.2014    Warjie Malibago - Created.
 * - 28.FEB.2014    Warjie Malibago - Updated
 */
public with sharing class GenerateEmployersStatement_CC{
    public Id accountId {get; set;}
    public Account acc {get; set;}
    public Account acc2 {get; set;}
    
    public GenerateEmployersStatement_CC(){
        accountId = ApexPages.currentPage().getParameters().get('aid');
        
        acc = new Account();
        acc2 = new Account();
        acc = [SELECT Id, Start_Date_Conga__c, End_Date_Conga__c FROM Account WHERE Id =: accountId];
    }
    
    public void GenerateStatement(){
        if(acc2.Start_Date_Conga__c == NULL || acc2.End_Date_Conga__c == NULL){
            acc2.Start_Date_Conga__c = Date.TODAY();
            acc2.End_Date_Conga__c = Date.TODAY();
        }
        if(acc2.Start_Date_Conga__c > acc2.End_Date_Conga__c){
            acc2.End_Date_Conga__c = acc2.Start_Date_Conga__c;
        }
        acc.Start_Date_Conga__c = acc2.Start_Date_Conga__c;
        acc.End_Date_Conga__c = acc2.End_Date_Conga__c;
        update acc;
        
        // 02.28.2014 WBM Start      
        List<Transaction_Line_Item__c> trans = new List<Transaction_Line_Item__c>();
        Map<Id, String> students = new Map<Id, String>();
        trans = [SELECT Id, Account_Transaction__c, Student__r.Name FROM Transaction_Line_Item__c WHERE Account_Transaction__r.Payer__c =: accountId];
        for(Transaction_Line_Item__c t: trans){
            students.put(t.Account_Transaction__c, t.Student__r.Name);
            system.debug('**Student map: ' + t.Account_Transaction__c + ' > ' + t.Student__r.Name);
        }
        
        
        List<Account_Transaction__c> act = new List<Account_Transaction__c>();
        act = [SELECT Id, Name, Student__c FROM Account_Transaction__c WHERE Id =: students.keySet()];
        for(Account_Transaction__c a: act){
            a.Student__c = students.get(a.Id);
            system.debug('**New Student: ' + a.Name + ' > ' + a.Student__c);
        }
        if(act.size() > 0){
            update act;
        }
        //WBM End
        system.debug('**Start Date: ' + acc.Start_Date_Conga__c);
        system.debug('**End Date: ' + acc.End_Date_Conga__c);
    }

    public pageReference Cancel(){
        PageReference pageRef;
        if(String.IsEmpty(accountId)){
            pageRef = new PageReference('/home/home.jsp');
        }
        else{
            pageRef = new PageReference('/'+ String.valueOf(accountId).mid(0,15));
        }
        
        pageRef.setRedirect(true);
        return pageRef;
    }
}