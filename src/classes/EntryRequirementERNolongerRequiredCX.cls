/**
 * @description Custom button handler for set Status to 'Closed' and set Optional to 'True' for ER
 * @author Biao Zhang
 * @date 20.JUN.2016
 */
public with sharing class EntryRequirementERNolongerRequiredCX {
	public EntryRequirement__c entryRequirement {get; set;}

	public EntryRequirementERNolongerRequiredCX(ApexPages.StandardController controller) {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		queryString += 'WHERE Id = \'' + controller.getId() + '\'';
		entryRequirement = Database.query(queryString);
	}

	public PageReference evaluateRedirects() {
		EntryRequirement__c er = new EntryRequirement__c(
			id = entryRequirement.Id,
			Optional__c = true,
			Status__c = 'Closed'
		);

		update er;

		PageReference result = new PageReference('/' + entryRequirement.Id);
		result.setRedirect(true);
		return result; 
	}
}