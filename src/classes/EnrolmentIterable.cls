/*
 * @description Enrolment Unit of Study iterable class for HEIMS pagination
 * @author D. Crisologo (CloudSherpas)
 * @date 19.JUN.2013
*/
global class EnrolmentIterable implements Iterator<list<EnrolmentWrapper>>{

   transient List<Enrolment__c> enrolList {get; set;}
   public transient List<EnrolmentWrapper> eWrapperList {get; set;}
   transient List<EnrolmentWrapper> eWrapperListRequested {get; set;}
   Integer i {get; set;}
   private String sQuery;
   private Set<Id> relatedEnrolmentIds;
   private Date dtStart1;
   private Date dtEnd1;
   
   public Integer setPageSize {get; set;}

  /*
     * @description Class constructor for EnrolmentIterable 
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        sQuery - the query string to be invoked in class constructor
     *                relatedEnrolmentIds - enrolment ids found in Enrolment Unit of Studies
     * @returns:      None
    */
   public EnrolmentIterable (String sQuery, Set<Id> relatedEnrolmentIds){
       this.sQuery = sQuery;
       this.relatedEnrolmentIds = relatedEnrolmentIds;
       enrolList = Database.Query(sQuery);
       eWrapperList = new list<EnrolmentWrapper>();
       eWrapperListRequested = new list<EnrolmentWrapper>();  
       for(Enrolment__c e : enrolList) {
            EnrolmentWrapper eWrap = new EnrolmentWrapper(false, e);
            eWrapperList.add(eWrap);
       }
       setPageSize = 100;
       i = 0;
   }

   public EnrolmentIterable (String sQuery, Set<Id> relatedEnrolmentIds, Date dtStart1, Date dtEnd1){
       this.sQuery = sQuery;
       this.relatedEnrolmentIds = relatedEnrolmentIds;
       this.dtStart1 = dtStart1;
       this.dtEnd1 = dtEnd1;
       enrolList = Database.Query(sQuery);
       eWrapperList = new list<EnrolmentWrapper>();
       eWrapperListRequested = new list<EnrolmentWrapper>();  
       for(Enrolment__c e : enrolList) {
            EnrolmentWrapper eWrap = new EnrolmentWrapper(false, e);
            eWrapperList.add(eWrap);
       }
       setPageSize = 100;
       i = 0;
   }  

  /*
     * @description check if current iteration is not yet the last record
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      Boolean - returns true if current iteration is not yet on the last record
    */
   global boolean hasNext(){
       
       String sQueryOffset;
       //if(i>0){
            //sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i-100);
       //}else{
            sQueryOffset = sQuery;
       //}
       system.debug('%%sQueryOffset: '+sQueryOffset);
       enrolList = Database.Query(sQueryOffset);
       eWrapperList = new list<EnrolmentWrapper>();
       for(Enrolment__c e : enrolList) {
            EnrolmentWrapper eWrap = new EnrolmentWrapper(false, e);
            eWrapperList.add(eWrap);
       }
       
       if(i >= eWrapperList.size()) {
           return false;
       } else {
           return true;
       }
   }
  
    /*
     * @description check if current iteration is not yet the first record
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      Boolean - returns true if current iteration is not yet on the first record
    */
   global boolean hasPrevious(){
       if(i <= setPageSize) {
           return false;
       } else {
           return true;
       }
   }  

  /*
     * @description moves the iteration on the next pagesize records
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      list<EnrolmentWrapper> - next set of records
    */
   global list<EnrolmentWrapper> next(){      
       eWrapperListRequested = new list<EnrolmentWrapper>();
       Integer startNumber;       
       
       String sQueryOffset;
       if(i>0){
            sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i-100);
       }else{
            sQueryOffset = sQuery;
       }
       system.debug('%%sQueryOffset: '+sQueryOffset);
       enrolList = Database.Query(sQueryOffset);
       eWrapperList = new list<EnrolmentWrapper>();
       for(Enrolment__c e : enrolList) {
            EnrolmentWrapper eWrap = new EnrolmentWrapper(false, e);
            eWrapperList.add(eWrap);
       }
       
       Integer size = eWrapperList.size();
       
       if(hasNext())
       { 
           if(size <= (i + setPageSize)) {
               startNumber = i;
               i = size;
           } else {
               i = (i + setPageSize);
               startNumber = (i - setPageSize);
           }
          
           for(integer start = startNumber; start < i; start++)
           {
               eWrapperListRequested.add(eWrapperList[start]);
           }
       }
       return eWrapperListRequested;
   }
  
    /*
     * @description moves the iteration on the previous pagesize records
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      list<EnrolmentWrapper> - previous set of records
    */
   global list<EnrolmentWrapper> previous(){     
       eWrapperListRequested = new list<EnrolmentWrapper>();

       String sQueryOffset;
       if(i>0){
            sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i-100);
       }else{
            sQueryOffset = sQuery;
       }
       system.debug('%%sQueryOffset: '+sQueryOffset);
       enrolList = Database.Query(sQueryOffset);
       eWrapperList = new list<EnrolmentWrapper>();
       for(Enrolment__c e : enrolList) {
            EnrolmentWrapper eWrap = new EnrolmentWrapper(false, e);
            eWrapperList.add(eWrap);
       }
       
       integer size = eWrapperList.size();
       if(i == size)
       {
           if(math.mod(size, setPageSize) > 0)
           {   
               i = size - math.mod(size, setPageSize);
           }
           else
           {
               i = (size - setPageSize);
           }
       }
       else
       {
           i = (i - setPageSize);
       }
      
       for(integer start = (i - setPageSize); start < i; ++start)
       {
           eWrapperListRequested.add(eWrapperList[start]);
       }
       return eWrapperListRequested;
   }  
   
}