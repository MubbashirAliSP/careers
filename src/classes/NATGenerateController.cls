public class NATGenerateController {

    public void generate() {
        
        String queryStringAddition = '';
        
        String logID = ApexPages.currentPage().getParameters().get('id');
        
        NAT_Validation_Log__c natLog = [ SELECT Id,Is_WA_RAPT__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
        
        if(natLog.Query_Students_2__c != null) {
            queryStringAddition = natLog.Query_Students_2__c;
        }
        
        if(natLog.Delete_Existing_NAT_files__c) {
            
            Id libraryId;
        
            if(natLog.Validation_State__c == 'Queensland') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'Victoria') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'New South Wales') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'South Australia') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'Western Australia') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files WA' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'Austrialian Capital Territory') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files ACT' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'Northern Territory') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NT' LIMIT 1].Id;
            }
            if(natLog.Validation_State__c == 'Tasmania') {
                libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files TAS' LIMIT 1].Id;
            }
        
            List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
         
            try {
                delete existingContent;
            }
            catch(Exception e) { 
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Existing NAT Files Could Not Be Deleted');
            }   
        }
        
        //WA RAPT
        if(natLog.Is_WA_RAPT__c) {
            Database.executeBatch(new NATGeneratorBatchWARAPTStudent(natLog.Id ) );
        }
        
        //OTHER NAT    
        else {
            if(natLog.Validation_State__c != 'New South Wales APL') {
                Database.executeBatch(new NATGeneratorBatch00010( natLog.Validation_State__c, natLog.Training_Organisation_Id__c,natLog.Id ) );    
            }
            else {
                Database.executeBatch(new NATGeneratorBatchEUCollection( natLog.Validation_State__c, natLog.query_main__c, natLog.Id ) );    
            }
        }
        
       
           
    }
}