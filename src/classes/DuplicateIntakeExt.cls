public with sharing class DuplicateIntakeExt {
	
	Intake__c OrigIntake;
	
	public DuplicateIntakeExt(ApexPages.StandardController stdController) {
        OrigIntake = (Intake__c)stdController.getRecord();
    }
}