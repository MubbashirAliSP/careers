/**
 * @description Line Item Qualification Trigger Handler
 * @author Janella Lauren Canlas
 * @date 23.NOV.2012
 *
 * HISTORY
 * - 23.NOV.2012	Janella Canlas - Created.
 */
public with sharing class LineItemQualificationTriggerHandler {
	/*
		This method will insert Line Item Qualification Units
	*/
	public static void insertLineItemQualificationUnits(List<Line_Item_Qualifications__c> LIQualList){
		//instantiate variables
		Set<Id> unitIds = new Set<Id>();
		Set<Id> qualIds = new Set<Id>();
		Set<Id> clIds = new Set<Id>();
		
		List<Qualification_Unit__c> qualUnitList = new List<Qualification_Unit__c>();
		List<Line_Item_Qualification_Units__c> liQualUnitList = new List<Line_Item_Qualification_Units__c>();
		List<Unit_Hours_and_Points__c> unitList = new List<Unit_Hours_and_Points__c>();
		List<Contract_Line_Items__c> states = new List<Contract_Line_Items__c>();
		
		Map<Id, List<Qualification_Unit__c>> qualMap = new Map<Id, List<Qualification_Unit__c>>();
		Map<Id, List<Unit_Hours_and_Points__c>> unitMap = new Map<Id, List<Unit_Hours_and_Points__c>>();
		Map<Id, String> stateMap = new Map<Id, String>(); 
		
		//colect qualification and contract line item ids
		for(Line_Item_Qualifications__c lq : LIQualList){
			qualIds.add(lq.Qualification__c);
			clIds.add(lq.Contract_Line_Item__c);
		}
		//query Contract State from Contract Line Items
		states = [select id, Purchasing_Contract__r.Contract_State__c FROM Contract_Line_Items__c WHERE Id IN:clIds];
		//create map of line item qualifications with corresponding contract state
		for(Line_Item_Qualifications__c l : LIQualList){
			for(Contract_Line_Items__c c : states){
				if(l.Contract_Line_Item__c == c.Id){
					if(!stateMap.containsKey(l.Id)){
						stateMap.put(l.Id,c.Purchasing_Contract__r.Contract_State__c);
					}
				}
			}
		}
		//query qualification units related
		system.debug('qualIds***'+qualIds);
		qualUnitList = [SELECT Unit__c, Unit__r.Name, Unit_Type__c, Stage__c, Qualification__c 
						FROM Qualification_Unit__c 
						WHERE Qualification__c IN: qualIds];
		//create map of qualification ids and corresponding list of qualification unit records				
		for(Id i : qualIds){
			for(Qualification_Unit__c q : qualUnitList){
				if(q.Qualification__c == i){
					if(qualMap.containsKey(i)){
						List<Qualification_Unit__c> tempList = qualMap.get(i);
	                    tempList.add(q);
	                    qualMap.put(i,tempList);
					}
					else{
						qualMap.put(i,new List<Qualification_Unit__c>{q});
					}
				}
				
			}
		}
		//collect unit ids
		for(Qualification_Unit__c q : qualUnitList){
			unitIds.add(q.Unit__c);
		}
		if(unitIds.size() > 0){
			system.debug('unitIds***'+unitIds);
			//query unit hours and points records
			unitList = [SELECT Nominal_Hours__c, Points__c, State__c,State__r.Name,Unit__c FROM Unit_Hours_and_Points__c WHERE Unit__c IN: unitIds];
			//create map of unit ids with corresponding list of unit hours and points records
			for(Unit_Hours_and_Points__c u : unitList){
				if(unitMap.containsKey(u.Unit__c)){
					List<Unit_Hours_and_Points__c> tempList = unitMap.get(u.Unit__c);
	                tempList.add(u);
	                unitMap.put(u.Unit__c,tempList);
	                system.debug('u.State__r.Name'+u.State__r.Name);
				}
				else{
					unitMap.put(u.Unit__c,new List<Unit_Hours_and_Points__c>{u});
				}
				
			}
		}
		system.debug('unitMap***'+unitMap);
		//code to create new Line Item Qualification Records
		for(Line_Item_Qualifications__c l : LIQualList){
			List<Qualification_Unit__c> tempQual = new List<Qualification_Unit__c>();
			List<Unit_Hours_and_Points__c> unitHours = new List<Unit_Hours_and_Points__c>();
			if(qualMap.get(l.Qualification__c) != null){
				tempQual = qualMap.get(l.Qualification__c);
				for(Qualification_Unit__c q : tempQual){
					Line_Item_Qualification_Units__c temp = new Line_Item_Qualification_Units__c();
					//retrieve list of unit hours and points
					if(unitMap.get(q.Unit__c) != null){
						unitHours = unitMap.get(q.Unit__c);
					}
					//retrieve state id
					string state ='';
					if(stateMap.get(l.Id)!= null){
						state = stateMap.get(l.Id);
					}
					system.debug('state*** '+ state);
					system.debug('unitHours*** '+ unitHours);
					if(unitHours.size() > 0){
						for(Unit_Hours_and_Points__c u : unitHours){
							system.debug('u.State__r.Name***'+u.State__r.Name);
							system.debug('u.Nominal_Hours__c***'+u.Nominal_Hours__c);
							system.debug('u.Points__c***'+u.Points__c);
							//populate new line item qualification unit nominal hours and points
							if(u.State__r.Name == state){
								temp.Nominal_Hours__c = u.Nominal_Hours__c;
								temp.Points__c = u.Points__c;
							}
							
						}
					}
					//populate other fields for the new line item qualification unit record
					temp.Qualification_Unit__c = q.Id;
					temp.Line_Item_Qualification__c = l.Id;
					liQualUnitList.add(temp);
				}
			}
		}
		//insert new line item qualification unit records
		try{
			insert liQualUnitList;}catch(Exception e){system.debug('ERROR: '+ e.getMessage());
		}
	}
	/*
		This method will populate Record Type of the Line Item Qualification depending on the Contract Line Item Record Type
		UPDATE: populate Commencement date and Completion Date as well.
	*/
	public static void populateRecordType(List<Line_Item_Qualifications__c> liqList){
		Set<Id> cliIds = new Set<Id>();
		Set<Id> pcids = new Set<Id>();
		
		Map<String,Id> rtMap = new Map<String,Id>();
		
		List<RecordType> rtList = new List<RecordType>();
		List<Contract_Line_Items__c> cliList = new List<Contract_Line_Items__c>();
		
		//collect contract line item ids
		for(Line_Item_Qualifications__c l : liqList){
			cliIds.add(l.Contract_Line_Item__c);
		}
		//query contract line item records
		cliList = [SELECT id, RecordTypeId, Purchasing_Contract__r.RecordType.Name, Purchasing_Contract__r.Commencement_Date__c, 
					Purchasing_Contract__r.Completion_Date__c 
					FROM Contract_Line_Items__c WHERE Id IN: cliIds];
		/*//query recordtype records for the object line item qualification					
		rtList = [SELECT Id, Name, SObjectType from RecordType 
					Where Name IN('AHC Funded','Unit Funded') AND SobjectType='Line_Item_Qualifications__c'];
		//create map of recortype Name and corresponding Id
		for(RecordType r : rtList){
			if(!rtMap.containsKey(r.Name)){
				rtMap.put(r.Name,r.Id);
			}
		}*/
		//populate fields of Line Item Qualifications for update
		for(Line_Item_Qualifications__c l : liqList){
			for(Contract_Line_Items__c c : cliList){
				if(l.Contract_Line_Item__c == c.Id){
					//l.RecordTypeId = rtMap.get(c.Purchasing_Contract__r.RecordType.Name);
					l.Commencement_Date__c = c.Purchasing_Contract__r.Commencement_Date__c;
					l.Completion_Date__c = c.Purchasing_Contract__r.Completion_Date__c;
				}
			}
		}
		
	}
}