/**
 * 26.JUN.2014    Mico Esteban    Added Order By
 * 20.OCT.2014    Bayani Cruz/Nick Guia    Optimized; Elist to be used as data for controller this is so we can utilize the pages for the records
 * @history
 *       28.AUG.2015    Ranyel Maliwanag    Fixed logic on getting Record Type Ids to fix clashes on similarly-named record types but used on different sObjectTypes
 */
public without sharing class InvoiceCommercial {
    
    private final Purchasing_Contracts__c contract;
    private final List<Invoice_Gate_Line_Item__c> ListGates;
    private final Map<Id, List<Invoice_Gate_Line_Item__c>> ListGatesByCli = new Map<Id, List<Invoice_Gate_Line_Item__c>>();
    private final Id unitRTID = Schema.SObjectType.Transaction_Line_Item__c.getRecordTypeInfosByName().get('Unit').getRecordTypeId();
    private final Id enrolmentRTID = Schema.SObjectType.Transaction_Line_Item__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId();
    private final Id batchInvoiceRTID = Schema.SObjectType.Account_Transaction__c.getRecordTypeInfosByName().get('Batch Invoice').getRecordTypeId();
    private AggregateResult[] groupedResults;
    public Purchasing_Contracts__c pur_contract {get;set;}
    private String IdStringShort = '';
    private String IdStringShort2 = '';
    private final List<RecordType> recordtypes;
    Set<Id> gateFeeExemptionsTypeIds = new Set<Id>();
    public static integer pagenumber = 1;
    public static double numrecords {get;set;}
    public static Long numpages {get;set;}
    public static double pgsize = 200.0;
    public static integer currPN {get;set;}
    public decimal pgsd;
    public static Set<String> recSet = new Set<String>();
    public Map<String,String> billedMap = new Map<String,String>();
    public Map<String,String> scheduledfeesMap = new Map<String,String>();
    Public List<Ewrapper> EListTemp = new List<Ewrapper>();
    Public List<Ewrapper>  Elist {get; set;}
    
    
    /* Added BAC 20.10.2014 */
    public Map<Id, List<Invoice_Gate_Line_Item__c>> nonCncessionalInv = new Map<Id, List<Invoice_Gate_Line_Item__c>>();
    public List<Enrolment__c> enrol = new List<Enrolment__c>();
    public static boolean hasInitialised = false;
    /* End BAC 20.10.2014 */

    public InvoiceCommercial() {
    
        contract = [Select 
                    Id, Invoicing_Model__c,Contract_Code__c,SF_Nominal_Amount__c,
                    Training_Organisation__c, Contract_Type__c, Don_t_include_tuition_fees_in_NAT_report__c
                    From Purchasing_Contracts__c
                    Where Id = : ApexPages.currentPage().getParameters().get('CId')];
        ListGates = new List<Invoice_Gate_Line_Item__c>(
                    [Select 
                     i.Contract_Line_Item__c,
                     i.of_Enrolment_Completed__c, 
                     i.Amount__c,
                     i.Fee_Exemption_Type__c,
                     i.Fee_Exemption_Type__r.Specific_Funding_Relationship__c,
                     i.Qualification__c,
                     i.Concessional_Rate__c
                     From Invoice_Gate_Line_Item__c i 
                     WHERE Contract_Line_Item__r.Purchasing_Contract__c = : ApexPages.currentPage().getParameters().get('CId') 
                     ORDER BY i.of_Enrolment_Completed__c]);
        groupedResults = [Select 
                          Enrolment__c, SUM(t.Net_Invoice_Amount__c) Total_Amount 
                          From Transaction_Line_Item__c t 
                          WHERE t.Enrolment__r.Purchasing_Contract_ID__c = : ApexPages.currentPage().getParameters().get('CId') 
                          GROUP BY Enrolment__c ];
        recordtypes = new List<RecordType>([Select 
                                            Id, Name 
                                            From RecordType 
                                            WHERE Name in ('Unit','Enrolment', 'Batch Invoice') 
                                            order by Name desc]);
    
        pur_contract = contract;
        
        system.debug('**List Gates: ' + ListGates.size());
        for ( Invoice_Gate_Line_Item__c g : ListGates){
            if ( !ListGatesByCli.containsKey(g.Contract_Line_Item__c) )
                ListGatesByCli.put(g.Contract_Line_Item__c, new List<Invoice_Gate_Line_Item__c>());
             ListGatesByCli.get(g.Contract_Line_Item__c).add(g);
            /* Added NAG 20.10.2014 */
            if(g.Fee_Exemption_Type__r.Specific_Funding_Relationship__c != 'Certificate 3 Guarantee - Concessional') {
                if (!nonCncessionalInv.containsKey(g.Contract_Line_Item__c)) {
                    nonCncessionalInv.put(g.Contract_Line_Item__c, new List<Invoice_Gate_Line_Item__c>());
                } else {
                    nonCncessionalInv.get(g.Contract_Line_Item__c).add(g);
                }
            }
            /* End NAG 20.10.2014 */
            
             system.debug('**Added!');
            gateFeeExemptionsTypeIds.add(g.Fee_Exemption_Type__c);
        }
        
        calculateBilledToDate();
        
        queryEnrolments();
        
        useSetCon();
    }
            
    Public ApexPages.StandardSetController setCon {
        get {
            
            if(setCon == null) {
                List<Enrolment__c> enr = new List<Enrolment__c>();
                for(Ewrapper ewr : Elist) {
                    enr.add(ewr.e);
                }
                setCon = new ApexPages.StandardSetController(enr);
                hasInitialised = true;
            }
            setCon.setPageSize(integer.valueof(pgsize));
            numrecords = setCon.getResultSize();            
            pgsd = numrecords / pgsize;
            numpages = pgsd.round(RoundingMode.CEILING);
            currPN = setCon.getPageNumber();
            return setCon;
            
        }
        set;
    }
  
    //20.10.2014 BAC/NAC Optimized
    public void queryEnrolments() {
        
        String Temp = ApexPages.currentPage().getParameters().get('CId');
        String Temp2 = pur_contract.Training_Organisation__c;
        boolean isCert3Contract = contract.Contract_Type__c == 'Certificate 3 Guarantee';
        
        IdStringShort = temp.Substring(0,15);
        
        IdStringShort2 = temp2.Substring(0,15);
        
        Elist = new List<Ewrapper>();
        
        groupedResults = [Select 
                          Enrolment__c, SUM(t.Net_Invoice_Amount__c) Total_Amount 
                          From Transaction_Line_Item__c t 
                          WHERE t.Enrolment__r.Purchasing_Contract_ID__c = : IdStringShort GROUP BY Enrolment__c ];
        
        calculateBilledToDate();
        
        List<Enrolment__c> ee = new List<Enrolment__c>();
        
        if (hasInitialised == true) {
            ee = setCon.getRecords();
        } else {
            
        ee = [ Select 
                                  Id,Enrolment_Status__c, Line_Item_Qualification__r.Contract_Line_Item__c,Student__c, Name,
                                  Student__r.Full_Student_Name__c, Student__r.Student_Identifer__c, 
                                  Qualification__r.Name, Invoice_Employer__c, Employer__c,
                                  Fee_Exemption__r.Discount_Percentage__c, of_Enrolment_Completed__c,
                                  Purchasing_Contract_ID__c,Training_Organization_ID__c,
                                  End_Date__c, Line_Item_Qualification__r.State_Fund_Source_Identifier__c
                              From Enrolment__c 
                              WHERE 
                                 Training_Organization_ID__c = :IdStringShort2 
                                 AND Line_Item_Qualification__r.Do_not_Invoice_related_Enrolments__c = false
                                 AND Purchasing_Contract_ID__c = : IdStringShort 
                                 AND of_Enrolment_Completed__c != null
                                 AND Ignore_for_Invoicing__c = false
                                 //26.JUN.2014 MME
                                 Order By Enrolment_Status__c, Student__r.Full_Student_Name__c ];
            
        }
    
                                         
        List<Fee_Exemption__c> feeExemptions = [ Select
                                                 Fee_Exemption_Type__c, Enrolment__c,
                                                 Valid_From__c, Valid_To__c,
                                                 Fee_Exemption_Type__r.Specific_Funding_Relationship__c
                                                 From Fee_Exemption__c 
                                                Where Enrolment__c IN :ee
                                                AND Fee_Exemption_Type__c in :gateFeeExemptionsTypeIds ];
                                                
        system.debug('**Gate Fee Exemption Id: ' + gateFeeExemptionsTypeIds.size());
        system.debug('**Fee Exemption List: ' + feeExemptions.size());
        /*We only need to show enrolments, where billed to date != scheduled payment */
        

        /* Added BAC 20.10.2014 */
        Map<Id, Fee_Exemption__c> mEnrFee = new Map<Id, Fee_Exemption__c>();
        for(Fee_Exemption__c fe : feeExemptions) {
            mEnrFee.put(fe.Enrolment__c, fe);
        }

        Map<Id, Ewrapper> mElistTemp = new Map<Id, Ewrapper>();
        for(EWrapper tw : ElistTemp) {
            mElistTemp.put(tw.e.Id, tw);
        }
        /* End BAC 20.10.2014 */
          
            for(Enrolment__c e : ee) {
            
                 //create a candidate wrapper...
                 EWrapper ew = New Ewrapper(e);
                 
                 //if selected in the temp list, wrapper must also be selected
                 if(mElistTemp.get(e.Id) != null) {
                     if(mElistTemp.get(e.Id).selected) {
                         ew.selected = true;
                     } else {
                         ew.selected = false;
                     }
                 }        
                          
                  /*To Calculate Current Student Fees following steps are required
                   *1. Grab all current invoice gates from purchasing contract with percent and amount field
                   *2. Loop through Enrolments and find in which percentage category it fits, for example
                       if 25% - 1000 dollars, than if 25 or more amount should be 1000.
                         if 10% - 1000
                            25% - 1000
                         than if Enrolment is at 25%, sum up all gates up to 25% including the 25% gate
                         In this case, Total Amount is 2000
                   *3. Attach EnrolmentId and Amount to the map

                    but: if contract type is "Certificate 3 Guarantee" then:
                    * also make sure that the gate qual matches 
                    * and make sure that the enrolment has EXACTLY one of the gate qualifications 
                   
                    and:
                    * if the enrolment is ENJ then the enrolment can't use their concessional value
                   */
                                      
                //find the enrolment's fee exemption
                boolean enrolmentIsCert3Concessional;
                Fee_Exemption_Types__c enrolmentFeeExemption = null;
                if ( isCert3Contract ){
                    system.debug('**Is Cert 3 Contract? ' + isCert3Contract);
                    Id lastCert3ExemptionMatched = null;

                    if(mEnrFee.get(e.Id) != null) {
                        //valid exemption
                        if ( lastCert3ExemptionMatched == null ){
                            //store the exemption.
                            lastCert3ExemptionMatched = mEnrFee.get(e.Id).Fee_Exemption_Type__c;
                            system.debug('**Cert Matched!');
                        }else if ( lastCert3ExemptionMatched != mEnrFee.get(e.Id).Fee_Exemption_Type__c ){
                            ew.errors.add('Enrolment matches multiple exemptions');
                            //break;
                        }
                        enrolmentFeeExemption = mEnrFee.get(e.Id).Fee_Exemption_Type__r;
                        system.debug('**Enrolment Fee Exemption: ' + enrolmentFeeExemption);
                    }
                }
                system.debug('**Enrolment Fee Exemption 2: ' + enrolmentFeeExemption);
                system.debug('**Is Cert 3 Contract 2? ' + isCert3Contract);
                system.debug('*3*'+ew);
                if ( isCert3Contract && enrolmentFeeExemption == null ){
                    //enrolments must have a fee exemption
                    ew.errors.add('Enrolment has no C3G Fee Exemptions');
                }else{
                    if ( ListGatesByCli.get(e.Line_Item_Qualification__r.Contract_Line_Item__c) == null ){
                        ew.errors.add('No Invoice Gates for Contract Line Item');
                    }else{
                        for ( Invoice_Gate_Line_Item__c g : ListGatesByCli.get(e.Line_Item_Qualification__r.Contract_Line_Item__c) ){
                             if ( isCert3Contract ){
                                //cert 3 contracts: we filter out some gates based on quals and fees:
        
                                 //ignore gate if it's not for the correct qualification
                                 if ( g.Qualification__c != e.Qualification__c ){
                                     continue;
                                 }
        
                                //special case: when enrolment funding is ENJ
                                if ( e.Line_Item_Qualification__r.State_Fund_Source_Identifier__c == 'ENJ'
                                    //and the enrolment has a concession
                                    && enrolmentFeeExemption.Specific_Funding_Relationship__c == 'Certificate 3 Guarantee - Concessional' ){
        
                                    //then we force the enrolment to use the non-concessional exemption...
                                    Id nonConcessionalExcemptionId = null;
                                    //try and find a non-concessional other invoice gate...
                                       //Refactored NAC 20.10.2014
                                    for ( Invoice_Gate_Line_Item__c o : nonCncessionalInv.get(e.Line_Item_Qualification__r.Contract_Line_Item__c)){
                                        if ( o.Qualification__c == e.Qualification__c && o.Fee_Exemption_Type__r.Specific_Funding_Relationship__c != 'Certificate 3 Guarantee - Concessional' ){
                                            nonConcessionalExcemptionId = o.Fee_Exemption_Type__c;
                                            System.debug('*1234*'+o.Fee_Exemption_Type__c);
                                        }
                                    }
                                        //End NAC 20.10.2014
        
                                    if ( nonConcessionalExcemptionId == null ){
                                        ew.errors.add('Enrolment is ENJ but no non-concessional gates were found');
                                        continue;
                                    }else{
                                        //skip other fee types
                                        if ( g.Fee_Exemption_Type__c != nonConcessionalExcemptionId ){
                                             continue;
                                        }
                                    }
        
                                }else{
                                    //skip non-matching fee types
                                    if ( g.Fee_Exemption_Type__c != enrolmentFeeExemption.Id ){
                                         continue;
                                     }
                                }
                             }
                            
                             //add this gate to what's owed if the gate has been passed
                             if( g.of_Enrolment_Completed__c <= e.of_Enrolment_Completed__c) {
                                 ew.ta += g.Amount__c;
                             }
                             ew.hasValidGates = true;
                        }
                    }
                }

                //flag users who don't match any gates at all
                if ( ew.getIsValid() && !ew.hasValidGates ){
                    ew.errors.add('Enrolment does not match any Invoice Gates');
                }             
                 //check if the enrolment has been billed against already
                 system.debug('*2*'+ew);
                 if(billedMap.ContainsKey(e.Id) ){
                     ew.billedToDate = Decimal.ValueOf(billedMap.get(e.Id));
                 }

                 //enrolment already billed this amount or more don't need to be shown
                 system.debug('*1*'+ew);
                 if ( ew.getIsValid() && ew.billedToDate >= ew.ta ){
                    continue;
                 }
                 
                 //calculate how much to pay
                 ew.tac = ew.ta - ew.billedToDate;
                 
                 //candidate for invoicing!
                 EList.add(ew);
                 system.debug('**Enrolment: ' + e.Student__r.Full_Student_Name__c + ' ,Billed to Date: ' + ew.billedToDate);
                
            }
            
            useSetCon();
        }
    
    /**********************************************************
    *  Added BAC 20.10.2014
  *  This is so we can get the use the page set of 
  *  the standardsetcontroller (Elist records)
  ***********************************************************/
    private void useSetCon() {
         /* get all enrolments from the standardSetController and place in map*/
            Map<Id, Enrolment__c> mEnr = new Map<Id, Enrolment__c>();
            List<Enrolment__c> enral = setCon.getRecords();
            for(Enrolment__c r : enral) {
                mEnr.put(r.Id, r);
            }
            
        /* replace the Elisth with the enrolments from mEnr - enrolment records from the standardSetController */
            List<Ewrapper> finalwrap = new List<Ewrapper>();
            for(Ewrapper ewra : Elist) {
                if(mEnr.containsKey(ewra.e.Id)) {
                    ewra.e = mEnr.get(ewra.e.Id);
                    finalwrap.add(ewra);
                }
            }
            Elist.clear();
            Elist = finalwrap;
    }
    
     /*Calculate Billed to Date for Each available Enrolment
      *This is the sum of Transaction Line Item Amounts per Enrolment
      */
      public void calculateBilledToDate() {
        
          for(AggregateResult ar : groupedResults)
            billedMap.put(String.ValueOf(ar.get('Enrolment__c')),String.ValueOf(ar.get('Total_Amount')));
     
      }
      
    // cancel button
    
    public PageReference cancel() {return new PageReference('/'+ApexPages.currentPage().getParameters().get('CId'));}
    
     // pageanation navigation buttons
         
    public PageReference doFirst() {
        setCon.first();
        queryEnrolments();
        return null;
    }
     
    public PageReference doNext() {
        if (hasNext) {
            
            
            
            setCon.next();
            queryEnrolments();
            return null;
            }
        return null;
    }
               
    public PageReference doPrev() {
        if (hasPrev) {
            setCon.previous();
            queryEnrolments();
            return null;
        }
        return null;
        }
     
    public PageReference doLast() {
        setCon.last();
        queryEnrolments();
        return null;
     }
     
    public PageReference doClick() {
    
    set<Id> ewId = new set<Id>();
    
    for (EWrapper w : Elist) {
        if (w.selected) {
            if( 0 > ElistTemp.size() ) {
                ElistTemp.add(w);
            }
            else {
                for (EWrapper ewt : ElistTemp) {
                    ewId.add(ewt.e.Id);
                }
            if( !ewId.contains(w.e.Id) ) {
                ElistTemp.add(w);
                }
            }
        }
    }
    
    if( 0 < ElistTemp.size() ) {
        for (EWrapper tw : ElistTemp) {
            for (EWrapper ew : EList) {
                if (ew.e.Id == tw.e.Id) {
                    tw.selected = ew.selected;
                        }
                    }
                }
            }
            
    // fixes page number display when onclick happens
    
    numpages = pgsd.round(RoundingMode.CEILING);
    currPN = setCon.getPageNumber();
   
    return null;
    }

    public Boolean hasNext {
        get { return setCon.getHasNext(); } set; }
        
    public Boolean hasPrev {
       get { return setCon.getHasPrevious(); } set; }

    // submit invoicing button

        public PageReference submit() {
        
        List<Account_Transaction__c> employerInvoice = new List<Account_Transaction__c>();
        Set<Id>uniqueEmployers = new Set<Id>();
        
        List<Account_Transaction__c> studentInvoice = new List<Account_Transaction__c>();
        Set<Id>uniqueStudents = new Set<Id>();
        
        //cretate batch record
        Invoice_Batch__c batch = new Invoice_Batch__c();
        insert batch;
        
        //Need to determine payer
        Set<Id> ewId = new Set<Id>();
        
        for (EWrapper w : ElistTemp) {
        if (w.selected) {
            if( 0 > EList.size() ) {
                EList.add(w);
            }
            else {
                for (EWrapper ewt : EList) {
                    ewId.add(ewt.e.Id);
                }
            if( !ewId.contains(w.e.Id) ) {
                Elist.add(w);
                }
            }
        }
    }
        
        
        
        for(EWrapper w : EList) {
                    
            if(test.isRunningTest())
                w.selected = true;
            
            if(w.selected) {
                //employer
                if(w.e.Invoice_Employer__c == true) {
                    if(!uniqueEmployers.contains(w.e.Employer__c)) {
                        Account_Transaction__c acc_tran = new Account_Transaction__c(Payer__c = w.e.Employer__c, Training_Organisation__c = contract.Training_Organisation__c,Invoice_Batch__c = batch.Id,Purchasing_Contract__c = pur_contract.Id, RecordTypeId = batchInvoiceRTID);
                        system.debug('**Payer: ' + w.e.Employer__c);
                        system.debug('**Training Org: ' + contract.Training_Organisation__c);
                        system.debug('**Invoice: ' + batch.Id);
                        system.debug('**PurCon: ' + pur_contract.Id);
                        system.debug('**RecType: ' + batchInvoiceRTID);
                        employerInvoice.add(acc_tran);
                        system.debug('**List: ' + employerInvoice);
                        uniqueEmployers.add(w.e.Employer__c);
                        
                    }
                    
                //student
                } else {
                    
                    if(!uniqueStudents.contains(w.e.Student__c)) {
                        Account_Transaction__c acc_tran = new Account_Transaction__c(Payer__c = w.e.Student__c, Training_Organisation__c = contract.Training_Organisation__c,Invoice_Batch__c = batch.Id,Purchasing_Contract__c = pur_contract.Id, RecordTypeId = batchInvoiceRTID);
                        studentInvoice.add(acc_tran);
                        uniqueStudents.add(w.e.Student__c);
                        
                    }
                }
            }
        }
        
        /* for(EWrapper tw : EListTemp) {
            
            if(test.isRunningTest())
                tw.selected = true;
            
            if(tw.selected) {
                if(tw.e.Invoice_Employer__c == true) {
                    if(!uniqueEmployers.contains(tw.e.Employer__c)) {
                        Account_Transaction__c acc_tran = new Account_Transaction__c(Payer__c = tw.e.Employer__c, Training_Organisation__c = contract.Training_Organisation__c,Invoice_Batch__c = batch.Id,Purchasing_Contract__c = pur_contract.Id, RecordTypeId = batchInvoiceRTID);
                        employerInvoice.add(acc_tran);
                        uniqueEmployers.add(tw.e.Employer__c);
                    }
                //student
                } else {
                    if(!uniqueStudents.contains(tw.e.Student__c)) {
                        Account_Transaction__c acc_tran = new Account_Transaction__c(Payer__c = tw.e.Student__c, Training_Organisation__c = contract.Training_Organisation__c,Invoice_Batch__c = batch.Id,Purchasing_Contract__c = pur_contract.Id, RecordTypeId = batchInvoiceRTID);
                        studentInvoice.add(acc_tran);
                        uniqueStudents.add(tw.e.Student__c);
                    }
                }
            }
        } */
        
        if(employerInvoice.size() == 0 && studentInvoice.size() == 0 && ElistTemp.size() == 0) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please Select At Least 1 Record To Continue');
            return null;
        }
        
        if(employerInvoice.size() == 0 && studentInvoice.size() == 0 && ElistTemp.size() == 0) {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select At Least 1 Record To Continue'));
            return null;
        }
        
        insert employerInvoice;
        insert studentInvoice;    
        
        /*now, run through all items and create line items
         * which will be attached to Account Transactions just inserted
         */
         
        Map<Id, Id> invoicesMap = new Map<Id, Id>();
        List<Transaction_Line_Item__c> items = new List<Transaction_Line_Item__c>();
        
        for(Account_Transaction__c a : [Select Id, Payer__c From Account_Transaction__c Where Invoice_Batch__c = : batch.Id])
            invoicesMap.put(a.Payer__c, a.Id);
        
        string transactionLineType = contract.Contract_Type__c == 'Certificate 3 Guarantee' ?  'Student Fees' : 'Commercial Services';
      
                 
        /* was creating dup line items, commented out because now handled by the temp wrapper item */
        
        for(EWrapper w : EList) {
        
            if(w.selected) {
                    if(w.e.Invoice_Employer__c == true) {
                        if(w.applyGST){
                        Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(w.e.Employer__c),Student__c = w.e.Student__c,Enrolment__c = w.e.Id,Amount__c = w.tac, Tax_Attributable__c = w.tac / 100 * 10,Type__c = transactionLineType);
                        items.add(item);
                        }else {
                        Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(w.e.Employer__c),Student__c = w.e.Student__c,Enrolment__c = w.e.Id,Amount__c = w.tac,Type__c = transactionLineType);
                        items.add(item);
                        }
                } else {
                    if(w.applyGST) {
                        Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(w.e.Student__c), Student__c = w.e.Student__c,Enrolment__c = w.e.Id,Amount__c = w.tac, Tax_Attributable__c = w.tac / 100 * 10,Type__c = transactionLineType);
                        items.add(item);
                }else {
                        Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(w.e.Student__c), Student__c = w.e.Student__c,Enrolment__c = w.e.Id,Amount__c = w.tac,Type__c = transactionLineType);
                        items.add(item);
                        
                    }
                }
            }
        }  
    
        /* for(EWrapper tw : EListTemp) {
            if(tw.selected) {
                        if(tw.e.Invoice_Employer__c == true) {
                            if(tw.applyGST){
                                Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(tw.e.Employer__c),Student__c = tw.e.Student__c,Enrolment__c = tw.e.Id,Amount__c = tw.tac, Tax_Attributable__c = tw.tac / 100 * 10,Type__c = transactionLineType);
                                items.add(item);
                            }else {
                                Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(tw.e.Employer__c),Student__c = tw.e.Student__c,Enrolment__c = tw.e.Id,Amount__c = tw.tac,Type__c = transactionLineType);
                                items.add(item);
                            }
                            } else {
                                if(tw.applyGST) {
                                Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(tw.e.Student__c), Student__c = tw.e.Student__c,Enrolment__c = tw.e.Id,Amount__c = tw.tac, Tax_Attributable__c = tw.tac / 100 * 10,Type__c = transactionLineType);
                                items.add(item);
                            }else {
                                Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = enrolmentRTID, Account_Transaction__c = invoicesMap.get(tw.e.Student__c), Student__c = tw.e.Student__c,Enrolment__c = tw.e.Id,Amount__c = tw.tac,Type__c = transactionLineType);
                                items.add(item);
                        
                        }
                    }
                }
            }   */
        
        insert items;
        
        InvoiceStudentFeesController.insertTuitionFeeHistoryEnroLevel(contract, items);
        
        return new PageReference('/' + batch.Id);

    }
    
     public class Ewrapper {
        public boolean hasValidGates;
        public Enrolment__c e {get;set;}
        public boolean selected {get;set;}
        public Boolean applyGST {get;set;}
        public Decimal billedToDate {get;set;}
        public Decimal ta  {get;set;} //total amount
        public Decimal tac {get;set;} //total amount to pay
        public set<string> errors {get;private set;}
         
         public Ewrapper(Enrolment__c enrolment) {
             this.e = enrolment;
             this.hasValidGates = false;
             this.selected = false;
             this.applyGST = false;
             this.billedToDate = 0.0;
             this.ta = 0.0;
             this.tac = 0.0;
             this.errors = new set<String>();
         }
         
         public boolean getIsValid(){
            return errors.isEmpty();
         }
         public string getErrorString(){
             string ret = '';
             for ( String error : errors ){
                if ( ret != '' )
                    ret += ', ';
                 ret += error;
             }
             return ret;
        }
    }
}