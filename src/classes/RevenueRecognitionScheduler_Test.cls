/**
 * @description Test class for `RevenueRecognitionScheduler`
 * @author Ranyel Maliwanag
 * @date 18.SEP.2015
 */
@isTest
public with sharing class RevenueRecognitionScheduler_Test {
    @testSetup
    static void schedulerSetup() {
        RevenueRecognitionSetting__c customSetting = new RevenueRecognitionSetting__c(
                Name = 'Test Email',
                Notification_Email__c = 'test@somewhere.com.example'
            );
        insert customSetting;
    }


    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Revenue Recognition Test', schedule, new RevenueRecognitionScheduler());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}