/**
 * @description Controller for RemoveFromClasses page Test Class
 * @author Janella Lauren Canlas
 * @date 18.MAR.2013
 *
 * HISTORY
 * - 19.MAR.2013    Janella Canlas - Created.
 * - 08.APR.2014    Michelle Magsarili - Change Status to Active to pass Enrolment Validation
 */
@isTest(seeAllData=true)
private class RemoveFromClasses_CC_Test {

    /*
        Test Remove From Classes CC
    */ 
    static testMethod void testRemoveFromClasses() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Type_of_Attendance__c = cattend.Id; 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            
            insert enrl;
                        
           // enrlUnit = [SELECT id, Start_Date__c, End_Date__c from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            System.debug('@@clas '+clas);
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);
            insert clEnrolment;
            PageReference testPage = Page.RemoveFromClasses;
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            RemoveFromClasses_CC remove = new RemoveFromClasses_CC();
            remove.intake = intake.Id;
            remove.getenrolmentRec();
            remove.getIntakeUnitPicklist();
            remove.getIntakePicklist();
            remove.fillPageWrapper();
            remove.removeEnrolment();
            remove.cancel();

            test.stopTest();
        }
    }
    static testMethod void testRemoveFromClasses2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Classes__c classRec = new Classes__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Type_of_Attendance__c = cattend.Id; 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            
            insert enrl;
                        
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            //List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            //clas.Intake_Unit__c = intakeUnit.Id;
            //clas.Start_Date_Time__c = date.today().addDays(30);
            //clas.End_Date_Time__c = date.today().addMonths(5);
            //update clas;
            
            //System.debug('@@clas '+clas);
            
            Classes__c class1 = new Classes__c();
            class1.Intake__c = intake.Id;
            class1.Start_Date_Time__c = datetime.newInstance(2013, 03, 18, 12, 30, 0);
            class1.End_Date_Time__c = datetime.newInstance(2013, 03, 18, 01, 0, 0);
            class1.Class_Configuration__c = classConfig.Id;
            insert class1;
            List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 2];
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas[0].Id, enrlIntakeUnit.Id, attend.Id);
            insert clEnrolment;
            
            clEnrolment2 = TestCoverageUtilityClass.createClassEnrolment(class1.Id, enrlIntakeUnit.Id, attend.Id);
            insert clEnrolment2;
            
            PageReference testPage = Page.RemoveFromClasses;
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            RemoveFromClasses_CC remove = new RemoveFromClasses_CC();
            remove.choseIntakeUnit = true;
            remove.intakeUnit = intakeUnit.Id;
            remove.getenrolmentRec();
            remove.getIntakeUnitPicklist();
            remove.getIntakePicklist();
            remove.fillPageWrapper();
            remove.removeEnrolment();
            remove.cancel();

            test.stopTest();
        }
    }
    static testMethod void testRemoveFromClasses3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit3 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment3 = new Class_Enrolment__c();
        Classes__c classRec = new Classes__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Type_of_Attendance__c = cattend.Id; 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            
            insert enrl;
            
                        
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            enrlUnitRec = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            enrlIntakeUnit2 = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit2;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            //List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            //clas.Intake_Unit__c = intakeUnit.Id;
            //clas.Start_Date_Time__c = date.today().addDays(30);
            //clas.End_Date_Time__c = date.today().addMonths(5);
            //update clas;
            
            //System.debug('@@clas '+clas);
            
            Classes__c class1 = new Classes__c();
            class1.Intake__c = intake.Id;
            class1.Start_Date_Time__c = datetime.newInstance(2013, 03, 18, 12, 30, 0);
            class1.End_Date_Time__c = datetime.newInstance(2013, 03, 18, 01, 0, 0);
            class1.Class_Configuration__c = classConfig.Id;
            //class1.Intake_Unit__c = intakeUnit.Id;
            insert class1;
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);
            clEnrolment.Attendance_Type__c = null;
            insert clEnrolment;
            
            clEnrolment2 = TestCoverageUtilityClass.createClassEnrolment(class1.Id, enrlIntakeUnit.Id, attend.Id);
            clEnrolment2.Enrolment__c = enrl.Id;
            clEnrolment2.Attendance_Type__c = null;
            insert clEnrolment2;
            
            clEnrolment3 = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit2.Id, attend.Id);
            clEnrolment3.Attendance_Type__c = null;
            insert clEnrolment3;
            
            PageReference testPage = Page.RemoveFromClasses;
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            test.startTest();
            RemoveFromClasses_CC remove = new RemoveFromClasses_CC();
            remove.intakeUnit = intakeUnit.Id;
            remove.getenrolmentRec();
            remove.getIntakeUnitPicklist();
            remove.getIntakePicklist();
            remove.fillPageWrapper();
            List<RemoveFromClasses_CC.WrapperClass> wc = remove.pageWrapperList;
            wc[0].check = false;
            wc[1].check = true;
            wc[2].check = true;
            remove.pageWrapperList = wc;
            remove.removeEnrolment();
            List<Class_Enrolment__c> eiu = [select Id from Class_Enrolment__c where Enrolment_Intake_Unit__c =: enrlIntakeUnit.Id];
            system.debug('*****EIU'+eiu); 
            remove.cancel();

            test.stopTest();
        }
    }

    static testMethod void testRemoveFromClasses4() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit2 = new Enrolment_Intake_Unit__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit3 = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment2 = new Class_Enrolment__c();
        Class_Enrolment__c clEnrolment3 = new Class_Enrolment__c();
        Classes__c classRec = new Classes__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Type_of_Attendance__c = cattend.Id; 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            
            insert enrl;
            
                        
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            enrlUnitRec = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            enrlIntakeUnit2 = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit2;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            //List<Classes__c> clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id];
            //clas.Intake_Unit__c = intakeUnit.Id;
            //clas.Start_Date_Time__c = date.today().addDays(30);
            //clas.End_Date_Time__c = date.today().addMonths(5);
            //update clas;
            
            //System.debug('@@clas '+clas);
            
            Classes__c class1 = new Classes__c();
            class1.Intake__c = intake.Id;
            class1.Start_Date_Time__c = datetime.newInstance(2013, 03, 18, 12, 30, 0);
            class1.End_Date_Time__c = datetime.newInstance(2013, 03, 18, 01, 0, 0);
            class1.Class_Configuration__c = classConfig.Id;
            //class1.Intake_Unit__c = intakeUnit.Id;
            insert class1;
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);
            clEnrolment.Attendance_Type__c = null;
            insert clEnrolment;
            
            clEnrolment2 = TestCoverageUtilityClass.createClassEnrolment(class1.Id, enrlIntakeUnit.Id, attend.Id);
            clEnrolment2.Enrolment__c = enrl.Id;
            clEnrolment2.Attendance_Type__c = null;
            insert clEnrolment2;
            
            clEnrolment3 = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit2.Id, attend.Id);
            clEnrolment3.Attendance_Type__c = null;
            insert clEnrolment3;
            
            PageReference testPage = Page.RemoveFromClasses;
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            test.startTest();
            RemoveFromClasses_CC remove = new RemoveFromClasses_CC();
            remove.intakeUnit = intakeUnit.Id;
            remove.getenrolmentRec();
            remove.getIntakeUnitPicklist();
            remove.getIntakePicklist();
            remove.fillPageWrapper();
            List<RemoveFromClasses_CC.WrapperClass> wc = remove.pageWrapperList;
            wc[0].check = true;
            wc[1].check = true;
            wc[2].check = true;
            remove.pageWrapperList = wc;
            remove.removeEnrolment();
            List<Class_Enrolment__c> eiu = [select Id from Class_Enrolment__c where Enrolment_Intake_Unit__c =: enrlIntakeUnit.Id];
            system.debug('*****EIU'+eiu); 
            remove.cancel();

            test.stopTest();
        }
    }
}