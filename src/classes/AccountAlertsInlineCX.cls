/**
 * @description Custom controller extension for displaying account alerts
 * @author Ranyel Maliwanag
 * @date 04.MAR.2016
 */
public without sharing class AccountAlertsInlineCX {
	public Account student {get; set;}
	public List<Event> accountAlerts {get; set;}

	public AccountAlertsInlineCX(ApexPages.StandardController controller) {
		Id recordId = controller.getId();
		student = [SELECT Id, (SELECT Id, Subject FROM Events WHERE RecordType.Name = 'Account Alert') FROM Account WHERE Id = :recordId];
		accountAlerts = student.Events;
	}
}