public with sharing class EnrolmentPlacementDetailHandler {
	public static void populateStudent(List<Enrolment_Placement_Detail__c> epdList){
		List<Enrolment__c> eList = new List<Enrolment__c>();
		Set<Id> eIds = new Set<Id>();
		Map<Id, Id> eMap = new Map<Id, Id>();
		for(Enrolment_Placement_Detail__c e : epdList){
			eIds.add(e.Enrolment__c);
		}
		eList = [select Id, Student__c from Enrolment__c where Id IN: eIds];
		for(Enrolment__c e : eList){
			eMap.put(e.Id,e.Student__c);
		}
		for(Enrolment_Placement_Detail__c e : epdList){
			if(e.Enrolment__c != null){
			 	e.Student__c = eMap.get(e.Enrolment__c);
			}
		}
	}
}