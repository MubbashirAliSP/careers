/*This is a test class for CMS website integration
 *created by Yuri Gribanov 28-10-2015
 */

@isTest private class CMSIntegrationTests {
    
    private static set<Id>qualIds = new Set<Id>();
    private static set<Id>wdIds = new Set<Id>();
    
    static void insertTestData() {
        
        CMS_Integration_Settings__c cms_cs = new CMS_Integration_Settings__c(Name= 'Default',
                                                                                   CMS_JitterBit_Live_Endpoint__c = 'http://CareersAusTRIAL89987.jitterbit.eu/clouddev/1.0/processsobject',
                                                                                   CMS_JitterBit_Staging_Endpoint__c = 'http://CareersAusTRIAL89987.jitterbit.eu/clouddev/1.0/processsobject'
       
      );


      insert cms_cs;
        
        
        
        
        Id recrdTypeIdFld = Schema.SObjectType.Field_of_Education__c.getRecordTypeInfosByName().get('Division').getRecordTypeId();

        Field_of_Education__c fld = new Field_of_Education__c(

            recordtypeid  = recrdTypeIdFld,
            Name = 'Test Field of Education'

        );

        insert fld;
        
        Id recrdTypeIdQual = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Qualification').getRecordTypeId();

        Qualification__c qual = new Qualification__c(

           RecordTypeId = recrdTypeIdQual,
           Name = 'Test Qualification',
           Qualification_Name__c = 'Diploma of Business',
           Available_for_Online_Enrolments__c = true,
           Include_in_CCQI_export__c = True,
           Active__c = true,
           Vet_Non_Vet__c = True,
           VFH_Approved__c = True,
           Eligible_for_Website__c = true,
           Field_of_Education__c = fld.Id,
           Website_Update_Required__c = true
           
        );
             
        insert qual;
        
        qualIds.add(qual.id);
        
        Website_Dual_Qualification__c dualqual = new Website_Dual_Qualification__c(WDQ_Qualification_1__c = qual.Id, WDQ_Description__c = 'blah',WDQ_Course_Amount_QLD__c = '1000');
        
        insert dualqual;
        
        Website_Stream__c websitestream = new Website_Stream__c(Qualification_1__c = qual.Id);
        
        insert websitestream;
        
        Website_Delivery_Mode__c websitedeliverymode = new Website_Delivery_Mode__c(Qualification__c = qual.Id);
        
        insert websitedeliverymode;
        
        Website_Provider__c websiteprovider = new Website_Provider__c(WP_Qualification__c = qual.Id);
        
        insert websiteprovider;
        
        Id UnitRTID = Schema.SObjectType.Unit__c.getRecordTypeInfosByName().get('Unit of Competency').getRecordTypeId();
        
        Unit__c unit = new Unit__c(RecordTypeId = UnitRTID,Name = 'Test Unit');
        
        insert unit;
        
        Qualification_Unit__c qualunit = new Qualification_Unit__c(unit__c = unit.Id, Qualification__c = qual.Id,Don_t_auto_add_this_unit_to_enrolments__c = false);
        
        insert qualunit;    
        
        Id disclaimerRTID = Schema.SObjectType.Website_Disclaimer__c.getRecordTypeInfosByName().get('Website Disclaimer').getRecordTypeId();

        Website_Disclaimer__c disclaimer = new Website_Disclaimer__c(
                WDO_Disclaimer__c = 'blah',
                WDO_Funding_Type__c = 'type',
                WDO_State__c = 'QLD',
                RecordTypeId = disclaimerRTID
            );
        insert disclaimer;
        
        Id disclaimerOptionRTID = Schema.SObjectType.Website_Disclaimer__c.getRecordTypeInfosByName().get('Website Disclaimer Option').getRecordTypeId();
        
        Website_Disclaimer__c disclaimerOption = new Website_Disclaimer__c(
                WDO_Disclaimer__c = 'blah',
                WDO_Funding_Type__c = 'type',
                WDO_State__c = 'QLD',
                RecordTypeId = disclaimerOptionRTID
            );
        insert disclaimerOption;
        
        
        wdIds.add(disclaimerOption.Id);
        
    }
           
      
     @isTest static void testImportCourses() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importCourses(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
         
        Test.startTest();
         CMSIntegrationController.resetUpdateRequiredFlagQual(qualIds);
        Test.StopTest();
         
        System.assertEquals([Select Website_Update_Required__c From Qualification__c where id in : qualIds LIMIT 1].Website_Update_Required__c, false);
        
    } 

    @isTest static void testimportDeliveryModes() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importDeliveryModes(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
    }
    
    @isTest static void testimportProviders() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importProviders(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
    }
    
    @isTest static void testimportUnits() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importUnits(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
    }
    
    @isTest static void testimportDisclaimerOptions() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importDisclaimerOptions(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
    }
    
    @isTest static void testimportCourseDisclaimers() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CMSMockHttpResponseGenerator());
         
         
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = CMSIntegrationController.importCourseDisclaimers(log.Id,qualIds, false);
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
    }
    
    @isTest static void testSingleDisclaimerOptions() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
        
        HttpResponse res = new HttpResponse();
        
        res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        // 
        test.startTest();
        	CMSIntegrationController.importSingleDisclaimerOption(log.Id,wdIds, false);
            CMSIntegrationController.resetUpdateRequiredFlagWD(wdIds);
        test.stopTest();
        
        // Verify response received contains fake values
        String actualValue = res.getBody();
        String expectedValue = '{"status":"ok","message":"0 courses imported and 91 course updated"}';
        System.assertEquals(actualValue, expectedValue);
        
        System.assertEquals([Select Website_Update_Required__c From Website_Disclaimer__c where id in : wdIds LIMIT 1].Website_Update_Required__c, false);
        
    }
    
     @isTest static void testSingleDisclaimerOptionTrigger() {
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log; 
        
        insertTestData();
         
        Website_Disclaimer__c wd = [Select Website_Live_Upsert_Required__c,Website_Update_Required__c from Website_Disclaimer__c where id in :wdIds And Website_Update_Required__c = false LIMIT 1];
        
        wd.Website_Update_Required__c = true;
        
        update wd;
        
    }
    
    
    
    @istest static void testPostMethod() {
        
        insertTestData();
        
        CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
        insert log;
        
        test.startTest();
        	CMSIntegrationController.post(log.Id, null, false);
        test.stopTest();
        
    }
       
  
  
  


}