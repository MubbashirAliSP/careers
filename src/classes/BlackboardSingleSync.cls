public with sharing class BlackboardSingleSync {
  
  private final Blackboard_Sync_Event__c event;
  public Boolean showResyncButton {get;set;}
  
  
  
  public BlackboardSingleSync(ApexPages.StandardController stdController) {
        
         List<String> fields = new List<String>();
         fields.add('Integration_point__c');
         fields.add('Record_Id__c');
         
       if(!Test.isRunningTest()) 
        stdcontroller.addFields(fields);
       
       
       this.event = (Blackboard_Sync_Event__c)stdController.getRecord();

       if(event.Integration_point__c == 'SaveUser' || event.Integration_point__c == 'SaveCourse' || event.Integration_point__c == 'SaveCourseMembership' || event.Integration_point__c == 'createOrg' || event.Integration_point__c == 'updateOrg')
          showResyncButton = true;
        else 
          showResyncButton = false;
     
  }
  
  @future(callout=true) private static void blackboardCallOut(Id eId) {
    //Re-query for call out
    Blackboard_Sync_Event__c e = [Select Record_Id__c, Integration_Point__c From Blackboard_Sync_Event__c Where id = : eId ];
    
   if(e.Integration_point__c == 'SaveUser') {
   		
   	  Blackboard_User__c bbuser = [Select Available_After_Sync__c,Student_Identifier__c,create__c, update__c, deactivate__c, reactivate__c, Birthdate__c,Last_Name__c,First_Name__c,Email__c,Mobile_Phone__c,Blackboard_Primary_Key__c,Blackboard_Username__c, Blackboard_Password__c From Blackboard_User__c Where id = : e.Record_Id__c];
   	  
      if(!Test.isRunningTest())
        BlackboardUserWS.saveUser(bbuser);
      
   }
   
    if(e.Integration_point__c == 'SaveCourseMembership') {
   		
   	  Blackboard_Course_Membership__c bbcm = [Select Blackboard_Primary_Key__c, Available_After_Sync__c, Blackboard_Course__r.Blackboard_Primary_Key__c,Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c, Name, create__c, deactivate__c,reactivate__c From Blackboard_Course_Membership__c Where id = : e.Record_Id__c];
   	  
      if(!Test.isRunningTest())
        BlackboardCourseMembershipWS.saveCourseMembership(bbcm);
      
   }
   
    if(e.Integration_point__c == 'SaveCourse') {
   		
   	  Blackboard_Course__c bbcourse = [Select Available_After_Sync__c, Course_Description__c, SIS_Sync_Attempted__c, Date_of_Last_Sync__c, Blackboard_Primary_Key__c, RecordType.DeveloperName,Id, Name, create__c,deactivate__c,Reactivate__c,Update__c, Blackboard_Course_Name__c, Available_In_BlackBoard__c From Blackboard_Course__c Where id = : e.Record_Id__c];
   	  
      //if(!Test.isRunningTest())
        BlackboardCourseWS.saveCourse(bbcourse);
      
   }
   
   if(e.Integration_point__c == 'createOrg') {
   		
   	  Blackboard_Course__c bbcourse = [Select Available_After_Sync__c, Course_Description__c, SIS_Sync_Attempted__c, Date_of_Last_Sync__c, Blackboard_Primary_Key__c, RecordType.DeveloperName,Id, Name, create__c,deactivate__c,Reactivate__c,Update__c, Blackboard_Course_Name__c, Available_In_BlackBoard__c From Blackboard_Course__c Where id = : e.Record_Id__c];
   	  
      //if(!Test.isRunningTest())
        BlackboardCourseWS.createOrg(bbcourse);
      
   }
   
   if(e.Integration_point__c == 'updateOrg') {
   		
   	  Blackboard_Course__c bbcourse = [Select Available_After_Sync__c, Course_Description__c, SIS_Sync_Attempted__c, Date_of_Last_Sync__c, Blackboard_Primary_Key__c, RecordType.DeveloperName,Id, Name, create__c,deactivate__c,Reactivate__c,Update__c, Blackboard_Course_Name__c, Available_In_BlackBoard__c From Blackboard_Course__c Where id = : e.Record_Id__c];
   	  
      //if(!Test.isRunningTest())
        BlackboardCourseWS.updateOrg(bbcourse);
      
   }
   
  }
  
  
     public PageReference singleSync() {
     
      HttpResponse initResponse; //initialize response
      HttpResponse loginReponse; //login response
        
        String sessionId = '';
        String result='';
      
      BlackboardcontextWS contextWS = new BlackboardcontextWS();

      if(!Test.isRunningTest()) {
      
          initResponse = contextWS.initialize();
          
          XmlStreamReader reader = initResponse.getXmlStreamReader();
      
           while(reader.hasNext()) {
               //  Start at the beginning of the book and make sure that it is a book
               if (reader.getEventType() == XmlTag.START_ELEMENT) {
                  if ('return' == reader.getLocalName()) {
                     sessionId = BlackboardUtility.parseResult(reader);             
                     break;
                  }
               }
                  reader.next();
             }
          
          loginReponse = contextWS.login(Blackboard_Configuration__c.getInstance('WS_Config Sandbox').userid__c, Blackboard_Configuration__c.getInstance('WS_Config Sandbox').password__c, 'YG','YG','', Integer.ValueOf(Blackboard_Configuration__c.getInstance('WS_Config Sandbox').expectedLifeSeconds__c), sessionId);
      
      }
      
      /*need to sync correct sobject based on the integration point */
      
      BlackboardUtility.createBlackBoardLog(SessionId);
      
         
      blackboardCallOut(event.Id);
      
        
        
      return new PageReference('/apex/BlackboardSingleSync');
      
    
    
  }
  
  public pageReference GoToRecord() {
    
    return new PageReference('/' + event.Record_Id__c);
  }
        

}