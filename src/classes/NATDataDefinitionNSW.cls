public class NATDataDefinitionNSW {
   
    // NAT00010 - File not reported for NSW
    // NAT00020 - File not reported for NSW
    // NAT00030 - File not reported for NSW
    
    // NAT00120 -  Enrolment Unit Object Fields
    public static final String FeeAmount = 'FeeAmount'; // value: 0000
    public static final String EquityFlag = 'EquityFlag'; // value: N
    public static final String BookingId = 'Enrolment__r.Booking_ID__c';
    public static final String CourseSiteId = 'Course_Site_ID__c';
    public static final String TrainingPlanDeveloped = 'Enrolment__r.Training_Plan_Developed__c';
    public static final String OutcomeIdentifierTrainingOrg = 'Enrolment__r.Enrolment_Status__c';//the value is based on enrolment status and hence added here but can be removed later
    public static final String CommitmentIdentifier = 'Enrolment__r.Commitment_Identifier__c';
    public static final String BlankField = 'BlankField';
    public static final String YearProgramCompleted = 'Year_Program_Completed__c';
    public static final String OutcomeIdentifierNational = 'AVETMISS_National_Outcome__c';
 
     public static final String ReportableQualificationHours = 'NSW_Reportable_Qualification_Hours__c';
    

    //NAT85 - Account object fields
    public static final String PostalBuildingPropertyName = 'Postal_building_property_name__c';
    public static final String PostalFlatUnitDetails = 'Postal_flat_unit_details__c';
    public static final String PostalStreetNumber = 'Postal_street_number__c';
    public static final String PostalStreetName = 'Postal_street_name__c';
    public static final String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static final String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static final String PostalPostCode = 'PersonOtherPostalCode';
    public static final String StateIdentifier='State_Identifier__c';
        
    public static final String NationalProviderNumber  = 'Account.National_Provider_Number__c';
    public static final String TrainingOrganisationNationalProviderNumber = 'Training_Organisation__r.National_Provider_Number__c';
    public static Final String AssociatedCourseIdentifier = 'Enrolment__r.Qualification__r.Associated_Course_Identifier__c';

     public static Final String ScheduledHours = 'Scheduled_Hours__c';    
    

     public static Map< Integer, NATDataElement > nat00010() {      
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00010();
         positions.put(1, new NATDataElement(NationalProviderNumber, 10, true, 'string' ) );
         
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00020() {  
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00020();
        positions.put(1, new NATDataElement(TrainingOrganisationNationalProviderNumber, 10, true, 'string' ) );

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00030() {
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00030(); 
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, FALSE, 'number' ) );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00060() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00060();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00080() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00080();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00085() {
        
       Map<Integer, NATDataElement > positions = NATDataDefinition.nat00085();
        
        positions.put(5, new NATDataElement(PostalBuildingPropertyName, 50, true, 'string') );
        positions.put(6, new NATDataElement(PostalFlatUnitDetails, 30, true, 'string') );
        positions.put(7, new NATDataElement(PostalStreetNumber, 15, true, 'string') );
        positions.put(8, new NATDataElement(PostalStreetName, 70, true, 'string' ) ); 
        positions.put(9, new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ); 
        positions.put(10, new NATDataElement(PostalSuburbLocalityTown, 50, true, 'string' ) ); 
        positions.put(11, new NATDataElement(PostalPostCode, 4, true, 'string' ) );
        positions.put(12, new NATDataElement(StateIdentifier, 2, false, 'string' ) ); 
        
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00090() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00090();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00100() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00100();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00120() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00120();
        
        positions.put(8, new NATDataElement(OutcomeIdentifierNational , 2, true, 'string') );
         positions.put(9, new NATDataElement(ScheduledHours, 4, true, 'number') );
        positions.put(17, new NATDataElement(OutcomeIdentifierTrainingOrg, 3, true, 'string') ); // set to 'TNC' for Cancelled, Withdrawn and Expired, 'D' for Deferred
        positions.put(18, new NATDataElement(BlankField, 3, true, 'string') );
        positions.put(19, new NATDataElement(BlankField, 4, true, 'string') );
        positions.put(21, new NATDataElement(CommitmentIdentifier, 12, true, 'string') );
        positions.put(22, new NATDataElement(BlankField, 3, true, 'string') );
        positions.put(23, new NATDataElement(BlankField, 4, true, 'string') );
        positions.put(24, new NATDataElement(AssociatedCourseIdentifier, 11, true, 'string') ); 

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00130() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00130();
        positions.put(4, new NATDataElement(YearProgramCompleted, 4, false, 'number') );

        return positions;
    }
    
}