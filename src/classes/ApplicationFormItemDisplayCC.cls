/**
 * @description Custom component controller for `ApplicationFormItemDisplay` component
 * @author Ranyel Maliwanag
 * @date 29.JUN.2015
 * @history
 *     13.Mar.2016 Biao Zhang - pull delivery location from portal visibility
 */
public without sharing class ApplicationFormItemDisplayCC {
    // Actual form item answer record to store the user answer
    public ApplicationFormItemAnswer__c formItemAnswerRecord {get; set;}

    // Form item record to fetch question information
    public ApplicationFormItem__c formItemRecord {get; set;}

    // Form submission status
    public Boolean isSubmitted {get; set;}

    // Form funding steam Id
    public Id fundingStreamId {get; set;}

    // Form qualification id
    public Id qualificationId {get; set;}

    // Custom select option choices for rendering dropdown-type form items
    public List<SelectOption> formChoices {
        get {
            List<SelectOption> options = new List<SelectOption>();

            // Make sure the dropdown options field has data to begin with
            if (String.isNotBlank(formItemRecord.DropdownOptions__c)) {
                // Split the options by carriage returns and new lines
                List<String> choices = formItemRecord.DropdownOptions__c.split('\r\n');
                isStandardAnswer = false;

                // Create a new option object for each non-blank parsed value
                for (String choice : choices) {
                    if (String.isNotBlank(choice)) {
                        options.add(new SelectOption(choice, choice));
                        if (formItemAnswerRecord != null && String.isNotBlank(formItemAnswerRecord.Answer__c) && formItemAnswerRecord.Answer__c == choice) {
                            isStandardAnswer = true;
                        }
                    }
                }

                if (hasOtherOptions) {
                    String otherValue = 'Other';
                    if (otherChoice != null && !isStandardAnswer) {
                        otherValue = otherChoice;
                    }
                    options.add(new SelectOption(otherValue, 'Other'));
                }
            }
            return options;
        }
        set;
    }

    public List<SelectOption> lookupChoices {
        get {
            List<SelectOption> options = new List<SelectOption>();
            Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

            if (String.isNotBlank(formItemRecord.LookupReferenceTable__c) && globalDescribe.containsKey(formItemRecord.LookupReferenceTable__c)) {
                String extraQuery = '';
                if (!String.isEmpty(formItemRecord.FieldToDisplay__c) && formItemRecord.FieldToDisplay__c.contains('.')) {
                    extraQuery += ', ' + formItemRecord.FieldToDisplay__c + ' ';
                }
                String queryString = EnrolmentsSiteUtility.getQueryString(globalDescribe.get(formItemRecord.LookupReferenceTable__c).getDescribe(), extraQuery);
                if (String.isNotBlank(formItemRecord.LookupFilter__c)) {
                    queryString += 'WHERE ' + formItemRecord.LookupFilter__c;
                }
                System.debug('queryString ::: ' + queryString);

                List<SObject> records = Database.query(queryString);

                String fieldToDisplay = (String.isNotBlank(formItemRecord.FieldToDisplay__c)) ? formItemRecord.FieldToDisplay__c : 'Name';

                for (SObject record : records) {
                    options.add(new SelectOption(String.valueOf(record.get('Id')), String.valueOf(record.get(fieldToDisplay))));
                }
            }
            return options;
        }
        set;
    }

    public List<String> multiSelectOptions {
        get {
            if (formItemAnswerRecord != null && String.isNotBlank(formItemAnswerRecord.Answer__c)) {
                multiSelectOptions = new List<String>();
                String parsingString = formItemAnswerRecord.Answer__c;
                for (String item : parsingString.split(';')) {
                    multiSelectOptions.add(item.trim());
                }
            }
            return multiSelectOptions;
        }
        set {
            multiSelectOptions = value;

            if (isMultiSelect) {
                formItemAnswerRecord.Answer__c = String.join(value, '; ');
            }
        }
    }

    public Boolean isMultiSelect {
        get {
            return formItemRecord.QuestionType__c == 'Multiple-select Dropdown';
        }
        set;
    }

    public Boolean isReadOnly {
        get {
            if (isSubmitted == null) {
                isSubmitted = false;
            }
            return formItemRecord.IsReadOnly__c || formItemAnswerRecord.IsSubmitted__c || isSubmitted;
        }
    }

    public String encryptedAnswer {
        get {
            return formItemAnswerRecord.Answer__c;
        }
        set {
            formItemAnswerRecord.Answer__c = value;
        }
    }

    public List<String> dropdownAnswer {
        get {
            return new List<String>{formItemAnswerRecord.Answer__c};
        }
        set {
            if (value != null && value.size() > 0) {
                formItemAnswerRecord.Answer__c = value[0];
            }
        }
    }

    public List<String> lookupAnswer {
        get {
            return new List<String>{formItemAnswerRecord.Answer__c};
        }
        set {
            if (value != null && value.size() > 0) {
                String currentValue = value[0];
                for (SelectOption option : lookupChoices) {
                    if (option.getValue() == currentValue) {
                        formItemAnswerRecord.Answer__c = currentValue;
                        formItemAnswerRecord.ReadableAnswer__c = option.getLabel();
                    }
                }
            }
        }
    }

    public String lookupSelectAnswer {
        get {
            return formItemAnswerRecord.Answer__c;
        }
        set {
            String currentValue = value;
            for (SelectOption option : lookupChoices) {
                if (option.getValue() == currentValue) {
                    formItemAnswerRecord.Answer__c = currentValue;
                    formItemAnswerRecord.ReadableAnswer__c = option.getLabel();
                }
            }
        }
    }

    public Boolean hasOtherOptions {
        get {
            return formItemRecord.IsOtherOptionAvailable__c;
        }
    }

    public String otherChoice {
        get {
            if (!isStandardAnswer) {
                otherChoice = formItemAnswerRecord.Answer__c;
            }
            return otherChoice;
        }
        set {
            if (formItemRecord.IsOtherOptionAvailable__c && dropdownAnswer != null && dropdownAnswer[0] == 'Other') {
                formItemAnswerRecord.Answer__c = value;
            }
        }
    }

    public Boolean isStandardAnswer {get; set;}

    public String layout {
        get {
            layout = 'lineDirection';
            if (formItemRecord.IsVerticalChoices__c) {
                layout = 'pageDirection';
            }
            return layout;
        }
        set;
    }

    public Boolean isRenderAsDropdown {
        get {
            isRenderAsDropdown = false;
            if (lookupChoices != null && lookupChoices.size() > 20) {
                isRenderAsDropdown = true;
            }
            return isRenderAsDropdown;
        }
        set;
    }

    public Boolean isAlternateAvailable {
        get {
            return String.isNotBlank(formItemAnswerRecord.ReadableAnswer__c);
        }
    }
}