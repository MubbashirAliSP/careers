/**
 * @description Test Class for ScheduleableUpdateResultStatus and UpdateResultStatus classes 
 * @author Janella Lauren Canlas
 * @date 14.NOV.2012
 */
@isTest(seeAllData=true)
private class ScheduleableUpdateResultStatus_Test {

    static testMethod void ScheduleableUpdateResultStatus_Test() {
    	Id recType = [Select Id from RecordType where Name ='National Outcome'].Id;
    	List<Results__c> resList = new List<Results__c>();
    	List<Intake__c> intakeList = new List<Intake__c>();
    	Country__c co = new Country__c();
    	State__c state = new State__c();
    	Locations__c ploc = new Locations__c();
    	Locations__c loc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Intake__c intake = new Intake__c();
		Location_Loadings__c loading = new Location_Loadings__c();
    	Test.startTest();
    	Results__c result = new Results__c();
    	result.RecordTypeId = recType;
    	result.Inactive_Date__c = date.today()-1;
    	result.Inactive__c = false;
    	insert result;
    	co = TestCoverageUtilityClass.createCountry();
    	state = TestCoverageUtilityClass.createState(co.Id);
    	loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
    	ploc = TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, loading.Id);
    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
    	ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
    	Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
    	Qualification__c qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
    	qual.Active__c = false;
    	qual.Expiry_Date__c = date.today() - 1;
    	update qual;
    	resList.add(result);
    	fundStream = TestCoverageUtilityClass.createFundingStream();
		program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
    	intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
    	intake.End_Date__c = date.today() - 1;
    	intake.Intake_Status__c = 'Active';
    	update intake;
    	intakeList.add(intake);
    	UpdateResultStatus urs = new UpdateResultStatus();
    	urs.resultsList = resList;
    	
    	String sch ='0 0 0 * * ?';
        String jobId = System.schedule('testBasicScheduledApex',sch,new ScheduleableUpdateResultStatus());
	    
	    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime 
	    					FROM CronTrigger WHERE id = :jobId];
	    System.assertEquals(sch, ct.CronExpression);
	    Test.stopTest();
    }
}