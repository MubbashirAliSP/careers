/**
 * @description Central trigger handler for the `ApplicationForm__c` object
 * @author Ranyel Maliwanag
 * @date 17.AUG.2015
 * @history
 *       17.NOV.2015    Ranyel Maliwanag    - Added check for Effectivity Date value on portal settings to determine whether or not loan forms are immediately created
 *       24.NOV.2015    Biao Zhang          - Populate Earliest/Expected loan form datetime
 *       22.DEC.2015    Biao Zhang          - Tried to send under 18 yo student loan completion email in trigger. Failed because Site Guest User cannot use getContent method in callout. Attachment pdf in email is always empty.
 *       29.DEC.2015    Biao Zhang          - Populate loan form submission date when insert loan form.
 *       19.JAN.2016    Biao zhang          - line 722, mapping IsNewZealandCitizenAndEligibleForVFH__c from Application Form Item Answer to Loan Form
 *       27.JAN.2016	Ranyel Maliwanag	- Added logic for creating Entry Requirements on Enrolment Application Form submission
 *		 03.FEB.2016	Biao Zhang          - Move update submission date logic from workflow to before update trigger
 *		 19.FEB.2016	Ranyel Maliwanag	- Removed logic for immediate creation of loan forms
 *											- Added handling of encrypted answers for Account mapping logic
 *											- Added autopopulation of Tax File Number field on the loan form
 *		 24.FEB.2016	Ranyel Maliwanag	- Added additional filters to the autopopulation of Tax File Number to prevent oversized data errors
 *		 01.MAR.2016	Ranyel Maliwanag	- Moved from custom settings to custom metadata for Service Case mappings
 *											- Added support for different funding stream types on Service Case mappings
 * 		 09.MAR.2016	Ranyel Maliwanag	- Added logic to autopopulate the StudentAccountId field on the Application Form based on the Guest Login as part of requirements for Marketing Cloud stuff
 *		 29.MAR.2016	Ranyel Maliwanag	- Added logic to create an Enrolment Application Milestone record.
 */
public without sharing class ApplicationFormTriggerHandler {
	static Id formRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
	static Id loanRTID = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId();
	static Id enrolmentRTID = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId();
	static Id llnReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	static Id nzCitizenReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('NZ Citizen').getRecordTypeId();
	static Id under18ReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Under 18').getRecordTypeId();
	static Id year12ReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Year 12 Certificate').getRecordTypeId();
	static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();

	/**
	 * @description Before insert trigger event handler definition
	 * @author Ranyel Maliwanag
	 * @date 23.AUG.2015
	 */
	public static void onBeforeInsert(List<ApplicationForm__c> records) {
		List<ApplicationForm__c> recordsForEmailPopulate = new List<ApplicationForm__c>();

		for (ApplicationForm__c record : records) {
			if (record.StudentCredentialsId__c != null && String.isBlank(record.StudentEmail__c)) {
				recordsForEmailPopulate.add(record);
			}
		}

		populateEmailField(recordsForEmailPopulate);
	}


	public static void onAfterInsert(List<ApplicationForm__c> records) {
		List<ApplicationForm__c> recordsForLoanCreation = new List<ApplicationForm__c>();

		for (ApplicationForm__c record : records) {
			if (record.RecordTypeId == loanRTID && String.isNotBlank(record.LinkedEnrolmentFormId__c)) {
				recordsForLoanCreation.add(record);
			}
		}
		createLoanForms(recordsForLoanCreation);
	}

	/**
	 * @description Before update trigger event handler definition
	 * @author Ranyel Maliwanag
	 * @date 17.AUG.2015
	 * @history
	 *		03.FEB.2016 Biao Zhang              Move the logic of update submission date from workflow to before update trigger. 
	 *											Because send under 18 yo student email is a workflow which relys on is_student_under_18__c field
	 *										    and is_student_under_18__c field is a fomular based on submission date. We cannot update the 
	 *											submission date in workflow because we cannot make sure the send email workflow always runs after the update submission date workflow.  
	 */
	public static void onBeforeUpdate(List<ApplicationForm__c> records, Map<Id, ApplicationForm__c> recordsMap) {
		List<ApplicationForm__c> submittedEnrolmentForms = new List<ApplicationForm__c>();

		for (ApplicationForm__c record : records) {
			if (record.Status__c == 'Submitted' && recordsMap.get(record.Id).Status__c != 'Submitted') {
				if (record.SubmissionDate__c == null) {
					record.SubmissionDate__c = Datetime.now();
				}

				if(record.RecordTypeId == enrolmentRTID && String.isBlank(record.ServiceCaseId__c) && record.IsMainApplicationForm__c) {
					submittedEnrolmentForms.add(record);
				}
			}
		}

		createFormServiceCases(submittedEnrolmentForms);
	}


	/**
	 * @description After update trigger event handler definition
	 * @author Ranyel Maliwanag
	 * @date 02.AUG.2015
	 */
	public static void onAfterUpdate(List<ApplicationForm__c> records, Map<Id, ApplicationForm__c> recordsMap) {
		List<ApplicationForm__c> submittedLoanForms = new List<ApplicationForm__c>();

		for (ApplicationForm__c record : records) {
			if (record.Status__c == 'Submitted' && recordsMap.get(record.Id).Status__c != 'Submitted' && record.RecordTypeId == loanRTID && String.isNotBlank(record.ServiceCaseId__c)) {
				submittedLoanForms.add(record);
			}
		}

		submitServiceCases(submittedLoanForms);
	}


	/**
	 * @description Creates a service case record for the submitted forms
	 * @author Ranyel Maliwanag
	 * @date 17.AUG.2015
	 * @history
	 *       17.NOV.2015    Ranyel Maliwanag    - Added check for Effectivity Date value on portal settings to determine whether or not loan forms are immediately created
	 *       24.NOV.2015    Biao Zhang          - Populate earliest loan form date and expected loan form date based on portal setting
	 *		 01.MAR.2016	Ranyel Maliwanag	- Moved from Applications Service Case Mappings custom setting to metadata
	 *											- Added support for Funding Stream-specific mappings
	 */
	private static void createFormServiceCases(List<ApplicationForm__c> records) {
		// Mapping of <Student Credentials Id> : <Student Credentials Record>
		Map<Id, GuestLogin__c> credentialsMap = new Map<Id, GuestLogin__c>();

		// Mapping of <Application Form Id> : <Service Case Record>
		Map<ApplicationForm__c, SObject> formServiceCaseMap = new Map<ApplicationForm__c, SObject>();

		// Mapping of <Application Form Id> : <Eligibility Record>
		Map<Id, Eligibility__c> formEligibilityMap = new Map<Id, Eligibility__c>();

		// Mapping of <Application Form Id> : <List of Entry Requirement Records to be created>
		Map<Id, List<EntryRequirement__c>> formReqsMap = new Map<Id, List<EntryRequirement__c>>();

		// Reference queue for the service case that gets created
		Group awaitingLoanFormQueue = [SELECT Id FROM Group WHERE DeveloperName = 'IncompleteApplications' AND Type = 'Queue'];
		Set<Id> credentialIds = new Set<Id>();
		Set<Id> formIds = new Set<Id>();

		for (ApplicationForm__c record : records) {
			credentialIds.add(record.StudentCredentialsId__c);
			formIds.add(record.Id);

			if (String.isNotBlank(record.LinkedDoubleEnrolmentFormId__c)) {
				formIds.add(record.LinkedDoubleEnrolmentFormId__c);
			}
		}

		// Build list of extra fields that needs to be queried from the Application Form object based on several mapping pieces
		// Extra fields are mostly lookup stuff
		Set<String> extrasSet = new Set<String>();
		String extras = ', ';
		List<ApplicationFormServiceCaseMapping__mdt> serviceCaseMappings = [SELECT FundingStream__c , SourceField__c, SourceObject__c, TargetField__c, IsUsingReadableAnswer__c, IsTranslateBooleanDropdowns__c FROM ApplicationFormServiceCaseMapping__mdt];
		//Map<String, ApplicationsServiceCaseFieldMappings__c> mappingSettings = ApplicationsServiceCaseFieldMappings__c.getAll();
		Map<String, ApplicationStudentAccountMappings__c> accountMappingSettings = ApplicationStudentAccountMappings__c.getAll();
		for (ApplicationFormServiceCaseMapping__mdt mapping : serviceCaseMappings) {
			if (mapping.SourceObject__c == 'ApplicationForm__c' && mapping.SourceField__c.contains('.')) {
				extrasSet.add(mapping.SourceField__c);
			}
		}
		// Ensure that Funding Stream Short name is queried
		extrasSet.add('FundingStreamTypeId__r.FundingStreamShortname__c');
		extras += String.join(new List<String>(extrasSet), ', ');

		// Build subQueryString for fetching related ApplicationFormItemAnswer__c records
		String subQueryString = ', (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationFormItemAnswer__c, ', ApplicationFormItemId__r.Name ');
		subQueryString = subQueryString.replace('ApplicationFormItemAnswer__c', 'ApplicationFormItemAnswers__r');
		subQueryString += ')';
		extras += subQueryString;

		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, extras);
		queryString += 'WHERE Id IN :formIds';
		List<ApplicationForm__c> formsInfo = Database.query(queryString);
		Map<Id, ApplicationForm__c> formsInfoMap = new Map<Id, ApplicationForm__c>();

		for (ApplicationForm__c formInfo : formsInfo) {
			formsInfoMap.put(formInfo.Id, formInfo);
		}


		List<GuestLogin__c> credentials = [SELECT Id, AccountId__c FROM GuestLogin__c WHERE Id IN :credentialIds];
		for (GuestLogin__c credential : credentials) {
			credentialsMap.put(credential.Id, credential);
		}

		// Get a mapping of record funding stream short name for Entry Requirement creation purpose
		Map<Id, String> formFundingMap = new Map<Id, String>();
		for (ApplicationForm__c form : formsInfo) {
			formFundingMap.put(form.Id, form.FundingStreamTypeId__r.FundingStreamShortname__c);
		}

		List<SObject> accountsForUpdate = new List<SObject>();
		for (ApplicationForm__c record : records) {
			ApplicationForm__c recordReference = formsInfoMap.get(record.Id);

			Map<String, ApplicationFormItemAnswer__c> answersMap = new Map<String, ApplicationFormItemAnswer__c>();
			for (ApplicationFormItemAnswer__c answer : recordReference.ApplicationFormItemAnswers__r) {
				answersMap.put(answer.ApplicationFormItemId__r.Name, answer);
			}

			// Create service case record
			SObject formServiceCase = Service_Cases__c.SObjectType.newSObject();
			formServiceCase.put('OwnerId', awaitingLoanFormQueue.Id);

			// Populate service case fields based on custom metadata
			for (ApplicationFormServiceCaseMapping__mdt mapping : serviceCaseMappings) {
				// Check if funding stream of mapping is applicable to the form
				if (mapping.FundingStream__c.contains('All') || (String.isNotBlank(recordReference.FundingStreamTypeId__r.FundingStreamShortname__c) && mapping.FundingStream__c.contains(recordReference.FundingStreamTypeId__r.FundingStreamShortname__c))) {
					// Condition: Service Case mapping is applicable to all Funding Streams OR is for the Funding Stream of the current Application Form

					if (mapping.SourceObject__c == 'ApplicationForm__c') {
						Object value = EnrolmentsSiteUtility.getReferenceFieldValue(recordReference, mapping.SourceField__c);
						formServiceCase.put(mapping.TargetField__c, value);
					} else if (mapping.SourceObject__c == 'RecordType') {
						Id recordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get(mapping.SourceField__c).getRecordTypeId();
						formServiceCase.put('RecordTypeId', recordTypeId);
					} else if (mapping.SourceObject__c == 'String') {
						String value = mapping.SourceField__c;
						formServiceCase.put(mapping.TargetField__c, value);
					} else {
						for (String sourceField : mapping.SourceField__c.split(',')) {
							// Remove whitespaces
							sourceField = sourceField.trim();

							// Check if the sourceField is in the answers map
							if (answersMap.containsKey(sourceField)) {
								Object value = answersMap.get(sourceField).Answer__c;

								// Special answers handling
								if (answersMap.get(sourceField).DateAnswer__c != null) {
									// Date answers
									value = answersMap.get(sourceField).DateAnswer__c;
								}

								if (mapping.IsUsingReadableAnswer__c && String.isNotBlank(answersMap.get(sourceField).ReadableAnswer__c)) {
									value = answersMap.get(sourceField).ReadableAnswer__c;
								}

								if (answersMap.get(sourceField).Answer__c == 'Yes') {
									value = true;
								} else if (answersMap.get(sourceField).Answer__c == 'No') {
									value = false;
								}

								if (mapping.IsTranslateBooleanDropdowns__c) {
									if (answersMap.get(sourceField).Answer__c == 'Yes') {
										value = true;
									} else if (answersMap.get(sourceField).Answer__c == 'No' || String.isBlank(answersMap.get(sourceField).Answer__c)) {
										value = false;
									}
								}

								formServiceCase.put(mapping.TargetField__c, value);
								break;
							}
						}
					}
				}
			}
			formServiceCaseMap.put(record, formServiceCase);

			// Update account record
			if (credentialsMap.containsKey(recordReference.StudentCredentialsId__c)) {
				SObject accountRecord = Account.SObjectType.newSObject(credentialsMap.get(recordReference.StudentCredentialsId__c).AccountId__c);
				System.debug('accountRecord ::: ' + accountRecord);
				for (ApplicationStudentAccountMappings__c mapping : accountMappingSettings.values()) {
					System.debug('account mapping ::: ' + mapping);
					if (mapping.SourceObject__c == 'ApplicationForm__c') {
						Object value = EnrolmentsSiteUtility.getReferenceFieldValue(recordReference, mapping.SourceField__c);
						accountRecord.put(mapping.Name, value);
					} else if (mapping.SourceObject__c == 'ApplicationFormItemAnswer__c') {
						for (String sourceField : mapping.SourceField__c.split(',')) {
							// Remove whitespaces
							sourceField = sourceField.trim();

							// Check if the sourceField is in the answers map
							if (answersMap.containsKey(sourceField)) {
								Object value = answersMap.get(sourceField).Answer__c;

								// Special answers handling
								if (answersMap.get(sourceField).DateAnswer__c != null) {
									// Date answers
									value = answersMap.get(sourceField).DateAnswer__c;
								}

								if (answersMap.get(sourceField).EncryptedAnswer__c != null) {
									// Encrypted answers
									value = answersMap.get(sourceField).EncryptedAnswer__c;
								}

								// Check if a non-null answer exists
								if (value != null) {
									// Boolean dropdowns handling
									if (mapping.IsTranslateBooleanDropdowns__c) {
										if (String.valueOf(value) == 'Yes') {
											value = true;
										} else if (String.valueOf(value) == 'No') {
											value = false;
										}
									}

									if (String.isNotBlank(mapping.DropdownTranslations__c)) {
										List<String> translations = mapping.DropdownTranslations__c.split('\r\n');
										// Mapping of <Form Answer> : <Record Value>
										Map<Object, String> answerTranslationMap = new Map<Object, String>();

										for (String translation : translations) {
											translation = translation.trim();
											String formAnswer = translation.split(':')[0].trim();
											String fieldValue = translation.split(':')[1].trim();
											answerTranslationMap.put(formAnswer, fieldValue);
										}

										if (answerTranslationMap.containsKey(value)) {
											value = answerTranslationMap.get(value);
										}
									}

									System.debug(mapping.Name + ' ::: ' + value);
									accountRecord.put(mapping.Name, value);
									break;
								}
							}
						}
					}
				}
				accountsForUpdate.add(accountRecord);
			}

			// Create eligibility record
			Eligibility__c eligibility = new Eligibility__c(
					CitizenshipResidentTypeId__c = answersMap.containsKey('ResidencyStatus') ? answersMap.get('ResidencyStatus').Answer__c : null,
					CountryId__c = answersMap.containsKey('SpecifyCountryOfBirth') ? answersMap.get('SpecifyCountryOfBirth').Answer__c : null,
					DocumentNumber__c = answersMap.containsKey('ProofDocumentNumber') ? answersMap.get('ProofDocumentNumber').Answer__c : null,
					DocumentType__c = answersMap.containsKey('ProofType') ? answersMap.get('ProofType').Answer__c : null,
					PlaceOfBirth__c = answersMap.containsKey('PlaceOfBirth') ? answersMap.get('PlaceOfBirth').Answer__c : null,
					StateOfBIrth__c = answersMap.containsKey('SpecifyStateOfBirth') ? answersMap.get('SpecifyStateOfBirth').Answer__c : null,
					IsBornInAustralia__c = (answersMap.containsKey('CountryOfBirth') && answersMap.get('CountryOfBirth').Answer__c == 'Yes') ? true : false,
					YearOfArrival__c = answersMap.containsKey('YearOfArrival') ? answersMap.get('YearOfArrival').Answer__c : null
				);
			formEligibilityMap.put(record.Id, eligibility);


			// Create entry requirement records based on satisfied criteria
			Boolean isSupplyingYear12Cert = answersMap.containsKey('SupplyingYear12Cert') ? Boolean.valueOf(answersMap.get('SupplyingYear12Cert').Answer__c == 'Yes') : false;
			Boolean isUnder18 = answersMap.containsKey('DOB') ? (answersMap.get('DOB').DateAnswer__c.monthsBetween(Date.today()) / 12 < 18) : false;
			Boolean isYear12CompletedLastYear = answersMap.containsKey('Year12CompletedLastYear') ? Boolean.valueOf(answersMap.get('Year12CompletedLastYear').Answer__c == 'Yes') : false;

			List<EntryRequirement__c> requirements = new List<EntryRequirement__c>();
			if(formFundingMap.get(record.Id) == null || formFundingMap.get(record.Id) == 'VFH') {
				Boolean isNZCitizen = answersMap.containsKey('ResidencyStatus') ? Boolean.valueOf(answersMap.get('ResidencyStatus').ReadableAnswer__c == 'New Zealand citizen') : false;

				if (isSupplyingYear12Cert) {
					// Create Year 12 Entry Requirement
					EntryRequirement__c req = new EntryRequirement__c(
							RecordTypeId = year12ReqRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload'
						);
					requirements.add(req);

					// Create LLN Entry Requirement
					EntryRequirement__c yr12ER = new EntryRequirement__c(
							RecordTypeId = llnReqRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'LLN - Pending YR12 Cert'
						);
					requirements.add(yr12ER);
				} else {
					// Create LLN Entry Requirement
					EntryRequirement__c req = new EntryRequirement__c(
							RecordTypeId = llnReqRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'LLN - Pending'
						);
					requirements.add(req);
				}
				if (isNZCitizen) {
					// Create NZ Citizen Entry Requirement
					EntryRequirement__c req = new EntryRequirement__c(
							RecordTypeId = nzCitizenReqRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload'
						);
					requirements.add(req);
				}
				if (isUnder18) {
					// Create Under 18 Entry Requirement
					EntryRequirement__c req = new EntryRequirement__c(
							RecordTypeId = under18ReqRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload'
						);
					requirements.add(req);
				}
			} else if (formFundingMap.get(record.Id) == 'C3G') {
				Boolean isNZCitizen = answersMap.containsKey('ResidencyStatusC3G') ? Boolean.valueOf(answersMap.get('ResidencyStatusC3G').Answer__c == 'New Zealand citizen') : false;
				Boolean isAUCitizen = answersMap.containsKey('ResidencyStatusC3G') ? Boolean.valueOf(answersMap.get('ResidencyStatusC3G').Answer__c == 'Australian citizen') : false;
				Boolean isHumanitarian = answersMap.containsKey('ResidencyStatusC3G') ? Boolean.valueOf(answersMap.get('ResidencyStatusC3G').Answer__c == 'Students/Applicants with permanent humanitarian visa') : false;
				Boolean isPRorTR = answersMap.containsKey('ResidencyStatusC3G') ? Boolean.valueOf(answersMap.get('ResidencyStatusC3G').Answer__c == 'Temporary or Permanent Australian Residency') : false;

				EntryRequirement__c dob = new EntryRequirement__c(
						RecordTypeId = c3gRTID,
						ApplicationFormId__c = record.Id,
						Status__c = 'Awaiting Upload',
						Category__c = 'Date of Birth'
					);
				requirements.add(dob);

				EntryRequirement__c residencyProof = new EntryRequirement__c(
						RecordTypeId = c3gRTID,
						ApplicationFormId__c = record.Id,
						Status__c = 'Awaiting Upload',
						Category__c = 'Proof of Queensland Residency'
					);
				requirements.add(residencyProof);

				if(isAUCitizen || isNZCitizen) {
					EntryRequirement__c citizenProof = new EntryRequirement__c(
							RecordTypeId = c3gRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload',
							Category__c = 'Proof of Australian / NZ Citizenship'
						);
					requirements.add(citizenProof);
				}

				if(isHumanitarian || isPRorTR) {
					EntryRequirement__c residencyAUProof = new EntryRequirement__c(
							RecordTypeId = c3gRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload',
							Category__c = 'Proof of Australia Residency - Permanent / Temporary'
						);
					requirements.add(residencyAUProof);
				}

				if(isYear12CompletedLastYear) {
					EntryRequirement__c y12Proof = new EntryRequirement__c(
							RecordTypeId = c3gRTID,
							ApplicationFormId__c = record.Id,
							Status__c = 'Awaiting Upload',
							Category__c = 'Fee Free Training Yr 12'
						);
					requirements.add(y12Proof);
				}
			}
			
			formReqsMap.put(record.Id, requirements);
		}
		insert formServiceCaseMap.values();

		// Fix for Custom Validation: `Account.Arrival_Status_Valiadtion_2`
		for (Account record : (List<Account>) accountsForUpdate) {
			if (String.isNotBlank(record.Year_of_Arrival_in_Australia__c)) {
				record.Arrival_Status__c = '**** : Known Year of Arrival';
			}
		}

		if (!Test.isRunningTest()) {
			update accountsForUpdate;
		}

		// Update eligibility records with service case Ids
		for (ApplicationForm__c record : formServiceCaseMap.keySet()) {
			if (formEligibilityMap.containsKey(record.Id)) {
				formEligibilityMap.get(record.Id).ServiceCaseId__c = formServiceCaseMap.get(record).Id;
			}

			if (formReqsMap.containsKey(record.Id)) {
				for (EntryRequirement__c requirement : formReqsMap.get(record.Id)) {
					requirement.ServiceCaseId__c = formServiceCaseMap.get(record).Id;
				}
			}
		}
		insert formEligibilityMap.values();

		List<EntryRequirement__c> entryReqsForInsert = new List<EntryRequirement__c>();
		for (List<EntryRequirement__c> reqsList : formReqsMap.values()) {
			entryReqsForInsert.addAll(reqsList);
		}
		insert entryReqsForInsert;

		// Mapping of <Enrolment Application Form> : <Loan Application Form>
		Map<ApplicationForm__c, ApplicationForm__c> linkedEnrolmentLoanForms = new Map<ApplicationForm__c, ApplicationForm__c>();
		// List of Linked double enrolment forms for separate updating
		List<ApplicationForm__c> doubleEnrolmentForms = new List<ApplicationForm__c>();

		// Mapping of <Linked Double Enrolment Form Id> : <Application Form Record>
		Map<Id, ApplicationForm__c> formIdMap = new Map<Id, ApplicationForm__c>();
		Set<Id> linkedFormIds = new Set<Id>();

		for (ApplicationForm__c record : records) {
			if (String.isNotBlank(record.LinkedDoubleEnrolmentFormId__c)) {
				linkedFormIds.add(record.LinkedDoubleEnrolmentFormId__c);
			}
		}

		ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
		Boolean isLoanFormImmediate = false && portalSettings != null && (portalSettings.EffectivityDate__c > Date.today() || portalSettings.LoanApplicationFormCreationOffset__c == 0);

		for (ApplicationForm__c record : formServiceCaseMap.keySet()) {
			record.ServiceCaseId__c = formServiceCaseMap.get(record).Id;
		}

		if (isLoanFormImmediate) {
			insert linkedEnrolmentLoanForms.values();

			for (ApplicationForm__c record : linkedEnrolmentLoanForms.keySet()) {
				record.LinkedLoanFormId__c = linkedEnrolmentLoanForms.get(record).Id;
			}
		}
		update doubleEnrolmentForms;

		populateLoanFormDate(records);
	}


	/**
	 * @description Populates the StudentEmail field of the Application Form records with the related student credential emails
	 * @author Ranyel Maliwanag
	 * @date 23.AUG.2015
	 */
	private static void populateEmailField(List<ApplicationForm__c> records) {
		// Mapping of <Student Credential Id> : <Student Email>
		Map<Id, GuestLogin__c> credentialsRecordMap = new Map<Id, GuestLogin__c>();
		Set<Id> credentialIds = new Set<Id>();

		for (ApplicationForm__c record : records) {
			credentialIds.add(record.StudentCredentialsId__c);
		}

		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c);
		queryString += 'WHERE Id IN :credentialIds';
		List<GuestLogin__c> credentials = Database.query(queryString);

		for (GuestLogin__c credential : credentials) {
			credentialsRecordMap.put(credential.Id, credential);
		}

		for (ApplicationForm__c record : records) {
			if (credentialsRecordMap.containsKey(record.StudentCredentialsId__c)) {
				record.StudentEmail__c = credentialsRecordMap.get(record.StudentCredentialsId__c).Email__c;
				record.StudentAccountId__c = credentialsRecordMap.get(record.StudentCredentialsId__c).AccountId__c;
			}
		}
	}


	/**
	 * @description Updates the service case records to submitted when the loan application forms get submitted
	 * @author Ranyel Maliwanag
	 * @date 24.AUG.2015
	 * @history
	 *		 29.MAR.2016	Ranyel Maliwanag	- Added logic to create an Enrolment Application Milestone record.
	 */
	private static void submitServiceCases(List<ApplicationForm__c> records) {
		Set<Id> serviceCaseIds = new Set<Id>();
		List<EnrolmentApplicationMilestone__c> milestonesForInsert = new List<EnrolmentApplicationMilestone__c>();

		for (ApplicationForm__c record : records) {
			serviceCaseIds.add(record.ServiceCaseId__c);
			EnrolmentApplicationMilestone__c milestone = new EnrolmentApplicationMilestone__c(
					ServiceCaseId__c = record.ServiceCaseId__c,
					ApplicationFormId__c = record.Id,
					MilestoneEndDate__c = DateTime.now()
				);
			milestonesForInsert.add(milestone);
		}

		String extras = ', (SELECT Id, Status__c FROM ApplicationForms__r WHERE RecordType.Name = \'Loan\')';
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Service_Cases__c, extras);
		queryString += 'WHERE Id IN :serviceCaseIds';
		List<Service_Cases__c> serviceCases = Database.query(queryString);

		for (Service_Cases__c serviceCase : serviceCases) {
			if (serviceCase.ApplicationForms__r != null && serviceCase.ApplicationForms__r.size() > 0) {
				Integer formsSize = serviceCase.ApplicationForms__r.size();
				Integer submittedSize = 0;
				
				for (ApplicationForm__c applicationForm : serviceCase.ApplicationForms__r) {
					if (applicationForm.Status__c == 'Submitted') {
						submittedSize++;
					}
				} 

				if (submittedSize == formsSize) {
					serviceCase.Related_Form_Status__c = 'Submitted';
				} else if (formsSize > 1 && submittedSize < formsSize) {
					serviceCase.Related_Form_Status__c = 'Awaiting Other Loan Form Submission';
				}
			}
		}

		insert milestonesForInsert;
		update serviceCases;
	}


	/**
	 * @description Creates a linked Loan Form record for the newly created Loan Application Forms
	 * @author Ranyel Maliwanag
	 * @date 27.AUG.2015
	 * @wip
	 */
	private static void createLoanForms(List<ApplicationForm__c> records) {
		// Mapping of <Application Form Record> : <Loan Form Record>
		// For mapping of LoanFormId__c field to the appropriate Loan Form record when inserted
		Map<ApplicationForm__c, LoanForm__c> formLoanMap = new Map<ApplicationForm__c, LoanForm__c>();
		// Mapping of <Application Form Id> : <Application Form record>
		// Used as a reference table
		Map<Id, ApplicationForm__c> referenceMap = new Map<Id, ApplicationForm__c>();
		Set<Id> recordIds = new Set<Id>();

		for (ApplicationForm__c record : records) {
			recordIds.add(record.LinkedEnrolmentFormId__c);
		}

		List<String> courseAmountFields = new List<String> {
			'QualificationId__r.Course_Amount_ACT__c',
			'QualificationId__r.Course_Amount_NSW__c',
			'QualificationId__r.Course_Amount_NT__c',
			'QualificationId__r.Course_Amount_QLD__c',
			'QualificationId__r.Course_Amount_SA__c',
			'QualificationId__r.Course_Amount_TAS__c',
			'QualificationId__r.Course_Amount_VIC__c',
			'QualificationId__r.Course_Amount_WA__c'
		};
		String extras = ', ' + String.join(courseAmountFields, ', ');

		// Build subQueryString for fetching related ApplicationFormItemAnswer__c records
		String subQueryString = ', QualificationId__r.Anticipated_Duration_In_Months__c, StateId__r.Short_Name__c, (' + EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationFormItemAnswer__c, ', ApplicationFormItemId__r.Name ');
		subQueryString = subQueryString.replace('ApplicationFormItemAnswer__c', 'ApplicationFormItemAnswers__r');
		subQueryString += '), StudentCredentialsId__r.AccountId__r.Tax_File_Number__c';
		extras += subQueryString;

		System.debug('recordIds ::: ' + recordIds);

		// Build reference map
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, extras);
		queryString += 'WHERE Id IN :recordIds';
		for (ApplicationForm__c record : Database.query(queryString)) {
			referenceMap.put(record.Id, record);
		}

		System.debug('referenceMap ::: ' + referenceMap);

		for (ApplicationForm__c record : records) {
			// Set up the reference record
			ApplicationForm__c recordReference = referenceMap.get(record.LinkedEnrolmentFormId__c);
			System.debug('recordReference ::: ' + recordReference);

			// Build Answer reference map
			Map<String, ApplicationFormItemAnswer__c> answerReferenceMap = new Map<String, ApplicationFormItemAnswer__c>();
			for (ApplicationFormItemAnswer__c answer : recordReference.ApplicationFormItemAnswers__r) {
				answerReferenceMap.put(answer.ApplicationFormItemId__r.Name, answer);
			}
			system.debug('answermap');
			for(String s : answerReferenceMap.keySet()){
				system.debug(s);
				system.debug(answerReferenceMap.get(s));
			}
			system.debug('anser map end');

			Integer courseAmount = null;
			if (String.isNotBlank(recordReference.StateId__c) && String.isNotBlank(recordReference.StateId__r.Short_Name__c) && recordReference.StateId__r.Short_Name__c != 'OTHER' && recordReference.StateId__r.Short_Name__c != 'OVERSEAS') {
				String courseAmountField = 'QualificationId__r.Course_Amount_' + recordReference.StateId__r.Short_Name__c + '__c';
				courseAmount = Integer.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue((SObject) recordReference, courseAmountField));
			}

			LoanForm__c loanForm = new LoanForm__c(
					FamilyName__c = record.LastName__c,
					GivenNames__c = record.FirstName__c,
					DateOfBirth__c = (answerReferenceMap.containsKey('DOB')) ? answerReferenceMap.get('DOB').DateAnswer__c : null,
					ApprovedTrainingProvider__c = 'Careers Australia',
					EstimatedCourseDuration__c = recordReference.QualificationId__r.Anticipated_Duration_In_Months__c + ' months',
					VETCourse__c = record.ParentQualificationName__c,
					StudentIdentifier__c = record.StudentIdentifier__c,
					Gender__c = (answerReferenceMap.containsKey('Gender')) ? answerReferenceMap.get('Gender').Answer__c : null,
					DeliveryLocation__c = (answerReferenceMap.containsKey('DeliveryLocation')) ? answerReferenceMap.get('DeliveryLocation').Answer__c : null,
					VETCourseCode__c = record.ParentQualificationCode__c,
					IsAustralianCitizen__c = (answerReferenceMap.containsKey('ResidencyStatus')) ? answerReferenceMap.get('ResidencyStatus').ReadableAnswer__c == 'Australian citizen' : false,
					IsPermanentHumanitarianVisaHolder__c = (answerReferenceMap.containsKey('ResidencyStatus')) ? answerReferenceMap.get('ResidencyStatus').ReadableAnswer__c == 'Students/Applicants with permanent humanitarian visa' : false,
					EstimatedCourseCost__c = courseAmount,
					IsNewZealandCitizenAndEligibleForVFH__c = (answerReferenceMap.containsKey('NewZealandEligibilityForVFH')) ? answerReferenceMap.get('NewZealandEligibilityForVFH').Answer__c == 'Yes' : false,
					TaxFileNumber__c = (answerReferenceMap.containsKey('TFN') && answerReferenceMap.get('TFN').EncryptedAnswer__c != null && String.valueOf(answerReferenceMap.get('TFN').EncryptedAnswer__c).length() == 9) ? answerReferenceMap.get('TFN').EncryptedAnswer__c : null
				);
			formLoanMap.put(record, loanform);
		}

		insert formLoanMap.values();

		List<ApplicationForm__c> appForms = new List<ApplicationForm__c>();

		for (ApplicationForm__c record : formLoanMap.keySet()) {
			ApplicationForm__c appForm = new ApplicationForm__c(
					Id = record.Id,
					LoanFormId__c = formLoanMap.get(record).Id
				);
			appForms.add(appForm);
		}

		update appForms;
	}

	/**
	 * @description Populates earliest loan form date and expected loan form date for Application Form
	 * @author Biao Zhang  
	 * @date 24.NOV.2015
	 */
	private static void populateLoanFormDate(List<ApplicationForm__c> records) {
		//check whether portal setting exist
		ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
		if(portalSettings == null || portalSettings.LoanApplicationFormCreationOffset__c == null) {
			return;
		}
		//Caculate offset by milliseconds
		Long offset; 
		if(portalSettings.MinutesDelayed__c == null) {
			offset = (integer) portalSettings.LoanApplicationFormCreationOffset__c * 24 * 60 * 60 * 1000;
		} else {
			offset = (integer) portalSettings.MinutesDelayed__c * 60 * 1000;
		}

		BusinessHours bh = [select id from businesshours where IsDefault=true]; 

		// Build a mapping of holidays per state
		// Firstly get a set of all state Ids for the mapping piece
		Set<Id> stateIds = new Set<Id>();
		for (ApplicationForm__c record : records) {
			if (String.isNotBlank(record.StateId__c)) {
				stateIds.add(record.StateId__c);
			}
		}
		// Mapping of <State Id> : <State Holidays>
		Map<Id, Set<Date>> stateHolidayMap = new Map<Id, Set<Date>>();
		for (Id stateId : stateIds) {
			stateHolidayMap.put(stateId, new Set<Date>());
		}

		List<Holiday__c> holidays = [SELECT Id, StateId__c, Date__c FROM Holiday__c where Date__c >= :Date.today() order by Date__c asc];
		for(Holiday__c holiday : holidays) {
			if(String.isNotBlank(holiday.StateId__c)) {
				if (stateHolidayMap.containsKey(holiday.StateId__c)) {
					stateHolidayMap.get(holiday.StateId__c).add(holiday.Date__c);
				}
			} else {
				//Holiday without specified state will apply to all states
				for (Id stateId : stateHolidayMap.keySet()) {
					stateHolidayMap.get(stateId).add(holiday.Date__c);
				}
			}
		}

		//real job starts here
		Long oneDayOffset = 24 * 60 * 60 * 1000;
		Long halfDayOffset = 12 * 60 * 60 * 1000;

		for(ApplicationForm__c form : records) {
			Set<Date> stateHolidays = stateHolidayMap.get(form.StateId__c);
			//check if any holiday fall into the portal setting offset period
			form.EarliestLoanFormDate__c = BusinessHours.add(bh.id, form.SubmissionDate__c, offset);
			for(Date d : stateHolidays) {
				if(d >= form.SubmissionDate__c.date() && d <= form.EarliestLoanFormDate__c.date() && BusinessHours.isWithin(bh.id, d)) {
					offset += oneDayOffset;
					form.EarliestLoanFormDate__c = BusinessHours.add(bh.id, form.SubmissionDate__c, offset);
				}
			}

			//expected loan form date will be the cloest following 12am/pm to the earliest loan form date
			DateTime noon = DateTime.newInstance(form.EarliestLoanFormDate__c.year(), form.EarliestLoanFormDate__c.month(), form.EarliestLoanFormDate__c.day(), 12, 0, 0);
			system.debug('noon');
			system.debug(noon);
			if(form.EarliestLoanFormDate__c < noon) {
				form.ExpectedLoanFormDate__c = noon;
			} else {
				form.ExpectedLoanFormDate__c = BusinessHours.add(bh.id, noon, halfDayOffset + 1000); //add one more minute to make sure it is a new day
				//if expected loan form date is 0 am next day, check against holidays
				while(stateHolidays.contains(form.ExpectedLoanFormDate__c.date())) {
					form.ExpectedLoanFormDate__c = BusinessHours.add(bh.id, form.ExpectedLoanFormDate__c, oneDayOffset);
				}
			}
		}
	}
}