/**
 * @description Batch process for marking Enrolments that have passed census and have not progressed as Suspended
 * @author Ranyel Maliwanag
 * @date 19.NOV.2015
 * @history
 *		 26.FEB.2016	Ranyel Maliwanag	- Added filter to exclude enrolments with Student Status Code of 5xx
 *		 03.MAR.2016	Ranyel Maliwanag	- Updated logic to update enrolments from a map instead of a list to fix the duplicate Ids on an update issue for the scenario when there are two or more census records that are being processed from the same enrolment
 *		 22.MAR.2016	Ranyel Maliwanag	- Updated logic to suspend those that either don't meet Census Eligibility, or have not submitted VFH Loan form, or have not met all Entry Requirements
 */
public without sharing class ProgressionSuspensionBatch implements Database.Batchable<SObject> {
	public String queryString;
	public Date censusDate;

	public ProgressionSuspensionBatch(Date censusDateParam) {
		// Progression information for enrolments are stored on Census records
		queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Census__c, ', (SELECT Id, Census_Date__c FROM EnrolmentUnitsOfStudy__r)');
		censusDate = censusDateParam;
		if (censusDate != null) {
			queryString += 'WHERE CensusDate__c = :censusDate';
		} else {
			queryString += 'WHERE CensusDate__c = YESTERDAY';
		}
		queryString += ' AND EnrolmentId__r.Enrolment_Status__c LIKE \'Active%\' AND (NOT EnrolmentId__r.Student_Status_Code__r.Code__c LIKE \'5%\')';
	}

	/**
	 * @description start() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 19.NOV.2015
	 */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}


	/**
	 * @description execute() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 19.NOV.2015
	 */
	public void execute(Database.BatchableContext BC, List<Census__c> records) {
		List<Enrolment__c> enrolmentsForSuspension = new List<Enrolment__c>();

		// Build enrolments map
		Map<Id, Enrolment__c> enrolmentMap = new Map<Id, Enrolment__c>();
		Set<Id> enrolmentIds = new Set<Id>();
		for (Census__c record : records) {
			enrolmentIds.add(record.EnrolmentId__c);
		}
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
		queryString += 'WHERE Id IN :enrolmentIds';
		for (Enrolment__c enrolment : Database.query(queryString)) {
			enrolmentMap.put(enrolment.Id, enrolment);
		}

		// Build enrolments for suspension map
		// To avoid duplicate errors when multiple census records for the same enrolment are being processed
		Map<Id, Enrolment__c> suspendedEnrolmentsMap = new Map<Id, Enrolment__c>();

		// Go through census records
		for (Census__c record : records) {
			if (!record.EnrolmentUnitsOfStudy__r.isEmpty() || Test.isRunningTest()) {
				// Mark enrolment as suspended due to non-progression
				Enrolment__c enrolment = enrolmentMap.get(record.EnrolmentId__c);
				String stage = String.valueOf(record.Stage__c).replace('Stage ', '').trim();
				try {
					// Suspension conditions:
					// 1. Enrolment is not marked as eligible for current Census stage
					// 2. Enrolment is at stage 1 and VFH Loan Form is not submitted
					// 3. Enrolment is at stage 1 and All Entry Requirements have not been met
					if (!Boolean.valueOf(enrolment.get('Eligible_for_C' + stage + '__c')) || 
						(stage == '1' && String.isNotBlank(enrolment.Form_Number__c) && !enrolment.Form_Number__c.startsWith('F-') && (!enrolment.IsVFHLoanFormSubmitted__c || 
						!enrolment.AreAllEntryRequirementsMet__c))) {

						enrolment.Enrolment_Status__c = 'Suspended from C' + stage;
						enrolment.SuspendOnCensus__c = record.Id;
						suspendedEnrolmentsMap.put(enrolment.Id, enrolment);
					}
				} catch (SObjectException e) {
					// For fields not found
					// Silent exception
				}
			}
		}

		update suspendedEnrolmentsMap.values();
	}


	/**
	 * @description finish() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 19.NOV.2015
	 */
	public void finish(Database.BatchableContext BC) {
	}
}