global class NATValidationBatch_WARAPTEnrolments implements Database.Batchable<sObject>,Database.Stateful {

    global String query = '';
    global NAT_Validation_Log__c log;
    global Integer enrolmentRecordCount = 0;
    
    
    
    global NATValidationBatch_WARAPTEnrolments (String logId) {
        
         log = [ SELECT ID,Validation_Complete__c,WA_RAPT_Enrolment_Record_Count__c,WA_RAPT_Student_Record_Count__c, RAPT_EnrolmentQuery__c, Validation_State__c, Training_Organisation_Id__c, query_students__c, query_students_2__c, query_students_3__c, query_students_4__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
    
    }


     global database.querylocator start(Database.BatchableContext BC) {
     
        query = log.RAPT_EnrolmentQuery__c;
         
        return Database.getQueryLocator(query);
            
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
          
          List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
          
          for(SObject s : scope) {
                
                
                
                Enrolment_Unit__c enrolmentUnit = (Enrolment_Unit__c) s;
                enrolmentRecordCount++;
                
                NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = enrolmentUnit.Id, Nat_File_Name__c = 'WA RAPT Enrolment',Parent_Record_Last_Modified_Date__c = enrolmentUnit.LastModifiedDate,Enrolment_Name__c = enrolmentUnit.Enrolment__r.Name);
                /*Student Id */
                  if(enrolmentUnit.Purchasing_Contract_Identifier__c == null)
                    event.Error_Message__c = 'Purchasing Contract Identifier cannot be blank';
        
                else if(enrolmentUnit.Unit__r.Unit_Name__c == null)
                    event.Error_Message__c = 'Unit Of Competency name cannot be blank, please refer to related UOC';
                
                else if(EnrolmentUnit.Scheduled_Hours__c == null)
                    event.Error_Message__c = 'Schedule Hours cannot be blank and must be round number, e.g 10 or 20';
                    
                else if(EnrolmentUnit.Fee_Exemption_Identifier__c == null)
                    event.Error_Message__c = 'Fee Type cannot be blank';
                    
                else if(EnrolmentUnit.Delivery_Strategy__r.Code__c == null)
                    event.Error_Message__c = 'Delivery Strategy cannot be blank';
                
                
                else if(EnrolmentUnit.Unit_Result__r.Result_Code__c == null)
                   event.Error_Message__c = 'Result Code cannot be blank';
                else if(EnrolmentUnit.Enrolment__r.Start_Date__c == null || EnrolmentUnit.Enrolment__r.End_Date__c == null)
                   event.Error_Message__c= 'Enrolment Start Date or End Date cannot be blank';
                else if(enrolmentUnit.Training_Delivery_Location__r.Child_Suburb__c == null)
                   event.Error_Message__c = 'Location Suburb cannot be blank';
                else if(enrolmentUnit.Training_Delivery_Location__r.Child_Postcode__c == null)
                   event.Error_Message__c = 'Location Postal Code cannot be blank';
                else if(enrolmentUnit.Enrolment__r.Qualification_Issued__c == null)
                   event.Error_Message__c = 'Qualification Issued flag needs to be Y or N';
                    
                    
                    
                if(event.Error_Message__c != null)
                    errorEventList.add(event);
          
          
          }
          
                     
           insert errorEventList; 
     
     
     }
     
     global void finish(Database.BatchableContext BC) {
         
        log.WA_RAPT_Enrolment_Record_Count__c = enrolmentRecordCount;
        log.Validation_Complete__c = true;
         
        update log;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('WA RAPT Validation is Complete');  
        mail.setPlainTextbody('WA RAPT Validation completed, please click on the URL to check the log. ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id);
        mail.setHTMLBody('WA RAPT Validation completed, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id + '">click here</a> to check the log.' +
              'WA RAPT Student Records: ' + log.WA_RAPT_Student_Record_Count__c + '</br>' +
              'WA RAPT Enrolment Records: ' + log.WA_RAPT_Enrolment_Record_Count__c + '</br>' 
              );
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
     
     
     }
     
     
     private static NAT_Validation_Log__c createLog() {
        
        NAT_Validation_Log__c log = new NAT_Validation_Log__c();
        
        try { insert log; }
        catch(DMLException e) {}
        
        return log;
    
    }




}