@isTest
private class QualificationUnitTriggerHandler_Test {
    static testMethod void qualificationUnitTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='qUnit_User', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Qualification_Unit__c qu = new Qualification_Unit__c();
    	system.runAs(u){
	    	test.startTest();
    		anzs = TestCoverageUtilityClass.createANZSCO();
    		fld = TestCoverageUtilityClass.createFieldOfEducation();
    		qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
    		unit = TestCoverageUtilityClass.createUnit();
    		qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
    		update qu;
	    	test.stopTest();
    	}  
    }
    static testMethod void qualificationUnitTest2() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='qUnit_User', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Qualification__c qual2 = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Unit__c unit2 = new Unit__c();
    	Unit__c unit3 = new Unit__c();
    	Qualification_Unit__c qu = new Qualification_Unit__c();
    	Qualification_Unit__c qu2 = new Qualification_Unit__c();
    	Qualification_Unit__c qu3 = new Qualification_Unit__c();
    	Qualification_Unit__c qu4 = new Qualification_Unit__c();
    	system.runAs(u){
	    	test.startTest();
    		anzs = TestCoverageUtilityClass.createANZSCO();
    		fld = TestCoverageUtilityClass.createFieldOfEducation();
    		qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
    		qual2 = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
    		unit = TestCoverageUtilityClass.createUnit();
    		unit2 = TestCoverageUtilityClass.createUnit();
    		unit3 = TestCoverageUtilityClass.createUnit();
    		qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
    		qu2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit2.Id);
    		qu3 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit3.Id);
    		try{
    			qu4 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit3.Id);
    			System.AssertEquals(qu4.Unit__c, unit3.Id);
    		}
    		catch(Exception e){
    			Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot create Qualification Units with the same Unit.') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
    		}
    		update qu;
	    	test.stopTest();
    	}  
    }
}