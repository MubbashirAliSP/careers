/**
 * @description Test class for ServiceCaseTriggerHandler
 * @author Ranyel Maliwanag
 * @date 29.MAY.2015
 */ 
@isTest
public with sharing class ServiceCaseTriggerHandler_Test {
    @testSetup
    static void triggerSetup() {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User adminUser = new User(
                ProfileId = adminProfile.Id,
                Username = 'adminUser@testing.org',
                Email = 'adminUser@testing.org',
                LastName = 'Telesales',
                Alias = 'tsu',
                TimeZoneSidKey = 'Australia/Brisbane',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
        insert adminUser;

        Group testQueue = new Group(
                Name = 'testQueue',
                Type = 'Queue'
            );
        insert testQueue;

        QueueSobject testQueueMapping = new QueueSobject(
                QueueId = testQueue.Id,
                SobjectType = 'Service_Cases__c'
            );
        insert testQueueMapping;

        testQueue = new Group(
                Name = 'otherQueue',
                Type = 'Queue'
            );
        insert testQueue;

        testQueueMapping = new QueueSobject(
                QueueId = testQueue.Id,
                SobjectType = 'Service_Cases__c'
            );
        insert testQueueMapping;

        System.runAs(adminUser) {
            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr'
                );
            insert student;

            Enrolment__c enrolment = new Enrolment__c(
                    IPS_Sync_on_next_update__c = true,
                    IPS_InPlace_Student__c = true,
                    Student__c = student.Id,
                    Enrolment_Status__c = 'Active (Commencement)',
                    Mailing_Country__c = 'Australia',
                    Mailing_Zip_Postal_Code__c = '4006',
                    Name_of_suburb_town_locality__c = 'Fortitude Valley',
                    Mailing_State_Provience__c = 'Queensland',
                    Address_street_number__c = '100',
                    Address_street_name__c = 'Brookes Street',
                    Address_flat_unit_details__c = 'Ground Floor',
                    Managed_by_GLS__c = true,
                    GLS_Enrolment_ID__c = 'EX1230'
                );
            insert enrolment;

            CentralTriggerSettings__c customSettings = SASTestUtilities.createCentralTriggerSettings();
            insert customSettings;
        }
    }

    @testSetup
    static void setup() {
        AllocationStream__c stream = new AllocationStream__c(
                Name = 'CAG Australia',
                SalesAgentIdentifiers__c = 'CA Sales BNE',
                DefaultStrategy__c = '1 - Tele /BTC Support'
            );
        insert stream;

        PostcodeAllocation__c allocation = new PostcodeAllocation__c(
                Postcode__c = '4000',
                AllocationCampus__c = 'Burleigh',
                PrimaryLLNSupportStrategy__c = '5 - Campus Support',
                AllocationStreamId__c = stream.Id,
                AllocationCode__c = stream.Name
            );
        insert allocation;
    }

    static testMethod void testUpdateLastQueueName() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Id onlineServiceCaseRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Online_Service_Case' AND SObjectType = 'Service_Cases__c'].Id;

        Test.startTest();
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testUser.Id,
                    RecordTypeId = onlineServiceCaseRecordType
                );
            insert serviceCase;

            serviceCase = [SELECT Id, Last_Queue_Name__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.Last_Queue_Name__c));

            serviceCase = new Service_Cases__c(
                    OwnerId = testQueue.Id,
                    RecordTypeId = onlineServiceCaseRecordType
                );
            insert serviceCase;

            serviceCase = [SELECT Id, Last_Queue_Name__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.Last_Queue_Name__c));
            System.assertEquals(testQueue.Name, serviceCase.Last_Queue_Name__c);

            serviceCase.OwnerId = otherQueue.Id;
            update serviceCase;

            serviceCase = [SELECT Id, Last_Queue_Name__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.Last_Queue_Name__c));
            System.assertEquals(otherQueue.Name, serviceCase.Last_Queue_Name__c);

            serviceCase.OwnerId = testUser.Id;
            update serviceCase;

            serviceCase = [SELECT Id, Last_Queue_Name__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.Last_Queue_Name__c));
            System.assertEquals(otherQueue.Name, serviceCase.Last_Queue_Name__c);
        Test.stopTest();
    }

    static testMethod void testStatusRevert1() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Id onlineServiceCaseRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Online_Service_Case' AND SObjectType = 'Service_Cases__c'].Id;

        Test.startTest();
            // Service Case gets inserted, assigned to a Queue
            // Assertion: service case's status remains `New`
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testQueue.Id,
                    RecordTypeId = onlineServiceCaseRecordType,
                    Status__c = 'New'
                );
            insert serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('New', serviceCase.Status__c);

            // Agent takes ownership of service case, changes status to something else
            // Assertion: service case's status changed to `In Progress`
            serviceCase.OwnerId = testUser.Id;
            serviceCase.Status__c = 'In Progress';
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('In Progress', serviceCase.Status__c);

            // Agent puts back the service case to the same queue
            // Assertion: service case status remains the same
            serviceCase.OwnerId = testQueue.Id;
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('In Progress', serviceCase.Status__c);
        Test.stopTest();
    }

    static testMethod void testStatusRevert2() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Id onlineServiceCaseRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Online_Service_Case' AND SObjectType = 'Service_Cases__c'].Id;

        Test.startTest();
            // Service Case gets inserted, assigned to a Queue
            // Assertion: service case's status remains `New`
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testQueue.Id,
                    RecordTypeId = onlineServiceCaseRecordType,
                    Status__c = 'New'
                );
            insert serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('New', serviceCase.Status__c);

            // Agent takes ownership of service case, changes status to something else
            // Assertion: service case's status changed to `In Progress`
            serviceCase.OwnerId = testUser.Id;
            serviceCase.Status__c = 'In Progress';
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('In Progress', serviceCase.Status__c);

            // Agent puts the service case to a different queue
            // Assertion: service case status returns to new
            serviceCase.OwnerId = otherQueue.Id;
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('New', serviceCase.Status__c);
        Test.stopTest();
    }

    static testMethod void testStatusRevert3() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Id onlineServiceCaseRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Online_Service_Case' AND SObjectType = 'Service_Cases__c'].Id;

        Test.startTest();
            // Service Case gets inserted, assigned to a Queue
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testQueue.Id,
                    RecordTypeId = onlineServiceCaseRecordType,
                    Status__c = 'In Progress'
                );
            insert serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('In Progress', serviceCase.Status__c);

            // Agent does not take ownership 
            // Assertion: service case status returns to new
            serviceCase.OwnerId = otherQueue.Id;
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('New', serviceCase.Status__c);
        Test.stopTest();
    }

    static testMethod void testStatusRevert4() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Id onlineServiceCaseRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Online_Service_Case' AND SObjectType = 'Service_Cases__c'].Id;

        Test.startTest();
            // Service Case gets inserted, assigned to a Queue
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testQueue.Id,
                    RecordTypeId = onlineServiceCaseRecordType,
                    Status__c = 'Closed'
                );
            insert serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('Closed', serviceCase.Status__c);

            // Agent does not take ownership and transfer to another queue
            // Assertion: service case status remains Closed
            serviceCase.OwnerId = otherQueue.Id;
            update serviceCase;
            serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals('Closed', serviceCase.Status__c);
        Test.stopTest();
    }

    static testMethod void markManagedByGLSTest() {
        User testUser = [SELECT Id FROM User WHERE Email = 'adminUser@testing.org'];
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];

        Test.startTest();
            Service_Cases__c serviceCase = new Service_Cases__c(
                    OwnerId = testUser.Id,
                    Enrolment__c = enrolment.Id,
                    Account_Student_Name__c = student.Id
                );
            insert serviceCase;
        Test.stopTest();
    }


    // Positive Test
    static testMethod void updateVFHLoanFormSubmittedStatusTest() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        insert serviceCase;

        Test.startTest();
            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.IsVFHLoanFormSubmitted__c);

            serviceCase.Related_Form_Status__c = 'Submitted';
            update serviceCase;

            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(enrolment.IsVFHLoanFormSubmitted__c);
        Test.stopTest();
    }

    static testMethod void updateVFHLoanFormSubmittedStatusTest1() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        insert serviceCase;

        Test.startTest();
            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.IsVFHLoanFormSubmitted__c);

            serviceCase.Related_Form_Status__c = 'Not Submitted';
            update serviceCase;

            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.IsVFHLoanFormSubmitted__c);
        Test.stopTest();
    }

    static testMethod void updateVFHLoanFormSubmittedStatusTest2() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.onlineServiceCaseRecordType
            );
        insert serviceCase;

        Test.startTest();
            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.IsVFHLoanFormSubmitted__c);

            serviceCase.Related_Form_Status__c = 'Submitted';
            update serviceCase;

            enrolment = [SELECT Id, IsVFHLoanFormSubmitted__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.IsVFHLoanFormSubmitted__c);
        Test.stopTest();
    }

    static testMethod void updateVFHLoanFormSubmittedStatusTest3() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        insert serviceCase;

        Test.startTest();
            enrolment = [SELECT Id, AreAllEntryRequirementsMet__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.AreAllEntryRequirementsMet__c);

            serviceCase.AreAllEntryRequirementsMet__c = true;
            update serviceCase;

            enrolment = [SELECT Id, AreAllEntryRequirementsMet__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(enrolment.AreAllEntryRequirementsMet__c);
        Test.stopTest();
    }

    static testMethod void updateVFHLoanFormSubmittedStatusTest4() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        // Create service case without enrolment
        Service_Cases__c serviceCase = new Service_Cases__c(
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        insert serviceCase;

        Test.startTest();
            enrolment = [SELECT Id, AreAllEntryRequirementsMet__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.AreAllEntryRequirementsMet__c);

            // Update Service Case
            serviceCase.AreAllEntryRequirementsMet__c = true;
            update serviceCase;

            enrolment = [SELECT Id, AreAllEntryRequirementsMet__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(!enrolment.AreAllEntryRequirementsMet__c);

            serviceCase.Enrolment__c = enrolment.Id;
            update serviceCase;

            enrolment = [SELECT Id, AreAllEntryRequirementsMet__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(enrolment.AreAllEntryRequirementsMet__c);
        Test.stopTest();
    }

    static testMethod void createOwnershipHistoryTest() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Group testQueue = [SELECT Id, Name FROM Group WHERE Name = 'testQueue'];
        Group otherQueue = [SELECT Id, Name FROM Group WHERE Name = 'otherQueue'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                OwnerId = testQueue.Id
            );
        Test.startTest();
            insert serviceCase;

            serviceCase = [SELECT Id, OwnerId, Owner.Name, (SELECT Id FROM OwnershipHistories__r) FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals(1, serviceCase.OwnershipHistories__r.size());

            serviceCase.Status__c = 'Updated';
            update serviceCase;

            serviceCase = [SELECT Id, OwnerId, Owner.Name, (SELECT Id FROM OwnershipHistories__r) FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals(1, serviceCase.OwnershipHistories__r.size());

            serviceCase.OwnerId = otherQueue.Id;
            update serviceCase;

            serviceCase = [SELECT Id, OwnerId, Owner.Name, (SELECT Id FROM OwnershipHistories__r) FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertEquals(2, serviceCase.OwnershipHistories__r.size());
        Test.stopTest();
    }


    static testMethod void updateCurrentOwnershipStartDateTest() {
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType
            );
        Test.startTest();
            insert serviceCase;

            serviceCase = [SELECT Id, CurrentOwnershipStartDate__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assertNotEquals(null, serviceCase.CurrentOwnershipStartDate__c);
        Test.stopTest();
    }


    /**
     * @method allocatePostcode
     * @scenario Service Case gets created and `Residential_Postcode__c` is populated and a matching Postcode Allocation record is found for the given `BrokerName__c` code
     * @acceptanceCriteria `PostcodeAllocationId__c` field gets populated on the Service Case
     * @acceptanceCriteria `Primary_LLN_Support_Strategies__c` field gets populated on the Service Case and matches the `PrimaryLLNSupportStrategy__c` field on the related Postcode Allocation record
     * @acceptanceCriteria `AllocationCampus__c` field gets populated on the Service Case and matches the `AllocationCampus__c` field on the related Postcode Allocation record
     */
    static testMethod void postcodeAllocationTest1() {
        String postCode = '4000';
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        PostcodeAllocation__c allocation = [SELECT Id, AllocationCampus__c, PrimaryLLNSupportStrategy__c FROM PostcodeAllocation__c WHERE Postcode__c = :postCode];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application',
                Residential_Postcode__c = postCode,
                BrokerName__c = 'CA Sales BNE'
            );

        Test.startTest();
            insert serviceCase;

            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.PostcodeAllocationId__c));
            System.assertEquals(allocation.PrimaryLLNSupportStrategy__c, serviceCase.Primary_LLN_Support_Strategies__c);
            System.assertEquals(allocation.AllocationCampus__c, serviceCase.AllocationCampus__c);
        Test.stopTest();
    }


    /**
     * @method allocatePostcode
     * @scenario Service Case gets created and `Residential_Postcode__c` is populated and a matching Postcode Allocation record is not found for the given `BrokerName__c` value
     * @acceptanceCriteria `PostcodeAllocationId__c` field gets populated on the Service Case
     * @acceptanceCriteria `Primary_LLN_Support_Strategies__c` field gets populated on the Service Case and matches the `PrimaryLLNSupportStrategy__c` field on the related Postcode Allocation record
     * @acceptanceCriteria `AllocationCampus__c` field gets populated on the Service Case and matches the `AllocationCampus__c` field on the related Postcode Allocation record
     */
    static testMethod void postcodeAllocationTest2() {
        String postCode = '4000';
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        PostcodeAllocation__c allocation = [SELECT Id, AllocationCampus__c, PrimaryLLNSupportStrategy__c FROM PostcodeAllocation__c WHERE Postcode__c = :postCode];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application',
                Residential_Postcode__c = '4001',
                BrokerName__c = 'CA Sales MAN'
            );
        Test.startTest();
            insert serviceCase;

            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.PostcodeAllocationId__c));
            System.assert(String.isBlank(serviceCase.Primary_LLN_Support_Strategies__c));
            System.assert(String.isBlank(serviceCase.AllocationCampus__c));
        Test.stopTest();
    }


    /**
     * @method allocatePostcode
     * @scenario Residential Postcode on Service Case gets updated and a matching Postcode Allocation record is found for the given `BrokerName__c` value
     * @acceptanceCriteria `PostcodeAllocationId__c` field gets populated on the Service Case
     * @acceptanceCriteria `Primary_LLN_Support_Strategies__c` field gets populated on the Service Case and matches the `PrimaryLLNSupportStrategy__c` field on the related Postcode Allocation record
     * @acceptanceCriteria `AllocationCampus__c` field gets populatedo n the Service Case and matches the `AllocationCampus__c` field on the related Postcode Allocation record
     */
    static testMethod void postcodeAllocationTest3() {
        String postCode = '4000';
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        PostcodeAllocation__c allocation = [SELECT Id, AllocationCampus__c, PrimaryLLNSupportStrategy__c FROM PostcodeAllocation__c WHERE Postcode__c = :postCode];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application',
                BrokerName__c = 'CA Sales BNE'
            );
        insert serviceCase;
        Test.startTest();
            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.PostcodeAllocationId__c));
            System.assert(String.isBlank(serviceCase.Primary_LLN_Support_Strategies__c));
            System.assert(String.isBlank(serviceCase.AllocationCampus__c));

            serviceCase.Residential_Postcode__c = postCode;
            update serviceCase;

            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.PostcodeAllocationId__c));
            System.assertEquals(allocation.PrimaryLLNSupportStrategy__c, serviceCase.Primary_LLN_Support_Strategies__c);
            System.assertEquals(allocation.AllocationCampus__c, serviceCase.AllocationCampus__c);
        Test.stopTest();
    }


     /**
     * @method allocatePostcode
     * @scenario `BrokerName__c` on Service Case gets updated and a matching Postcode Allocation record is found for the new `BrokerName__c` value
     * @acceptanceCriteria `PostcodeAllocationId__c` field gets populated on the Service Case
     * @acceptanceCriteria `Primary_LLN_Support_Strategies__c` field gets populated on the Service Case and matches the `PrimaryLLNSupportStrategy__c` field on the related Postcode Allocation record
     * @acceptanceCriteria `AllocationCampus__c` field gets populatedo n the Service Case and matches the `AllocationCampus__c` field on the related Postcode Allocation record
     */
    static testMethod void postcodeAllocationTest4() {
        String postCode = '4000';
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        PostcodeAllocation__c allocation = [SELECT Id, AllocationCampus__c, PrimaryLLNSupportStrategy__c FROM PostcodeAllocation__c WHERE Postcode__c = :postCode];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application',
                Residential_Postcode__c = postCode
            );
        insert serviceCase;
        Test.startTest();
            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.PostcodeAllocationId__c));
            System.assert(String.isBlank(serviceCase.Primary_LLN_Support_Strategies__c));
            System.assert(String.isBlank(serviceCase.AllocationCampus__c));

            serviceCase.BrokerName__c = 'CA Sales BNE';
            update serviceCase;

            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isNotBlank(serviceCase.PostcodeAllocationId__c));
            System.assertEquals(allocation.PrimaryLLNSupportStrategy__c, serviceCase.Primary_LLN_Support_Strategies__c);
            System.assertEquals(allocation.AllocationCampus__c, serviceCase.AllocationCampus__c);
        Test.stopTest();
    }


    /**
     * @method allocatePostcode
     * @scenario Residential Postcode on Service Case gets updated and a matching Postcode Allocation record is not found
     * @acceptanceCriteria `PostcodeAllocationId__c` field remains blank on the Service Case
     * @acceptanceCriteria `Primary_LLN_Support_Strategies__c` field gets populated to the default LLN Strategy for the stream
     * @acceptanceCriteria `AllocationCampus__c` field remains blank on the Service Case
     */
    static testMethod void postcodeAllocationTest5() {
        String postCode = '4000';
        Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        PostcodeAllocation__c allocation = [SELECT Id, AllocationCampus__c, PrimaryLLNSupportStrategy__c FROM PostcodeAllocation__c WHERE Postcode__c = :postCode];
        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application',
                BrokerName__c = 'CA Sales BNE'
            );
        insert serviceCase;
        Test.startTest();
            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.PostcodeAllocationId__c));
            System.assert(String.isBlank(serviceCase.Primary_LLN_Support_Strategies__c));
            System.assert(String.isBlank(serviceCase.AllocationCampus__c));

            serviceCase.Residential_Postcode__c = '4001';
            update serviceCase;

            serviceCase = [SELECT Id, PostcodeAllocationId__c, Primary_LLN_Support_Strategies__c, AllocationCampus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
            System.assert(String.isBlank(serviceCase.PostcodeAllocationId__c));
            System.assert(String.isNotBlank(serviceCase.Primary_LLN_Support_Strategies__c));
            System.assert(String.isBlank(serviceCase.AllocationCampus__c));
        Test.stopTest();
    }
}