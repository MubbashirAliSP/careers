/**
 * @description Custom controller extension for `AwardUnitsGeneration` page to handle `AwardUnit__c` record creation before going to the Conga link in order to record relationships between "Enrolment Units" and "Awards"
 * @author Ranyel Maliwanag
 * @date 09.DEC.2015
 * @history
 *		 03.MAY.2016	Ranyel Maliwanag	- Added National Outcome code of `60` to the Enrolment Units filter for Award Units generation
 */
public without sharing class AwardUnitsGenerationCX {
	public Awards__c award {get; set;}

	/**
	 * @description Default controller extension constructor for initialisations
	 * @author Ranyel Maliwanag
	 * @date 09.DEC.2015
	 * @param (ApexPages.StandardController) controller: The standard controller that will be extended by this class
	 */
	public AwardUnitsGenerationCX(ApexPages.StandardController controller) {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Awards__c, ', Enrolment__r.Student_Name__c, Enrolment__r.Name');
		queryString += 'WHERE Id = \'' + controller.getId() + '\'';
		award = Database.query(queryString);
	}


	/**
	 * @description Page load action handler to redirect the user to the Conga url to generate the Award document
	 * @author Ranyel Maliwanag
	 * @date 09.DEC.2015
	 * @history
	 *		 03.MAY.2016	Ranyel Maliwanag	- Added National Outcome code of `60` to the Enrolment Units filter for Award Units generation
	 */
	public PageReference evaluateRedirects() {
		String congaUrl = 'https://www.appextremes.com/apps/Conga/Composer.aspx';
		congaUrl += '?SessionId=' + EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
		congaUrl += '&ServerUrl=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/34.0/' + String.valueOf(UserInfo.getOrganizationId()).substring(0, 15), 'UTF-8');
		congaUrl += '&Id=' + String.valueOf(award.Id).substring(0, 15);
		System.debug('Award.Id ::: ' + award.Id);
		System.debug('Award.Enrolment__r.Name ::: ' + award.Enrolment__r.Name);
		System.debug('Award.Enrolment__c ::: ' + award.Enrolment__c);
		System.debug('Award.Qualification_ID__c ::: ' + award.Qualification_ID__c);
		System.debug('Award.Award_ID__c ::: ' + award.Award_ID__c);
		
		congaUrl += '&ReportId=' + '[Student]00O90000004rbtI?pv0=' + String.valueOf(award.Id).substring(0, 15) + ', [EnrolmentUnit]00O90000002xEQV?pv0=' + award.Enrolment__r.Name + ',[RTO]00O90000002xEQX?pv0=' + award.Enrolment__r.Name + ',[Enrolment]00O90000002xEQU?pv0=' + String.valueOf(award.Enrolment__c).substring(0, 15) + ',[Qualification]00O90000002xEQW?pv0=' + String.valueOf(award.Qualification_ID__c).substring(0, 15) + ',[Award]00O90000002xEQT?pv0=' + award.Award_ID__c + ',[Campus]00O90000005VLXz?pv0=' + String.valueOf(award.Enrolment__c).substring(0, 15);
		congaUrl += '&DefaultPDF=' + '1';
		congaUrl += '&TemplateID=' + award.Award_Template_ID__c;
		congaUrl += '&OFN=' + 'Award ' + award.Enrolment__r.Student_Name__c + ' ' + award.Award_ID__c;
		congaUrl += '&DS7=' + '1';


		List<Enrolment_Unit__c> enrolmentUnits = [SELECT Id FROM Enrolment_Unit__c WHERE Enrolment__c = :award.Enrolment__c AND AVETMISS_National_Outcome__c IN ('20', '51', '60', '61')];

		List<AwardUnit__c> awardUnits = new List<AwardUnit__c>();
		for (Enrolment_Unit__c enrolmentUnit : enrolmentUnits) {
			AwardUnit__c awardUnit = new AwardUnit__c(
					AwardId__c = award.Id,
					EnrolmentUnitId__c = enrolmentUnit.Id
				);
			awardUnits.add(awardUnit);
		}
		insert awardUnits;

		PageReference result = new PageReference(congaUrl);
		result.setRedirect(true);
		return result;
	}
}