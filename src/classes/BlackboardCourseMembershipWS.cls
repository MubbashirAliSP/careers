/*BlackBoard Course Membership Integration Class only
 *complies with methods required for integration
 *any further modifications should be noted here
 *Contains Save And Get operations
 *Get operation also contains Get Course Roles
 *Both get methods are on demand
 *Save method is part of batch operation
 *Please Refer to method comments for more details
 *written by YG, CloudSherpas 17 July 2014
 */

global class BlackboardCourseMembershipWS {


    public static HTTPResponse saveCourseMembership(Blackboard_Course_Membership__c bbcoursemember) {

         List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
         
         String SessionId = log.get(0).Session_Id__c;
         
         Id logId = log.get(0).Id;
         
         String syncStatus = '';
         
         String Message = '';
         
         String coursesMembershipsXML = BlackBoardXMLBuilder.buildCourseMembershipXML(bbcoursemember);
         
         String blackboardPrimaryKey = ''; 
         
         String SoapXMLBody ='';
         
         //contruct XML

         SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://coursemembership.ws.blackboard" xmlns:xsd="http://coursemembership.ws.blackboard/xsd">' +
     
                       '  <soapenv:Header>'+
                          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                                '  <wsu:Timestamp wsu:Id="TS-24">'+
                                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                                    
                                '  </wsu:Timestamp>'+
                                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                                '    <wsse:Username>session</wsse:Username>'+
                                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                                    
                                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                    
                        
                        
                      
                        
                        ' <soapenv:Body> ' +
                          
                          '  <cour:saveCourseMembership>'+
                          
                                 coursesMembershipsXML +
                          
                          '  </cour:saveCourseMembership>' +
                        
                        
                        ' </soapenv:Body> ' +
                      
                      '</soapenv:Envelope>';


         string SoapXML;
         SoapXML = SoapXMLBody;
         Integer ContentLength = 0;
         ContentLength = SoapXML.length();
            
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         HttpResponse res = new HttpResponse();

         req.setMethod('POST');
         req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_Membership_WS_Endpoint__c); 
         req.setHeader('Content-type','text/xml');
         req.setHeader('Content-Length',ContentLength.format());  
         req.setHeader('SoapAction','saveCourseMembership');
         req.setBody(SoapXML); 
        
        

         if(Test.isRunningTest())
            res.setBody('<return>_1234</return>');
         else
            res = h.send(req);

         XmlStreamReader reader = res.getXmlStreamReader();

       
          
          while(reader.hasNext()) {
               //  Start at the beginning of the book and make sure that it is a book
               if (reader.getEventType() == XmlTag.START_ELEMENT) {
                  if ('return' == reader.getLocalName()) {
                     blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                     syncStatus = 'Success';
                     Message = 'Record Synced';           
                     break;
                  }
                  
                  if('faultstring' == reader.getLocalName()) {
                    syncStatus = 'Fail';
                    Message = BlackboardUtility.parseResult(reader);
                    break;
                  }
               }
              reader.next();
          }
          
          //if no errors from blackboard, update fields in SFDC
          if(SyncStatus == 'Success') {
          
              bbcoursemember.Blackboard_Primary_Key__c = blackboardPrimaryKey;
              bbcoursemember.Date_of_Last_Sync__c = System.Now();
              bbcoursemember.Changed_Since_Last_Sync__c  = false; 
              bbcoursemember.Available_In_BlackBoard__c = bbcoursemember.Available_After_Sync__c;
              
              
          /*write back to master unit */
            try {
                update bbcoursemember;
            } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}

         } 


         //write sync event
         BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bbcoursemember.Id,syncStatus, 'SaveCourseMembership'); //create log event for each record processed

        
         return res;
          



    }
    
    WebService static void singleGetOperation(string recordId, String oType) {

        HttpResponse initResponse; //initialize response
        HttpResponse loginReponse; //login response

      
        String sessionId = '';
        String result='';

        String syncStatus = '';
        String Message = '';
    
        BlackboardcontextWS contextWS = new BlackboardcontextWS();

        if(!Test.isRunningTest()) {

            initResponse = contextWS.initialize();

        
            XmlStreamReader reader = initResponse.getXmlStreamReader();

             while(reader.hasNext()) {
                 //  Start at the beginning of the book and make sure that it is a book
                 if (reader.getEventType() == XmlTag.START_ELEMENT) {

                    if ('return' == reader.getLocalName()) {
                       sessionId = BlackboardUtility.parseResult(reader);             
                       break;
                    }
                 }
                    reader.next();

               }    
           
           loginReponse = contextWS.login(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).userid__c, Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).password__c, 'YG','YG','', Integer.ValueOf(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).expectedLifeSeconds__c), sessionId);

        }
        
        /*Determine if this is Course Memebership or Course Role and Callout future method to process*/
        if(oType == 'CM')
         getSingleBlackboardCourseMembership(recordId, sessionId);

        if(oType == 'CR')
         getSingleCourseRole(recordId,sessionId);
        

     }
     
     /*This method performs single get operation against Course Role
      *Primary Key is returned and stored against Blackboard Role object
      *Login is performed in the getSingleOperation method
      
     
      */
     @future(callout=true)
     public static void getSingleCourseRole(String bbrid, String sessionId) {

        Blackboard_Role__c bbrole = [Select name,Role_Id__c,Role_Validated__c From Blackboard_Role__c Where id = : bbrid LIMIT 1];

        String syncStatus = '';

        String SoapXMLBody ='';
        
        String Message = '';
        
        String blackboardPrimaryKey = '';


      
              SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://coursemembership.ws.blackboard" xmlns:xsd="http://coursemembership.ws.blackboard/xsd">' +
     
                       '  <soapenv:Header>'+
                          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                                '  <wsu:Timestamp wsu:Id="TS-24">'+
                                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                                    
                                '  </wsu:Timestamp>'+
                                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                                '    <wsse:Username>session</wsse:Username>'+
                                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                                    
                                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                          
                        
                          
                                ' <soapenv:Body>'+
                                '    <cour:getCourseRoles>'+
                                '   </cour:getCourseRoles>'+
                                ' </soapenv:Body>' +
                        
                        '</soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_Membership_WS_Endpoint__c); 
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getCourseRoles');
               req.setBody(SoapXML);

              if(Test.isRunningTest())
                res.setBody('<roleIdentifier>_1234</roleIdentifier>');
             else
                res = h.send(req);

               XmlStreamReader reader2 = res.getXmlStreamReader();

               

               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('roleIdentifier' == reader2.getLocalName()) {
                           if(bbrole.Role_Id__c  == BlackboardUtility.parseResult(reader2)){
                           syncStatus = 'Success';
                           Message = 'Record Synced';
                              
                           break;
                          }
                        }
                      
                    }
                    reader2.next();
               }

          if(syncStatus == 'Success') 
                bbrole.Role_Validated__c = true;
          else
                bbrole.Role_Validated__c = false;
                

          update bbrole; 
           
           
           BlackboardUtility.createBlackBoardLog(sessionId);
           List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
           BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbrole.Id,syncStatus, 'getCourseRoles'); //create log event for each record processed

     }
     
     /*This method performs single get operation against Course Membership
      *Primary Key is returned and stored against Blackboard Course Membership object
      *Login is performed in the getSingleOperation method
      
     
      */
     @future(callout=true)
     public static void getSingleBlackboardCourseMembership(string bbcmid, String sessionId) {      
        
        
        Blackboard_Course_Membership__c bbcm = [Select name, Blackboard_Course__r.Blackboard_Primary_Key__c,Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c From Blackboard_Course_Membership__c Where id = : bbcmid];
        String syncStatus = '';

        String SoapXMLBody ='';
        
        String Message = '';
        
        String blackboardPrimaryKey = '';
      
              SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://coursemembership.ws.blackboard" xmlns:xsd="http://coursemembership.ws.blackboard/xsd">' +
     
                       '  <soapenv:Header>'+
                          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                                '  <wsu:Timestamp wsu:Id="TS-24">'+
                                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                                    
                                '  </wsu:Timestamp>'+
                                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                                '    <wsse:Username>session</wsse:Username>'+
                                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                                    
                                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                          
                        
                          
                                '<soapenv:Body>'+
                                '        <cour:getCourseMembership>'+
                                '        <cour:courseId>'+bbcm.Blackboard_Course__r.Blackboard_Primary_Key__c+'</cour:courseId>'+
                                '            <cour:f>'+
                                '                <xsd:filterType>6</xsd:filterType>'+
                                '                <xsd:userIds>'+bbcm.Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c+'</xsd:userIds>'+
                                '            </cour:f>'+
                                '       </cour:getCourseMembership>'+
                                ' </soapenv:Body>'+
                        
                        '</soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_Membership_WS_Endpoint__c);
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getCourseMembership');
               req.setBody(SoapXML); 
              

               if(Test.isRunningTest())
                  res.setBody('<id>_1234</id>');
                else
                  res = h.send(req);

               XmlStreamReader reader2 = res.getXmlStreamReader();
                
               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('id' == reader2.getLocalName()) {
                           blackboardPrimaryKey  = BlackboardUtility.parseResult(reader2);
                           syncStatus = 'Success';
                           Message = 'Record Synced';
                              
                           break;
                        }
                        
                        if('faultstring' == reader2.getLocalName()) {
                          syncStatus = 'Fail';
                           Message = BlackboardUtility.parseResult(reader2);
                        
                          break;
                        }
                    }
                    reader2.next();
               }


          if(syncStatus == 'Success') {
            bbcm.Blackboard_Primary_Key__c = blackboardPrimaryKey;
            update bbcm;

              
           }
           
           BlackboardUtility.createBlackBoardLog(sessionId);
           List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
           BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbcm.Id,syncStatus, 'getCourseMembership'); //create log event for each record processed


     }

    
    
}