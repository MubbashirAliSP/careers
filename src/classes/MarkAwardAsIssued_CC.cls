public with sharing class MarkAwardAsIssued_CC {
	public String theId;
	public MarkAwardAsIssued_CC(){
		theId = ApexPages.currentPage().getParameters().get('id');
			
	}
	public PageReference autoRun(){
		Awards__c award = [select Id, Qualification_Issued__c from Awards__c where Id =: theId];
		List<Task> taskList = [SELECT AccountId, OwnerId, Id, WhatId, Subject 
								FROM Task WHERE WhatId =: award.Id AND Subject = 'Print & Issue Approved Award'];
		//for(Awards__c a : awardList){
		award.Qualification_Issued__c = 'Y';
		//}
		for(Task t : taskList){
			t.OwnerId = UserInfo.getUserId();
			t.Status = 'Completed';
		}
		update award;
		update taskList;
		
		PageReference pageRef = new PageReference('/' + theId);
        pageRef.setRedirect(true);
        return pageRef;
	}
	
}