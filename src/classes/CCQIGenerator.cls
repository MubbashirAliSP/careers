public with sharing class CCQIGenerator {
	

	
	public static void exportCCQIFiles(String libraryId,String state, String Year, Account TrainingOrg, List<CCQIWrapperQualifications> Quals, List<CCQIWrapperUnits> Units) {
		
		//Training ORG
		
		String body = '';
		
		
			
			 body += NATGeneratorUtility.formatOrPadTxtFile(TrainingOrg.Training_Org_Identifier__c, CCQITrainingOrg__c.getInstance('1').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(TrainingOrg.Name, CCQITrainingOrg__c.getInstance('2').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(TrainingOrg.Offshore_Indicator_Flag__c, CCQITrainingOrg__c.getInstance('3').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(year, CCQITrainingOrg__c.getInstance('4').Length__c);
			
		Blob pBlob = Blob.valueof(body);
		
			
      			insert new ContentVersion(
           			 versionData =  pBlob,
           			 Title = 'TrainingOrg_' + state,
            		 PathOnClient = 'TrainingOrg_' + state + '.txt',
            		 FirstPublishLocationId = libraryId);
		
		
		
		
		body = '';
		
		if(Quals.size() > 0) {
		
		for(CCQIWrapperQualifications q : Quals) {
			
			 body += NATGeneratorUtility.formatOrPadTxtFile(TrainingOrg.Training_Org_Identifier__c, CCQIQualification__c.getInstance('1').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(year, CCQIQualification__c.getInstance('2').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(q.Qual.Name, CCQIQualification__c.getInstance('3').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(q.Qual.Qualification_Name__c, CCQIQualification__c.getInstance('4').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(state, CCQIQualification__c.getInstance('5').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(String.ValueOf(q.EC), CCQIQualification__c.getInstance('6').Length__c);
			 body += NATGeneratorUtility.formatOrPadTxtFile(String.ValueOf(q.CC), CCQIQualification__c.getInstance('7').Length__c);
			 
			 body += '\r\n';
			
			
			
		}
		
		
	    pBlob = Blob.valueof(body);
		
			
      			insert new ContentVersion(
           			 versionData =  pBlob,
           			 Title = 'Quals_' + state,
            		 PathOnClient = 'Quals_' + state + '.txt',
            		 FirstPublishLocationId = libraryId);
            		 
        body = '';
        
		}
		
		if(Units.size() > 0) { 
		       for(CCQIWrapperUnits u : Units) {
					
					 body += NATGeneratorUtility.formatOrPadTxtFile(TrainingOrg.Training_Org_Identifier__c, CCQIQualification__c.getInstance('1').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(year, CCQIQualification__c.getInstance('2').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(u.EU.Unit__r.Name, CCQIUnit__c.getInstance('3').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(u.EU.Unit__r.Unit_Name__c, CCQIUnit__c.getInstance('4').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(state, CCQIUnit__c.getInstance('5').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(String.ValueOf(u.EC), CCQIUnit__c.getInstance('6').Length__c);
					 body += NATGeneratorUtility.formatOrPadTxtFile(String.ValueOf(u.CC), CCQIUnit__c.getInstance('7').Length__c);
					
					 body += '\r\n';
					
				}
				
				
			    pBlob = Blob.valueof(body);
				
					
		      			insert new ContentVersion(
		           			 versionData =  pBlob,
		           			 Title = 'Units_' + state,
		            		 PathOnClient = 'Units_' + state + '.txt',
		            		 FirstPublishLocationId = libraryId);
				
			}
	
	
	}


}