/**
 * @description Trigger Handler for Contract Line Item object 
 * @author Janella Lauren Canlas
 * @date 27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas - Created.
 */
public with sharing class ContractLineItemTriggerHandler {
	/*
		This method will populate the CLI Record Type depending on the Record Type of the Related Purchasing Contract Record Type
	*/
	public static void populateRecordType(List<Contract_Line_Items__c> cliList){
		Set<Id> pcIds = new Set<Id>();
		List<Purchasing_Contracts__c> pcList = new List<Purchasing_Contracts__c>();
		List<RecordType> rtList = new List<RecordType>();
		Map<String,Id> rtMap = new Map<String,Id>();
		//collect ids of related purchasing contracts
		for(Contract_Line_Items__c c: cliList){
			pcIds.add(c.Purchasing_Contract__c);
		}
		//query purchasing record types and its equivalents for CLI SObject
		pcList = [SELECT id, RecordTypeId, RecordType.Name,Commencement_Date__c, Completion_Date__c FROM Purchasing_Contracts__c WHERE ID IN: pcIds];
		rtList = [SELECT Id, Name, SObjectType from RecordType Where Name IN('AHC Funded','Unit Funded') AND SobjectType='Contract_Line_Items__c'];
		//create a map of Record Type Name and Id
		for(RecordType r : rtList){
			if(!rtMap.containsKey(r.Name)){
				rtMap.put(r.Name,r.Id);
			}
		}
		//populate the record type, commencement date and completion date of CLI
		for(Contract_Line_Items__c c : cliList){
			for(Purchasing_Contracts__c p : pcList){
				if(c.Purchasing_Contract__c == p.Id){
					c.RecordTypeId = rtMap.get(p.RecordType.Name);
					c.Commencement_Date__c = p.Commencement_Date__c;
					c.Completion_Date__c = p.Completion_Date__c;
				}
			}
		}
	}
	
	
}