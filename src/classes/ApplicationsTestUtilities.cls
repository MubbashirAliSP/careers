/**
 * @description Test class utilities used by SAS team
 * @author Ranyel Maliwanag
 * @date 15.JUL.2015
 * @history
 */
@isTest
public without sharing class ApplicationsTestUtilities {
    public static void initialiseCountryAndStates() {
        Country__c australia = new Country__c(
                Country_Code__c = 'AUS',
                Name = 'Australia'
            );
        insert australia;

        State__c stateNSW = new State__c(
                Short_Name__c = 'NSW',
                Name = 'New South Wales',
                Code__c = '01',
                Country__c = australia.Id
            );
        insert stateNSW;

        State__c stateQLD = new State__c(
                Short_Name__c = 'QLD',
                Name = 'Queensland',
                Code__c = '03',
                Country__c = australia.Id
            );
        insert stateQLD;
    }


    public static void initialiseFormComponents() {
        ApplicationFormPage__c formPageQLD1 = new ApplicationFormPage__c(
                SortOrder__c = 1,
                State__c = 'QLD'
            );
        insert formPageQLD1;

        ApplicationFormPage__c formPageNSW1 = new ApplicationFormPage__c(
                SortOrder__c = 1,
                State__c = 'NSW'
            );
        insert formPageNSW1;


        ApplicationFormItem__c formItemQLDNSW0 = new ApplicationFormItem__c(
                Name = 'DeliveryMode',
                DisplayText__c = 'Delivery Mode',
                PageNumber__c = 1,
                QuestionType__c = 'Text',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW'
            );
        insert formItemQLDNSW0;


        ApplicationFormItem__c formItemQLDNSW1 = new ApplicationFormItem__c(
                DisplayText__c = 'First Name',
                PageNumber__c = 1,
                QuestionType__c = 'Text',
                IsRequired__c = true,
                SortOrder__c = 101,
                State__c = 'QLD;NSW'
            );
        insert formItemQLDNSW1;

        ApplicationFormItem__c formItemQLDNSW2 = new ApplicationFormItem__c(
                DisplayText__c = 'Last Name',
                PageNumber__c = 1,
                QuestionType__c = 'Text',
                IsRequired__c = true,
                SortOrder__c = 102,
                State__c = 'QLD;NSW',
                Name = 'LastName'
            );
        insert formItemQLDNSW2;

        ApplicationFormItem__c formItemQLD1 = new ApplicationFormItem__c(
                DisplayText__c = 'Has Pet Owl',
                PageNumber__c = 2,
                QuestionType__c = 'Checkbox',
                IsRequired__c = false,
                SortOrder__c = 103,
                State__c = 'QLD'
            );
        insert formItemQLD1;

        ApplicationFormItem__c formItemQLD2 = new ApplicationFormItem__c(
                DisplayText__c = 'Has Pet Owl',
                PageNumber__c = 2,
                QuestionType__c = 'Encrypted',
                IsRequired__c = false,
                SortOrder__c = 103,
                Name = 'TFN',
                State__c = 'QLD'
            );
        insert formItemQLD2;

        ApplicationFormItem__c formItemQLD3 = new ApplicationFormItem__c(
                DisplayText__c = 'Has Pet Owl',
                PageNumber__c = 2,
                QuestionType__c = 'Text',
                IsRequired__c = false,
                SortOrder__c = 104,
                State__c = 'QLD',
                Name = 'SupplyingYear12Cert'
            );
        insert formItemQLD3;

        ApplicationFormItem__c formItemNSW1 = new ApplicationFormItem__c(
                DisplayText__c = 'Has Pet Frog',
                PageNumber__c = 2,
                QuestionType__c = 'Checkbox',
                IsRequired__c = false,
                SortOrder__c = 103,
                State__c = 'NSW'
            );
        insert formItemNSW1;

        ApplicationFormItem__c formItemVIC1 = new ApplicationFormItem__c(
                DisplayText__c = 'Has Pet Mouse',
                PageNumber__c = 2,
                QuestionType__c = 'Checkbox',
                IsRequired__c = false,
                SortOrder__c = 103,
                State__c = 'VIC'
            );
        insert formItemVIC1;
    }


    public static void initialiseLogins() {
        Id studentRTID = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        Id brokerRTID = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Employee').getRecordTypeId();

        GuestLogin__c studentLogin = new GuestLogin__c(
                FirstName__c = 'Ronald',
                LastName__c = 'Weasley',
                Email__c = 'rweasley=test@hogwarts.edu.uk',
                Password__c = 'Scabbers',
                Phone__c = '12345678',
                AddressState__c = 'QLD',
                RecordTypeId = studentRTID
            );
        insert studentLogin;

        GuestLogin__c brokerLogin = new GuestLogin__c(
                FirstName__c = 'Bellatrix',
                LastName__c = 'Lestrange',
                Email__c = 'blestrange=test@ministryof.magic',
                Password__c = 'Scissors',
                Phone__c = '12345670',
                RecordTypeId = brokerRTID
            );
        insert brokerLogin;
    }

    public static void initialiseEnrolmentApplication() {
        initialiseCountryAndStates();
        initialiseFormComponents();
        initialiseLogins();
        initialiseQualifications();

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        GuestLogin__c student = [SELECT Id FROM GuestLogin__c WHERE RecordType.Name = 'Student'];
        Qualification__c qual = [SELECT Id From Qualification__c limit 1];

        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = student.Id,
                IsMainApplicationForm__c = true,
                StateId__c = stateQLD.Id,
                QualificationId__c = qual.Id
            );
        insert form;
    }

    public static void intiialiseApplicationCustomSettings() {
        List<String> enrolmentMappingStrings = new List<String>{
            'Student__c : ApplicationForm__c : StudentCredentialsId__r.AccountId__c : StudentCredentialsId__r.AccountId__r.Name : 00N90000004xMet',
            'Qualification__c : ApplicationForm__c : QualificationId__c : QualificationId__r.Name : 00N90000004xMnq',
            'Tax_File_Number__c : ApplicationForm__c : LinkedLoanFormId__r.LoanFormId__r.TaxFileNumber__c :  : 00N90000004xMo6',
            'Web_Referral_Source__c : ApplicationForm__c : ServiceCaseId__r.BrokerName__c :  : 00N9000000Bnku7',
            'Student2__c : RecordType : Dual Funding Enrolment :  : 00N90000004xMet',
            'Student3__c : String : Hello :  : 00N90000004xMet',
            'Student4__c : ApplicationFormItemAnswer__c : LastName :  : 00N90000004xMet'
        };
        List<String> accountMappingStrings = new List<String>{
            'LastName : ApplicationFormItemAnswer__c : LastName',
            'Reporting_Billing_State__c : ApplicationForm__c : StateId__c'
        };
        List<String> serviceCaseMappingStrings = new List<String>{
            'Category__c : ApplicationFormItemAnswer__c : LastName',
            'IsFormSubmittedWithUSI__c : ApplicationForm__c : IsFormSubmittedWithUSI__c',
            'Case_Origin__c : String : Web',
            'RecordTypeId : RecordType : Form'
        };
        List<String> opportunityMappingStrings = new List<String>{
            'Email__c : Account.PersonEmail'
        };

        List<ApplicationsEnrolmentFieldMapping__c> enrolmentMappingsForInsert = new List<ApplicationsEnrolmentFieldMapping__c>();
        for (String mappingString : enrolmentMappingStrings) {
            List<String> parsings = mappingString.split(':');
            ApplicationsEnrolmentFieldMapping__c mapping = new ApplicationsEnrolmentFieldMapping__c(
                    Name = parsings[0].trim(),
                    SourceObject__c = parsings[1].trim(),
                    SourceField__c = parsings[2].trim(),
                    AdditionalFieldQuery__c = parsings[3].trim(),
                    FieldId__c = parsings[4].trim()
                );
            enrolmentMappingsForInsert.add(mapping);
        }
        insert enrolmentMappingsForInsert;

        List<ApplicationStudentAccountMappings__c> accountMappingsForInsert = new List<ApplicationStudentAccountMappings__c>();
        for (String mappingString : accountMappingStrings) {
            List<String> parsings = mappingString.split(':');
            ApplicationStudentAccountMappings__c mapping = new ApplicationStudentAccountMappings__c(
                    Name = parsings[0].trim(),
                    SourceObject__c = parsings[1].trim(),
                    SourceField__c = parsings[2].trim()
                );
            accountMappingsForInsert.add(mapping);
        }
        insert accountMappingsForInsert;

        List<ApplicationsServiceCaseFieldMappings__c> serviceCaseMappingsForInsert = new List<ApplicationsServiceCaseFieldMappings__c>();
        for (String mappingString : serviceCaseMappingStrings) {
            List<String> parsings = mappingString.split(':');
            ApplicationsServiceCaseFieldMappings__c mapping = new ApplicationsServiceCaseFieldMappings__c(
                    Name = parsings[0].trim(),
                    SourceObject__c = parsings[1].trim(),
                    SourceField__c = parsings[2].trim()
                );
            serviceCaseMappingsForInsert.add(mapping);
        }
        insert serviceCaseMappingsForInsert;

        List<ApplicationsOpportunityMappingSetting__c> opportunityMappingsForInsert = new List<ApplicationsOpportunityMappingSetting__c>();
        for (String mappingString : opportunityMappingStrings) {
            List<String> parsings = mappingString.split(':');
            ApplicationsOpportunityMappingSetting__c mapping = new ApplicationsOpportunityMappingSetting__c(
                    Name = parsings[0].trim(),
                    OpportunityField__c = parsings[1].trim()
                );
            opportunityMappingsForInsert.add(mapping);
        }
        insert opportunityMappingsForInsert;
    }


    public static Account initialiseBusinessAccounts() {
        Id businessRTID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

        Account businessAccount = new Account(
                Name = 'Test Business',
                RecordTypeId = businessRTID,
                BusinessIdentifier__c = 'TestBusiness'
            );
        return businessAccount;
    }


    public static void initialiseTriggerCustomSettings() {
        CentralTriggerSettings__c settings = CentralTriggerSettings__c.getInstance();
        settings.GuestLogin__c = true;
        settings.GuestLogin_CreateAccounts__c = true;
        insert settings;
    }


    public static void initialiseQualifications() {
        Qualification__c parentQualification = SASTestUtilities.createParentQualification();
        insert parentQualification;
    }


    public static GuestLogin__c createGuestLogin() {
        Id studentLoginRTID = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        GuestLogin__c record = new GuestLogin__c(
                FirstName__c = 'Tom',
                LastName__c = 'Riddle',
                Email__c = 'triddle@hogwarts.edu',
                Password__c = 'LordVoldemort',
                Phone__c = '123',
                RecordTypeId = studentLoginRTID
            );
        return record;
    }

    public static PortalVisibility__c createPortalVisibility(Id fundingStreamId, Id qualId) {
        PortalVisibility__c pv = new PortalVisibility__c();
        pv.Funding_Stream_Type__c = fundingStreamId;
        pv.Qualification__c = qualId;
        return pv;
    }
}