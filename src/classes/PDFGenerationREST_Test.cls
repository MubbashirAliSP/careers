/**
 * @description Test class for `PDFGenerationRest` class
 * @author Ranyel Maliwanag
 * @date 07.JAN.2016
 */
@isTest
public with sharing class PDFGenerationREST_Test {
	@testSetup
	static void setup() {
		Census__c census = SASTestUtilities.createCensus();
		insert census;
	}

	static testMethod void apiCensusTest() {
		Census__c census = [SELECT Id, EnrolmentId__c FROM Census__c LIMIT 1];
		List<Id> censusIdList = new List<Id>{census.Id};

		Test.startTest();
			Integer attachmentCount = [SELECT count() FROM Attachment WHERE ParentId = :census.Id];
			Integer oldAttachmentCount = attachmentCount;
			RestRequest request = new RestRequest();
			RestResponse response = new RestResponse();
			request.requestURI = '/services/apexrest/PDFGeneration';
			request.httpMethod = 'POST';
			RestContext.request = request;
			RestContext.response = response;
			PDFGenerationREST.doPost(censusIdList, 'Census__c');
		Test.stopTest();

		attachmentCount = [SELECT count() FROM Attachment WHERE ParentId = :census.Id];
		System.assert(attachmentCount > oldAttachmentCount);
	}

	static testMethod void apiEnrolmentTest() {
		Census__c census = [SELECT Id, EnrolmentId__c FROM Census__c LIMIT 1];
		Id enrolmentId = census.EnrolmentId__c;

		Test.startTest();
			Integer attachmentCount = [SELECT count() FROM Attachment WHERE ParentId = :enrolmentId];
			Integer oldAttachmentCount = attachmentCount;
			RestRequest request = new RestRequest();
			RestResponse response = new RestResponse();
			request.requestURI = '/services/apexrest/PDFGeneration';
			request.httpMethod = 'POST';
			RestContext.request = request;
			RestContext.response = response;
			PDFGenerationREST.doPost(new List<Id>{enrolmentId}, 'Enrolment__c');
		Test.stopTest();

		attachmentCount = [SELECT count() FROM Attachment WHERE ParentId = :enrolmentId];
		System.assert(attachmentCount > oldAttachmentCount);
	}
}