public without sharing class VFHStudentSearchHelper {
    
    public static Boolean hasStudent(Form__c f) {
        
            if(searchStudent(f) == null)
                return false;
            else
                return true;
        
    }
    
    public static Account getStudent(Form__c f) {
        
        return searchStudent(f);
    }
    
    public static Account createStudentLead(Form__c f) {
        
        Id sLeadRecTypeId = [Select Id From RecordType Where DeveloperName = 'Student_Lead'].Id;
        
        Id ReportingBillingState;
        Id ReportingOtherState;
        Id CountryId = [Select Id From Country__c Where Name = 'Australia'].Id;
        Id postalCountryId;
       
        String s = f.Residential_State__c;
        
        if(f.Residential_State__c != null && f.Residential_State__c!= '-------- Select a State --------')
            ReportingBillingState = [Select Id From State__c Where Name = : f.Residential_State__c OR Short_Name__c = :  f.Residential_State__c].Id;
        
        if(f.Postal_State__c != null && f.Postal_State__c != '-------- Select a State --------') {
            postalCountryId = CountryId;
            if(f.Postal_State__c == f.Residential_State__c) {
                ReportingOtherState = ReportingBillingState;
            } else {
                ReportingOtherState = [Select Id From State__c Where Name = : f.Postal_State__c OR Short_Name__c = : f.Postal_State__c].Id;
            }
        }
        
       
       
        
        Account person = new Account(Salutation = f.Salutation__c,
                                     FirstName = f.First_Name__c,
                                     Middle_Name__c = f.Middle_Name__c,
                                     LastName = f.Last_Name__c,
                                     PersonEmail = f.Email_Address__c,
                                     RecordTypeId = sLeadRecTypeId,
                                     
                                     PersonMobilePhone = f.Mobile_Phone_Number__c,
                                     PersonHomePhone = f.Home_Phone_Number__c,
                                     Fax = f.Fax_Number__c,
                                     
                                     
                                     Address_building_property_name__c = f.Address_building_property_name__c,
                                     Address_flat_unit_details__c = f.Address_flat_unit_details__c,
                                     Address_street_number__c = f.Address_street_number__c,
                                     Address_street_name__c = f.Address_street_name__c,
                                     Suburb_locality_or_town__c = f.Residential_Town_Suburb__c,
                                     Address_Post_Code__c = f.Residential_Postcode__c,
                                     Address_Country__c = CountryId,
                                     
                                     Postal_delivery_box__c = f.Postal_Street_Address__c,
                                     Postal_building_property_name__c = f.Postal_building_property_name__c,
                                     Postal_flat_unit_details__c = f.Postal_flat_unit_details__c,
                                     Postal_street_number__c = f.Postal_street_number__c,
                                     Postal_street_name__c = f.Postal_street_name__c,
                                     Postal_suburb_locality_or_town__c = f.Postal_Town_Suburb__c,
                                     Postal_Post_Code__c = f.Postal_Postcode__c,
                                     Postal_Country__c = postalCountryId,                 
                                     
                                     Reporting_Billing_State__c = ReportingBillingState,
                                     Reporting_Other_State__c = ReportingOtherState,
                                     Referral_Source__c = f.Referral_Source__c,
                                     Sex__c = f.Sex__c,
                                     PersonBirthdate = f.Date_of_Birth__c,
                                     Emergency_Contact_Name__c = f.Emergency_Contact_Name__c,
                                     Relationship_to_Emergency_Contact__c = f.Relationship_to_Emergency_Contact__c,
                                     Emergency_Contact_Phone_Number__c = f.Emergency_Contact_Home_Phone__c,
                                     Emergency_Contact_Mobile_Number__c = f.Emergency_Contact_Mobile_Phone__c,
                                     Main_Language_Spoken_at_Home__c = f.Language__c,
                                     Country_of_Birth__c = f.Country_of_Birth__c,
                                     //Citizenship_Status__c = f.Citizenship_Information__c,
                                     Highest_School_Level_Completed__c = f.Highest_School_Level_Completed__c,
                                     Year_Highest_Education_Completed__c = f.Year_Highest_Education_Completed__c, // JEA -  SEPT.07.2015 Added Year Highest Education Completed
                                     Former_Surname__c = f.Previous_Surname__c,
                                     Proficiency_in_Spoken_English__c = f.Proficiency_in_Spoken_English__c,
                                     //BillingCountry = f.Residential_Country__c
                                     Indigenous_Status__c = f.Aboriginal_and_Torres_Strait_Code_AVET__c,
                                     Disability_Type__c = f.Disability_Type_AVETMISS__c,
                                     Does_student_have_a_disability__c = f.Have_Disability__c,
                                     Unique_student_identifier__c = f.USI__c,
                                     Permission_Granted_to_Apply_For_USI__c = f.Permission_to_apply_for_USI__c,
                                     //S.Hadjinoor - added 25/11/2014
                                     Town_City_Of_Birth__c = f.Town_City_Of_Birth__c,
                                     Preferred_Contact_Method__c = f.Preferred_Contact_Method__c,
                                     Country_Studying_In__c = f.Country_Studying_In__c
                                     
                                     );
        insert person;
        
        return person;
        
    }
    
    /*Update student details, based on details changed on the form */
    public static void updateStudentLead(Account sLead, Form__c f) {
               
         Id ReportingBillingState;
         Id ReportingOtherState;
        
            if(f.Residential_State__c != null && f.Residential_State__c != '-------- Select a State --------')
                ReportingBillingState = [Select Id From State__c Where Name = : f.Residential_State__c OR Short_Name__c = : f.Residential_State__c].Id;
            
            if(f.Postal_State__c != null && f.Postal_State__c != '-------- Select a State --------') {
                if(f.Postal_State__c == f.Residential_State__c)
                    ReportingOtherState = ReportingBillingState;
                else
                    ReportingOtherState = [Select Id From State__c Where Name = : f.Postal_State__c OR Short_Name__c = : f.Postal_State__c ].Id;
            
            }
        
                
               sLead.Salutation = f.Salutation__c;
               sLead.FirstName = f.First_Name__c;
               sLead.Middle_Name__c = f.Middle_Name__c;
               sLead.LastName = f.Last_Name__c;
               sLead.PersonEmail = f.Email_Address__c;
              // sLead.RecordTypeId = sLeadRecTypeId; Recordtype not updated
               sLead.Address_building_property_name__c = f.Address_building_property_name__c;
               sLead.Address_flat_unit_details__c = f.Address_flat_unit_details__c;
               sLead.Address_street_number__c = f.Address_street_number__c;
               sLead.Address_street_name__c = f.Address_street_name__c;
               sLead.Suburb_locality_or_town__c = f.Residential_Town_Suburb__c;
               sLead.Address_Post_Code__c = f.Residential_Postcode__c;
               sLead.Reporting_Billing_State__c = ReportingBillingState;
               
               
               sLead.Postal_delivery_box__c = f.Postal_Street_Address__c;
               slead.Postal_building_property_name__c = f.Postal_building_property_name__c;
               slead.Postal_flat_unit_details__c = f.Postal_flat_unit_details__c;
               slead.Postal_street_number__c = f.Postal_street_number__c;
               slead.Postal_street_name__c = f.Postal_street_name__c;
               sLead.Postal_suburb_locality_or_town__c = f.Postal_Town_Suburb__c;
               sLead.Reporting_Other_State__c = ReportingOtherState;
               sLead.Postal_Post_Code__c = f.Postal_Postcode__c;
               

               sLead.Referral_Source__c = f.Referral_Source__c;
               sLead.Sex__c = f.Sex__c;
               sLead.PersonBirthdate = f.Date_of_Birth__c;
               sLead.Emergency_Contact_Name__c = f.Emergency_Contact_Name__c;
               sLead.Relationship_to_Emergency_Contact__c = f.Relationship_to_Emergency_Contact__c;
               sLead.Emergency_Contact_Phone_Number__c = f.Emergency_Contact_Home_Phone__c;
               sLead.Emergency_Contact_Mobile_Number__c = f.Emergency_Contact_Mobile_Phone__c;
               sLead.Main_Language_Spoken_at_Home__c = f.Language__c;
               sLead.Country_of_Birth__c = f.Country_of_Birth__c;
               //sLead.Citizenship_Status__c = f.Citizenship_Information__c;
               sLead.Highest_School_Level_Completed__c = f.Highest_School_Level_Completed__c;
               sLead.Year_Highest_Education_Completed__c = f.Year_Highest_Education_Completed__c;  // JEA -  SEPT.07.2015 Added Year Highest Education Completed
               sLead.Former_Surname__c = f.Previous_Surname__c;
               sLead.Proficiency_in_Spoken_English__c = f.Proficiency_in_Spoken_English__c;
               //sLead.BillingCountry = f.Residential_Country__c;
                sLead.Indigenous_Status__c = f.Aboriginal_and_Torres_Strait_Code_AVET__c;
                sLead.Disability_Type__c = f.Disability_Type_AVETMISS__c;
                sLead.Does_student_have_a_disability__c = f.Have_Disability__c;
                sLead.Unique_student_identifier__c = f.USI__c;
                sLead.Permission_Granted_to_Apply_For_USI__c = f.Permission_to_apply_for_USI__c;
                //S.Hadjinoor - added 25/11/2014
                sLead.Town_City_Of_Birth__c = f.Town_City_Of_Birth__c;
                sLead.Preferred_Contact_Method__c = f.Preferred_Contact_Method__c;
                slead.Country_Studying_In__c = f.Country_Studying_In__c;

               
               update sLead;
                                     
        
    }
    
    /*Convert Existing Student Lead into Actual Student
     * Additional Details will be copied from the form
     * Fields that are populated via student lead creation/update
     * will not be used for here. Only additional fields, available on student only
     */
    public static void convertStudentLead(Account sLead, Form__c f) {
        
        //Need to check, if Student Lead has open opp and close won it
        
        List<Qualification__c>attachedQual = new List<Qualification__c>([Select Id, Name, Qualification_Name__c From Qualification__c Where Id = : f.VET_Qualification__c]);
        
        List<Opportunity>openOpp = new List<Opportunity>([Select Id,
                                                                 Qualification__c,
                                                                 Enrolment_Stage__c,
                                                                 Enrolment_Complete__c,
                                                                 StageName
                                                          From Opportunity Where StageName != 'Closed Won' AND
                                                                                 StageName != 'Closed Won' AND
                                                                                 AccountId = : sLead.Id    AND
                                                                                 Qualification__c = : attachedQual.get(0).Id
                                                          order by lastmodifieddate
                                                          LIMIT 1
                                                          ]);
        Id sLeadRecTypeId = [Select Id From RecordType Where DeveloperName = 'PersonAccount'].Id;
        
        sLead.RecordTypeId = sLeadRecTypeId; // change Recordtype to student
        sLead.PersonBirthdate = f.Date_of_Birth__c;
        sLead.Sex__c = f.Sex__c;
        sLead.Does_student_have_a_disability__c = f.Have_Disability__c;
        sLead.Disability_Type__c = f.Disability_Type__c;
        sLead.Country_of_Birth__c = f.Country_of_Birth__c;
        sLead.Citizenship_Status__c = f.Citizenship_Information__c;
        sLead.Main_Language_Spoken_at_Home__c =  f.Language__c;
        sLead.Proficiency_in_Spoken_English__c = f.Proficiency_in_Spoken_English__c;
        sLead.Year_of_Arrival_in_Australia__c = f.Year_Arrived_in_Australia__c;  
        sLead.Labour_Force_Status_1__c = f.Labour_Force_Status_Code__c;
        sLead.Emergency_Contact_Name__c = f.Emergency_Contact_Name__c;
        sLead.Emergency_Contact_Phone_Number__c = f.Emergency_Contact_Home_Phone__c;
        sLead.Emergency_Contact_Mobile_Number__c = f.Emergency_Contact_Mobile_Phone__c;
        sLead.Relationship_to_Emergency_Contact__c = f.Relationship_to_Emergency_Contact__c;
        sLead.BillingCountry = f.Residential_Country__c;
        sLead.Referral_Source__c = f.Referral_Source__c;
        sLead.Sex__c = f.Sex__c;
        sLead.PersonBirthdate = f.Date_of_Birth__c;
        sLead.Emergency_Contact_Name__c = f.Emergency_Contact_Name__c;
        sLead.Relationship_to_Emergency_Contact__c = f.Relationship_to_Emergency_Contact__c;
        sLead.Emergency_Contact_Phone_Number__c = f.Emergency_Contact_Home_Phone__c;
        sLead.Emergency_Contact_Mobile_Number__c = f.Emergency_Contact_Mobile_Phone__c;
        sLead.Main_Language_Spoken_at_Home__c = f.Language__c;
        sLead.Country_of_Birth__c = f.Country_of_Birth__c;
        //sLead.Citizenship_Status__c = f.Citizenship_Information__c;
        sLead.Highest_School_Level_Completed__c = f.Highest_School_Level_Completed__c;
        sLead.Year_Highest_Education_Completed__c = f.Year_Highest_Education_Completed__c;    // JEA -  SEPT.07.2015 Added Year Highest Education Completed
        sLead.Former_Surname__c = f.Previous_Surname__c;
        
        sLead.Indigenous_Status__c = f.Aboriginal_and_Torres_Strait_Code_AVET__c;
        sLead.Disability_Type__c = f.Disability_Type_AVETMISS__c;
        sLead.Does_student_have_a_disability__c = f.Have_Disability__c;
        sLead.Unique_student_identifier__c = f.USI__c;
         sLead.Permission_Granted_to_Apply_For_USI__c = f.Permission_to_apply_for_USI__c;

        
        try {update sLead;} catch(Exception e) { ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));}
        
        
        if(openOpp.size() == 1)  //close won opportunity
            closeWonOpportunity(openOpp.get(0));
        
        
    }
    
    @Testvisible private static void closeWonOpportunity(Opportunity o) {
        
        o.Enrolment_Complete__c = true;
        o.Enrolment_Stage__c = 'Enrolment Complete';
        o.StageName = 'Closed Won';
        
        try {update o;} catch(Exception e) { ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));}
        
    }
    
    private static Account searchStudent(Form__c f) {
            
        List<Account>resultsStudents = new List<Account>();
        String fname = f.First_Name__c + ' ' + f.Last_Name__c;
        
        //USE SOSL to search for leads and contacts
            resultsStudents =  [Select id,FirstName,LastName,PersonEmail,RecordTypeId,PersonMobilePhone,
                                       PersonMailingStreet,
                                       PersonMailingState,
                                       PersonMailingPostalCode,
                                       PersonMailingCountry,
                                       PersonMailingCity,
                                       PersonOtherStreet,
                                       PersonOtherState,
                                       PersonOtherPostalCode,
                                       PersonOtherPhone,
                                       PersonOtherCountry,
                                       PersonOtherCity,
                                       Age_Group__c,
                                       Phone,
                                       Fax,
                                       BillingCountry,
                                       Referral_Source__c,
                                       Sex__c,
                                       PersonBirthdate,
                                       Emergency_Contact_Name__c,
                                       Relationship_to_Emergency_Contact__c,
                                       Emergency_Contact_Phone_Number__c,
                                       Emergency_Contact_Mobile_Number__c,
                                       Main_Language_Spoken_at_Home__c,
                                       Country_of_Birth__c,
                                       //Citizenship_Status__c,
                                       Highest_School_Level_Completed__c,
                                       Former_Surname__c,
                                       Proficiency_in_Spoken_English__c
                                From Account
                                Where IsPersonAccount = true AND Name = : fname AND PersonEmail = :f.Email_Address__c  AND Recordtype.DeveloperName = 'Student_Lead' ORDER BY lastmodifieddate  DESC LIMIT 1 ];
       
         
        
         if(resultsStudents.size() == 0) {
            return null;
         }
         else {
            Account person = resultsStudents.get(0);
            return person;
         } 
         
        
    }
    
}