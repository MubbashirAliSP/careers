/*Controls creation of Transaction Line Item - Item Only
 * #Invoicing
 */

public with sharing class TransactionLineItemControllerExt {
	
	private final Transaction_Line_Item__c item;
	
	public Double totalAmount {get;set;}
	
	public TransactionLineItemControllerExt(ApexPages.StandardController stdController) {
        this.item = (Transaction_Line_Item__c)stdController.getRecord();
        //Need to retrieve account attached
        Account sAccount = new Account();
        sAccount = [Select Id, RecordType.Name From Account Where Id = : ApexPages.currentPage().getParameters().get('accId')];
        
        if(item.Account_Transaction__c == null)
        	item.Account_Transaction__c = ApexPages.currentPage().getParameters().get('acctranId');
        if(item.RecordTypeId == null)
        	item.RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        if(item.Student__c == null && sAccount.RecordType.Name == 'Student')
            item.Student__c = ApexPages.currentPage().getParameters().get('accId');
        
        totalAmount = 0.0;
    }
    
    /*Only required for manual page */
    public void calculateTotalAmount() {
    	
        Double gstTotal = (item.Quantity__c * item.Amount_Per_Item__c) * 0.10;
    	
    	if(item.Apply_Gst__c) {
    		totalAmount = (item.Quantity__c * item.Amount_Per_Item__c) + gstTotal;
    		item.Tax_Attributable__c = gstTotal;
    	}
    	else {
    		totalAmount = item.Quantity__c * item.Amount_Per_Item__c;
    		item.Tax_Attributable__c = 0.0;
    		
    	}
    	
    	item.Amount__c = item.Quantity__c * item.Amount_Per_Item__c;
    	
    }

}