/**
 * @description Batch process for marking Enrolments that have finished a stage without eligibility for the next one as Suspended
 * @author Ranyel Maliwanag
 * @date 13.DEC.2015
 */
public without sharing class StageSuspensionBatch implements Database.Batchable<SObject> {
	public String queryString;

	public StageSuspensionBatch() {
		// Progression information for enrolments are stored on Census records
		queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Census__c);
		queryString += 'WHERE EndDate__c = YESTERDAY AND EnrolmentId__r.Enrolment_Status__c LIKE \'Active%\' AND (NOT EnrolmentId__r.Student_Status_Code__r.Code__c LIKE \'5%\')';
	}


	/**
	 * @description start() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 13.DEC.2015
	 */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}


	/**
	 * @description execute() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 13.DEC.2015
	 */
	public void execute(Database.BatchableContext BC, List<Census__c> records) {
		List<Enrolment__c> enrolmentsForSuspension = new List<Enrolment__c>();

		// Build enrolments map
		Map<Id, Enrolment__c> enrolmentMap = new Map<Id, Enrolment__c>();
		Set<Id> enrolmentIds = new Set<Id>();
		for (Census__c record : records) {
			enrolmentIds.add(record.EnrolmentId__c);
		}
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
		queryString += 'WHERE Id IN :enrolmentIds';
		for (Enrolment__c enrolment : Database.query(queryString)) {
			enrolmentMap.put(enrolment.Id, enrolment);
		}

		// Go through census records
		for (Census__c record : records) {
			// Mark enrolment as suspended due to non-progression
			Enrolment__c enrolment = enrolmentMap.get(record.EnrolmentId__c);
			String stage = String.valueOf(record.Stage__c).replace('Stage ', '').trim();
			Integer stageInt = Integer.valueOf(stage);
			stageInt++;
			try {
				if (!Boolean.valueOf(enrolment.get('Eligible_for_Stage_' + String.valueOf(stageInt) + '__c'))) {
					enrolment.Enrolment_Status__c = 'Suspended from S' + String.valueOf(stageInt);
					enrolment.SuspendOnCensus__c = record.Id;
					enrolmentsForSuspension.add(enrolment);
				}
			} catch (SObjectException e) {
				// For fields not found
				// Silent exception
			}
		}

		update enrolmentsForSuspension;
	}


	/**
	 * @description finish() method implementation of the Database.Batchable interface
	 * @author Ranyel Maliwanag
	 * @date 13.DEC.2015
	 */
	public void finish(Database.BatchableContext BC) {
	}
}