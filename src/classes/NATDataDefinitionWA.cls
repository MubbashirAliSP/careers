public class NATDataDefinitionWA {
    
    //Common Fields - Utility
    public static final String BlankField = 'BlankField';
    
     // NAT00010 Fields - Award Object
    public static String PublicPrivateTrainingOrganisationFlag = 'PublicPrivateTrainingOrganisationFlag';
    public static String DateOfIssue = 'Date_Issued__c';
    
     // NAT00030 Fields - Qualification Object
     public static final String ParentQualificationCode = 'Parent_Qualification_Code__c';
     public static final String ReportableQualificationHours = 'WA_Reportable_Qualification_Hours__c';
    
    // NAT00060 Fields - Unit Object
    public static final String SubjectIdentifier = 'Name';
    public static final String NominalHours = 'NominalHours';
           
    // NAT00080 Fields - Account Object
    
    public static final String PersonMailingCity = 'PersonMailingCity';
    public static final String Addressstreetnumber = 'Address_street_number__c';
    public static final String Addressstreetname = 'Address_street_name__c';
    public static final String AddressDwellingTypeIdentifier = 'Address_Dwelling_Type_Identifier__c';
    public static final String Addressflatunitdetails = 'Address_Unit_Number__c';
    public static final String AddressLotNumber = 'Address_Lot_Number__c';
    public static final String AddressFloor = 'Address_Floor__c';
    public static final String AddressRoadTypeIdentifier = 'Address_Road_Type_Identifier__c';
    public static final String AddressRoadSuffixIdentifier = 'Address_Road_Suffix_Identifier__c';
    public static final String StatisticalAreaLevel1Identifier='';
    public static final String StatisticalAreaLevel2Identifier='';
    public static final String EnglishMainLanguageFlag='EnglishMainLanguageFlag'; //dependent on Main Language Spoken at Home on Account
    public static final String EducationIdentifier = 'Learner_Unique_Identifier__c';
    //public static final String EducationIdentifier='Education_Identifier__c';

    // NAT00085 Fields - Account Object
        
    public static String PostalDwellingTypeIdentifier = 'Postal_Dwelling_Type_Identifier__c';
    public static String PostalLotNumber = 'Postal_Lot_Number__c';
    public static String PostalFloor = 'Postal_Floor__c';
    public static String PostalRoadTypeIdentifier = 'Postal_Road_Type_Identifier__c';
    public static String PostalRoadSuffixIdentifier = 'Postal_Road_Suffix_Identifier__c';
    public static String PostalStreetNumber = 'Postal_street_number__c';
    public static String PostalStreetName = 'Postal_street_name__c';
    public static String PostalFlatUnitDetails = 'Postal_Unit_Number__c';
    public static String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static String PostalPostCode = 'PersonOtherPostalCode';
    public static String ReportingOtherStateIdentifier = 'Reporting_Other_State_Identifier__c';
    public static String PersonHomePhone = 'PersonHomePhone';
    public static String PersonWorkPhone = 'Work_Phone__c';
    public static String PersonMobilePhone = 'PersonMobilePhone';
    public static String PersonEmail = 'PersonEmail';
    
     // NAT00120 Fields - Enrolment Unit Object
    
 
    public static String HoursAttended = 'BlankField';
    public static String PurchasingContractIdentifier = 'Purchasing_Contract_Identifier__c';
    public static String ResourceFee = 'Client_Fees_Other__c';
    public static String OutcomeDate = 'End_Date__c';
    public static String CourseFee = 'Reportable_Tuition_Fee__c'; 

    public static String FeeExemptionTypeIdentifier = 'Enrolment__r.Fee_Exemption__r.Code__c'; 
    public static String FeeMaintenanceFlag = 'Enrolment__r.Fee_Maintenance_Flag__c';
    public static String ProgramIdentifier = 'Enrolment__r.Qualification__r.Parent_Qualification_Code__c';
    public static String TrainingContractIdentifier = 'Enrolment__r.Training_Contract_Identifier__c';
    public static String SpecificFundingIdentifier = 'Enrolment__r.Specific_Program_Identifier__c';
    public static String AssociatedCourseIdentifier = 'Enrolment__r.Qualification__r.Associated_Course_Identifier__c';
    public static String EnrolledSchoolIdentifier = 'Enrolment__r.Enrolled_School_Identifier__c';
    public static String TrainingTypeIdentifier = 'Enrolment__r.Training_Type_Identifier__c';
    public static String DeliveryModeIdentifierState = 'Delivery_Mode_Type_State__r.Code__c';
    public static String EnrolmentCategoryIdentifier = 'Enrolment__r.Enrolment_Category_Identifier__c';
    
    public static String StateFundSourceIdentifier = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c';
    public static String FundingSourceNational = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c';
    
    public static String SpecificFundingIdentifierState = 'SpecificFundingIdentifierState'; //what actually goes here??
    public static String PurchasingContractScheduleIdentifier = 'BlankField';
    
    public static String EducationTrainingClientIdentifier = 'Enrolment__r.Student__r.Education_Training_Client_Identifier__c';
    public static String IncomeContigentLoanLiability = 'Total_Amount_Charged__c';
    public static String CensusDate = 'Census_Date__c'; // batch class dodgyness
    public static String RollIdentifier = 'Intake_Code__c';// batch class dodgyness
    

    //NAT130 Award object
    public static String EducationAndTrainingInternationalFlag = 'Education_Training_International_Flag__c'; //update once its created
   
    public static Map< Integer, NATDataElement > nat00010() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00010();
        positions.put(13, new NATDataElement(PublicPrivateTrainingOrganisationFlag, 1, true, 'string' ) );  
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00020() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00020();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00030() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00030();
        positions.put(9, new NATDataElement(ParentQualificationCode, 10, FALSE, 'string' ) ); 
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, FALSE, 'number' ) ); 
        
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00060() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00060();
        positions.put(6, new NATDataElement(NominalHours, 4, true, 'number') ); 
        positions.put(7, new NATDataElement(SubjectIdentifier, 12, false, 'string') );
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00080() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00080();
        positions.put(16, new NATDataElement(PersonMailingCity, 46, false, 'string' ) );
        positions.put(20, new NATDataElement(AddressDwellingTypeIdentifier, 4, true, 'string' ) );
        positions.put(21, new NATDataElement(Addressflatunitdetails, 7, false, 'string' ) );
        positions.put(22, new NATDataElement(AddressFloor,10, false, 'string' ) );
        positions.put(23, new NATDataElement(Addressstreetnumber, 15, false, 'string' ) );
        positions.put(24, new NATDataElement(AddressLotNumber, 6, false, 'string' ) );
        positions.put(25, new NATDataElement(Addressstreetname, 45, false, 'string' ) );
        positions.put(26, new NATDataElement(AddressRoadTypeIdentifier, 4, true, 'string' ) );
        positions.put(27, new NATDataElement(AddressRoadSuffixIdentifier, 2, true, 'string' ) );
        positions.put(28, new NATDataElement(BlankField, 11, true, 'string' ) );
        positions.put(29, new NATDataElement(BlankField, 9, true, 'string' ) );
        positions.put(30, new NATDataElement(EnglishMainLanguageFlag, 1, TRUE, 'string' ) ); //Logic - Set this value to Y,N, @ based on 'Main Language Spoken at Home' on Account. 
        positions.put(31, new NATDataElement(EducationIdentifier, 15, FALSE, 'string' ) );

        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00085() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00085();
        positions.put(6, new NATDataElement(PostalDwellingTypeIdentifier, 4, true, 'string') );
        positions.put(7, new NATDataElement(PostalFlatUnitDetails, 7, true, 'string') ); 
        positions.put(8, new NATDataElement(PostalFloor, 10, true, 'string' ) );
        positions.put(9, new NATDataElement(PostalStreetNumber, 15, true, 'string') );
        positions.put(10, new NATDataElement(PostalLotNumber, 6, true, 'string') );        
        positions.put(11, new NATDataElement(PostalStreetName, 45, true, 'string' ) );
        positions.put(12, new NATDataElement(PostalRoadTypeIdentifier, 4, true, 'string') );        
        positions.put(13, new NATDataElement(PostalRoadSuffixIdentifier, 2, true, 'string' ) ); 
        positions.put(14, new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ); 
        positions.put(15, new NATDataElement(PostalSuburbLocalityTown, 46, true, 'string' ) ); 
        positions.put(16, new NATDataElement(PostalPostCode, 4, true, 'string' ) ); 
        positions.put(17, new NATDataElement(ReportingOtherStateIdentifier, 2, false, 'number' ) ); 
        positions.put(18, new NATDataElement(PersonHomePhone, 20, false, 'string' ) ); 
        positions.put(19, new NATDataElement(PersonWorkPhone, 20, false, 'string' ) ); 
        positions.put(20, new NATDataElement(PersonMobilePhone, 20, false, 'string' ) );
        positions.put(21, new NATDataElement(PersonEmail, 80, false, 'string' ) );
        
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00090() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00090();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00100() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00100();
        return positions;
    }
    
    public static Map< Integer, NATDataElement > nat00120() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00120();
        
        positions.put(20, new NATDataElement(FeeExemptionTypeIdentifier, 2, true, 'string') );
        positions.put(22, new NATDataElement(PurchasingContractScheduleIdentifier, 3, true, 'string') ); 
        positions.put(23, new NATDataElement(HoursAttended, 4, true, 'string' ) );
        positions.put(25, new NATDataElement(EnrolledSchoolIdentifier, 10, true, 'string') );
        positions.put(26, new NATDataElement(TrainingTypeIdentifier, 1, true, 'string') );
        positions.put(27, new NATDataElement(DeliveryModeIdentifierState, 2, true, 'string') );
        positions.put(28, new NATDataElement(RollIdentifier, 10, true, 'string') ); //logic - Use @ if no roll no is used
        positions.put(29, new NATDataElement(EnrolmentCategoryIdentifier, 1, true, 'string') );
        positions.put(30, new NATDataElement(SpecificFundingIdentifierState, 10, true, 'string') );
        positions.put(31, new NATDataElement(EducationTrainingClientIdentifier, 10, true, 'string') );
        positions.put(32, new NATDataElement(CourseFee, 5, false, 'number') );
        positions.put(33, new NATDataElement(ResourceFee,5, false, 'number') );
        positions.put(34, new NATDataElement(IncomeContigentLoanLiability, 5, false, 'number') );
        positions.put(35, new NATDataElement(CensusDate, 8, true, 'string') );
        positions.put(36, new NATDataElement(OutcomeDate, 8, false, 'date') );
        positions.put(37, new NATDataElement(FeeMaintenanceFlag, 1, true, 'boolean') );
  
        return positions;
    }
    
    public static Map<Integer, NATDataElement> nat00130() {
        
        Map<Integer, NATDataElement > positions = NATDataDefinition.nat00130();
        positions.put(6, new NATDataElement(EducationAndTrainingInternationalFlag, 6, false, 'boolean') );
        return positions;
    }
        
}