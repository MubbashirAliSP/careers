global without sharing class NATValidationBatch_EnrolmentUnits implements Database.Batchable<sObject>,Database.Stateful {
    
    global String query;
    global NAT_Validation_Log__c log;
    global Integer recordCount = 0;
    global DateTime currentDT;
    
    global NATValidationBatch_EnrolmentUnits(Id logId) {
        
    log = [ SELECT ID, Validation_State__c, Training_Organisation_Id__c, Log_Run_At__c, query_main__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
		
        query = log.query_main__c;
        currentDT = log.Log_Run_At__c;

        String WAqueryLogic = ' OR Enrolment__r.Enrolled_School_Identifier__c = null OR ' +
                              'Enrolment__r.Training_Type_Identifier__c = null OR ' +
                              'Enrolment__r.Enrolment_Category_Identifier__c = null OR ' +
                              'Enrolment__r.Enrolment_Category_Identifier__c = \'X - Exam Only\' OR ' +
                              'Reportable_Tuition_Fee__c = null OR ' +
                              'WA_Resource_Fee__c = null OR ' +
                              '((Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'51\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'52\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'53\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'54\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'60\')) ';

        query = query +' AND (Delivery_Location_Identifier__c = null OR ' +
                         '      Scheduled_Hours__c = null OR ' +
                         '      Enrolment__r.Qualification_Identifier__c = null OR ' +
                         '      Enrolment__r.Start_Date__c = null OR ' +
                         '      Enrolment__r.End_Date__c = null OR ' +
                         '      (AVETMISS_National_Outcome__c = null AND Enrolment__r.Delivery_location_State__c != \'Victoria\' ) OR ' +
                         '      Commencing_Course_Identifier__c = null OR ' +
                         '      Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c = null OR ' +
                         '      Purchasing_Contract_Identifier__c = null OR ' +
                         '      VET_in_Schools__c = null OR ' +
                         '      Line_Item_Qualification_Unit__r.Nominal_Hours__c = null OR ' +
                         '      Study_Reason_Identifier__c = null OR ' +
                         '      (Enrolment__r.Victorian_Commencement_Date__c = null AND Enrolment__r.Delivery_location_State__c =  \'Victoria\' ) ' ;

          //add WA query conditions             
        if(log.Validation_State__c == 'Western Australia') {
            query = query +  WAqueryLogic + ' )';
        }

        else {
            query = query + ' )';           
        }
        
        
            
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(log.query_main__c);
            
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
        Set<String>nswAllowedOutcomes = new Set<String>{'20','30','40','51','53','60','66','70'};
        Set<String>waAllowedOutcomes = new Set<String>{'51','52','53','54','60'};

        
        try {
            
            for(SObject so : scope) {
                
                recordCount++;
                
                Enrolment_Unit__c eu = (Enrolment_Unit__c) so;
        
        		NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id,Record_Id__c = eu.Id, Enrolment_Id__c = eu.Enrolment__c, Enrolment_Name__c = eu.Enrolment__r.Name, Nat_File_Name__c = 'NAT00120',Parent_Record_Last_Modified_Date__c = eu.LastModifiedDate);
        
             if(eu.Delivery_Location_Identifier__c  == null)
                    event.Error_Message__c = 'Training Organisation Delivery Location Identifier cannot be blank';
                
                
             else if(eu.Scheduled_Hours__c == null) 
                     event.Error_Message__c = 'Scheduled Hours cannot be blank';
                   
             
                
             else if(eu.Enrolment__r.Qualification_Identifier__c == null)
                     event.Error_Message__c = 'Qualification Identifier cannot be blank';
                    
                
             else if(eu.Enrolment__r.Start_Date__c == null || eu.Enrolment__r.End_Date__c == null )   
                     event.Error_Message__c = 'Enrolment activity Start date Or End date  cannot be blank';        
                 
             else if(eu.AVETMISS_National_Outcome__c == null && eu.Enrolment__r.Delivery_location_State__c != 'Victoria')   
                     event.Error_Message__c = 'Outcome Identifier cannot be blank';
               
             else if(eu.Commencing_Course_Identifier__c == null) 
                     event.Error_Message__c = 'Commencing Course Identifier cannot be blank';
                    
             else if(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c == null)  
                     event.Error_Message__c = 'Funding Source Identifier cannot be blank';
               
             else if(eu.Purchasing_Contract_Identifier__c == null)   
                     event.Error_Message__c = 'Purchasing Contract Identifier cannot be blank';

             else if(eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null)
                     event.Error_Message__c = 'Line Item Qualification Nominal Hours Cannot be blank';
                    
             else if(eu.Enrolment__r.Victorian_Commencement_Date__c == null && eu.Enrolment__r.Delivery_location_State__c == 'Victoria')
                     event.Error_Message__c = 'Victorian Commencment Date (Located on Enrolment) cannot be blank';
                    
             else if(eu.Study_Reason_Identifier__c == null)      
                     event.Error_Message__c = 'Study Reason Identifier cannot be blank';

             else if(eu.Victorian_Tuition_Rate__c != null) {
                     if(String.ValueOf(Integer.ValueOf(eu.Victorian_Tuition_Rate__c)).length() > 4)
                     event.Error_Message__c = 'Victoria Tuition Rate cannot be greater than 4 digits';
             }

             else if((log.Validation_State__c == ' New South Wales') && !(nswAllowedOutcomes.contains(eu.AVETMISS_National_Outcome__c)))
                    event.Error_Message__c = 'New South Wales National Outcome Can only be 20,30,40,51,53,60,66,70';

                if(log.Validation_State__c == 'Western Australia') {
                   
                     if(eu.Enrolment__r.Enrolled_School_Identifier__c == null)
                            event.Error_Message__c = 'Enrolled School Identifier cannot be blank';
                    
                     else if(eu.Enrolment__r.Training_Type_Identifier__c == null)
                            event.Error_Message__c = 'Training Type Identifier cannot be blank';
        
                     else if(eu.Delivery_Mode_Identifier__c =='90' &&  !(waAllowedOutcomes.contains(eu.AVETMISS_National_Outcome__c)))
                            event.Error_Message__c = 'Outcome Identifier can only be 51,52,53,54,60 When Delivery Mode Identifier is 90';

                     else if(eu.Enrolment__r.Enrolment_Category_Identifier__c == null)
                            event.Error_Message__c = 'Enrolment Category Identifier cannot be blank';
        
                     else if(eu.Enrolment__r.Enrolment_Category_Identifier__c == 'X - Exam Only') {
                            if(eu.AVETMISS_National_Outcome__c == '70' ||  String.ValueOf(eu.End_Date__c.year()) != log.Collection_Year__c)
                              event.Error_Message__c = ' Activity End Date must be in this collection year and the Outcome Identifier - National must not be 70';
                     }
        
                     else if(eu.Reportable_Tuition_Fee__c == null)
                           event.Error_Message__c = 'Reportable Tuition Fee cannot be blank';
        
                     else if(eu.WA_Resource_Fee__c == null)
                           event.Error_Message__c = 'WA Resource Fee cannot be blank';

                }
                    
                if(event.Error_Message__c != null) {
                    errorEventList.add(event);
                }
       
    		}
            
            insert ErrorEventList;
            
            
    	}
        catch (exception e) {
            log.System_Message__c = e.getMessage();
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
         
        log.NAT00120_Record_Count__c = recordCount;
        
        update log;
        
        Database.executeBatch(new NATValidationBatch_Locations( log.id ) );

    }

}