public class BlackboardUtility {

     /*Helper method for retrieve token */
  public static String parseResult(XmlStreamReader reader) {
    
    String result;
    
    while(reader.hasNext()) {
        if (reader.getEventType() == XmlTag.END_ELEMENT) {
           break;
        } else if (reader.getEventType() == XmlTag.CHARACTERS) {
           result = reader.getText();
           break;
        }
        reader.next();
     }
      
  
      
    return result;
  
  }
  
  /*used for security header of the xml*/
  public static String createdDate() {
    
      return Datetime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
    
  }
  
  /*used for security header of the xml*/
  public static String expiredDate() {
    
    Datetime expiresDT = Datetime.now().addSeconds(Integer.ValueOf(Blackboard_Configuration__c.getInstance(getEndpoint()).expectedLifeSeconds__c));
       
        return expiresDT.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
    
  }
  
  public static void createBlackBoardLog(String sessionId) {
    
         
    Blackboard_Sync_Log__c log = new Blackboard_Sync_Log__c(session_Id__c = sessionId);
    
    insert log;    
  
  }
  
  
  public static void createBlackBoardSyncEvents(Id logId, String msg, String recId, String sts, String IntPoint) { /*Create event for each record processed */
    
     Blackboard_Sync_Event__c event = new Blackboard_Sync_Event__c(Blackboard_Sync_Log__c = logId, Message__c = msg, Record_Id__c = recId, Event_Status__c = sts, Integration_Point__c = IntPoint);
    
     insert event;
  
  }
  
  

  public static String getEndpoint() {
      
      if(UserInfo.getOrganizationId() == '00D90000000aUVdEAM') {
          
          return 'WS_Config Production';

      } 
      
      else {
      
          return 'WS_Config Sandbox';
      }
      
  
     
  }

}