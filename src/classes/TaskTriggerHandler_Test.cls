/**
 * @description Test class for TaskTriggerHandler
 * @author Ranyel Maliwanag
 * @date 12.MAY.2015
 * @history
 *       04.JUN.2015    Ranyel Maliwanag    - Updated inserted tasks to have `Status = Completed` as per updated requirements to consider only completed Tasks
 */
@isTest
public with sharing class TaskTriggerHandler_Test {
    static Id onlineTaskRTID = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId();

    @testSetup static void triggerSetup() {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User adminUser = new User(
                ProfileId = adminProfile.Id,
                Username = 'adminUser@testing.org',
                Email = 'adminUser@testing.org',
                LastName = 'Telesales',
                Alias = 'tsu',
                TimeZoneSidKey = 'Australia/Brisbane',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
        insert adminUser;

        System.runAs(adminUser) {
            Lead leadRecord = new Lead(
                    LastName = 'Test Lead',
                    Status = 'Open',
                    Referral_Source_Type_Detail__c = 'Contactability'
                );
            insert leadRecord;

            Service_Cases__c serviceCaseRecord = new Service_Cases__c(
                    OwnerId = adminUser.Id
                );
            insert serviceCaseRecord;

            Account student = new Account(
                    FirstName = 'Godric',
                    LastName = 'Gryffindor',
                    Student_Identifer__c = 'CX1230',
                    PersonBirthdate = Date.newInstance(2015, 5, 19),
                    PersonEmail = 'person@inplace.com',
                    Middle_Name__c = 'Taylor',
                    Phone = '548242',
                    PersonMobilePhone = '745635',
                    Salutation = 'Mr'
                );
            insert student;

            Enrolment__c enrolment = new Enrolment__c(
                    Student__c = student.Id,
                    Enrolment_Status__c = 'Active (Commencement)',
                    Mailing_Country__c = 'Australia',
                    Mailing_Zip_Postal_Code__c = '4006',
                    Name_of_suburb_town_locality__c = 'Fortitude Valley',
                    Mailing_State_Provience__c = 'Queensland',
                    Address_street_number__c = '100',
                    Address_street_name__c = 'Brookes Street',
                    Address_flat_unit_details__c = 'Ground Floor',
                    Managed_by_GLS__c = true,
                    GLS_Enrolment_ID__c = 'EX1230'
                );
            insert enrolment;
        }
    }

    static testMethod void testUncompletedTask() {
        String queryString = 'SELECT Id, Total_Tasks__c ';
        queryString += 'FROM Lead ';
        Lead leadRecord = Database.query(queryString);

        System.assert(leadRecord.Total_Tasks__c == 0 || leadRecord.Total_Tasks__c == null);

        Task testTask = new Task(
                WhoId = leadRecord.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'Call Back',
                Subject = 'Call'
            );
        insert testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);

        testTask.Contact_Outcome__c = 'Not Registered';
        update testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);

        delete testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);
    }

    static testMethod void testCompletedTask() {
        String queryString = 'SELECT Id, Total_Tasks__c ';
        queryString += 'FROM Lead ';
        Lead leadRecord = Database.query(queryString);

        System.assert(leadRecord.Total_Tasks__c == 0 || leadRecord.Total_Tasks__c == null);

        Task testTask = new Task(
                WhoId = leadRecord.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'Call Back',
                Subject = 'Call'
            );
        insert testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);

        testTask.Contact_Outcome__c = 'Not Registered';
        testTask.Status = 'Completed';
        update testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        delete testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);


        testTask = new Task(
                WhoId = leadRecord.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'Call Back',
                Subject = 'Call',
                Status = 'Completed'
            );
        insert testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        testTask.Contact_Outcome__c = 'Not Registered';
        update testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        delete testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);


        testTask = new Task(
                WhoId = leadRecord.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'Call Back',
                Subject = 'Call',
                Status = 'Completed'
            );
        insert testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        testTask.Contact_Outcome__c = 'Not Registered';
        testTask.Status = 'New';
        update testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);

        delete testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);
    }

    static testMethod void testContactAttempted() {
        String queryString = 'SELECT Id, Total_Tasks__c ';
        queryString += 'FROM Lead ';
        Lead leadRecord = Database.query(queryString);

        System.assert(leadRecord.Total_Tasks__c == 0 || leadRecord.Total_Tasks__c == null);

        Task testTask = new Task(
                WhoId = leadRecord.Id,
                Contact_Type__c = 'Not Contacted',
                Contact_Outcome__c = 'No Answer',
                Subject = 'Call',
                Status = 'Completed'
            );
        insert testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        testTask.Contact_Outcome__c = 'Not Registered';
        update testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(1, leadRecord.Total_Tasks__c);

        delete testTask;

        leadRecord = Database.query(queryString);
        System.assertEquals(0, leadRecord.Total_Tasks__c);
    }

    static testMethod void testDefaultContactTypeAndOutcome() {
        Service_Cases__c serviceCaseRecord = [SELECT Id FROM Service_Cases__c];
        Task testTask = new Task(
                WhatId = serviceCaseRecord.Id,
                Subject = 'Email: ',
                Status = 'Completed'
            );
        insert testTask;

        testTask = [SELECT Id, Contact_Type__c, Contact_Outcome__c FROM Task WHERE Id = :testTask.Id];
        System.assertEquals('Email', testTask.Contact_Type__c);
        System.assertEquals('Email Sent', testTask.Contact_Outcome__c);
    }

    // Insert test - Positive
    static testMethod void updateLastSuccessfulContactDateTest() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
        queryString += 'WHERE Student_Identifer__c = \'CX1230\'';
        Account testAccount = Database.query(queryString);

        String queryStringEn = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
        queryStringEn += 'WHERE Student__c = \'' + testAccount.Id + '\'';
        Enrolment__c enrolment = Database.query(queryStringEn);

        Test.startTest();
            System.assertEquals(null, testAccount.LastSuccessfulContactDate__c);

            // Insert task
            Task testTask = new Task(
                    WhatId = enrolment.Id,
                    Subject = 'Successful Contact',
                    Status = 'Completed',
                    Contact_Type__c = 'Phone Inbound - Successful'
                );
            insert testTask;

            testAccount = Database.query(queryString);
            System.assertNotEquals(null, testAccount.LastSuccessfulContactDate__c);
        Test.stopTest();
    }

    // Insert test - Negative
    static testMethod void updateLastSuccessfulContactDateTest2() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
        queryString += 'WHERE Student_Identifer__c = \'CX1230\'';
        Account testAccount = Database.query(queryString);

        Test.startTest();
            System.assertEquals(null, testAccount.LastSuccessfulContactDate__c);

            // Insert task
            Task testTask = new Task(
                    WhatId = testAccount.Id,
                    Subject = 'Unsuccessful Contact',
                    Status = 'Completed',
                    Contact_Type__c = 'Not Contacted'
                );
            insert testTask;

            testAccount = Database.query(queryString);
            System.assertEquals(null, testAccount.LastSuccessfulContactDate__c);
        Test.stopTest();
    }


    // Postive test
    static testMethod void updateEnrolmentOpenActivityCountTest() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
        Enrolment__c enrolment = Database.query(queryString);

        Test.startTest();
            System.assertEquals(null, enrolment.OpenActivityCount__c);

            Task testTask = new Task(
                    WhatId = enrolment.Id,
                    Subject = 'Successful Contact',
                    Status = 'Not Started',
                    Contact_Type__c = 'Phone Inbound - Successful',
                    RecordTypeId = onlineTaskRTID
                );
            System.debug('::: Sentinel Start');
            insert testTask;
            System.debug('::: Sentinel End');

            enrolment = Database.query(queryString);
            System.assertEquals(1, enrolment.OpenActivityCount__c);
        Test.stopTest();
    }

    // Negative test - completed task
    static testMethod void updateEnrolmentOpenActivityCountTest2() {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
        Enrolment__c enrolment = Database.query(queryString);

        Test.startTest();
            System.assertEquals(null, enrolment.OpenActivityCount__c);

            Task testTask = new Task(
                    WhatId = enrolment.Id,
                    Subject = 'Successful Contact',
                    Status = 'Completed',
                    Contact_Type__c = 'Phone Inbound - Successful',
                    RecordTypeId = onlineTaskRTID
                );
            insert testTask;

            enrolment = Database.query(queryString);
            System.assertEquals(0, enrolment.OpenActivityCount__c);
        Test.stopTest();
    }
}