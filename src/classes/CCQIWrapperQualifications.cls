public with sharing class CCQIWrapperQualifications {
	
	public Qualification__c Qual {get;set;}
    	public Integer EC {get;set;}
    	public Integer CC {get;set;}
    	
    	public CCQIWrapperQualifications(Qualification__c q, Integer e_count, Integer c_count) {
    		
    		Qual = q;
    		EC = e_count;
    		CC = c_count;
    		
    	}

}