public class UnitsOfStudyAllocationWrapper {
	
	public Boolean selected {get;set;}
		public Unit__c unit {get;set;}
		public String Stage {get;set;}
		public String SelectedStage {get;set;}
		public String selectedType {get;set;}
		
		public List<SelectOption>getStages() {
		   			 List<SelectOption> options = new List<SelectOption>();
		   			 options.add(new SelectOption('Stage 1','Stage 1'));
		   			 options.add(new SelectOption('Stage 1a','Stage 1a'));
		   			 options.add(new SelectOption('Stage 1b','Stage 1b'));
		             options.add(new SelectOption('Stage 2','Stage 2'));
		             options.add(new SelectOption('Stage 2a','Stage 2a'));
		             options.add(new SelectOption('Stage 2b','Stage 2b'));
		             options.add(new SelectOption('Stage 3','Stage 3'));
		             options.add(new SelectOption('Stage 4','Stage 4'));
		             options.add(new SelectOption('Stage 5','Stage 5'));
		             options.add(new SelectOption('Stage 6','Stage 6'));
		             options.add(new SelectOption('Stage 7','Stage 7'));
		             options.add(new SelectOption('Stage 8','Stage 8'));
		             options.add(new SelectOption('Stage 9','Stage 9'));
		             options.add(new SelectOption('Stage 10','Stage 10'));
		   		 	
		   	 return options;
		   	}
		   	
		   	public List<SelectOption>getTypes() {
		   		 List<SelectOption> options = new List<SelectOption>();
		   		 
		   		     
            	 options.add(new SelectOption('Core','Core'));
            	 options.add(new SelectOption('Elective','Elective'));
            	
		   		 
		   		return options;
		   	}
		
		
		public UnitsOfStudyAllocationWrapper(Boolean sel, Unit__c u) {
			
			selected = sel;
			unit = u;
			Stage = SelectedStage;
		}
	}