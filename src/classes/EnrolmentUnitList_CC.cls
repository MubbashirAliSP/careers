/*------------------------------------------------------------
Author:        Jade Serrano
Company:       Cloud Sherpas
Description:   
Test Class:    EnrolmentUnitList_CC_Test
History
05 JUNE 2013     Jade Serrano    Created
29 AUGUST 2013   Warjie Malibago Added Result Code in the query for eusList
27 AUGUST 2014   Warjie Malibago Added Credit Transfer in the filter
28 AUGUST 2014   Warjie Malibago Added new functionalities; see WBM
29 AUGUST 2014   Warjie Malibago Added new method showEUOS; see WBM
22 APRIL  2015   Warjie Malibago Changed null to 0.00 for paid upfront; see WBM
17 Nov    2015   Biao Zhang      Only save records whose values have been changed; Refer case 00030708
14 JUNE   2016   Biao Zhang      Only VFH data team will be able to save
------------------------------------------------------------*/
public with sharing class EnrolmentUnitList_CC {
  
    public List<Enrolment_Unit_of_Study__c> eusList{get;set;}
    private Map<Id, Enrolment_Unit_of_Study__c> originEUSMap {get; set;} //a map for original EUS record, used to check if any field changed.

    String enrolmentId = '';
    public List<wrapperEUOS> wrapEUOS {get; set;}
    public Boolean forceBlank {get; set;}

    public EnrolmentUnitList_CC() {
        forceBlank = false;  
        showEUOS();
    }

    //VFH Data team profile ids, only they will be able to modify data via this page
    private Id VFHDataTeamRoleId {
        get {
            if(VFHDataTeamRoleId == null) {
                VFHDataTeamRoleId = [Select Id from UserRole Where Name = 'VFH Data Management' limit 1].Id;
            }
            system.debug(VFHDataTeamRoleId);
            return VFHDataTeamRoleId;
        }
        set;
    }
    
    /*
        Show EUOS records; WBM
    */
    public void showEUOS(){     
        enrolmentId = ApexPages.currentPage().getParameters().get('eId');
        wrapEUOS = new List<wrapperEUOS>();
        eusList = new List<Enrolment_Unit_of_Study__c>();
        originEUSMap = new Map<Id, Enrolment_Unit_of_Study__c>();
              
        List<Completion_Status_Type__c> cst = new List<Completion_Status_Type__c>();
        cst = [SELECT Id, Name FROM Completion_Status_Type__c WHERE Name = 'Unit to be commenced later in the year or still in process of completing' LIMIT 1];
        system.debug('%%cst: '+cst);
        
        eusList = [SELECT Id, Name, Enrolment_Unit__r.Unit_Result__r.Name, Unit_of_Study_Code__c, Unit_Name__c, Census_Date__c, EFTSL__c, Result__c, Total_amount_charged__c, Amount_paid_upfront__c, Unit_of_study_HELP_Debt_UI__c, Loan_Fee__c, Enrolment_Unit__r.Unit_Result__r.Result_Code__c, Recredit_Revision__c FROM Enrolment_Unit_of_Study__c WHERE Enrolment_Unit__r.Enrolment__r.Id =: enrolmentId ORDER BY Unit_of_Study_Code__c ASC];
        system.debug('%%eusListSIZE: '+eusList.size());
        system.debug('%%eusList: '+eusList);
        
        for(Enrolment_Unit_of_Study__c e: eusList){
            if(e.Result__c==null && e.Enrolment_Unit__r.Unit_Result__r.Name != 'Credit Transfer' && forceBlank == false){ //WBM 27.AUG.2014
                if(cst.size()>0){
                    system.debug('%%e.Name: '+e.Name);
                    e.Result__c = String.valueOf(cst[0].Id);                    
                }
            }
            originEUSMap.put(e.Id, e.clone(true, true, true, true));
        }
        
        for(Enrolment_Unit_of_Study__c euos: eusList){
            wrapEUOS.add(new wrapperEUOS(euos));
        }    
    }
    
    /*
        Removes EUOS data; WBM
    */
    public PageReference removeEUOS(){ 
        if(UserInfo.getUserRoleId() != VFHDataTeamRoleId) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sorry but only VFH Data Team are allowed to modify data from this page.'));
            return null;
        }

        Set<Id> selectedId = new Set<Id>();   
        for(wrapperEUOS c: wrapEUOS){
            if(c.selected == true){
                selectedId.add(c.euosList.Id);
                system.debug('**Selected: ' + c.euosList.Name);
            }
        }
        system.debug('**Selected ID: ' + selectedId);
        
        if(selectedId.size() > 0){
            forceBlank = true;
            List<Enrolment_Unit_of_Study__c> euosList2 = new List<Enrolment_Unit_of_Study__c>();
            euosList2 = [SELECT Id, Name, Census_Date__c, Result__c, Total_amount_charged__c, Amount_paid_upfront__c, Loan_Fee__c FROM Enrolment_Unit_of_Study__c WHERE Id IN: selectedId];
        
            for(Enrolment_Unit_of_Study__c euos: euosList2){
                euos.Census_Date__c = null;
                euos.Result__c = null;
                euos.Total_amount_charged__c = null;
                euos.Amount_paid_upfront__c = 0.00; //4/22 WBM; changed null to 0.00
                euos.Loan_Fee__c = null;
            }
            update euosList2;
        }    
        showEUOS();
        String url = '/'+enrolmentId;
        return new PageReference(url);        
    }
    
    public PageReference save(){
        system.debug(UserInfo.getUserRoleId());
        if(UserInfo.getUserRoleId() != VFHDataTeamRoleId) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sorry but only VFH Data Team are allowed to modify data from this page.'));
            return null;
        }

        List<Enrolment_Unit_of_Study__c> updateList = new List<Enrolment_Unit_of_Study__c>();
        for(Enrolment_Unit_of_Study__c e: eusList){
            if(e != originEUSMap.get(e.Id)){
                updateList.add(e);
            }
        }
        update updateList;
        
        String url = '/'+enrolmentId;
        return new PageReference(url);
    }
    
    public PageReference cancel(){      
        String url = '/'+enrolmentId;
        return new PageReference(url);
    }

    public class wrapperEUOS{
        public Enrolment_Unit_of_Study__c euosList {get; set;}
        public Boolean selected {get; set;}
        
        public wrapperEUOS(Enrolment_Unit_of_Study__c euosList){
            this.euosList = euosList;
            this.selected = false;
        }
    }
}