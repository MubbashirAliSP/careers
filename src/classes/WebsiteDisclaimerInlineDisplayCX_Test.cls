/**
 * @description Test class for WebsiteDisclaimerInlineDisplayCX
 * @author Ranyel Maliwanag
 * @date 25.JUN.2015
 */
@isTest
public with sharing class WebsiteDisclaimerInlineDisplayCX_Test {
    @testSetup
    static void controllerSetup() {
        Website_Disclaimer__c disclaimerOption = new Website_Disclaimer__c(
                WDO_Funding_Type__c = 'VFH',
                WDO_State__c = 'QLD',
                WDO_Disclaimer__c = 'This is a long text disclaimer',
                RecordTypeId = Schema.SObjectType.Website_Disclaimer__c.getRecordTypeInfosByName().get('Website Disclaimer Option').getRecordTypeId()
            );
        insert disclaimerOption;

        Website_Disclaimer__c disclaimer = new Website_Disclaimer__c(
                Website_Disclaimer_Option__c = disclaimerOption.Id,
                RecordTypeId = Schema.SObjectType.Website_Disclaimer__c.getRecordTypeInfosByName().get('Website Disclaimer').getRecordTypeId()
            );
        insert disclaimer;
    }

    static testMethod void testAssignment() {
        Website_Disclaimer__c disclaimer = [SELECT Id FROM Website_Disclaimer__c WHERE Website_Disclaimer_Option__c != null];

        Test.startTest();
            Test.setCurrentPage(Page.WebsiteDisclaimerInlineDisplay);
            ApexPages.StandardController stdController = new ApexPages.StandardController(disclaimer);
            WebsiteDisclaimerInlineDisplayCX controller = new WebsiteDisclaimerInlineDisplayCX(stdController);

            System.assertNotEquals(null, controller.activeRecord);
        Test.stopTest();
    }
}