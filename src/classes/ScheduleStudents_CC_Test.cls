/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for ScheduleStudents_CC Class
Test Class:
History
20/12/2012      Created         Sairah Hadjinoor
29/01/2013      Updated         Janella Canlas
05/02/2013      Updated         Cletz Cadiz
05/03/2013      Updated         Janella Canlas
08/26/2013      Updated         Warjie Malibago - added code for Tuition_Fee__c for higher test coverage
08/27/2013      Updated         Warjie Malibago - added code for populateTuitionFeeOnEUOS method for higher test coverage
08/27/2015      Updated         Juan Miguel de Guia - moved test.startTest nearer to sched2.save()
------------------------------------------------------------*/
@isTest(seeAllData=true)
private class ScheduleStudents_CC_Test {

    /*
        Test Schedule Students CC
    */ 
    static testMethod void testScheduleStudents_1() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account acc2 = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualifications__c lineItem2 = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            
            acc = TestCoverageUtilityClass.createStudent();
            acc2 = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItem2 = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Type_of_Attendance__c = cattend.Id; 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Enrolment_Status__c = 'Active (Commencement)';
            insert enrl;
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            
                        
            //enrl2 = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem2.Id, loc.Id, country.Id);
            //enrl2.Type_of_Attendance__c = cattend.Id; 
            //enrl2.Predominant_Delivery_Mode__c = del.Id;
            //enrl2.Enrolment_Status__c = 'Active (Commencement)';
            //insert enrl2;  
                      
                        
           // enrlUnit = [SELECT id, Start_Date__c, End_Date__c from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            System.debug('@@clas '+clas);
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);
            insert clEnrolment;
            PageReference testPage = Page.ScheduleStudentsPage1;
            testPage.getParameters().put('intakeId', intake.Id);
            Test.setCurrentPage(testPage);
            
            test.startTest();
            ScheduleStudents_CC sched = new ScheduleStudents_CC();
            //ScheduleStudents_CC.enrolmentListWrapper  newWrap = ScheduleStudents_CC.enrolmentListWrapper(enrl,false);
            try
            {
                sched.next();
            }
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('Please select an Enrolment') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            } 
            
            
            sched.searchValue = '01';
            sched.searchField = 'Number';
            sched.getEnrolments();
            
            sched.searchValue='Test student Account';
            sched.getSearchPicklist();
            sched.searchField ='Name';
            test.stopTest();
            sched.getEnrolments();
            sched.fillPage1Wrapper();
            
            sched.enrolmentId = enrl.Id;
            sched.next();
            
            //sched.getSearchPicklist();
            sched.cancel();
        }
    }
    
    static testMethod void testScheduleStudents_2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        //WM 08/26/2013 additional tuition fee code start
        Tuition_Fee__c tuition = new Tuition_Fee__c();
        Intake_Unit_of_Study_Tuition_Fee__c iustf = new Intake_Unit_of_Study_Tuition_Fee__c();
        Intake_Unit_of_Study__c ius = new Intake_Unit_of_Study__c ();
        //WM 08/26/2013 end
        system.runAs(u){
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            //WM 08/26/2013 additional tuition fee code start
            Id recrdType = [select Id from RecordType where Name = 'Tailored Qualification' and sObjectType ='Qualification__c' LIMIT 1].Id;
            qual.RecordTypeId = recrdType;
            update qual;
            tuition = TestCoverageUtilityClass.createTuitionFee(qual.Id, state.Id); 
            //WM 08/26/2013 end
            //eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(enrlUnitRec.Id, qunit.Id, fld.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id); 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            insert enrl;
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            enrl.Enrolment_Status__c = 'Active (Commencement)';
            //MAM 09/09/2013 update recordType start
            recrdType = [select Id from RecordType where Name = 'Dual Funding Enrolment' and sObjectType ='Enrolment__c' LIMIT 1].Id;
            enrl.RecordTypeId = recrdType;
            //MAM 09/09/2013 end
            update enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            //enrlUnit = [SELECT id, Start_Date__c, End_Date__c from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            //insert enrlUnit;
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            //WB 08/26/2013 additional tuition fee code start
            ius = TestCoverageUtilityClass.createIntakeUnitOfStudy(intakeUnit.Id, unit.Id);
            iustf = TestCoverageUtilityClass.createIntakeUnitofStudyTuitionFee(state.Id, ius.Id);
            //WB 08/26/2013 end
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);
            insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            System.debug('@@clas '+clas);
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);insert clEnrolment;
            system.debug('**NatCode: ' + enrlUnit.Unit_Result__r.National_Outcome_Code__c + ' ,Result: ' + enrlUnit.Unit_Result__c + ' ,Name: ' + enrlUnit.Unit_Result__r.Name);
            PageReference testPage = Page.ScheduleStudentsPage2;
            testPage.getParameters().put('intakeId', intake.Id);
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            ScheduleStudentsPage2_CC sched2 = new ScheduleStudentsPage2_CC();           
            sched2.getIntakeUnitPicklist();
            sched2.allCheck = true;
            sched2.unitCheck = true;
            sched2.unitCode = intakeUnit.Id;
            sched2.fillPageTable();
            sched2.back();
            sched2.cancel();
            system.debug('**Unit Check 2: ' + sched2.unitCheck);

            //WM 08/27/2013 additional codes start
            //List<Classes__c> clas2 = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            //sched2.populateTuitionFeeOnEUOS(clas2);
            //WM 08/27/2013 end
            test.startTest(); //JMDG moved test.startTest nearer to sched2.save(); 27 AUG 2015
            List<ScheduleStudentsPage2_CC.Page2Wrapper> wrapList = new List<ScheduleStudentsPage2_CC.Page2Wrapper>();
            ScheduleStudentsPage2_CC.Page2Wrapper newWrap = new ScheduleStudentsPage2_CC.Page2Wrapper(clas, true, '', clas.Start_Date_Time__c.format('EEEE'), clas.End_Date_Time__c.format('EEEE'));
            wrapList.add(newWrap);
            sched2.enrolWrapperList  = wrapList;
            sched2.save();
            test.stopTest();      
        }
    }
    
    /*
    static testMethod void testScheduleStudents_3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        Attendance_Types__c cattend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        //Class_Attendance_Type__c attend = new Class_Attendance_Type__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual2 = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();
            state = TestCoverageUtilityClass.createState(country.Id);
            state2 = TestCoverageUtilityClass.createState(country.Id);
            unitHours = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.Id, qunit.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            cattend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id); 
            enrl.Predominant_Delivery_Mode__c = del.Id;
            enrl.Type_of_Attendance__c = cattend.Id;
            insert enrl;
            attend = TestCoverageUtilityClass.createClassAttendanceType();
            enrl.Enrolment_Status__c = 'Active (Commencement)';
            update enrl;
                        
            //enrlUnit = [SELECT id, Start_Date__c, End_Date__c from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, enrlUnit.Id);insert enrlIntakeUnit;
            classConfig = TestCoverageUtilityClass.createClassConfiguration(intake.Id, intakeUnit.Id);
            staff = TestCoverageUtilityClass.createStaffMember(intake.Id,u.Id);
            
            Classes__c clas = [select id, Intake_Unit__c FROM Classes__c WHERE Intake__c =: intake.Id LIMIT 1];
            clas.Intake_Unit__c = intakeUnit.Id;
            clas.Start_Date_Time__c = date.today().addDays(30);
            clas.End_Date_Time__c = date.today().addMonths(5);
            update clas;
            
            System.debug('@@clas '+clas);
            clEnrolment = TestCoverageUtilityClass.createClassEnrolment(clas.Id, enrlIntakeUnit.Id, attend.Id);insert clEnrolment;
            
            PageReference testPage = Page.ScheduleStudentsPage2;
            testPage.getParameters().put('intakeId', intake.Id);
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            ScheduleStudentsPage2_CC sched2 = new ScheduleStudentsPage2_CC();
            sched2.unitCheck = true;
            ScheduleStudentsPage2_CC.Page2Wrapper newWrap = new ScheduleStudentsPage2_CC.Page2Wrapper(clas, true, '', clas.Start_Date_Time__c.format('EEEE'), clas.End_Date_Time__c.format('EEEE'));
            sched2.unitCode = intakeUnit.Id;
            sched2.save();
            test.stopTest();
        }
    }*/
}