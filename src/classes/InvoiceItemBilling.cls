/* This controls item billing for invoicing component
 * Cloud Sherpas, YG
 */
public with sharing class InvoiceItemBilling {
	
	private final Purchasing_Contracts__c contract = [Select Id, Invoicing_Model__c,Contract_Code__c,SF_Nominal_Amount__c,Training_Organisation__c From Purchasing_Contracts__c Where Id = : ApexPages.currentPage().getParameters().get('CId')];
	private final List<RecordType> recordtypes = new List<RecordType>([Select Id, Name From RecordType WHERE Name in ('Item', 'Batch Invoice') order by Name desc]);
	public Purchasing_Contracts__c pur_contract {get;set;}
	public List<Item__c> items {get;set;}
	public Item__c currentItem {get;set;}
	public string SelectedItemId { get; set; }
	public Account payer {get;set;}
	public Integer itemCount {get;set;}
	public decimal tamount {get;set;}
	public decimal tamountwithgst {get;set;}
	
	public InvoiceItemBilling() {
		
		pur_contract = contract;
		currentItem = new Item__c();
		items = new List<Item__c>();
		payer = new Account();
		
	}
	
	public pageReference addRow() {
		
		if(currentItem.selfitem__c != null) {
			Item__c newItem = [Select Id, Name,Product_Amount__c,Product_Name__c,Tax_Attributable__c,Total_Product_Amount__c FROM Item__c Where Id = : currentItem.selfitem__c];
			items.add(newItem);
		}
		return null;
	}
	
	public void deleteItem() {
		for(Integer i = 0; i < items.size(); i++) {
			if(items[i].Id == SelectedItemId) {
				items.remove(i);
				break;
			}
		}
		  
	}
	
	public pageReference cancel() {
		return new pageReference('/' + pur_contract.Id);
		
	}
	
	public pageReference back() {
		return new pageReference('/apex/InvoiceItemBilling?CId' + pur_contract.Id);
	}
	 
	public pageReference backToStudentFees() {
		return new pageReference('/apex/InvoiceStudentFees?cId=' + + pur_contract.Id);
	}
	
	public pageReference next() {
		
		if(items.size() == 0) {
			
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Add At Least 1 Item'));
			
			return null;
		}
		itemCount = items.size();
		tamount = 0.0;
		tamountwithgst = 0.0;
		 
		for(Item__c i : items) {
			tamount += i.Tax_Attributable__c;
			tamountwithgst += i.Total_Product_Amount__c;
		}
		return new PageReference('/apex/InvoiceItemBilling2');
	}
	
	public pageReference createInvoices() {
		
		List<Transaction_Line_Item__c> lineItems = new List<Transaction_Line_Item__c>();
		
		
		//cretate batch record
    	Invoice_Batch__c batch = new Invoice_Batch__c();
    	
    	insert batch;
    	
    	Account_Transaction__c acc_tran = new Account_Transaction__c(Payer__c = payer.parentId, Training_Organisation__c = contract.Training_Organisation__c,Invoice_Batch__c = batch.Id,Purchasing_Contract__c = pur_contract.Id, RecordTypeId = recordtypes.get(1).Id );
    	
    	insert acc_tran;
    	
    	for(Item__c i : items) {
    		
    		Transaction_Line_Item__c item = new Transaction_Line_Item__c(RecordTypeId = recordtypes.get(0).id, Account_Transaction__c = acc_tran.Id,Quantity__c = 1, Item__c = i.Id,Bypass_Enrolment_RTO_APEX_Validation__c = true);
    		lineItems.add(item);
    	}
    	
    	insert lineItems;
    	
    	return new PageReference('/' + batch.Id);
    	
    	
		return null;
	}
}