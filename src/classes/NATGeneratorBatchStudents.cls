/**
 * @description Batch Class for NAT00080 NAT00085 NAT00090 and NAT00100
 * @author 
 * @date 31.JUL.2013
 *
 * HISTORY
 * -31.JUL.2013    Created.
 * -28.OCT.2013    Michelle Magsarili Code Restructure to include NAT85 and to prevent CPU Time Limit Exceeded error
 * -29.OCT.2013    Michelle Magsarili Code restructure to prevent Heaps limit and CPU Time Limit Exceeded errors
 */
global class NATGeneratorBatchStudents implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global final String functionquery;
    private String currentState;
    public String libraryId;
    global String body80;
    global String body85;
    global String body90; 
    global String body100;
    
    
    global NATGeneratorBatchStudents(String q, String state) {
        functionquery = q;
        
        query = q;
        
        currentState = state;
        
        if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        
        
        /*We will delete all files in content folder for the state */
        
        //List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
        
        //query = q.replace('LIMIT 100000','ORDER BY Enrolment__r.Student__c LIMIT 100000');      
    }
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body80 = '';    
        body85 = '';    
        body90 = '';
        body100 = '';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        List<String>splitter = new List<String>();
        String Salutation = '';
        String MailingAddress = '';
        Set<Id> BatchStudentIds = new Set<Id>();
        
        for(SObject s :scope) { 
        	
        	Account st = (Account) s; 
        
            body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00080__c.getInstance('01').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Full_Student_Name__c, NAT00080__c.getInstance('02').Length__c,'')+
                            NATGeneratorUtility.newFormatTxtFile(st.Highest_School_Level_Comp_Identifier__c, NAT00080__c.getInstance('03').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Year_Highest_Education_Completed__c, NAT00080__c.getInstance('04').Length__c)+   
                            NATGeneratorUtility.formatOrPadTxtFile(st.Sex__c, NAT00080__c.getInstance('05').Length__c,'')+
                            
                            

                            NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(st.PersonBirthDate)), NAT00080__c.getInstance('06').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.PersonMailingPostalCode, NAT00080__c.getInstance('07').Length__c,'')+
                            NATGeneratorUtility.newFormatTxtFile(st.Indigenous_Status_Identifier__c, NAT00080__c.getInstance('08').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Main_Language_Spoken_at_Home_Identifier__c, NAT00080__c.getInstance('09').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Labour_Force_Status_Identifier__c, NAT00080__c.getInstance('10').Length__c)+
                            NATGeneratorUtility.newFormatTxtFile(st.Country_of_Birth_Identifer__c, NAT00080__c.getInstance('11').Length__c)+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Disability_Flag_0__c, NAT00080__c.getInstance('12').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Prior_Achievement_Flag__c, NAT00080__c.getInstance('13').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.At_School_Flag__c, NAT00080__c.getInstance('14').Length__c,'');
                            
                            if(currentState == 'Victoria' && st.Main_Language_Spoken_at_Home_Identifier__c != null) {
                            	Boolean isMatch = false;
                            	List<NAT00080_English_Languages__c> nat80el = NAT00080_English_Languages__c.getall().values();
                            	for(NAT00080_English_Languages__c n : nat80el) {
                            		if (st.Main_Language_Spoken_at_Home_Identifier__c == n.Code__c) {
                            			body80 += NATGeneratorUtility.newFormatTxtFile(' ', NAT00080__c.getInstance('15').Length__c);
                            			isMatch = true;
                            		}
                            	}
                            	if (!isMatch) {
	                            	body80 += NATGeneratorUtility.newFormatTxtFile(st.Proficiency_in_Spoken_English_Identifier__c, NAT00080__c.getInstance('15').Length__c);
                            	}             	
                            }
                            
                            else {
                            	body80 += NATGeneratorUtility.newFormatTxtFile(st.Proficiency_in_Spoken_English_Identifier__c, NAT00080__c.getInstance('15').Length__c);
                            }
                            
                            body80 += NATGeneratorUtility.formatOrPadTxtFile(st.PersonMailingCity, NAT00080__c.getInstance('16').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Unique_Student_Identifier__c, NAT00080__c.getInstance('17').Length__c,'');
                           
							// this is a whole of aust requirement, if Post Code = OSPC, state is always 99 - OTHER
                            
                            if (st.PersonMailingPostalCode != null) {
	                            if (st.PersonMailingPostalCode == 'OSPC') {
	                            	body80 += NATGeneratorUtility.formatOrPadTxtFile('99', NAT00080__c.getInstance('18').Length__c,'');
	                            }
	                            
	                            else {
	                            	body80 += NATGeneratorUtility.formatOrPadTxtFile(st.State_Identifier__c, NAT00080__c.getInstance('18').Length__c,'');
	                            }
                            }
                            
                            else {
                            	body80 += NATGeneratorUtility.formatOrPadTxtFile(st.State_Identifier__c, NAT00080__c.getInstance('18').Length__c,'');
                            }
                            
                            body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Address_building_property_name__c, NAT00080__c.getInstance('19').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_flat_unit_details__c, NAT00080__c.getInstance('20').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_number__c, NAT00080__c.getInstance('21').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_name__c, NAT00080__c.getInstance('22').Length__c,'');

                            if (currentState == 'Victoria') {
                                body80 += NATGeneratorUtility.formatOrPadTxtFile('', NAT00080__c.getInstance('23').Length__c,'');
                                body80 += NATGeneratorUtility.formatOrPadTxtFile('', NAT00080__c.getInstance('24').Length__c,'');
                                body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Victorian_Student_Number__c, NAT00080__c.getInstance('25').Length__c,'');
                                body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Client_Industry_of_Employment__r.Division__c, NAT00080__c.getInstance('29').Length__c,'');
                                body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Client_Occupation_Identifier__c, NAT00080__c.getInstance('28').Length__c,'');  
                            }
                                 
                            if (currentState == 'Queensland') {
                                body80 += NATGeneratorUtility.formatOrPadTxtFile(st.Learner_Unique_Identifier__c, NAT00080__c.getInstance('23').Length__c,'');
                                System.Debug('**LUI'+st.Learner_Unique_Identifier__c);
                            }
                            
                            body80 += '\r\n';

         
             
            //NAT85
            Salutation = !String.IsEmpty(st.Salutation) ? st.Salutation.Replace('.', '') : '';
            MailingAddress = '';
            if(!String.IsEmpty(st.PersonMailingStreet)){
                MailingAddress =  st.PersonMailingStreet.Replace('\r\n', ' ');
                MailingAddress =  MailingAddress.Replace('\n', ' '); // To remove all instances of newlines and carriage returns
            }
            
            body85 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00085__c.getInstance('01').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(Salutation, NAT00085__c.getInstance('02').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.FirstName, NAT00085__c.getInstance('03').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.LastName, NAT00085__c.getInstance('04').Length__c,'');
                        
                        if (currentState == 'Victoria' && st.Postal_street_number__c == null && st.Postal_street_name__c == null && st.Postal_suburb_locality_or_town__c == null) {

                            body85 += NATGeneratorUtility.formatOrPadTxtFile(st.Address_building_property_name__c, NAT00085__c.getInstance('05').Length__c,'')+
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_flat_unit_details__c, NAT00085__c.getInstance('06').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_number__c, NAT00085__c.getInstance('07').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Address_street_name__c, NAT00085__c.getInstance('08').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile('', NAT00085__c.getInstance('09').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Suburb_locality_or_town__c, NAT00085__c.getInstance('10').Length__c,'') +
                            NATGeneratorUtility.newformatTxtFile(st.Address_Post_Code__c, NAT00085__c.getInstance('11').Length__c);

                        }

                        else {

                            body85 += NATGeneratorUtility.formatOrPadTxtFile(st.Postal_building_property_name__c, NAT00085__c.getInstance('05').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Postal_flat_unit_details__c, NAT00085__c.getInstance('06').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Postal_street_number__c, NAT00085__c.getInstance('07').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Postal_street_name__c, NAT00085__c.getInstance('08').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Postal_delivery_box__c, NAT00085__c.getInstance('09').Length__c,'') +
                            NATGeneratorUtility.formatOrPadTxtFile(st.Postal_suburb_locality_or_town__c, NAT00085__c.getInstance('10').Length__c,'') +
                            NATGeneratorUtility.newformatTxtFile(st.PersonOtherPostalCode, NAT00085__c.getInstance('11').Length__c);
                        }

                        body85 += NATGeneratorUtility.formatOrPadTxtFile(st.Reporting_Other_State_Identifier__c, NAT00085__c.getInstance('12').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonHomePhone, NAT00085__c.getInstance('13').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.Work_Phone__c, NAT00085__c.getInstance('14').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonMobilePhone, NAT00085__c.getInstance('15').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(st.PersonEmail, NAT00085__c.getInstance('16').Length__c,'')+                        
                        '\r\n';
                   
            if(st.Disability_Flag_0__c == 'Y') {
            	if (st.Disability_Type__c != null ) {
	                splitter = st.Disability_Type__c.Split(';');
	                for(String sp: splitter) {
	                     body90 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT00090__c.getInstance('01').Length__c,'')+
	                               NATGeneratorUtility.formatOrPadTxtFile(sp.substring(0,2), NAT00090__c.getInstance('02').Length__c,'')+
	                            '\r\n';
	                }
            	}
                
            }
            
            if(st.Prior_Achievement_Flag__c == 'Y' && currentState != 'Victoria' ) {
            	if (st.Prior_Achievement_Type_s__c != null ) {
                splitter = st.Prior_Achievement_Type_s__c.Split(';');
	                for(String sp: splitter) {                        
	                     body100 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT000100__c.getInstance('01').Length__c,'')+
	                             NATGeneratorUtility.formatOrPadTxtFile(sp.substring(0,3), NAT000100__c.getInstance('02').Length__c,'')+
	                             '\r\n';
                	}
            	}
            }
                
            if(st.Prior_Achievement_Flag__c == 'Y' && currentState == 'Victoria') {
            	if (st.Prior_Education_Achievement_Recognition__c != null) {
	                splitter = st.Prior_Education_Achievement_Recognition__c.Split(';');
	                for(String sp: splitter) {                        
	                     body100 += NATGeneratorUtility.formatOrPadTxtFile(st.Student_Identifer__c, NAT000100__c.getInstance('01').Length__c,'')+
	                             NATGeneratorUtility.formatOrPadTxtFile(sp.substring(0,3), NAT000100__c.getInstance('02').Length__c,'')+
	                             NATGeneratorUtility.formatOrPadTxtFile(sp.right(1), NAT000100__c.getInstance('03').Length__c,'')+
	                             '\r\n';
	                }
            	}
            }
        }
        
       
    }


    global void finish(Database.BatchableContext BC) {
        
        Blob pBlob80 = Blob.valueof(body80);
        insert new ContentVersion(
            versionData =  pBlob80,
            Title = 'NAT00080',
            PathOnClient = '/NAT00080.txt',
            FirstPublishLocationId = libraryId);
        
        Blob pBlob85 = Blob.valueof(body85);
        insert new ContentVersion(
           versionData =  pBlob85,
           Title = 'NAT00085',
           PathOnClient = '/NAT00085.txt',
           FirstPublishLocationId = libraryId); 
        
       if(body90 == '') {
       		body90 = ' ';
       }  
        
        Blob pBlob90 = Blob.valueof(body90);
        insert new ContentVersion(
            versionData =  pBlob90,
            Title = 'NAT00090',
            PathOnClient = '/NAT00090.txt',
            FirstPublishLocationId = libraryId); 
        
        if(body100 == '')
            body100 = ' ';      
        
        Blob pBlob100 = Blob.valueof(body100);
        insert new ContentVersion(
            versionData =  pBlob100,
            Title = 'NAT00100',
            PathOnClient = '/NAT00100.txt',
            FirstPublishLocationId = libraryId);   
           
        
    }
    
}