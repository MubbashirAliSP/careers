/*Wrapper Class used to NAT Generator */

public with sharing class PurchasingContractView {
	
	 public Purchasing_Contracts__c pc {get;set;}
            public boolean selected {get;set;}
            
            public PurchasingContractView(Purchasing_Contracts__c contract, Boolean sel) {
                
                pc = contract;
                selected = sel;
            }
            
}