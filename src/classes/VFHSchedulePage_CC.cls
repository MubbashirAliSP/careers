/**
 * @description Controller for VFHSchedulePage
 * @author Janella Lauren Canlas
 * @date 21.JAN.2013
 *
 * HISTORY
 * - 21.JAN.2013    Janella Canlas 		- Created.
 * - 11.MAR.2013	Janella Canlas 		- Removed unnecessary codes
 */
 
public with sharing class VFHSchedulePage_CC {
    public Id qualId;
    public VFH_Controller__c vfhRec {get;set;}
    public Qualification__c qRec {get;set;}
    public String SessionId;
    public String ServerURL;
    public String congaURL {get;set;}
    
    public VFHSchedulePage_CC(){
    	//current qualification Id
        qualId = ApexPages.currentPage().getParameters().get('qualId');
        //sessionId and ServerURL passed from the custom button to be used on Conga URL
        SessionId = ApexPages.currentPage().getParameters().get('sessionId');
        ServerURL = ApexPages.currentPage().getParameters().get('serverurl');
        vfhRec = new VFH_Controller__c();
        vfhRec = getVHFCRecord();
        qRec = getQualRec();
    }
    //get VFH Controller record
    public VFH_Controller__c getVHFCrecord(){
        VFH_Controller__c vfh = new VFH_Controller__c();
        vfh = [SELECT Report_End_Date__c, Report_Start_Date__c, Id FROM VFH_Controller__c LIMIT 1];
        return vfh;
    }
    //get Qualification record
    public Qualification__c getQualRec(){
        Qualification__c q = new Qualification__c();
        q = [select Id, Name,Skills_Reform_Government_Subsidised_plac__c from Qualification__c where Id =: qualId];
        return q;
    }
    //go back to the intake detail page
     public PageReference cancel(){
        PageReference p = new PageReference('/'+qualId);
        p.setredirect(true);
        return p;
    }
    //generate conga link
    public PageReference generate(){
        //system.debug('QualInfo' + qrec.id + ' ' + qrec.name);
        Id templateId = [Select Id from APXTConga4__Conga_Template__c where APXTConga4__Name__c='VFH Schedule - SG'].Id;
        Id attachmentId = [Select Id from Attachment where ParentId =: templateId order by CreatedDate DESC LIMIT 1].Id;
        Id reportData1 = [select Id from Report where Name='VFH Report Data 1'].Id;
        Id reportData2 = [select Id from Report where Name='VFH Report Data 2'].Id;
        Id reportData3 = [select Id from Report where Name='VFH Report Data 3'].Id;
        /*congaURL = 'https://www.appextremes.com/apps/Conga/PM.aspx'+ 
                    '?SessionId='+ sessionId + 
                    '&ServerUrl='+ ServerURL + 
                    '&Id='+ qualId + 
                    '&ReportID=[Data1]00ON0000000WrIp?pv0='+ qRec.Name + 
                    ',[Data2]00ON0000000XGL9?pv0='+ qRec.Name +
                    ',[Data3]00ON0000000XHD1?pv0=VFHC-1'+
                    '&TemplateId=a10N000000090kP'+ 
                    '&OFN=VFH+Schedule+-+'+ qRec.Name +'&DS7=13';
        congaURL = 'https://www.appextremes.com/apps/Conga/PM.aspx'+
					'?SessionId='+ sessionId +
					'&ServerUrl='+ ServerURL +
					'&Id='+ qualId +
					'&ReportID=[Data1]00O90000002xERN?pv0='+ qRec.Name +
					',[Data2]00O90000002xERO?pv0='+ qRec.Name +
					',[Data3]00O90000002xERP?pv0=VFHC-1'+
					'&TemplateId=a0H90000006EJWs'+
					'&OFN=VFH+Schedule+-+'+ qRec.Name +'&DS7=13';            
                    */
        congaURL = 'https://www.appextremes.com/apps/Conga/PM.aspx'+
					'?SessionId='+ sessionId +
					'&ServerUrl='+ ServerURL +
					'&Id='+ qualId +
					'&ReportID=[Data1]'+ reportData1 +'?pv0='+ qRec.Name +
					',[Data2]'+ reportData2 +'?pv0='+ qRec.Name +
					',[Data3]'+ reportData3 +'?pv0=VFHC-1'+
					'&TemplateId='+ attachmentId +
					'&OFN=VFH+Schedule+-+'+ qRec.Name +'&DS7=13';
					        
        system.debug('***SessionId'+sessionId);
        system.debug('***ServerURL'+ServerURL);                                                                          
        try{
            update vfhRec;
            update qRec;
            return null;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: ' + e.getMessage()));return null;
        }
    }
}