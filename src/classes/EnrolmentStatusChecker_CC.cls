public with sharing class EnrolmentStatusChecker_CC {
	private ApexPages.StandardController controller {get; set;}
	private Enrolment__c enrolment;
	public Enrolment__c enrolmentRec {get;set;}
	public Boolean redirect {get;set;}
	public EnrolmentStatusChecker_CC(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        this.enrolment = (Enrolment__c)controller.getRecord();
        check();
    }
    public void check(){
    	redirect = false;
    	system.debug('***enrolment.Id: '+ enrolment.Id);
    	
    	List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
    	enrolmentRec = [select Id, Enrolment_Status__c from Enrolment__c where Id=: enrolment.Id];
    	euList = [select Id from Enrolment_Unit__c where Enrolment__c =: enrolment.Id AND Unit_Result__c = null AND Enrolment__r.Delivery_Location__r.Name NOT IN ('Darwin', 'Canberra')];
    	if(euList.size() > 0 && (enrolmentRec.Enrolment_Status__c == 'Cancelled' || enrolmentRec.Enrolment_Status__c == 'Withdrawn')){
    		redirect = true;
    	}
    }
}