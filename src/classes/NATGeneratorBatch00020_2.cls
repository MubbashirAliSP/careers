global class NATGeneratorBatch00020_2 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global String body20;
    private String currentState;
    private String logId;
    private String locIds;

    global NATGeneratorBatch00020_2(String state, Set<Id> locSet, String LId) {
        
        logId = LId;
        
        currentState = state;
        
        locIds = '';
        
        for (String s : locSet) {
            locIds += '\'' + s + '\',';
        }
        
        locIds = locIds.lastIndexOf(',') > 0 ? '(' + locIds.substring(0,locIds.lastIndexOf(',')) + ')' : locIds;
        
        query = 'SELECT Id, Delivery_Location_Identifier__c, ' +
            'Training_Organisation__r.National_Provider_Number__c, ' +
            'Training_Organisation__r.Training_Org_Identifier__c, ' +
            'Name, Postcode__c, Address_Location__c, Child_Postcode__c, ' +
            'Child_Suburb__c, Country_Identifier__c, State_Identifier__c, ' +
            'Child_Country__c, Country__r.Country_Code__c, ' +
            'Child_Address_building_property_name__c, Child_Address_flat_unit_details__c, ' +
            'Child_Address_street_number__c, Child_Address_street_name__c ' +
            'FROM Locations__c WHERE Id in ' + locIds;
       
    }

    global database.querylocator start(Database.BatchableContext BC)
    {
        body20 = '';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {

        if (currentState == 'Queensland') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinition.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if (currentState == 'New South Wales') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionNSW.nat00020(), currentState);
                body20 += '\r\n';
            }
        }	
        
        else if (currentState == 'New South Wales APL') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionNSW.nat00020(), currentState);
                body20 += '\r\n';
            }
        }	
        
        else if (currentState == 'Australian Capital Territory') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionACT.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if(currentState == 'Victoria') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionVIC.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if (currentState == 'South Australia') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionSA.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if (currentState == 'Western Australia') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionWA.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if(currentState == 'Northern Territory') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionNT.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if(currentState == 'Tasmania') {
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinitionTAS.nat00020(), currentState);
                body20 += '\r\n';
            }
        }
        
        else if(currentState == 'National') {
                        
            for (SObject s :scope) {
                Locations__c l = (Locations__c) s;
                body20 += NATConstructor.natFile(l, NATDataDefinition.nat00020(), currentState);
                body20 += '\r\n'; 
            }
        }	
    }

    global void finish(Database.BatchableContext BC) {
        
        NAT_Validation_Log__c natLog = [ SELECT Id, Delete_Existing_NAT_files__c, Content_Library_ID__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
        
        if(body20 == '') {
            body20 = ' ';
        }

        String fbody20 = NATGeneratorUtility.RemoveDuplicates(body20);
        
        Blob pBlob20 = Blob.valueof(fbody20);
        insert new ContentVersion(
            versionData =  pBlob20,
            Title = 'NAT00020',
            PathOnClient = '/NAT00020.txt',
            FirstPublishLocationId = natLog.Content_Library_ID__c
        );
    }
}