/**
 * @description Test class for AssessmentAttemptTriggerHandler
 * @author Biao Zhang
 * @date 12.Nov.2015
 */
@isTest
public with sharing class AssessmentAttemptTriggerHandler_Test {
    static testMethod void testPopulateCensusFields() {
        Census__c census1 = SASTestUtilities.createCensus();
        insert census1;

        Census__c census2 = SASTestUtilities.createCensus();
        insert census2;

        Intake__c intake1 = [Select Start_Date__c, End_Date__c from Intake__c where id = :census1.IntakeId__c limit 1];
        intake1.Start_Date__c = Date.today();
        intake1.End_Date__c = Date.today().addDays(30);
        update intake1;

        Intake__c intake2 = [Select Start_Date__c, End_Date__c from Intake__c where id = :census2.IntakeId__c limit 1];
        intake2.Start_Date__c = Date.today().addDays(31);
        intake2.End_Date__c = Date.today().addDays(60);
        update intake2;

        Enrolment__c e1 = [Select Student__c from Enrolment__c where id =: census1.EnrolmentId__c limit 1];
        e1.Assigned_Trainer__c = UserInfo.getUserId();
        update e1;
        
        Enrolment__c e2 = [Select Student__c from Enrolment__c where id =: census2.EnrolmentId__c limit 1];
        e2.Assigned_Trainer__c = UserInfo.getUserId();
        update e2;

        Test.startTest();

        AssessmentAttempt__c aa1 = new AssessmentAttempt__c();
        aa1.AccountId__c = e1.Student__c;
        aa1.BBAssessmentRecordId__c = '123';
        aa1.SubmittedDate__c = Date.today().addDays(5);
        insert aa1;

        aa1 = [select Census__c from AssessmentAttempt__c where id =: aa1.id limit 1];
        //test update census field in attempt
        system.assertEquals(aa1.Census__c, census1.Id);

        Census__c c1 = [select Id, AssessmentAttemptsCount__c from Census__c where id =: census1.Id limit 1];
        //system.assertEquals(c1.AssessmentAttemptsCount__c, 1);
        
        List<AssessmentAttempt__c> aas = new List<AssessmentAttempt__c>();
        for(Integer dayOffset = 35; dayOffset<40; dayOffset++) {
            AssessmentAttempt__c aa = new AssessmentAttempt__c();
            aa.AccountId__c = e2.Student__c;
            aa.BBAssessmentRecordId__c = String.valueof(dayOffset);
            aa.SubmittedDate__c = Date.today().addDays(dayOffset);
            aas.add(aa);
        }
        insert aas;

        //test increase attempt counter in Census
        Census__c c2 = [select Id, AssessmentAttemptsCount__c from Census__c where id =: census2.Id limit 1];
        //system.assertEquals(c2.AssessmentAttemptsCount__c, 5);

        delete aas;

        //test decrease attempt counter in Census
        c2 = [select Id, AssessmentAttemptsCount__c from Census__c where id =: census2.Id limit 1];
        //system.assertEquals(c2.AssessmentAttemptsCount__c, 0);

        Test.stopTest();
    }
    
}