@isTest
public class USI_LogManager_Test {
    
    public static Account student = NATTestDataFactory.createStudent();
    public static USI_Integration_Log__c log = USI_LogManager.createUSILog();

    static testMethod void createUSILogTest() {
        
        USI_Integration_Log__c testLog = USI_LogManager.createUSILog();
        
        List<USI_Integration_Log__c> checkUsiLog = [ SELECT ID FROM USI_Integration_Log__c WHERE ID = :testLog.Id ];
        
        system.assert(checkUsiLog.size() == 1);
        
        
    }
    
    static testMethod void createUSIEventTestVerify() { 
            
        USI_LogManager.createUSIEvent(log.Id, student.Id, student.FirstName, '1234' , 'Success', false);
        
        List<USI_Integration_Event__c> usiEvents = [ SELECT Id, Service__c, Event_Status__c, USI_Integration_Log__c, Record_Id__c, Message__c FROM USI_Integration_Event__c WHERE USI_Integration_Log__c = :log.Id ];
        
        for(USI_Integration_Event__c ue : usiEvents) {
            
            system.assert(ue.Service__c == 'Verify');
            system.assert(ue.Event_Status__c == 'Success');
            system.assert(ue.USI_Integration_Log__c == log.Id);
            system.assert(ue.Record_Id__c == student.Id);
            system.assert(ue.Message__c == 'Success');
        
        }
    
    }
    
    static testMethod void createUSIEventTestCreate() { 
        
        USI_LogManager.createUSIEvent(log.Id, student.Id, student.FirstName, '1234' , 'Success', true);

        
        List<USI_Integration_Event__c> usiEvents = [ SELECT Id, Service__c, Event_Status__c, USI_Integration_Log__c, Record_Id__c, Message__c FROM USI_Integration_Event__c WHERE USI_Integration_Log__c = :log.Id ];
        
        for(USI_Integration_Event__c ue : usiEvents) {
            
            system.assert(ue.Service__c == 'Create');
            system.assert(ue.Event_Status__c == 'Success');
            system.assert(ue.USI_Integration_Log__c == log.Id);
            system.assert(ue.Record_Id__c == student.Id);
            system.assert(ue.Message__c == 'Success');

        }
    
    }
        
    static testMethod void createUSIErrorLogVerify() { 
        
        USI_LogManager.errorHandle('Error', student.Id, student.FirstName, '1234', log.Id, false);
        
        List<USI_Integration_Event__c> usiEvents = [ SELECT Id, Service__c, Event_Status__c, USI_Integration_Log__c, Record_Id__c, Message__c FROM USI_Integration_Event__c WHERE USI_Integration_Log__c = :log.Id ];
        
        for(USI_Integration_Event__c ue : usiEvents) {
            
            system.assert(ue.Service__c == 'Verify');
            system.assert(ue.Event_Status__c == 'Failure');
            system.assert(ue.USI_Integration_Log__c == log.Id);
            system.assert(ue.Record_Id__c == student.Id);
            system.assert(ue.Message__c == 'Error');

        }
        
        List<Service_Cases__c> checkSvcCase = [SELECT Id, Account_Student_Name__c, Description__c, USI_Issue_Type__c, Requires_USI_Assistance__c, Subject__c FROM Service_Cases__c ];
        
        for(Service_Cases__c sc : checkSvcCase) {
            
            system.assert(sc.Account_Student_Name__c == student.Id);
        	system.assert(sc.Description__c == 'Error');
        	system.assert(sc.USI_Issue_Type__c == 'Verify'); 
        	system.assert(sc.Requires_USI_Assistance__c);
        	system.assert(sc.Subject__c == 'USI Generation/Verification Issue');
            
        }
        
        
    }
    
    static testMethod void createUSIErrorLogCreate() { 
        
        USI_LogManager.errorHandle('Error', student.Id, student.FirstName, '1234', log.Id, true);
        
        List<USI_Integration_Event__c> usiEvents = [ SELECT Id, Service__c, Event_Status__c, USI_Integration_Log__c, Record_Id__c, Message__c FROM USI_Integration_Event__c WHERE USI_Integration_Log__c = :log.Id ];
        
        for(USI_Integration_Event__c ue : usiEvents) {
            
            system.assert(ue.Service__c == 'Create');
            system.assert(ue.Event_Status__c == 'Failure');
            system.assert(ue.USI_Integration_Log__c == log.Id);
            system.assert(ue.Record_Id__c == student.Id);
            system.assert(ue.Message__c == 'Error');

        }
        
        List<Service_Cases__c> checkSvcCase = [SELECT Id, Account_Student_Name__c, Description__c, USI_Issue_Type__c, Requires_USI_Assistance__c, Subject__c FROM Service_Cases__c ];
        
        for(Service_Cases__c sc : checkSvcCase) {
            
            system.assert(sc.Account_Student_Name__c == student.Id);
        	system.assert(sc.Description__c == 'Error');
        	system.assert(sc.USI_Issue_Type__c == 'Create'); 
        	system.assert(sc.Requires_USI_Assistance__c);
        	system.assert(sc.Subject__c == 'USI Generation/Verification Issue');
            
        }
        
        
    }
    
    static testMethod void createUSIErrorLogNullMessage() { 
        
        //USI_LogManager.errorHandle(null, student.Id, log.Id, true);
        USI_LogManager.errorHandle(null, student.Id, student.FirstName, '1234', log.Id, true);

        
        List<USI_Integration_Event__c> usiEvents = [ SELECT Id, Service__c, Event_Status__c, USI_Integration_Log__c, Record_Id__c, Message__c FROM USI_Integration_Event__c WHERE USI_Integration_Log__c = :log.Id ];
        
        for(USI_Integration_Event__c ue : usiEvents) {
            
            system.assert(ue.Service__c == 'Create');
            system.assert(ue.Event_Status__c == 'Failure');
            system.assert(ue.USI_Integration_Log__c == log.Id);
            system.assert(ue.Record_Id__c == student.Id);

        }
        
        List<Service_Cases__c> checkSvcCase = [SELECT Id, Account_Student_Name__c, Description__c, USI_Issue_Type__c, Requires_USI_Assistance__c, Subject__c FROM Service_Cases__c ];
        
        for(Service_Cases__c sc : checkSvcCase) {
            
            system.assert(sc.Account_Student_Name__c == student.Id);
        	system.assert(sc.USI_Issue_Type__c == 'Create'); 
        	system.assert(sc.Requires_USI_Assistance__c);
        	system.assert(sc.Subject__c == 'USI Generation/Verification Issue');
            
        }
        
        
    }
    
    

    
    
    
    

}