/**
 * @description Test class for `AccountOpenInSECCX`
 * @author Ranyel Maliwanag
 * @date 04.MAR.2016
 */
@isTest
public with sharing class AccountOpenInSECCX_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		insert enrolment;
	}

	static testMethod void controllerTest() {
		Account student = [SELECT Id FROM Account];

		Test.setCurrentPage(Page.AccountOpenInSEC);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(student);
			AccountOpenInSECCX controller = new AccountOpenInSECCX(stdController);

			PageReference result = controller.evaluateRedirects();
			System.assertNotEquals(null, result);
		Test.stopTest();
	}
}