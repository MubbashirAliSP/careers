/**
 * @description Trigger handler for the Service Case object
 * @author Ranyel Maliwanag
 * @date 29.MAY.2015
 * @history
 *       24.JUN.2015    Ranyel Maliwanag    - Changed `updateLastQueueName()` method signature to include oldMap
 *                                          - Added updating of status to new if the owner has been changed to a queue
 *       18.MAR.2016    Ranyel Maliwanag    - Added `onAfterUpdate()` handler method
 *                                          - Added logic to update `IsVFHLoanFormSubmitted__c` field on Enrolment when the loan form comes gets submitted
 *                                          - Removed logic around GLS markings
 *       24.MAR.2016    Ranyel Maliwanag    - Added logic to create Ownership History records
 *       29.MAR.2016    Ranyel Maliwanag    - Added logic to update `CurrentOwnershipStartDate__c` field everytime the Service Case owner gets updated. Done in code instead of workflow because limit of active workflows on Service Case has been reached.
 *		 14.JUN.2016	Ranyel Maliwanag	- Added logic to populate the `PostcodeAllocationId__c`, `Primary_LLN_Support_Strategies__c`, and `AllocationCampus__c` fields based on the existence of a Postcode Allocation record for the `Residential_Postcode__c` of the Service Case
 */
public without sharing class ServiceCaseTriggerHandler {
	// Reference variable for Online Service Case Record Type Id
	public static Id onlineServiceCaseRecordType =  Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Online Service Case').getRecordTypeId();
	// Reference variable for Form record type Id
	public static Id formRecordType = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();


	/**
	 * @description Handles all before insert operations for the Service Case object
	 * @author Ranyel Maliwanag
	 * @date 29.MAY.2015
	 * @param (List<Service_Cases__c>) serviceCases: List of service case records that will be inserted
	 */
	public static void onBeforeInsert(List<Service_Cases__c> serviceCases) {
		List<Service_Cases__c> filteredServiceCases = new List<Service_Cases__c>();
		for (Service_Cases__c serviceCase : serviceCases) {
			if (serviceCase.RecordTypeId == formRecordType && serviceCase.Type__c == 'VFH Online Enrolment Application' && String.isNotBlank(serviceCase.Residential_Postcode__c) && String.isNotBlank(serviceCase.BrokerName__c)) {

				// Conditions:
				// * Record Type == 'Form'
				// * Type == 'VFH Online Enrolment Application'
				// * Residential Postcode is not blank
				// * Sales Agent is not blank
				// Note:
				// * 14 June 2016: As per PW, should run for all Service Cases and not just Telesales-created.
				filteredServiceCases.add(serviceCase);
			}

			if (serviceCase.RecordTypeId == formRecordType) {
				serviceCase.CurrentOwnershipStartDate__c = Datetime.now();
			}
		}
		updateLastQueueName(serviceCases, null);
		
		// Run the allocatePostcode method if there are filtered Service Cases
		if (!filteredServiceCases.isEmpty()) {
			allocatePostcode(filteredServiceCases);
		}
	}


	/**
	 * @description Handles all before update operations for the Service Case object
	 * @author Ranyel Maliwanag
	 * @date 29.MAY.2015
	 * @param (List<Service_Cases__c>) serviceCases: List of service case records that will be updated
	 * @param (Map<Id, Service_Cases__c>) oldServiceCaseMap: Map of service case records and their previous field values
	 * @history
	 *       29.MAR.2016    Ranyel Maliwanag    - Added logic to update `CurrentOwnershipStartDate__c` field everytime the Service Case owner gets updated.
	 */
	public static void onBeforeUpdate(List<Service_Cases__c> serviceCases, Map<Id, Service_Cases__c> oldServiceCaseMap) {
		CentralTriggerSettings__c settings = CentralTriggerSettings__c.getInstance();
		List<Service_Cases__c> filteredServiceCases = new List<Service_Cases__c>();
		for (Service_Cases__c serviceCase : serviceCases) {
			if (serviceCase.RecordTypeId == formRecordType && serviceCase.Type__c == 'VFH Online Enrolment Application' && (serviceCase.Residential_Postcode__c != oldServiceCaseMap.get(serviceCase.Id).Residential_Postcode__c || serviceCase.BrokerName__c != oldServiceCaseMap.get(serviceCase.Id).BrokerName__c) && String.isNotBlank(serviceCase.Residential_Postcode__c) && String.isNotBlank(serviceCase.BrokerName__c)) {

				// Conditions:
				// * Record Type == 'Form'
				// * Type == 'VFH Online Enrolment Application'
				// * Residential Postcode is not blank
				// * Sales Agent is not blank
				// * Residential Postcode value has changed or Sales Agent value has changed
				// Note:
				// * 14 June 2016: As per PW, should run for all Service Cases and not just Telesales-created.
				filteredServiceCases.add(serviceCase);
			}
			if (serviceCase.RecordTypeId == formRecordType && serviceCase.OwnerId != oldServiceCaseMap.get(serviceCase.Id).OwnerId) {
				serviceCase.CurrentOwnershipStartDate__c = Datetime.now();
			}
		}
		updateLastQueueName(serviceCases, oldServiceCaseMap);

		// Run the allocatePostcode method if there are filtered Service Cases
		if (!filteredServiceCases.isEmpty()) {
			allocatePostcode(filteredServiceCases);
		}
	}


	/**
	 * @description Handles all after insert operations for the Service Case object
	 * @author Ranyel Maliwanag
	 * @date 29.MAY.2015
	 * @param (List<Service_Cases__c>) serviceCases: List of service case records that will be inserted
	 */
	public static void onAfterInsert(List<Service_Cases__c> serviceCases) {
		List<Service_Cases__c> ownerChanged = new List<Service_Cases__c>();
		for (Service_Cases__c record : serviceCases) {
			if (record.RecordTypeId == formRecordType && String.isNotBlank(record.OwnerId)) {
				ownerChanged.add(record);
			}
		}

		if (!ownerChanged.isEmpty()) {
			createOwnershipHistory(ownerChanged);
		}
	}


	/**
	 * @description Handles all after update operations for the Service Case object
	 * @author Ranyel Maliwanag
	 * @date 29.MAY.2015
	 * @param (List<Service_Cases__c>) serviceCases: List of service case records that will be updated
	 * @param (Map<Id, Service_Cases__c>) oldServiceCaseMap: Map of service case records and their previous field values
	 */
	public static void onAfterUpdate(List<Service_Cases__c> serviceCases, Map<Id, Service_Cases__c> oldServiceCaseMap) {
		// @TODO: Logic for updating the VFH Loan Form Submitted checkbox on the enrolment
		List<Service_Cases__c> filteredRecords = new List<Service_Cases__c>();
		List<Service_Cases__c> ownerChanged = new List<Service_Cases__c>();

		// Filter through the service cases to get a list of those that have had their Related Form Status changed
		for (Service_Cases__c record : serviceCases) {
			// Conditions:
			// * Record Type = 'Form'
			// * Related Form Status has changed or Enrolment lookup has changed or All Entry Requirements Met has changed
			if (record.RecordTypeId == formRecordType && (record.Related_Form_Status__c != oldServiceCaseMap.get(record.Id).Related_Form_Status__c || record.Enrolment__c != oldServiceCaseMap.get(record.Id).Enrolment__c || record.AreAllEntryRequirementsMet__c != oldServiceCaseMap.get(record.Id).AreAllEntryRequirementsMet__c)) {
				filteredRecords.add(record);
			}

			if (record.RecordTypeId == formRecordType && record.OwnerId != oldServiceCaseMap.get(record.Id).OwnerId) {
				ownerChanged.add(record);
			}
		}

		updateVFHLoanFormSubmittedStatus(filteredRecords);
		if (!ownerChanged.isEmpty()) {
			createOwnershipHistory(ownerChanged);
		}
	}


	/**
	 * @description Updates the `IsVFHLoanFormSubmitted__c` field on qualified enrolment records
	 * @author Ranyel Maliwanag
	 * @date 16.FEB.2016
	 */
	private static void updateVFHLoanFormSubmittedStatus(List<Service_Cases__c> records) {
		List<Enrolment__c> enrolmentsForUpdate = new List<Enrolment__c>();

		for (Service_Cases__c record : records) {
			if (String.isNotBlank(record.Enrolment__c)) {
				Enrolment__c enrolment = new Enrolment__c(
						Id = record.Enrolment__c,
						IsVFHLoanFormSubmitted__c = record.Related_Form_Status__c == 'Submitted',
						AreAllEntryRequirementsMet__c = record.AreAllEntryRequirementsMet__c
					);
				enrolmentsForUpdate.add(enrolment);
			}
		}

		if (!enrolmentsForUpdate.isEmpty()) {
			update enrolmentsForUpdate;
		}
	}


	/**
	 * @description Updates the Last Queue Name field for the given service case records
	 * @author Ranyel Maliwanag
	 * @date 29.MAY.2015
	 * @param (List<Service_Cases__c>) serviceCases: List of service case records whose Last Queue Name field needs to be updated
	 * @history
	 *       24.JUN.2015    Ranyel Maliwanag    - Updated method signature to include oldMap
	 *                                          - Added updating of status to new if the owner has been changed to a queue
	 */
	private static void updateLastQueueName(List<Service_Cases__c> serviceCases, Map<Id, Service_Cases__c> oldServiceCaseMap) {
		List<Id> queueIds = new List<Id>();
		// Mapping of <Queue Id> : <Queue Name>
		Map<Id, String> queueMap = new Map<Id, String>();

		Set<Id> affectedRecordTypes = new Set<Id>{onlineServiceCaseRecordType, formRecordType};

		// Build a list of existing queue ids based on service case records being worked on
		for (Service_Cases__c serviceCase : serviceCases) {
			if (affectedRecordTypes.contains(serviceCase.RecordTypeId)) {
				queueIds.add(serviceCase.OwnerId);
			}
		}

		// Build mapping of <Queue Id> : <Queue Name> based on queue Ids collected
		if (!queueIds.isEmpty()) {
			List<Group> queues = [SELECT Id, Name FROM Group WHERE Id IN :queueIds];
			if (!queues.isEmpty()) {
				for (Group queue : queues) {
					queueMap.put(queue.Id, queue.Name);
				}
			}
		}

		for (Service_Cases__c serviceCase : serviceCases) {
			if (affectedRecordTypes.contains(serviceCase.RecordTypeId)) {
				// Condition: Service Case record type is Online Service Case
				if (queueMap.containsKey(serviceCase.OwnerId)) {
					Boolean isQueueChanged = false;
					// Condition: The owner of the service case is a queue, so we update the Last Queue Name field
					isQueueChanged = serviceCase.Last_Queue_Name__c != queueMap.get(serviceCase.OwnerId) && serviceCase.Status__c != 'Closed';
					serviceCase.Last_Queue_Name__c = queueMap.get(serviceCase.OwnerId);

					if (oldServiceCaseMap != null && oldServiceCaseMap.containsKey(serviceCase.Id) && oldServiceCaseMap.get(serviceCase.Id).OwnerId != serviceCase.OwnerId && isQueueChanged) {
						// Condition: Service case is not new and OwnerId has been changed to a queue (from prior if condition)
						serviceCase.Status__c = 'New';
					}
				}
			}
		} 
	}


	/**
	 * @description Creates a Servcie Case Ownerhip History record for each service case and populates the end date and deactivates the current active Service Case Ownerhip History record (if any)
	 * @author Ranyel Maliwanag
	 * @date 24.MAR.2016
	 */
	private static void createOwnershipHistory(List<Service_Cases__c> records) {
		List<ServiceCaseOwnershipHistory__c> historiesForUpsert = new List<ServiceCaseOwnershipHistory__c>();
		
		// Collect Service Case Ids for querying
		Set<Id> serviceCaseIds = new Set<Id>();
		for (Service_Cases__c record : records) {
			serviceCaseIds.add(record.Id);
		}

		// Query Service Case records together with Ownership Histories
		for (Service_Cases__c record : [SELECT Id, OwnerId, Owner.Name, (SELECT Id FROM OwnershipHistories__r WHERE IsActive__c = true) FROM Service_Cases__c WHERE Id IN :serviceCaseIds]) {

			if (String.isNotBlank(record.Owner.Name)) {
				// Create a new Ownership History
				ServiceCaseOwnershipHistory__c historyRecord = new ServiceCaseOwnershipHistory__c(
						ServiceCaseId__c = record.Id,
						IsActive__c = true,
						OwnerName__c = record.Owner.Name,
						StartDate__c = Datetime.now()
					);
				historiesForUpsert.add(historyRecord);
			}

			// Update existing active history records
			for (ServiceCaseOwnershipHistory__c activeHistory : record.OwnershipHistories__r) {
				activeHistory.IsActive__c = false;
				activeHistory.EndDate__c = Datetime.now();
				historiesForUpsert.add(activeHistory);
			}
		}
		upsert historiesForUpsert;
	}


	/**
	 * @description Finds a Postcode Allocation record for the given Service Cases
	 * @author Ranyel Maliwanag
	 * @date 14.JUN.2016
	 */
	private static void allocatePostcode(List<Service_Cases__c> records) {
		// Collect a list of sales agents for querying
		Set<String> agentCodes = new Set<String>();
		for (Service_Cases__c record : records) {
			if (String.isNotBlank(record.BrokerName__c) && String.isNotBlank(record.Residential_Postcode__c)) {
				agentCodes.add(record.BrokerName__c);
			}
		}
		String agentCodesString = '(\'';
		agentCodesString += String.join(new List<String>(agentCodes), '\';\'');
		agentCodesString += '\')';

		// Query allocation streams based on sales agent codes
		String queryString = 'SELECT Id, SalesAgentIdentifiers__c, DefaultStrategy__c, (SELECT Id, Postcode__c, PrimaryLLNSupportStrategy__c, AllocationCampus__c FROM PostcodeAllocations__r) FROM AllocationStream__c WHERE SalesAgentIdentifiers__c INCLUDES ' + agentCodesString;
		List<AllocationStream__c> streams = Database.query(queryString);

		// Build a mapping of <Agent Code> : <Allocation Stream>
		Map<String, AllocationStream__c> streamMap = new Map<String, AllocationStream__c>();
		// Build a mapping of <Allocation Stream> : <Postcode allocation map>
		Map<Id, Map<String, PostcodeAllocation__c>> streamAllocationMap = new Map<Id, Map<String, PostcodeAllocation__c>>();

		for (AllocationStream__c stream : streams) {
			if (String.isNotBlank(stream.SalesAgentIdentifiers__c)) {
				for (String agentCode : stream.SalesAgentIdentifiers__c.split(';')) {
					streamMap.put(agentCode, stream);
				}
			}

			// Build a mapping of <Postcode> : <Postcode Allocation record>
			Map<String, PostcodeAllocation__c> allocationMap = new Map<String, PostcodeAllocation__c>();
			for (PostcodeAllocation__c allocation : stream.PostcodeAllocations__r) {
				allocationMap.put(allocation.Postcode__c, allocation);
			}

			streamAllocationMap.put(stream.Id, allocationMap);
		}

		// Iterate over the service cases
		for (Service_Cases__c record : records) {

			// Check if Service Case has a corresponding allocation stream
			if (streamMap.containsKey(record.BrokerName__c)) {
				AllocationStream__c stream = streamMap.get(record.BrokerName__c);

				// Check if Service Case has a Postcode Allocation record based on `Residential_Postcode__c` on the related stream
				if (streamAllocationMap.containsKey(stream.Id) && streamAllocationMap.get(stream.Id).containsKey(record.Residential_Postcode__c)) {

					// Populate the `PostcodeAllocationId__c`, `Primary_LLN_Support_Strategies__c`, and `AllocationCampus__c` fields
					PostcodeAllocation__c allocation = streamAllocationMap.get(stream.Id).get(record.Residential_Postcode__c);
					record.PostcodeAllocationId__c = allocation.Id;
					record.Primary_LLN_Support_Strategies__c = allocation.PrimaryLLNSupportStrategy__c;
					record.AllocationCampus__c = allocation.AllocationCampus__c;
				} else {

					// @TODO populate `Primary_LLN_Support_Strategies__c` based on Allocation Stream information
					record.Primary_LLN_Support_Strategies__c = stream.DefaultStrategy__c;
				}
			}
		}
	}
}