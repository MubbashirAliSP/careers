/**
 * @description Controller for Schedule Students page 2
 * @author Janella Lauren Canlas
 * @date 12.DEC.2012
 * @history
 *       12.DEC.2012    Janella Canlas      - Created.
 *       12.FEB.2013    Janella Canlas      - Created method to populate related EUOS Tuition Fee field
 *       21.AUG.2013    Michelle Magsarili  - Modified method populateCensusDate to display a sensible error message if there are no Tuition Fee Records Retrieved
 *                                          - Modified method save to display errormessage obtained in populateCensusDate
 *       09.SEP.2013    Michelle Magsarili  - modified populateCensusDate method to check for the record type if not Standard Enrolment then proceed with populateCensusDate
 *                                          - Obtain Tuition Fees only if these records exist.
 *       10.SEP.2013    Michelle Magsarili  - Raise Error if Enrolment Intake Units (Scheduled Units) records were not created during saving
 *       22.OCT.2013    Michelle Magsarili  - Added additional condition to proceed processing if there are Alternate Delivery Option Classes
 *       13.FEB.2015    Warjie Malibago     - Populate Loan Fee with 0 after scheduling, see WBM
 *       31.MAR.2015    Kim Saclag          - Added filter for 501 Enrolment Status Code.
 *       29.APR.2015    Warjie Malibago     - Recode; added rollbackChanges() if encoutered issues; removed populateTuitionFeeOnEUOS() since its not used anymore
 *       06.MAY.2015    Arnie Ug            - Remove 'Interim Result-Awaiting Employer/Placement Verification' from Enrolment Unit Query 
 *       07.MAY.2015    Ranyel Maliwanag    - Implemented standard rollback functionality. Improved bulkification of codes
 *       08.MAY.2015    Ranyel Maliwanag    - Updated tuition fee determining logic to couple with indvidual EUOS data instead of Enrolment. Code cleanup.
 *                                          - Documentation updates
 *       11.MAY.2015    Ranyel Maliwanag    - Added updating of Start and End Dates of enrollment units based on Intake dates when successfully creating enrolment intake units
 *       04.JUN.2015    Ranyel Maliwanag    - Update enrolment unit start and end dates based on start and end dates of scheduled units
 *       28.AUG.2015    Nick Guia           - Minor optimization to fix too many SOQL query error
 *       06.OCT.2015    Rhyan Cruz          - Modified SOQL condition of populateCensusDate to exclude EOUS with Recredit/Revision value
 *       30.OCT.2015    Ranyel Maliwanag    - Implementation of the new model for the Census Regulatory requirements
 *       02.NOV.2015    Ranyel Maliwanag    - Added fallback to using the rate per hour model for calculating tuition fees when no census periods information has been defined
 *		 23.NOV.2015	Ranyel Maliwanag	- Modified the conditions to call the `populateCensusDate` method in the `save` method to only run specifically for `VET Fee Help Enrolment` and `Dual Funding Enrolment` record types instead of not `Standard Enrolments` with the introduction of additional Enrolment record types that are neither Standard- nor VFH-related
 * 		 02.DEC.2015	Ranyel Maliwanag	- Implemented support for multiple subsidised rates per state model using Student Fee Types
 *		 20.JAN.2016	Ranyel Maliwanag	- Updated tuition fee computation to divide by the total number of Intake Units with matching Enrolment Units of Study instead of by the number of Intake Units that are only being scheduled
 *		 24.FEB.2016	Ranyel Maliwanag	- Added exception to Victorian enrolments so that EU Start and End dates won't get overwritten during scheduling
 *		 29.APR.2016	Ranyel Maliwanag	- Updated tuition fee computation to divide by the total number of Intake Units with matching Enrolment Units of Study that don't have a Credit Transfer result against them. Reference Case: 00078939
 */
public with sharing class ScheduleStudentsPage2_CC {
	public Id intakeId {get; set;}
	public Id enrolmentId {get; set;}
	
	public String studId = '';
	public String unitCode {get; set;}
	public Boolean unitCheck {get;set;}
	public Boolean allCheck {get;set;}
	public Boolean recCheck {get;set;}      
	public List<Page2Wrapper> enrolWrapperList {get; set;}
	private Set<Id> tempEUIds = new Set<Id>();
	private Boolean isCenrolInserted;
	private Boolean isEiuInserted;
	private Enrolment__c activeEnrolment;
	private List<Enrolment_Unit__c> enrolmentUnitList;
	private List<Classes__c> classesList;
	private ApplicationsPortalCentralSettings__c customSettings;
	private Boolean isUsingCensusPeriods;

	public List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
	public List<Enrolment_Intake_Unit__c> eiuList2 = new List<Enrolment_Intake_Unit__c>();
	public List<Class_Enrolment__c> cenrolList = new List<Class_Enrolment__c>();
	public List<Intake_Unit__c> iuList = new List<Intake_Unit__c>();
 
	public ScheduleStudentsPage2_CC() {
		intakeId = ApexPages.currentPage().getParameters().get('intakeId');
		enrolmentId = ApexPages.currentPage().getParameters().get('enrolmentId');
		enrolWrapperList = new List<Page2Wrapper>();
		enrolmentUnitList = getEnrolmentUnits();
		activeEnrolment = getEnrolment();
		classesList = getClassList();
		fillPageTable();
		isCenrolInserted = false;
		isEiuInserted = false;
		customSettings = ApplicationsPortalCentralSettings__c.getInstance();
		isUsingCensusPeriods = customSettings.CensusPeriods__c != null && customSettings.EffectivityDate__c <= activeEnrolment.Start_Date__c;
	}
	
	//intake unit picklist above the pageblocktable
	public List<SelectOption> getIntakeUnitPicklist() {
		List<Classes__c> tempClass = new List<Classes__c>();
		List<SelectOption> options = new List<SelectOption>();
		Set<String> iuIds = new Set<String>();
	
		//collect intake unit ids
		for(Classes__c c : classesList){
		  iuIds.add(c.Intake_Unit__c);
		}
		
		//populate picklist
		options.add(new SelectOption('', ''));   
		for (Intake_Unit__c r : [SELECT Id, Name,Unit_of_Study__r.Name, Unit_Name__c from Intake_Unit__c where Id IN: iuIds]) {
			options.add(new SelectOption(r.Id, r.Unit_of_Study__r.Name + ' - '+ r.Unit_Name__c + ' - ' + r.Name));
		}  
		
		return options;
	}    
	
	public Enrolment__c getEnrolment(){
		Enrolment__c tempAcc = new Enrolment__c();

		//MAM 09/09/2013 added RecordType.Name
		tempAcc = [SELECT Id, Name, Start_Date__c, Student_Identifier__c, Delivery_Location_State__c, Student__r.Name, Qualification__c, RecordType.Name, Student_Status_Code__c, 
						Student_Status_Code__r.Use_Subsidised_Amount__c, Student_Status_Code__r.Student_Status_Code_State__c
					FROM Enrolment__c
					WHERE Id =: enrolmentId LIMIT 1];
		return tempAcc;
	}
	
	/**
	*   @history
	*       Arnie Ug        MAY.6.2015      Removed 'Interim Result-Awaiting Employer/Placement Verification'
	*       Troy Collins    JUNE.3.2015     deactivated, there is already exiting functionality for this
	*       Nick Guia       AUG.28.2015     Moved to constructor as it is being called many times. Assigned to private var:enrolmentUnitList
	*/
	public List<Enrolment_Unit__c> getEnrolmentUnits(){
		ResultNamesForExclusion__c exclusionSettings = ResultNamesForExclusion__c.getInstance();
		Map<String, ResultNamesForExclusion__c> exclusionMap = ResultNamesForExclusion__c.getAll();
		List<String> exclusionList = new List<String>();
		List<Enrolment_Unit__c> tempAcc = new List<Enrolment_Unit__c>();
		
		for (String mapKey : exclusionMap.keySet()) {
			exclusionList.add(exclusionMap.get(mapKey).ResultName__c);
		}

		tempAcc = [SELECT Id, Unit_Result__r.National_Outcome_Code__c, Unit__r.Name, Unit_Result__r.National_Outcome__r.Exclude_for_Scheduling__c, 
					Unit_Result__r.Name, Unit_Result__r.National_Outcome__r.Name,
					(SELECT Id, Intake_Unit__c, Intake_Name__c FROM Enrolment_Intake_Units__r)
					FROM Enrolment_Unit__c
					WHERE Enrolment__c =: enrolmentId
					AND Unit_Result__r.Name NOT IN :exclusionList]; // Removed 'Interim Result-Awaiting Employer/Placement Verification'
		return tempAcc;
	}
	
	/**
	*   @history
	*       Nick Guia       AUG.28.2015     - modified query as enrolment intake unit and enrolment is nullable (But there should be no scenario where both is null)
	*                                       - removed unused variables
	*/
	public List<Class_Enrolment__c> getEnrolmentList(){
		List<Class_Enrolment__c> tempList = new List<Class_Enrolment__c>();
		
		tempList = [SELECT Name, Unit_Name__c, Unit_Code__c, Class_Start_Date_Time__c, Class_End_Date_Time__c, Class__r.Name, Class__c
				FROM Class_Enrolment__c
				WHERE Student_Identifier__c = :activeEnrolment.Student_Identifier__c
				AND (Enrolment__c =: enrolmentId OR Enrolment_Intake_Unit__r.Enrolment__c =: enrolmentId)];
			//WHERE Enrolment_Intake_Unit__r.Enrolment__r.Student__c =: tempStud.Student__c];

		studId = activeEnrolment.Student__c;
		return tempList;
	}
	
	/**
	*   @history
	*       Nick Guia       AUG.28.2015     - moved to constructor as it is being called many times
	*/
	public List<Classes__c> getClassList(){
		List<Classes__c> tempList = new List<Classes__c>();
		List<Class_Enrolment__c> tempEnrols = new List<Class_Enrolment__c>();
		Set<String> classRest = new Set<String>();
		Set<String> unitCodes = new Set<String>();

		tempEnrols = getEnrolmentList(); //class enrolment

		//NG:unused
		//Enrolment__c tempStud = activeEnrolment;
		
		//collect class ids
		for(Class_Enrolment__c c: tempEnrols){
			classRest.add(c.Class__c);
		}
		
		//collect unit names
		for(Enrolment_Unit__c e: enrolmentUnitList){
			unitCodes.add(e.Unit__r.Name);
		}
		
		tempList = [SELECT Unit_Code__c, Unit_Name__c, End_Date_Time__c, Start_Date_Time__c, Name, Intake_Unit__c, Intake_Unit__r.Unit_of_Study__c, 
						Intake_Unit__r.Id, Class_Delivery__c, Alternate_Class_Delivery_Option__c
					FROM Classes__c
					WHERE (Alternate_Class_Delivery_Option__c != NULL OR Unit_Code__c IN: unitCodes)
					AND Id NOT IN: classRest
					AND Intake__c =: intakeId
					ORDER BY Start_Date_Time__c];
		
		system.debug(templist);

		return tempList;
	}
	

	public void fillPageTable(){
		enrolWrapperList = new List<Page2Wrapper>();
		Boolean check = false;
		Map<String, String> unitOutMap = new Map<String, String>();
		Map<String, Boolean> unitExcludeMap = new Map<String, Boolean>();
		String dy = '';
		String edy = '';
		String outcomeStr = '';
		
		for (Enrolment_Unit__c eu: enrolmentUnitList) {
			// Create map of unit code and national outcome codes
			if (eu.Unit_Result__c != NULL) {
				unitOutMap.put(eu.Unit__r.Name, eu.Unit_Result__r.National_Outcome_Code__c + ' - ' + eu.Unit_Result__r.National_Outcome__r.Name);
			} else if (activeEnrolment.Delivery_Location_State__c == 'Victoria') {
				// Exemptions for Victorian enrolments where Unit Result is expected to null
				unitOutMap.put(eu.Unit__r.Name, '');
			}
			// Create map of unit code and exclude for scheduling
			unitExcludeMap.put(eu.Unit__r.Name, eu.Unit_Result__r.National_Outcome__r.Exclude_for_Scheduling__c);
		}
		
		//populate wrapper list
		if(unitCheck == TRUE){
			for(Classes__c c: classesList){
				check = FALSE;
				if(c.Intake_Unit__c == unitCode){
					check = TRUE;
				}
				if(unitOutMap.get(c.Unit_Code__c) != NULL){
					outcomeStr = unitOutMap.get(c.Unit_Code__c);
					system.debug('**Outcome: ' + outcomeStr + ', Class: ' + c.Name);
					if(outcomeStr.toLowerCase().contains('competent') || outcomeStr.toLowerCase().contains('competency achieved') || 
						outcomeStr.toLowerCase().contains('rpl') || outcomeStr.toLowerCase().contains('credit transfer') || 
						outcomeStr.toLowerCase().contains('competency achieved/pass'))
					{
						outcomeStr = 'false';
					}
					//else{
						//outcomeStr = '';
					//}
					
					dy = c.Start_Date_Time__c.FORMAT('EEEE');
					edy = c.End_Date_Time__c.FORMAT('EEEE');
					if(outcomeStr != 'false'){
						if(unitExcludeMap.get(c.Unit_Code__c) != NULL && unitExcludeMap.get(c.Unit_Code__c) == FALSE){
							Page2Wrapper wc = new Page2Wrapper(c, check, outcomeStr, dy, edy);
							enrolWrapperList.add(wc);
						}
						else if(c.Alternate_Class_Delivery_Option__c != NULL){
							Page2Wrapper wc = new Page2Wrapper(c, FALSE, outcomeStr, dy, edy);
							enrolWrapperList.add(wc);
						}
					}
				}
				//JMDG 28 AUG 2015 ELSE IF FOR ALTERNATE DELIVERY OPTION
				else if(c.Alternate_Class_Delivery_Option__c != NULL && outcomeStr != 'false'){
					dy = c.Start_Date_Time__c.FORMAT('EEEE');
					edy = c.End_Date_Time__c.FORMAT('EEEE');
					Page2Wrapper wc = new Page2Wrapper(c, FALSE, outcomeStr, dy, edy);
					enrolWrapperList.add(wc);
				}
			}
		}
		else{
			for (Classes__c c : classesList) {
				if(unitOutMap.get(c.Unit_Code__c) != NULL){
					outcomeStr = unitOutMap.get(c.Unit_Code__c);
					system.debug('**Outcome: ' + outcomeStr + ', Class: ' + c.Name);
					if(outcomeStr.toLowerCase().contains('competent') || outcomeStr.toLowerCase().contains('competency achieved') || 
						outcomeStr.toLowerCase().contains('rpl') || outcomeStr.toLowerCase().contains('credit transfer') || 
						outcomeStr.toLowerCase().contains('competency achieved/pass'))
					{
						outcomeStr = 'false';
					}
					//else{
						//outcomeStr = '';
					//}
					
					dy = c.Start_Date_Time__c.FORMAT('EEEE');
					edy = c.End_Date_Time__c.FORMAT('EEEE');
					if(outcomeStr != 'false'){
						if(unitExcludeMap.get(c.Unit_Code__c) != NULL && unitExcludeMap.get(c.Unit_Code__c) == FALSE){
							Page2Wrapper wc = new Page2Wrapper(c, check, outcomeStr, dy, edy);
							enrolWrapperList.add(wc);
						}
						else if(c.Alternate_Class_Delivery_Option__c != NULL){
							Page2Wrapper wc = new Page2Wrapper(c, FALSE, outcomeStr, dy, edy);
							enrolWrapperList.add(wc);
						}
					}
				}
				//JMDG 28 AUG 2015 ELSE IF FOR ALTERNATE DELIVERY OPTION
				else if(c.Alternate_Class_Delivery_Option__c != NULL && outcomeStr != 'false'){
					dy = c.Start_Date_Time__c.FORMAT('EEEE');
					edy = c.End_Date_Time__c.FORMAT('EEEE');
					Page2Wrapper wc = new Page2Wrapper(c, FALSE, outcomeStr, dy, edy);
					enrolWrapperList.add(wc);
				}
				outcomeStr=' '; //JMDG 28 AUG 2015
			}
		}
	}
	
	public PageReference back(){
		PageReference pageRef = new PageReference('/apex/ScheduleStudentsPage1');
		pageRef.getParameters().put('intakeId',intakeId);
		pageRef.setRedirect(true);
		return pageRef;     
	}
	
	public PageReference cancel(){
		PageReference p = new PageReference('/'+intakeId);
		p.setredirect(true);
		return p;
	}

	/**
	 * @description: Refreshes the list variables that were inserted during an unsuccessful previous save to work around the insertion with Ids issues when resubmitting the form. This is based on the fact that Ids are retained on the variables even after a successful database rollback.
	 * @author: Ranyel Maliwanag
	 * @date 07.MAY.2015
	 */
	private void refreshVariables() {
		eiuList = new List<Enrolment_Intake_Unit__c>();
		eiuList2 = new List<Enrolment_Intake_Unit__c>();
		cenrolList = new List<Class_Enrolment__c>();
		iuList = new List<Intake_Unit__c>();
	}
	
	/**
	 * @description N/A
	 * @author N/A
	 * @date N/A
	 * @history
	 *       04.JUN.2015    Ranyel Maliwanag    - Update enrolment unit start and end dates based on start and end dates of scheduled units
	 *       30.OCT.2015    Ranyel Maliwanag    - Implementation of the new model for the Census Regulatory requirements
	 *		 23.NOV.2015	Ranyel Maliwanag	- Changed condition to run the `populateCensusDate` method specifically on `VET Fee Help Enrolment` and `Dual Funding Enrolment` record types only
	 *		 20.JAN.2016	Ranyel Maliwanag	- Added intakeSize variable as a reference to the total number of Intake Units in the given intake and passed the value on to `populateCensusDate` method for tuition fee computation
	 *		 29.APR.2016	Ranyel Maliwanag	- Added Unit Result != Credit Transfer to the euosList query filter
	 */ 
	public PageReference save() {
		String errMsg;
		Savepoint sp = Database.setSavepoint();
		// Set of Intake Unit Ids
		Set<String> iuIds = new Set<String>();
		// Unit of Study Names set
		Set<String> setUnitOfStudy = new Set<String>();
		// Mapping of <Unit Code> : <Intake Unit Id>
		Map<String, String> iuMap = new Map<String, String>();
		Set<String> classClassDelOptionIds = new Set<String>();
		List<Intake_Unit__c> iuList = new List<Intake_Unit__c>();
		List<Classes__c> tempClass = new List<Classes__c>();


		Intake__c currentIntake = [SELECT Id, CensusNumber__c, Name, Start_Date__c, End_Date__c, Census_Date__c, (SELECT Id, Unit_of_Study__c FROM Intake_Unit__r) FROM Intake__c WHERE Id =: ApexPages.currentPage().getParameters().get('intakeId')];
		Integer censusNumber = Integer.valueOf(currentIntake.CensusNumber__c);
		Integer intakeSize = currentIntake.Intake_Unit__r.size();

		Set<Id> unitIdList = new Set<Id>();
		for (Intake_Unit__c intakeUnit : currentIntake.Intake_Unit__r) {
			unitIdList.add(intakeUnit.Unit_of_Study__c);
		}

		// Fetch list of EUOS records
		List<Enrolment_Unit_of_Study__c> euosList = [SELECT Id FROM Enrolment_Unit_of_Study__c WHERE Enrolment_Unit__r.Unit__c IN :unitIdList AND Enrolment_Unit__r.Enrolment__c = :enrolmentId AND Enrolment_Unit__r.Unit_Result__r.Name != 'Credit Transfer'];
		intakeSize = euosList.size();

		System.debug('euosList size ::: ' + euosList.size());

		
		// Build list of classes
		// Iterate over the classes selected on Page 2
		for (Page2Wrapper p : enrolWrapperList) {
			if (p.ch == TRUE) {
				tempClass.add(p.cls);
			}
		}
		
		if (!tempClass.isEmpty()) {
			// Condition: At least one class has been selected for enrolment
			for (Classes__c c : tempClass) {
				//MAM 08/23/2013 retrieve classes assigned to Class Delivery Option start
				if (String.isNotEmpty(c.Intake_Unit__c)) {
					iuIds.add(c.Intake_Unit__c);
				} else if (String.isNotEmpty(c.Alternate_Class_Delivery_Option__c)) {
					classClassDelOptionIds.add(c.id);
				}
				//MAM 08/23/2013 end
			}

			// Query intake units
			iuList = [SELECT Id, Name, Unit_Name__c, Unit_of_Study__r.Name FROM Intake_Unit__c WHERE Id IN: iuIds];
			
			if (!iuList.isEmpty()) {
				// Create map to hold unit codes and intake unit record 
				for (Intake_Unit__c i: iuList) {
					iuMap.put(i.Unit_of_Study__r.Name, i.Id);
					setUnitOfStudy.add(i.Unit_of_Study__r.Name);
				}
			}
			
			// Iterate over enrolment units
			for (Enrolment_Unit__c e : enrolmentUnitList) {
				// Iterate over existing enrolment intake units for this enrolment unit
				for (Enrolment_Intake_Unit__c i : e.Enrolment_Intake_Units__r) {
					if (i.Intake_Name__c == currentIntake.Name) {
						tempEUIds.add(e.Id);
						eiuList2.add(i);
					}
				}
			}

			// Create new EIU records
			Set<Id> eunitSet = new Set<Id>();

			// MAM 09/10/2013 Check if tempEUIds is Empty. 
			// If it is then it means that Scheduled Units were not created since Enrolment Unit Id of Enrolment does not match the Intake Unit Id for the Intake.
			Map<String, String> mUnit = new Map<String, String>();
			if (eiuList.isEmpty()) {
				for (Enrolment_Unit__c e: enrolmentUnitList) {
					eunitSet.add(e.Id);
					if (iuMap.containsKey(e.Unit__r.Name)) {
						if (!tempEUIds.contains(e.Id) || tempEUIds.isEmpty()) { //Add MAM 09/09/2013 insert EIU records if tempEUIds is empty
							Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
							eiu.Intake_Unit__c = iuMap.get(e.Unit__r.Name);
							eiu.Enrolment__c = enrolmentId;
							eiu.Enrolment_Unit__c = e.Id;
							eiuList.add(eiu);
						}
					} else {
						// If EUI list is Empty or if the selected Unit of Study has the same name but it as different ids
						if (tempEUIds.isEmpty() || setUnitOfStudy.contains(e.Unit__r.Name)) {
							mUnit.put(e.Unit__c, e.Unit__r.Name);
						}
					}
				}
			}
			
			// If selected Classes have uiIds and if Scheduled List is Empty raise error
			if (eiuList.isEmpty() && !iuIds.isEmpty()) {
				if (!mUnit.isEmpty()) {
					errMsg = 'Error saving Student Schedule: Enrolment Unit and Intake Unit do not match for the following Enrolment Unit Records - ' + mUnit.values();
					ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,ErrMsg));
					return NULL;
				}
			}
			//MAM 09/10/2013 end
			
			//MAM 08/21/2013 Start Modify Method to obtain ErrorMessage from populateCensusDate method
			
			try {
				insert eiuList;
				
				if (eiuList.size() > 0) {
				system.debug('**EIU List : ' + eiuList.size());
					for (Classes__c c : tempClass) {
						for (Enrolment_Intake_Unit__c e : eiuList) {
							if (c.Intake_Unit__c == e.Intake_Unit__c) {
								Class_Enrolment__c enrol = new Class_Enrolment__c();
								enrol.Class__c = c.Id;
								/*
								* JEA Aug.31.2015
								* Remove Comment enrol.Enrolment_Intake_Unit__c = e.Id;
								*/
								enrol.Enrolment_Intake_Unit__c = e.Id;
								cenrolList.add(enrol);            
							}
						}
					}
				} else {
					system.debug('**EIU List 2: ' + eiuList2.size());
					if (eiuList2.size() > 0) {
						for (Classes__c c : tempClass) {
							for (Enrolment_Intake_Unit__c e : eiuList2) {
								if (c.Intake_Unit__c == e.Intake_Unit__c) {
									Class_Enrolment__c enrol = new Class_Enrolment__c();
									enrol.Class__c = c.Id;
									enrol.Enrolment_Intake_Unit__c = e.Id;
									cenrolList.add(enrol);
								}
								//  eiuListIds.add(e.Id); 
							}
						}
					}
				}
				
				//MAM 08/23/2013 retrieve classes assigned to Class Delivery Option start
				if (!classClassDelOptionIds.isEmpty()) {
					for (String s : classClassDelOptionIds) {
						Class_Enrolment__c enrol = new Class_Enrolment__c();
						enrol.Class__c = s;
						//enrol.Enrolment_Intake_Unit__c = enrolmentId;
						enrol.Enrolment__c = enrolmentId;
						cenrolList.add(enrol);
					}
				}
				//MAM 08/23/2013 end

				// Build a set of Enrolment Unit Ids whose start and end dates need to be updated
				Set<Id> enrolmentUnitsForUpdate = new Set<Id>();
				for (Enrolment_Intake_Unit__c scheduledUnit : eiuList) {
					enrolmentUnitsForUpdate.add(scheduledUnit.Enrolment_Unit__c);
				}

				// Update enrolment unit start and end dates based on start and end dates of scheduled units
				if (activeEnrolment.Delivery_Location_State__c != 'Victoria') {
					for (Enrolment_Unit__c enrolmentUnit : enrolmentUnitList) {
						if (enrolmentUnitsForUpdate.contains(enrolmentUnit.Id)) {
							enrolmentUnit.Start_Date__c = currentIntake.Start_Date__c;
							enrolmentUnit.End_Date__c = currentIntake.End_Date__c;
						}
					}
				}
				system.debug(enrolmentUnitList);
				update enrolmentUnitList;
				
				// If there are no intake student record yet for the student in the intake, create one.
				List<Intake_Students__c> tmp = new List<Intake_Students__c>();
				tmp = [SELECT Id FROM Intake_Students__c WHERE Student__c =: studId AND Intake__c =: intakeId];
			
				//create intake student records if there are no intake student records related to the student yet
				if (tmp.size() == 0) {
					Intake_Students__c i = new Intake_Students__c();
					i.Enrolment__c = enrolmentId;
					i.Intake__c = intakeId;
					i.Student__c = studId;
					insert i;
				}

				//insert class enrolment records
				if (cenrolList.size() > 0 && !isCenrolInserted) {
				system.debug('**cenrol List 2: ' + cenrolList);
					insert cenrolList;
					isCenrolInserted = true;
				}
				
				// Check for the Enrolment Record Type before proceeding start
				if (String.valueOf(activeEnrolment.RecordType.Name).equalsIgnoreCase('Dual Funding Enrolment') || String.valueOf(activeEnrolment.RecordType.Name).equalsIgnoreCase('VET Fee Help Enrolment')) {
					Census__c census = null;

					if (isUsingCensusPeriods) {
						try {
							census = [SELECT Id, IntakeId__c, EnrolmentId__c, CensusDate__c FROM Census__c WHERE IntakeId__c = :currentIntake.Id AND EnrolmentId__c = :activeEnrolment.Id];
						} catch (QueryException e) {
							// Create a new Census object for this intake and enrolment
							census = new Census__c(
									IntakeId__c = currentIntake.Id,
									EnrolmentId__c = activeEnrolment.Id,
									CensusDate__c = currentIntake.Census_Date__c
								);
							insert census;
						}
					}

					System.debug('iuIds ::: ' + iuIds);

					errMsg = populateCensusDate(eunitSet, enrolmentId, iuIds, census, censusNumber, intakeSize, sp);
					//MAM 08/21/2013 check if there is an Error Message retrieved
					if(String.isNotEmpty(errMsg)){
						ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, errMsg));
						return NULL;
					}
				}
				//MAM 09/09/2013 end
			}
			catch(Exception e){
				Database.rollback(sp);
				refreshVariables();
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'ERROR inserting Class Enrolments: ' + e.getMessage()));
				return NULL;
			}
			//MAM 08/21/2013 end
		   // cenrolListIds=cenrolList;
		}
		
		PageReference pref = new PageReference('/' + intakeId);
		pref.setRedirect(true);
		return pref;
	}

	/**
	 * @description Updates related Census Date field of EUOS
	 * @author N/A
	 * @date N/A
	 * @param (Set<Id>) eunitSet : Set of Enrolment Unit Ids
	 * @param (Id) enrolmentId : The Id of the Enrolment record that is being scheduled
	 * @param (Set<String>) intakeUnitIds : Set of Intake Unit Ids
	 * @param (Census__c) census : The census record that the EUOS records will be collected under
	 * @param (Integer)	intakeSize: The total number of intake units that will be used to compute the tuition fees based on the Census regulations
	 * @param (Savepoint) sp : The savepoint object to roll back to when errors are encountered
	 * @history
	 *       30.OCT.2015    Ranyel Maliwanag    - Added census parameter to the method signature to associate EUOS records with
	 *       18.NOV.2015    Ranyel Maliwanag    - Added weighted census split metadata processing to determine the split proportion
	 *		 02.DEC.2015	Ranyel Maliwanag	- Implemented support for Student Fee Types to determine the rate
	 *		 20.JAN.2016	Ranyel Maliwanag	- Added intakeSize parameter to compute the tuition fee based on the total number of intake units
	 */
	public String populateCensusDate(Set<Id> eunitSet, Id enrolmentId, Set<String> intakeUnitIds, Census__c census, Integer censusNumber, Integer intakeSize, Savepoint sp){
		String errorMsg;
		Tuition_Fee__c tuitionFee;
		Tuition_Fee__c regularTuitionFee;
		Decimal totalAmount = 0.0;
		Set<Id> unitIdsForIUOS = new Set<Id>();
		Set<Id> unitIds = new Set<Id>();
		List<Enrolment_Intake_Unit__c> enrolmentIntakeUnitList = new List<Enrolment_Intake_Unit__c>();
		List<Enrolment_Unit_of_Study__c> enrolmentUnitStudyUpdateList = new List<Enrolment_Unit_of_Study__c>();
		List<Enrolment_Unit_of_Study__c> enrolmentUnitStudyList = new List<Enrolment_Unit_of_Study__c>(); 
		Map<Id, Date> eiuMap = new Map<Id, Date>();
		Map<Id,Decimal> unitsMap = new Map<Id,Decimal>();  

		Integer censusPeriods = Integer.valueOf(customSettings.CensusPeriods__c);
		Qualification__c qual = [SELECT Id, Name, CensusPeriods__c, (SELECT Id, Stage__c FROM Qualification_Units__r WHERE Unit_Record_Type__c = 'Unit of Study' AND Don_t_auto_add_this_unit_to_enrolments__c = false) FROM Qualification__c WHERE Id =: activeEnrolment.Qualification__c LIMIT 1];
		if (qual != null && qual.CensusPeriods__c != null) {
			censusPeriods = Integer.valueOf(qual.CensusPeriods__c);
		}

		// Mapping of <Stage Id> : <Number of Units>
		Map<Id, Integer> stageUnitsMap = new Map<Id, Integer>();

		// Determine unit size per stage
		for (Qualification_Unit__c qualUnit : qual.Qualification_Units__r) {
			if (stageUnitsMap.containsKey(qualUnit.Stage__c)) {
				Integer currentCount = stageUnitsMap.get(qualUnit.Stage__c);
				currentCount++;
				stageUnitsMap.put(qualUnit.Stage__c, currentCount);
			} else {
				stageUnitsMap.put(qualUnit.Stage__c, 1);
			}
		}

		System.debug('stageUnitsMap ::: ' + stageUnitsMap);

		// @parked
		//// Query census splits metadata
		//List<CensusSplit__mdt> censusSplits = [SELECT Id, CensusNumber__c, Weight__c FROM CensusSplit__mdt];

		//// Use the number of census split records to determine the number of census periods if there are existing records and it's explicitly configured to use weighted splits from the portal settings
		//if (customSettings.IsUsingWeightedSplits__c && censusSplits.size() > 0) {
		//	// Condition: Custom setting is configured to use weighted splits and there are existing split records
		//	censusPeriods = censusSplits.size();
		//}

		//// Build mapping of the weights per census
		//Map<Integer, Decimal> censusWeights = new Map<Integer, Decimal>();
		//for (CensusSplit__mdt censusSplit : censusSplits) {
		//	// Set equal weight split as default
		//	Decimal weight = 100.0 / censusPeriods;
		//	if (censusSplit.Weight__c > 0) {
		//		weight = censusSplit.Weight__c * 1.0;
		//	}
		//	weight = weight / 100.0;

		//	censusWeights.put(Integer.valueOf(censusSplit.CensusNumber__c), weight);
		//}

		//Boolean isUsingWeightedSplits = censusNumber != null && censusWeights.keySet().size() > 0 && censusWeights.keySet().contains(censusNumber);

		// Given = EUOS : Status Code, Status Code : State, Tuition Fee : State
		// Required = Status Code : Tuition Fee 
		Set<Id> stateIds = new Set<Id>();
		// Mapping of State : Tuition Fee
		Map<Id, Tuition_Fee__c> stateFees = new Map<Id, Tuition_Fee__c>();

		// Query the Tuition Fee record for the given student
		if (activeEnrolment != null && qual != null) {
			String deliveryLocationState = activeEnrolment.Delivery_Location_State__c;
			//String queryString = 'SELECT State__r.Name, State__c, Qualification__c, Name, Id, Amount_per_Scheduled_Hour__c, Subsidised_Amount_per_Scheduled_Hour__c, CourseAmount__c, SubsidisedCourseAmount__c ';
			//queryString += 'FROM Tuition_Fee__c ';
			String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Tuition_Fee__c, ', State__r.Name ');
			queryString += 'WHERE Qualification__c = \'' + qual.Id + '\' AND State__r.Name = :deliveryLocationState';

			regularTuitionFee = Database.query(queryString);
		}

		System.debug('intakeUnitIds ::: ' + intakeUnitIds);

		for(Intake_Unit_of_Study__c iuos: [SELECT Id, Unit__c, Unit__r.Name FROM Intake_Unit_of_Study__c WHERE Intake_Unit_of_Competency__c IN: intakeUnitIds]){
			unitIdsForIUOS.add(iuos.Unit__c);
			System.debug('Unit ::: ' + iuos.Unit__r.Name);
		}

		System.debug('unitIdsForIUOS ::: ' +  unitIdsForIUOS);
		
		enrolmentUnitStudyList = [SELECT Id, Enrolment_Unit__r.Enrolment__c, Total_Amount_Charged__c, Enrolment_Unit__r.Unit__c, Qualification_Unit__r.Unit__c, Enrolment_Unit__r.Enrolment__r.Delivery_Location_State__c, Census_date__c, Loan_Fee__c, Amount_Paid_Upfront__c, Student_Status_Code__r.Code__c, Student_Status_Code__r.Student_Status_Code_State__c, Student_Status_Code__r.Use_Subsidised_Amount__c, Enrolment_Unit__r.Enrolment__r.Student_Status_Code__r.Code__c, StudentFeeTypeId__c, StudentFeeTypeId__r.StateId__c, StudentFeeTypeId__r.StateId__r.Name, StudentFeeTypeId__r.FeeType__c, StudentFeeTypeId__r.Name, Qualification_Unit__r.Stage__c
								FROM Enrolment_Unit_of_Study__c 
								WHERE Enrolment_Unit__r.Enrolment__c =: enrolmentId 
								AND (Recredit_Revision__c != 'Recredit' AND Recredit_Revision__c != 'Revision')
								AND Qualification_Unit__r.Unit__c IN : unitIdsForIUOS];

		System.debug('enrolmentUnitStudyList ::: ' + enrolmentUnitStudyList);
		
		for(Enrolment_Unit_of_Study__c euos: enrolmentUnitStudyList){
			unitIds.add(euos.Qualification_Unit__r.Unit__c);
		}
				
		//MAM 09/09/2013 Check for the Tuition Fee only if these exist start
		if(regularTuitionFee != null){
			for(Unit_Hours_and_Points__c unitsHours : [Select u.Unit__c, u.Unit_Record_Type__c, u.Tuition_Fee_Hours__c, u.Points__c, u.Placement_Hours__c, u.Nominal_Hours__c, u.Name, u.Institutional_Hours__c, u.State__c From Unit_Hours_and_Points__c u Where u.Unit__c in :unitIds AND u.State__c  = : regulartuitionFee.State__c]){
				unitsMap.put(unitsHours.Unit__c,unitsHours.Tuition_Fee_Hours__c);
			}
		}
		//MAM 09/09/2013 end
		
		enrolmentIntakeUnitList = [SELECT Id, Enrolment_Unit__c, Intake_Unit__r.Intake__r.Census_Date__c FROM Enrolment_Intake_Unit__c WHERE Enrolment_Unit__c IN: eunitSet];
		
		for (Enrolment_Intake_Unit__c e: enrolmentIntakeUnitList){
			eiuMap.put(e.Enrolment_Unit__c, e.Intake_Unit__r.Intake__r.Census_Date__c);
		}

		State__c deliveryState = [SELECT Id FROM State__c WHERE Name = :activeEnrolment.Delivery_Location_State__c LIMIT 1];
		if (deliveryState != null) {
			stateIds.add(deliveryState.Id);
		}

		for (Enrolment_Unit_of_Study__c euos: enrolmentUnitStudyList) {
			stateIds.add(euos.Student_Status_Code__r.Student_Status_Code_State__c);
		}

		//String queryString = 'SELECT State__r.Name, State__c, Qualification__c, Name, Id, Amount_per_Scheduled_Hour__c, Subsidised_Amount_per_Scheduled_Hour__c ';
		//queryString += 'FROM Tuition_Fee__c ';
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Tuition_Fee__c, ', State__r.Name ');
		queryString += 'WHERE Qualification__c = \'' + qual.Id + '\'';
		List<Tuition_Fee__c> tuitionFees = Database.query(queryString);

		for (Tuition_Fee__c tfee : tuitionFees) {
			stateFees.put(tfee.State__c, tfee);
		}

		for (Enrolment_Unit_of_Study__c euos: enrolmentUnitStudyList) {
			Integer stageSize = intakeSize;
			System.debug('stageSize ::: ' + stageSize);

			if (stageUnitsMap.containsKey(euos.Qualification_Unit__r.Stage__c)) {
				stageSize = stageUnitsMap.get(euos.Qualification_Unit__r.Stage__c);
			}

			if (eiuMap.containsKey(euos.Enrolment_Unit__c)) {
				// Check if Student Fee Type is defined
				if (String.isNotBlank(euos.StudentFeeTypeId__c)) {
					if (euos.StudentFeeTypeId__r.StateId__c != null) {
						tuitionFee = stateFees.get(euos.StudentFeeTypeId__r.StateId__c);
					} else {
						// Note: regularTuitionFee is the Tuition Fee record for the Delivery Location State of the Enrolment record
						tuitionFee = regularTuitionFee;
					}

					if (tuitionFee != null) {
						String feeType = euos.StudentFeeTypeId__r.FeeType__c;
						Decimal courseAmount;
						if (feeType == 'Full') {
							courseAmount = tuitionFee.CourseAmount__c;
						} else if (feeType == 'Subsidised 1') {
							courseAmount = tuitionFee.SubsidisedCourseAmount__c;
						} else if (feeType == 'Subsidised 2') {
							courseAmount = tuitionFee.SubsidisedCourseAmount2__c;
						}

						if (courseAmount != null) {
							totalAmount = courseAmount / censusPeriods / stageSize;
						} else {
							Database.rollback(sp);
							refreshVariables();
							return errorMsg = 'Error: No ' + feeType + ' Course Amount can be found for this Tuition Fee record: ' + tuitionFee.Name;
						}
					} else {
						Database.rollback(sp);
						refreshVariables();
						return errorMsg = 'Error: No tuition fee record can be found for the state of ' + euos.StudentFeeTypeId__r.StateId__r.Name + ' for Qualification: ' + qual.Name;
					}
				} else {
					// Old fee model 
					// Fetch tution fee here
					if (euos.Student_Status_Code__r.Student_Status_Code_State__c != null) {
						tuitionFee = stateFees.get(euos.Student_Status_Code__r.Student_Status_Code_State__c);
					} else {
						tuitionFee = regularTuitionFee;
					}
					if (tuitionFee != null) {
						// Check for the unit to get full or subsidized amount
						if (euos.Student_Status_Code__r.Use_Subsidised_Amount__c == TRUE) {
							//WBM 05/04/2015 -- check if Subsidised is empty
							if (tuitionFee.Subsidised_Amount_per_Scheduled_Hour__c == NULL) {
								Database.rollback(sp);
								refreshVariables();
								return errorMsg = 'Error: There is no value for the Subsidised Amount for this State: ' + tuitionFee.State__r.Name;
							} else {
								totalAmount = unitsMap.get(euos.Qualification_Unit__r.Unit__c) * tuitionFee.Subsidised_Amount_per_Scheduled_Hour__c;
								if (isUsingCensusPeriods && censusPeriods != null) {
									if (tuitionFee.SubsidisedCourseAmount__c == null) {
										Database.rollback(sp);
										refreshVariables();
										return errorMsg = 'Error: There is no Subsidised Course Amount for this Tuition Fee record: ' + tuitionFee.Name;
									} else {
										totalAmount = tuitionFee.SubsidisedCourseAmount__c / censusPeriods / stageSize;

										// @parked
										//// Override for the tuition fee calculation if Weighted Splits feature is activated and is supported by the current intake
										//if (isUsingWeightedSplits) {
										//	totalAmount = tuitionFee.SubsidisedCourseAmount__c * censusWeights.get(censusNumber) / enrolmentUnitStudyList.size();
										//}
									}
								}
							}
						}
						else{
							//WBM 05/04/2015 -- check if Amount per Scheduled is empty
							System.debug('::: flag5');
							if(tuitionFee.Amount_per_Scheduled_Hour__c == NULL){
								System.debug('::: flag6');
								Database.rollback(sp);
								refreshVariables();
								return errorMsg = 'Error: There is no value for the Amount per Scheduled Hour for this State: ' + tuitionFee.State__r.Name ;
							} else {
								System.debug('::: flag7');
								totalAmount = unitsMap.get(euos.Qualification_Unit__r.Unit__c) * tuitionFee.Amount_per_Scheduled_Hour__c;
								if (isUsingCensusPeriods && censusPeriods != null) {
									if (tuitionFee.CourseAmount__c == null) {
										Database.rollback(sp);
										refreshVariables();
										return errorMsg = 'Error: There is no Course Amount for this Tuition Fee record: ' + tuitionFee.Name;
									} else {
										totalAmount = tuitionFee.CourseAmount__c / censusPeriods / stageSize;
										System.debug('totalAmount ::: ' + totalAmount);

										// @parked
										//// Override for the tuition fee calculation if Weighted Splits feature is activated and is supported by the current intake
										//if (isUsingWeightedSplits) {
										//	totalAmount = tuitionFee.CourseAmount__c * censusWeights.get(censusNumber) / enrolmentUnitStudyList.size();
										//}
									}
								}
							}
						}
					}
				}
				//MAM 09/09/2013 end
				
				totalAmount = totalAmount.setScale(2);
				if(euos.Census_Date__c == NULL){
					euos.Census_Date__c = eiuMap.get(euos.Enrolment_Unit__c);
				}
				if (euos.Student_Status_Code__c != null && String.isNotBlank(euos.Student_Status_Code__r.Code__c) && euos.Student_Status_Code__r.Code__c.startsWith('5')) {
					euos.Amount_paid_upfront__c = totalAmount;
				}
				euos.Total_Amount_Charged__c = totalAmount;

				// Updated EUOS to link to the created Census record
				if (isUsingCensusPeriods && census != null) {
					euos.CensusId__c = census.Id;
					euos.EnrolmentId__c = enrolmentId;
				}
				
				//04.MAY.2015 - comment out
				/*
				//13.FEB.2015 WBM
				//31.MAR.2015 KS
				if(euos.Enrolment_Unit__r.Enrolment__r.Student_Status_Code__r.Code__c == '501'){
					euos.Amount_Paid_Upfront__c = totalAmount;
					euos.Loan_Fee__c = 0.00;
				}
				//WBM End
				*/
				enrolmentUnitStudyUpdateList.add(euos);
			}
		}
		
		try{
			if(enrolmentUnitStudyUpdateList.size() > 0){
				update enrolmentUnitStudyUpdateList;
			}
		}
		catch(Exception e){
			errorMsg = 'Error updating Enrolment Unit of Study Records: '+ e.getMessage();
		}
		return errorMsg;
		//MAM 08/21/2013 end
	}
	
	public class Page2Wrapper{
		public Classes__c cls {get; set;}
		public Boolean ch {get; set;}
		public String outcome {get; set;}
		public String sday {get; set;}
		public String eday {get; set;}
		
		public Page2Wrapper(Classes__c enrol, Boolean check, String out, String sday, String eday){
			cls = enrol;
			ch = check;
			outcome = out;
			this.sday = sday;
			this.eday = eday;
		}
	}
}