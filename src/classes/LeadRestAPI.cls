/**
 * @description API class for accepting leads from different sources
 * @author Andrew Burgess
 * @date 20.JUL.2015
 * @history
 *       16.MAR.2016    Ranyel Maliwanag    - Brought back mapping of sourceId to SID__c field so that it's value maps to both SID__c and Source_Id__c field. Reference Case: 00049774
 *       06.APR.2016    Ranyel Maliwanag    - Added `CurrentEmploymentStatus__c` field to the request strucutre. Reference Case: 00063056
 *       20.APR.2016    Biao Zhang          - Add 'Description'. Refer case 00056704.
 */
@RestResource(urlMapping='/Lead/*')
global without sharing class LeadRestAPI {

    /**
     * @description GET method handler for accepting leads
     * @author Andrew Burgess
     * @date 20.JUL.2015
     * @return (String) The lead creation response string
     */
    @HttpGet
    global static String doGet() {
        Lead leadRecord = new Lead();
        String result = '';
        leadRecord.FirstName = RestContext.request.params.get('firstName');
        leadRecord.LastName = RestContext.request.params.get('lastName');
        try {
            insert leadRecord;
            result = leadRecord.Id;
        } catch (Exception e) {
            result = e.getMessage();
        }
        
        return result;
    }

    /**
     * @description POST method handler for accepting leads
     * @author Andrew Burgess
     * @date 20.JUL.2015
     * @return (String) The lead creation response string
     */
    @HttpPost
    global static String doPost(String first_name, String last_name, String phone, String email, String zip, String state, String referralStream, 
            String sourceId, String leadId, String affiliateId,String city,String street,String country,String courseEnquiry, String employmentStatus, String description) {

        String result = '';
        Lead leadRecord = new Lead(
                FirstName = first_name,
                LastName = last_name,
                Phone = phone,
                Email = email,
                PostalCode = zip,
                State = state,
                City = city,
                Country = country,
                Street = street,
                Referral_Source_Type_Detail__c = referralStream,
                Source_Id__c = sourceId,
                SID__c = sourceId,
                Lead_ID__c = leadId,
                Affiliate_Id__c = affiliateId,
                Course_Interested_In__c = courseEnquiry,
                CurrentEmploymentStatus__c = employmentStatus,
                OwnerId ='00G6F000002oemWUAQ',
                Description = description
            );
        try {
            insert leadRecord;
            result = 'Success';
        } catch (Exception e) {
            result = e.getMessage();
        }
        return result;
    }
}