/*BlackBoard Course Integration Class only
 *complies with methods required for integration
 *any further modifications should be noted here
 *SIS and WS methods are combined in this class
 *Please keep SIS methods high in this class
 *Please Refer to method comments for more details
 *written by YG, CloudSherpas 21 May 2014
 */

Global class BlackboardCourseWS {

   /*This method is used for SIS callout only
    *It is void, because SIS provides no feedback
    *and we do not any other error handling for this operation
    */
   public static void saveCoursesSIS(List<Blackboard_Course__c> bbcourses) {

      Http h = new Http();

      String username = Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).SIS_End_Point_Username__c;
      String password = Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).SIS_End_Point_Password__c;
  
      String XMLBody ='';


      HttpRequest req = new HttpRequest();
      req.setEndpoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).SIS_Endpoint__c);
      req.setMethod('POST');
      req.setHeader('Content-type','text/xml');
      Blob headerValue = Blob.valueOf(username + ':' + password);
      String authorizationHeader = 'BASIC ' +
      EncodingUtil.base64Encode(headerValue);
      req.setHeader('Authorization', authorizationHeader);

      String coursesXML = BlackboardXMLBuilder.buildCourseSISXML(bbcourses);

         XMLBody =  '<enterprise> ' +
                       '<properties></properties> ' +
                        
                       coursesXML +
             
                    '</enterprise>';
  
     
    
    
      req.SetBody(XMLBody);

      /*Since we're not getting any response back
       *We will not emulate send request to SIS for the purpose of test method
       */
      if(!Test.isRunningTest()) {
        // Send the request, and return a response
        HttpResponse res = h.send(req);
      }

      for(Blackboard_Course__c course : bbcourses) {
        course.SIS_Sync_Attempted__c = true;
      }

      update bbcourses;

    
   }


  
   /*This method is used for Web Service Callouts only
    *Process will execute only 1 record at the time
    *for error handling
    *It will create and updated course records in blackboard
    *there are additional functionality from SFDC side
    *as update maybe deactivate/reactivate/update
    *but from BB side its saveCourse, so will
    *use apex code to determine what we should do
    *with the records
   */
   public static HTTPResponse saveCourse(Blackboard_Course__c bbcourse) {
   
     String IntegrationPoint = '';
     
     List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
     
     String SessionId = log.get(0).Session_Id__c;
     
     Id logId = log.get(0).Id;
     
     String syncStatus = '';
     
     String Message = '';
     
     String coursesXML = BlackBoardXMLBuilder.buildCourseWSXML(bbcourse);
     
     String blackboardPrimaryKey = ''; 
     
     String SoapXMLBody ='';
      
     SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
          '  <soapenv:Header>'+
          '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                '  <wsu:Timestamp wsu:Id="TS-24">'+
                    '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                    
                '  </wsu:Timestamp>'+
                '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                '    <wsse:Username>session</wsse:Username>'+
                '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                    
                '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
    
        
        
      
        
        ' <soapenv:Body> ' +
          
          '  <cour:saveCourse>'+
          
                 coursesXML +
          
          '  </cour:saveCourse>' +
        
        
        ' </soapenv:Body> ' +
      
      '</soapenv:Envelope>';
     
     string SoapXML;
     SoapXML = SoapXMLBody;
     Integer ContentLength = 0;
     ContentLength = SoapXML.length();
            
     Http h = new Http();
     HttpRequest req = new HttpRequest();
     HttpResponse res = new HttpResponse();

     req.setMethod('POST');
     req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_WS_Endpoint__c); 
     req.setHeader('Content-type','text/xml');
     req.setHeader('Content-Length',ContentLength.format());  
     req.setHeader('SoapAction','saveCourse');
     req.setBody(SoapXML); 
        
     

     if(Test.isRunningTest())
        res.setBody('<return>_1234</return>');
     else
        res = h.send(req);

     XmlStreamReader reader = res.getXmlStreamReader();
      
      while(reader.hasNext()) {
           //  Start at the beginning of the book and make sure that it is a book
           if (reader.getEventType() == XmlTag.START_ELEMENT) {
              if ('return' == reader.getLocalName()) {
                 blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                 syncStatus = 'Success';
                 Message = 'Record Synced';           
                 break;
              }
              
              if('faultstring' == reader.getLocalName()) {
                syncStatus = 'Fail';
                Message = BlackboardUtility.parseResult(reader);
                break;
              }
           }
          reader.next();
      }
      

      if(SyncStatus == 'Success') {
      
          bbcourse.Blackboard_Primary_Key__c = blackboardPrimaryKey;
          bbcourse.Date_of_Last_Sync__c = System.Now();
          bbcourse.Changed_Since_Last_Sync__c  = false; 
          bbcourse.Available_In_BlackBoard__c = bbcourse.Available_After_Sync__c;
          
          
      }/*write back to master unit */
        try {
            update bbcourse;
        } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
          
      if(bbcourse.RecordType.DeveloperName == 'T2_Template_Course')
          IntegrationPoint = 'SaveCourse';
      if(bbcourse.RecordType.DeveloperName == 'T5_Support_Materials')
          IntegrationPoint = 'SaveSupportMaterial';
      
      BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bbcourse.Id,syncStatus, IntegrationPoint); //create log event for each record processed

    
     return res;
      
        
   }
    
    /*This method is used for Web Service Callouts only
    *Process will execute only 1 record at the time
    *for error handling
    *It will retrieve PrimaryKey(PK) from Blackboard
   */
    public static HTTPResponse getCourse(Blackboard_Course__c bbcourse) {

     List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
     
     String SessionId = log.get(0).Session_Id__c;
     
     Id logId = log.get(0).Id;
     
     String syncStatus = '';
     
     String Message = '';
     
     String coursesXML = BlackboardXMLBuilder.buildCourseWSXML(bbcourse);

     String SoapXMLBody ='';

     String blackboardPrimaryKey = '';
      
     SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
              '  <soapenv:Header>'+
              '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                    '  <wsu:Timestamp wsu:Id="TS-24">'+
                        '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                        '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                        
                    '  </wsu:Timestamp>'+
                    '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                    '    <wsse:Username>session</wsse:Username>'+
                    '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                        
                    '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
        
            
            
          
            
            ' <soapenv:Body> ' +
              
              '  <cour:getCourse>'+
              
                     coursesXML +
              
              '  </cour:getCourse>' +
            
            
            ' </soapenv:Body> ' +
          
          '</soapenv:Envelope>';

     String SoapXML;
     SoapXML = SoapXMLBody;
     Integer ContentLength = 0;
     ContentLength = SoapXML.length();
     Http h = new Http();     
     HttpRequest req = new HttpRequest();
     HttpResponse res = new HttpResponse();

     req.setMethod('POST');
     req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_WS_Endpoint__c); 
     req.setHeader('Content-type','text/xml');
     req.setHeader('Content-Length',ContentLength.format());  
     req.setHeader('SoapAction','getCourse');
     req.setBody(SoapXML); 
    

     if(Test.isRunningTest())
        res.setBody('<id>_1234</id>');
      else
        res = h.send(req);

     XmlStreamReader reader = res.getXmlStreamReader();
      
     while(reader.hasNext()) {
           //  Start at the beginning of the book and make sure that it is a book
          if (reader.getEventType() == XmlTag.START_ELEMENT) {
               if ('id' == reader.getLocalName()) {
                 blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                 syncStatus = 'Success';
                 Message = 'Record Synced';           
                 break;
              }
              
              
           }
          reader.next();
        }


     if(SyncStatus == 'Success') {
      
          bbcourse.Blackboard_Primary_Key__c = blackboardPrimaryKey;
          bbcourse.Changed_Since_Last_Sync__c  = false; 
          bbcourse.Available_In_BlackBoard__c = true;
          bbcourse.SIS_Sync_Attempted__c = false;
          bbcourse.Get_T3_PK_Attempted__c = false;
          
      /*write back to master unit */
        try {
            update bbcourse;
        } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
          
     } else {

          if(bbcourse.Get_T3_PK_Attempted__c) {

             bbcourse.SIS_Sync_Attempted__c = false;
             bbcourse.Get_T3_PK_Attempted__c = false;
             Message = 'SIS ERROR, check blackboard log for more details';
             syncStatus = 'Fail';

          }
          else {
             bbcourse.Get_T3_PK_Attempted__c = true;
             Message = 'Failed first PK get attempt';
             syncStatus = 'Fail';
          }

          try {
            update bbcourse;
          } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
          

      }  
       
      
      BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bbcourse.Id,syncStatus, 'getCourse'); //create log event for each record processed



     return res;  
    

    }

    /*This method is used for Web Service Callouts only
    *Process will execute only 1 record at the time
    *for error handling
    *Type4 recordtypes only(SFDC) used for Organisations(RecordType4) in BB
    *This will execute Create function(Create)
    *This is separated from updated, because of BB
    *requiring create/update instead of save(upsert) for course
    */
     public static HTTPResponse createOrg(Blackboard_Course__c bborg) {

         List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
     
         String SessionId = log.get(0).Session_Id__c;
     
         Id logId = log.get(0).Id;
     
         String syncStatus = '';
     
         String Message = '';
     
         String orgXML = BlackboardXMLBuilder.buildCourseWSXML(bborg);
         
         /*Type4 recordtype blackboard course record type */
              
       String blackboardPrimaryKey = '';
     
       String SoapXMLBody ='';       

       SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
                  '  <soapenv:Header>'+
                  '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                        '  <wsu:Timestamp wsu:Id="TS-24">'+
                            '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                            '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                            
                        '  </wsu:Timestamp>'+
                        '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                        '    <wsse:Username>session</wsse:Username>'+
                        '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                            
                        '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                        '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
            
                
                
              
                
                ' <soapenv:Body> ' +
                  
                  '  <cour:createOrg>'+
                  
                         orgXML +
                  
                  '  </cour:createOrg>' +
                
                
                ' </soapenv:Body> ' +
              
              '</soapenv:Envelope>';
             
      string SoapXML;
      SoapXML = SoapXMLBody;
      Integer ContentLength = 0;
      ContentLength = SoapXML.length();
            
      Http h = new Http();

      HttpRequest req = new HttpRequest();

      HttpResponse res = new HttpResponse();
      

      req.setMethod('POST');

      req.setEndPoint('https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Course.WS'); 

      req.setHeader('Content-type','text/xml');
      req.setHeader('Content-Length',ContentLength.format());

      req.setHeader('SoapAction','createOrg');
      
      req.setBody(SoapXML);

      

      if(Test.isRunningTest())
        res.setBody('<return>_1234</return>');
      else
        res = h.send(req);
      
      XmlStreamReader reader = res.getXmlStreamReader();
      
      while(reader.hasNext()) {
           //  Start at the beginning of the book and make sure that it is a book
           if (reader.getEventType() == XmlTag.START_ELEMENT) {
              if ('return' == reader.getLocalName()) {
                 blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                 syncStatus = 'Success';
                 Message = 'Record Synced';           
                 break;
              }
              
              if('faultstring' == reader.getLocalName()) {
                syncStatus = 'Fail';
                Message = BlackboardUtility.parseResult(reader);
                break;
              }
           }
          reader.next();
      }
      
      if(SyncStatus == 'Success') {
      
          bborg.Blackboard_Primary_Key__c = blackboardPrimaryKey;
          bborg.Date_of_Last_Sync__c = System.Now();
          bborg.Changed_Since_Last_Sync__c  = false; 
          bborg.Available_In_BlackBoard__c = true;

          
          
      }/*write back to master unit */
        try {
            update bborg;
        } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
          
        
       
      
      BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bborg.Id,syncStatus, 'createOrg'); //create log event for each record processed

      return res;

     }

    /*This method is used for Web Service Callouts only
    *Process will execute only 1 record at the time
    *for error handling
    *This will execute Create function(Create)
    *This is separated from updated, because of BB
    *requiring create/update instead of save(upsert) for course
    */

     public static HTTPResponse updateOrg(Blackboard_Course__c bborg) {

       List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
     
       String SessionId = log.get(0).Session_Id__c;
     
       Id logId = log.get(0).Id;
     
       String syncStatus = '';
     
       String Message = '';

       String blackboardPrimaryKey = '';
     
       String orgXML = BlackboardXMLBuilder.buildCourseWSXML(bborg);

       String SoapXMLBody ='';       

       SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
              '  <soapenv:Header>'+
              '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                    '  <wsu:Timestamp wsu:Id="TS-24">'+
                        '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                        '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                        
                    '  </wsu:Timestamp>'+
                    '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                    '    <wsse:Username>session</wsse:Username>'+
                    '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                        
                    '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                    '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
        
            
            
          
            
            ' <soapenv:Body> ' +
              
              '  <cour:updateOrg>'+
              
                     orgXML +
              
              '  </cour:updateOrg>' +
            
            
            ' </soapenv:Body> ' +
          
          '</soapenv:Envelope>';

      string SoapXML;
      SoapXML = SoapXMLBody;
      Integer ContentLength = 0;
      ContentLength = SoapXML.length();
            
      Http h = new Http();

      HttpRequest req = new HttpRequest();

      HttpResponse res = new HttpResponse();
      

      req.setMethod('POST');

      req.setEndPoint('https://careersaustraliastaging.blackboard.com:443/webapps/ws/services/Course.WS'); 

      req.setHeader('Content-type','text/xml');
      req.setHeader('Content-Length',ContentLength.format());

      req.setHeader('SoapAction','updateOrg');
      
      req.setBody(SoapXML);

      

      if(Test.isRunningTest())
        res.setBody('<return>_1234</return>');
      else
        res = h.send(req);


       XmlStreamReader reader = res.getXmlStreamReader();
      
      while(reader.hasNext()) {
           //  Start at the beginning of the book and make sure that it is a book
           if (reader.getEventType() == XmlTag.START_ELEMENT) {
              if ('return' == reader.getLocalName()) {
                 blackboardPrimaryKey  = BlackboardUtility.parseResult(reader);
                 syncStatus = 'Success';
                 Message = 'Record Synced';           
                 break;
              }
              
              if('faultstring' == reader.getLocalName()) {
                syncStatus = 'Fail';
                Message = BlackboardUtility.parseResult(reader);
                break;
              }
           }
          reader.next();
      }

        if(SyncStatus == 'Success') {
      
          bborg.Blackboard_Primary_Key__c = blackboardPrimaryKey;
          bborg.Date_of_Last_Sync__c = System.Now();
          bborg.Changed_Since_Last_Sync__c  = false; 
          bborg.Available_In_BlackBoard__c = bborg.Available_After_Sync__c;

          
          
      }/*write back to master unit */
        try {
            update bborg;
        } catch(System.DmlException ex) {SyncStatus = 'Fail'; Message = ex.getDmlMessage(0);}
          
        
       
      
      BlackBoardUtility.createBlackBoardSyncEvents(logId,Message,bborg.Id,syncStatus, 'updateOrg'); //create log event for each record processed

      

      return res;
     }
     
     /*peforms login, for get operation */
     WebService static void singleGetOperation(string recordId) {

        HttpResponse initResponse; //initialize response
        HttpResponse loginReponse; //login response

      
        String sessionId = '';
        String result='';

        String syncStatus = '';
        String Message = '';
    
        BlackboardcontextWS contextWS = new BlackboardcontextWS();
    
        initResponse = contextWS.initialize();

    
        XmlStreamReader reader = initResponse.getXmlStreamReader();

         while(reader.hasNext()) {
             //  Start at the beginning of the book and make sure that it is a book
             if (reader.getEventType() == XmlTag.START_ELEMENT) {

                if ('return' == reader.getLocalName()) {
                   sessionId = BlackboardUtility.parseResult(reader);             
                   break;
                }
             }
                reader.next();

           }
               
        loginReponse = contextWS.login(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).userid__c, Blackboard_Configuration__c.getInstance('WS_Config Sandbox').password__c, 'YG','YG','', Integer.ValueOf(Blackboard_Configuration__c.getInstance('WS_Config Sandbox').expectedLifeSeconds__c), sessionId);
        getSingleBlackboardCourse(recordId, sessionId);
       

     }

     /*This method performs a single WS callout to retrieve primary key
      *from blackboard and set to BB course record
     */
     
     @future(callout=true)
     public static void getSingleBlackboardCourse(string bbcid, String sessionId) {     
        
        Blackboard_Course__c bbcourse = [Select name, Blackboard_Primary_Key__c, Recordtype.DeveloperName From Blackboard_Course__c Where id = : bbcid];
        String syncStatus = '';
        String Message = '';

        //For orgs we have to use separate WS
        if(bbcourse.RecordType.DeveloperName == 'T4_Organisation') {


              String SoapXMLBody ='';

              String blackboardPrimaryKey = '';
      
              SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
                            '  <soapenv:Header>'+
                            '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                                  '  <wsu:Timestamp wsu:Id="TS-24">'+
                                      '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                      '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                                      
                                  '  </wsu:Timestamp>'+
                                  '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                                  '    <wsse:Username>session</wsse:Username>'+
                                  '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                                      
                                  '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                  '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                      
                          
                          
                        
                          
                          ' <soapenv:Body> ' +
                            
                            '  <cour:getOrg>'+
                            
                                   '<cour:filter> ' +
              
                
             
                                    '<xsd:courseIds>'+bbcourse.name +'</xsd:courseIds>'+
                                   
                                    
                                  
                                    '<xsd:filterType>1</xsd:filterType>'+
                                   
                                 
                                  '</cour:filter>'+
                            
                            '  </cour:getOrg>' +
                          
                          
                          ' </soapenv:Body> ' +
                        
                        '</soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_WS_Endpoint__c); 
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getOrg');
               req.setBody(SoapXML); 
              

               if(Test.isRunningTest())
                  res.setBody('<id>_1234</id>');
                else
                  res = h.send(req);

               XmlStreamReader reader2 = res.getXmlStreamReader();
                
               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('id' == reader2.getLocalName()) {
                           blackboardPrimaryKey  = BlackboardUtility.parseResult(reader2);
                           syncStatus = 'Success';
                           Message = 'Record Synced';
                              
                           break;
                        }
                        
                        if('faultstring' == reader2.getLocalName()) {
                          syncStatus = 'Fail';
                          Message = BlackboardUtility.parseResult(reader2);
                          break;
                        }
                    }
                    reader2.next();
               }


          if(syncStatus == 'Success') {
            bbcourse.Blackboard_Primary_Key__c = blackboardPrimaryKey;
            update bbcourse;

              
           }
           
           BlackboardUtility.createBlackBoardLog(sessionId);
           List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
           BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbcourse.Id,syncStatus, 'getCourse'); //create log event for each record processed

         

          } else {

              String SoapXMLBody ='';

              String blackboardPrimaryKey = '';
      
              SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cour="http://course.ws.blackboard" xmlns:xsd="http://course.ws.blackboard/xsd">' +
      
                            '  <soapenv:Header>'+
                            '      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                                  '  <wsu:Timestamp wsu:Id="TS-24">'+
                                      '<wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                      '<wsu:Expires>'+BlackboardUtility.expiredDate()+'</wsu:Expires>'+
                                      
                                  '  </wsu:Timestamp>'+
                                  '  <wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                                  '    <wsse:Username>session</wsse:Username>'+
                                  '    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+SessionId+'</wsse:Password>'+
                                      
                                  '      <wsu:Created>'+BlackboardUtility.createdDate()+'</wsu:Created>'+
                                  '  </wsse:UsernameToken></wsse:Security> </soapenv:Header>'+
                      
                          
                          
                        
                          
                          ' <soapenv:Body> ' +
                            
                            '  <cour:getCourse>'+
                            
                                   '<cour:filter> ' +
              
                
             
                                    '<xsd:courseIds>'+bbcourse.name +'</xsd:courseIds>'+
                                   
                                    
                                  
                                    '<xsd:filterType>1</xsd:filterType>'+
                                   
                                 
                                  '</cour:filter>'+
                            
                            '  </cour:getCourse>' +
                          
                          
                          ' </soapenv:Body> ' +
                        
                        '</soapenv:Envelope>';

               String SoapXML;
               SoapXML = SoapXMLBody;
               Integer ContentLength = 0;
               ContentLength = SoapXML.length();
               Http h = new Http();     
               HttpRequest req = new HttpRequest();
               HttpResponse res = new HttpResponse();

               req.setMethod('POST');
               req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Course_WS_Endpoint__c); 
               req.setHeader('Content-type','text/xml');
               req.setHeader('Content-Length',ContentLength.format());  
               req.setHeader('SoapAction','getCourse');
               req.setBody(SoapXML); 
              

               if(Test.isRunningTest())
                  res.setBody('<id>_1234</id>');
                else
                  res = h.send(req);

               XmlStreamReader reader2 = res.getXmlStreamReader();
                
               while(reader2.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader2.getEventType() == XmlTag.START_ELEMENT) {
                         if ('id' == reader2.getLocalName()) {
                           blackboardPrimaryKey  = BlackboardUtility.parseResult(reader2);
                           syncStatus = 'Success';
                           Message = 'Record Synced'; 
                           break;
                        }
                        
                        if('faultstring' == reader2.getLocalName()) {
                          syncStatus = 'Fail';
                          Message = BlackboardUtility.parseResult(reader2);
                        
                          break;
                        }
                    }
                    reader2.next();
               }


          if(syncStatus == 'Success') {
            bbcourse.Blackboard_Primary_Key__c = blackboardPrimaryKey;
            update bbcourse;

              
           }
             BlackboardUtility.createBlackBoardLog(sessionId);
            List<Blackboard_Sync_Log__c> log = new List<Blackboard_Sync_Log__c>([Select id,Session_Id__c From Blackboard_Sync_Log__c Order By CreatedDate Desc LIMIT 1]);
            BlackBoardUtility.createBlackBoardSyncEvents(log.get(0).Id,Message,bbcourse.Id,syncStatus, 'getCourse'); //create log event for each record processed


          }

     }
  
    
}