/**
 * @description Controller for AssignIntakeToClasses page 
 * @author Janella Lauren Canlas
 * @date 07.NOV.2012
 *
 * HISTORY
 * - 07.NOV.2012	Janella Canlas - Created.
 * - 16.JAN.2013	Janella Canlas - added method deleteIntakeStudents
 */

public with sharing class AssignIntakeToClasses_CC {
	//declare variables to be used
	public Id intakeId;
	public List<WrapperClass> classWrapList {get;set;}
	public Id intakeUnitId {get;set;}
	
	public AssignIntakeToClasses_CC(){
		intakeId = ApexPages.currentPage().getParameters().get('intakeId');//retrieves the intake ID passed from the button
		
		fillHelperClass();
	}
	//retrieve current intake record
	public Intake__c getIntakeRecord(){
        Intake__c intake = [SELECT id from Intake__c where Id =: intakeId LIMIT 1];
        return intake;
    }
    //display intake units as picklist
    public List<SelectOption> getIntakeUnitsPicklist() {
        List<SelectOption> options = new List<SelectOption>();                       
        options.add(new SelectOption('', ''));   
        for (Intake_Unit__c iu : [SELECT Id, Name,Unit_Name__c, Unit_of_Study__r.Name from Intake_Unit__c where Intake__c =: intakeId]) {
            options.add(new SelectOption(iu.Id, iu.Unit_of_Study__r.Name +' - '+ iu.Unit_Name__c));
        }  
        return options;
    }
    
    public class WrapperClass {
	    public Classes__c c {get;set;}
	    
	    public WrapperClass(Classes__c passClassRecord) {   
		    c = passClassRecord;
	    }
    }
    //this will fill the table of classes
    public void fillHelperClass() {
        //reinstantiate variables
        classWrapList = new List<WrapperClass>();
        List<Classes__c> classList = new List<Classes__c>();
        //query the related classes to the intake id
        classList = [SELECT id, Class_Configuration__c, End_Date_Time__c, Intake__c,
        				Room__c,Room_Address__c,Start_Date_Time__c,Name,Unit_Name__c,
        				Intake_Unit__r.Name, Intake_Unit__c, Intake_Unit__r.Unit_of_Study__r.Name,
        				Alternate_Class_Delivery_Option__c,Class_Delivery__c
        				FROM Classes__c where Intake__c =: intakeId ORDER BY Start_Date_Time__c];
       	//populate the list which will be used to display data on page
       	for(Classes__c c : classList){
       		WrapperClass wc = new WrapperClass(c);
       		classWrapList.add(wc);
       	}
    }
    //upon clicking of the save button
    public PageReference updateClasses(){
    	List<Classes__c> classList = new List<Classes__c>();
    	for(WrapperClass w : classWrapList){	
    		if(w.c.Intake_Unit__c != null && w.c.Alternate_Class_Delivery_Option__c != null){
    			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'You can only choose Unit OR Alternate Class Delivery Option.'));
    			return null;
    		}
    		else{
    			classList.add(w.c);
    		}	
    	}
    	try{
    		update classList;
    		PageReference pref = new PageReference('/'+intakeId);
        	pref.setRedirect(true);
        	return pref;
    	}
    	catch(Exception e){
    		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,e.getMessage()));
    		return null;
    	}	
    }
    //upon clicking of the cancel button, return to the intake record
    public PageReference cancel(){
    	PageReference p = new PageReference('/'+intakeId);
    	p.setredirect(true);
    	return p;
    }
}