/**
 * @description Updates the Inactive field on the Results object
 * @author Janella Lauren Canlas
 * @date 13.NOV.2012
 *
 * HISTORY
 * - 13.NOV.2012    Janella Canlas      Created.
 * - 06.FEB.2013    Janella Canlas      call methods to populate Intake and Qualification Status
 */
global class ScheduleableUpdateResultStatus implements Schedulable{
    global void execute(SchedulableContext SC) {
      UpdateResultStatus.executeUpdate();
      UpdateResultStatus.updateIntakeStatus();
      UpdateResultStatus.updateQualificationStatus();
    }
}