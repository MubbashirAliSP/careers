/**
 * @description Test class for `WebsiteJSONExporterCC` class
 * @author Ranyel Maliwanag
 * @date 28.JUL.2015
 */
@isTest
public with sharing class WebsiteJSONExporterCC_Test {
    @testSetup
    static void setupObjects() {
        Id disclaimerRTID = Schema.SObjectType.Website_Disclaimer__c.getRecordTypeInfosByName().get('Website Disclaimer').getRecordTypeId();

        Website_Disclaimer__c disclaimer = new Website_Disclaimer__c(
                WDO_Disclaimer__c = 'blah',
                WDO_Funding_Type__c = 'type',
                WDO_State__c = 'QLD',
                RecordTypeId = disclaimerRTID
            );
        insert disclaimer;
    }

    static testMethod void testCourseDisclaimer() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'CourseDisclaimer');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testCourses() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'Courses');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testDisclaimerOptions() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'DisclaimerOptions');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testDeliveryModes() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'DeliveryModes');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testDualCourses() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'DualCourses');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testProvider() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'Provider');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testUnits() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'Units');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }

    static testMethod void testWebsiteStreams() {
        Test.setCurrentPage(Page.WebsiteJSONExporter);
        ApexPages.currentPage().getParameters().put('type', 'WebsiteStreams');
        Test.startTest();
            WebsiteJSONExporterCC controller = new WebsiteJSONExporterCC();
        Test.stopTest();
    }
}