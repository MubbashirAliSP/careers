/**
 * @description Custom controller for `EntryRequirements` page
 * @author Ranyel Maliwanag
 * @date 27.JAN.2016
 * @history
 * 		 07.Jun.2016 	Biao Zhang - `Submit` button only submit the single ER which it line up with
 */
public without sharing class EntryRequirementsCC {
	// Reference variable for the Application Form Id that gets passed as a parameter to the page
	public Id formId {get; set;}

	// The actual application form record
	public ApplicationForm__c form {get; set;}

	// List of all entry requirements related to the Application Form
	public List<EntryRequirement__c> entryRequirements {get; set;}
	
	// Mapping of <Entry Requirement Id> : <Attachment record>
	// To keep track of the relationship between Entry Requirements and Attachments
	public Map<Id, Attachment> attachmentMap {get; set;}

	public Boolean isUsiNextVisible {get; set;}

	private Map<String, String> pageParameters;

	//the submitted entry requirement id 
	public String submittedERId {get; set;}

	public Boolean isVFHRendered {
		get {
			if(isVFHRendered == null) {
				isVFHRendered = false;
				if(form.FundingStreamTypeId__r.FundingStreamShortname__c == 'VFH' || form.FundingStreamTypeId__r.FundingStreamShortname__c == null) {	
					isVFHRendered = true;
				}
			}
			return isVFHRendered;
		}
		set;
	}

	/**
	 * @description Default constructor for the custom controller
	 * @author Ranyel Maliwanag
	 * @date 27.JAN.2016
	 */
	public EntryRequirementsCC() {
		pageParameters = ApexPages.currentPage().getParameters();

		// Fetch the formId parameter passed to the page
		if (ApexPages.currentPage().getParameters().containsKey('formId')) {
			formId = ApexPages.currentPage().getParameters().get('formId');
		}

		isUsiNextVisible = false;
		if (pageParameters.containsKey('usinext')) {
			isUsiNextVisible = true;
		}

		// Query the actual form record
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, ', StudentCredentialsId__r.AccountId__c, FundingStreamTypeId__r.FundingStreamShortname__c ');
		queryString += 'WHERE Id = :formId';
		form = Database.query(queryString);		 

		// Build the list of requirements and attachment mappings
		refreshRequirements();

		submittedERId = null;
	}


	/**
	 * @description Command button handler to upload the attachments and relate them to the proper entry requirement
	 * @author Ranyel Maliwanag
	 * @date 27.JAN.2016
	 */
	public PageReference upload() {
		// DML call
		List<Attachment> attachmentsForUpsert = new List<Attachment>();
		for (Attachment record : attachmentMap.values()) {
			if (record.Id == null && record.Body != null && record.Name != null) {
				attachmentsForUpsert.add(record);
			}
		}
		upsert attachmentsForUpsert;

		// @TODO: If the entry requirement has a status of Awaiting Upload and an upload is done, change the status to Awaiting Verification
		List<EntryRequirement__c> requirementsForUpdate = new List<EntryRequirement__c>();
		for (EntryRequirement__c requirement : entryRequirements) {
			if (attachmentMap.containsKey(requirement.Id) && attachmentMap.get(requirement.Id).Id != null) {
				requirement.Status__c = 'Awaiting Verification';
				requirementsForUpdate.add(requirement);
			}
		}
		update requirementsForUpdate;

		// Redirect the page as a whole to overcome large viewstate errors
		PageReference result = Page.EntryRequirements;
		result.getParameters().putAll(pageParameters);
		result.setRedirect(true);
		return result;
	}

	/**
	 * @description Command button handler to upload a single attachment and relate it to its entry requirement
	 * @author Biao Zhang
	 * @date 07.Jun.2016
	 */
	public PageReference uploadSingle() {
		// upsert the attachment 
		Attachment doc = attachmentMap.get(Id.valueOf(submittedERId));
		if (doc.Id == null && doc.Body != null && doc.Name != null) {
			upsert doc;
		}

		// If the entry requirement has a status of Awaiting Upload and an upload is done, change the status to Awaiting Verification
		for (EntryRequirement__c requirement : entryRequirements) {
			if(requirement.Id == Id.valueOf(submittedERId)) {
				if (attachmentMap.containsKey(requirement.Id) && attachmentMap.get(requirement.Id).Id != null) {
					requirement.Status__c = 'Awaiting Verification';
					update requirement;
					break;
				}
			}
		}

		// Redirect the page as a whole to overcome large viewstate errors
		PageReference result = Page.EntryRequirements;
		result.getParameters().putAll(pageParameters);
		result.setRedirect(true);
		return result;
	}

	/**
	 * @description Fetches the related entry requirement records of the current application form and all associated attachments to build reference variables
	 * @author Ranyel Maliwanag
	 * @date 27.JAN.2016
	 */
	private void refreshRequirements() {
		// Query entry requirements with attachments
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c, ', RecordType.Name, (SELECT Id, Name FROM Attachments)');
		queryString += 'WHERE ApplicationFormId__c = :formId';
		entryRequirements = Database.query(queryString);

		// Build mapping of attachments for each entry requirement record
		attachmentMap = new Map<Id, Attachment>();
		for (EntryRequirement__c entryRequirement : entryRequirements) {
			// Creates a blank attachment by default
			Attachment document = new Attachment(
				ParentId = entryRequirement.Id,
				OwnerId = UserInfo.getUserId()
			);

			// If an attachment already exists, reuse the record to prevent creating multiple attachment records for each 
			if (!entryRequirement.Attachments.isEmpty()) {
				document = entryRequirement.Attachments[0];
			}
			//if (entryRequirement.Status__c == 'Awaiting Upload') {
				attachmentMap.put(entryRequirement.Id, document);
			//}
		}
	}

}