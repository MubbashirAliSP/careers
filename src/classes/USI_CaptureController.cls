/**
 * @description Controller for the USI Pages
 * @author Unknown
 * @date Unknown
 * @history
 *       09.SEP.2015    Ranyel Maliwanag    Code rework. Fixes.
 */
public without sharing class USI_CaptureController {
    
    public Boolean ackConsent {get; set;}
    public Boolean agreeTerms {get; set;}
    public String docTypeValue {get; set;}
    public Date verifyDOB {get; set;}
    public String verifySurname {get; set;}
    public String verifyFirstName {get; set;}
    public Account student {get; set;}
    public PortalAccount portalStudent {get; set;}
    
    public USI_Identification_Document__c capturedDocument {get; set;}

    public Boolean isFromApplicationsPortal {get; set;}

    public GuestLogin__c guestLogin {get; set;}
    
    /* Page Return Methods */
    
    public void initTermsPage() {
        
        student = new Account();
    
    }
    
    public Account lookupStudent(String studentId) {
        
        return student = [ SELECT ID, FirstName, LastName, PersonEmail, isPersonAccount, Address_Post_Code__c, Middle_Name__c,
                   PersonBirthdate, Country_of_Birth__c,Country_of_Birth__r.Name , Town_City_Of_Birth__c, Reporting_Billing_State__c, Reporting_Billing_State__r.Name,     
                   Sex__c, Business_Account_Mobile__c, PersonHomePhone, Address_Country__c, Address_Country__r.Name, Address_street_number__c,
                   Address_street_name__c, Unique_Student_Identifier__c, Preferred_Contact_Method__c, PersonMobilePhone,Permission_Granted_to_Apply_For_USI__c,
                   Suburb_locality_or_town__c FROM Account WHERE isPersonAccount = true AND Id = :studentId];
        
    }
    
    public pageReference gotoInitialDetails() {
        
        //String encrypted = ApexPages.currentPage().getParameters().get('data');

        verifyDOB = student.PersonBirthDate; 
             
        //String studentId = USI_EncryptionHandler.decryptStudentId(encrypted);
        String studentId = ApexPages.currentPage().getParameters().get('data');
        
        student = lookupStudent(studentId);
        portalStudent = new PortalAccount(student);
        
        if(verifyDOB == student.PersonBirthdate && verifyFirstName == student.FirstName && verifySurname == student.LastName && ackConsent && agreeTerms) {
            return new pageReference('/apex/USI_InitialDetails');
        }
        
        else {
            if( !ackConsent || !agreeTerms ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You must agree to the terms and give consent to Careers Australia to create a USI on your behalf' ));
            }
            return null;
        }
    }
    
    public pageReference gotoInitialDetailsVFH() {
        
        if(ackConsent && agreeTerms) {
            
            student.Permission_Granted_to_Apply_For_USI__c = true;
            
            update student;
            
            portalStudent = new PortalAccount(student);

            return new pageReference('/apex/USI_InitialDetails');
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You must agree to the terms and give consent to Careers Australia to create a USI on your behalf' ));
            return null;
        }
    }
    
    /**
     * @description Controller initialisation for the USI Pages coming from the Applications Portal
     * @author Unknown
     * @date Unknown
     * @history
     *       09.SEP.2015    Ranyel Maliwanag    Added initialisation for App Portal tracking flag
     */
    public void initialTermsVFHLookup() {
        //String encrypted = ApexPages.currentPage().getParameters().get('data');             
        //String studentId = USI_EncryptionHandler.decryptStudentId(encrypted);
        
        String studentId = ApexPages.currentPage().getParameters().get('data');
        isFromApplicationsPortal = ApexPages.currentPage().getParameters().containsKey('fromPortal');
        student = lookupStudent(studentId);    
        portalStudent = new PortalAccount(student);

        if (ApexPages.currentPage().getCookies().containsKey('authenticationId')) {
            String authenticationId = ApexPages.currentPage().getCookies().get('authenticationId').getValue();
            // Get GuestLogin record based on Authentication Id
            String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c, ', RecordType.Name');
            queryString += 'WHERE Id = :authenticationId';
            try {
                guestLogin = Database.query(queryString);
            } catch (Exception e) {
                // Silent exception
            }
        }
    }
    
    public void reValidationLookup() {
                
        //String encrypted = ApexPages.currentPage().getParameters().get('data');
        //String studentId = USI_EncryptionHandler.decryptStudentId(encrypted);
        
        String studentId = ApexPages.currentPage().getParameters().get('data');
        
        student = lookupStudent(studentId);       
        portalStudent = new PortalAccount(student);
    }
    
    public pageReference gotoTermsPage() {
        PageReference result = Page.USI_TermsPage;
        if (isFromApplicationsPortal != null && isFromApplicationsPortal) {
            result = Page.USI_TermsPageVFH;
        }
        return result;
   
    }
    
    public pageReference gotoConfirmDetails() {
        
        try{
            update student;
            
            student = lookupStudent(student.Id);
            
            return new pageReference('/apex/USI_ConfirmDetails');
        }
        catch (exception e) {
            return null;
        }
                
    }
    
    public pageReference gotoConfirmReValidation() {
                
        return new pageReference('/apex/USI_ReValidationConfirm');
   
    }
    
    public pageReference gotoSaveReValidation() {
        
        try {
            student.Middle_Name__c = portalStudent.middleName;
            student.Town_City_Of_Birth__c = portalStudent.cityOfBirth;
            student.Sex__c = portalStudent.gender;
            student.PersonEmail = portalStudent.email;
            student.PersonMobilePhone = portalStudent.mobile;
            student.PersonHomePhone = portalStudent.homePhone;
            student.Country_of_Birth__c = portalStudent.countryOfBirth;
            student.Address_Country__c = portalStudent.countryOfResidence;
            student.Address_street_number__c = portalStudent.streetNumber;
            student.Address_street_name__c = portalStudent.streetName;
            student.Suburb_locality_or_town__c = portalStudent.suburb;
            student.Reporting_Billing_State__c = portalStudent.state;
            student.Address_Post_Code__c = portalStudent.postCode;
            student.Preferred_Contact_Method__c = portalStudent.contactMethod;

            update student;
            return new pageReference('/apex/USI_ReValidationSave');        
        }
        
        catch (exception e) {            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage() ));
            System.debug('exception ::: ' + e.getMessage());
            return null;
        }
   
    }
    
    public pageReference gotoDocumentCapture() {
        
        capturedDocument = new USI_Identification_Document__c();
        return new pageReference('/apex/USI_DocumentCapture');
    
    }
    
    public pageReference gotoUSIComplete() {
                
        capturedDocument.Account__c = student.Id;
        capturedDocument.RecordTypeId = docTypeValue;
        
        try {
            insert capturedDocument;
            return new pageReference('/apex/USI_Complete');
        } catch (exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage() ));
            return null;
        }
        
    }
            
    /* Select List Methods */    
    
    public List<SelectOption> getCountries() {
        List<SelectOption> options = new List<SelectOption>();
        
        Country__c australia = [ SELECT ID, Name FROM Country__C WHERE Name = 'Australia' LIMIT 1];
        
        options.add(new SelectOption(australia.Id, australia.Name));
        
        List<Country__c> countries = [ SELECT ID, Name from Country__c ORDER BY Name ASC];
		        
        for(Country__c c : countries) {
            if(c.Name != 'Australia') {
                options.add(new SelectOption(c.Id, c.Name));
            }
        }
        
        return options; 
    }
    
    public List<SelectOption> getStates() {
        
        List<SelectOption> options = new List<SelectOption>();
        
         for(State__c s : [SELECT Id, Name FROM State__c ORDER BY Name ASC]) {
            options.add(new SelectOption(s.Id, s.Name));
        }
        
        return options; 
    
    
    }
    
    public List<SelectOption> getGender() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('M - Male', 'M - Male'));
            options.add(new SelectOption('F - Female', 'F - Female'));             
        return options; 
    }
    
    public List<SelectOption> getContactMethods() { //replace with metadata
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Home', 'Home'));
            options.add(new SelectOption('Mobile', 'Mobile'));     
            options.add(new SelectOption('Email', 'Email'));     
            options.add(new SelectOption('Post', 'Post'));  
            options.add(new SelectOption('Fax', 'Fax'));  
            options.add(new SelectOption('In Person', 'In Person'));          
        return options; 
    }
    
    public List<SelectOption> getCapturedDocumentState() {
        Schema.DescribeFieldResult usiIdentState = USI_Identification_Document__c.State__c.getDescribe();
        List<Schema.PicklistEntry> usiIdentStateValues = usiIdentState.getPicklistValues();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.PicklistEntry pl :usiIdentStateValues) {
            options.add(new SelectOption( pl.getValue(), pl.getLabel() ));
        }
        
        return options;
        
    }
        
    public List<SelectOption> getDocType() {
        
        List<SelectOption> options = new List<SelectOption>();
                    
        options.add(new SelectOption('---Please Select---', '---Please Select---'));
        
        List<RecordType> rtype = new List<RecordType>([ SELECT Id, Name FROM RecordType WHERE sObjectType = 'USI_Identification_Document__c' ]);

        for(RecordType r :rtype) {
            options.add(new SelectOption(r.Id, r.Name));
        }
        
        return options; 
    }
        
    /* ReRender Methods */
    
    public PageReference rerender() {
        return null;
    }


    public class PortalAccount {
        public Account accountRecord {get; set;}
        public String middleName {get; set;}
        public String cityOfBirth {get; set;}
        public String gender {get; set;}
        public String email {get; set;}
        public String mobile {get; set;}
        public String homePhone {get; set;}
        public String countryOfBirth {get; set;}
        public String countryOfResidence {get; set;}
        public String streetNumber {get; set;}
        public String streetName {get; set;}
        public String suburb {get; set;}
        public String state {get; set;}
        public String postCode {get; set;}
        public String contactMethod {get; set;}

        public PortalAccount(Account record) {
            accountRecord = record;

            middleName = record.Middle_Name__c;
            cityOfBirth = record.Town_City_Of_Birth__c;
            gender = record.Sex__c;
            email = record.PersonEmail;
            mobile = record.PersonMobilePhone;
            homePhone = record.PersonHomePhone;
            countryOfBirth = record.Country_of_Birth__c;
            countryOfResidence = record.Address_Country__c;
            streetNumber = record.Address_street_number__c;
            streetName = record.Address_street_name__c;
            suburb = record.Suburb_locality_or_town__c;
            state = record.Reporting_Billing_State__c;
            postCode = record.Address_Post_Code__c;
            contactMethod = record.Preferred_Contact_Method__c;
        }
    }
}