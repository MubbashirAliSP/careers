/*JOB2a Staging table loader job
 *This class is resposible for loading creating and loading 
 *Blackboard Course Memberships
 *Course memberships are created from initial Enrolment Intake Units, however
 *before we can create course memberships we need to create Blackboard Users and
 *Blackboard User Role for each BBCM created if there is no Blackboard User and Blackboard User Role
 *previously created.
 *This job is designed for T2(template course) type courses
 *Please Refer to ERD for further details about relationships
 *Created By Yuri Gribanov - CloudSherpas 15th July 2014
 *BATCH SIZE LIMIT 1 ONLY
 */

global with sharing class BlackboardScheduleUnitJ2A implements Database.Batchable<sObject> {

    
    global String query;
    
    global BlackboardScheduleUnitJ2A() {
    /*Initial query to be passed into query locator performed against Enrolment Intake Units*/
        query = 'Select Id,Enrolment_Unit__r.Unit__c,Enrolment_Unit__r.Enrolment__r.Qualification__c,Intake_Unit__r.Blackboard_Template_Course__c,Enrolment_Unit__r.Enrolment__r.Student__c From Enrolment_Intake_Unit__c Where Intake_Unit__r.Blackboard_Template_Course__c != null And Blackboard_Course_Membership_Created__c = false And Enrolment_Unit__r.Enrolment__r.Blackboard_Required__c = true';
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Blackboard_Course_Membership__c> bbcmList = new List<Blackboard_Course_Membership__c>();
        List<Blackboard_User__c> bbuserList = new List<Blackboard_User__c>();
        List<Blackboard_User_Role__c>bbuserroleList = new List<Blackboard_User_Role__c>();
        Set<Id>accIds = new Set<Id>();
        Map<Id,Id>userRolesInDBMap = new Map<Id,Id>();
        Set<Id>t2Ids = new Set<Id>();
        Map<Id,Id>t2t3Map = new Map<Id,Id>();
        List<Enrolment_Intake_Unit__c> EnrolmentIntakeUnitsToUpdateList = new List<Enrolment_Intake_Unit__c>();
        Id BBCId, StudentId, EIUId, QualId,UnitId;
        
        //Recordtypes for BBCM
        List<RecordType>bbcmRecTypes = New List<RecordType>([Select Id,DeveloperName From RecordType Where DeveloperName = 'Student_T3']);
        //RecordTypes for User And User Role
        List<RecordType>userAnduserRoleRecTypes = new List<RecordType>([Select Id, DeveloperName, SObjectType From RecordType Where DeveloperName = 'Student' And (SObjectType = 'Blackboard_User__c' OR SObjectType = 'Blackboard_User_Role__c')  ORDER BY SObjectType Desc]);
        
        List<Blackboard_Role__c> bbRole = new List<Blackboard_Role__c>([Select Id From Blackboard_Role__c Where Blackboard_Role_Name__c = 'Student' AND RecordType.DeveloperName = 'Course_Role']);
      
      
     
      

        /*ALWAYS ONLY 1 RECORD IN SCOPE */
        for(SObject s :scope) {

             Enrolment_Intake_Unit__c eiu = (Enrolment_Intake_Unit__c) s;
             
             //Add student Ids
             accIds.add(eiu.Enrolment_Unit__r.Enrolment__r.Student__c);
             //Collect Template Course Ids
             t2Ids.add(eiu.Intake_Unit__r.Blackboard_Template_Course__c);

             BBCId = eiu.Intake_Unit__r.Blackboard_Template_Course__c;
             StudentId = eiu.Enrolment_Unit__r.Enrolment__r.Student__c;
             EIUId = eiu.Id;
             QualId = eiu.Enrolment_Unit__r.Enrolment__r.Qualification__c;
             UnitId = eiu.Enrolment_Unit__r.Unit__c;


             //If Blackboard User Doesn't Exist - Create
             if([Select Id From Blackboard_User__c Where Account__c = : eiu.Enrolment_Unit__r.Enrolment__r.Student__c].size() == 0) {

                Blackboard_User__c bbuser = new Blackboard_User__c(Account__c = eiu.Enrolment_Unit__r.Enrolment__r.Student__c,
                                                                   Enable_Blackboard_User__c = true,
                                                                   RecordTypeId = userAnduserRoleRecTypes.get(0).Id);

                bbuserList.add(bbuser);

             }
             
             

             
        }


        //Insert new Blackboard Users Here
        insert bbuserList;

        if(bbuserList.size() > 0) {

          /*Create New Blackboard User Roles here for just created new users */
          for(Blackboard_User__c bbuser : bbuserList) {

            Blackboard_User_Role__c bbuserrole = new Blackboard_User_Role__c(RecordTypeId = userAnduserRoleRecTypes.get(1).Id,
                                                                             Blackboard_User__c = bbuser.Id,
                                                                             Blackboard_Role__c = bbRole.get(0).Id);

                bbuserroleList.add(bbuserrole);
          }
          //Insert New Blackboard User Roles here
          insert bbuserroleList;

        }


        for(Blackboard_User_Role__c bur : [Select Id,Blackboard_User__r.Account__c From Blackboard_User_Role__c Where Blackboard_User__r.Account__c in : accIds LIMIT 1])
              userRolesInDBMap.put(bur.Blackboard_User__r.Account__c, bur.Id);

      

        for(Blackboard_Course__c bbcourse : [Select id,Template_Course__c From Blackboard_Course__c Where Template_Course__c in : t2Ids ])
            t2t3Map.put(bbcourse.Template_Course__c, bbcourse.Id);

      

        //requery enrolment intake units from DB, this will give us updated formula fields and relationships

        /*There is a case where BBCM will not be created, so we must perform
         *a check by quering BBCM object and finding if there is a BBCM record
         *that already relates to same BBUR, EIU and BBC. If record exist
         *we set its EIU and DO NOT CREATE NEW BBCM
         */


        List<Blackboard_Course_Membership__c> bbcminDB = new List<Blackboard_Course_Membership__c>([Select Enrolment_Intake_Unit__c
                                                                                                    From Blackboard_Course_Membership__c
                                                                                                    WHERE Blackboard_Course__c = : t2t3Map.get(BBCId) AND 
                                                                                                   Blackboard_User_Role__c = :  userRolesInDBMap.get(StudentId)  LIMIT 1]);

        if(bbcminDB.size() > 0) {

            bbcminDB.get(0).Enrolment_Intake_Unit__c = EIUId;

            update bbcminDB;

          } else {

              for(Enrolment_Intake_Unit__c eiu : [Select Id,Intake_Unit__c,Enrolment_Unit__c,Intake_Unit__r.Blackboard_Template_Course__c,Enrolment_Unit__r.Enrolment__r.Student__c From Enrolment_Intake_Unit__c Where Id = : EIUId ]) {

                //Attempt to retrieve sequence number
                Decimal sequence_number;
                List<Qualification_Unit__c> qualunitsList = new List<Qualification_Unit__c>([Select Sequence_Number__c From Qualification_Unit__c Where Qualification__c = : QualId AND Unit__c = : UnitId LIMIT 1]);

                if(qualunitsList.size() > 0)
                  sequence_number = qualunitsList.get(0).Sequence_Number__c;

              //construct new BBCM here
                  Blackboard_Course_Membership__c bbcm = new Blackboard_Course_Membership__c(

                      Enrolment_Intake_Unit__c = eiu.Id,
                      Blackboard_User_Role__c = userRolesInDBMap.get(eiu.Enrolment_Unit__r.Enrolment__r.Student__c),
                      Blackboard_Course__c = t2t3Map.get(eiu.Intake_Unit__r.Blackboard_Template_Course__c),
                      Intake_Unit__c = eiu.Intake_Unit__c,
                      Enrolment_Unit__c = eiu.Enrolment_Unit__c,
                      Sequence_Number__c = sequence_number,
                      RecordTypeId = bbcmRecTypes.get(0).Id

                  );

                  bbcmList.add(bbcm);
                  
                  //set the flag to true for processed Enrolment intake unit
                  eiu.Blackboard_Course_Membership_Created__c = true;

                  EnrolmentIntakeUnitsToUpdateList.add(eiu);

              }

              /*We're inserting new blackboard course membership records here
              */
              insert bbcmList;

              /*We need to update Enrolment Intake Units to make sure, we don't pick up
               *the same on the next run. Flag is set in code above

              */
              update EnrolmentIntakeUnitsToUpdateList;

            }
          
        
           }
    
    global void finish(Database.BatchableContext BC) {
        
      BlackboardIntakeStudentJ2B b1 = new BlackboardIntakeStudentJ2B();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }
    
}