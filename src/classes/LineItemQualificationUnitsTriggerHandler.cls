/**
 * @description Line Item Qualification Unit Trigger Handler
 * @author Janella Lauren Canlas
 * @date 27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas - Created.
 */
public with sharing class LineItemQualificationUnitsTriggerHandler {
	/*
		This method will populate the LIQU recordtype based on the related Line Item Qualification recordtype
	*/
	public static void populateRecordType(List<Line_Item_Qualification_Units__c> liquList){
		Set<Id> liqIds = new Set<Id>();
		Map<String,Id> rtMap = new Map<String,Id>();
		
		List<RecordType> rtList = new List<RecordType>();
		List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
		
		//collect line item qualification ids
		for(Line_Item_Qualification_Units__c l : liquList){
			liqIds.add(l.Line_Item_Qualification__c);
		}
		//query recordtypes for the object line item qualification units
		rtList = [SELECT Id, Name, SObjectType from RecordType Where Name IN('AHC Funded','Unit Funded') AND SobjectType='Line_Item_Qualification_Units__c'];
		//create map of recordtype name with corresponding Id
		for(RecordType r : rtList){
			if(!rtMap.containsKey(r.Name)){
				rtMap.put(r.Name,r.Id);
			}
		}
		//query related line item qualifications
		liqList = [SELECT id, Contract_Line_Item__r.Purchasing_Contract__r.RecordType.Name FROM Line_Item_Qualifications__c WHERE Id IN: liqIds];
		//populate line item qualification unit recordtype for update
		for(Line_Item_Qualification_Units__c l : liquList){
			for(Line_Item_Qualifications__c q : liqList){
				if(l.Line_Item_Qualification__c == q.Id){
					l.RecordTypeId = rtMap.get(q.Contract_Line_Item__r.Purchasing_Contract__r.RecordType.Name);
				}
			}
		}
	}
	/*
		This method will populate Unit Value on related Enrolment Units upon update
	*/
	public static void populateUnitValue(Set<Id> liquIds, List<Line_Item_Qualification_Units__c> liquList){
		List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
		euList = [select Id, Unit_Value__c, Line_Item_Qualification_Unit__c from Enrolment_Unit__c where Line_Item_Qualification_Unit__c IN: liquIds];
		for(Line_Item_Qualification_Units__c l : liquList){
			for(Enrolment_Unit__c e : euList){
				if(l.Id == e.Line_Item_Qualification_Unit__c){
					e.Unit_Value__c = l.Unit_Value__c;
				}
			}
		}
		try{
			update euList;}catch(Exception e){
		}
	}
}