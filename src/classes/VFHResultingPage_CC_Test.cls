/**
 * @description Test Class for UnitHoursPointsTriggerHandler 
 * @author Cletz Cadiz
 * @date 31.JAN.2013
 *
 */
@isTest
private class VFHResultingPage_CC_Test {

    static testMethod void testResultPage() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Test_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry(); 
            //countryAU = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id); 
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id); 
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            //enrl.Overseas_Country__c = null;
            enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            enrl.Start_Date__c = Date.TODAY() - 50;
            enrl.End_Date__c = Date.TODAY() + 50;
            insert enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
            
            PageReference testPage = Page.VFHResultingPage;
            testPage.getParameters().put('enrolmentId', enrl.Id);
            Test.setCurrentPage(testPage);
            
            VFHResultingPage_CC vfhRPage = new VFHResultingPage_CC();
            vfhRPage.enrolmentUnitId = eu.Id;
            vfhRPage.getEUPicklist();
            vfhRPage.fillEUFields();
            vfhRPage.refreshResults();
            vfhRPage.geteuosList();
            vfhRPage.geteuosResultPicklist();
            vfhRPage.geteuosVarReasonPicklist();
            vfhRPage.cancel();
            vfhRPage.save();
            test.stopTest();
            
        }
    }
}