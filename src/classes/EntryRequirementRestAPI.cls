/**
 * @description API class for updating LLN Information on Entry Requirements
 * @author Ranyel Maliwanag
 * @date 22.JUN.2016
 */
@RestResource(urlMapping='/EntryRequirement/*')
global with sharing class EntryRequirementRestAPI {
    /**
     * @description POST method handler for updating ER
     * @author Ranyel Maliwanag
     * @date 22.JUN.2016
     */
    @HttpPost
    global static String doPost(String erId, String ipAddress, String mathsExitLevel, String mathsDate, String englishExitLevel, String englishDate) {
        String result = '';
        Datetime englishDateGMT = Datetime.valueOf(englishDate);
        Datetime mathDateGMT = Datetime.valueOf(mathsDate);
        EntryRequirement__c requirement = new EntryRequirement__c(
            Id = erId,
            IPAddress__c = ipAddress,
            Literacy_Level__c = englishExitLevel,
            Numeracy_Level__c = mathsExitLevel,
            English_End_Date__c = Datetime.valueOf(englishDate),
            Maths_End_Date__c = Datetime.valueOf(mathsDate)
        );
        try {
            update requirement;
            result = 'Success';
        } catch (Exception e) {
            result = e.getMessage();
        }
        return result;
    }
}