global class NATGeneratorBatch00030_2 implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global String body30;
    global String currentState;
    global String logId;
    global String qualIds;
    Set<Id> quals = new Set<Id>();
    
    public class applicationException extends Exception {}

    global NATGeneratorBatch00030_2(String state, Set<Id> qualSet, String LId) {
        currentState = state;
                
        qualIds = '';
        
        for (String s : qualSet) {
            if(s != null) {
                qualIds += '\'' + s + '\',';
            }
        }
        
        qualIds = qualIds.lastIndexOf(',') > 0 ? '(' + qualIds.substring(0,qualIds.lastIndexOf(',')) + ')' : qualIds;

       //JEA  18.SEPT.2015  Added Reportable Qualification Hours for ACT, NT, and TAS
        query = 'SELECT Id, Name, Qualification_Name__c, Recognition_Status_Identifier__c, Qualification_Category_Identifier__c, ' +
                'Field_Of_Education_Identifier__c, ANZSCO_Identifier__c, Vet_Non_Vet__c, NAT_Error_Code__c, ' +
                'QLD_Reportable_Qualification_Hours__c, VIC_Reportable_Qualification_Hours__c,SA_Reportable_Qualification_Hours__c, ' +
                'ACT_Reportable_Qualification_Hours__c, NT_Reportable_Qualification_Hours__c, TAS_Reportable_Qualification_Hours__c ,' + 
                'WA_Reportable_Qualification_Hours__c, NSW_Reportable_Qualification_Hours__c, Parent_Qualification_Code__c, Parent_Qualification_Name__c ' +
                'FROM Qualification__c WHERE Id in ' + qualIds; 
        
       
        
        //throw new applicationException(qualIds);

        
        logId = LId;
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        body30 = '';
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        if (currentState == 'Queensland') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionQLD.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if (currentState == 'New South Wales') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionNSW.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if (currentState == 'New South Wales APL') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionNSW.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if (currentState == 'Australian Capital Territory') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionACT.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if(currentState == 'Victoria') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionVIC.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if (currentState == 'South Australia') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionSA.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if (currentState == 'Western Australia') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionWA.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if(currentState == 'Northern Territory') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionNT.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        else if(currentState == 'Tasmania') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinitionTAS.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
        if (currentState == 'National') {
            for (SObject s :scope) {
                Qualification__c q = (Qualification__c) s;
                body30 += NATConstructor.natFile(q, NATDataDefinition.nat00030(), currentState);
                body30 += '\r\n';
            }
        }
    } 
       
    global void finish(Database.BatchableContext BC) {
        
        NAT_Validation_Log__c natLog = [ SELECT Id, Content_Library_Id__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];

        if(body30 == '') {
            body30 = ' ';
        }
        
        String fbody30 = NATGeneratorUtility.RemoveDuplicates(body30);
        System.debug(body30);

        Blob pBlob30 = Blob.valueof(fbody30);
        insert new ContentVersion(
            versionData =  pBlob30,
            Title = 'NAT00030',
            PathOnClient = '/NAT00030.txt',
            FirstPublishLocationId = natLog.Content_Library_Id__c
        );
    }
}