/**
 * @description Page Controller for Change Purchasing Contract Utility
 * @author Janella Lauren Canlas
 * @date 19.DEC.2012
 *
 * HISTORY
 * - 19.DEC.2012    Janella Canlas - Created.
 */
public with sharing class ChangePurchasingContractPage1_CC {
    //declare variables
    public Id contractId;
    public String oldliqId {get;set;}
    public String oldConName {get;set;}
    public Map<String,Enrolment_Unit__c> oldConMap = new Map<String,Enrolment_Unit__c>();
    public String resultCode {get;set;}
    public Boolean page2SelectAll {get;set;}
    public List<page1Wrapper> page1WrapperList {get;set;}
    public List<page2Wrapper> page2WrapperList {get;set;}
    public List<page3Wrapper> page3WrapperList {get;set;}
    public List<page4Wrapper> page4WrapperList {get;set;}
    //page 3 filters
    public String codeFilter {get;set;}
    public String fsCodeFilter {get;set;}
    public String trainingOrg {get;set;}
    public String newLiqId {get;set;}
    public String newConName {get;set;}
    //undo record Id
    public String undoRecId {get;set;}
    public String oldLiquId {get;set;}
    
    public List<Enrolment_Unit__c> euUpdateList {get;set;}
    
    public ChangePurchasingContractPage1_CC(){
        //retrieves the contract ID passed from the button
        contractId = ApexPages.currentPage().getParameters().get('contractId');
        fillPage1Wrapper();
    }
    //retrieves current contract record
    public Purchasing_Contracts__c getContractRecord(){
        Purchasing_Contracts__c tempContract = new Purchasing_Contracts__c();
        tempContract = [select Contract_Code__c, Contract_Name__c, Contract_Manager__c, Client__c, 
                        Client__r.Name, Fund_Source_Code__c, Contract_State__c, Contract_Manager__r.Name
                        from Purchasing_Contracts__c
                        where Id =: contractId
                    ];
        return tempContract;
    }
    //retrieves old contract details
    public Line_Item_Qualifications__c getOldContract(){
        Line_Item_Qualifications__c temp = new Line_Item_Qualifications__c();
        temp = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Id=:oldliqId
                ];
        return temp;
    }
    //list of valid contracts depending on the filters entered by user
    public List<Line_Item_Qualifications__c> getContractList(){
        String fsValue = '';
        List<Line_Item_Qualifications__c> tempContractList = new List<Line_Item_Qualifications__c>();
        String conCodeSearchText = '%'+codeFilter+'%';
        String fsCodeSearchText = '%'+fsCodeFilter+'%';
        if(fsCodeFilter != null){
            fsValue = fsCodeFilter;
        }
        
        if(codeFilter != null && fsValue=='' && trainingOrg == null){
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            ];
        }
        else if(!fsValue.contains(' AND ') && trainingOrg == null){
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            AND Contract_Line_Item__r.Purchasing_Contract__r.Fund_Source_Code__c =: fsCodeFilter
                            ];
        }
        else if(fsValue.contains(' AND ') && trainingOrg == null){
            String[] fsCodeSplit = fsCodeFilter.split(' AND ');
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            AND Contract_Line_Item__r.Purchasing_Contract__r.Fund_Source_Code__c IN: fsCodeSplit
                            ];
        }
        else if(codeFilter != null && fsValue=='' && trainingOrg != null){
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            and Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c =: trainingOrg
                            ];
        }
        else if(!fsValue.contains(' AND ') && trainingOrg != null){
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            AND Contract_Line_Item__r.Purchasing_Contract__r.Fund_Source_Code__c =: fsCodeFilter
                            and Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c =: trainingOrg
                            ];
        }
        else if(fsValue.contains(' AND ') && trainingOrg != null){
            String[] fsCodeSplit = fsCodeFilter.split(' AND ');
            tempContractList = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Contract_Name__c,
                            Contract_Line_Item__r.Purchasing_Contract__r.Name,
                            Contract_Line_Item__r.Purchasing_Contract__r.Active__c
                            from Line_Item_Qualifications__c
                            where Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c LIKE: conCodeSearchText
                            AND Contract_Line_Item__r.Purchasing_Contract__r.Fund_Source_Code__c IN: fsCodeSplit
                            and Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c =: trainingOrg
                            ];
        }
        if(codeFilter == '' && fsValue=='' && trainingOrg == null){
            tempContractList = new List<Line_Item_Qualifications__c>();
        }
        system.debug('*****codeFilter: ' + codeFilter);
        system.debug('*****conCodeSearchText: ' + conCodeSearchText);
        system.debug('*****tempContractList: ' + tempContractList);
        return tempContractList;
    }
    //retrieve list of line item qualifications to update
    public List<Line_Item_Qualifications__c> getliqList(){
        List<Line_Item_Qualifications__c> tempCLI = new List<Line_Item_Qualifications__c>();
        tempCLI = [select id, Name, Qualification_Name__c, Qualification__r.Name, Commencement_Date__c, Completion_Date__c
                    from Line_Item_Qualifications__c where Contract_Line_Item__r.Purchasing_Contract__c =: contractId];
        return tempCLI;
    }
    //code filling up page 1 table
    public void fillPage1Wrapper(){
        page1WrapperList = new List<page1Wrapper>();
        List<Line_Item_Qualifications__c> liq = new List<Line_Item_Qualifications__c>();
        liq = getliqList();
        for(Line_Item_Qualifications__c l : liq){
            page1Wrapper w = new page1Wrapper(l);
            page1WrapperList.add(w);
        }
    }
    //code filling up page 2 table
    public void fillPage2Wrapper(){
        page2WrapperList = new List<page2Wrapper>();
        List<Enrolment_Unit__c> euList = new List<Enrolment_Unit__c>();
        euList = getEnrolmentUnits();
        for(Enrolment_Unit__c e : euList){
            oldConMap.put(e.Id, e);
            Boolean chk = false;
            if(page2SelectAll == true){
                chk = true;
            }
            page2Wrapper w = new page2Wrapper(e, page2SelectAll);
            page2WrapperList.add(w);
        }
    }
    //code filling up page 3 table
    public void fillPage3Wrapper(){
        page3WrapperList = new List<page3Wrapper>();
        List<Line_Item_Qualifications__c> pcList = new List<Line_Item_Qualifications__c>();
        pcList = getContractList();
        for(Line_Item_Qualifications__c p : pcList){
            page3Wrapper w;
            if(p.Contract_Line_Item__r.Purchasing_Contract__r.Active__c == true){
                w = new page3Wrapper(p, 'No');
            }
            else{
                w = new page3Wrapper(p, 'Yes');
            }
            
            page3WrapperList.add(w);
        }
    }
    //code filling up page 4 table
    public void fillPage4Wrapper(){
        page4WrapperList = new List<page4Wrapper>();
        Line_Item_Qualifications__c oldCon = getOldContract();
        List<Enrolment_Unit__c> tempList = new List<Enrolment_Unit__c>(); 
        tempList = getUpdatedEuList();
        String oldConName = oldCon.Contract_Line_Item__r.Purchasing_Contract__r.Contract_Code__c;
        String oldLIQU = '';
        String oldId = '';
        for(Enrolment_Unit__c eu : tempList){
            if(oldConMap.get(eu.Id) != null){
                Enrolment_Unit__c temp = oldConMap.get(eu.Id);
                oldLIQU = temp.Line_Item_Qualification_Unit__r.Name;
                oldId = temp.Line_Item_Qualification_Unit__r.Id;
                system.debug('oldId***' + oldId);
                system.debug('oldLIQU***' + oldLIQU);
                page4WrapperList.add(new page4Wrapper(eu,oldConName,oldLIQU,oldId));
            }
        }
    }
    //retrieve updated enrolment unit records
    public List<Enrolment_Unit__c> getUpdatedEuList(){
        List<Enrolment_Unit__c> tempList = new List<Enrolment_Unit__c>();
        Set<Id> euIds = new Set<Id>();
        system.debug('euUpdateList**** '+euUpdateList);
        for(Enrolment_Unit__c e  : euUpdateList){
            euIds.add(e.Id);
        }
        tempList = [select Id, Enrolment__r.Student__r.Name,End_Date__c, Enrolment__r.Student__r.Student_Identifer__c, Student_Identifier__c, Enrolment__c, Enrolment__r.Name, Name, 
                        Unit_Name__c,Unit__r.Name, Unit_Result__r.Result_Code__c, Purchasing_Contract_Identifier__c, Line_Item_Qualification_Unit__r.Name, Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.Name 
                     FROM Enrolment_Unit__c
                     WHERE Id IN: euIds];
        return tempList;
    }
    //retrieve list of enrolment units depending on codes inputted by user
    public List<Enrolment_Unit__c> getEnrolmentUnits(){
        List<Enrolment_Unit__c> tempList = new List<Enrolment_Unit__c>();
        if(resultCode=='' || resultCode == null){
            tempList = [select Id, Enrolment__r.Student__r.Name,End_Date__c, Student_Identifier__c, Enrolment__c, Enrolment__r.Name, Name, 
                        Unit_Name__c,Unit__r.Name, Unit_Result__r.Result_Code__c, Purchasing_Contract_Identifier__c,
                        Line_Item_Qualification_Unit__r.Name,Line_Item_Qualification_Unit__c , Line_Item_Qualification_Unit__r.Id
                     FROM Enrolment_Unit__c
                     WHERE Line_Item_Qualification_Unit__r.Line_Item_Qualification__c =: oldliqId];
        }else if(!resultCode.contains('AND')){
            tempList = [select Id, Enrolment__r.Student__r.Name,End_Date__c, Student_Identifier__c, Enrolment__c, Enrolment__r.Name, Name, 
                        Unit_Name__c,Unit__r.Name, Unit_Result__r.Result_Code__c, Purchasing_Contract_Identifier__c,
                        Line_Item_Qualification_Unit__r.Name,Line_Item_Qualification_Unit__c, Line_Item_Qualification_Unit__r.Id
                     FROM Enrolment_Unit__c
                     WHERE Line_Item_Qualification_Unit__r.Line_Item_Qualification__c =: oldliqId
                     and Unit_Result__r.Result_Code__c =: resultCode
                     ];
        }else if(resultCode.contains('AND')){
            String[] results = resultCode.split(' AND ');
            tempList = [select Id, Enrolment__r.Student__r.Name,End_Date__c, Student_Identifier__c, Enrolment__c, Enrolment__r.Name, Name, 
                        Unit_Name__c,Unit__r.Name, Unit_Result__r.Result_Code__c, Purchasing_Contract_Identifier__c,
                        Line_Item_Qualification_Unit__r.Name,Line_Item_Qualification_Unit__c, Line_Item_Qualification_Unit__r.Id
                         FROM Enrolment_Unit__c
                         WHERE Line_Item_Qualification_Unit__r.Line_Item_Qualification__c =: oldliqId
                         and Unit_Result__r.Result_Code__c IN: results
                     ];
            system.debug('RESULTS****' + results);
        }
        
        system.debug('oldConMap***' + oldConMap);
        return tempList;
    }
    //cancel method
    public PageReference cancel(){
        PageReference p = new PageReference('/'+contractId);
        p.setredirect(true);
        return p;
    }
    //redirect to page 2
    public PageReference nextToPage2(){
        if(oldliqId == ''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please select a Qualification.'));
            return null;
        }else{
            fillPage2Wrapper();
            PageReference p = new PageReference('/apex/ChangePurchasingContractPage2');
            p.setredirect(false);
            return p;   
        }
        
    }
    //redirect to page 3
    public PageReference nextToPage3(){
        boolean checker = false;
        for(page2Wrapper w : page2WrapperList){
            if(w.check == true){
                checker = true;
            }
        }
        if(checker == true){
            fillPage3Wrapper();
            PageReference p = new PageReference('/apex/ChangePurchasingContractPage3');
            p.setredirect(false);
            return p;   
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please select Enrolment Unit(s).'));
            return null;
        }
        
    }
    //redirect back to page 1
    public PageReference backToPage1(){
        fillPage1Wrapper();
        PageReference p = new PageReference('/apex/ChangePurchasingContractPage1?contractId='+contractId);
        p.setredirect(true);
        return p;
    }
    //redirect back to page 2
    public PageReference backToPage2(){
        fillPage2Wrapper();
        PageReference p = new PageReference('/apex/ChangePurchasingContractPage2');
        p.setredirect(false);
        return p;
    }
    //change purchasing contract on chosen enrolment units
    public PageReference submitChanges(){
        system.debug('page4WrapperList****' + page4WrapperList);
        PageReference pr = new PageReference('/apex/ChangePurchasingContractPage4');
        pr.setredirect(false);
        euUpdateList = new List<Enrolment_Unit__c>();
        //create a list of LIQU's related to the new LIQ ID
        List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();
        Map<String,String> liqUnitMap = new Map<String,String>();
        liquList = [SELECT Line_Item_Qualification__c, Qualification_Name__c, Qualification_Unit__c, Qualification_Code__c, 
                    Unit_Code__c, Unit_Cost__c, Unit_Name__c, Id 
                    FROM Line_Item_Qualification_Units__c
                    WHERE Line_Item_Qualification__c =: newLiqId];
        //create new list for selected enrolment units
        for(page2Wrapper p : page2WrapperList){
            if(p.check == true){
                euUpdateList.add(p.euRecord);
            }
        } 
        //create map with Unit name and LIQU Id
        for(Line_Item_Qualification_Units__c l : liquList){
            if(!liqUnitMap.containsKey(l.Unit_Code__c)){
                liqUnitMap.put(l.Unit_Code__c, l.Id);
            }
        }
        
        //loop selected enrolment units
        for(Enrolment_Unit__c e : euUpdateList){
            //get liqu id based on unit code
            String liqu = '';
            if(liqUnitMap.get(e.Unit__r.Name) != null){
                liqu = liqUnitMap.get(e.Unit__r.Name);
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'The Qualification you have chosen is invalid.'));
                return null;
            }
            if(liqu != ''){
                e.Line_Item_Qualification_Unit__c = liqu;
            }
        }
        try{
            update euUpdateList;
            fillPage4Wrapper();
            return pr;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: ' + e.getMessage()));return null;}
    }
    //undo enrolment units update one by one
    public PageReference undo(){
        PageReference p = new PageReference('/apex/ChangePurchasingContractPage4');
        p.setredirect(false);
        Enrolment_Unit__c tempEU = new Enrolment_Unit__c();
        system.debug('undoRecId****'+undoRecId);
        system.debug('oldLiquId****'+oldLiquId);
        tempEU = [Select Line_Item_Qualification_Unit__c from Enrolment_Unit__c where Id=: undoRecId];
        tempEU.Line_Item_Qualification_Unit__c = oldLiquId;
        try{
            update tempEU;
            fillPage4Wrapper();
            system.debug('tempEU.Line_Item_Qualification_Unit__c****'+tempEU.Line_Item_Qualification_Unit__c);
            return p;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error: ' + e.getMessage()));return null;}
    }
    //retrieve training organisation values
    public List<SelectOption> getTrainingOrgPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        Map<String,String> optionMap = new Map<String,String>();
        List<Line_Item_Qualifications__c> liq = new List<Line_Item_Qualifications__c>();
        liq = [select Id, Name, Contract_Line_Item__r.Purchasing_Contract__r.Client__r.Name, 
                Contract_Line_Item__r.Purchasing_Contract__r.Client__c,
                Contract_Line_Item__r.Purchasing_Contract__r.Client__r.RecordTypeId,
                Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c,
                Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__r.Name
                FROM Line_Item_Qualifications__c
                WHERE Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c != null
                order by Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__r.Name
                ];
        options.add(new SelectOption('', ''));      
        for(Line_Item_Qualifications__c l : liq){
            if(!optionMap.containsKey(l.Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__r.Name)){
                options.add(new SelectOption(l.Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__c, l.Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__r.Name));
                optionMap.put(l.Contract_Line_Item__r.Purchasing_Contract__r.Training_Organisation__r.Name,''); 
            }
        } 
        
        return options;
    }
    /* WRAPPER CLASSES */
    public class page1Wrapper{
        public Line_Item_Qualifications__c liqRec {get;set;}
        public Boolean radio {get;set;}
        
        public page1Wrapper(Line_Item_Qualifications__c liqRec){
            this.liqRec = liqRec;
            radio = false;
        }
    }
    
    public class page2Wrapper{
        public Enrolment_Unit__c euRecord {get;set;}
        public Boolean check {get;set;}
        
        public page2Wrapper(Enrolment_Unit__c euRecord, boolean check){
            this.euRecord = euRecord;
            this.check = check;
        }
    }
    
    public class page3Wrapper{
        public Line_Item_Qualifications__c pc {get;set;}
        public Boolean radio {get;set;}
        public String active {get;set;}
        
        public page3Wrapper(Line_Item_Qualifications__c pc, String active){
            this.pc = pc;
            radio = false;
            this.active = active;
        }
    }
    
    public class page4Wrapper{
        public Enrolment_Unit__c updatedeuRecord {get;set;}
        public String oldContract {get;set;}
        public String oldLIQU {get;set;}
        public String oldLiquId {get;set;}
        
        public page4Wrapper(Enrolment_Unit__c updatedeuRecord, String oldContract, String oldLIQU, String oldLiquId){
            this.updatedeuRecord = updatedeuRecord;
            this.oldContract = oldContract;
            this.oldLIQU = oldLIQU;
            this.oldLiquId = oldLiquId;
        }
    }
    /*END OF WRAPPER CLASSES*/
}