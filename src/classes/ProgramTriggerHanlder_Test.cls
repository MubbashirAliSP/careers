@isTest
private class ProgramTriggerHanlder_Test{

        
    static testMethod void updateToInactiveProgram(){
            
        Test.startTest();
        
        Qualification__c qual = new Qualification__c();
        Program__c prog = new Program__c();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Intake__c intake = new Intake__c();
     
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        prog = TestCoverageUtilityClass.createProgram(qual.id, 'a1f90000001QBn0');
        intake = TestCoverageUtilityClass.createIntake(qual.id, prog.id, 'a1f90000001QBn0', 'a1O90000001pPot');
      
        prog.Inactive_Program__c = true;
        update prog;     

        prog.Inactive_Program__c = true;
        update prog;  
        
        Test.stopTest();
        
    }
    
    static testMethod void updateToActiveProgram(){
            
        Test.startTest();
        
        Qualification__c qual = new Qualification__c();
        Program__c prog = new Program__c();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Intake__c intake = new Intake__c();
     
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        prog = TestCoverageUtilityClass.createProgram(qual.id, 'a1f90000001QBn0');
        intake = TestCoverageUtilityClass.createIntake(qual.id, prog.id, 'a1f90000001QBn0', 'a1O90000001pPot');

        prog.Inactive_Program__c = False;
        update prog;  
        
        Test.stopTest();
        
    }
}