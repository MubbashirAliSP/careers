public with sharing class ClaimController2 {
    
    public Claim_Number__c dummyClaimNumber {get;set;}
    
    public String selectedFile {get;set;}
    
    public Boolean includeUnclaimed {get;set;}
    
    public Boolean activeEnrl {get;set;}
    
    public String reportingType {get;set;}
    
    private final String state = ApexPages.currentPage().getParameters().get('state');
    
    private final Id rto = ApexPages.currentPage().getParameters().get('rto');
    
    private final Boolean deleteExisting = Boolean.valueOf(ApexPages.currentPage().getParameters().get('de'));
        
    private final String isNSWClaim = ApexPages.currentPage().getParameters().get('claim');
    
    public String fundingSourceSearch {get;set;}
    
    public String ContractCodeSearch {get;set;}
    
    public String claimType {get;set;}
    
    public List<NSWEnrolmentStudent>NSWEnrolmentStudentList {get;set;}
   
    public List<Enrolment__c> FilteredEnrolments {get;set;}
    
    public List<PurchasingContractView> FilteredContracts {get;set;}
    
    public List<SelectOption>claimTypes  {get;set;}
    
    public List<SelectOption>NSWOptions {get;set;}
    
    public List<SelectOption>claimList {get;set;}
    
    private Id claimId;
    
    public List<Id>EnrolmentIds = new List<Id>();
    
    public Set<String>ContractsCodes = new Set<String>();

    public ClaimController2 () {
        
        reportingType = String.valueOf(ApexPages.currentPage().getParameters().get('rt')); 

        claimTypes = new List<SelectOption>();
        claimTypes.add(new SelectOption('Commencement Claim', 'Commencement Claim'));
        claimTypes.add(new SelectOption('Midway Claim', 'Midway Claim'));
        claimTypes.add(new SelectOption('Final Claim', 'Final Claim'));
        
        dummyClaimNumber = new Claim_Number__c();
        
        activeEnrl = false;
        
        FilteredContracts = new List<PurchasingContractView>();
        NSWEnrolmentStudentList = new List<NSWEnrolmentStudent>();
        
        for(Purchasing_Contracts__c c : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state]) {
            FilteredContracts.add(new PurchasingContractView(c, false));
        }

    }
    
    public void doClaim() {
        if(state == 'New South Wales') {
            claimId = ClaimGenerator.claimNSW(FilteredEnrolments, state, rto, dummyClaimNumber.Claim_Date__c,claimType );
        }
    }

    public PageReference NSWNextButton() { //run validator for APL
        FilteredEnrolments = new List<Enrolment__c>();
        
        for(NSWEnrolmentStudent es : NSWEnrolmentStudentList) {
            if(es.selected) {
                FilteredEnrolments.add(es.Enrl);
                EnrolmentIds.add(es.Enrl.Id);
            }
         }
        
         if(EnrolmentIds.size() == 0) {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'You Must Select At Least 1 Student'));
             return null;
         }

         /*Do Claim and return no normal validation screen */
        if(isNSWClaim == 'false') {
            doClaim();
        }
                
        Database.executeBatch(new NATValidationBatch_InitialNSW( rto, EnrolmentIds, deleteExisting ) ); 

        return new PageReference('/apex/NATValidatorRunning');
         
    }
    
    public PageReference NSWValidate() { //Validator for S&S
       
       List<String> selectedContractCodes = new List<String>();
       List<String> selectedContractIds = new List<Id>();
        
       String pur_codes = '';
       Set<String> tempPur_codes = new Set<String>();
         
         for(PurchasingContractView p : FilteredContracts ) {
            if(p.selected) {
                selectedContractCodes.add(p.pc.Contract_Code__c);
                selectedContractIds.add(p.pc.Id);
            }
        }
        
        Database.executeBatch(new NATValidationBatch_Initial( rto, state, null, selectedContractCodes, selectedContractIds, deleteExisting, false) );

        return new PageReference('/apex/NATValidatorRunning');
         
    }
    
    public PageReference NSWBackButton() {
        return new PageReference('/apex/NSWClaimingPurchasingContractFilter2');
    }
                
    public PageReference NSWSearch() {
            
        NSWEnrolmentStudentList = new List<NSWEnrolmentStudent>();
        
        dummyClaimNumber = new Claim_Number__c();
        
        Map<Id,Enrolment_Claims__c>ecMap  = new Map<Id,Enrolment_Claims__c>();
        
        ContractsCodes = new Set<String>();
        
        for(PurchasingContractView p : FilteredContracts ) {
            if(test.isRunningTest()) {
                p.selected = true;
            }
            if(p.selected) {
                ContractsCodes.add(p.pc.Contract_Code__c);
            }
        }         
        
        List<Enrolment__c> Enrols  = new List<Enrolment__c>([Select id, Name,
                                                             Student__r.FirstName,
                                                             Student__r.LastName,
                                                             Student__r.PersonBirthDate,
                                                             Student__r.Full_Student_Name__c,
                                                             Student__r.Student_Identifer__c,
                                                             Student__r.IsPersonAccount,
                                                             Qualification__r.Name,
                                                             Contract_Code__c,
                                                             No_of_Enrolment_Claims__c,
                                                             of_Enrolment_Completed__c,
                                                             End_Date__c
                                                             
                                                             FROM Enrolment__c
                                                             WHERE Delivery_Location_State__c = : state AND
                                                             NSW_Claiming_Allowed__c = 'Yes' AND
                                                             Contract_Code__c in :ContractsCodes AND
                                                             No_of_Enrolment_Claims__c in : NATGeneratorUtility.claimCodes(claimType)
                                                             LIMIT 500
                                                            ]);
        
        
        for(Enrolment_Claims__c ec : [Select Id, Enrolment__c,of_Enrolment_Completed__c,createdDate FROM Enrolment_Claims__c WHERE Enrolment__c in : Enrols order by createdDate]) {
            ecMap.put(ec.Enrolment__c,ec);
        }
        
        
        for(Enrolment__c e : Enrols) {
            if(e.No_of_Enrolment_Claims__c == 0) {
                NSWEnrolmentStudentList.add(new NSWEnrolmentStudent(false,null, e));
            }
            else {
                if(e.of_Enrolment_Completed__c > ecMap.get(e.Id).of_Enrolment_Completed__c) {
                    NSWEnrolmentStudentList.add(new NSWEnrolmentStudent(false,Date.ValueOf(ecMap.get(e.Id).CreatedDate), e));
                }
            }
        }                       
        return null;
    }
    
      
      public pageReference cancelButton() {
          return new PageReference('/apex/NATGeneratorStateSelection2'); 
      }
      public PageReference NextToClaiming() {
          return new PageReference('/apex/NSWClaiming2?state=' +state + '&rto=' + rto); 
      }

      public pageReference filterPurchasingContract() {

      	FilteredContracts = new List<PurchasingContractView>();
        
        if(fundingSourceSearch == '' && ContractCodeSearch == '') {
            for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state] ) {
                FilteredContracts.add(new PurchasingContractView(pc, false));
            }
        } 
        else {
            if(fundingSourceSearch !='' && ContractCodeSearch != '') {
                ContractCodeSearch = '%' + ContractCodeSearch + '%';
                List<String> searchParams = new List<String>();
                searchParams = fundingSourceSearch.split(' AND ');
                
                for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE State_Fund_Source__r.Code__c in : searchParams AND Contract_Code__c like : ContractCodeSearch AND Training_Organisation__c = : rto AND Contract_State__c = : state]) {
                    FilteredContracts.add(new PurchasingContractView(pc, false));
                }
            }
            else if(fundingSourceSearch !='' && ContractCodeSearch == ''){
                
                List<String> searchParams = new List<String>();
                searchParams = fundingSourceSearch.split(' AND ');
                
                for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE State_Fund_Source__r.Code__c in : searchParams AND Training_Organisation__c = : rto AND Contract_State__c = : state]) {
                    FilteredContracts.add(new PurchasingContractView(pc, false));
                }
            }
            
            else if(fundingSourceSearch =='' && ContractCodeSearch != ''){
                ContractCodeSearch = '%' + ContractCodeSearch + '%';
                
                for(Purchasing_Contracts__c pc : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE Contract_Code__c like : ContractCodeSearch AND Training_Organisation__c = : rto AND Contract_State__c = : state]) {
                    FilteredContracts.add(new PurchasingContractView(pc, false));
                }
            }
        }
        return null;
    }

    public class NSWEnrolmentStudent {
        public boolean selected {get;set;}
        public Date lastClaim {get;set;}
        public Enrolment__c Enrl {get;set;}
            
        public NSWEnrolmentStudent(Boolean sel, Date lclaimDate, Enrolment__c enrolment ) {
            selected = sel;
            lastClaim = lclaimDate;
            Enrl = enrolment;
            }
        }
}