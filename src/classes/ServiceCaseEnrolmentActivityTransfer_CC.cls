/**
 * @description Extension controller for the Service Case Enrolment Activity Transfer page
 * @author Ranyel Maliwanag
 * @date 19.MAY.2015
 * @history
 *       21.MAY.2015    Ranyel Maliwanag    Changed to `without sharing`
 */
public without sharing class ServiceCaseEnrolmentActivityTransfer_CC {
    public Service_Cases__c activeRecord {get; set;}
    public String targetEnrolmentNumber {get; set;}
    public Boolean isTransferred {get; set;}
    public Id sourceEnrolmentId {get; set;}
    public List<Task> sourceEnrolmentTasks {get; set;}

    private Enrolment__c targetEnrolment {get; set;}
    private Boolean isValidated {get; set;}


    /** 
     * @description Standard controller constructor for the extension class
     * @author Ranyel Maliwanag
     * @date 19.MAY.2015
     */
	public ServiceCaseEnrolmentActivityTransfer_CC(ApexPages.StandardController controller) {
        // Fetch active Service Case record
		Id activeRecordId = controller.getId();
        String queryString = 'Select Id, Name, Enrolment__r.Id, Enrolment__r.Name, Enrolment__r.Student__c ';
        queryString += 'FROM Service_Cases__c ';
        queryString += 'WHERE Id = :activeRecordId';
        activeRecord = Database.query(queryString);
        sourceEnrolmentId = activeRecord.Enrolment__r.Id;

        // Fetch tasks to be transferred
        queryString = 'SELECT Id, Subject, Who.Name ';
        queryString += 'FROM Task ';
        queryString += 'WHERE WhatId = :sourceEnrolmentId';
        sourceEnrolmentTasks = Database.query(queryString);

        // Initialise condition variables
        isValidated = false;
        isTransferred = false;
	}


    /**
     * @description CommandButton handler to validate the enrolment number given that it exists and is of the same account as the source enrolment record
     * @author Ranyel Maliwanag
     * @date 19.MAY.2015
     * @return (PageReference): Expected to return null to trigger reRendering of apex components
     */
    public PageReference validateEnrolmentNumber() {
        String queryString = 'SELECT Id, Student__c, Name ';
        queryString += 'FROM Enrolment__c ';
        queryString += 'WHERE Name = :targetEnrolmentNumber';

        ApexPages.Message infoMessage;
        try {
            targetEnrolment = Database.query(queryString);

            if (targetEnrolment != null && targetEnrolment.Student__c == activeRecord.Enrolment__r.Student__c) {
                // Condition: Target enrolment exists and of the same account as the source enrolment
                infoMessage = new ApexPages.Message(ApexPages.Severity.INFO, 'Your Enrolment Number is validated');
                isValidated = true;
            } else {
                // Condition: Target enrolment and source enrolment belong to different accounts
                infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'The Enrolment Numbers must belong to the same student. Please make sure Enrolment Numbers are correct.');
            }
        } catch (Exception e) {
            // Condition: No target enrolment found for the given enrolment number
            infoMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'This Enrolment Number does not exist.');
        }

        ApexPages.addMessage(infoMessage);
        return null;
    }


    /**
     * @description CommandButton handler to transfer tasks from source enrolment to target enrolment
     * @author Ranyel Maliwanag
     * @date 19.MAY.2015
     * @return (PageReference): Expected to return null to trigger reRendering of apex components
     */
    public PageReference transferTasks() {
        if (isValidated) {
            // Condition: Target enrolment number is validated and record exists
            if (sourceEnrolmentTasks != null && sourceEnrolmentTasks.size() > 0 && targetEnrolment != null) {
                // Condition: There are enrolment tasks to transfer and the target enrolment record exists
                // Actual task transfer update
                for (Task taskRecord : sourceEnrolmentTasks) {
                    taskRecord.WhatId = targetEnrolment.Id;
                }
                update sourceEnrolmentTasks;

                // Update Service Case record with successful transfer information
                activeRecord.IsTaskListTransferred__c = true;
                activeRecord.Tasks_Transferred_to_Enrolment__c = targetEnrolment.Id;
                update activeRecord;

                // Reinitialise page variables to update information displayed on the visualforce page
                sourceEnrolmentTasks = new List<Task>();
                targetEnrolmentNumber = '';
                isTransferred = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Enrolment tasks have been transferred from ' + activeRecord.Enrolment__r.Name + ' to ' + targetEnrolment.Name));
            } else {
                // Condition: No enrolment tasks to transfer
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There are no tasks or activities to transfer from this Enrolment'));
            }
        } else {
            // Condition: Target enrolment number is not validated
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Enrolment number needs to be validated first'));
        }

        return null;
    }
}