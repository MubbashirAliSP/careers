public with sharing class UnitUpgradeCtrl {
    public UnitUpgradeCtrl(){
        system.assert(System.currentPagereference().getParameters().containsKey('newValue'), 'parameter newValue must be given');
    }
    public String retURL {
        get{
            return System.currentPagereference().getParameters().get('retURL');
        }
    }
    
    private Training_Component__c cachedNewComponent;
    public Training_Component__c newComponent {
        get{
            if ( cachedNewComponent == null)
                cachedNewComponent = [ select 
                        Id, 
                        Parent_Code__c, //CPC08
                        Code__c 
                    From 
                        Training_Component__c 
                    Where 
                        Code__c = :System.currentPagereference().getParameters().get('newValue') ]; //CPCSFS5002B
            return cachedNewComponent;
        }
    }
    
    private Unit__c cachedOldUnit;
    public Unit__c oldUnit {
        get{
            if ( cachedOldUnit== null ){
                Training_Component__c newComponent = this.newComponent;
                system.debug('newComponent ***'+newComponent);
                String baseCodeH = getBaseUnitCode(newComponent.Code__c) + '%';
                Unit__c oldUnit = null;
            
                for ( Unit__c unit : [ select Name from Unit__c where Name Like :baseCodeH ORDER BY Name ] ){
                    if ( unit.name.compareTo(newComponent.Code__c) > 0 ){
                        //while the unit comes before (alphabetically) this tc use this one
                        //note that the units are sorted in the query, so the 'latest' unit will come last
                        break;
                    }
                    oldUnit = unit;
                }
                cachedOldUnit = oldUnit;
            }
            return cachedOldUnit;
        }
    }
    
    public class TS{
        public boolean i { get; set; }
        public Enrolment_Unit__c p{get;set;}
        public boolean isChangeable {
            get{
                if ( p.Unit_Result__c == null || p.Unit_Result__r.National_Outcome__c == null )
                    return true;
                return p.Unit_Result__r.National_Outcome__r.TGA_Re_Enrolable_Outcome__c != null && p.Unit_Result__r.National_Outcome__r.TGA_Re_Enrolable_Outcome__c;
            }
        }
        
        public TS(){
            i = false;
        }
    }
    
    private List<TS> cachedStudents;
    public List<TS> transferrableStudents{
        get{
            if ( cachedStudents == null ){
                Unit__c oldUnit = this.oldUnit;
                List<TS> ret = new List<TS>();
                for ( Enrolment_Unit__c eu : [ 
                        select 
                            Id,
                            Name,
                            Start_Date__c,
                            End_Date__c,
                            Enrolment__r.Student_Identifier__c,
                            Enrolment__r.Student__r.Name,
                            Unit_Result__r.National_Outcome__r.Name,
                            Unit_Result__r.National_Outcome__r.TGA_Re_Enrolable_Outcome__c
                        From 
                            Enrolment_Unit__c 
                        Where 
                            Unit__c = :oldUnit.id 
                        Order By
                            Enrolment__r.Student__r.Name
                        ] ){
                    TS r = new TS();
                    r.p = eu;
                    ret.add(r);
                }
                
                cachedStudents = ret;
            }
            return cachedStudents;
        }
    }


    private string getBaseUnitCode(String code){
        if ( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.countMatches(code.substring(code.length()-1)) > 0 ){
            return code.substring(0, code.length()-1);
        }
        else{
            return null;    
        }    
    }
    
    
    public PageReference importUnit(){
        Unit__c oldUnit = this.oldUnit; //fetch before we create new unit
        UnitsChangesReportCtrl uctrl = new UnitsChangesReportCtrl();
        uctrl.parentCode = newComponent.Parent_Code__c;
        for ( UnitsChangesReportCtrl.TC u : uctrl.units){
            if ( u.p.id == newComponent.id ){
                u.i = true;
                u.isUpgrade = false; //force allowing of upgrade...
            }
        }
        uctrl.importSelectedUnits();
        
        Unit__c newUnit = [
            Select Name From Unit__c Where Name = :newComponent.Code__c 
        ];
        
        
        
        
        List<Enrolment_Unit__c> toUpdate = new List<Enrolment_Unit__c>();
        for ( TS t : transferrableStudents ){
            if ( t.i ){
                //Update to Unit__c Lookup Required to reference the new Unit
                t.p.Unit__c = newUnit.id;
                toUpdate.add(t.p);
            }
        }
        update toUpdate;
        
        
        
        
        Intake_Unit__c[] intakes = [ 
            Select 
                Id,
                Unit_Start_Date__c,
                Unit_of_Study__r.Name
            From 
                Intake_Unit__c 
            Where 
                Unit_Start_Date__c > :System.today() AND 
                Unit_of_Study__c = :oldUnit.id
                ];
        if ( intakes.size() > 0 ){
            for ( Intake_Unit__c intake : intakes ){
                // Update to Unit__c Lookup Required to reference the new Unit
                intake.Unit_of_Study__c = newUnit.id;
            }
            update intakes;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, intakes.size() + ' intake units were transferred to ' + newUnit.name));
        
        }
        
        
        
        
        List<Qualification_Unit__c> newQualUnitsList = new List<Qualification_Unit__c>();
        List<Qualification_Unit__c> oldQualUnitsList = new List<Qualification_Unit__c>();       
        // we are required now to create a new qual unit which supersedes teh old one
        for ( Qualification_Unit__c oldQualUnit : [
            Select
                Qualification__c, 
                Stage__c, 
                Unit_Record_Type__c, 
                Unit_Type__c
            From
                Qualification_Unit__c
            Where
                Unit__c = :oldUnit.id
        ]){

            //update old qual unit to be inactive
            oldQualUnit.Inactive__c = true;
           // update oldQualUnit;
           oldQualUnitsList.add(oldQualUnit);
          
            //create new qual unit as a copy
            Qualification_Unit__c newQualUnit = new Qualification_Unit__c(
                Qualification__c = oldQualUnit.Qualification__c, 
                Stage__c = oldQualUnit.Stage__c, 
                Unit_Record_Type__c = oldQualUnit.Unit_Record_Type__c, 
                Unit_Type__c = oldQualUnit.Unit_Type__c,
                Unit__c = newUnit.id,
                Superseeded_Qualification_Unit__c = oldQualUnit.id,
                Don_t_auto_add_this_unit_to_enrolments__c=true
            );
           // insert newQualUnit;
              newQualUnitsList.add(newQualUnit);
           }
           
           update oldQualUnitsList;
           insert newQualUnitsList;
            
            
           List<Line_Item_Qualification_Units__c>oldLiQualUnitList = new List<Line_Item_Qualification_Units__c>();
           List<Line_Item_Qualification_Units__c>newLiQualUnitList = new List<Line_Item_Qualification_Units__c>();
            //we also need to get the old LIQU and supercede it
          for (  Line_Item_Qualification_Units__c oldLiQualUnit :  [
                Select
                    Line_Item_Qualification__c,
                    Nominal_Hours__c, 
                    Points__c, 
                    Unit_Cost__c
    
                From
                    Line_Item_Qualification_Units__c
                Where
                    Qualification_Unit__c in :oldQualUnitsList
            ]){
                //update old qual unit to be inactive
                oldLiQualUnit.Inactive__c = true;
                oldLiQualUnitList.add(oldLiQualUnit);
                
              
            }
            
            update oldLiQualUnitList;
            
            for(Qualification_Unit__c newQualUnit : newQualUnitsList) {
            
            	for(Line_Item_Qualification_Units__c oldLiQualUnit : oldLiQualUnitList) {
            		
            		if(newQualUnit.Superseeded_Qualification_Unit__c == oldLiQualUnit.Qualification_Unit__c ) {
            			
            			Line_Item_Qualification_Units__c newLiQualUnit = new Line_Item_Qualification_Units__c(
                    	Qualification_Unit__c = newQualUnit.id,
                   		Line_Item_Qualification__c = oldLiQualUnit.Line_Item_Qualification__c,
                   		Nominal_Hours__c = oldLiQualUnit.Nominal_Hours__c, 
                   		Points__c = oldLiQualUnit.Points__c, 
                   		Unit_Cost__c = oldLiQualUnit.Unit_Cost__c,
                    	Superseeded_Line_Item_Qualification_Unit__c = oldLiQualUnit.id
                		);
                		
                		
            			newLiQualUnitList.add(newLiQualUnit);
            		}
            		
            		
            		
            		
            	
            	
            	}
            
            	 
            
            }
            
            insert newLiQualUnitList;
        
        
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, toUpdate.size() + ' students were transferred to ' + newUnit.name));
        cachedStudents = null;
    
        return null;
    }
    
    public void selectAllStudents(){
        for ( TS t : transferrableStudents ){
            if ( t.isChangeable )
                t.i = true;
        }
    }
}