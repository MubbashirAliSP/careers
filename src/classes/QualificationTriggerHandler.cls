/**
 * @description Trigger handler for Qualification object 
 * @author Yuri Gribanov
 * @date 16.OCT.2012
 *
 * HISTORY
 * - 16.OCT.2012  Yuri Gribanov   - Created.
 * - 18.JAN.2013  Janella Canlas  - added Method to update related EUoS records
 * - 06.MAY.2013  Jade Serrano    - created a method updateDisciplineCode that will update discipline code of Enrolment Unit of Study
 * - 02.APR.2015  Kim Saclag      - moved the for loop inside the if condition.
**/ 
 
public with sharing class QualificationTriggerHandler {
  /*
    This code copies qualification units from Standard Qual, to Tailored Qual
  */
  public static void cloneTailoredQualificationUnits(Set<Id> qualIds, List<Qualification__c> tailoredQuals) {
    List<Qualification__c>standardQuals = [Select Id, Name, (Select id,Unit__c,Unit_Type__c FROM Qualification_Units__r ) From Qualification__c WHERE id = : qualIds];
    List<Qualification_Unit__c>qualUnits = new List<Qualification_Unit__c>();
    
    for(Qualification__c q: tailoredQuals) {
      for(Qualification__c sq: standardQuals ) {
        if(q.Award_Based_On__c == sq.id && q.DoNotAddQualUnits__c == false) {
        for(Qualification_Unit__c units: sq.Qualification_Units__r ) {
            Qualification_Unit__c newQualUnit = new Qualification_Unit__c(Qualification__c = q.id,
                                            Unit__c = units.Unit__c,
                                            Unit_Type__c = units.Unit_Type__c);
            qualUnits.add(newQualUnit);
          }
        }
      }
    }
    insert qualUnits;
  }
  /*
    This method will update Course of Study Type code of related EUoS records
  */
  public static void updateEUOS(Set<Id> qualificationIds){
    //query qualifications
    List<Qualification__c> qList = [Select Id, Course_of_Study_Type__r.Code__c from Qualification__c where Id IN: qualificationIds];
    //query related EUOS records
    List<Enrolment_Unit_of_Study__c> eList = [Select Id, Course_of_Study_type_code__c, Qualification_Unit__r.Qualification__c
                          FROM Enrolment_Unit_of_Study__c
                          WHERE Qualification_Unit__r.Qualification__c IN: qualificationIds];
    Map<Id, Qualification__c> qMap = new Map<Id,Qualification__c>();
    for(Qualification__c q : qList){
        qMap.put(q.Id, q);
    }
    if(eList.size() > 0){
        for(Enrolment_Unit_of_Study__c e : eList){
            if(qMap.containsKey(e.Qualification_Unit__r.Qualification__c)){
                e.Course_of_Study_type_code__c = qMap.get(e.Qualification_Unit__r.Qualification__c).Course_of_Study_Type__r.Code__c;
            }
        }
    }
    /*//check if query returned records and loop to populate field
    if(eList.size() > 0){
      for(Enrolment_Unit_of_Study__c e : eList){
        for(Qualification__c q : qList){
          if(e.Qualification_Unit__r.Qualification__c == q.Id){
            e.Course_of_Study_type_code__c = q.Course_of_Study_Type__r.Code__c;
          }
        }
        system.debug('STC****'+e.Course_of_Study_type_code__c);
      }
    }*/
    try{
      update eList;}catch(Exception e){system.debug('ERROR: '+e.getMessage());
    }
  }
  
    public static void updateDisciplineCode(Set<Id> quaIds){
  
        if(quaIds.size()>0){
        
            Qualification__c qList = [SELECT id, Discipline_code__c FROM Qualification__c WHERE id IN: quaIds LIMIT 1];
            system.debug('##qList: '+qList);
            
            if(qList != null){         
                List<Field_of_Education__c> foeList = [SELECT id, FoE_Code__c FROM Field_of_Education__c WHERE id =: qList.Discipline_code__c LIMIT 1];          
                
                if(foeList.size()>0){
                
                    List<Qualification_Unit__c> quList = [SELECT id FROM Qualification_Unit__c WHERE Qualification__c IN: quaIds];
                    system.debug('##quList: '+quList);
                    
                    if(quList.size()>0){
                    
                        Set<Id> quIds = new Set<Id>();
                        /*for(Qualification_Unit__c q: quList){
                            quIds.add(q.Id);
                        }*/
                        if(quIds.size()>0){
                        
                            List<Enrolment_Unit_of_Study__c> euosList = [SELECT id, Discipline_code__c FROM Enrolment_Unit_of_Study__c WHERE Qualification_Unit__c IN: quList];
                            system.debug('##euosList: '+euosList);
                        
                            for(Enrolment_Unit_of_Study__c e: euosList){
                                e.Discipline_code__c = foeList[0].FoE_Code__c;
                            }
                            //KS Start 2.APR.2015
                            for(Qualification_Unit__c q: quList){
                                quIds.add(q.Id);
                            }//KS End
                            update euosList;
                            
                        }       
                        
                    }
                
                }
                
            }       
            
        }
  
    }

}