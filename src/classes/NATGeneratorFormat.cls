public class NATGeneratorFormat {
    
    public static String formatDate (String inDate) {
        
        if(inDate == null || inDate == 'null' || inDate == '') {
            return '';
        }
        else {
            String tempDate = inDate.subString(0,10);
            String outDate = tempDate.subString(8,10) + tempDate.subString(5,7) + tempDate.subString(0,4);
            return outDate;
        }
    }
    
    public static String formatCheckBox(Boolean checked) {
        if(checked)
            return 'Y';
        else
            return 'N';
     }
    
    public static String formatString(String inString, Integer inLength) {
        String outString;
        if(inString == null || inString == 'null') { 
            inString = ' ';
        }
        outString = instring.rightPad(100).subString(0, inLength);
        return outString.toUppercase();        
    }
    
    public static String formatNumber(String inNumber, Integer inLength) {
        String outNumber;
        if(inNumber == null || inNumber == 'null' || inNumber == '') {
            outNumber = '0';
            outNumber = outNumber.rightPad(100, '0').subString(0, inLength);
        }
        else {
            Decimal inDec = Decimal.valueOf(inNumber);
            Integer inInt = Integer.valueOf(inDec.round());
            String outStr = String.valueOf(inInt);
			String padding = ''.leftPad( (inLength - outStr.length() ), '0');
            outNumber = padding + outStr;
        }
        return outNumber; 
    }
}