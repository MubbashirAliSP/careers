/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchCampus implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchCampus() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPC_Campus_Code__c, IPC_InPlace_Campus__c, IPC_Sync_on_next_update__c, Description__c, Name, Id ';
        queryString += 'FROM Locations__c ';
        queryString += 'WHERE IPC_InPlace_Campus__c = true AND IPC_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Locations__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Locations__c record : records) {
            if (!exportedIdentifiers.contains(record.IPC_Campus_Code__c)) {
                InPlace__c forExport = new InPlace__c(
                        IPCampus_CampusCode__c = record.IPC_Campus_Code__c,
                        IPCampus_Description__c = record.Name
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPC_Campus_Code__c);
            }
            
            record.IPC_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchCourse nextBatch = new InPlaceExportBatchCourse();
        Database.executeBatch(nextBatch);
    }
}