/**
 * @description N/A
 * @author N/A
 * @date N/A
 * @history
 *       08.JAN.2016    Biao Zhang          - Fetch the enrolment unit in collection year. No idea why previous change from JEA remove the year range. Regarding case 00036654. 
 *       15.JAN.2016    Ranyel Maliwanag    - Added fix to the messed up dynamic SOQL in case there are no enrolments included in the Batch scope
 *       01.FEB.2016    Ranyel Maliwanag    - Added additional fields for `queryStringMain`. Reference Case: 00038985
 */
global without sharing class NATValidationBatch_Initial implements Database.Batchable<sObject>,Database.Stateful {
    
    global String currentState;
    global Id tOrgId;
    global String currentYear;
    global List<String> selectedContractCodes;
    global List<String> selectedContractIds;
    global Boolean delOldFiles;
    global NAT_Validation_Log__c log;
    global final Integer max_query_length = 131072;
    global string pur_codes;
    global string recTypeName;
    global String studentIds = '(';
    global String queryStringStudents;
    global String queryStringStudentRAPT = '';
    global String queryStringEnrolmentRAPT = '';
    global String queryStringMain;
    global String endQuery;
    global String queryStringUnitEnrolment;
    global String libraryId;
    global Boolean isWARAPT = false;
    global String reportType;
    global DateTime currentDT;
    global String currentDTS;
    
    global NATValidationBatch_Initial(Id trainingOrgId, String state, String year, List<String> purchasingContractCodes, List<String> purchasingContractIds, Boolean deleteOldFiles, Boolean isRAPT) {
                
        currentState = state;
        tOrgId = trainingOrgId;
        if(State != 'New South Wales' && State != 'New South Wales APL') {
            currentYear = year;
        }
        selectedContractCodes = purchasingContractCodes.clone();
        selectedContractIds = purchasingContractIds.clone();
        delOldFiles = deleteOldFiles;
        isWARAPT = isRAPT;
        
        reportType = 'State';
        
        if(currentState == 'National') {
            reportType = currentState;
        }
                       
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        pur_codes = '';
        
        currentDT = Datetime.Now();
        currentDTS = currentDT.format('yyyy-MM-dd')+'T00:00:01.000Z';    
        recTypeName = 'Unit of Competency';
        
        if(currentState == 'New South Wales APL' || currentState == 'New South Wales') {
            log = new NAT_Validation_Log__c(Validation_State__c = currentState, Training_Organisation_Id__c = tOrgId, Delete_Existing_NAT_Files__c = delOldFiles);

        }
        else {
            log = new NAT_Validation_Log__c(Validation_State__c = currentState, Training_Organisation_Id__c = tOrgId, Collection_Year__c = currentYear, Delete_Existing_NAT_Files__c = delOldFiles);
        }
                 
        for(Integer i = 0 ; i < selectedContractCodes.size() ; i++ ) {
             pur_codes += '\'' + selectedContractCodes.get(i) + '\',';
        }
                
        pur_codes = pur_codes.lastIndexOf(',') > 0 ? '(' + pur_codes.substring(0,pur_codes.lastIndexOf(',')) + ')' : pur_codes;
        
        String query;
        
        //if Report_for_AVETMISS_Type__c blank or state report state, else national
        /*
        *   @history
        *   JEA 28.SEPT.2015 - modified the current variable in query - currentDTS
        *                    - updated the query to get all enrolments except the one which has final payment made equals true which is just remove Enrolment_Status__c LIKE \'%Active%\' filter from query
        *                    - updated enrolment query to get all enrolments irrespective of year selected, so we need to get all enrolments irrespective of year 
        */    
        if(reportType == 'State') {
            if(currentState == 'New South Wales'|| currentState == 'New South Wales APL') {
                query = 'SELECT Id, Student__c FROM Enrolment__c WHERE Id IN ' +
                                '(SELECT Enrolment__c FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Enrolment__r.Final_Payment_Made__c = false AND Report_for_AVETMISS__c= true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND Unit__r.RecordType.Name = \''+recTypeName+'\' AND CreatedDate <=' + currentDTS + ')';
            }
            else {
                query = 'SELECT Id, Student__c FROM Enrolment__c WHERE Id IN ' +
                                     '(SELECT Enrolment__c FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) + ')  AND Unit__r.RecordType.Name = \'' +recTypeName+'\')';  

                    //comment out by Biao on 31/12/2015
                     //'(SELECT Enrolment__c FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) + ')  AND Unit__r.RecordType.Name = \'' +recTypeName+'\' AND CreatedDate <=' + currentDTS + ')';  
            }    
        }
        
        else {
            query = 'SELECT Id, Student__c FROM Enrolment__c WHERE Id IN ' +
                  '(SELECT Enrolment__c FROM Enrolment_Unit__c WHERE Enrolment__r.Report_for_AVETMISS_Type__c = \'National\' AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) + ')  AND Unit__r.RecordType.Name = \''+recTypeName+'\' AND CreatedDate <=' + currentDTS + ')';           
            } 
         /* end JEA 28.SEPT.2015 */           
          
        System.debug('query ::: ' + query);
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        
                       
        for (SObject s :scope) {
            Enrolment__c e = (Enrolment__c) s;
            studentIds += '\'' + e.Student__c + '\',';
        }
        
       
       
    }


    /**
     * @description N/A
     * @author N/A
     * @date N/A
     * @history
     *       15.JAN.2016    Ranyel Maliwanag    Reset the `studentIds` variable to ('') as a fix for the "Expecting colon, found ')'" error
     */
    global void finish(Database.BatchableContext BC) {
        
        // Need to use 2 recordtypes, Avetmiss and RAPT. List contains 2 values. 0 = AV, 1 = RAPT
        List<RecordType> recTypes =  new List<RecordType>( [Select Id,developername From RecordType Where SObjectType = 'NAT_Validation_Log__c']);
        
        //If recordtypes are not available, fail assert and do not proceed
        System.AssertEquals(recTypes.size(), 2, 'There must be 2 recordtypes available, Avetmiss and RAPT');
        
        System.debug('studentIds ::: ' + studentIds);
        studentIds = studentIds.substring(0, studentIds.length() - 1);
        
        studentIds += ')';
        
        // Provision in case the code above messes up
        if (studentIds == ')') {
            studentIds = '(\'\')';
        }
        
        queryStringStudents =   'SELECT ID, Student_Identifer__c,Reporting_Other_State_Identifier__c,PersonOtherPostalCode, Highest_School_Level_Comp_Identifier__c, Year_Highest_Education_Completed__c,'+
                                'Sex__c, PersonBirthdate, PersonMailingPostalCode, PersonMailingState,Indigenous_Status_Identifier__c,'+
                                'Main_Language_Spoken_at_Home__r.Name, Main_Language_Spoken_at_Home_Identifier__c, Labour_Force_Status_Identifier__c,'+ 
                                'Country_of_Birth_Identifer__c, At_School_Flag__c,'+
                                'Client_Industry_of_Employment__r.Division__c,Language_Spoken_at_Home_Formula__c,' + 
                                'Proficiency_in_Spoken_English_Identifier__c,Learner_Unique_Identifier__c,'+
                                'Has_prior_educational_achievement_s__c,Prior_Achievement_Flag__c,'+
                                'Prior_Achievement_Type_s__c,Prior_Education_Achievement_Recognition__c,Does_student_have_a_disability__c,Disability_Type__c,Disability_Flag_0__c,'+
                                'FirstName, LastName, PersonMailingStreet, Full_Student_Name__c,'+
                                'PersonMailingCity, State_Identifier__c, PersonHomePhone, Work_Phone__c, Address_building_property_name__c,Address_flat_unit_details__c,Address_street_number__c,Address_street_name__c,Postal_Delivery_Box__c,'+
                                'PersonMobilePhone, PersonEmail, Victorian_Student_Number__c, Salutation,Name_for_encryption_student__c,Age_Group__c,Unique_Student_Identifier__c,'+
                                'Postal_building_property_name__c, Postal_Road_Suffix_Identifier__c, Postal_Road_Type_Identifier__c ,Address_Road_Suffix_Identifier__c, Address_Lot_Number__c, Postal_Floor__c, Postal_flat_unit_details__c,Postal_street_number__c,Postal_street_name__c,Postal_suburb_locality_or_town__c, '+                                
                                'Labour_Force_Status_1__c, Postal_Dwelling_Type_Identifier__c, Address_Floor__c, Client_Industry_of_Employment__c, Client_Occupation_Identifier__c, Suburb_locality_or_town__c, USI_Status__c, Address_Post_Code__c,' +
                                'Labour_Force_Status_1__r.Code__c, Postal_Unit_Number__c, Address_Unit_Number__c, Postal_Lot_Number__c,LastModifiedDate, Address_Road_Type_Identifier__c, Training_Org_Identifier__c, AVETMISS_Organisation_Name__c, Training_Org_Type_Identifier__c, BillingStreet, BillingCity, BillingPostalCode, Address_Dwelling_Type_Identifier__c, Education_Identifier__c,Unique_student_identifier_NAT_Reporting__c ' +
                                ' FROM Account WHERE Id in ' + studentIds;
                                
        if(isWARAPT) {                  
         queryStringStudentRAPT = 'Select Student_Identifer__c, '+
                        'FirstName, '+
                        'LastName, '+
                        'PersonBirthDate, '+
                        'PersonMailingStreet, '+
                        'PersonMailingCity, ' +
                        'PersonMailingPostalCode, ' +
                        'PersonEmail, ' +
                        'Address_street_number__c, ' +
                        'Address_street_name__c, ' +
                        'Suburb_locality_or_town__c, ' +
                        'PersonMailingState, ' +
                        'Address_Post_Code__c, ' +
                        'Work_Phone__c, ' +
                        'PersonMobilePhone, ' +
                        'PersonHomePhone, ' +
                        'Postal_delivery_box__c, '+
                        'Postal_suburb_locality_or_town__c, ' +
                        'PersonOtherPostalCode, ' +
                        'Reporting_Other_State__r.Short_Name__c, ' +
                        'Country_of_Birth_Identifer__c, ' +
                        'Main_Language_Spoken_at_Home_Identifier__c, '+
                        'Main_Language_Spoken_at_Home__c, ' +
                        'Proficiency_in_Spoken_English_Identifier__c, ' +
                        'Indigenous_status_identifier__c, '+
                        'Highest_School_Level_Comp_Identifier__c, ' +
                        'Year_Highest_Education_Completed__c, ' +
                        'At_School_Flag__c, ' +
                        'Labour_Force_Status_Identifier__c, ' +       
                        'Study_Reason_Identifier__c, ' +       
                        'Full_Student_Name__c, ' +
                        'Prior_Achievement_Flag__c, ' +
                        'Prior_Achievement_Type_s__c, ' +
                        'LastModifiedDate, '+
                        'Sex_Identifier__c, ' +
                        'Disability_Type__c, ' +
                        'Disability_Flag_0__c ' +
                   'From Account WHERE Id in ' + studentIds;
                   
                   
       queryStringEnrolmentRAPT = 'Select Id, ' +        
                              'Purchasing_Contract_Identifier__c,(Select Intake_Name__c FROM Enrolment_Intake_Units__r order by createddate desc LIMIT 1), ' +
                              'Unit__r.Name, ' +
                              'Unit__r.Unit_Name__c, ' +
                              'Unit_Code__c, ' +
                              'Unit_Name__c, ' +                              
                              'Scheduled_Hours__c, ' +
                              'Delivery_Strategy__r.Code__c, ' +
                              'Fee_Exemption_Identifier__c, ' +
                              'Unit_Result__r.Result_Code__c, ' +
                              'Enrolment__r.Start_Date__c, ' +
                              'Enrolment__r.End_Date__c, ' +
                              'Start_Date__c, ' +
                              'End_Date__c, ' +
                              'Enrolment__r.Delivery_Strategy__r.Code__c, ' +
                              'Reportable_Tuition_Fee__c, ' +
                              'Client_Fees_Other__c, ' +
                              'Total_Amount_Charged__c, ' +
                              'Training_Delivery_Location__r.Child_Suburb__c, ' +
                              'Training_Delivery_Location__r.Child_Postcode__c, ' +
                              'Enrolment__r.Student__r.TRS_Number__c, ' +
                              'Enrolment__r.Student__r.Student_Identifer__c, ' +
                              'Enrolment__r.Student__r.LastName, ' +
                              'Enrolment__r.Student__r.FirstName, ' +
                              'Enrolment__r.Student__r.PersonBirthDate, ' +
                              'Enrolment__r.Qualification_Issued__c, '+
                              'Enrolment__r.Name,'+
                              'Enrolment__r.Student__r.Unique_student_identifier__c, ' +
                              'LastModifiedDate '+
                              'FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) +')' + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\' ' ;
        
        
        }
        queryStringMain = 'Select Id,' +
                            'Name,' +
                            'Enrolment__r.Qualification__c,'+
                            'Scheduled_Hours__c,'+
                            'Unit_of_Competency_Identifier__c,'+
                            'Training_Delivery_Location__c,'+
                            'Unit__r.RecordType.Name,'+
                            'Unit__r.Field_of_Education_Identifier__c,'+
                            'Unit__r.Unit_Flag__c,'+
                            'Unit__r.Vet_Non_Vet__c,'+
                            'Enrolment__r.Student__c,'+
                            'Enrolment__r.Student_Identifier__c,'+
                            'Delivery_Location_Identifier__c,'+
                            'Delivery_Mode_Identifier__c,'+
                            'AVETMISS_National_Outcome__c,'+
                            'Commencing_Course_Identifier__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c,'+
                            'Purchasing_Contract_Identifier__c,'+
                            'VET_in_Schools__c,'+
                            'Registration_Number_Identifier__c,'+
                            'Client_Identifier_New_Apprenticeships__c,'+
                            'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c,'+
                            'Enrolment__r.Qualification__r.Associated_Course_Identifier__c,'+
                            'Delivery_Mode_Type_State__r.Code__c,'+
                            'Reportable_Tuition_Fee__c,'+
                            'Fee_Exemption_Identifier__c,'+
                            'Enrolment__r.Specific_Program_Identifier__c,'+
                            'Enrolment__r.Training_Contract_Identifier__c,'+
                            'Purchasing_Contract_Schedule_Identifier__c,'+
                            'Hours_Attended__c,'+
                            'Enrolment__r.Fee_Exemption__r.Code__c,' +
                            'Enrolment__r.Fee_Maintenance_Flag__c,' +                          
                            'Enrolment__r.Delivery_mode_type_state__c,' +
                            'Course_Commencement_Date__c,'+
                            'Eligibility_Exemption__c,'+ 
                            'VET_Fee_Help__c,'+
                            'ANZSIC_Code__c,'+
                            'Enrolment__c,' +
                            'Full_Time_Learning__c,'+
                            'Start_Date__c,'+
                            'End_Date__c,'+
                            'Study_Reason_Identifier__c,'+
                            'Unit__r.name,'+
                            'Qualification_Identifier__c,'+
                            'Has_Been_Superseded__c,' +
                            'Enrolment__r.Qualification__r.Parent_Qualification_Code__c,'+
                            'Enrolment__r.Qualification_Identifier__c,'+
                            'Enrolment__r.Start_Date__c,'+
                            'Enrolment__r.End_Date__c,'+
                            'Enrolment__r.Name,'+
                            'Unit__r.Unit_Flag_Identifier__c,'+
                            'Unit__r.Unit_Name__c,'+
                            'Student_Identifier__c,'+ 
                            'Enrolment__r.Qualification_Hours__c,'+
                            'Line_Item_Qualification_Unit__r.Nominal_Hours__c,'+
                            'Enrolment__r.Qualification__r.Parent_Qualification_Id__c,'+
                            'Enrolment__r.VET_FEE_HELP_Indicator__c,'+
                            'Enrolment__r.Victorian_Commencement_Date__c,'+
                            'Enrolment__r.Delivery_Location_State__c,'+
                            'Training_Delivery_Location__r.Training_Organisation_ABN__c,' +    
                            'Enrolment__r.Subcontracted_Training_Organisation__r.ABN__c,' + 
                            'Supersession_Count__c,' +  
                            'Superseded_1_EU_ID__c,' +
                            'Superseded_1_Unit_ID__c,' +
                            'Superseded_1_EU_End_Date__c,' +
                            'Superseded_2_EU_End_Date__c,' +
                            'Superseded_2_EU_ID__c,' +
                            'Superseded_2_Unit_ID__c,' +
                            'Superseded_3_EU_End_Date__c,' +
                            'Superseded_3_EU_ID__c,' +
                            'Superseded_3_Unit_ID__c,' +
                            'Superseded_4_EU_End_Date__c,' +
                            'Superseded_4_EU_ID__c,' +
                            'Superseded_4_Unit_ID__c,' +
                            'Superseded_5_EU_End_Date__c,' +
                            'Superseded_5_EU_ID__c,' +
                            'Superseded_5_Unit_ID__c,' +
                            'Superseded_6_EU_End_Date__c,' +
                            'Superseded_6_EU_ID__c,' +
                            'Superseded_6_Unit_ID__c,' +
                            'Victorian_Tuition_Rate__c,' +
                            'Enrolment__r.Enrolment_Category_Identifier__c,' +
                            'WA_Resource_Fee__c,' +
                            'Enrolment__r.Student__r.Education_Training_Client_Identifier__c,' +
                            'Total_Amount_Charged__c,' +
                            'Superseded_1_Unit_SFID__c,' +
                            'Superseded_2_Unit_SFID__c,' +
                            'Superseded_3_Unit_SFID__c,' +
                            'Superseded_4_Unit_SFID__c,' +
                            'Superseded_5_Unit_SFID__c,' +
                            'Superseded_6_Unit_SFID__c,' +
                            'LastModifiedDate,'+
                            'Enrolment__r.Commitment_Identifier__c,' +
                            'Enrolment__r.Enrolled_School_Identifier__c,' +
                            'Enrolment__r.Enrolment_Status__c,' +
                            'Enrolment__r.Training_Type_Identifier__c,' +
                            'Enrolment__r.Training_Plan_Developed__c,' +
                            'Enrolment__r.Booking_ID__c,' +
                            'Course_Site_Id__c,' +
                            'Enrolment__r.Final_Payment_Made__c,' +
                            'Client_Fees_Other__c,'+
                            'Enrolment__r.FundingEligibilityKey__c,' +         
                            'Enrolment__r.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c' ;
                           
        /*
        *   @history
        *   JEA 28.SEPT.2015 - modified the current variable in query - currentDTS
        *                    - updated the query to get all enrolments except the one which has final payment made equals true which is just remove Enrolment_Status__c LIKE \'%Active%\' filter from query
        *                    - updated enrolment query to get all enrolments irrespective of year selected, so we need to get all enrolments irrespective of year 
        *
        *   08.JAN.2015 Biao Zhang      Fetch the enrolment unit in collection year. No idea why previous change from JEA remove the year range. Regarding case 00036654. 
        */           
        if(reportType == 'State') {
            if(currentState == 'New South Wales' || currentState == 'New South Wales APL') {
                endQuery = ' FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Enrolment__r.Final_Payment_Made__c = false AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + ' AND Unit__r.RecordType.Name = \''+recTypeName+'\' AND CreatedDate <= ' + currentDTS;
            }   
            else {
                endQuery = ' FROM Enrolment_Unit__c WHERE (Enrolment__r.Report_for_AVETMISS_Type__c = \'State\' OR Enrolment__r.Report_for_AVETMISS_Type__c = null ) AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\' AND CreatedDate <=' + currentDTS + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) + ')';
                system.debug(endQuery);
            } 
        }
        if(reportType == 'National') {
            endQuery = ' FROM Enrolment_Unit__c WHERE Enrolment__r.Report_for_AVETMISS_Type__c = \'National\' AND Report_for_AVETMISS__c=true AND Purchasing_Contract_Identifier__c in ' +  pur_codes + '  AND Unit__r.RecordType.Name = \''+recTypeName+'\' AND CreatedDate <=' + currentDTS + ' AND (CALENDAR_YEAR(End_Date__c) = ' + Integer.valueof(currentYear.trim()) + ' OR CALENDAR_YEAR(Start_Date__c) = ' + Integer.valueof(currentYear.trim()) + ')';        
        }
        /* end JEA 28.SEPT.2015 */

         
        log.end_query__c = endQuery;
        
        queryStringMain += (endQuery + ' ORDER BY Enrolment__r.Student_Identifier__c Desc');   // JEA   28.SEPT.2015 - Added Order By Student identifier
        
        log.query_main__c = queryStringMain;
        
        /* using field sets, will finish this later but need to deploy ASAP 
         * 
         
        List<Schema.FieldSetMember> fields = SObjectType.Nat_Validation_Log__c.FieldSets.Query_Students.getFields(); 
        
        Integer queryLength = queryStringStudents.length();
        
        Long queryFieldsL = queryLength/max_query_length;
        
        Integer queryFields = queryFieldsL.intValue();
        
        Integer loopCount = 0;
        
        if(queryStringStudents.length() > max_query_length ) {
            for(Schema.FieldSetMember f : fields) { 
                loopCount++;
                
                if(loopCount == queryFields + 1) {
                   break;
                }
                
                if(loopCount == 1) {
                    log.put(f.getFieldPath(), queryStringStudents.substring(0, max_query_length * loopCount));
                }
                if( loopCount > 1) {
                   log.put(f.getFieldPath(), queryStringStudents.substring(max_query_length * (loopCount-1), max_query_length * loopCount));
                }
 
            }
        }
        else {
            log.query_students__c = queryStringStudents;
        } */
        
        //this will be cleaned up!
        
            String queryStringStudents2;
            String queryStringStudents3;
            String queryStringStudents4;
            String queryStringStudents5;
            String queryStringStudents6;        

            if(queryStringStudents.length() > max_query_length ) {
                log.query_students__c = queryStringStudents.Substring(0,max_query_length);
                queryStringStudents2 =  queryStringStudents.Substring(max_query_length);
                if(queryStringStudents2.length() > max_query_length) {
                    log.query_students_2__c = queryStringStudents2.Substring(0,max_query_length);
                    queryStringStudents3 =  queryStringStudents2.Substring(max_query_length);
                    if(queryStringStudents3.length() > max_query_length) {
                        log.query_students_3__c = queryStringStudents3.Substring(0,max_query_length);
                        queryStringStudents4 =  queryStringStudents3.Substring(max_query_length);
                            if(queryStringStudents4.length() > max_query_length) {
                                log.query_students_4__c = queryStringStudents4.Substring(0,max_query_length);
                                queryStringStudents5 =  queryStringStudents4.Substring(max_query_length);
                                if(queryStringStudents5.length() > max_query_length) {
                                    log.query_students_5__c = queryStringStudents5.Substring(0,max_query_length);
                                    queryStringStudents6 =  queryStringStudents5.Substring(max_query_length);
                                    log.query_students_6__c = queryStringStudents6.Substring(0,max_query_length);
                                }
                                else {
                                    log.Query_Students_5__c = queryStringStudents4;
                                }
                            }
                            else {
                                log.query_students_4__c = queryStringStudents4;
                            }
                        }
                        else {
                            log.query_students_3__c = queryStringStudents3;
                        }
                    }
                    else {
                        log.query_students_2__c = queryStringStudents2;
                    }
                }
            else {
                log.query_students__c = queryStringStudents;
            }
       
        
        log.RAPT_StudentQuery__c = queryStringStudentRAPT;
        log.RAPT_EnrolmentQuery__c = queryStringEnrolmentRAPT;
        //added List<ContentWorkspace> to stop failing of test classes JMDG 30.SEPT.2015
        //all soql has been changed JMDG 30.SEPT.2015
        //
        List<ContentWorkspace> c = new List<ContentWorkSpace>();
                
        if (currentState == 'Queensland') {
            c = [Select Name, Id From ContentWorkspace  WHERE Name = 'NAT Files QLD' LIMIT 1];
        }
        else if (currentState == 'New South Wales' || currentState == 'New South Wales APL') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files NSW' LIMIT 1];
        }
        else if (currentState == 'Australian Capital Territory') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files ACT' LIMIT 1];
        }
        else if(currentState == 'Victoria') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files VIC' LIMIT 1];
        }
        else if (currentState == 'South Australia') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files SA' LIMIT 1];
        }
        else if (currentState == 'Western Australia') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files WA' LIMIT 1];
        }
        else if(currentState == 'Northern Territory') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files NT' LIMIT 1];
        }
        else if(currentState == 'Tasmania') {
            c = [Select Name, Id From ContentWorkspace c WHERE Name = 'NAT Files TAS' LIMIT 1];
        }
        else if(currentState == 'National') {
            c = [Select Name, Id From ContentWorkspace WHERE Name = 'NAT Files National' LIMIT 1];
        }
        //get ContentWorkspace ID JMDG 30.SEPT.2015
        for(ContentWorkspace cWS : c){
            libraryId = cWS.id;
        }

        Account tOrgAct = [ SELECT ID, Name FROM Account WHERE ID = :tOrgId];
        
        log.RTO__c = tOrgAct.Name;

        log.Content_Library_Id__c = libraryId;
        
        log.Is_WA_RAPT__c = isWARAPT;
        
        log.Log_Run_At__c = currentDT;
        
        //Set correct log recordtype
        
        if(isWARAPT) // WA RAPT
            log.RecordTypeId = recTypes.get(1).Id;
        else //AVETMISS
        log.RecordTypeId = recTypes.get(0).Id;
        
        Insert log;
                    
        if(delOldFiles) {
            List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
            delete existingContent;
        }
        
        try {
            if(isWARAPT) {
                Database.executeBatch(new NATValidationBatch_WARAPTStudents( log.id ) ); //students batch will call the other batches
            }
            else {
                Database.executeBatch(new NATValidationBatch_Students( log.id ) ); //students batch will call the other batches
    
            }
        }
        catch (exception er) {
            log.System_Message__c = er.getMessage() + ' AT LINE: ' + er.getLineNumber();
        }
          
        Update log;
        
    }
    
}