@istest private class NATValidationWARAPTTests {

   
   public static testmethod void validateStudent() {
       
       List<RecordType> recTypes =  new List<RecordType>( [Select Id,developername From RecordType Where SObjectType = 'NAT_Validation_Log__c']);
       
       System.AssertEquals(recTypes.size(), 2, 'There must be 2 recordtypes available, Avetmiss and RAPT');
   
       Account tOrg = NATTestDataFactory.createTrainingOrganisation();
   
       NAT_Validation_Log__c log = new NAT_Validation_Log__c(Validation_State__c = 'Western Australia', Training_Organisation_Id__c = tOrg.Id, Collection_Year__c = '2014', Delete_Existing_NAT_Files__c = true);
       
       
       log.RAPT_StudentQuery__c = 'Select Student_Identifer__c, '+
                        'FirstName, '+
                        'LastName, '+
                        'PersonBirthDate, '+
                        'PersonMailingStreet, '+
                        'PersonMailingCity, ' +
                        'PersonMailingPostalCode, ' +
                        'PersonEmail, ' +
                        'Address_street_number__c, ' +
                        'Address_street_name__c, ' +
                        'Suburb_locality_or_town__c, ' +
                        'PersonMailingState, ' +
                        'Address_Post_Code__c, ' +
                        'Work_Phone__c, ' +
                        'PersonMobilePhone, ' +
                        'PersonHomePhone, ' +
                        'Postal_delivery_box__c, '+
                        'Postal_suburb_locality_or_town__c, ' +
                        'PersonOtherPostalCode, ' +
                        'Reporting_Other_State__r.Short_Name__c, ' +
                        'Country_of_Birth_Identifer__c, ' +
                        'Main_Language_Spoken_at_Home_Identifier__c, '+
                        'Main_Language_Spoken_at_Home__c, ' +
                        'Proficiency_in_Spoken_English_Identifier__c, ' +
                        'Indigenous_status_identifier__c, '+
                        'Highest_School_Level_Comp_Identifier__c, ' +
                        'Year_Highest_Education_Completed__c, ' +
                        'At_School_Flag__c, ' +
                        'Labour_Force_Status_Identifier__c, ' +       
                        'Study_Reason_Identifier__c, ' +       
                        'Full_Student_Name__c, ' +
                        'Prior_Achievement_Flag__c, ' +
                        'Prior_Achievement_Type_s__c, ' +
                        'LastModifiedDate, '+
                        'Sex_Identifier__c, ' +
                        'Disability_Type__c, ' +
                        'Disability_Flag_0__c ' +
                   'From Account';
                   
        log.Is_WA_RAPT__c = true;
       
        log.RecordTypeId = recTypes.get(1).Id;
       
        Account stu1 = NATTestDataFactory.createStudent();
        
        Account stu2 = NATTestDataFactory.createStudent();
        stu2.PersonBirthdate = null;
        update stu2;
        
        Account stu3 = NATTestDataFactory.createStudent();
        stu3.PersonMailingStreet = '123';
        update stu3;
        
        Account stu4 = NATTestDataFactory.createStudent();
        stu4.PersonMailingStreet = '123';
        stu4.PersonMailingCity = 'Brisbane';
        update stu4;
        
        Account stu5 = NATTestDataFactory.createStudent();
        stu5.PersonMailingStreet = '123';
        stu5.PersonMailingCity = 'Brisbane';
        stu5.PersonMailingPostalCode = '4211';
        update stu5;
        
        Account stu6 = NATTestDataFactory.createStudent();
        stu6.PersonMailingStreet = '123';
        stu6.PersonMailingCity = 'Brisbane';
        stu6.PersonMailingPostalCode = '4211';
        stu6.PersonMailingState = 'Queensland';
        update stu6;
        
        Country__c ctry = NATTestDataFactory.createCountry();
        
        Account stu7 = NATTestDataFactory.createStudent();
        stu7.PersonMailingStreet = '123';
        stu7.PersonMailingCity = 'Brisbane';
        stu7.PersonMailingPostalCode = '4211';
        stu7.PersonMailingState = 'Queensland';
        stu7.Country_of_Birth__c = ctry.Id;
        update stu7;
        
        Language__c lan = NATTestDataFactory.createLanguage();
        Account stu8 = NATTestDataFactory.createStudent();
        stu8.PersonMailingStreet = '123';
        stu8.PersonMailingCity = 'Brisbane';
        stu8.PersonMailingPostalCode = '4211';
        stu8.PersonMailingState = 'Queensland';
        stu8.Country_of_Birth__c = ctry.Id;
        stu8.Main_Language_Spoken_at_Home__c = lan.Id;
        update stu8;
        
        
        insert log;
        
        database.executeBatch( new NATValidationBatch_WARAPTStudents(log.Id) );
   
   }
    
    public static testmethod void validateEnrolment() {
        
        List<RecordType> recTypes =  new List<RecordType>( [Select Id,developername From RecordType Where SObjectType = 'NAT_Validation_Log__c']);
       
       	System.AssertEquals(recTypes.size(), 2, 'There must be 2 recordtypes available, Avetmiss and RAPT');
   
        Account tOrg = NATTestDataFactory.createTrainingOrganisation();
        
        NAT_Validation_Log__c log = new NAT_Validation_Log__c(Validation_State__c = 'Western Australia', Training_Organisation_Id__c = tOrg.Id, Collection_Year__c = '2014', Delete_Existing_NAT_Files__c = true);
       
 		log.RAPT_EnrolmentQuery__c = 'Select Id, ' +        
                              'Purchasing_Contract_Identifier__c,(Select Intake_Name__c FROM Enrolment_Intake_Units__r order by createddate desc LIMIT 1), ' +
                              'Unit__r.Name, ' +
                              'Unit__r.Unit_Name__c, ' +
                              'Unit_Code__c, ' +
                              'Unit_Name__c, ' +                              
                              'Scheduled_Hours__c, ' +
                              'Delivery_Strategy__r.Code__c, ' +
                              'Fee_Exemption_Identifier__c, ' +
                              'Unit_Result__r.Result_Code__c, ' +
                              'Enrolment__r.Start_Date__c, ' +
                              'Enrolment__r.End_Date__c, ' +
                              'Start_Date__c, ' +
                              'End_Date__c, ' +
                              'Enrolment__r.Delivery_Strategy__r.Code__c, ' +
                              'Reportable_Tuition_Fee__c, ' +
                              'Client_Fees_Other__c, ' +
                              'Total_Amount_Charged__c, ' +
                              'Training_Delivery_Location__r.Child_Suburb__c, ' +
                              'Training_Delivery_Location__r.Child_Postcode__c, ' +
                              'Enrolment__r.Student__r.TRS_Number__c, ' +
                              'Enrolment__r.Student__r.Student_Identifer__c, ' +
                              'Enrolment__r.Student__r.LastName, ' +
                              'Enrolment__r.Student__r.FirstName, ' +
                              'Enrolment__r.Student__r.PersonBirthDate, ' +
                              'Enrolment__r.Qualification_Issued__c, '+
                              'Enrolment__r.Name,'+
                              'Enrolment__r.Student__r.Unique_student_identifier__c, ' +
                              'LastModifiedDate '+
                              'FROM Enrolment_Unit__c';
        
                   
        log.Is_WA_RAPT__c = true;
       
        log.RecordTypeId = recTypes.get(1).Id;
        
        insert log;

        Account stu = NATTestDataFactory.createStudent();
        Country__c ctry = NATTestDataFactory.createCountry();
        ANZSCO__c anz = NATTestDataFactory.createANZSCO();
        
        Unit__c unt = NATTestDataFactory.createUnit();
        Unit__c unt2 = NATTestDataFactory.createUnit();
     
      
        Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();
        Field_of_Education__c foe2 = NATTestDataFactory.createFieldOfEducation();
        
        State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
        Results__c res = NATTestDataFactory.createResult(sta.Id);
        Results__c natResults =  NATTestDataFactory.createNationalResult();
        Results__c res2 = NATTestDataFactory.createResult(sta.Id);
        Results__c natResults2 =  NATTestDataFactory.createNationalResult();
        Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
        Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
        Locations__c ploc2 = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);

        Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
        Locations__c loc2 = NATTestDataFactory.createLocation(ctry.Id, ploc2.Id, tOrg.Id);
        
        Funding_Source__c fsrcNat = new Funding_Source__c( RecordTypeId = [Select Id,Name from RecordType where DeveloperName = 'National_Funding_Source' and sObjectType = 'Funding_Source__c'].Id,
                                                            Fund_Source_Name__c = 'test Source',
                                                            Code__c = '2334');
        insert fsrcNat;
        Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);

        Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
        Qualification__c qual2 = NATTestDataFactory.createQualification(anz.Id, foe.Id);
        
        Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
        Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
        Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
        Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
        Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
        Line_Item_Qualification_Units__c liqu2 = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
        Enrolment__c enrl = NATTestDataFactory.createEnrolment(stu.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
        Enrolment__c enrl2 = NATTestDataFactory.createEnrolment(stu.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
       
        Enrolment_Unit__c enu = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);
        Enrolment_Unit__c enu2 = NATTestDataFactory.createEnrolmentUnit(unt2.Id, enrl.Id, res.Id, loc2.Id, liqu.Id);
        Enrolment_Unit__c enu3 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl2.Id, res.Id, loc2.Id, liqu2.Id);

        Fee_Exemption_Types__c  fee2 = new Fee_Exemption_Types__c(Active__c = true, Code__c = '01', State__c = sta.Id );
        insert fee2;
        
        ploc2.Suburb__c = 'Sydney';
        
        update ploc2;
        
        Delivery_Mode_Types__c dmode = NATTestDataFactory.createDeliveryModeType();
        
        Delivery_Strategy__c  dstat = NATTestDataFactory.createDeliveryStrategy(dmode.Id);
        
        res.Result_Code__c = '3';
        
        update res;
        
        dstat.Code__c = '01';
        
        update dstat;
        
        enu3.Delivery_Strategy__c = dstat.Id;
                        
        update enu3;
        
        unt2.Unit_Name__c = 'blah';
        update unt2;
        
        enu2.Scheduled_Hours__c = 10;
        update enu2;
        
        enrl.Fee_Exemption__c = fee2.Id;
        update enrl;
        
                
        
        database.executeBatch( new NATValidationBatch_WARAPTEnrolments(log.Id) );
        

    }

}