/**
 * @description Controller for Generate Employer Statement page
 * @author Warjie Malibago
 * @date 06.FEB.2014
 *
 * HISTORY
 * - 06.FEB.2014    Warjie Malibago - Created.
 */
public with sharing class CommonwealthAssistanceNotice_CC{
    public Id enrolmentId {get; set;}
    public Enrolment__c Enrolment {get;set;}
    public Enrolment__c EnrolmentD {get;set;}
    List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
    public Double loanFee, upfront, amountCharged, HELPDebt;
    
    public CommonwealthAssistanceNotice_CC(){
        enrolmentId = ApexPages.currentPage().getParameters().get('eid');
        
        Enrolment = new Enrolment__c ();
        EnrolmentD = new Enrolment__c ();
        if(String.isNotEmpty(enrolmentId)){
            Enrolment = [Select id, Start_Date_Conga_2__c,End_Date_Conga_2__c,
                            Enrolment_Total_Loan_Fee_2__c, Enrolment_Amount_Paid_Upfront_2__c,
                            Enrolment_Tuition_Fees_2__c, Total_VFH_Enrolment_Debt_2__c 
                                From Enrolment__c 
                                    Where id =: enrolmentId];                                         
        }        
    }
    
    public void GenerateNotice(){
        euosList = new List<Enrolment_Unit_of_Study__c>();
        
        //Save Conga Start and End dates
        Enrolment.Start_Date_Conga_2__c = EnrolmentD.Start_Date_Conga_2__c;
        Enrolment.End_Date_Conga_2__c  = EnrolmentD.End_Date_Conga_2__c;
        update Enrolment;
        system.debug('**Start Date: ' + Enrolment.Start_Date_Conga_2__c);
        system.debug('**End Date: ' + Enrolment.End_Date_Conga_2__c);
        
        euosList = [SELECT Id, Census_Date__c,
                        Loan_Fee__c, Amount_paid_upfront__c,
                        Total_amount_charged__c, Unit_of_study_HELP_Debt_UI__c
                            FROM Enrolment_Unit_of_Study__c 
                                WHERE 
                                    Enrolment_Unit__r.Enrolment__c =: enrolmentId
                                    AND Census_Date__c >=: EnrolmentD.Start_Date_Conga_2__c
                                    AND Census_Date__c <=: EnrolmentD.End_Date_Conga_2__c];     
        
        loanFee = 0;
        upfront = 0;
        amountCharged = 0;
        HELPDebt = 0;                
        system.debug('**EUOS List size: ' + euosList.size());     
        if(euosList.size() > 0){
            for(Enrolment_Unit_of_Study__c euos : euosList){
                loanFee = loanFee + euos.Loan_Fee__c;
                upfront = upfront + euos.Amount_paid_upfront__c;
                amountCharged = amountCharged + euos.Total_amount_charged__c;
                HELPDebt = HELPDebt + euos.Unit_of_study_HELP_Debt_UI__c;
            }
            system.debug('**Loan Fee: ' + loanFee);
            system.debug('**Upfront: ' + upfront);
            system.debug('**Amount Charged: ' + amountCharged);
            system.debug('**HELP Debt: ' + HELPDebt);
            
            Enrolment.Enrolment_Total_Loan_Fee_2__c = loanFee;
            Enrolment.Enrolment_Amount_Paid_Upfront_2__c = upfront;
            Enrolment.Enrolment_Tuition_Fees_2__c = amountCharged;
            Enrolment.Total_VFH_Enrolment_Debt_2__c = HELPDebt;
            
            update Enrolment;
        }                           
    }

    public pageReference Cancel(){
        PageReference pageRef;
        if(String.IsEmpty(enrolmentId)){
            pageRef = new PageReference('/home/home.jsp');
        }
        else{
            pageRef = new PageReference('/'+ String.valueOf(enrolmentId).mid(0,15));
        }
        
        pageRef.setRedirect(true);
        return pageRef;
    }
}