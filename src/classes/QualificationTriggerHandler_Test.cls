/**
 */
@isTest(seeAllData=true)
private class QualificationTriggerHandler_Test {

    static testMethod void qualificationTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='QUAL_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='standuser5@testorg.com');
                    
        ANZSCO__c anzs = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qu = new Qualification_Unit__c();
        Qualification_Unit__c qu2 = new Qualification_Unit__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Enrolment__c enrol = new Enrolment__c();
        Account student = new Account();
        Line_Item_Qualifications__c liq = new Line_Item_Qualifications__c();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            student = TestCoverageUtilityClass.createStudent();
            anzs = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            Id recrdType = [select Id from RecordType where Name = 'Tailored Qualification' And SObjectType = 'Qualification__c' LIMIT 1].Id;
            qual2.RecordTypeId = recrdType;
            qual2.Name = 'Test Qualification';
            qual2.Qualification_Name__c = 'Test Qualification Name';
            qual2.ANZSCO__c = anzs.Id;
            qual2.Field_of_Education__c = fld.Id;
            qual2.Recognition_Status__c = '14 - Other Courses';    
            qual2.Award_based_on__c = qual.Id;
            qual2.DoNotAddQualUnits__c  = false;
            insert qual2;
            
            
            qu2 = TestCoverageUtilityClass.createQualificationUnit(qual2.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry(); 
            state = TestCoverageUtilityClass.createState(country.Id); 
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            liq = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(liq.Id, qu.Id);
            res = TestCoverageUtilityClass.createResult(state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType(); 
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrol = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, country.Id); 
            enrol.Predominant_Delivery_Mode__c = del.Id;
            enrol.Type_of_Attendance__c = attend.Id;
            enrol.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
            enrol.Start_Date__c = Date.TODAY() - 50;
            enrol.End_Date__c = Date.TODAY() + 50;
            insert enrol;
            
            eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrol.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qu.Id, fld.Id);
            insert euos;
            /*List<Enrolment_Unit_of_Study__c> eList = [Select Id, Course_of_Study_type_code__c, Qualification_Unit__r.Qualification__c
                                                        FROM Enrolment_Unit_of_Study__c
                                                        WHERE Qualification_Unit__r.Qualification__c =: qual.Id];*/
            //qual.Award_based_on__c = qual.Id;
            update qual;
            test.stopTest();
        }  
    }

    static testMethod void updateDisciplineCode() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='QUAL_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='standuser6@testorg.com');
                    
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Field_of_Education__c fld2 = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qu = new Qualification_Unit__c();
        Qualification_Unit__c qu2 = new Qualification_Unit__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Enrolment__c enrol = new Enrolment__c();
        Account student = new Account();
        Line_Item_Qualifications__c liq = new Line_Item_Qualifications__c();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Results__c res = new Results__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Course_of_Study_Type__c cost = new Course_of_Study_Type__c();
        system.runAs(u){
            test.startTest();
            country = TestCoverageUtilityClass.createCountry(); 
            unit = TestCoverageUtilityClass.createUnit();
            student = TestCoverageUtilityClass.createStudent();
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            fld2 = TestCoverageUtilityClass.createFieldOfEducation();
            Id recrdType = [select Id from RecordType where DeveloperName = 'Field' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
            fld2.RecordTypeId = recrdType;
            update fld2;
            cost.Name = 'Course of Study Type Test Name'; 
            cost.code__c = 'Code Test';
            insert cost;
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            
            state = TestCoverageUtilityClass.createState(country.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id);
            liq = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
            
            enrol = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, country.Id); 
            enrol.Predominant_Delivery_Mode__c = del.Id;
            enrol.Type_of_Attendance__c = attend.Id;
            enrol.Enrolment_Status__c = 'Active (Commencement)';
            enrol.Start_Date__c = Date.TODAY() - 50;
            enrol.End_Date__c = Date.TODAY() + 50;
            insert enrol;
            res = TestCoverageUtilityClass.createResult(state.Id);
            
            qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(liq.Id, qu.Id);
            eu = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrol.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            
            euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qu.Id, fld.Id);
            euos.Course_of_Study_type_code__c = 'Test Code';
            insert euos;

            qual.Discipline_code__c = fld2.Id;
            qual.Course_of_Study_Type__c = cost.Id;
            update qual;

            system.debug('##asdasd: ' +qu.Qualification__c);
            test.stopTest();
        }  
    }
}