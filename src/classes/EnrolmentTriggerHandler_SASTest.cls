/**
 * @description Test class for EnrolmentTriggerHandler (SAS Components)
 * @author Ranyel Maliwanag
 * @date 07.JUL.2015
 * @history
 *      11.DEC.2015 Biao Zhang          Create service case test
 */
@isTest
public with sharing class EnrolmentTriggerHandler_SASTest {
    @testSetup
    static void triggerSetup() {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User adminUser = SASTestUtilities.createUser();
        adminUser.ProfileId = adminProfile.Id;
        adminUser.Username = 'adminUser@casis.org';
        adminUser.Email = 'adminUser@casis.org';
        insert adminUser;


        User casisUser = SASTestUtilities.createUser();
        casisUser.ProfileId = adminProfile.Id;
        casisUser.Username = 'casisUser@casis.org';
        casisUser.Email = 'casisUser@casis.org';
        insert casisUser;

        System.runAs(adminUser) {
            CentralTriggerSettings__c casisSettings = SASTestUtilities.createCentralTriggerSettings();
            casisSettings.SetupOwnerId = casisUser.Id;
            casisSettings.Enrolment_UnmarkForGLSSync__c = true;
            casisSettings.Account_UnmarkForGLSSync__c = true;
            casisSettings.ServiceCase_UnmarkForGLSSync__c = true;
            casisSettings.Task_UnmarkForGLSSync__c = true;
            insert casisSettings;

                    
            Account student = SASTestUtilities.createStudentAccount();
            insert student;

            Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
            enrolment.Assigned_Trainer__c = UserInfo.getUserId();
            insert enrolment;

            Id onlineServiceCaseRecordType =  Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Online Service Case').getRecordTypeId();
            Service_Cases__c serviceCase = new Service_Cases__c(
                    RecordTypeId = onlineServiceCaseRecordType,
                    Enrolment__c = enrolment.Id,
                    Account_Student_Name__c = student.Id
                );
            insert serviceCase;

            Task taskRecord = new Task(
                    WhatId = enrolment.Id,
                    Contact_Type__c = 'Contacted',
                    Contact_Outcome__c = 'Call Back',
                    Subject = 'Call'
                );
            insert taskRecord;
        }
    }

    static testMethod void unmarkForGLSSync() {
        Test.startTest();
            Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
            enrolment.Managed_by_GLS__c = true;
            enrolment.Ready_for_GLS_Sync__c = true;
            update enrolment;
            User apiUser = [SELECT Id FROM User WHERE Email = 'casisUser@casis.org'];
            
            enrolment = [SELECT Id, Managed_by_GLS__c, Ready_for_GLS_Sync__c, GLS_Enrolment_ID__c FROM Enrolment__c WHERE Id = :enrolment.Id];
            System.assert(String.isBlank(enrolment.GLS_Enrolment_ID__c));

            System.runAs(apiUser) {
                enrolment.GLS_Enrolment_ID__c = 'GLS0001';
                update enrolment;

                enrolment = [SELECT Id, GLS_Enrolment_ID__c, Ready_for_GLS_Sync__c FROM Enrolment__c WHERE Id = :enrolment.Id];
                System.assert(String.isNotBlank(enrolment.GLS_Enrolment_ID__c));
            }
        Test.stopTest();
    }

    static testMethod void markScheduledUnitsForSync() {
        Enrolment__c student = [SELECT Id FROM Enrolment__c];
        Test.startTest();
            // Student gets ticked as an InPlace student
            student.IPS_InPlace_Student__c = true;
            update student;

            // Assertion: student gets ticked for sync as well
            student = [SELECT Id, IPS_Sync_on_next_update__c, (SELECT Id, IPSUO_InPlace_Unit_Offering_a__c, IPSUO_Sync_on_next_update__c FROM Enrolment_Intake_Units__r WHERE IPSUO_InPlace_Unit_Offering_a__c = true) FROM Enrolment__c];
            System.assert(student.IPS_Sync_on_next_update__c);

            // Assertion: all enrolment intake units that have inplace unit offering flag on should be marked for sync
        Test.stopTest();
    }

    /**
     * @description Test service case createion for suspension non-progression enorlment
     * @author Biao Zhang
     * @date 07.DEC.2015
     */ 
    static testMethod void TestServiceCaseCreation() {
        Enrolment__c enrolment = [SELECT Id, Enrolment_Status__c FROM Enrolment__c];
        enrolment.Enrolment_Status__c = 'Suspended from C1';
        update enrolment;
        Service_Cases__c[] sc = [select id from Service_Cases__c where Enrolment__c = :enrolment.Id and Type__c like 'Online – Census 1 Suspension'];
        system.assertEquals(sc.size(), 1);    

        enrolment.Enrolment_Status__c = 'Suspended from C2';
        update enrolment;
        sc = [select id from Service_Cases__c where Enrolment__c = :enrolment.Id and Type__c like 'Online – Census 2 Suspension'];
        system.assertEquals(sc.size(), 1); 

        enrolment.Enrolment_Status__c = 'Suspended from C3';
        update enrolment;
        sc = [select id from Service_Cases__c where Enrolment__c = :enrolment.Id and Type__c like 'Online – Census 3 Suspension'];
        system.assertEquals(sc.size(), 1); 

        enrolment.Enrolment_Status__c = 'Suspended from S2';
        update enrolment;
        sc = [select id from Service_Cases__c where Enrolment__c = :enrolment.Id and Type__c like 'Online – Stage 2 Suspension'];
        system.assertEquals(sc.size(), 1); 

        enrolment.Enrolment_Status__c = 'Suspended from S3';
        update enrolment;
        sc = [select id from Service_Cases__c where Enrolment__c = :enrolment.Id and Type__c like 'Online – Stage 3 Suspension'];
        system.assertEquals(sc.size(), 1);     
    } 


    static testMethod void enrolledStudentIdentifierTest() {
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
        Enrolment__c enrolment = [SELECT Id, Enrolled_School_Identifier__c, Enrolled_School_Identifier1__c FROM Enrolment__c];
        ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();

        GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

        ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;

        EntryRequirement__c req = new EntryRequirement__c(
            RecordTypeId = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId(),
            ApplicationFormId__c = appform.Id,
            EnrolmentId__c = enrolment.Id,
            Status__c = 'LLN - Pending'
        );

        appForm = [SELECT Id, Name FROM ApplicationForm__c WHERE Id = :appForm.Id];

        Test.startTest();
            System.assert(String.isBlank(enrolment.Enrolled_School_Identifier__c));
            System.assert(String.isBlank(enrolment.Enrolled_School_Identifier1__c));

            enrolment.Enrolled_School_Identifier1__c = 'HARP';
            enrolment.Form_Number__c = appForm.Name;
            enrolment.Assigned_Trainer__c = UserInfo.getUserId();
            update enrolment;

            enrolment = [SELECT Id, Enrolled_School_Identifier__c, Enrolled_School_Identifier1__c FROM Enrolment__c];
            System.assert(String.isNotBlank(enrolment.Enrolled_School_Identifier__c));
            System.assert(String.isNotBlank(enrolment.Enrolled_School_Identifier1__c));
            System.assertEquals(enrolment.Enrolled_School_Identifier1__c, enrolment.Enrolled_School_Identifier__c);
        Test.stopTest();
    }
}