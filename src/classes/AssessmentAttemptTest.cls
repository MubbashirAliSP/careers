@istest public class AssessmentAttemptTest {
    
    
    public static testmethod void testAssessmentAttemptPositive() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
                    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test'); 
        system.runAs(u) {
        Account student = TestCoverageUtilityClass.createStudent();
        
        ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
        Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
        Qualification__c qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        Country__c country = TestCoverageUtilityClass.createCountry();
        State__c state = TestCoverageUtilityClass.createState(country.Id);
        Location_Loadings__c loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        Locations__c  parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
        Locations__c   loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
        Enrolment__c  enrl = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, null, loc.Id, country.Id);
        enrl.Assigned_Trainer__c = u.Id;
        insert enrl;
        Program__c program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
        Funding_Stream_Types__c fundStream = TestCoverageUtilityClass.createFundingStream();
        Intake__c intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
        Intake_Students__c intake_student = new Intake_Students__c(

            Intake__c = intk.Id,
            Enrolment__c = enrl.Id,
            Student__c = student.Id

        );

        insert intake_student;
            
        Census__c census = new Census__c(
                EnrolmentId__c = enrl.Id,
                IntakeId__c = intk.Id,
                CensusDate__c = Date.today().addDays(-100),
                IsManualProgression__c = true
            );
            
        insert census;
        
        PageReference pageRef = Page.AssessmentAttempt;
		Test.setCurrentPage(pageRef);
		
        //Add Intake ID parameter
        ApexPages.currentPage().getParameters().put('id', intk.Id);
        
        AssessmentAttemptController controller = new AssessmentAttemptController();
        AssessmentAttempt__c attempt = new AssessmentAttempt__c(SubmittedDate__c = system.Today(), BBAssessmentId__c = 'Assessment Item', BBCourseId__c = 'test unit');
        AssessmentAttemptController.AssessmentAttemptWrapper cmp = new AssessmentAttemptController.AssessmentAttemptWrapper(true,intake_student, attempt);
        
        controller.students.add(cmp);
        
        test.StartTest();
          controller.submit();
        test.stopTest();    
        
                
        System.assertEquals([Select AccountId__c From AssessmentAttempt__c].AccountId__c, student.Id,'Student Id Was Not Properly Set');
        System.assertEquals([Select BBCourseId__c From AssessmentAttempt__c].BBCourseId__c, 'test unit','Course Id Was Not Properly Set');
        System.assertEquals([Select BBAssessmentId__c From AssessmentAttempt__c].BBAssessmentId__c, 'Assessment Item','Assessment Id Was Not Properly Set');
        System.assertEquals([Select SubmittedDate__c From AssessmentAttempt__c].SubmittedDate__c, System.today(),'Submitted Date Is Not Correct');
        
        controller.cancel();

        } 
    }
    
    public static testmethod void testAssessmentAttemptNoneSelected() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
                    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u) {
        
        Account student = TestCoverageUtilityClass.createStudent();
        
        ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
        Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
        Qualification__c qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        Country__c country = TestCoverageUtilityClass.createCountry();
        State__c state = TestCoverageUtilityClass.createState(country.Id);
        Location_Loadings__c loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        Locations__c  parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
        Locations__c   loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
        Enrolment__c  enrl = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, null, loc.Id, country.Id);
        enrl.Assigned_Trainer__c = u.Id;
        insert enrl;
        Program__c program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
        Funding_Stream_Types__c fundStream = TestCoverageUtilityClass.createFundingStream();
        Intake__c intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
        Intake_Students__c intake_student = new Intake_Students__c(

            Intake__c = intk.Id,
            Enrolment__c = enrl.Id,
            Student__c = student.Id

        );

        insert intake_student;
        
        Census__c census = new Census__c(
                EnrolmentId__c = enrl.Id,
                IntakeId__c = intk.Id,
                CensusDate__c = Date.today().addDays(-100),
                IsManualProgression__c = true
            );
            
        insert census;
        
        PageReference pageRef = Page.AssessmentAttempt;
		Test.setCurrentPage(pageRef);
		
        //Add Intake ID parameter
        ApexPages.currentPage().getParameters().put('id', intk.Id);
        
        AssessmentAttemptController controller = new AssessmentAttemptController();
        
        System.runAs(u) {
        
        test.StartTest();
          controller.submit();
        test.stopTest();
        }
        //Assert that no students have been selected
        List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		 for(Apexpages.Message msg:msgs){
    		if (msg.getDetail().contains('Please Select At Least 1 Student')) b = true;
		  }
		system.assert(b);
        
        }
        
    }
    
    public static testmethod void testAssessmentAttemptFieldsNotFilledIn() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
                    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Intake_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u) {
        
        Account student = TestCoverageUtilityClass.createStudent();
        
        ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
        Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
        Qualification__c qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        Country__c country = TestCoverageUtilityClass.createCountry();
        State__c state = TestCoverageUtilityClass.createState(country.Id);
        Location_Loadings__c loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        Locations__c  parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
        Locations__c   loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
        Enrolment__c  enrl = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, null, loc.Id, country.Id);
        enrl.Assigned_Trainer__c = u.Id;
        insert enrl;
        Program__c program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
        Funding_Stream_Types__c fundStream = TestCoverageUtilityClass.createFundingStream();
        Intake__c intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
        Intake_Students__c intake_student = new Intake_Students__c(

            Intake__c = intk.Id,
            Enrolment__c = enrl.Id,
            Student__c = student.Id

        );

        insert intake_student;
        
        Census__c census = new Census__c(
                EnrolmentId__c = enrl.Id,
                IntakeId__c = intk.Id,
                CensusDate__c = Date.today().addDays(-100),
                IsManualProgression__c = true
            );
            
        insert census;
        
        PageReference pageRef = Page.AssessmentAttempt;
		Test.setCurrentPage(pageRef);
		
        //Add Intake ID parameter
        ApexPages.currentPage().getParameters().put('id', intk.Id);
        
        AssessmentAttemptController controller = new AssessmentAttemptController();
        AssessmentAttempt__c attempt = new AssessmentAttempt__c(BBAssessmentId__c = 'Assessment Item', BBCourseId__c = 'test unit');
        AssessmentAttemptController.AssessmentAttemptWrapper cmp = new AssessmentAttemptController.AssessmentAttemptWrapper(true,intake_student, attempt);
        
        controller.students.add(cmp);
        
        
        
        test.StartTest();
          controller.submit();
        test.stopTest();
        
        //Assert that no students have been selected
        List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		 for(Apexpages.Message msg:msgs){
    		if (msg.getDetail().contains('Please Fill In All Mandatory Fields')) b = true;
		  }
		system.assert(b);
        
        }
        
    }
}