/**
 * @description Custom component controller for `SEC_ListView` component
 * @author Ranyel Maliwanag
 * @date 11.JAN.2016
 */
public without sharing class SEC_ListViewCC {
	// The actual container object from the Student Engagement Console controller
	public SEC_Container__mdt container {
		get;
		set {
			System.debug('::: containerReset ::: ' + value);
			container = value;
			//currentListView = null;
			fieldsetMembers = null;
			ssc = null;
			recordCategories = null;
			recordsMap = null;
			sortedList = null;
			sorter = null;
		}
	}

	public SEC_Container__mdt listContainer {
		get {
			return container;
		}
	}

	// The list of list views that will be displayed in this container
	public List<SEC_ListView__mdt> listViews {
		get; 
		set {
			listViews = value;
			if (currentListView.SEC_Container__c != container.DeveloperName) {
				currentListView = null;
			}
		}
	}

	public String oldTab {get; set;}

	// To keep track of the number of list views
	public Boolean isMultipleListViews {
		get {
			return listViews.size() > 1;
		}
	}

	// To keep track of the current list view and its properties
	public SEC_ListView__mdt currentListView {
		get {
			if (currentListView == null) {
				currentListView = listViews[0];
				if (String.isNotBlank(currentListViewId)) {
					for (SEC_ListView__mdt lv : listViews) {
						if (lv.Id == currentListViewId) {
							currentListView = lv;
						}
					}
				}
			}
			return currentListView;
		}
		set;
	}

	public Id currentListViewId {
		get {
			if (ApexPages.currentPage().getParameters().containsKey('lid')) {
				currentListViewId = ApexPages.currentPage().getParameters().get('lid');
			}
			return currentListViewId;
		}
		set;
	}

	// For displaying the SObject name on the list view headers
	public String objectName {
		get {
			return Schema.getGlobalDescribe().get(currentListView.SEC_SObjectName__c).getDescribe().getLabelPlural();
		}
	}

	// The list of Fieldset members that will represent a column to be displayed on the list view data table
	public List<Schema.FieldSetMember> fieldsetMembers {
		get {
			if (fieldsetMembers == null) {
				System.debug('::: sortingfield reset');
				Schema.FieldSet fieldSet = null;
				// Validate that the SObject name configured in the metadata is valid
				if (Schema.getGlobalDescribe().containsKey(currentListView.SEC_SObjectName__c)) {
					// Validate that the Fieldset name exists for the configured SObject in the meta
					if (Schema.getGlobalDescribe().get(currentListView.SEC_SObjectName__c).getDescribe().fieldSets.getMap().containsKey(currentListView.SEC_FieldsetName__c)) {
						// Fetch the actual Fieldset record if it exists
						fieldSet = Schema.getGlobalDescribe().get(currentListView.SEC_SObjectName__c).getDescribe().fieldSets.getMap().get(currentListView.SEC_FieldsetName__c);
					}
				}
				if (fieldSet != null) {
					fieldsetMembers = fieldSet.getFields();
				}
			}
			return fieldsetMembers;
		}
		set;
	}

	public Id currentTrainer {get; set;}
	private Id oldTrainer {get; set;}
	private String oldListView {get; set;}
	public String newListView {get; set;}
	private List<String> validStatuses {
		get {
			return new List<String> {
				'Active%'
				//'Suspended%'
			};
		}
	}

	// The list of records that will be displayed on the list view data table
	public List<SObject> currentListViewRecords {
		get {
			Boolean isChanged = currentTrainer != oldTrainer || currentListView.DeveloperName != oldListView;
			oldListView = currentListView.DeveloperName;
			oldTrainer = currentTrainer;
			if (isChanged) {
				ssc = null;
				recordCategories = null;
				recordsMap = null;
				sortingField = null;
				sortAscending = null;
			}
			System.debug('currentListView ::: ' + currentListView);
			if (currentListViewRecords == null || isChanged) {
				// Set up the set of fields that needs to be queried for this specific data table
				Set<String> fieldsForQuery = new Set<String>();
				for (Schema.FieldSetMember fsm : fieldsetMembers) {
					fieldsForQuery.add(fsm.getFieldPath());

					// Special handling for reference fields: include the name in the query
					if (fsm.getType() == Schema.DisplayType.Reference && fsm.getFieldPath().endsWith('__c')) {
						fieldsForQuery.add(fsm.getFieldPath().replace('__c', '__r') + '.Name');
					}
				}
				// Ensure that the Id field is included in the query, in case it's not included in the fieldset
				if (!fieldsForQuery.contains('Id')) {
					fieldsForQuery.add('Id');
				}
				if (!fieldsForQuery.contains('Name')) {
					fieldsForQuery.add('Name');
				}
				// Additional query for categorisation
				if (isCategorised && currentListView.SEC_CategorisationSource__c == 'Record' && String.isNotBlank(currentListView.SEC_CategorisationField__c) && !fieldsForQuery.contains(currentListView.SEC_CategorisationField__c)) {
					fieldsForQuery.add(currentListView.SEC_CategorisationField__c);
				}
				// Additional query for Enrolment actions
				if (currentListView.SEC_SObjectName__c == 'Enrolment__c') {
					fieldsForQuery.add('Student__r.PersonContactId');
					fieldsForQuery.add('Student__r.TrainerCategory__c');
					fieldsForQuery.add('Student__r.Blackboard_UserID__c');
				} else if (currentListView.SEC_SObjectName__c == 'AssessmentAttempt__c') {
					fieldsForQuery.add('AccountId__r.PersonContactId');
					fieldsForQuery.add('AccountId__r.Blackboard_UserID__c');
					fieldsForQuery.add('BB_Course_PK1__c');
					fieldsForQuery.add('courseMembershipId__c');
					fieldsForQuery.add('outcomeDefinitionId__c');
				}

				// Build the query string based on the fields defined on the fieldset
				String queryString = 'SELECT ' + String.join(new List<String>(fieldsForQuery), ', ');
				queryString += ' FROM ' + currentListView.SEC_SObjectName__c;
				if (String.isNotBlank(currentListView.SEC_ListViewFilter__c)) {
					queryString += ' WHERE ' + currentListView.SEC_ListViewFilter__c;
				}
				if (String.isNotBlank(currentListView.FilterOptionsLabel__c)) {
					String labels = Label.AA_list_view_filter_options;
					List<String> exclusionList = labels.split(',');
					System.debug('exclusionList ::: ' + exclusionList);
					queryString += ' AND (NOT BBAssessmentId__c LIKE :exclusionList) ';
				}
				//if (String.isNotBlank(currentListView.Extra_Filter_Options__c)) {
				//	queryString += currentListView.Extra_Filter_Options__c;
				//}
				// @TODO This is an arbitrary limit, need to transfer to metadata configuration
				queryString += ' LIMIT 1000';
				System.debug('queryString ::: ' + queryString);
				currentListViewRecords = Database.query(queryString);
			}
			return currentListViewRecords;
		}
		set;
	}

	// Reference map of SObject Ids and their associated key-value pair reference map for each field defined on the fieldset meta
	public Map<Id, Map<String, String>> recordsMap {
		get {
			if (recordsMap == null) {
				recordsMap = new Map<Id, Map<String, String>>();

				// Go over the list of records that were queried for this list view
				for (SObject record : currentListViewRecords) {
					Map<String, String> recordValues = new Map<String, String>();

					// Fetch the value for each field on the fieldset for the current record in the iteration. Blank values are replaced with '---' as a fix for the null value error when accessing map keys with no values
					for (Schema.FieldSetMember fsm : fieldsetMembers) {
						recordValues.put(fsm.getFieldPath(), EnrolmentsSiteUtility.getReferenceFieldValue(record, fsm.getFieldPath()) != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, fsm.getFieldPath())) : '---');
					} 

					if (currentListView.SEC_SObjectName__c == 'Enrolment__c') {
						recordValues.put('Student__r.PersonContactId', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.PersonContactId') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.PersonContactId')) : '---');
						recordValues.put('Student__r.Blackboard_UserID__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.Blackboard_UserID__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.Blackboard_UserID__c')) : '');
						recordValues.put('Student__r.TrainerCategory__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.TrainerCategory__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'Student__r.TrainerCategory__c')) : '');
					}
					if (currentListView.SEC_SObjectName__c == 'AssessmentAttempt__c') {
						recordValues.put('AccountId__r.PersonContactId', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'AccountId__r.PersonContactId') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'AccountId__r.PersonContactId')) : '---');
						recordValues.put('AccountId__r.Blackboard_UserID__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'AccountId__r.Blackboard_UserID__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'AccountId__r.Blackboard_UserID__c')) : '');
						recordValues.put('BB_Course_PK1__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'BB_Course_PK1__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'BB_Course_PK1__c')) : '---');
						recordValues.put('courseMembershipId__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'courseMembershipId__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'courseMembershipId__c')) : '---');
						recordValues.put('outcomeDefinitionId__c', EnrolmentsSiteUtility.getReferenceFieldValue(record, 'outcomeDefinitionId__c') != null ? String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, 'outcomeDefinitionId__c')) : '');
					}
					recordsMap.put(record.Id, recordValues);
				} 
			}
			return recordsMap;
		}
		set;
	}

	// For displaying the total number of records displayed on the list view
	public Integer resultSize {
		get {
			return currentListViewRecords.size();
		}
	}

	// Standard set controller
	public ApexPages.StandardSetController ssc {
		get {
			if (ssc == null) {
				Integer pageSize = 10;
				if (currentListView.SEC_Size__c != null) {
					pageSize = Integer.valueOf(currentListView.SEC_Size__c);
				}
				ssc = new ApexPages.StandardSetController(sortedList);
				ssc.setPageSize(pageSize);
			}
			return ssc;
		}
		set;
	}

	public List<SObject> currentPageRecords {
		get {
			return ssc.getRecords();
		}
	}

	// Reference variable to identify if categorisation is applicable
	public Boolean isCategorised {
		get {
			isCategorised = String.isNotBlank(currentListView.SEC_Categorisation__c);
			return isCategorised;
		}
		set;
	}

	public Map<Id, String> recordCategories {
		get {
			if (isCategorised && recordCategories == null) {
				recordCategories = new Map<Id, String>();
				Boolean isRecordSourced = currentListView.SEC_CategorisationSource__c == 'Record';
				Boolean isQuerySourced = currentListView.SEC_CategorisationSource__c == 'Query';

				for (SObject record : currentListViewRecords) {
					String category = 'white';
					if (isRecordSourced && EnrolmentsSiteUtility.getReferenceFieldValue(record, currentListView.SEC_CategorisationField__c) != null) {
						category = String.valueOf(EnrolmentsSiteUtility.getReferenceFieldValue(record, currentListView.SEC_CategorisationField__c)).toLowerCase();
					}
					category = category.replace(' ', '-').toLowerCase();
					recordCategories.put(record.Id, category);
				}
			}
			return recordCategories;
		}
		set;
	}

	public Task newTask {
		get {
			if (newTask == null) {
				newTask = new Task(
					OwnerId = UserInfo.getUserId(),
					RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
					Subject = 'Call',
					WhoId = taskWhoId,
					WhatId = taskWhatId,
					Status = 'Completed',
					Priority = 'Normal',
					ActivityDate = Date.today()
				);
			}
			return newTask;
		}
		set;
	}

	public Id taskWhoId {get; set;}
	public Id taskWhatId {get; set;}
	public Id accountId {get; set;}
	public String defaultCategory {get; set;}
	public Boolean isCallModalVisible {get; set;}
	public Boolean isCategoryModalVisible {get; set;}

	// Private copy of the global describe map for this class
	//private transient Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
	public Account accountForCategorisation {get; set;}
	public Enrolment__c referenceEnrolment {get; set;}
	public Id enrolmentId {get; set;}
	public String debugger {get; set;}
	public String firstContactStatus {get;set;}


	public PageReference saveNewTask() {
		insert newTask;
		taskWhatId = null;
		taskWhoId = null;
		newTask = null;
		return null;
	}


	public PageReference saveAccountCategory() {
		System.debug('::: saveAccountCategory ::: ' + currentListView);
		update accountForCategorisation;
		System.debug('accountForCategorisation ::: ' + accountForCategorisation);
		
		referenceEnrolment.Id = enrolmentId;
		update referenceEnrolment;
		System.debug('referenceEnrolment ::: ' + referenceEnrolment);

		if (isCategorised && recordCategories != null) {
			recordCategories.put(enrolmentId, String.isNotBlank(accountForCategorisation.TrainerCategory__c) ? accountForCategorisation.TrainerCategory__c.replace(' ', '-').toLowerCase() : 'white');
		}
		debugger = accountForCategorisation.TrainerCategory__c;
		currentListViewRecords = null;
		recordCategories = null;
		return resetCategoryModal();
	}


	public PageReference resetCategoryModal() {
		accountForCategorisation = null;
		referenceEnrolment = null;
		accountId = null;
		return null;
	}


	public PageReference initialiseAccountCategory() {
		System.debug('::: initialiseAccountCategory');
		accountForCategorisation = [SELECT Id, TrainerCategory__c FROM Account WHERE Id = :accountId];
		referenceEnrolment = new Enrolment__c(
				Student__c = accountId
			);
		return null;
	}


	public PageReference initialiseLogCall() {
		newTask = null;
		return null;
	}


	public PageReference switchListViews() {
		System.debug('::: switchListViews called');
		// Update current list view based on new list view parameter
		for (SEC_ListView__mdt listView : listViews) {
			if (newListView == listView.DeveloperName) {
				System.debug('::: listView Found');
				currentListView = listView;
				ssc = null;
				recordCategories = null;
				recordsMap = null;
				currentListViewRecords = null;
				break;
			}
		}
		System.debug('::: iterationEnd');
		return null;
	}

	public AP_SortHelper sorter {
		get {
			if (sorter == null) {
				sorter = new AP_SortHelper();
				sorter.originalList = currentListViewRecords;
			}
			return sorter;
		}
		set;
	}

	public String sortingField {
		get {
			if (sortingField == null) {
				if (String.isBlank(currentListView.DefaultSortField__c)) {
					sortingField = 'Name';
				} else {
					sortingField = currentListView.DefaultSortField__c;
				}
			}
			return sortingField;
		}
		set {
			sortingField = value;
			System.debug('::: setting SortingField value to ' + value);
		}
	}

	public Boolean sortAscending {
		get {
			if (sortAscending == null) {
				sortAscending = true;
			}
			return sortAscending;
		}
		set;
	}

	String tempSort {get; set;}


	public List<SObject> sortedList {
		get {
			if (sortedList == null) {
				if (tempSort != null) {
					sortedList = sorter.getSortedList(tempSort, sortAscending);
				} else {
					sortedList = sorter.getSortedList(sortingField, sortAscending);
				}
			}
			return sortedList;
		}
		set {
			sortedList = value;
			ssc = null;
		}
	}

	public PageReference next() {
		ssc.next();
		return null;
	}


	public PageReference sortBy() {
		System.debug('sortingField ::: ' + sortingField);
		System.debug('sortAscending ::: ' + sortAscending);
		String localTempSort = sortingField;
		for (Schema.FieldSetMember fsm : fieldsetMembers) {
			if (fsm.getFieldPath() == sortingField && String.valueOf(fsm.getType()) == 'reference') {
				if (localTempSort.endsWith('__c')) {
					localTempSort = localTempSort.replace('__c', '__r') + '.Name';
				} else if (localTempSort.endsWith('Id')) {
					localTempSort += '.Name';
				}
			}
		}
		if (localTempSort != sortingField) {
			tempSort = localTempSort;
		} else {
			tempSort = null;
		}
		sortedList = sorter.getSortedList(localTempSort, sortAscending);
		return null;
	}

	public String currentPageUrl {
		get {
			PageReference reference = ApexPages.currentPage();
			reference.getParameters().remove('lid');
			return reference.getUrl();
		}
	}
}