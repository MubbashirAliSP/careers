/**
 * @description Test class for `SEC_TaskViewCC`
 * @author Ranyel Maliwanag
 * @date 23.FEB.2016
 */
@isTest
public with sharing class SEC_TaskViewCC_Test {
	@testSetup
	static void setup() {
		Task testTask = new Task(
				OwnerId = UserInfo.getUserId()
			);
		insert testTask;
	}


	static testMethod void accessorTest() {
		SEC_TaskViewCC controller = new SEC_TaskViewCC();
		Task record = [SELECT Id FROM Task];
		Test.startTest();
			controller.currentTrainer = UserInfo.getUserId();
			controller.currentTaskId = record.Id;

			System.assertNotEquals(null, controller.myTasks);
			System.assert(!controller.myTasks.isEmpty());
			System.assertNotEquals(null, controller.listViewQueryList);
			System.assertNotEquals(null, controller.listViewQueryMap);
			System.assertNotEquals(null, controller.taskMap);
			System.assertNotEquals(null, controller.currentTask);
			System.assertNotEquals(null, controller.newTask);
			System.assertNotEquals(null, controller.attachments);
			System.assertEquals(null, controller.displayCommentModal);
			System.assertEquals(null, controller.isTaskEditMode);
		Test.stopTest();
	}


	static testMethod void selectCurrentTaskTest() {
		SEC_TaskViewCC controller = new SEC_TaskViewCC();
		Task record = [SELECT Id FROM Task];
		Test.startTest();
			controller.currentTrainer = UserInfo.getUserId();
			controller.currentTaskId = record.Id;
			
			System.assertEquals(null, controller.selectCurrentTask());
			System.assertEquals(null, controller.refreshTaskList());
		Test.stopTest();
	}


	static testMethod void deleteTaskTest() {
		SEC_TaskViewCC controller = new SEC_TaskViewCC();
		Task record = [SELECT Id FROM Task];
		Test.startTest();
			controller.currentTrainer = UserInfo.getUserId();
			controller.currentTaskId = record.Id;
			
			System.assertEquals(null, controller.deleteTask());
		Test.stopTest();
	}


	static testMethod void createTaskTest() {
		SEC_TaskViewCC controller = new SEC_TaskViewCC();
		Task record = [SELECT Id FROM Task];
		Test.startTest();
			controller.currentTrainer = UserInfo.getUserId();
			controller.currentTaskId = record.Id;
			
			System.assertNotEquals(null, controller.createTask());
		Test.stopTest();
	}
}