public with sharing class SMSHistoryTriggerHandler {

    public static void relateRecordToAccount(List<smagicinteract__smsMagic__c> smsHistoryList, Set<Id> smsHistoryId){
        Set<Id> intakeStudentIds = new Set<Id>();
        List<Intake_Students__c> intakeStudentList = new List<Intake_Students__c>();
        Map<String, String> intakeStudentMap = new Map<String,String>();
        List<smagicinteract__smsMagic__c> smsHistoryInsert = new List<smagicinteract__smsMagic__c>();
        List<smagicinteract__smsMagic__c> smsHistoryToUpdate = new List<smagicinteract__smsMagic__c>();
        for(smagicinteract__smsMagic__c s : smsHistoryList){
            intakeStudentIds.add(s.Intake_Students__c);
        }
        if(intakeStudentIds.size() > 0){
            intakeStudentList = [select Id, Student__c from Intake_Students__c where Id IN: intakeStudentIds];
            for(Intake_Students__c i : intakeStudentList){
                intakeStudentMap.put(i.Id, i.Student__c);
            }
            
            List<smagicinteract__smsMagic__c> sList = [select Id,Intake_Students__c, Account__c from smagicinteract__smsMagic__c where Id in : smsHistoryId];
            for(smagicinteract__smsMagic__c s : sList){
                try{
                system.debug(Logginglevel.info,'s.Intake_Students__c : '+s.Intake_Students__c);
                system.debug(Logginglevel.info,'intakeStudentMap.get(s.Intake_Students__c) : '+intakeStudentMap.get(s.Intake_Students__c));
                if(intakeStudentMap.containsKey(s.Intake_Students__c)){
                    s.Account__c = intakeStudentMap.get(s.Intake_Students__c);
                    smsHistoryToUpdate.add(s);
                }
                }catch(Exception e){
                    system.debug(Logginglevel.info,'****'+e);
                }
            }
        }
        if(smsHistoryToUpdate.size() > 0)
            update smsHistoryToUpdate;
    }
    
}