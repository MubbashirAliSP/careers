/**
 * @description Test class for `StudentEngagementConsoleLC` class
 * @author Ranyel Maliwanag
 * @date 06.JUN.2016
 */
@isTest
public with sharing class StudentEngagementConsoleLC_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.Assigned_Trainer__c = UserInfo.getUserId();
		insert enrolment;
	}


	/**
	 * @description Accessors test. No assertions needed.
	 * @acceptanceCriteria Should finish without errors
	 */
	static testMethod void accessorsTest() {
		StudentEngagementConsoleLC.getTrainersList();

		List<SEC_Tab__mdt> tabs = StudentEngagementConsoleLC.getTabsList();
		List<SEC_Container__mdt> containers = StudentEngagementConsoleLC.getContainersList(tabs[0].DeveloperName);
		List<SEC_ListView__mdt> views = StudentEngagementConsoleLC.getContainerListViews(containers[0].DeveloperName);
		SEC_ListViewWrapper wrapper = StudentEngagementConsoleLC.getCurrentListView('Home_NewStudents_NewStud');
		StudentEngagementConsoleLC.getCurrentTrainerId();
		StudentEngagementConsoleLC.getCurrentTrainerName();
		StudentEngagementConsoleLC.getNewTask();
		StudentEngagementConsoleLC.getPicklistValues('Enrolment__c', 'Enrolment_Status__c');
		StudentEngagementConsoleLC.passiveAction();
	}


	/**
	 * @method getTrainerTaskList
	 * @acceptanceCrtieria Should return list of open tasks for the given trainer
	 */
	static testMethod void getTrainerTaskListTest() {
		Task record = new Task(
				ActivityDate = Date.today()
			);
		insert record;

		Test.startTest();
			List<Task> tasks = StudentEngagementConsoleLC.getTrainerTaskList(UserInfo.getUserId(), 'Today');
			System.assert(!tasks.isEmpty());

			record = StudentEngagementConsoleLC.getCurrentTask(record.Id);
			System.assertNotEquals(null, record);

			StudentEngagementConsoleLC.saveTask(record);
		Test.stopTest();
	}


	/**
	 * @method saveLogACall
	 * @acceptanceCriteria Should be able to create a task, given a single record Id in the recordIds list
	 */
	static testMethod void saveLogACallSingularTest() {
		List<Enrolment__c> enrolments = [SELECT Id, Student__r.PersonContactId FROM Enrolment__c];
		Task sampleTask = new Task();

		Integer initialTaskCount = [SELECT count() FROM Task];
		Integer enrolmentCount = enrolments.size();

		Test.startTest();
			StudentEngagementConsoleLC.saveLogACall(sampleTask, enrolments);
			System.assertEquals(initialTaskCount + enrolmentCount, [SELECT count() FROM Task]);
		Test.stopTest();
	}


	/**
	 * @method saveLogACall
	 * @acceptanceCriteria Should be able to create a task for each of the given record Ids in the recordIds list
	 */
	static testMethod void saveLogACallBulkTest() {
		Account student = [SELECT Id FROM Account];
		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.Assigned_Trainer__c = UserInfo.getUserId();
		insert enrolment;

		List<Enrolment__c> enrolments = [SELECT Id, Student__r.PersonContactId FROM Enrolment__c];
		Task sampleTask = new Task();

		Integer initialTaskCount = [SELECT count() FROM Task];
		Integer enrolmentCount = enrolments.size();

		Test.startTest();
			StudentEngagementConsoleLC.saveLogACall(sampleTask, enrolments);
			System.assertEquals(initialTaskCount + enrolmentCount, [SELECT count() FROM Task]);
		Test.stopTest();
	}


	/**
	 * @method markAsGraded
	 * @acceptanceCriteria Should mark the given Assessment Attempts as Completed
	 */
	static testMethod void markAsGradedTest() {
		// Create Assessment Attempt records
		AssessmentAttempt__c attempt = new AssessmentAttempt__c(
				BBAssessmentRecordId__c = 'AA0001'
			);
		insert attempt;

		List<String> recordIds = new List<String>{
			attempt.Id
		};

		Test.startTest();
			StudentEngagementConsoleLC.markAsGraded(recordIds);
		Test.stopTest();
	}


	/**
	 * @method getAccountAlerts
	 */
	static testMethod void getAccountAlertsTest() {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
		List<Enrolment__c> enrolments = Database.query(queryString);

		Test.startTest();
			StudentEngagementConsoleLC.getAccountAlerts(enrolments);
		Test.stopTest();
	}


	/**
	 * @method getEnrolment
	 */
	static testMethod void getEnrolmentTest() {
		Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
		Test.startTest();
			StudentEngagementConsoleLC.getEnrolment(enrolment.Id);
		Test.stopTest();
	}

	static testMethod void saveAssignCategoryTest() {
		Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
		List<String> recordIds = new List<String>{
			enrolment.Id
		};

		Test.startTest();
			StudentEngagementConsoleLC.saveAssignCategory(recordIds, 'Pale Green');
		Test.stopTest();
	}


	static testMethod void getCurrentListViewRecordsTest() {
		SEC_ListViewWrapper wrapper = StudentEngagementConsoleLC.getCurrentListView('Home_NewStudents_NewStud');

		Test.startTest();
			StudentEngagementConsoleLC.getCurrentListViewRecords(wrapper.meta, wrapper.fieldsetMembers, UserInfo.getUserId());
		Test.stopTest();
	}
}