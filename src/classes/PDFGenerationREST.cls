/**
 * @description REST service for generating PDFs and attaching them to the parent records
 * @author Ranyel Maliwanag
 * @date 27.NOV.2015
 */
@RestResource(urlMapping='/PDFGeneration/*')
global with sharing class PDFGenerationREST {
	/**
	 * @description
	 * @author Ranyel Maliwanag
	 * @date 27.NOV.2015
	 */
	@HttpPost
	global static void doPost(List<Id> recordIdList, String sObjectType) {
		List<Attachment> attachmentsForInsert = new List<Attachment>();

		for (Id recordId : recordIdList) {
			Attachment pdfInstance = new Attachment();
			PageReference pdf;

			if (sObjectType == 'Census__c') {
				pdfInstance.Name = 'Invoice.pdf';
				pdf = Page.PrintInvoice;
			}

			if (sObjectType == 'Enrolment__c') {
				pdfInstance.Name = 'CAN.pdf';
				pdf = Page.PrintCAN;
			}

			if (pdf != null) {
				pdf.getParameters().put('id', recordId);
				Blob body = Blob.valueOf('Attachment');
				if (!Test.isRunningTest()) {
					body = pdf.getContent();
				}
				pdfInstance.Body = body;
				pdfInstance.IsPrivate = false;
				pdfInstance.ParentId = recordId;
				attachmentsForInsert.add(pdfInstance);
			}
		}

		insert attachmentsForInsert;
	}
}