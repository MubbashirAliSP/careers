/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for PersonAccountHandler Class
Test Class:
History
07/01/2013     Created      Sairah Hadjinoor
------------------------------------------------------------*/
@isTest
private class PersonAccountHandler_Test {
    
    static testMethod void insertAcc() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            try{
                accList[0].Tax_File_Number__c = '12345432';
                update sacct;
            }
            catch(Exception e){
                
            }
            test.stopTest();
        }            
    }
    static testMethod void insertAcc2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CAIT12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            try{
                accList[0].Tax_File_Number__c = '1234543';
                update sacct;
            }
            catch(Exception e){
                
            }
            test.stopTest();
        }            
    }
    static testMethod void insertAcc3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
        
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CAEI12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            try{
                accList[0].Tax_File_Number__c = '333333333';
                update sacct;
            }
            catch(Exception e){
                
            }
            test.stopTest();
        }            
    }
    static testMethod void testIntakeStudent() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        Intake_Students__c intStud = new Intake_Students__c();
        system.runAs(u){
            Test.startTest();
        
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                //accList.add(sacct);
            //}
            insert sacct;
            
            test.stopTest();
        }            
    }
    static testMethod void insertOldAcc() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
        
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=1000;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.Legacy_Student_ID__c = '00000';
                sacct.LastName = 'Accounts';
                sacct.Student_Identifer__c = '01';
                sacct.External_Id__c = 'CACH';
                sacct.FirstName = 'You';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            test.stopTest();
        }
    }
    static testMethod void insertAccTFN() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
         Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
           
                Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
                labor.Name = 'Labor';
                labor.Code__c = '@@';
                insert labor;
                Country__c country = new Country__c();
                country.Name = 'Country';
                country.Country_Code__c = '@@@@';
                insert country;
                Language__c language = new Language__c();
                language.Name = 'Language';
                language.Code__c = '@@@@';
                insert language;
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.LastName = 'Accounts';
                sacct.Student_Identifer__c = '01';
                sacct.Tax_File_Number__c = '123456782';
                sacct.FirstName = 'You';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
              
            insert sacct;
            test.stopTest();
        }
    }
    static testMethod void insertAccTFN2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
                Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
                labor.Name = 'Labor';
                labor.Code__c = '@@';
                insert labor;
                Country__c country = new Country__c();
                country.Name = 'Country';
                country.Country_Code__c = '@@@@';
                insert country;
                Language__c language = new Language__c();
                language.Name = 'Language';
                language.Code__c = '@@@@';
                insert language;
            
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.LastName = 'Account';
                sacct.FirstName = 'You';
                sacct.Student_Identifer__c = '01';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
              
            insert sacct;
            test.stopTest();
        }
    }
    static testMethod void insertAccTFN3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
             Test.startTest();
                Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
                labor.Name = 'Labor';
                labor.Code__c = '@@';
                insert labor;
                Country__c country = new Country__c();
                country.Name = 'Country';
                country.Country_Code__c = '@@@@';
                insert country;
                Language__c language = new Language__c();
                language.Name = 'Language';
                language.Code__c = '@@@@';
                insert language;
            
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.LastName = 'Account';
                sacct.FirstName = 'You';
                sacct.Student_Identifer__c = '01';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Tax_File_Number__c = '333333333';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
              
            insert sacct;
            test.stopTest();
        }
    }
    static testMethod void insertOldAcc2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            List<Account> accList = new List<Account>();
            //for(integer i=0; i<=1000;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.Legacy_Student_ID__c = '00011';
                sacct.LastName = 'Account';
                sacct.FirstName = 'You';
                sacct.Student_Identifer__c = '01';
                sacct.External_Id__c = 'CAIT';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            test.stopTest();
        }
    }
    static testMethod void insertOldAcc3() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=1000;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.Legacy_Student_ID__c = '00011';
                sacct.FirstName = 'You';
                sacct.LastName = 'Account';
                sacct.Student_Identifer__c = '01';
                sacct.External_Id__c = 'CAEI';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                accList.add(sacct);
            //}
            insert accList;
            test.stopTest();
        }
    }
    
    static testMethod void populateIntakeStudentMobileTest() {
        
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        Unit_Hours_and_Points__c uhp2 = new Unit_Hours_and_Points__c();
        Enrolment_Unit_of_Study__c euos = new Enrolment_Unit_of_Study__c(); 
        Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Intake__c intk = new Intake__c();
        Intake_Unit__c iu = new Intake_Unit__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
            Test.startTest();
                Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
                labor.Name = 'Labor';
                labor.Code__c = '@@';
                insert labor;
                Country__c ctry = new Country__c();
                ctry.Name = 'Country';
                ctry.Country_Code__c = '@@@@';
                insert ctry;
                Language__c language = new Language__c();
                language.Name = 'Language';
                language.Code__c = '@@@@';
                insert language;
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry();           
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Predominant_Delivery_Mode__c = del.Id; 
                insert enrl;
                
                fundStream = TestCoverageUtilityClass.createFundingStream();
                program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
                intk = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
                
                Intake_Students__c iStud = new Intake_Students__c();
                iStud.Student__c = acc.Id;
                iStud.Enrolment__c = enrl.Id;
                iStud.Intake__c = intk.Id;
                //iStud.Mobile__c = acc.PersonMobilePhone;
                insert iStud;
                
                acc.PersonMobilePhone = '61847292112';
                update acc;
                
                iStud.Student__c = acc.Id;
                //iStud.Mobile__c = acc.PersonMobilePhone;
                update iStud;
            test.stopTest();
        }
    }
    
    static testmethod void testAV7AddressFields() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser999@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='standuser999@testorg.com');
        Id recrdType = [select Id from RecordType where Name = 'Student' And SObjectType = 'Account' LIMIT 1].Id;
        
        system.runAs(u){
            
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            
            State__c state = new State__c();
            state.Name = 'State';
            state.Country__c = country.Id;
            state.Code__c = '@@@@';
            state.Short_Name__c = 'S';
            insert state;
            
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            
            Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Address_building_property_name__c = 'Property Name';
                sacct.Address_flat_unit_details__c = '12';
                sacct.Address_street_number__c = '90';
                sacct.Address_street_name__c = 'Aaa Street';
                sacct.Suburb_locality_or_town__c = 'Suburb';
                sacct.Address_Post_Code__c = '4209';
                sacct.Reporting_Billing_State__c = state.Id;
                sacct.Address_Country__c = country.Id;
                sacct.Postal_Delivery_Box__c = 'PO BOX 999';
                sacct.Postal_suburb_locality_or_town__c = 'Coomera';
                sacct.Postal_Post_Code__c = '4209';
                sacct.Reporting_Other_State__c = state.Id;
                sacct.Postal_Country__c = country.Id;
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                
           Test.startTest();
           
           insert sacct;
           
           update sacct;
           
           Test.stopTest();
            
        }
        
    }
    static testMethod void deleteUSIDocument() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c country = new Country__c();
            country.Name = 'Country';
            country.Country_Code__c = '@@@@';
            insert country;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            Id recrdType = [select Id from RecordType where DeveloperName = 'PersonAccount' And SObjectType = 'Account' LIMIT 1].Id;
            List<Account> accList = new List<Account>();
            
            //for(integer i=0; i<=10;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.FirstName = 'You';
                sacct.LastName = 'Accounts';
                sacct.External_Id__c = 'CACH12345';
                sacct.Legacy_Student_ID__c = '123';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292111';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                sacct.Permission_Granted_to_Apply_For_USI__c= true; 
                accList.add(sacct);
            //}
            insert accList;

            USI_Identification_Document__c newUSIDocument = new USI_Identification_Document__c();
            newUSIDocument.Document_Number_Encrypted__c = 'A82137812';
            newUSIDocument.Account__c = accList[0].Id;
            insert newUSIDocument;

            try{
                accList[0].Validated_by_Webservice__c = true; 
                update accList;
            }
            catch(Exception e){
                
            }
            test.stopTest();
        }            
    }
}