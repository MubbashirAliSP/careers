/*This class is resposible for constructing XMLs for both Web Services and SIS
 *created by Yuri Gribanov 19 May 2014
 */

public class BlackboardXMLBuilder {

    /*All recordtypes except recordtype3 which is done via SIS
     *Operations include, create/update/get
     */
    public static String buildCourseWSXML(Blackboard_Course__c course) {

        String singleCourseBody = '';

        String courseServiceLevel = '';

        if(course.RecordType.DeveloperName == 'T4_Organisation')
            courseServiceLevel = 'Organization';
        else
          courseServiceLevel = 'Course';

                   //Perform a get
           if(course.SIS_Sync_Attempted__c) {

                 SingleCourseBody = '<cour:filter> ' +
              
                
             
                '<xsd:courseIds>'+course.name +'</xsd:courseIds>'+
               
                
              
                '<xsd:filterType>1</xsd:filterType>'+
               
             
              '</cour:filter>';
           } else {

              if(course.create__c == true) { //INSERT
                  singleCourseBody = '<cour:c> ' +
                          
                         '<xsd:available>true</xsd:available>'+
                        
                         '<xsd:courseId>'+course.Name+'</xsd:courseId>'+
                          
                         '<xsd:name>'+course.Blackboard_Course_Name__c+'</xsd:name>'+

                         '<xsd:description>'+course.Course_Description__c+'</xsd:description>'+

                         '<xsd:courseServiceLevel>'+courseServiceLevel+'</xsd:courseServiceLevel>'+                      
                         
                       ' </cour:c>';
                       
                    } else { //UPDATE
                    
                        singleCourseBody = '<cour:c> ' +
                          
                         '<xsd:available>'+course.Available_After_Sync__c+'</xsd:available>'+
                        
                         '<xsd:courseId>'+course.Name+'</xsd:courseId>'+
                          
                         '<xsd:name>'+course.Blackboard_Course_Name__c+'</xsd:name>'+
                         
                         '<xsd:id>'+course.Blackboard_Primary_Key__c+'</xsd:id>'+

                         '<xsd:description>'+course.Course_Description__c+'</xsd:description>'+
                         
                         '<xsd:courseServiceLevel>'+courseServiceLevel+'</xsd:courseServiceLevel>'+
                         
                         
                       ' </cour:c>';
                    
                    }

        }
        

       
        
         return singleCourseBody;

    }

    public static String buildCourseSISXML(List<Blackboard_Course__c> courses) {

        String xml = '';
        for(Blackboard_Course__c c : courses) {

           xml+='<group recstatus="1"> ' +
                   '<sourcedid> ' +
                      '<source>Blackboard_Course__c</source>'+
                      '<id>'+c.Id+'</id>'+
                    '</sourcedid> '+
                  '<grouptype>'+
                    '<typevalue level="0">0</typevalue>'+
                  '</grouptype>'+
                  '<description>'+
                    '<short>'+c.Name+'</short>'+
                    '<long>'+c.Blackboard_Course_Name__c+'</long>'+ 
                    '<full>'+c.Course_Description__c+'</full>'+
                  '</description>'+
              '<extension> '+
                '<x_bb_datasource_key>SALESFORCE</x_bb_datasource_key> '+
                '<x_bb_available>Y</x_bb_available> '+
                '<x_bb_institution_name>institution-name</x_bb_institution_name> '+
                '<X_BB_ROW_STATUS>0</X_BB_ROW_STATUS><X_BB_TEMPLATEKEY>'+c.Template_Course__r.Name+'</X_BB_TEMPLATEKEY> '+
              '</extension> '+
             
            
            '</group> ';



       }

       return xml;
    }


     public static String buildCourseMembershipXML(Blackboard_Course_Membership__c bbcoursemember) {
       
       String singleCourseMembershipBody = '';

       
           if(bbcoursemember.create__c == true) { //INSERT
         
           singleCourseMembershipBody =  '<cour:courseId>'+bbcoursemember.Blackboard_Course__r.Blackboard_Primary_Key__c+'</cour:courseId>'+
           
            
              
                 '<cour:cmArray>'+
            
                 ' <xsd:available>true</xsd:available>'+
                 ' <xsd:courseId>'+bbcoursemember.Blackboard_Course__r.Blackboard_Primary_Key__c+'</xsd:courseId>'+
                 
                 '  <xsd:userId>'+bbcoursemember.Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c +'</xsd:userId>'+
                 ' <xsd:roleId>'+bbcoursemember.Blackboard_User_Role__r.Blackboard_Role__r.Role_Id__c +'</xsd:roleId>'+
              
                '</cour:cmArray>';
             
            

            } else { //UPDATE

               singleCourseMembershipBody = '<cour:courseId>'+bbcoursemember.Blackboard_Course__r.Blackboard_Primary_Key__c+'</cour:courseId>'+
           
             
              
                 '<cour:cmArray>'+
            
                 ' <xsd:available>'+bbcoursemember.Available_After_Sync__c+'</xsd:available>'+
                 ' <xsd:courseId>'+bbcoursemember.Blackboard_Course__r.Blackboard_Primary_Key__c+'</xsd:courseId>'+
                 ' <xsd:id>'+bbcoursemember.Blackboard_Primary_Key__c+'</xsd:id>'+

                 ' <xsd:roleId>'+bbcoursemember.Blackboard_User_Role__r.Blackboard_Role__r.Role_Id__c +'</xsd:roleId>'+
                 
                 '  <xsd:userId>'+bbcoursemember.Blackboard_User_Role__r.Blackboard_User__r.Blackboard_Primary_Key__c +'</xsd:userId>'+
                 
              
                '</cour:cmArray>';
             
            

            }

        
       
       return singleCourseMembershipBody;
   
   }

   public static String buildUserXML(Blackboard_User__c bbuser) {

       String singleUserBody = '';
       
       String multiInsRoleBody = '';

       String birthdate = '';

       if(bbuser.Birthdate__c != null)
              birthdate = String.Valueof(bbuser.Birthdate__c.Day()) + String.ValueOf(bbuser.Birthdate__c.Month()) + String.ValueOf(bbuser.Birthdate__c.Year());


      if(bbuser.create__c == true) { //INSERT

           singleUserBody = ' <user:user> ' +
           ' <xsd:birthDate>'+ birthdate +'</xsd:birthDate>' +
                       
               '  <xsd:extendedInfo>' +
                 
                   '  <xsd:emailAddress>'+bbuser.Email__c+'</xsd:emailAddress>' +
                   
                   '  <xsd:familyName>'+bbuser.Last_Name__c+'</xsd:familyName>' +
                   
                   '  <xsd:givenName>'+bbuser.First_Name__c+'</xsd:givenName>' +
                   
                   '  <xsd:mobilePhone>' +bbuser.Mobile_Phone__c +'</xsd:mobilePhone>' +                 
                   
                '  </xsd:extendedInfo>' +
                         
                '  <xsd:id xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>' +
                
               		buildInsRoleXML(bbuser) +
             
                '  <xsd:isAvailable>true</xsd:isAvailable>' +
             
                '  <xsd:name>'+bbuser.Blackboard_Username__c+'</xsd:name>'+
                
                '  <xsd:password>'+bbuser.Blackboard_Password__c+'</xsd:password>' +
             
                '  <xsd:studentId>'+bbuser.Student_Identifier__c+'</xsd:studentId>'+
                
                '  <xsd:systemRoles xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'+
             
                              
              '  </user:user>';

        } else { //UPDATE


           singleUserBody = ' <user:user> ' +
           ' <xsd:birthDate>' + birthdate +'</xsd:birthDate>' +
                       
               '  <xsd:extendedInfo>' +
                 
                   '  <xsd:emailAddress>'+bbuser.Email__c+'</xsd:emailAddress>' +
                   
                   '  <xsd:familyName>'+bbuser.Last_Name__c+'</xsd:familyName>' +
                   
                   '  <xsd:givenName>'+bbuser.First_Name__c+'</xsd:givenName>' +
                   
                   '  <xsd:mobilePhone>' +bbuser.Mobile_Phone__c +'</xsd:mobilePhone>' +                 
                   
                '  </xsd:extendedInfo>' +
                         
                               
                '  <xsd:id>'+bbuser.Blackboard_Primary_Key__c+'</xsd:id>'+
                
                    buildInsRoleXML(bbuser) +
             
                '  <xsd:isAvailable>'+bbuser.Available_After_Sync__c+'</xsd:isAvailable>' +
             
                '  <xsd:name>'+bbuser.Blackboard_username__c+'</xsd:name>'+
                
                '  <xsd:studentId>'+bbuser.Student_Identifier__c+'</xsd:studentId>'+
                
                '  <xsd:systemRoles xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'+
             
                              
              '  </user:user>';

        }


      return singleUserBody;

   }
   
   private static String buildInsRoleXML(Blackboard_User__c bbuser) {
   	
   	 String insRoleXml = '';
   	 String insRoleXMLSec = '';
   	
   	 List<Blackboard_User_Role__c> primaryRole = [Select Primary_Institutional_Role__c, Blackboard_Role__r.Role_Id__c From Blackboard_User_Role__c Where Primary_Institutional_Role__c = True AND Blackboard_User__c = : bbuser.Id AND RecordType.DeveloperName = 'Faculty_Institutional' LIMIT 1];
   	 
   	 //construct Primary here
   	 
   	 insRoleXml = '<xsd:insRoles>' + primaryRole.get(0).Blackboard_Role__r.Role_Id__c + '</xsd:insRoles>';
   
   	 //build secondary roles
   	 for(Blackboard_User_Role__c role : [Select Primary_Institutional_Role__c, Blackboard_Role__r.Role_Id__c From Blackboard_User_Role__c Where  Blackboard_User__c = : bbuser.Id AND Primary_Institutional_Role__c = False And RecordType.DeveloperName = 'Faculty_Institutional'] ) 
   	 	insRoleXMLSec = insRoleXMLSec +  '<xsd:insRoles>' + role.Blackboard_Role__r.Role_Id__c + '</xsd:insRoles>';
   	 	
   	 //combine and return
   	 
   	 return insRoleXML + insRoleXMLsec;
   	 	
   }




}