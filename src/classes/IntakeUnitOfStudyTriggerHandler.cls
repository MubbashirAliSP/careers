/**
 * @description 	Intake Unit of Study Object Trigger Handler
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			07.JAN.2013
 *
 * HISTORY
 * - 07.JAN.2013	Janella Canlas		Created.
 * - 08.FEB.2013	Janella Canlas		add method to create Intake Unit of Study tuition fees
 */
public with sharing class IntakeUnitOfStudyTriggerHandler {
	/*
		This method will populate Scheduled Hours based on the related Unit Hours and Points records
	*/
	public static void populateScheduledHours(List<Intake_Unit_of_Study__c> iusList){
		Set<Id> iuIds = new Set<Id>();
		Set<Id> unitIds = new Set<Id>();
		List<Intake_Unit__c> iuList = new List<Intake_Unit__c>();
		List<Unit__c> iuUpdateList = new List<Unit__c>();
		List<Unit_Hours_and_Points__c> uhpList = new List<Unit_Hours_and_Points__c>();
		Map<Id,Decimal> hoursMap = new Map<Id,Decimal>();
		Map<String,String> unitMap = new Map<String,String>();
		system.debug('*****iusList: '+ iusList);
		//select intake unit of study ids
		for(Intake_Unit_of_Study__c i : iusList){
			iuIds.add(i.Unit__c);
		}
		system.debug('*****IUSIDS: '+ iuIds);
		//query Parent_UOC__c on Unit Records related to the Intake Unit of Study
		iuUpdateList = [SELECT Id, Parent_UoC__c 
						FROM Unit__c
						WHERE Id IN: iuIds];
		for(Unit__c i : iuUpdateList){
			unitIds.add(i.Parent_UoC__c);
			unitMap.put(i.Id,i.Parent_UoC__c);
		}
		system.debug('*****unitIds'+unitIds);
		uhpList = [select Unit__r.Parent_UoC__c, state__c, Unit__c, State__r.Short_Name__c, Nominal_Hours__c,Institutional_Hours__c 
					from Unit_Hours_and_Points__c where Unit__c IN: unitIds and State__r.Short_Name__c = 'QLD'];
		system.debug('*****uhpList'+uhpList);			
		for(Intake_Unit_of_Study__c i : iusList){
			String unitId = unitMap.get(i.Unit__c);
			for(Unit_Hours_and_Points__c u : uhpList){
				if(unitId == u.Unit__c){
					if(u.Institutional_Hours__c != null){
						i.Scheduled_Hours__c = u.Institutional_Hours__c;
					}
					else{
						i.Scheduled_Hours__c = u.Nominal_Hours__c;						
					}

				}
			}
		}
	}
	/*
		This method will insert Intake Unit of Study Tuition Fee records upon insert
	*/
	public static void insertTuitionFees(Set<Id> iusIds, List<Intake_Unit_of_Study__c> iusList){
		List<Intake_Unit_of_Study__c> createdIus = new List<Intake_Unit_of_Study__c>();
		List<Intake_Unit_of_Study_Tuition_Fee__c> iustfInsert = new List<Intake_Unit_of_Study_Tuition_Fee__c>();
		List<Tuition_Fee__c> tuitionList = new List<Tuition_Fee__c>();
		Set<Id> qualificationIds = new Set<Id>();
		Map<Id, List<Tuition_Fee__c>> qualTuitionMap = new Map<Id, List<Tuition_Fee__c>>();
		createdIus = [SELECT Id, Intake_Unit_of_Competency__r.Intake__r.Qualification__c, Scheduled_Hours__c 
					FROM Intake_Unit_of_Study__c
					WHERE Id IN: iusIds
					AND Intake_Unit_of_Competency__r.Intake__r.Intake_Status__c = 'Active'
					];
		for(Intake_Unit_of_Study__c i : createdIus){
			qualificationIds.add(i.Intake_Unit_of_Competency__r.Intake__r.Qualification__c);
		}
		tuitionList = [Select Id, Amount_per_Scheduled_Hour__c, Qualification__c, State__c 
						FROM Tuition_Fee__c WHERE Qualification__c IN: qualificationIds];
		for(Tuition_Fee__c t : tuitionList){
			if(qualTuitionMap.containsKey(t.Qualification__c)){
				List<Tuition_Fee__c> temp = qualTuitionMap.get(t.Qualification__c);
				temp.add(t);
				qualTuitionMap.put(t.Qualification__c, temp);	
			}
			else{
				qualTuitionMap.put(t.Qualification__c, new List<Tuition_Fee__c>{t});
			}
		}
		for(Intake_Unit_of_Study__c i : createdIus){
			if(qualTuitionMap.get(i.Intake_Unit_of_Competency__r.Intake__r.Qualification__c) != null){
				List<Tuition_Fee__c> tfs = qualTuitionMap.get(i.Intake_Unit_of_Competency__r.Intake__r.Qualification__c); 
				for(Tuition_Fee__c t : tfs){
					Intake_Unit_of_Study_Tuition_Fee__c temp = new Intake_Unit_of_Study_Tuition_Fee__c();
					temp.Intake_Unit_of_Study__c = i.Id;
					if(i.Scheduled_Hours__c != null && t.Amount_per_Scheduled_Hour__c != null){
						temp.Tuition_Fee__c = i.Scheduled_Hours__c * t.Amount_per_Scheduled_Hour__c;	
					}
					temp.State__c = t.State__c; 
					iustfInsert.add(temp);
				}
			}	
		}
		try{
			insert iustfInsert;}catch(Exception e){System.debug('ERROR: ' + e.getMessage());
		}
	}
	/*
		This method will update Intake Unit of Study Tuition Fee records upon update
	*/
	public static void updateTuitionFees(Set<Id> iusIds, List<Intake_Unit_of_Study__c> iusList){
		List<Intake_Unit_of_Study__c> updatedIus = new List<Intake_Unit_of_Study__c>();
		List<Intake_Unit_of_Study_Tuition_Fee__c> iustfUpdate = new List<Intake_Unit_of_Study_Tuition_Fee__c>();
		List<Tuition_Fee__c> tuitionList = new List<Tuition_Fee__c>();
		Set<Id> qualificationIds = new Set<Id>();
		Map<Id, List<Tuition_Fee__c>> stateTuitionMap = new Map<Id, List<Tuition_Fee__c>>();
		iustfUpdate = [SELECT Id, State__c, Tuition_Fee__c, Intake_Qualification_Code__c, 
						Intake_Unit_of_Study__r.Intake_Unit_of_Competency__r.Intake__r.Qualification__c,  Intake_Unit_of_Study__c,
						Intake_Unit_of_Study__r.Scheduled_Hours__c
						FROM Intake_Unit_of_Study_Tuition_Fee__c WHERE Intake_Unit_of_Study__c IN: iusIds];
						
		/*updatedIus = [SELECT Id, Intake_Unit_of_Competency__r.Intake__r.Qualification__c, Scheduled_Hours__c
						FROM Intake_Unit_of_Study__c
						WHERE Id IN: iusIds
					];*/
		for(Intake_Unit_of_Study_Tuition_Fee__c i : iustfUpdate){
			qualificationIds.add(i.Intake_Unit_of_Study__r.Intake_Unit_of_Competency__r.Intake__r.Qualification__c);
		}
		tuitionList = [Select Id, Amount_per_Scheduled_Hour__c, Qualification__c, State__c 
						FROM Tuition_Fee__c WHERE Qualification__c IN: qualificationIds];
		for(Tuition_Fee__c t : tuitionList){
			if(stateTuitionMap.containsKey(t.State__c)){
				List<Tuition_Fee__c> temp = stateTuitionMap.get(t.State__c);
				temp.add(t);
				stateTuitionMap.put(t.State__c, temp);	
			}
			else{
				stateTuitionMap.put(t.State__c, new List<Tuition_Fee__c>{t});
			}
		}
		for(Intake_Unit_of_Study_Tuition_Fee__c i : iustfUpdate){
			if(stateTuitionMap.get(i.State__c) != null){
				List<Tuition_Fee__c> tfs = stateTuitionMap.get(i.State__c);
				for(Tuition_Fee__c tf : tfs){
					if(i.Intake_Unit_of_Study__r.Intake_Unit_of_Competency__r.Intake__r.Qualification__c == tf.Qualification__c){
						if(i.State__c == tf.State__c){
							i.Tuition_Fee__c = i.Intake_Unit_of_Study__r.Scheduled_Hours__c * tf.Amount_per_Scheduled_Hour__c;	
						}
					}
				}
			}
		}
		try{
			update iustfUpdate;}catch(Exception e){System.debug('ERROR: ' + e.getMessage());
		}
	}
}