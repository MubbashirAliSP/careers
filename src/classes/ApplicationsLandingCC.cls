/**
 * @description Controller for `ApplicationsLanding` page
 * @author Ranyel Maliwanag
 * @date 11.JUN.2015
 * @history 
 *     17.MAR.2016 Biao Zhang - use standard controller for pagination
 */
public without sharing class ApplicationsLandingCC {
    // Flag to determine whether or not the user accessing the site is authenticated
    public Boolean isAuthenticated {
        get {
            isAuthenticated = EnrolmentsSiteUtility.checkAuthentication(ApexPages.currentPage());
            return isAuthenticated;
        }
        set;
    }
    // Flag to determine whether or not the user has confirmed credentials
    public Boolean isConfirmed {get; set;}
    // Flag to determine if the user is a staff
    public Boolean isStaff {get; set;}
    // Flag to determine if the user is a salesforce licence holder
    public Boolean isSalesforceUser {
        get {
            isSalesforceUser = UserInfo.getUserType() != 'Guest';
            return isSalesforceUser;
        }
        set;
    }
    // GuestLogin record to capture login information
    public GuestLogin__c guestLogin {get; set;}
    // List of application records for the current user
    //public List<ApplicationForm__c> studentApplicationForms {get; set;}
    public List<ApplicationForm__c> studentApplicationForms  
    {  
        get  
        {  
            if(con != null)  
                return (List<ApplicationForm__c>)con.getRecords();  
            else  
                return null ;  
        }  
        set;
    }  

    // Form Id for cancellation
    public Id formForCancellationId {get; set;}
    // Reference map for student application forms
    public Map<Id, ApplicationForm__c> studentApplicationFormsMap {get; set;}

    public Boolean isBusinessRelated {get; set;}
    public Account businessAccount {get; set;}
    
    
    // Private variables for internal references
    private Id authenticationId;
    private Boolean isStaffLoginToBeCreated;

    // Pagination stuff
    public ApexPages.StandardSetController con{get; set;}  
    //Boolean to check if there are more records after the present displaying records  
    public Boolean hasNext {  
        get {  
            return con.getHasNext();  
        }  
        set; 
    }  
   
    //Boolean to check if there are more records before the present displaying records  
    public Boolean hasPrevious {  
        get {  
            return con.getHasPrevious();  
        }  
        set;  
    }  
   
    //Page number of the current displaying records  
    public Integer pageNumber {  
        get {  
            return con.getPageNumber();  
        }  
        set;  
    } 

    //Max page number
    public Integer maxPageNumber {
        get {
            return (Integer)((Decimal)con.getResultSize() / con.getPageSize()).round(System.RoundingMode.CEILING);
        }
        set;
    }
  
    //Returns the previous page of records  
    public void previous() {  
        con.previous();  
    }  
   
    //Returns the next page of records  
    public void next() {  
        con.next();  
    }

    //days offset for display correct submission date and created date in local timezone (+10)
    public double timezoneOffset {
        get {
            if(timezoneOffset == null) {
                TimeZone tz = UserInfo.getTimeZone();
                timezoneOffset = tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
            }
            return timezoneOffset;
        }
        set;
    }

    /**
     * @description Default constructor for the class
     * @author Ranyel Maliwanag
     * @date 11.JUN.2015
     */
    public ApplicationsLandingCC() {
        Map<String, Cookie> activeCookies = ApexPages.currentPage().getCookies();
        String queryString = '';
        // Variable initialisations
        isConfirmed = false;
        isStaff = UserInfo.getUserType() != 'Guest';
        isStaffLoginToBeCreated = false;
        isBusinessRelated = ApexPages.currentPage().getParameters().containsKey('bid') && String.isNotBlank(ApexPages.currentPage().getParameters().get('bid'));

        guestLogin = null;
        studentApplicationFormsMap = new Map<Id, ApplicationForm__c>();

        System.debug('isSalesforceUser ::: ' + isSalesforceUser);

        if (isBusinessRelated) {
            String businessIdentifier = ApexPages.currentPage().getParameters().get('bid');
            System.debug('businessIdentifier ::: ' + businessIdentifier);

            queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
            queryString += 'WHERE BusinessIdentifier__c = :businessIdentifier';

            try {
                businessAccount = Database.query(queryString);
            } catch (Exception e) {
                System.debug('exception ::: ' + e.getMessage());
                isBusinessRelated = false;
                businessAccount = null;
            }
        }

        if (isAuthenticated) {
            authenticationId = activeCookies.get('authenticationId').getValue();

            // Get GuestLogin record based on Authentication Id
            queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c, ', RecordType.Name');
            queryString += 'WHERE Id = :authenticationId';
            try {
                guestLogin = Database.query(queryString);
            } catch (Exception e) {
                // Condition: Guest login not found
                // Expire the authentication Id
                activeCookies.put('authenticationId', new Cookie('authenticationId', null, null, -1, true));
                ApexPages.currentPage().setCookies(activeCookies.values());
            }
        } else {
            // Check if User is a Salesforce licence holder
            if (isSalesforceUser) {
                queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.GuestLogin__c, ', RecordType.Name');
                queryString += 'WHERE UserId__c = \'' + UserInfo.getUserId() + '\'';
                try {
                    guestLogin = Database.query(queryString);
                    activeCookies.put('authenticationId', new Cookie('authenticationId', guestLogin.Id, null, -1, true));
                    ApexPages.currentPage().setCookies(activeCookies.values());
                } catch (Exception e) {
                    // Condition: Guest login not found
                    // Create a new one on parameter evaluation
                    isStaffLoginToBeCreated = true;
                }
            }
        }

        if (guestLogin != null) {
            isStaff = guestLogin.RecordType.Name == 'Employee';
            isConfirmed = guestLogin.IsEmailConfirmed__c;

            if (isStaff) {
                // Fetch application forms for the staff
                queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, ', RecordType.Name, LinkedLoanFormId__r.Status__c');
                queryString += 'WHERE StaffCredentials__c = \'' + guestLogin.Id + '\' AND RecordType.Name = \'Enrolment\'';
            } else {
                // Fetch application forms for the student
                queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.ApplicationForm__c, ', RecordType.Name');
                queryString += 'WHERE StudentCredentialsId__c = :authenticationId AND IsMainApplicationForm__c = true';
            }
            con = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));  
            con.setPageSize(50);

            // Build reference map for application forms
            for (ApplicationForm__c form : studentApplicationForms) {
                studentApplicationFormsMap.put(form.Id, form);
            }
        } else {
            guestLogin = new GuestLogin__c();
        }
        
        // Notification messages handling
        Cookie messageCookie = activeCookies.get('notificationMessage');
        if (messageCookie != null && String.isNotBlank(messageCookie.getValue())) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, messageCookie.getValue()));
            messageCookie = new Cookie('notificationMessage', null, null, 0, true);
            activeCookies.put('notificationMessage', messageCookie);
        }
        ApexPages.currentPage().setCookies(activeCookies.values());

        // isBusinessRelated flag fall back: Determine status from Guest Login
        if (!isBusinessRelated && isAuthenticated && guestLogin != null && ! isStaff) {
            isBusinessRelated = guestLogin.BusinessAccountId__c != null;
            if (isBusinessRelated) {
                queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Account);
                queryString += 'WHERE Id = \'' + guestLogin.BusinessAccountId__c + '\'';

                try {
                    businessAccount = Database.query(queryString);
                } catch (Exception e) {
                    System.debug('exception ::: ' + e.getMessage());
                    isBusinessRelated = false;
                    businessAccount = null;
                }
            }
        }
    }

    /**
     * @description Page load action handler to evaluate parameters and setup redirect
     * @author Ranyel Maliwanag
     * @date 20.JUL.2015
     */
    public PageReference evaluateParameters() {
        String queryString;
        PageReference result = null;
        // Store GET parameters for reference
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        // Page cookies reference
        Map<String, Cookie> activeCookies = ApexPages.currentPage().getCookies();
        Id employeeRTID = Schema.SObjectType.GuestLogin__c.getRecordTypeInfosByName().get('Employee').getRecordTypeId();

        if (isStaffLoginToBeCreated) {
            Id userId = UserInfo.getUserId();
            User userRecord = [SELECT Id, Broker_ID__c FROM User WHERE Id = :userId];
            
            GuestLogin__c staffLogin = new GuestLogin__c(
                    Email__c = UserInfo.getUserEmail(),
                    UserId__c = UserInfo.getUserId(),
                    FirstName__c = UserInfo.getFirstName(),
                    LastName__c = UserInfo.getLastName(),
                    Password__c = '*',
                    IsEmailConfirmed__c = true,
                    RecordTypeId = employeeRTID,
                    BrokerIdentifier__c = userRecord.Broker_ID__c
                );
            insert staffLogin;
            result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsLanding, staffLogin.Id);
            result.setRedirect(true);
        }

        // Process parameters
        if (parameters.containsKey('confirm')) {
            if (isAuthenticated && !isConfirmed) {
                Id confirmationId = parameters.get('confirm');
                Id authenticationId = activeCookies.get('authenticationId').getValue();

                if (authenticationId == confirmationId) {
                    result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsConfirm, authenticationId);
                    result.getParameters().put('confirm', ApexPages.currentPage().getParameters().get('confirm'));
                    result.setRedirect(true);
                }
            }
        }
        return result;
    }


    /**
     * @description Command button handler to authenticate the login information provided
     * @author Ranyel Maliwanag
     * @date 11.JUN.2015
     */
    public PageReference authenticateLogin() {
        // Initialise result to null
        PageReference result = null;

        // Build query string for getting the matching guest login record
        String queryString = 'SELECT Id, Email__c, Password__c, RecordType.Name ';
        queryString += 'FROM GuestLogin__c ';
        queryString += 'WHERE Email__c = \'' + guestLogin.Email__c + '\'';

        try {
            GuestLogin__c match = Database.query(queryString);

            if (match != null) {
                if (match.Password__c == guestLogin.Password__c) {
                    isAuthenticated = true;
                    result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsLanding, match.Id);
                    if (ApexPages.currentPage().getParameters().containsKey('confirm')) {
                        result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsConfirm, match.Id);
                        result.getParameters().put('confirm', ApexPages.currentPage().getParameters().get('confirm'));
                    }

                    if (ApexPages.currentPage().getParameters().containsKey('retUrl')) {
                        result = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
                        result = EnrolmentsSiteUtility.generatePageReference(result, match.Id);
                    }

                    if (isBusinessRelated && businessAccount != null) {
                        // Condition: Enrolment portal is parameterised to a certain Business/Group
                        if (match.RecordType.Name == 'Student') {
                            // Condition: Guest login being authenticated is a student
                            // Action: Update the guest login to reference the Business account that currently owns the parameterised portal
                            match.BusinessAccountId__c = businessAccount.Id;
                            update match;
                        }
                    }
                    result.setRedirect(true);
                } else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Incorrect password entered'));
                }
            }
        } catch (QueryException e) {
            // Query exception for when there is no Guest Login record with the given email
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'The email you\'re trying to log in with is not yet registered'));
        }
        return result;
    }


    /**
     * @description Command button handler for launching a new application form
     * @author Ranyel Maliwanag
     * @date 29.JUN.2015
     */
    public PageReference launchNewApplication() {
        PageReference result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsApply, authenticationId);
        if (isBusinessRelated) {
            result.getParameters().put('bid', ApexPages.currentPage().getParameters().get('bid'));
        }
        result.setRedirect(true);
        return result;
    }


    /**
     * @description Command button handler to log the user out of the Applications portal
     * @author Ranyel Maliwanag
     * @date 13.JUL.2015
     */
    public PageReference logout() {
        PageReference result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsLanding, null);
        result.setRedirect(true);
        return result;
    }


    /**
     * @description Command link handler to cancel a given application form
     * @author Ranyel Maliwanag
     * @date 20.JUL.2015
     */
    public PageReference cancelApplicationForm() {
        PageReference result = null;
        if (formForCancellationId != null && studentApplicationFormsMap.containsKey(formForCancellationId)) {
            ApplicationForm__c form = studentApplicationFormsMap.get(formForCancellationId);
            studentApplicationFormsMap.remove(formForCancellationId);
            delete form;
        }

        studentApplicationForms = studentApplicationFormsMap.values();
        studentApplicationForms.sort();
        return result;
    }
}