/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchCourse implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchCourse() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPC_Course_Code__c, IPC_Description__c, Expiry_Date__c, IPC_End_Date__c, IPC_InPlace_Course__c, IPC_Start_Date__c, IPC_Sync_on_next_update__c, IPC_Version__c, InPlace_Discipline__r.Name, InPlace_faculty__r.Name, Name, Id ';
        queryString += 'FROM Qualification__c ';
        queryString += 'WHERE IPC_InPlace_Course__c = true AND IPC_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<Qualification__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (Qualification__c record : records) {
            if (!exportedIdentifiers.contains(record.IPC_Course_Code__c)) {
                InPlace__c forExport = new InPlace__c(
                        IPCourse_CourseCode__c = record.IPC_Course_Code__c,
                        IPCourseVersion_CourseCode__c = record.IPC_Course_Code__c,
                        IPCourseVersion_Description__c = record.IPC_Description__c,
                        //IPCourseVersion_EndDate__c = record.IPC_End_Date__c,
                        IPCourseVersion_FacultyCode__c = record.InPlace_faculty__r.Name,
                        IPCourseVersion_PrimaryDisciplineCode__c = record.InPlace_Discipline__r.Name,
                        IPCourseVersion_StartDate__c = record.IPC_Start_Date__c,
                        IPCourseVersion_Version__c = record.IPC_Version__c
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.IPC_Course_Code__c);
            }
            
            record.IPC_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchUnit nextBatch = new InPlaceExportBatchUnit();
        Database.executeBatch(nextBatch);
    }
}