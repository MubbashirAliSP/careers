/**
 * @description Test methods for Class Configuration, Classes, Staff related classes/triggers, 
 *				as well as AssignIntakeToClass and DuplicateIntake pages 
 * @author 		Janella Lauren Canlas
 * @Company		CloudSherpas
 * @date 		31.OCT.2012
 *
 * HISTORY
 * - 31.OCT.2012	Janella Canlas		Created.
 * - 24.JAN.2013	Janella Canlas		Merge with TestCoverageUtilityClass.
 * - 04.MAR.2013	Janella Canlas		Removed seeAllData=true
 */
@isTest
private class ConfigurationClassesStaffTriggers_Test {
	/*
		SCENARIO: Create Classes upon Creation of Class Configuration
	*/
    static testMethod void insertConfigTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Locations__c ploc = new Locations__c();
		Country__c co = new Country__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
	    		anzs = TestCoverageUtilityClass.createANZSCO();
		    	fld = TestCoverageUtilityClass.createFieldOfEducation();
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Update Classes upon Update of Class Configuration (Date and Day fields)
	*/
    static testMethod void updateConfigTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Locations__c ploc = new Locations__c();
		Country__c co = new Country__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation();
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	cc.Class_End_Time__c = '9:30 AM';
		    	cc.Class_Start_Time__c = '10:00 PM';
		    	cc.Day__c = 'Tuesday';
		    	update cc;
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Update Classes upon Update of Class Configuration (Date and Day fields)
	*/
    static testMethod void updateConfigTest1() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Locations__c ploc = new Locations__c();
		Country__c co = new Country__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation();
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	cc.Class_End_Time__c = '11:30 PM';
		    	cc.Class_Start_Time__c = '10:00 PM';
		    	cc.Day__c = 'Tuesday';
		    	update cc;
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Update Classes upon Update of Class Configuration (Date and Day fields)
	*/
    static testMethod void updateConfigTest2() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Locations__c ploc = new Locations__c();
		Country__c co = new Country__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation();
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	cc.Class_End_Time__c = '12:00 PM';
		    	cc.Class_Start_Time__c = '12:00 PM';
		    	cc.Day__c = 'Tuesday';
		    	update cc;
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Update Classes upon Update of Class Configuration (Location field)
	*/
    static testMethod void updateConfigTest3() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Locations__c pRoom = new Locations__c();
    	Locations__c room = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		State__c state = new State__c();
		Account acc = new Account();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs 	= TestCoverageUtilityClass.createANZSCO(); 
		    	fld 	= TestCoverageUtilityClass.createFieldOfEducation(); 
		    	qual 	= TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit 	= TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU 	= TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	
		    	//ploc 	= TestCoverageUtilityClass.createParentLocation();
		    	//loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	acc = TestCoverageUtilityClass.createTrainingOrganisation();
		    	pRoom = TestCoverageUtilityClass.createParentLocationRoom();
		    	room = TestCoverageUtilityClass.createLocationWithParent(co.Id, acc.Id, pRoom.Id, state.Id);
		    	cc 		= TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	cc.Class_Room__c = room.Id;
		    	update cc;
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Delete Classes upon Deletion of Class Configuration
	*/
    static testMethod void deleteConfigTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation(); 
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co 		= TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc 	= TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
				delete cc;
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Insert Class Staff Member if Staff Member is not null and Add Staff Members to Classes = true
	*/
    static testMethod void insertStaffTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Staff_Member__c st = new Staff_Member__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation(); 
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co = TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id);
		    	st = TestCoverageUtilityClass.createStaffMember(intke.Id,u.Id);
		    	st.Add_staff_member_to_all_classes__c = true;
		    	insert st;
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Delete/Insert Class Staff Member upon update of Staff Member
	*/
    static testMethod void updateStaffTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Locations__c ploc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Staff_Member__c st = new Staff_Member__c();
    	Country__c co = new Country__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation(); 
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	co = TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	ploc 	= TestCoverageUtilityClass.createParentLocation(co.Id, state.Id, locationLoading.Id);
		    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id);
		    	st = TestCoverageUtilityClass.createStaffMember(intke.Id,u.Id);
		    	st.Add_staff_member_to_all_classes__c = true;
		    	insert st;
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	st.Add_staff_member_to_all_classes__c = false;
		    	update st;
		    	st.Add_staff_member_to_all_classes__c = true;
		    	update st;
		    	
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Assign Intake to corresponding Class where Intake Unit is not null (AssignIntakeToClasses VF Page)
	*/
    static testMethod void assignUnitsTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Staff_Member__c st = new Staff_Member__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
		    	anzs = TestCoverageUtilityClass.createANZSCO(); 
		    	fld = TestCoverageUtilityClass.createFieldOfEducation(); 
		    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
		    	unit = TestCoverageUtilityClass.createUnit();
		    	co = TestCoverageUtilityClass.createCountry();
		    	state = TestCoverageUtilityClass.createState(co.Id);
		    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
		    	fundStream = TestCoverageUtilityClass.createFundingStream();
				program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
		    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
		    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
		    	st = TestCoverageUtilityClass.createStaffMember(intke.Id,u.Id);
		    	st.Add_staff_member_to_all_classes__c = true;
		    	insert st;
		    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
		    	
		    	Classes__c clas = new Classes__c();
		    	clas = [SELECT id, Class_Configuration__c, End_Date_Time__c, Intake__c,
	        				Room__c,Room_Address__c,Start_Date_Time__c,Name,Unit_Name__c,
	        				Intake_Unit__r.Name, Intake_Unit__c, Intake_Unit__r.Unit_of_Study__r.Name
	        				FROM Classes__c where Intake__c =: intke.Id LIMIT 1];
		    	clas.Intake_Unit__c = null;
		    	update clas;
		    	PageReference testPage = Page.AssignIntakeToClasses;
		        testPage.getParameters().put('intakeId', intke.Id);
		      	Test.setCurrentPage(testPage);
		      	
		      	AssignIntakeToClasses_CC assign = new AssignIntakeToClasses_CC();
		      	assign.getIntakeUnitsPicklist();
		      	assign.getIntakeRecord();
		      	List<AssignIntakeToClasses_CC.WrapperClass> wrapperList = new List<AssignIntakeToClasses_CC.WrapperClass>();
		      	AssignIntakeToClasses_CC.WrapperClass wrapper = new AssignIntakeToClasses_CC.WrapperClass(clas);
		      	wrapper.c.Intake_Unit__c = intkeU.id;
		      	wrapperList.add(wrapper);
		      	assign.classWrapList = wrapperList;
		      	assign.updateClasses();
		    	assign.cancel();
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Assign Intake to corresponding Class where Intake Unit is null (AssignIntakeToClasses VF Page)
	*/
    static testMethod void assignUnitsTest2() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Staff_Member__c st = new Staff_Member__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
		Location_Loadings__c locationLoading = new Location_Loadings__c();
		State__c state = new State__c();
    	system.runAs(u){
	    	test.startTest();
	    	anzs = TestCoverageUtilityClass.createANZSCO();
	    	fld = TestCoverageUtilityClass.createFieldOfEducation();
	    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
	    	unit = TestCoverageUtilityClass.createUnit();
	    	co = TestCoverageUtilityClass.createCountry();
	    	state = TestCoverageUtilityClass.createState(co.Id);
	    	locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
	    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);    
	    	fundStream = TestCoverageUtilityClass.createFundingStream();
			program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
	    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
	    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
	    	st = TestCoverageUtilityClass.createStaffMember(intke.Id,u.Id);
	    	st.Add_staff_member_to_all_classes__c = true;
			insert st;
	    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);	
	    	
	    	Classes__c clas = new Classes__c();
	    	clas = [SELECT id, Class_Configuration__c, End_Date_Time__c, Intake__c,
        				Room__c,Room_Address__c,Start_Date_Time__c,Name,Unit_Name__c,
        				Intake_Unit__r.Name, Intake_Unit__c, Intake_Unit__r.Unit_of_Study__r.Name
        				FROM Classes__c where Intake__c =: intke.Id LIMIT 1];
	    	PageReference testPage = Page.AssignIntakeToClasses;
	        testPage.getParameters().put('intakeId', intke.Id);
	      	Test.setCurrentPage(testPage);
	      	
	      	AssignIntakeToClasses_CC assign = new AssignIntakeToClasses_CC();
	      	assign.getIntakeUnitsPicklist();
	      	assign.getIntakeRecord();
	      	List<AssignIntakeToClasses_CC.WrapperClass> wrapperList = new List<AssignIntakeToClasses_CC.WrapperClass>();
	      	AssignIntakeToClasses_CC.WrapperClass wrapper = new AssignIntakeToClasses_CC.WrapperClass(clas);
	      	wrapper.c = new Classes__c();
	      	wrapperList.add(wrapper);
	      	assign.classWrapList = wrapperList;
	      	assign.updateClasses();
	    	assign.cancel();
	    	test.stopTest();
    	}  
    }
    /*
		SCENARIO: Duplicate Intake (DuplicateIntake VF Page)
	*/
    /*static testMethod void duplicateIntakesTest() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    			LocaleSidKey='en_US', ProfileId = p.Id, 
	      			TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
	      			
    	ANZSCO__c anzs = new ANZSCO__c();
    	Field_of_Education__c fld = new Field_of_Education__c();
    	Qualification__c qual = new Qualification__c();
    	Unit__c unit = new Unit__c();
    	Intake__c intke = new Intake__c();
    	Intake_Unit__c intkeU = new Intake_Unit__c();
    	Locations__c loc = new Locations__c();
    	Class_Configuration__c cc = new Class_Configuration__c();
    	Staff_Member__c st = new Staff_Member__c();
    	Country__c co = new Country__c();
    	Locations__c ploc = new Locations__c();
    	Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
		Program__c program = new Program__c();
    	system.runAs(u){
	    	test.startTest();
	    	
	    	anzs = TestCoverageUtilityClass.createANZSCO(); 
	    	fld = TestCoverageUtilityClass.createFieldOfEducation(); 
	    	qual = TestCoverageUtilityClass.createQualification(anzs.Id, fld.Id);
	    	unit = TestCoverageUtilityClass.createUnit();
	    	co = TestCoverageUtilityClass.createCountry();
	    	ploc = TestCoverageUtilityClass.createParentLocation();
	    	loc = TestCoverageUtilityClass.createLocation(co.Id,ploc.Id);
	    	fundStream = TestCoverageUtilityClass.createFundingStream();
			program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
	    	intke = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id); 
	    	intkeU = TestCoverageUtilityClass.createIntakeUnit(intke.Id, unit.Id); 
	    	st = TestCoverageUtilityClass.createStaffMember(intke.Id,u.Id);
	    	st.Add_staff_member_to_all_classes__c = true;
	    	insert st;
	    	cc = TestCoverageUtilityClass.createClassConfiguration(intke.Id, intkeU.Id);
	    	
	    	PageReference testPage = Page.DuplicateIntake;
	        testPage.getParameters().put('intakeId', intke.Id);
	      	Test.setCurrentPage(testPage);
	      	
	      	DuplicateIntake_CC dup = new DuplicateIntake_CC();
	      	dup.getIntakeRecord();
	      	dup.getClassList();
	      	dup.saveIntake();
	    	dup.cancel();
	    	test.stopTest();
    	}  
    }*/
}