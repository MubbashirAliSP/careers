public with sharing class UnitsChangesReportCtrl {
    private List<TC> tcCache; //wrapperlist

    public class Change{
        public String oldValue{get;set;}
        public String newValue{get;set;}
        public boolean encodeValues {get; set; }
        
        public Change(){
            encodeValues = true;
        }
    }
    public class TC {
        private boolean ivalue = false;
        public boolean isUpgrade { get; set; }
        public boolean i{
            get{
                return ivalue && !isUpgrade;
            }
            set{
                ivalue = value;
            }
        }
        public Training_Component__c p{get;set;}
        public List<Change> changes{get;set;}
        
        public TC(){
            isUpgrade = false;
        }
    }
    
    public void selectAllUnits(){
        integer c = 0;
        for ( TC t : units ){
            t.i = true;
            if ( ++c >= 300 ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, c + ' items selected. Please import these first before importing any more.'));
                break;
            }
        }
    }
    
    public String retURL {
        get{
            return System.currentPagereference().getParameters().get('retURL');
        }
    }
    
    private String pParentCode;
    public String parentCode {
        get{
            if ( pParentCode == null )
                pParentCode = System.currentPagereference().getParameters().get('parent');
            return pParentCode;
        }
        set{
            pParentCode = value;
        }
    }
    
    boolean convert(Training_Component__c frm, Unit__c to, boolean copyTitle){
        boolean ret = to.id == null;
        
        if ( to.name != frm.code__c) ret = true;
        to.name = frm.code__c;
        
        if ( to.id == null ){
            to.Field_of_Education__c = frm.Fields_of_Education__c;
            to.Unit_Name__c = frm.Title__c;
            to.Standard_Unit__c = true;
        }
        if ( copyTitle ){
            if ( to.Unit_Name__c != frm.Title__c) ret = true;
            to.Unit_Name__c = frm.Title__c;
        }
        return ret;
    }
    
    void markComponentImported(string code){
        Training_Component__c[] cc = [ SELECT
                imported__c,
                updated__c
            From
                Training_Component__c
            Where
                Code__c = :code ];
        if ( cc.size() == 1 && cc[0].updated__c != cc[0].imported__c ){
           //     system.assert(false, 'hi' + cc[0].imported__c + ' ' + cc[0].updated__c);
            cc[0].imported__c = cc[0].updated__c;
            update cc[0];
        }
    }
    
    
    
    public List<TC> units {
        get{
            if ( tcCache == null ){ //wrapperlist 
                
                String parent = parentCode;
                System.assert( parent != null && parent != '', 'Parent parameter must be set');
                
                Training_Component__c[] r = [ 
                    select 
                        Code__c,
                        Fields_of_Education__c,
                        Title__c,
                        Updated__c,
                        Training_Package__c,
                        Training_Package_Unit__c,
                        Component_Type__c,
                        Parent_Code__c,
                        Unit__c,
                        Unit__r.Unit_Name__c
                    from Training_Component__c
                    Where Component_Type__c = 'Unit'
                    And Parent_Code__c = :parent
                    Order By Code__c
                ];
                TC p;
                map<string, Training_Component__c> tcMap = new Map<string, Training_Component__c>();
                list<Training_Component__c> newTCs = new list<Training_Component__c>();
                map<Id, TC> TCByTcId = new map<Id, TC>();
                for (Training_Component__c tc : r ){
                    tcMap.put(tc.code__c, tc);
                    
                    if ( p == null ){
                        p = new TC();
                        p.changes = new List<Change>();
                    }
                    p.p = tc;
                    if ( tc.Unit__c == null ){
                        Change c = new Change();
                        c.newValue = 'New Unit';
                        p.changes.add(c);
                        newTCs.add(tc);
                    }else{
                        //check for unit <> tc changes
                        if ( tc.Title__c != tc.Unit__r.Unit_Name__c ){
                            Change c = new Change();
                            c.oldValue = tc.Unit__r.Unit_Name__c;
                            c.newValue = tc.Title__c;
                            p.changes.add(c);
                        }
                    }
                    
                    if ( p.changes.size() > 0 ){
                        TCByTcId.put(tc.id, p);
                        p = null;
                    }
                }
                
                //find all the base unit candidates
                if ( newTCs.size() > 0 ){
                    String basesSql = '';
                    map<string, Unit__c> tcUnitCandidates = new map<string, Unit__c>();
                    map<string, list<Training_Component__c>> tcByBaseCode = new map<string, list<Training_Component__c>>();
                    for ( Training_Component__c newTC : newTCs ){
                        string baseCode = getBaseUnitCode(newTC.code__c);
                        if ( baseCode == null ){
                            continue;
                        }
                        
                        if ( !tcByBaseCode.containsKey(baseCode) )
                            tcByBaseCode.put(baseCode, new List<Training_Component__c>());
                        tcByBaseCode.get(baseCode).add(newTC);
                        
                        if ( basesSql != '' )
                            basesSql += ' OR ';
                        basesSql += 'Name LIKE \'' + baseCode + '%\'';
                    }
                    
                    if ( basesSql.length() > 0 ){
                        
                        for ( Unit__c unit : Database.query('select Name from Unit__c where ' + basesSql + ' ORDER BY Name') ){
                            string unitBaseCode = getBaseUnitCode(unit.name);
                            list<Training_Component__c> tcs = tcByBaseCode.get(unitBaseCode);
                            if ( tcs == null )
                                continue;
                            for ( Training_Component__c tc : tcs ){
                                if ( unit.name.compareTo(tc.code__c) <= 0 ){
                                    //while the unit comes before (alphabetically) this tc use this one
                                    //note that the units are sorted in the query, so the 'latest' unit will come last
                                    tcUnitCandidates.put(tc.code__c, unit); 
                                }
                            }
                        }
                        
                        //find number of enrolments per unit
                        Map<String, integer> enrolmentsPerUnit = new Map<String, Integer>();
                        for ( AggregateResult ar : Database.query('SELECT count(Enrolment__c) c, Unit__r.Name n FROM Enrolment_Unit__c Where Unit__c IN (select Id from Unit__c where ' + basesSql + ') Group By Unit__r.Name') ){
                            enrolmentsPerUnit.put((String)ar.get('n'), (Integer)ar.get('c'));
                        }
                        
                        Map<String, integer> intakesPerUnit = new Map<String, Integer>();
                        date today = system.today();
                        for ( AggregateResult ar : Database.query('SELECT count(Id) c, Unit_of_Study__r.Name n FROM Intake_Unit__c Where Unit_Start_Date__c > :today AND Unit_of_Study__c IN (select Id from Unit__c where ' + basesSql + ') Group By Unit_of_Study__r.Name') ){
                            intakesPerUnit.put((String)ar.get('n'), (Integer)ar.get('c'));
                        }
                        
                        //go through candidates and find the 'previous' unit for all new units
                        for ( Training_Component__c newTC : newTCs ){
                            Unit__c previous = tcUnitCandidates.get(newTC.code__c);
                            if ( previous != null ){
                                integer enrolmentsCount = enrolmentsPerUnit.get(previous.Name);
                                if ( enrolmentsCount == null )
                                    enrolmentsCount = 0;
                                integer intakesCount = intakesPerUnit.get(previous.Name);
                                if ( intakesCount == null )
                                    intakesCount = 0;
                                Change c = new Change();
                                c.encodeValues = false;
                                c.oldValue = '<a href="/' + previous.id + '">' + previous.Name + '</a> (' + enrolmentsCount + ', ' + intakesCount+ ')';
                                c.newValue = newTC.Code__c;
                                TCByTcId.get(newTC.id).changes.clear();
                                TCByTcId.get(newTC.id).changes.add(c);
                                
                                if ( enrolmentsCount > 0 || intakesCount > 0 )
                                    TCByTcId.get(newTC.id).isUpgrade = true;
                            }
                        }
                    }
                }
                tcCache = TCByTcId.values();
            }
            return tcCache;
        }    
    }
    
    private string getBaseUnitCode(String code){
        if ( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.countMatches(code.substring(code.length()-1)) > 0 )
            return code.substring(0, code.length()-1);
        return null;
    }
    
    
    
    public PageReference importSelectedUnits(){
        //all TC to update with new data
        Map<String, Training_Component__c> toUpdate = new Map<String, Training_Component__c>();
        
        
        //create TP if it doesn't exist
        Training_Component__c trainingPackagesComponent = [ select Id, Title__c, Code__c, Updated__c From Training_Component__c Where Code__c = :parentCode ];
        Training_Package__c[] trainingPackages = [ select Id From Training_Package__c Where Name = :parentCode ];
        Training_Package__c trainingPackage;
        boolean trainingPackageIsNew = false;
        if ( trainingPackages.size() == 0 ){
            trainingPackage = new Training_Package__c();
            trainingPackage.Training_Package_Name__c = trainingPackagesComponent.Title__c;
            trainingPackage.name = trainingPackagesComponent.code__c;
            insert trainingPackage;
            trainingPackageIsNew= true;
            
            
            //link new TPs to TC
            trainingPackagesComponent.Training_Package__c = trainingPackage.id;
            update trainingPackagesComponent;
        }else{
            trainingPackage = trainingPackages[0];
        }
        
        
         //fetch components, and link units
         List<Unit__c> toUpdateUnit = new List<Unit__c>();
         Map<String, Unit__c> units = new Map<String, Unit__c>();
         Map<String, Unit__c> existingUnits = new Map<String, Unit__c>();
         List<String> unitCodes = new List<String>();
         for ( TC tp : this.units ){
            if ( tp.i ){
                unitCodes.add(tp.p.code__c);
            }
         }
         for ( Unit__c unit : [ 
                    select 
                        Id,
                        Name
                    From Unit__c
                    Where
                        Name in :unitCodes
                     ]){
                     existingUnits.put(unit.Name, unit);
                 }

         for ( TC tp : this.units ){
            if ( tp.i ){
                 //create or update the unit
                 Unit__c unit = existingUnits.get(tp.p.Code__c);
                 if ( unit == null ){
                     unit = new Unit__c();
                     toUpdate.put(tp.p.Code__c, tp.p);
                 }else if ( tp.p.Unit__c != unit.id ){
                     tp.p.unit__c = unit.id;
                     toUpdate.put(tp.p.Code__c, tp.p);
                 }
                 if ( convert(tp.p, unit, true) )
                     toUpdateUnit.add(unit);
            }
        }
        
        //insert units
        upsert toUpdateUnit;
        // and link them up to the training_components
        for ( Unit__c newunit : toUpdateUnit ){
            Training_Component__c tc = toUpdate.get(newunit.name);
            if ( tc!= null ){
                tc.Unit__c = newunit.id;
            }
        }
        
        
        //fetch training package units
        Map<String, Training_Package_Unit__c> existingTpunits = new Map<String, Training_Package_Unit__c>();
        for ( Training_Package_Unit__c tpunit : [ 
            select 
                Id,
                National_Code__c
            From Training_Package_Unit__c
            Where
                 Training_Package__c = :trainingPackage.id
             ]){
             existingTpunits.put(tpunit.National_Code__c, tpunit);
        }

        //now handle the training package units
        List<Training_Package_Unit__c> toUpdateTPU = new List<Training_Package_Unit__c>();
        for ( TC tp : this.units ){
            if ( tp.i ){
                 
                 //create and update units and tp unit
                 Training_Package_Unit__c tpunit = existingTpunits.get(tp.p.code__c);
                 if ( tpunit == null ){
                     tpunit = new Training_Package_Unit__c();
                     tpunit.Training_Package__c = trainingPackage.id;
                     tpunit.Unit_of_Competency__c = tp.p.Unit__c;
                     toUpdateTPU.add(tpunit);
                     toUpdate.put(tp.p.Code__c, tp.p);
                 }else if ( tp.p.Training_Package_Unit__c != tpunit.id ){
                     toUpdate.put(tp.p.Code__c, tp.p);
                     tp.p.Training_Package_Unit__c = tpunit.id;
                 }
             }
        }
        
        
        //update TPUs with new units
        upsert toUpdateTPU;
        for ( Training_Package_Unit__c newtpu : toUpdateTPU ){
            for ( Training_Component__c tc: toUpdate.values() ){
                if ( newtpu.Unit_of_Competency__c == tc.Unit__c ){
                    tc.Training_Package_Unit__c = newtpu.id;
                }
            }
        }
        
        
        
        //check that we have all required data
        for ( Training_Component__c c : toUpdate.values() ){
            if ( c.Component_Type__c == 'Unit' ){
                system.assert(c.Unit__c != null);
                system.assert(c.Training_Package_Unit__c != null, c);
            }else{
                system.assert(false, 'Invalid Component Type: ' + c.Component_Type__c);
            }
        }
        upsert toUpdate.values();
        
        tcCache = null;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, (trainingPackageIsNew ? 1 : 0) + ' new training packages were added with ' + toUpdateUnit.size() + ' units and ' + toUpdateTPU.size() + ' training package units.'));
        
        if ( this.units.size() == 0 ){
            markComponentImported(parentCode);
            
            PageReference ret = Page.TrainingPackageDownloader;
            ret.setRedirect(true);
            return ret;
        }else{
            return null;
        }
        
    }
    
}