/**
 * @description Controller for Edit Result And Avetmiss page
 * @author Janella Lauren Canlas
 * @date 13.NOV.2012
 *
 * HISTORY
 * - 13.NOV.2012    Janella Canlas - Created.
 * - 03.JUL.2013    Eu Cadag - Optimized codes to isolate the error Limit execution of apex Script
 * - 01.AUG.2013    Michelle Magsarili - Fixed the Wrapper Class codes to generate the correct and aligned class roll records.
 * - 06.FEB.2014    Bayani Cruz - Modified gettoDateTime() method's datetime.newInstance() to datetime.newInstanceGmt()
 * - 07.FEB.2014    Bayani Cruz - Sorted Student Name alphabetically
 */
public with sharing class GenerateClassRoll_CC {
    public Id intakeId;//holds the Id of the previous Intake record

    public String DateTo {get;set;} //holds the date string the user inputs on 'To Date' field
    public String DateFrom {get;set;}//holds the date string the user inputs on 'From Date' field
    public DateTime toDateTime; //holds the DateTime value of DateTo
    public DateTime fromDateTime; //holds the DateTime value of DateFrom
    public Date toDate; //holds the Date value of DateTo
    public Date fromDate; //holds the Date value of DateFrom

    public List<Class_Enrolment__c> enrolList {get;set;} //list of class enrolments related to the intake id within range entered by user
    public List<Classes__c> clasList {get;set;} //list of classes enrolments related to the intake id within range entered by user

    public String trainer {get;set;} //holds the Educator Name chosen from the 'Roll Signatory' picklist

    //declare list of wrappers needed
    public List<AttendWrapper> attendWrapperList {get;set;}
    public List<headerMWrapper> headerWrapperList {get;set;}
    public List<lineWrapperClass> lineWrapperList {get;set;}
    public List<headerWrapper> headerLineWrapperList {get;set;}
    public List<pageWrapper> pageWrapperList {get;set;}

    public GenerateClassRoll_CC(){
    //intake id passed by the button
    intakeId = ApexPages.currentPage().getParameters().get('intakeId');
    }
  
    /*retrieve the current Intake Record using the Id retrieved*/
    public Intake__c getIntakeRecord(){
        Intake__c intke = [SELECT Id, Name, Qualification_Name__c,Training_Delivery_Location__r.Name, Start_Date__c, End_Date__c, Program__r.Name, Qualification__r.Name FROM Intake__c WHERE Id =: intakeId];
        return intke;
    }

    /*this will display the Staff Members related to the current Intake record as picklist values*/
    public List<SelectOption> getStaffPicklist() {
        List<SelectOption> options = new List<SelectOption>();                       
        options.add(new SelectOption('', ''));   
        for (Staff_Member__c iu : [SELECT Id, Name, First_Name__c, Last_Name__c, Trainer__c, Trainer__r.Name from Staff_Member__c WHERE Intake__c =: intakeId]) {
            /*the selected option will pass the Id value on the Trainer__c field*/
            options.add(new SelectOption(iu.Trainer__r.Name,iu.Trainer__r.Name));
        }  
        return options;
    }
    
    /*query all classes related to the intake within range*/
    public List<Classes__c> getClassList(){
        List<Classes__c> clsList = new List<Classes__c>();
        clsList = [Select Unit_Name__c, Unit_Code__c, Start_Date_Time__c, Room__c, Room_Address__c, Name, 
                    Intake__c, Intake_Unit__c, Id, End_Date_Time__c, Class_Configuration__c 
                    From Classes__c WHERE Intake__c =: intakeId
                    AND Start_Date_Time__c >=: fromDateTime AND End_Date_Time__c <=: toDateTime
                    order by Start_Date_Time__c];
        return clsList;
    }
    /*query all class enrolments/students related to the intake within range*/
    public List<Class_Enrolment__c> getclsEnrolList(){
        List<Class_Enrolment__c> enrlmnt = new List<Class_Enrolment__c>();
        enrlmnt = [Select Unit_Name__c, Unit_Code__c, Student_Name__c, Student_Identifier__c, Name, Id, Enrolment_Intake_Unit__c, 
                    Class__c, Class__r.Intake__c, Class_Start_Date_Time__c, Class_End_Date_Time__c, Attendance_Type__c,
                    Attendance_Type__r.Name, Attendance_Code__c,LLN_DIS__c, Enrolment__r.Student__r.Disability_flag_0__c,
                    Enrolment_Intake_Unit__r.Enrolment__r.Student__r.Disability_flag_0__c
                    From Class_Enrolment__c
                    WHERE Class_Start_Date_Time__c >=: fromDateTime AND Class_End_Date_Time__c <=: toDateTime
                    AND Class__r.Intake__c =: intakeId
                    ORDER BY Class_Start_Date_Time__c
                    ];
        return enrlmnt;
    }
    
    /*this will convert DateTo value from string to DateTime type*/
    public DateTime gettoDateTime(){
      String[] toArray = DateTo.split('/');
      Date toDate = date.valueOf(toArray[2]+'-'+toArray[1]+'-'+toArray[0]);
      DateTime toDateTime = datetime.newInstanceGmt(toDate.year(), toDate.month(),toDate.day(),0,0,0);
      system.debug('***TO: ' + toDateTime);
      return toDateTime;
    }
    
    /*this will convert DateFrom from string to DateTime type*/
    public DateTime getfromDateTime(){
      String[] fromArray = DateFrom.split('/');
      Date fromDate = date.valueOf(fromArray[2]+'-'+fromArray[1]+'-'+fromArray[0]);
      DateTime fromDateTime = datetime.newInstance(fromDate.year(), fromDate.month(),fromDate.day(),0,0,0);
      system.debug('***FROM: ' + fromDateTime);
      return fromDateTime;
    }
    
    /*this will convert DateTo value from string to Date type*/
    public DateTime gettoDate(){
      String[] toArray = DateTo.split('/');
      Date toDate = date.valueOf(toArray[2]+'-'+toArray[1]+'-'+toArray[0]);
      return toDate;
    }
    /*this will convert DateFrom value from string to Date type*/
    public DateTime getfromDate(){
      String[] fromArray = DateFrom.split('/');
      Date fromDate = date.valueOf(fromArray[2]+'-'+fromArray[1]+'-'+fromArray[0]);
      return fromDate;
    }
    
    /*this will generate PDF Class Rolls*/
    public PageReference generate(){
        //this will redirect to the ClassRollPDF page 
        PageReference pageRef = new PageReference('/apex/ClassRollPDF');  

        //variables
        DateTime fromDateTime = getfromDateTime();
        DateTime toDateTime = gettoDateTime();

        Set<String> studentNames = new Set<String>();

        Map<String, List<Classes__c>> dateClassMap = new Map<String, List<Classes__c>>();
        Map<String, List<Class_Enrolment__c>> studEnrolMap = new Map<String, List<Class_Enrolment__c>>();
        Map<String, List<Class_Enrolment__c>> classEnrolMap = new Map<String, List<Class_Enrolment__c>>();
        Map<String,List<lineWrapperClass>> lineWrapperMap = new Map<String,List<lineWrapperClass>>();

        /*validations*/
        //to make sure toDate is later than fromDate
        if (Date.ValueOf(fromDateTime)>Date.ValueOf(toDateTime)){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'From Date should be earlier than the To Date.'));
            return null;
        }
        //fromDate must always be Monday and toDate must always be Friday
        if(fromDateTime.format('EEEE') != 'Monday'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'From Date should be a Monday'));
            return null;
        }
        else if(toDateTime.format('EEEE') != 'Friday'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'To Date should be a Friday'));
            return null;
        }
        /*end of validations*/
        else{
            //instantitate variables
            enrolList = new List<Class_Enrolment__c>();
            enrolList = getclsEnrolList();
            clasList = new List<Classes__c>();
            clasList = getClassList();
            
            system.debug('***ENROLMENT LIST: ' + enrolList.size() + ' -- ' + enrolList);

            //re-instantiate wrapper lists
            attendWrapperList = new List<AttendWrapper>();
            headerWrapperList = new List<headerMWrapper>();
            headerLineWrapperList = new List<headerWrapper>();
            //variables used to populate page wrapper
            List<headerWrapper> tempH = new List<headerWrapper>();
            List<AttendWrapper> tempA = new List<AttendWrapper>();
            List<lineWrapperClass> tempL = new List<lineWrapperClass>();
            List<lineWrapperClass> tempLs = new List<lineWrapperClass>();
            Map<Integer, List<AttendWrapper>> attMap = new Map<Integer,List<AttendWrapper>>();
            Map<Integer, List<headerWrapper>> headMap = new Map<Integer,List<headerWrapper>>();
            Map<Integer, List<lineWrapperClass>> lineMap = new Map<Integer,List<lineWrapperClass>>();
            Integer headCount = 0;
            Integer attCount = 0;
            DateTime tempFromDATE;
            //create map that will hold classes per date
            dateClassMap = new Map<String, List<Classes__c>>();
            Map<id,Classes__c> clasList_Cont = new Map<Id, Classes__c>();
            
            while(fromDateTime <= toDateTime){
                String fromDateStr = convertToString(fromDateTime);
                for(Classes__c c : clasList){
                  //tempFromDATE = convertedDateTime(c.Start_Date_Time__c);
                  clasList_Cont.put(c.Id,c);
                  tempFromDATE = c.Start_Date_Time__c;
                  String tempFromStr = convertToString(tempFromDATE);
                  if(fromDateStr == tempFromStr){
                    if(dateClassMap.containsKey(tempFromStr)){
                        List<Classes__c> tempList = dateClassMap.get(tempFromStr);
                        tempList.add(c);
                        dateClassMap.put(tempFromStr,tempList);
                    }
                    else{
                      dateClassMap.put(tempFromStr, new List<Classes__c>{c});
                    }  
                  }
                }
                //increment fromDateTime
                fromDateTime = datetime.newInstance(fromDateTime.year(), fromDateTime.month(),fromDateTime.day()+1, fromDateTime.hour(), fromDateTime.minute(), fromDateTime.second());
            }
          
            //loop to populate header classes
            tempH = new List<headerWrapper>();
            tempA = new List<AttendWrapper>();
            fromDateTime = getfromDate();
            String fromDateStr = '';
            List<Classes__c> tempClasses = new List<Classes__c>();
            headerLineWrapperList = new List<headerWrapper>();
            while(fromDateTime <= convertedDateTime(toDateTime)){
                tempClasses = new List<Classes__c>();
                fromDateStr = convertToString(fromDateTime);
                //classes that fall on Saturdays and Sundays are not included
                if(fromDateTime.format('EEEE') != 'Saturday' && fromDateTime.format('EEEE')!='Sunday'){
                    if(dateClassMap.get(fromDateStr) != null){
                        //list of classes which start date = fromDateTime value
                        tempClasses = dateClassMap.get(fromDateStr);
                        for(Classes__c c : tempClasses){
                            headerLineWrapperList.add(new headerWrapper(c.Unit_Code__c, c.Start_Date_Time__c));
                        }
                    }
                    else{
                        headerLineWrapperList.add(new headerWrapper('', fromDateTime));
                    }
                }
                //increment fromDateTime
                fromDateTime = datetime.newInstance(fromDateTime.year(), fromDateTime.month(),fromDateTime.day()+1, fromDateTime.hour(), fromDateTime.minute(), fromDateTime.second());
                //tempClasses.clear();
            }
            //end of header code
            
            Map<String,Class_Enrolment__c> studentMap = new Map<String,Class_Enrolment__c>();
            Map<String,Class_Enrolment__c> studentMap_cont = new Map<String,Class_Enrolment__c>();
            //create set that will hold student identifiers
            for(Class_Enrolment__c e : enrolList){
                studentNames.add(e.Student_Identifier__c);
                studentMap.put(e.Student_Identifier__c, e);
                if(e.Unit_Code__c != null){
                    studentMap_cont.put(e.Student_Identifier__c, e);
                }
            }
            //map to hold set of class start date times per student
            /******************************Start: Optimized Block of Codes By Eu***************************************/
            Map<String,Set<String>> studSetMap = new Map<String,Set<String>>();
            for(String s : studentNames){
                    String unit = '';
                    if(studentMap_cont.containsKey(s)){
                        unit = studentMap_cont.get(s).Unit_Code__c;
                        if(studSetMap.containsKey(s)){
                            Set<String> temp = studSetMap.get(s);
                            temp.add(unit+' '+studentMap_cont.get(s).Class_Start_Date_Time__c);
                            studSetMap.put(s,temp);
                        }
                        else{
                            studSetMap.put(s,new Set<String>{unit+' '+studentMap_cont.get(s).Class_Start_Date_Time__c});
                        }
                    }
            }
            /******************************End: Optimized Block of Codes By Eu***************************************/
            /************* Previous Code that executes many time 
            for(String s : studentNames){
                for(Class_Enrolment__c e : enrolList){
                    String unit = '';
                    if(e.Unit_Code__c != null){
                        unit = e.Unit_Code__c;
                    }
                    if(s == e.Student_Identifier__c){
                        if(studSetMap.containsKey(s)){
                            Set<String> temp = studSetMap.get(s);
                            temp.add(unit+' '+e.Class_Start_Date_Time__c);
                            studSetMap.put(s,temp);
                        }
                        else{
                            studSetMap.put(s,new Set<String>{unit+' '+e.Class_Start_Date_Time__c});
                        }
                    }
                }
            }*/
            //create map that will hold class enrolments per student
            for(Class_Enrolment__c e : enrolList){
                if(studEnrolMap.containsKey(e.Student_Identifier__c)){
                  List<Class_Enrolment__c> tempList = studEnrolMap.get(e.Student_Identifier__c);
                        tempList.add(e);
                        studEnrolMap.put(e.Student_Identifier__c,tempList);
                }
                else{
                  studEnrolMap.put(e.Student_Identifier__c, new List<Class_Enrolment__c>{e});
                }
            }
            //create map that will hold class enrolments per class - stored in clasList_Cont
            /******************************Start: Optimized Block of Codes By Eu***************************************/
            for(Class_Enrolment__c e : enrolList){
                if(clasList_Cont.containsKey(e.Class__c)){
                    if(studEnrolMap.containsKey(clasList_Cont.get(e.Class__c).Id)){
                        List<Class_Enrolment__c> tempList = studEnrolMap.get(clasList_Cont.get(e.Class__c).Id);
                        tempList.add(e);
                        studEnrolMap.put(clasList_Cont.get(e.Class__c).Id,tempList);
                    }
                    else{
                        studEnrolMap.put(clasList_Cont.get(e.Class__c).Id, new List<Class_Enrolment__c>{e});
                    }
                }
            }
            /******************************Start: Optimized Block of Codes By Eu***************************************/    
            /************* Previous Code that executes many time
            for(Classes__c c : clasList){
                for(Class_Enrolment__c e : enrolList){
                    if(e.Class__c == c.Id){
                        if(studEnrolMap.containsKey(c.Id)){
                            List<Class_Enrolment__c> tempList = studEnrolMap.get(c.Id);
                            tempList.add(e);
                            studEnrolMap.put(c.Id,tempList);
                        }
                        else{
                            studEnrolMap.put(c.Id, new List<Class_Enrolment__c>{e});
                        }
                    }
                }
            }*/
            
            /*START OF WRAPPER CODES*/
            Set<String> studValid = new Set<String>();
            for(String s : studentNames){
                fromDateTime = getfromDateTime();
                while(fromDateTime <= convertedDateTime(toDateTime)){
                    tempClasses = new List<Classes__c>();
                    fromDateStr = convertToString(fromDateTime);
                    if(fromDateTime.format('EEEE') != 'Saturday' && fromDateTime.format('EEEE')!='Sunday'){
                        if(dateClassMap.get(fromDateStr) != null){
                            tempClasses = dateClassMap.get(fromDateStr);
                            for(Classes__c c : tempClasses){
                                //header code
                                if(studEnrolMap.get(c.Id) != null){
                                    List<Class_Enrolment__c> tempEnrol = studEnrolMap.get(c.Id);
                                    for(Class_Enrolment__c e : tempEnrol){
                                        //List<lineWrapperclass> tempLine = new List<lineWrapperClass>();
                                        Set<String> st = new Set<String>();
                                        if(studSetMap.get(s) != null){
                                          st = studSetMap.get(s);
                                        }
                                        if(e.Student_Identifier__c == s){
                                            //lineWrapperClass WILL POPULATE STUDENT ATTENDANCE TYPE
                                            if(lineWrapperMap.containsKey(s)){                                          
                                                List<lineWrapperClass> tempList = lineWrapperMap.get(s);
                                                //MAM 08/01/2013: get Enrolment Start Date e.Class_Start_Date_Time__c instead of fromDateTime
                                                tempList.add(new lineWrapperClass(e.Attendance_Type__r.Name,e.Class_Start_Date_Time__c));
                                                lineWrapperMap.put(s,tempList);
                                            }
                                            else{
                                                //MAM 08/01/2013: get Enrolment Start Date e.Class_Start_Date_Time__c instead of fromDateTime
                                                lineWrapperMap.put(s,new List<lineWrapperClass>{new lineWrapperClass(e.Attendance_Type__r.Name,e.Class_Start_Date_Time__c)});
                                            }   
                                        }
                                        else{
                                          //if the student is not enrolled to the class
                                            if(!studValid.contains(s+' '+e.Class_Start_Date_Time__c) && !st.contains(e.Unit_Code__c+' '+e.Class_Start_Date_Time__c)){
                                                if(lineWrapperMap.containsKey(s)){
                                                    List<lineWrapperClass> tempList = lineWrapperMap.get(s);
                                                    //MAM 08/01/2013: get Enrolment Start Date e.Class_Start_Date_Time__c instead of fromDateTime
                                                    tempList.add(new lineWrapperClass('NR',e.Class_Start_Date_Time__c));
                                                    lineWrapperMap.put(s,tempList);
                                                }
                                                else{
                                                    //MAM 08/01/2013: get Enrolment Start Date e.Class_Start_Date_Time__c instead of fromDateTime
                                                    lineWrapperMap.put(s,new List<lineWrapperClass>{new lineWrapperClass('NR',e.Class_Start_Date_Time__c)});
                                                }
                                            }
                                        }
                                        studValid.add(s+' '+e.Class_Start_Date_Time__c);
                                    }
                                }
                                else{
                                    //if there are no class enrolments
                                    if(lineWrapperMap.containsKey(s)){
                                        List<lineWrapperClass> tempList = lineWrapperMap.get(s);                                      
                                        //MAM 08/01/2013: get Class Start Date (c.Start_Date_Time__c) instead of fromDateTime
                                        tempList.add(new lineWrapperClass('NR',c.Start_Date_Time__c)); 
                                        lineWrapperMap.put(s,tempList);
                                    }
                                    else{
                                        //MAM 08/01/2013: get Class Start Date (c.Start_Date_Time__c) instead of fromDateTime
                                        lineWrapperMap.put(s,new List<lineWrapperClass>{new lineWrapperClass('NR',c.Start_Date_Time__c)}); 
                                    }
                                }
                            }
                        }
                        
                        else{//for extra spaces for extra dates
                            //header code
                            //headerLineWrapperList.add(new headerWrapper('', fromDateTime));
                            if(lineWrapperMap.containsKey(s)){
                                List<lineWrapperClass> tempList = lineWrapperMap.get(s);
                                tempList.add(new lineWrapperClass('NR',fromDateTime));
                                lineWrapperMap.put(s,tempList);
                            }
                            else{
                                lineWrapperMap.put(s,new List<lineWrapperClass>{new lineWrapperClass('NR',fromDateTime)});
                            }
                        }
                    }
                  
                    //increment fromDateTime
                    fromDateTime = datetime.newInstance(fromDateTime.year(), fromDateTime.month(),fromDateTime.day()+1, fromDateTime.hour(), fromDateTime.minute(), fromDateTime.second());
                }
                
                String llndis = '';
                String lln = '';
                String dis = '';
                if(studentMap.get(s).Enrolment_Intake_Unit__r.Enrolment__r.Student__r.Disability_flag_0__c != null){
                    dis = studentMap.get(s).Enrolment_Intake_Unit__r.Enrolment__r.Student__r.Disability_flag_0__c;
                }
                else{
                    dis = studentMap.get(s).Enrolment__r.Student__r.Disability_flag_0__c;
                }
                if(studentMap.get(s).LLN_DIS__c != null){
                    lln = studentMap.get(s).LLN_DIS__c;
                }
                else{
                    lln = 'None';
                }
                llndis =  lln + '/' + dis;
                
                //MAM 08/01/2013 start: Remove ALL Duplicates in lineWrapperClass
                List<LineWrapperClass> initialList = new List<LineWrapperClass> ();
                initialList = lineWrapperMap.get(s);
                Map<String,LineWrapperClass> templwc = new Map<String,LineWrapperClass>();
                if(!initialList.isEmpty()){
                    for(lineWrapperClass lwc: initialList){
                        if(lwc.Attend=='NR'){
                            templwc.put(s+'~'+String.ValueOf(lwc.startdate),lwc);
                        }
                        if(lwc.Attend==null || String.IsNotEmpty(lwc.Attend)){
                            //override NR record before adding new one, NULL is ok too as this means that the Student has a Class Enrolment record
                            templwc.put(s+'~'+String.ValueOf(lwc.startdate),lwc);
                        }
                    }
                    
                    
                    lineWrapperMap.remove(s);
                    lineWrapperMap.put(s,templwc.values());
                }
                //MAM 08/01/2013 end
                
                attendWrapperList.add(new AttendWrapper(s, studentMap.get(s).Student_Name__c, llndis, lineWrapperMap.get(s), fromDateTime));
            
            }            
            fromDateTime = getfromDate();
            //populate pageWrapper
            pageWrapperList = new List<pageWrapper>();
            Integer index=0;
            //create a map of list of headers and index
            for(headerWrapper h : headerLineWrapperList){
                DateTime hStart = h.startDate;
                if(headMap.containsKey(headCount)){
                    List<headerWrapper> tempList = headMap.get(headCount);
                    tempList.add(h);
                    headMap.put(headCount,tempList);
                }
                else{
                    headMap.put(headCount,new List<headerWrapper>{h});
                }
                index = index + 1;
                //add index if the current day is friday
                if(hStart.format('EEEE')=='Friday' && index < headerLineWrapperList.size() && headerLineWrapperList[index].startDate.format('EEEE') != 'Friday'){
                    headCount++;  
                }
                if(index < headerLineWrapperList.size()){
                }
            }
      
            //create map of line items and index
            for(AttendWrapper a : attendWrapperList){
                index = 0;
                DateTime aStart = a.startDate;
                tempL = a.attendance;
                tempL.sort();                       //MAM 08/01/2013 Use the NEW Sort feature
                for(lineWrapperClass l : tempL){
                    
                    tempLs.add(l);
                    //add index if current day is friday
                    if(l.startDate.format('EEEE') == 'Friday' && index+1 < tempL.size() && tempL[index+1].startDate.format('EEEE') == 'Monday'){
                        tempA.add(new AttendWrapper(a.identifier, a.student, a.LLN, tempLs, a.startDate));
                        tempLs = new List<lineWrapperClass>();
                    }
                    else if(l.startDate.format('EEEE') == 'Friday' && index+1 >= tempL.size()){
                        tempA.add(new AttendWrapper(a.identifier, a.student, a.LLN, tempLs, a.startDate));
                        tempLs = new List<lineWrapperClass>();
                    }
                    index = index + 1;
                }
            }   
      
            /*for(AttendWrapper a : attendWrapperList){
            DateTime aStart = a.startDate;
            tempL = a.attendance;
            index = 0;
            for(integer i = 0; i < tempL.size(); i++){
              integer index1 = i + 1;
              if(tempL[i].startDate.format('EEEE') == 'Friday' && index1 < tempL.size() && tempL[index1].startDate.format('EEEE') != 'Friday'){
                tempA.add(new AttendWrapper(a.identifier, a.student, a.LLN, tempLs, a.startDate));
                tempLs = new List<lineWrapperClass>();
              }
              else{
                tempLs.add(tempL[i]);
              }
            }
            }*/
            
            //collect student identifiers
            Set<String> studs = new Set<String>();
            for(AttendWrapper a : attendWrapperList){
                studs.add(a.identifier);
            }
            //
            Map<String,List<AttendWrapper>> mapAttend = new Map<String,List<AttendWrapper>>();
            for(AttendWrapper a : tempA){
                if(mapAttend.containsKey(a.identifier)){
                    List<AttendWrapper> tempList = mapAttend.get(a.identifier);
                    tempList.add(a);
                    mapAttend.put(a.identifier,tempList);
                }
                else{
                    mapAttend.put(a.identifier,new List<AttendWrapper>{a});
                }
            }
            
            for(String s : studs){
                attCount = 0;
                List<AttendWrapper> tmpA = new List<AttendWrapper>();
                if(mapAttend.get(s) != null){
                    tmpA = mapAttend.get(s);
                }
                for(AttendWrapper a : tmpA){
                    if(attMap.containsKey(attCount)){
                        List<AttendWrapper> tempList = attMap.get(attCount);
                        tempList.add(a);
                        attMap.put(attCount,tempList);
                    }
                    else{
                        attMap.put(attCount,new List<AttendWrapper>{a});
                    }
                    attCount++;
                } 
            }
            //fill page wrapper list
            for(integer i=0; i <= headCount; i++){
                pageWrapperList.add(new pageWrapper(attMap.get(i),headMap.get(i)));
            }
            
            //BAC sorts pagewrapperlist 2/7/2014
            for(pageWrapper pw : pageWrapperList) {
                for (Integer x = 0; x < pw.attendance.size(); x++) {
                AttendWrapper key;
                Integer y = x;
                    while(y != 0 && pw.attendance[y].student < pw.attendance[y - 1].student) {
                        key = pw.attendance[y];
                        pw.attendance[y] = pw.attendance[y - 1];
                        pw.attendance[y - 1] = key;
                        y--;
                    }
                }
            }
            //BAC End
            
            //clear variables used
            attMap.clear();
            headMap.clear();
            lineWrapperMap.clear();
            dateClassMap.clear();
            dateClassMap = new Map<String, List<Classes__c>>();
            enrolList = new List<Class_Enrolment__c>();
            enrolList = getclsEnrolList();
            clasList = new List<Classes__c>();
            clasList = getClassList();
            /*END OF WRAPPER CODES*/

            pageRef.getParameters().put('intakeId',intakeId);
            pageRef.setRedirect(false);
            return pageRef;
        }
    }
    
    //go back to the intake detail page
    public PageReference cancel(){
        PageReference p = new PageReference('/'+intakeId);
        p.setredirect(true);
        return p;
    }
    
    /*WRAPPER CLASSES*/
    //will hold all data per page
    public class pageWrapper{
        public List<AttendWrapper> attendance {get; set;}
        public List<headerWrapper> header {get; set;}
        
        public pageWrapper(List<AttendWrapper> attendance, List<headerWrapper> header){
            this.attendance = attendance;
            this.header = header;
        }
        
    }
    //holds the students and all attendance types for the particular student
    public class AttendWrapper{
        public String identifier {get;set;}
        public String student {get;set;}
        public String LLN {get;set;}
        public List<lineWrapperclass> attendance {get;set;}
        public DateTime startDate {get;set;}
        
        public AttendWrapper(String identifier,String student, String LLN, List<lineWrapperclass> attendance, DateTime startDate){
            this.identifier = identifier;
            this.student = student;
            this.LLN = LLN;
            this.attendance = attendance;
            this.startDate = startDate;
        }
    }
    
    //holds the attendance types
    //MAM 08/01/2013 start: converted lineWrapperclass to have Sorting Features 
    public class lineWrapperclass implements Comparable {
        public String attend {get;set;}
        public DateTime startDate {get;set;}
        public lineWrapperclass(String attend, DateTime startDate){
            this.attend = attend;
            this.startDate = startDate;
        }
        
        //08/01/2013 NEW METHOD: CompareTo Method to sort the List according to dates
        public Integer compareTo(Object compareTo) {
            lineWrapperclass compareLWC = (lineWrapperclass)compareTo;
            if (startDate == compareLWC.startDate) return 0;
            if (startDate > compareLWC.startDate) return 1;
            return -1;        
        }
    }
    //MAM 08/01/2013 end
    
    //for the class columns
    public class headerMWrapper{
        public List<headerWrapper> header{get;set;}
        public Date startDate {get;set;}
        
        public headerMWrapper(List<headerWrapper> header, Date startDate){
            this.header = header;
            this.startDate = startDate;
        }
    }
    
    //holds the unit codes and start date and times
    public class headerWrapper{
        public String unitCode {get;set;}
        public DateTime startDate {get;set;}
        
        public headerWrapper(String unitCode, DateTime startDate){
            this.unitCode = unitCode;
            this.startDate = startDate;
        }
    }
    
    /*END OF WRAPPER CLASSES*/
    /*Date Conversion Methods*/
    public static DateTime convertedDateTime(DateTime temp){
        DateTime converted = datetime.newInstance(temp.year(), temp.month(),temp.day(), temp.hour()+10, temp.minute(), temp.second());
        return converted;
    }
    public static Date convertToDate(DateTime temp){
        String dateString = string.ValueOf(temp.year() +'/' +temp.month()+'/'+temp.day());
        Date converted = date.valueOf(dateString);
        return converted;
    }
    public static String convertToString(DateTime temp){
        String dateString = string.ValueOf(temp.year() +'/' +temp.month()+'/'+temp.day());
        return dateString;
    }
    /*End of Date Conversion Methods*/
}