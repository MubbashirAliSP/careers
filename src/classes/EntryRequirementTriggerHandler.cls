/**
 * @description Central trigger handler for the `EntryRequirement__c` object
 * @author Ranyel Maliwanag
 * @date 27.JAN.2016
 * @history
 *		 22.FEB.2016	Biao Zhang 			- Update Service Case Lowest Entry Requirement Status field
 *		 17.MAR.2016	Ranyel Maliwanag	- Added logic for processing changed ACSF Levels for LLN Entry Requirements
 *		 21.MAR.2016	Ranyel Maliwanag	- Added logic for processing updated Literacy and/or Numeracy Level scores for fast-tracking the movement of the Service Case to the `Online - Enrolments` queue as part of the pilot programme to increase turn-around time
 *											- Streamlined processing for both updated ACSF and Literacy/Numeracy levels into one method to optimise SOQL and DML calls
 *		 29.MAR.2016	Ranyel Maliwanag	- Added logic to create Enrolment Application Milestone records for each Entry Requirement record that gets completed
 *		 31.MAR.2016	Ranyel Maliwanag	- Added field update to Service Case to set the Secondary LLN Support Strategy field to `6 - Level 2 Support`
 *		 06.APR.2016	Ranyel Maliwanag	- Added logic to exclude Nursing applications from fast tracking
 *		 14.APR.2016	Ranyel Maliwanag	- Updated handling for ACSF Level 2 Scenario 1 to move the Service Case to the Service Cases to be closed queue instead of being fast tracked
 */
public without sharing class EntryRequirementTriggerHandler {
	//Entry Requirement Record Type Ids
	private static Id llnRecordTypeId = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	private static Id year12ReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Year 12 Certificate').getRecordTypeId();
	private static Id nzCitizenReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('NZ Citizen').getRecordTypeId();
	private static Id under18ReqRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Under 18').getRecordTypeId();
	private static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();
	private static Id certifcateOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Certificate Opportunity').getRecordTypeId();

	private static Map<String, Integer> statusOrder = new Map<String, Integer>{'Awaiting Upload' => 1, 'Awaiting Verification' => 2, 'Confirmed' => 3};
	private static Set<Id> VFHEntryRequirementRecordTypeIds = new Set<Id> {llnRecordTypeId, year12ReqRTID, nzCitizenReqRTID, under18ReqRTID};

	private static Set<Id> forMilestoneCreationIds = new Set<Id>();


	/**
	 * @description Handles all before update operations for Entry Requirement records
	 */
	public static void onBeforeUpdate(List<EntryRequirement__c> records, Map<Id, EntryRequirement__c> oldMap) {
		List<EntryRequirement__c> forDocumentCheck = new List<EntryRequirement__c>();
		for (EntryRequirement__c record : records) {
			if (record.Sighted__c && record.Sighted__c != oldMap.get(record.Id).Sighted__c && record.RecordTypeId == c3gRTID) {
				forDocumentCheck.add(record);
			}
		}
		if(!forDocumentCheck.isEmpty()) checkDocumentCount(forDocumentCheck);

	}

	/**
	 * @description Handles all after insert operations for Entry Requirement records
	 */
	public static void onAfterInsert(List<EntryRequirement__c> records) {
		evaluateAllEntryRequirementsMet(records);

		List<EntryRequirement__c> statusUpdatedERs = new List<EntryRequirement__c>();
		for (EntryRequirement__c er : records) {
			// Only update Service case Lowest ER status field when the ER status is one of these three status
			if(!VFHEntryRequirementRecordTypeIds.contains(er.RecordTypeId) && (statusOrder.keySet().contains(er.Status__c) || er.Status__c == 'Closed')) {
				statusUpdatedERs.add(er);
			}
		}
		if (!statusUpdatedERs.isEmpty()) {
			updateServiceCaseLowestERStatus(statusUpdatedERs);
		}

		List<EntryRequirement__c> llnResits = new List<EntryRequirement__c>();
		for (EntryRequirement__c record : records) {
			if (record.RecordTypeId == llnRecordTypeId && record.Status__c == 'LLN - Resit') {
				llnResits.add(record);
			}
		}

		if (!llnResits.isEmpty()) {
			updateServiceCaseResitCount(llnResits);
		}
	}


	/**
	 * @description Handles all after update operations for Entry Requirement records
	 */
	public static void onAfterUpdate(List<EntryRequirement__c> records, Map<Id, EntryRequirement__c> oldMap) {
		List<EntryRequirement__c> c3gStatusUpdatedERs = new List<EntryRequirement__c>();
		List<EntryRequirement__c> statusUpdatedERs = new List<EntryRequirement__c>();
		List<EntryRequirement__c> confirmedYear12ERs = new List<EntryRequirement__c>();
		List<EntryRequirement__c> failedERs = new List<EntryRequirement__c>();	
		List<EntryRequirement__c> forMilestoneCreation = new List<EntryRequirement__c>();

		for(EntryRequirement__c er : records) {
			if(!VFHEntryRequirementRecordTypeIds.contains(er.RecordTypeId) 
					&& (er.Status__c != oldMap.get(er.Id).Status__c && (statusOrder.keySet().contains(er.Status__c) || er.Status__c == 'Closed'))) {
				c3gStatusUpdatedERs.add(er);
			}

			if(er.Status__c != oldMap.get(er.Id).Status__c) {
				statusUpdatedERs.add(er);
			}

			if(er.RecordTypeId == year12ReqRTID && er.Status__c != oldMap.get(er.Id).Status__c && er.Status__c == 'Confirmed') {
				confirmedYear12ERs.add(er);
			}

			if (er.Status__c != oldMap.get(er.Id).Status__c && ((er.RecordTypeId == llnRecordTypeId && er.Status__c == 'LLN - Confirmed') || er.Status__c == 'Confirmed')) {
				System.debug('Status ::: ' + er.Status__c + ' ::: ' + oldMap.get(er.Id).Status__c);
				if (!forMilestoneCreationIds.contains(er.Id)) {
					forMilestoneCreation.add(er);
					forMilestoneCreationIds.add(er.Id);
				}
			}
		}

		if (!forMilestoneCreation.isEmpty()) {
			createMilestones(forMilestoneCreation);
		}
		if (!statusUpdatedERs.isEmpty()) {
			evaluateAllEntryRequirementsMet(statusUpdatedERs);
		}
		if (!c3gStatusUpdatedERs.isEmpty()) {
			updateServiceCaseLowestERStatus(c3gStatusUpdatedERs);
		}
		if (!confirmedYear12ERs.isEmpty()) {
			closeLLNEntryRequirements(confirmedYear12ERs);
		}
		processUpdatedACSFLevels(records, oldMap);
	}

	private static void updateServiceCaseResitCount(List<EntryRequirement__c> records) {
		Set<Id> serviceCaseIds = new Set<Id>();
		Map<Id, Integer> incrementMap = new Map<Id, Integer>();
		for (EntryRequirement__c record : records) {
			serviceCaseIds.add(record.ServiceCaseId__c);
			if (incrementMap.containsKey(record.ServiceCaseId__c)) {
				incrementMap.put(record.ServiceCaseId__c, incrementMap.get(record.ServiceCaseId__c) + 1);
			} else {
				incrementMap.put(record.ServiceCaseId__c, 1);
			}
		}

		List<Service_Cases__c> serviceCases = [SELECT Id, LLNResitCount__c FROM Service_Cases__c WHERE Id IN :serviceCaseIds];
		for (Service_Cases__c serviceCase : serviceCases) {
			if (serviceCase.LLNResitCount__c == null) {
				serviceCase.LLNResitCount__c = 0;
			} 
			serviceCase.LLNResitCount__c += incrementMap.get(serviceCase.Id);
		}

		update serviceCases;
	}

	/**
	 * @description Validates the Entry Requirement records that they should have an attached document first before marking as sighted
	 * @author Ranyel Maliwanag
	 * @date 14.MAR.2016
	 */
	private static void checkDocumentCount(List<EntryRequirement__c> records) {
		Map<Id, EntryRequirement__c> recordMap = new Map<Id, EntryRequirement__c>();
		for (EntryRequirement__c record : records) {
			recordMap.put(record.Id, record);
		}

		for (EntryRequirement__c record : [SELECT Id, (SELECT Id FROM Attachments) FROM EntryRequirement__c WHERE Id IN :recordMap.keySet()]) {
			if (record.Attachments.isEmpty()) {
				recordMap.get(record.Id).addError('Documents must be attached to this record before you can mark it as "Sighted"');
			}
		}
	}

	/**
     * @description 'Awaiting Upload' < 'Awaiting Verification' < 'Confirmed'. Whenever an ER status was changed, update its service case LowestERStatus field 
     * @author Biao Zhang
     * @date 23.FEB.2016
     */
	private static void updateServiceCaseLowestERStatus(List<EntryRequirement__c> records) {
		
		Set<Id> serviceCaseIds = new Set<Id>();
		for(EntryRequirement__c er : records) {
			serviceCaseIds.add(er.ServiceCaseId__c);
		}

		List<Service_Cases__c> scs = [SELECT Id, LowestERStatus__c, (SELECT Status__c FROM EntryRequirements__r) FROM Service_Cases__c WHERE Id IN: serviceCaseIds AND (LowestERStatus__c IN :statusOrder.keySet() OR LowestERStatus__c = null)];

		for(Service_Cases__c sc : scs) {
			String lowestERStatus = 'Confirmed';
			for(EntryRequirement__c er : sc.EntryRequirements__r) {
				if(statusOrder.keySet().contains(er.Status__c) && statusOrder.get(er.Status__c) < statusOrder.get(lowestERStatus)) {
					lowestERStatus = er.Status__c;
				}
			}

			sc.LowestERStatus__c = lowestERStatus;
		}

		update scs;
	}

	/**
     * @description For VHF enrolment which is going to submit year 12 certificate. When 'Year 12 Certificate' entry requirement is submitted, update the status of the LLN entry requirement under the same service case from 'LLN - Pending YR12 Cert' to 'LLN - Closed'
     * @author Biao Zhang
     * @date 25.FEB.2016
     */
	private static void closeLLNEntryRequirements(List<EntryRequirement__c> records) {
		Set<Id> serviceCaseIds = new Set<Id>();
		for(EntryRequirement__c er : records) {
			serviceCaseIds.add(er.ServiceCaseId__c);
		}

		List<EntryRequirement__c> closeLLNERs = [Select Status__c, Optional__c FROM EntryRequirement__c WHERE ServiceCaseId__c in :serviceCaseIds AND RecordTypeId = :llnRecordTypeId AND Status__c = 'LLN - Pending YR12 Cert'];

		for(EntryRequirement__c er : closeLLNERs) {
			er.Status__c = 'LLN - Closed';
			er.Optional__c = true;
		}

		update closeLLNERs;
	}


	/**
	 * @description Determines whether all entry requirements have been met/confirmed for their related service case and enrolment records
	 * @author Ranyel Maliwanag
	 * @date 17.MAR.2016
	 */
	private static void evaluateAllEntryRequirementsMet(List<EntryRequirement__c> records) {
		// Collect set of Enrolment Ids
		Set<Id> enrolmentIds = new Set<Id>();

		// Collect set of Service Case Ids
		Set<Id> serviceCaseIds = new Set<Id>();

		for (EntryRequirement__c record : records) {
			if (String.isNotBlank(record.EnrolmentId__c)) {
				enrolmentIds.add(record.EnrolmentId__c);
			}

			if (String.isNotBlank(record.ServiceCaseId__c)) {
				serviceCaseIds.add(record.ServiceCaseId__c);
			}
		}

		// Query Enrolments with Entry Requirements
		List<Enrolment__c> enrolments = [SELECT Id, (SELECT Id, Status__c, RecordTypeId FROM EntryRequirements__r WHERE Optional__c = false) FROM Enrolment__c WHERE Id IN :enrolmentIds];
		for (Enrolment__c enrolment : enrolments) {
			Boolean isAllEntryRequirementsMet = true;
			Boolean isLLNPreviouslySatisfied = false;

			for (EntryRequirement__c requirement : enrolment.EntryRequirements__r) {
				if (requirement.RecordTypeId == llnRecordTypeId && requirement.Status__c == 'LLN - Confirmed') {
					isLLNPreviouslySatisfied = true;
				}
			}

			for (EntryRequirement__c requirement : enrolment.EntryRequirements__r) {
				if ((requirement.RecordTypeId != llnRecordTypeId && requirement.Status__c != 'Confirmed') || (requirement.RecordTypeId == llnRecordTypeId && !isLLNPreviouslySatisfied)) {
					isAllEntryRequirementsMet = false;
					break;
				}
			}
			enrolment.AreAllEntryRequirementsMet__c = isAllEntryRequirementsMet;
		}
		update enrolments;

		// Query Service Cases with Entry Requirements
		List<Service_Cases__c> serviceCases = [SELECT Id, (SELECT Id, Status__c, RecordTypeId FROM EntryRequirements__r WHERE Optional__c = false) FROM Service_Cases__c WHERE Id IN :serviceCaseIds];
		for (Service_Cases__c serviceCase : serviceCases) {
			Boolean isAllEntryRequirementsMet = true;
			Boolean isLLNPreviouslySatisfied = false;

			for (EntryRequirement__c requirement : serviceCase.EntryRequirements__r) {
				if (requirement.RecordTypeId == llnRecordTypeId && requirement.Status__c == 'LLN - Confirmed') {
					isLLNPreviouslySatisfied = true;
				}
			}

			for (EntryRequirement__c requirement : serviceCase.EntryRequirements__r) {
				if ((requirement.RecordTypeId != llnRecordTypeId && requirement.Status__c != 'Confirmed') || (requirement.RecordTypeId == llnRecordTypeId && !isLLNPreviouslySatisfied)) {
					isAllEntryRequirementsMet = false;
					break;
				}
			}
			serviceCase.AreAllEntryRequirementsMet__c = isAllEntryRequirementsMet;
		}
		update serviceCases;
	}


	/**
	 * @description Processes LLN Entry requirements with updated Pass or Fail ACSF Levels. Those that pass will have related Year 12 Entry Requirement closed. Those that fail will have all other related Entry Requirements and Service Case closed and an Opportunity created for it if it doesn't have a Year 12 Entry Requirement
	 * @author Ranyel Maliwanag
	 * @date 17.MAR.2016
	 * @history
	 *		 31.MAR.2016	Ranyel Maliwanag	- Added field update to Service Case to set the Secondary LLN Support Strategy field to `6 - Level 2 Support`
	 *		 14.APR.2016	Ranyel Maliwanag	- Updated handling for ACSF Level 2 Scenario 1 to move the Service Case to the Service Cases to be closed queue instead of being fast tracked
	 */
	private static void processUpdatedACSFLevels(List<EntryRequirement__c> records, Map<Id, EntryRequirement__c> oldMap) {
		// Routing lists of Entry Requirements
		List<EntryRequirement__c> acsfUpdatedERs = new List<EntryRequirement__c>();
		List<EntryRequirement__c> preFastTrackedERs = new List<EntryRequirement__c>();

		// Service case Id sets. `allSCIds` are used to query all related Service Cases while `passingSCIds` and `failingSCIds` are used to determine the state of the service cases based on updated ACSF Levels
		Set<Id> passingSCIds = new Set<Id>();
		Set<Id> failingSCIds = new Set<Id>();
		Set<Id> allSCIds = new Set<Id>();

		// Id map of Service Case records to be updated. Using a map instead of list to avoid duplicate Ids during the update DML call
		Map<Id, Service_Cases__c> serviceCaseUpdateMap = new Map<Id, Service_Cases__c>();

		// Mapping of Entry Requirement and the highest score that got updated
		Map<Id, Integer> llnERScoreMap = new Map<Id, Integer>();


		for (EntryRequirement__c er : records) {
			// Condition: Entry Requirement has LLN record type and ACSF Level value has been updated with either a Pass or a Fail
			if (er.RecordTypeId == llnRecordTypeId && 
					er.LLNACSFLevel__c != oldMap.get(er.Id).LLNACSFLevel__c && 
					(er.LLNACSFLevel__c == 'Fail' || er.LLNACSFLevel__c == 'Pass')) {

				// Service Case Id collection
				if (er.LLNACSFLevel__c == 'Fail') {
					failingSCIds.add(er.ServiceCaseId__c);
				} else if (er.LLNACSFLevel__c == 'Pass') {
					passingSCIds.add(er.ServiceCaseId__c);
				}
				allSCIds.add(er.ServiceCaseId__c);

				// Routing of Entry Requirement record for processing of updated ACSF Levels
				acsfUpdatedERs.add(er);
			}

			// Condition: Entry Requirement has LLN record type and either Literacy Level or Numeracy Level is updated with a score of 2 or the Is Enrolment Fast tracked gets ticked
			if (er.RecordTypeId == llnRecordTypeId && 
					(er.Literacy_Level__c != oldMap.get(er.Id).Literacy_Level__c && 
						String.isNotBlank(er.Literacy_Level__c) && 
						er.Literacy_Level__c.right(1).isNumeric() && 
						Integer.valueOf(er.Literacy_Level__c.right(1)) == 2) ||
					(er.Numeracy_Level__c != oldMap.get(er.Id).Numeracy_Level__c &&
						String.isNotBlank(er.Numeracy_Level__c) && 
						er.Numeracy_Level__c.right(1).isNumeric() && 
						Integer.valueOf(er.Numeracy_Level__c.right(1)) == 2) ||
					(er.IsReadyforFastTrack__c && er.IsReadyforFastTrack__c != oldMap.get(er.Id).IsReadyforFastTrack__c)) {

				llnERScoreMap.put(er.Id, 2);

				// Service Case Id collection
				allSCIds.add(er.ServiceCaseId__c);

				// Routing of Entry Requirement record for evaluation of fast track enrolment
				preFastTrackedERs.add(er);
			}
		}

		// First level gate: Check if there are any Entry Requirement record to process before we do any further SOQL or DML calls
		if (!acsfUpdatedERs.isEmpty() || !preFastTrackedERs.isEmpty()) {
			// General Service Case query
			String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Service_Cases__c, ', Account_Student_Name__r.Name, (SELECT Id, ApplicationFormId__r.StateId__r.Short_Name__c, RecordTypeId, RecordType.Name FROM EntryRequirements__r), (SELECT Id FROM Opportunities__r) ');
			queryString += 'WHERE Id IN :allSCIds';
			List<Service_Cases__c> serviceCases = Database.query(queryString);

			// Build Service Case reference map
			Map<Id, Service_Cases__c> serviceCaseMap = new Map<Id, Service_Cases__c>();
			for (Service_Cases__c serviceCase : serviceCases) {
				serviceCaseMap.put(serviceCase.Id, serviceCase);
			}
			
			// Processing of possible fast-tracked Entry Requirement records
			if (!preFastTrackedERs.isEmpty()) {
				List<Service_Cases__c> toBeClosed = new List<Service_Cases__c>();

				for (EntryRequirement__c record : preFastTrackedERs) {
					if (serviceCaseMap.containsKey(record.ServiceCaseId__c) && !serviceCaseMap.get(record.ServiceCaseId__c).ParentQualificationName__c.containsIgnoreCase('Nursing')) {
						// Condition: Non-nursing qualification

						if (serviceCaseMap.containsKey(record.ServiceCaseId__c) && String.isBlank(serviceCaseMap.get(record.ServiceCaseId__c).Primary_LLN_Support_Strategies__c)) {
							// Condition: No Primary LLN Support Strategy

							// Flag to determine if we need to update Service Case
							Boolean isServiceCaseQualified = false;

							// Check if ACSF Level has been updated to Resit
							if (record.LLNACSFLevel__c != oldMap.get(record.Id).LLNACSFLevel__c && record.LLNACSFLevel__c == 'Resit') {
								isServiceCaseQualified = true;
							}

							// Check if the Entry Requirement has been flagged as Ready for Fast Track (i.e., it has been 24 hours since the Level 2 Score came through)
							if (record.IsReadyForFastTrack__c && record.IsReadyForFastTrack__c != oldMap.get(record.Id).IsReadyForFastTrack__c && record.LLNACSFLevel__c == 'Pending') {
								isServiceCaseQualified = true;
							}

							if (isServiceCaseQualified) {
								// Update Service Case record to be fast tracked
								Service_Cases__c serviceCase = serviceCaseMap.get(record.ServiceCaseId__c);
								serviceCase.IsEnrolmentFastTracked__c = true;
								serviceCase.Secondary_LLN_Support_Strategies__c = '6 - Level 2 Support';
								serviceCaseUpdateMap.put(serviceCase.Id, serviceCase);
							}
						} else if (serviceCaseMap.containsKey(record.ServiceCaseId__c) && String.isNotBlank(serviceCaseMap.get(record.ServiceCaseId__c).Primary_LLN_Support_Strategies__c) && serviceCaseMap.get(record.ServiceCaseId__c).Primary_LLN_Support_Strategies__c != '2 - Trainer Lead Support') {
							// Condition: Primary LLN Support Strategy is populated and is not Trainer Lead

							// Flag to determine if we need to update Service Case
							Boolean isServiceCaseQualified = false;

							// If the score == 2, we need to count the resit
							if (record.Status__c != 'LLN - Closed' && llnERScoreMap.get(record.Id) == 2 && serviceCaseMap.get(record.ServiceCaseId__c).LLNResitCount__c >= 1) {
								isServiceCaseQualified = true;
							}

							if (isServiceCaseQualified) {
								Service_Cases__c serviceCase = serviceCaseMap.get(record.ServiceCaseId__c);
								toBeClosed.add(serviceCase);
							}
						}
					}
				}

				if (!toBeClosed.isEmpty()) {
					Group toBeClosedQueue = [SELECT Id FROM Group WHERE DeveloperName = 'Service_Cases_to_be_closed' AND Type = 'Queue'];
					for (Service_Cases__c serviceCase : toBeClosed) {
						serviceCase.OwnerId = toBeClosedQueue.Id;
						serviceCaseUpdateMap.put(serviceCase.Id, serviceCase);
					}
				}
			}

			if (!acsfUpdatedERs.isEmpty()) {
				List<EntryRequirement__c> passingRecords = new List<EntryRequirement__c>();
				List<EntryRequirement__c> failingRecords = new List<EntryRequirement__c>();
				List<EntryRequirement__c> recordsForUpdate = new List<EntryRequirement__c>();
				List<Opportunity> opportunitiesForInsert = new List<Opportunity>();

				// Create a map of <Service Case Id> : <Map of Literacy and Numeracy Levels>
				Map<Id, Map<String, String>> serviceCaseLevels = new Map<Id, Map<String, String>>();

				for (EntryRequirement__c record : records) {
					if (record.LLNACSFLevel__c == 'Pass') {
						passingRecords.add(record);
						passingSCIds.add(record.ServiceCaseId__c);
					} else if (record.LLNACSFLevel__c == 'Fail') {
						failingRecords.add(record);
						failingSCIds.add(record.ServiceCaseId__c);
						serviceCaseLevels.put(record.ServiceCaseId__c, new Map<String, String>{
								'Literacy' => record.Literacy_Level__c,
								'Numeracy' => record.Numeracy_Level__c
							});
					}
					allSCIds.add(record.ServiceCaseId__c);
				}

				if (!passingRecords.isEmpty()) {
					for (EntryRequirement__c record : [SELECT Id, Status__c FROM EntryRequirement__c WHERE ServiceCaseId__c IN :passingSCIds AND RecordTypeId = :year12ReqRTID AND Status__c = 'Awaiting Upload']) {
						record.Status__c = 'Closed';
						recordsForUpdate.add(record);
					}
				}

				if (!failingRecords.isEmpty()) {
					// Query Service Cases with Entry Requirements
					List<Service_Cases__c> failingServiceCases = new List<Service_Cases__c>();

					for (Service_Cases__c serviceCase : serviceCases) {
						if (failingSCIds.contains(serviceCase.Id)) {
							Boolean hasYear12ER = false;
							Boolean isQueensland = false;
							for (EntryRequirement__c requirement : serviceCase.EntryRequirements__r) {
								if (requirement.RecordTypeId == year12ReqRTID) {
									hasYear12ER = true;
									break;
								}
								if (requirement.ApplicationFormId__r.StateId__r.Short_Name__c == 'QLD') {
									isQueensland = true;
								}
							}

							if (!hasYear12ER && isQueensland) {
								// Check if the Service Case update map already has a pending update for this service case record so as not to overwrite potential changes made from previous update
								if (serviceCaseUpdateMap.containsKey(serviceCase.Id)) {
									serviceCase = serviceCaseUpdateMap.get(serviceCase.Id);
								}

								serviceCase.Status__c = 'Closed';
								serviceCase.Has_Enrolment_been_created__c = 'No - Student does not wish to continue with Enrolment';
								serviceCase.Application_Close_Reasons__c = 'Ineligible for VFH';
								// Close Entry Requirements
								for (EntryRequirement__c requirement : serviceCase.EntryRequirements__r) {
									if (requirement.RecordType.Name == 'LLN') {
										requirement.Status__c = 'LLN - Closed';
									} else {
										requirement.Status__c = 'Closed';
									}
									recordsForUpdate.add(requirement);
								}
								Opportunity certificateOpp = new Opportunity();
								if (!serviceCase.Opportunities__r.isEmpty()) {
									certificateOpp = serviceCase.Opportunities__r[0];
								} 
								certificateOpp.Service_Case__c = serviceCase.Id;
								certificateOpp.AccountId = serviceCase.Account_Student_Name__c;
								certificateOpp.RecordTypeId = certifcateOppRTID;
								certificateOpp.Name = serviceCase.Account_Student_Name__r.Name;
								certificateOpp.StageName = 'Prospecting';
								certificateOpp.CloseDate = Date.today().addMonths(1);
								certificateOpp.Referral_Stream__c = 'ACSF Level 1';
								certificateOpp.LLN_Literacy_Level__c = serviceCaseLevels.get(serviceCase.Id).get('Literacy');
								certificateOpp.LLN_Numeracy_Level__c = serviceCaseLevels.get(serviceCase.Id).get('Numeracy');
								opportunitiesForInsert.add(certificateOpp);
								serviceCaseUpdateMap.put(serviceCase.Id, serviceCase);
							}
						}
					}
				}
				update recordsForUpdate;
				upsert opportunitiesForInsert;
			}

			update serviceCaseUpdateMap.values();
		}
	}


	/**
	 * @description Creates Enrolment Application Milestone records for each Entry Requirement record that has been closed off
	 * @author Ranyel Maliwanag
	 * @date 29.MAR.2016
	 */
	private static void createMilestones(List<EntryRequirement__c> records) {
		List<EnrolmentApplicationMilestone__c> milestonesForInsert = new List<EnrolmentApplicationMilestone__c>();

		for (EntryRequirement__c record : records) {
			EnrolmentApplicationMilestone__c milestone = new EnrolmentApplicationMilestone__c(
					ServiceCaseId__c = record.ServiceCaseId__c,
					EntryRequirementId__c = record.Id,
					MilestoneEndDate__c = Datetime.now()
				);
			milestonesForInsert.add(milestone);
		}

		insert milestonesForInsert;
	}
}