/**
 * @description Test class for the Census Eligibility Notification Batches
 * @author Yuri Gribanov
 * @date n/a
 * @history
 *       18.APR.2016    Ranyel Maliwanag    - Corrected assertions for the Stage Eligibility Notification batch
 */
@isTest 
public class CensusEligibilityNotificationBatchTest {
    
    private static testMethod void testCensusDate() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u) {
            Census__c census = SASTestUtilities.createCensus();            
            census.CensusDate__c = Date.Today().addDays(+16);
            insert census;
            
            Stage__c stage1 = new Stage__c(Name = 'Stage 1');
            
            insert stage1;
            
            Intake__c intake = [Select Stage__c From Intake__c];
            
            intake.Stage__c = stage1.Id;
            
            update intake;
            
            Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
            
            enrl.Assigned_Trainer__c = u.Id;
            
            update enrl;            
                
            test.StartTest();
                
                CensusEligibilityNotificationBatch b = new CensusEligibilityNotificationBatch();
    			database.executebatch(b, 1);
                
            test.StopTest();
                
            
                
            Task testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() LIMIT 1];
            
             //assert task
            
            system.assertEquals(testTask.RecordtypeId, Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId());
            system.assertEquals('Approaching C1 16 days until C1', testTask.Subject);
            system.assertEquals(testTask.ownerId, u.Id);
            system.assertEquals(testTask.whoId, enrl.Student__r.PersonContactId);
            system.assertEquals(testTask.whatId, enrl.Id);
          
            Census__c record = [SELECT Id, CensusTaskId__c FROM Census__c WHERE Id = :census.Id];
            System.assert(String.isNotBlank(record.CensusTaskId__c));
        }
        
    }
    
    private static testmethod void testCensusDateSchedule() {
       Test.StartTest();
          CensusEligibilityNotificationScheduler scheduler = new CensusEligibilityNotificationScheduler();
           
           String sch = '0 0 23 * * ?';
           system.schedule('Test Territory Check', sch, scheduler);
       Test.StopTest();
    }
    
    private static testmethod void testCensusEndDate() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u) {
            Census__c census = SASTestUtilities.createCensus();            
            
            insert census;
            
            Stage__c stage2 = new Stage__c(Name = 'Stage 2');
                
            insert stage2;
                
            Intake__c intake = [Select Stage__c, End_date__c From Intake__c];
                
            intake.End_Date__c = Date.Today().addDays(+16);
            
            update intake;
            
            
            
            intake.Stage__c = stage2.Id;
            
            update intake;
            
            Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
            
            enrl.Assigned_Trainer__c = u.Id;
            
            update enrl;

            List<Task> testTaskList = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() AND Subject = 'Non Eligible Student'];

            System.assert(testTaskList.isEmpty());          
                
            test.StartTest();
                
                CensusEligibilityNotificationBatch2 b = new CensusEligibilityNotificationBatch2();
    			database.executebatch(b, 1);
                
            test.StopTest();
                
            Task testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() LIMIT 1];
            
             //assert task
            
            system.assertEquals(testTask.RecordtypeId, Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId());
            system.assertEquals(testTask.Subject, 'Stage Notification 16 days until end of Stage 2');
            system.assertEquals(testTask.ownerId, u.Id);
            system.assertEquals(testTask.whoId, enrl.Student__r.PersonContactId);
            system.assertEquals(testTask.whatId, enrl.Id);
          
            Census__c record = [SELECT Id, StageTaskId__c FROM Census__c WHERE Id = :census.Id];
            System.assert(String.isNotBlank(record.StageTaskId__c));
        }
        
    }

    private static testmethod void testCensusEndDateNegative() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u) {
            Census__c census = SASTestUtilities.createCensus();            
            
            insert census;
            
            Stage__c stage2 = new Stage__c(Name = 'Stage 2');
                
            insert stage2;
                
            Intake__c intake = [Select Stage__c, End_date__c From Intake__c];
                
            intake.End_Date__c = Date.Today().addDays(+16);
            
            update intake;
            
            
            
            intake.Stage__c = stage2.Id;
            
            update intake;
            
            Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
            
            enrl.Assigned_Trainer__c = u.Id;
            
            update enrl;            


            AssessmentAttempt__c attempt = new AssessmentAttempt__c(
                    Census__c = census.Id,
                    AccountId__c = enrl.Student__c,
                    BBAssessmentRecordId__c = 'BBAttemptx00'
                );
            insert attempt;

            List<Task> testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() AND Subject = 'Non Eligible Student'];
            System.assert(testTask.isEmpty());
                
            test.StartTest();
                
                CensusEligibilityNotificationBatch2 b = new CensusEligibilityNotificationBatch2();
                database.executebatch(b);
                
            test.StopTest();
                
            testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() AND Subject = 'Non Eligible Student'];
            
            //assert task
            System.assert(testTask.isEmpty());
        }
        
    }
    
    private static testmethod void testCensusDateSchedule2() {
       Test.StartTest();
          CensusEligibilityNotificationScheduler2 scheduler = new CensusEligibilityNotificationScheduler2();
           
           String sch = '0 0 23 * * ?';
           system.schedule('Test Territory Check', sch, scheduler);
       Test.StopTest();
    }

}