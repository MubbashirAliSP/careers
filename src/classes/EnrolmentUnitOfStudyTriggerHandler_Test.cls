/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for  Enrolment Unit Of Study Trigger Management 
Test Class:
History
11/01/2013      Created     Sairah Hadjinoor
28/01/2013      Update      Janella Canlas
04/03/2013      Update      Janella Canlas
------------------------------------------------------------*/
@isTest
private class EnrolmentUnitOfStudyTriggerHandler_Test {
    
    /*
        Test EnrolmentUnitofStudyTriggerHandler.populateFields with HELP Debt = '00000000'
    */
    
    static testMethod void populateFields() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Enrolment_Intake_Unit__c enrolmentIntakeUnit = new Enrolment_Intake_Unit__c();
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Credit_status_Higher_Ed_provider_code__c creditStatus = new Credit_status_Higher_Ed_provider_code__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType(); 
                enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'AUSTRALIA', 'Unknown', '', false, del.Id,attend.Id);
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                update enrl;
                result = TestCoverageUtilityClass.createResult(state.Id);
                enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
                fundStream = TestCoverageUtilityClass.createFundingStream();
                program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
                intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
                intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
                creditStatus = TestCoverageUtilityClass.createCreditStatus();
                List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c
                            where Enrolment__c =: enrl.Id
                            ];
                for(Enrolment_Unit__c eu: euList){
                    eu.Start_Date__c = Date.TODAY() - 25;
                    eu.End_Date__c = Date.TODAY() - 1;
                }            
                update euList;
                Set<Id> euIds = new Set<Id>();
                for(Enrolment_Unit__c eu : euList){
                    euIds.add(eu.Id);
                    Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
                    eiu = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, eu.Id);
                    eiuList.add(eiu);
                }
                insert eiuList;
                system.debug('euIds*****'+ euIds);
                List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
                
                
                Field_of_Education__c fieldOfEducation = new Field_of_Education__c();
                Id recrdType = [select Id from RecordType where Name = 'Division' LIMIT 1].Id;
                fieldOfEducation.RecordTypeId = recrdType;
                fieldOfEducation.Name = 'No credit was offered for VET';
                insert fieldOfEducation;
                
                Education_Provider_Type__c educatorProviderType = new Education_Provider_Type__c();
                educatorProviderType.Name = 'No credit/RPL was offered for VET';
                educatorProviderType.Code__c = '12';
                insert educatorProviderType;
                
                Level_of_Prior_Study_for_RPL_Type__c level = new Level_of_Prior_Study_for_RPL_Type__c();
                level.Name = 'No credit/RPL was offered for VET';
                level.Code__c = '12';
                insert level;
                
                Credit_status_Higher_Ed_provider_code__c credit = new Credit_status_Higher_Ed_provider_code__c();
                credit.Name = 'No higher education credit offered';
                credit.Code__c = '12';
                insert credit;
                
                Recognition_of_Prior_Learning_Status__c rec = new Recognition_of_Prior_Learning_Status__c();
                rec.Name = 'Unit of study is NOT an RPL unit of study';
                rec.Code__c = '12';
                insert rec;
                
                /*RPL_prior_study_Types__c rpl = new RPL_prior_study_Types__c();
                rpl.Name = 'No credit/RPL was offered';
                rpl.Code__c = '0100';
                insert rpl;*/
                
                RPL_prior_study_Types__c rpl2 = new RPL_prior_study_Types__c();
                rpl2.Name = 'No credit/RPL was offered';
                rpl2.Code__c = '1234';
                insert rpl2;
                
                //Id rplCode = TestCoverageUtilityClass.queryRPL('0100');
                
                for(Enrolment_Unit__c eu : euList){
                    Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
                    e = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fld.Id);
                    e.Credit_status_Higher_Ed_provider_code__c = creditStatus.Id;
                    e.RPL_details_of_prior_study__c = rpl2.Id;
                    e.Start_Date__c = date.newInstance(2012,1,1);
                    euosList.add(e);
                }
                insert euosList;
                system.debug('EUOSLIST*****'+ euosList);
                //enrl.Start_Date__c = date.today() + 100;
                //update enrl;
                
            test.stopTest();
        }
    }
    /*
        Test EnrolmentUnitofStudyTriggerHandler.populateFields with Student Identifier 231
    */
    
    static testMethod void populateFields231() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Credit_status_Higher_Ed_provider_code__c creditStatus = new Credit_status_Higher_Ed_provider_code__c();
        Level_of_Prior_Study_for_RPL_Type__c levelPrior = new Level_of_Prior_Study_for_RPL_Type__c();
        Education_Provider_Type__c educationProvider = new Education_Provider_Type__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'JAPAN', 'Yes', 'Listening;Visual;Mobility;Medical;Other;Learning',true,del.Id,attend.Id);
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                update enrl;
                result = TestCoverageUtilityClass.createResult(state.Id);
                enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
                levelPrior = TestCoverageUtilityClass.createLevelPriorStudy();
                creditStatus = TestCoverageUtilityClass.createCreditStatus();
                educationProvider = TestCoverageUtilityClass.createEducationProviderType();
                List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c
                            where Enrolment__c =: enrl.Id
                            ];
                Set<Id> euIds = new Set<Id>();
                for(Enrolment_Unit__c eu : euList){
                    euIds.add(eu.Id);
                    eu.Start_Date__c = Date.TODAY() - 25;
                    eu.End_Date__c = Date.TODAY() - 1;
                }
                update euList;
                system.debug('euIds*****'+ euIds);
                List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
                //Id rplCode = TestCoverageUtilityClass.queryRPL('0200');
                Id statusCode = TestCoverageUtilityClass.insertStudentStatusCodeType('231');
                
                Field_of_Education__c fieldOfEducation = new Field_of_Education__c();
                Id recrdType = [select Id from RecordType where Name = 'Division' LIMIT 1].Id;
                fieldOfEducation.RecordTypeId = recrdType;
                fieldOfEducation.Name = 'No credit was offered for VET';
                insert fieldOfEducation;
                
                Education_Provider_Type__c educatorProviderType = new Education_Provider_Type__c();
                educatorProviderType.Name = 'No credit/RPL was offered for VET';
                educatorProviderType.Code__c = '12';
                insert educatorProviderType;
                
                Level_of_Prior_Study_for_RPL_Type__c level = new Level_of_Prior_Study_for_RPL_Type__c();
                level.Name = 'No credit/RPL was offered for VET';
                level.Code__c = '12';
                insert level;
                
                Credit_status_Higher_Ed_provider_code__c credit = new Credit_status_Higher_Ed_provider_code__c();
                credit.Name = 'No higher education credit offered';
                credit.Code__c = '12';
                insert credit;
                
                Recognition_of_Prior_Learning_Status__c rec = new Recognition_of_Prior_Learning_Status__c();
                rec.Name = 'Unit of study is NOT an RPL unit of study';
                rec.Code__c = '12';
                insert rec;
                
                RPL_prior_study_Types__c rpl = new RPL_prior_study_Types__c();
                rpl.Name = 'No credit/RPL was offered';
                rpl.Code__c = '12';
                insert rpl;
                
                for(Enrolment_Unit__c eu : euList){
                    Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
                    e = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fld.Id);
                    e.Credit_status_Higher_Ed_provider_code__c = creditStatus.Id;
                    e.Start_Date__c = date.today() + 1;
                    e.Student_Status_Code__c = statusCode; 
                    euosList.add(e);
                }
                insert euosList;
                system.debug('EUOSLIST*****'+ euosList);
                //enrl.Start_Date__c = date.today() + 100;
                //update enrl;
                
            test.stopTest();
        }
    }
    /*
        Test EnrolmentUnitofStudyTriggerHandler.populateFields with Student Identifier 231 less than Start Date
    */
    
    static testMethod void populateFields231lessStartDate() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Credit_status_Higher_Ed_provider_code__c creditStatus = new Credit_status_Higher_Ed_provider_code__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'AUSTRALIA', 'Yes', 'Hearing', false, del.Id, attend.Id);
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                update enrl;
                result = TestCoverageUtilityClass.createResult(state.Id);
                enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
                creditStatus = TestCoverageUtilityClass.createCreditStatus();
                List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c
                            where Enrolment__c =: enrl.Id
                            ];
                Set<Id> euIds = new Set<Id>();
                for(Enrolment_Unit__c eu : euList){
                    euIds.add(eu.Id);
                    eu.Start_Date__c = Date.TODAY() - 25;
                    eu.End_Date__c = Date.TODAY() - 1;
                }
                update euList;
                
                system.debug('euIds*****'+ euIds);
                List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
                Id statusCode = TestCoverageUtilityClass.insertStudentStatusCodeType('231');
                
                Field_of_Education__c fieldOfEducation = new Field_of_Education__c();
                Id recrdType = [select Id from RecordType where Name = 'Division' LIMIT 1].Id;
                fieldOfEducation.RecordTypeId = recrdType;
                fieldOfEducation.Name = 'No credit was offered for VET';
                insert fieldOfEducation;
                
                Education_Provider_Type__c educatorProviderType = new Education_Provider_Type__c();
                educatorProviderType.Name = 'No credit/RPL was offered for VET';
                educatorProviderType.Code__c = '12';
                insert educatorProviderType;
                
                Level_of_Prior_Study_for_RPL_Type__c level = new Level_of_Prior_Study_for_RPL_Type__c();
                level.Name = 'No credit/RPL was offered for VET';
                level.Code__c = '12';
                insert level;
                
                Credit_status_Higher_Ed_provider_code__c credit = new Credit_status_Higher_Ed_provider_code__c();
                credit.Name = 'No higher education credit offered';
                credit.Code__c = '12';
                insert credit;
                
                Recognition_of_Prior_Learning_Status__c rec = new Recognition_of_Prior_Learning_Status__c();
                rec.Name = 'Unit of study is NOT an RPL unit of study';
                rec.Code__c = '12';
                insert rec;
                
                RPL_prior_study_Types__c rpl = new RPL_prior_study_Types__c();
                rpl.Name = 'No credit/RPL was offered';
                rpl.Code__c = '12';
                insert rpl;
                
                for(Enrolment_Unit__c eu : euList){
                    Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
                    e = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fld.Id);
                    e.Credit_status_Higher_Ed_provider_code__c = creditStatus.Id;
                    e.Student_Status_Code__c = statusCode; 
                    e.Start_Date__c = date.newinstance(2011, 1, 1);
                    euosList.add(e);
                }
                insert euosList;
                system.debug('EUOSLIST*****'+ euosList);
                
            test.stopTest();
        }
    }
    /*
        Test EnrolmentUnitofStudyTriggerHandler.populateLoanFee with Student Identifier 401
    */
    
    static testMethod void populateLoanFee401() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id);
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'JAPAN', 'No', '', false, del.Id, attend.Id);
                enrl.Start_Date__c = Date.TODAY() - 50;
                enrl.End_Date__c = Date.TODAY() + 50;
                update enrl;
                result = TestCoverageUtilityClass.createResult(state.Id);
                enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
                List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c
                            where Enrolment__c =: enrl.Id
                            ];
                Set<Id> euIds = new Set<Id>();
                for(Enrolment_Unit__c eu : euList){
                    euIds.add(eu.Id);
                    eu.Start_Date__c = Date.TODAY() - 25;
                    eu.End_Date__c = Date.TODAY() - 1;
                }
                update euList;
                
                Field_of_Education__c fieldOfEducation = new Field_of_Education__c();
                Id recrdType = [select Id from RecordType where Name = 'Division' LIMIT 1].Id;
                fieldOfEducation.RecordTypeId = recrdType;
                fieldOfEducation.Name = 'No credit was offered for VET';
                insert fieldOfEducation;
                
                Education_Provider_Type__c educatorProviderType = new Education_Provider_Type__c();
                educatorProviderType.Name = 'No credit/RPL was offered for VET';
                educatorProviderType.Code__c = '12';
                insert educatorProviderType;
                
                Level_of_Prior_Study_for_RPL_Type__c level = new Level_of_Prior_Study_for_RPL_Type__c();
                level.Name = 'No credit/RPL was offered for VET';
                level.Code__c = '12';
                insert level;
                
                Credit_status_Higher_Ed_provider_code__c credit = new Credit_status_Higher_Ed_provider_code__c();
                credit.Name = 'No higher education credit offered';
                credit.Code__c = '12';
                insert credit;
                
                Recognition_of_Prior_Learning_Status__c rec = new Recognition_of_Prior_Learning_Status__c();
                rec.Name = 'Unit of study is NOT an RPL unit of study';
                rec.Code__c = '12';
                insert rec;
                
                RPL_prior_study_Types__c rpl = new RPL_prior_study_Types__c();
                rpl.Name = 'No credit/RPL was offered';
                rpl.Code__c = '12';
                insert rpl;
                
                Aboriginal_and_Torres_Strait_Code__c ats = new Aboriginal_and_Torres_Strait_Code__c();
                ats.Name = 'ats1';
                ats.Code__c = '1';
                insert ats;
                
                Aboriginal_and_Torres_Strait_Code__c ats2 = new Aboriginal_and_Torres_Strait_Code__c();
                ats2.Name = 'ats2';
                ats2.Code__c = '2';
                insert ats2;
                
                system.debug('euIds*****'+ euIds);
                List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
                Id statusCode = TestCoverageUtilityClass.insertStudentStatusCodeType('401');
                for(Enrolment_Unit__c eu : euList){
                    Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
                    e = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fld.Id);
                    e.Start_Date__c = date.today() + 1;
                    e.Student_Status_Code__c = statusCode; 
                    e.Credit_status_Higher_Ed_provider_code__c = credit.Id;
                    e.Aboriginal_and_Torres_Strait_Is_Status__c = ats.Id;
                    euosList.add(e);
                }
                insert euosList;
                system.debug('EUOSLIST*****'+ euosList);
                euosList[0].Aboriginal_and_Torres_Strait_Is_Status__c = ats2.Id;
                euosList[0].Start_Date__c = date.newInstance(2011,1,1);
                update euosList[0]; 
            test.stopTest();
        }
    }
    
    static testMethod void cloneFailedEUOS(){
               Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Enrolment_Intake_Unit__c enrolmentIntakeUnit = new Enrolment_Intake_Unit__c();
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Credit_status_Higher_Ed_provider_code__c creditStatus = new Credit_status_Higher_Ed_provider_code__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                language = TestCoverageUtilityClass.createLanguage();
                acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
                loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType(); 
                enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'AUSTRALIA', 'Unknown', '', false, del.Id,attend.Id);
                result = TestCoverageUtilityClass.createResult(state.Id);
                enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
                Completion_Status_Type__c failCompletion = TestCoverageUtilityClass.createFailCompletionStatusType();
                Enrolment_Unit_Of_Study__c euos = TestCoverageUtilityClass.createEnrolmentUnitofStudy(enrolmentUnit.Id, qunit.Id, fld.Id);
                insert euos;
                euos.result__c = failCompletion.Id;
                euos.Census_Date__c = Date.today();
                update euos;
           test.stopTest();     
        }
    }
}