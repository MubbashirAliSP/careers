public with sharing class DisabilityObject {
	
	public String student_identifier {get;set;}
	public String disablity_identifier {get;set;}
	
	public DisabilityObject(String sId, String dId) {
		student_identifier = sId;
		disablity_identifier = dId; 
	}

}