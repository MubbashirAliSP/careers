/**
 * @description Trigger handler for Enrolment object 
 * @author Janella Lauren Canlas
 * @date 11.JAN.2013
 *
 * HISTORY
 * - 11.JAN.2013	Janella Canlas - Created.
 * - 16.JAN.2013	Janella Canlas - added method deleteIntakeStudents
 * - 16.JAN.2013	Janella Canlas - moved insertIntakeStudent functionality to Schedule Student Page 2 controller
 */
 
public with sharing class ClassEnrolmentTriggerHandler {
	/*
		This method will delete the related intake student record if there are no more class 
	  	enrolment records related to the student
	*/
	public static void deleteIntakeStudents(List<Class_Enrolment__c> ceList){
		Set<Id> intakeIds = new Set<Id>();
		Set<Id> deletedCEIds = new Set<Id>();
		Set<String> studentIdentifiers = new Set<String>();
		List<Class_Enrolment__c> classEnrolmentDEL = new List<Class_Enrolment__c>();
		List<Class_Enrolment__c> remainingClassEnrolments = new List<Class_Enrolment__c>();
		List<Intake_Students__c> intakeStudents = new List<Intake_Students__c>();
		List<Intake_Students__c> intakeStudentsDEL = new List<Intake_Students__c>();
		Map<String,Class_Enrolment__c> classEnrolmentMap = new Map<String,Class_Enrolment__c>();
		for(Class_Enrolment__c c : ceList){
			deletedCEIds.add(c.Id);
			studentIdentifiers.add(c.Student_Identifier__c);
		}
		classEnrolmentDEL = [select Id, Class__r.Intake__c from Class_Enrolment__c where Id IN: deletedCEIds];
		for(Class_Enrolment__c c : classEnrolmentDEL){
			intakeIds.add(c.Class__r.Intake__c);
		}
		remainingClassEnrolments = [select Id, Class__r.Intake__c, Student_Identifier__c from Class_Enrolment__c where Id NOT IN: deletedCEIds
									AND Class__r.Intake__c IN: intakeIds
									];
		if(remainingClassEnrolments.size() > 0){
			system.debug('remainingClassEnrolments***'+remainingClassEnrolments);
			for(Class_Enrolment__c c : remainingClassEnrolments){
				classEnrolmentMap.put(c.Student_Identifier__c,c);
			}
			system.debug('classEnrolmentMap***'+classEnrolmentMap);
			intakeStudents = [select Id,Student__r.Student_Identifer__c from Intake_Students__c where Id IN: intakeIds AND Student__r.Student_Identifer__c IN: studentIdentifiers];
			system.debug('intakeStudents***'+intakeStudents);
			for(Intake_Students__c i : intakeStudents){
				if(!classEnrolmentMap.containsKey(i.Student__r.Student_Identifer__c)){
					intakeStudentsDEL.add(i);
				}
			}
			try{
				delete intakeStudentsDEL;
			}
			catch(Exception e){
				system.debug('Error deleting intake students: '+e.getMessage());
			}
		}
		else{
			intakeStudents = [select Id,Student__r.Student_Identifer__c from Intake_Students__c where Intake__c IN: intakeIds];
			for(Intake_Students__c i : intakeStudents){
					intakeStudentsDEL.add(i);
			}
			try{
				delete intakeStudentsDEL;
			}
			catch(Exception e){
				system.debug('Error deleting intake students: '+e.getMessage());
			}
		}
	}
}