/**
 * @description Intake Object Trigger Handler
 * @author     Janella Lauren Canlas
 * @Company    CloudSherpas
 * @date     14.JAN.2013
 *
 * HISTORY
 * - 22.JAN.2013  Janella Canlas    Created.
 * - 05.FEB.2013  Janella Canlas    Added method populateVFHController.
 * - 19.JUL.2013  Patrisha Sales    Optimized code.
 *									Fixed bug on updating of related Enrolment Unit of Study records.
 */
public with sharing class IntakeTriggerHandler {
  /*
    This method populates Census Date on all related EUOS records
  */
  public static void populateEUOSCensusDate(Set<Id> intakeIdSet, List<Intake__c> intakeList){
    List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();
    List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
    Set<Id> euIds = new Set<Id>();
    Map<Id,Date> euCensusMap = new Map<Id,Date>();
    
    //create map of enrolment unit ids and corresponding census date
    
    for (Enrolment_Intake_Unit__c eiu : [select Id, Enrolment_Unit__c,
    										Intake_Unit__r.Intake__c, Intake_Unit__r.Intake__r.Census_Date__c 
            								from Enrolment_Intake_Unit__c 
            								where Intake_Unit__r.Intake__c IN: intakeIdSet]) {
    	if (eiu.Enrolment_Unit__c!=null && !euCensusMap.containsKey(eiu.Enrolment_Unit__c)) {
    		euCensusMap.put(eiu.Enrolment_Unit__c, eiu.Intake_Unit__r.Intake__r.Census_Date__c);
    	}        								
    }

    //query optimized to avoid Salesforce query limit - Patrisha Sales
    //populate census date on euos
    for(Enrolment_Unit_of_Study__c euos : [select Id, Census_Date__c, Enrolment_Unit__c 
    										FROM Enrolment_Unit_of_Study__c 
    										WHERE Enrolment_Unit__c IN :euCensusMap.keySet() and Census_Date__c!= null]){
    	if(euCensusMap.containsKey(euos.Enrolment_Unit__c)){
		    euos.Census_Date__c = euCensusMap.get(euos.Enrolment_Unit__c);
		    euosList.add(euos);
    	}
    }
    //update euos records
    try{
    	if (euosList.size()>0)
      		update euosList;}catch(Exception e){system.debug('ERROR: '+e.getMessage());
    }
  }
  
  /*
    This will populate VFH Controller Id upon Insert/update of Intake
  */
  public static void populateVFHController(Set<Id> intakeIdSet, List<Intake__c> intakeList){
    VFH_Controller__c vfh = new VFH_Controller__c();
    vfh = [select Id from VFH_Controller__c LIMIT 1];
    for(Intake__c i : intakeList){
      i.VFH_Controller__c = vfh.Id;
    }
  }
}