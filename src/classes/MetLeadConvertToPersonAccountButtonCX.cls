/**
 * @description Custom controller extension for MET Lead Convert To Persion Account Button
 * @author Biao Zhang
 * @date 07.MAR.2016
 */
public with sharing class MetLeadConvertToPersonAccountButtonCX {
	public Id leadId {get; set;}
	public MetLeadConvertToPersonAccountButtonCX(ApexPages.StandardController controller) {
		leadId = controller.getId();
	}

	public PageReference convertToPersonal() {
		Lead l = [Select Company, Employer__c from Lead where id =: leadId limit 1];
		if(!String.isBlank(l.Company)) {
			l.Employer__c = l.Company;
			l.Company = '';			
		}
		update l;

		PageReference page = new PageReference('/lead/leadconvert.jsp?retURL=%2F' + leadId + '&id=' + leadId);
		return page;
	}
}