/**
 * @description Test class for `LeadRestAPI` class
 * @author Ranyel Maliwanag
 * @date 20.JUL.2015
 * @history        
 *    27.OCT.2015   Jerome Almanza  -  added string id in the parameter of method postRequestHandler
 *    20.JAN.2016   Biao Zhang      -  add affiliate Id in test
 */
@isTest
public with sharing class LeadRestAPI_Test {
    static testMethod void getRequestHandler() {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/Lead?firstName=Severus&lastName=Snape';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;

        LeadRestAPI.doGet();
    }

    static testMethod void postRequestHandler() {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/Lead';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;

        LeadRestAPI.doPost('Severus', 'Snape', '09000', 'ssnape@hogwarts.edu', '4000', 'QLD', 'Floo Network', 'FN0001', '00QN00000032GYwMAM', '1234','yes','no','yes','Business', 'Employed', 'some description'); // JEA  27.OCT.2015  Added string id for leadId
    }
}