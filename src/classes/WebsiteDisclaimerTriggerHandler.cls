public class WebsiteDisclaimerTriggerHandler { 
    

    public static void onAfterUpdate(List<Website_Disclaimer__c> records, Map<Id, Website_Disclaimer__c> recordsMap) {
        
        CMSIntegrationController.handleDisclaimers(records,recordsMap);
        
    }

   

}