global class SamlJitUserHandler implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
    private void handleUser(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard) {
        Account pa = [select Id, FirstName, LastName, PersonEmail from Account where PersonEmail = :federationIdentifier limit 1];
        if(create) {
            u.FirstName = pa.FirstName;
            u.LastName = pa.LastName;
            u.Email = pa.PersonEmail;
            u.Username = pa.PersonEmail;
            u.Alias = pa.FirstName.substring(0, 3).toLowerCase() + pa.LastName.substring(0, 2).toLowerCase();
            u.CommunityNickname = pa.FirstName + '.' + pa.LastName;
            u.TimeZoneSidKey = 'Australian Eastern StandardTime (New South Wales) (Australia/Sydney)';
            
        }

        /*   
        if(!create) {
            update(u);
        }
        */
    }

    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        if(communityId != null || portalId != null) {
            // String account = handleAccount(create, u, attributes);
            // handleContact(create, account, u, attributes);
            handleUser(create, u, attributes, federationIdentifier, false);
        } else {
            handleUser(create, u, attributes, federationIdentifier, true);
        }
    }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
            System.debug(String.format('Saml-CreateU-Jit: [ProviderId={0},CommunityId={1},PortalId={2},FederationId={3},Attributes={4},Assertion={5}]',
                                   new String[] {
                                       String.valueOf(samlSsoProviderId),
                                       String.valueOf(communityId),
                                       String.valueOf(portalId),
                                       String.valueOf(federationIdentifier), 
                                       String.valueOf(attributes),
                                       String.valueOf(assertion)}));
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
        return u;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug(String.format('Saml-UpdateU-Jit: [[ProviderId={0},CommunityId={1},PortalId={2},FederationId={3},Attributes={4},Assertion={5}]',
                                   new String[] {
                                       String.valueOf(samlSsoProviderId),
                                       String.valueOf(communityId),
                                       String.valueOf(portalId),
                                       String.valueOf(federationIdentifier), 
                                       String.valueOf(attributes),
                                       String.valueOf(assertion)}));

        User u = [SELECT Id FROM User WHERE Id=:userId];
        handleJit(false, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
    }
}