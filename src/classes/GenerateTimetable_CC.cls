/**
 * @description Controller for GenerateTimetable page
 * @author Michelle Magsarili
 * @date 12.AUG.2013
 *
 * HISTORY
 * - 12.AUG.2013    Michelle Magsarili - Created.
 */
public with sharing class GenerateTimetable_CC {

    public Id enrolmentId {get; set;} //holds the Id of the Enrolment record
    public Enrolment__c Enrolment {get;set;}
    public Enrolment__c EnrolmentD {get;set;}
   
    public GenerateTimetable_CC(){
        //enrolment id passed by the button
        enrolmentId = ApexPages.currentPage().getParameters().get('eId');
        
        Enrolment = new Enrolment__c ();
        EnrolmentD = new Enrolment__c ();
        if(String.isNotEmpty(enrolmentId)){
            Enrolment = [Select id, Delivery_Location__c, Student__c, Student_Identifier__c, Student__r.Name,
                                Start_Date_Conga__c,End_Date_Conga__c 
                                          From Enrolment__c 
                                          Where id =: enrolmentId];
                                          
        }
    }
  
    public void GenerateTimeTable(){
        //Save Conga Start and End dates
        Enrolment.Start_Date_Conga__c = EnrolmentD.Start_Date_Conga__c;
        Enrolment.End_Date_Conga__c  = EnrolmentD.End_Date_Conga__c;
        update Enrolment;
    }
    
    public pageReference Cancel(){
        PageReference pageRef;
        if(String.IsEmpty(enrolmentId)){
            pageRef = new PageReference('/home/home.jsp');
        }
        else{
            pageRef = new PageReference('/'+ String.valueOf(enrolmentId).mid(0,15));
        }
        
        pageRef.setRedirect(true);
        return pageRef;
    }
}