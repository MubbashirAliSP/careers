/**
 * @description Trigger handler for Account object
 * @author Ranyel Maliwanag
 * @date 01.JUL.2015
 * @history 
 *       02.DEC.2015    Biao Zhang          Changed Highest School Completed field value from Application Form style to Account style
 *       04.JAN.2015    Biao Zhang          Changed Disability Type field value from Application Form style to Account style
 */
public without sharing class AccountTriggerHandler {
    /**
     * @description Handles all before insert operations for the Account object
     * @author Ranyel Maliwanag
     * @date 04.JUL.2015
     */
    public static void onBeforeInsert(List<Account> records) {
        CentralTriggerSettings__c triggerSettings = CentralTriggerSettings__c.getInstance();
        List<Account> personAccounts = new List<Account>();

        for (Account record : records) {
            if (record.IsPersonAccount) {
                personAccounts.add(record);
            }
        }

        if (triggerSettings.Account__c) {
            if (triggerSettings.Account_DetermineEnglishCode__c) {
                determineEnglishCode(personAccounts);
            }
        }

        mapHighestSchoolCompleted(records);
        mapDisabilityType(records);
    }


    /**
     * @description Handles all before update operations for the Account object
     * @author Ranyel Maliwanag
     * @date 01.JUL.2015
     */
    public static void onBeforeUpdate(List<Account> records, Map<Id, Account> oldRecordsMap) {
        CentralTriggerSettings__c triggerSettings = CentralTriggerSettings__c.getInstance();
        List<Account> personAccounts = new List<Account>();

        for (Account record : records) {
            if (record.IsPersonAccount) {
                personAccounts.add(record);
            }
        }

        if (triggerSettings.Account__c) {
            if (triggerSettings.Account_DetermineEnglishCode__c) {
                determineEnglishCode(personAccounts);
            }
        }

        mapHighestSchoolCompleted(records);
        mapDisabilityType(records);
    }


    /**
     * @descriptoin Handles all after update operations for the Account objecct
     * @author Ranyel Maliwanag
     * @date 23.FEB.2016
     */
    public static void onAfterUpdate(List<Account> records, Map<Id, Account> oldRecordsMap) {
        // Determine which accounts had their email fields updated
        List<Account> updatedEmails = new List<Account>();

        for (Account record : records) {
            if (record.IsPersonAccount && record.PersonEmail != oldRecordsMap.get(record.Id).PersonEmail) {
                updatedEmails.add(record);
            }
        }

        if (!updatedEmails.isEmpty()) {
            updateRelatedEmails(updatedEmails);
        }
    }


    /**
     * @description Determines the correct English language record for the Student account based on ATSI status
     * @author Ranyel Maliwanag
     * @date 11.SEP.2015
     */
    private static void determineEnglishCode(List<Account> records) {
        Id atsiEnglish;
        Id nonAtsiEnglish;

        for (EnglishLanguageMapping__mdt language : [SELECT RecordId__c, Code__c FROM EnglishLanguageMapping__mdt]) {
            if (language.Code__c == '0001') {
                atsiEnglish = language.RecordId__c;
            }
            if (language.Code__c == '1201') {
                nonAtsiEnglish = language.RecordId__c;
            }
        }

        if (atsiEnglish != null && nonAtsiEnglish != null) {
            for (Account record : records) {
                if (String.isNotBlank(record.Indigenous_Status__c) && record.Language_Spoken_at_Home_Formula__c == 'English') {
                    if (record.Indigenous_Status__c.contains('Yes')) {
                        // Condition: Account record is ATSI. Use the 0001 English Code
                        record.Main_Language_Spoken_at_Home__c = atsiEnglish;
                    } else {
                        // Condition: Account record is not ATSI. Use the 1201 English Code
                        record.Main_Language_Spoken_at_Home__c = nonAtsiEnglish;
                        record.Proficiency_in_Spoken_English__c = null;
                    }
                }
            }
        }
    }

    /**
     * @description Mapping of Highest School Completed
     * @author Biao Zhang
     * @date 2.DEC.2015
     */
    private static void mapHighestSchoolCompleted(List<Account> records) {
        //a mapping for transfering the dropdown value in Application Form Item to Account custom field 
        //Map<ApplicationFormItemValue, AccountFieldValue>
        Map<String, String> mapping = new Map<String, String>();
        mapping.put('Did not go to school', '02 - Did not go to school');
        mapping.put('Completed Year 8 or Lower', '08 - Year 8 or below');
        mapping.put('Completed a Year 9 qualification or equivalent', '09 - Year 9 or equivalent');
        mapping.put('Completed a Year 10 qualification or equivalent', '10 - Completed Year 10');
        mapping.put('Completed a Year 11 qualification or equivalent', '11 - Completed Year 11');
        mapping.put('Completed a Year 12 qualification or equivalent', '12 - Completed Year 12');


        for(Account acc : records) {
            for(String key : mapping.keySet()) {
                if(acc.Highest_School_Level_Completed__c == key) {
                    acc.Highest_School_Level_Completed__c = mapping.get(key);
                }
            }
        }

    }

    /**
     * @description change the dropdown value from Application Form Item to Account diability_type__c value 
     * @author Biao Zhang
     * @date 4.JAN.2015
     */
    private static void mapDisabilityType(List<Account> records) {
        //Map<ApplicationFormItemValue, AccountFieldValue>
        Map<String, String> mapping = new Map<String, String>();
        mapping.put('Hearing / Deaf', '11 - Hearing / Deaf');
        mapping.put('Physical', '12 - Physical');
        mapping.put('Intellectual', '13 - Intellectual');
        mapping.put('Learning', '14 - Learning');
        mapping.put('Mental Illness', '15 - Mental Illness');
        mapping.put('Acquired Brain Impairment', '16 - Acquired brain impairment');
        mapping.put('Vision', '17 - Vision');
        mapping.put('Medical Condition', '18 - Medical Condition');
        mapping.put('Other', '19 - Other');
        mapping.put('Multiple Disabilities', '98 - Multiple Disabilities');
        mapping.put('Unspecified', '99 - Unspecified');

        for(Account acc : records) {
            if(acc.Disability_Type__c != null) {
                List<String> disabilities = acc.Disability_Type__c.split(';');
                
                for(Integer i = 0; i<disabilities.size(); i++) {
                    if(mapping.containsKey(disabilities[i])) {
                        disabilities[i] = mapping.get(disabilities[i]);
                    }
                }
                acc.Disability_Type__c = String.join(disabilities, ';');
            }
        }

    }


    /**
     * @description Copies across the new email value to the related records
     * @author Ranyel Maliwanag
     * @date 23.FEB.2016
     * @history
     *       25.FEB.2016    Biao Zhang              - Update only the non-closed service cases
     *       02.MAR.2016    Ranyel Maliwanag        - Update open Entry Requirements
     */
    private static void updateRelatedEmails(List<Account> records) {
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Account> accountMap = new Map<Id, Account>();

        List<Service_Cases__c> serviceCasesUpdate = new List<Service_Cases__c>();
        List<ApplicationForm__c> formsUpdate = new List<ApplicationForm__c>();
        List<GuestLogin__c> loginsUpdate = new List<GuestLogin__c>();
        List<EntryRequirement__c> erUpdate = new List<EntryRequirement__c>();

        for (Account record : records) {
            accountMap.put(record.Id, record);
        }
        accountIds = accountMap.keySet();

        // Update service case and guest login records based off the account
        for (Account record : [SELECT Id, PersonEmail, (SELECT Id FROM Service_Cases__r WHERE RecordType.Name = 'Form' AND Status__c != 'Closed'), (SELECT Id FROM GuestLogins__r) FROM Account WHERE Id IN :accountIds]) {
            if (!record.Service_Cases__r.isEmpty()) {
                for (Service_Cases__c serviceCase : record.Service_Cases__r) {
                    serviceCase.Student_Email__c = record.PersonEmail;
                    serviceCasesUpdate.add(serviceCase);
                }
            }

            if (!record.GuestLogins__r.isEmpty()) {
                for (GuestLogin__c login : record.GuestLogins__r) {
                    login.Email__c = record.PersonEmail;
                    loginsUpdate.add(login);
                }
            }
        }

        // Get Guest Logins with Application Forms
        Set<String> closedERStatuses = new Set<String>{
            'Closed',
            'Confirmed',
            'LLN - Closed',
            'LLN - Confirmed'
        };
        for (ApplicationForm__c record : [SELECT Id, StudentCredentialsId__r.AccountId__c, (SELECT Id FROM EntryRequirements__r WHERE Status__c NOT IN :closedERStatuses) FROM ApplicationForm__c WHERE StudentCredentialsId__r.AccountId__c IN :accountIds]) {
            record.StudentEmail__c = accountMap.get(record.StudentCredentialsId__r.AccountId__c).PersonEmail;
            if (!record.EntryRequirements__r.isEmpty()) {
                for (EntryRequirement__c requirement : record.EntryRequirements__r) {
                    requirement.Email__c = record.StudentEmail__c;
                    erUpdate.add(requirement);
                }
            }
            formsUpdate.add(record);
        }

        //for (GuestLogin__c record : [SELECT Id, AccountId__c, (SELECT Id FROM StudentApplicationForms__r) FROM GuestLogin__c WHERE AccountId__c IN :accountIds]) {
        //    record.Email__c = accountMap.get(record.AccountId__c).PersonEmail;
        //    if (!record.StudentApplicationForms__r.isEmpty()) {
        //        for (ApplicationForm__c form : record.StudentApplicationForms__r) {
        //            form.StudentEmail__c = record.Email__c;
        //            formsUpdate.add(form);
        //        }
        //    }
        //    loginsUpdate.add(record);
        //}

        List<SObject> objectsForUpdate = new List<SObject>();
        objectsForUpdate.addAll((List<SObject>) serviceCasesUpdate);
        objectsForUpdate.addAll((List<SObject>) formsUpdate);
        objectsForUpdate.addAll((List<SObject>) loginsUpdate);
        objectsForUpdate.addAll((List<SObject>) erUpdate);

        update objectsForUpdate;
    }
}