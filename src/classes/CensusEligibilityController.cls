/**
 * @description This class controls Census Eligibility functionality. All work gets routed to this class, from TaskTriggerHandler
 * @author Yuri Gribanov CloudSherpas
 * @date 02.DEC.2015
 * @history
 *       13.JAN.2016    Biao Zhang			- Added Due date(ActivityDate) for the tasks and change task subject from 'Call' to 'Assessment Attempt'
 *		 01.MAR.2016	Ranyel Maliwanag	- Defaulted to current running user if the assigned trainer is blank for the owner of the created tasks
 *		 20.APR.2016	Ranyel Maliwanag	- Code cleanup
 *											- Fixed handling of Task and Case record creation for inactive assigned trainers
 *											- Streamlined Case record creation to avoid creating multiple records for a single student
 *		 21.APR.2016	Ranyel Maliwanag	- Added logic to restrict creation of Stage Eligibility for Active Enrolment records only
 */
public class CensusEligibilityController {
	// Constant Record Type Ids
	private final static Id c1RecordTypeId = Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C1 (incur debt)').getRecordTypeId();
	private final static Id s2RecordTypeId = Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for Stage 2 (unit release)').getRecordTypeId();
	private final static Id c2RecordTypeId = Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C2 (incur debt)').getRecordTypeId();
	private final static Id c3RecordTypeId = Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C3 (incur debt)').getRecordTypeId();
	private final static Id s3RecordTypeId = Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for Stage 3 (unit release)').getRecordTypeId();
	private final static Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Unit For Release').getRecordTypeId();
	private final static Id onlineTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId();
	private final static Id queueId = [SELECT Id FROM Group WHERE Name = 'Unit For Release' AND Type = 'Queue'].Id;
		
	
	/**
	 * @description Acts as a router when called from `TaskTrigggerHandler`. Will process and split depending on stage. Workfs for Census Eligibility
	 */
	public static void EligibilityForCensusMapper(List<Task> tasks) {
		List<Task> census1Tasks = new List<Task>();
		List<Task> census2Tasks = new List<Task>();
		List<Task> census3Tasks = new List<Task>();

		for (Task t : tasks) {
			if (t.Eligibility_Items__c == 'Stage 1 Introduction' || t.Eligibility_Items__c == 'Stage 1 Lesson') {
				census1Tasks.add(t);
			}
			if (t.Eligibility_Items__c == 'Stage 2 Introduction' || t.Eligibility_Items__c == 'Stage 2 Lesson') {
				census2Tasks.add(t);
			}
			if (t.Eligibility_Items__c == 'Stage 3 Introduction' || t.Eligibility_Items__c == 'Stage 3 Lesson') {
				census3Tasks.add(t);
			}
		}

		if (census1Tasks.size() > 0) {
			EligibleForCensus(census1Tasks,c1RecordTypeId,'Stage 1');
		}
		if (census2Tasks.size() > 0) {
			EligibleForCensus(census2Tasks,c2RecordTypeId,'Stage 2');
		}
		if (census3Tasks.size() > 0) {
			EligibleForCensus(census3Tasks,c3RecordTypeId,'Stage 3');
		}
	}
	
	
	/**
	 * @description Acts as a router when called from `AssessmentAttemptTriggerHandler`. Will process and split depending on stage. Works for Stage Eligibility
	 */
	public static void EligibilityForStageMapper(List<AssessmentAttempt__c> assessmentAttempts) {
		List<AssessmentAttempt__c> stage1Attempts = new List<AssessmentAttempt__c>();
		List<AssessmentAttempt__c> stage2Attempts = new List<AssessmentAttempt__c>();
		List<AssessmentAttempt__c> attempts = new List<AssessmentAttempt__c>();
		
		attempts = [SELECT Id, Census__c, Census__r.Stage__c, Census__r.EnrolmentId__c, Census__r.EnrolmentId__r.Assigned_Trainer__c, Census__r.EnrolmentId__r.Enrolment_Status__c, Census__r.EnrolmentId__r.Assigned_Trainer__r.IsActive, Census__r.EnrolmentId__r.Student__c, Census__r.EnrolmentId__r.Student__r.PersonContactId, Census__r.EnrolmentId__r.Eligible_for_C1__c, Census__r.EnrolmentId__r.Eligible_for_C2__c, Census__r.EnrolmentId__r.Eligible_for_C3__c, Census__r.EndDate__c FROM AssessmentAttempt__c WHERE Id IN :assessmentAttempts];
		
		for (AssessmentAttempt__c att : attempts) {
			
			/*Comment out by Biao on 23 Dec 2015. Discussed with Pierce. */
			//if(att.Census__r.EnrolmentId__r.Assigned_Trainer__c == null)
				//throw new internalServerException('Assigned Trainer On Enrolment Must Be Populated' +  att.Census__r.EnrolmentId__c);
		
			if (att.Census__r.Stage__c == 'Stage 1') {
				stage1Attempts.add(att);
			}
			if (att.Census__r.Stage__c == 'Stage 2') {
				stage2Attempts.add(att);
			}
		}

		if (stage1Attempts.size() > 0) {
			EligibleForStage(stage1Attempts,s2RecordTypeId, 'Stage 1 Assessment', 'Please Release the Stage 2 Units');
		}
		
		if (stage2Attempts.size() > 0) {
			EligibleForStage(stage2Attempts,s3RecordTypeId, 'Stage 2 Assessment','Please Release the Stage 3 Units');
		}
	}


	/**
	 * @description Handles Eligible for Census 1, 2, 3 work here. Applies to the following Statuses: "1 Introduction", "1 Lesson", "2 Introduction", "2 Lesson", "3 Introduction", "3 Lesson"
	 */
	private static void EligibleForCensus(List<Task> tasks, Id EligibilityRecordTypeId, String stage) {        
		Set<Id> enrlIds = new Set<Id>();
		List<Census_Eligibility_Items__c> eligItem = new List<Census_Eligibility_Items__c>();
		List<Census__c> censusList = new List<Census__c>();
		Map<Id,Id> censusMap = new Map<Id,Id>();
		
		for (Task t : tasks) {
			enrlIds.add(t.whatId);
		}
		
		censusList = [SELECT id,EnrolmentId__c FROM Census__c WHERE EnrolmentId__c IN :enrlIds and Stage__c = :stage];
		
		for (Census__c c : censusList) {
			censusMap.put(c.EnrolmentId__c, c.Id);
			c.EligibleforCensus__c = true;
		}

		String enrolmentIdPrefix = Schema.SObjectType.Enrolment__c.getKeyPrefix();
		for (Task t : tasks) {
			if (t.whatId != null && ((String) t.whatId).startsWith(enrolmentIdPrefix)) {
				// Create census record
				Census_Eligibility_Items__c item = new Census_Eligibility_Items__c(
					Enrolment__c = t.whatId,
					Eligibility_Item__c = t.Eligibility_Items__c,
					RecordTypeId = EligibilityRecordTypeId,
					Census__c = censusMap.get(t.whatId)
				);
				eligItem.add(item);
			}
		}

		insert eligItem;
		update censusList;
	}
	
	
	private static void EligibleForStage(List<AssessmentAttempt__c> attempts, Id EligibilityRecordTypeId, String stage, String caseDescription ) {

		List<Census_Eligibility_Items__c> eligItem = new List<Census_Eligibility_Items__c>();
		List<Task> tasks = new List<Task>();
		List<Case>cases = new List<Case>();
		Map<Id, String> enrlStageMap = new Map<Id, String>();
		
		// Mapping of <Contact Id> : <Contact Record>
		Map<Id, Census__c> censusMap = new Map<Id, Census__c>();

		Set<Id> contactIds = new Set<Id>();

		for (AssessmentAttempt__c att :  attempts ) {
			if (att.Census__r.EnrolmentId__r.Enrolment_Status__c.startsWithIgnoreCase('Active')) {
				if ((stage.contains('1') && att.Census__r.EnrolmentId__r.Eligible_for_C1__c == false) || 
					(stage.contains('2') && att.Census__r.EnrolmentId__r.Eligible_for_C2__c == false)) {
					enrlStageMap.put(att.Census__r.EnrolmentId__c, stage);
				}
			
				Census_Eligibility_Items__c item = new Census_Eligibility_Items__c(
					Enrolment__c = att.Census__r.EnrolmentId__c,
					Eligibility_Item__c = stage,
					RecordTypeId = EligibilityRecordTypeId,
					Census__c = att.Census__c
				);
				eligItem.add(item);
				
				Task t = new Task(
					Eligibility_Items__c = stage,
					RecordTypeId = onlineTaskRecordTypeId,
					Subject = 'Assessment Attempt',
					ownerId = (String.isNotBlank(att.Census__r.EnrolmentId__r.Assigned_Trainer__c) && att.Census__r.EnrolmentId__r.Assigned_Trainer__r.IsActive) ? att.Census__r.EnrolmentId__r.Assigned_Trainer__c : UserInfo.getUserId(),
					whoId   = att.Census__r.EnrolmentId__r.Student__r.PersonContactId,
					whatId  = att.Census__r.EnrolmentId__c,
					Status = 'Completed',
					ActivityDate = Date.today()
				);
				tasks.add(t);

				censusMap.put(att.Census__c, att.Census__r);
				contactIds.add(att.Census__r.EnrolmentId__r.Student__r.PersonContactId);
			}
		}

		// Build a contact map as reference for the cases assigned to a specific student contact
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		for (Contact studentContact : [SELECT Id, (SELECT Id, Unit_Release_For_Census_Date__c FROM Cases WHERE RecordType.Name = 'Unit For Release' AND Subject = 'Unit for Release') FROM Contact WHERE Id IN :contactIds]) {
			contactMap.put(studentContact.Id, studentContact);
		}

		for (Census__c census : censusMap.values()) {
			Id contactId = census.EnrolmentId__r.Student__r.PersonContactId;
			if (String.isNotBlank(contactId) && contactMap.containsKey(contactId)) {
				Contact studentContact = contactMap.get(contactId);
				Boolean isCreated = false;
				for (Case studentCase : studentContact.Cases) {
					if (studentCase.Unit_Release_For_Census_Date__c == census.EndDate__c) {
						isCreated = true;
						break;
					}
				}

				if (!isCreated) {
					Case attemptCase = new Case (
						RecordTypeId = caseRecordTypeId,
						Type = 'New',
						Status = 'New',
						Origin = 'Web',
						Subject = 'Unit for Release',
						Description = caseDescription,
						Priority = 'Medium',
						Submitted_By_User__c = (String.isNotBlank(census.EnrolmentId__r.Assigned_Trainer__c) && census.EnrolmentId__r.Assigned_Trainer__r.IsActive) ? census.EnrolmentId__r.Assigned_Trainer__c : UserInfo.getUserId(),
						OwnerId = queueId,
						ContactId = census.EnrolmentId__r.Student__r.PersonContactId,
						Unit_Release_For_Census_Date__c = census.EndDate__c
					);
					cases.add(attemptCase);
				}
			}
		}
		
		if (!eligItem.isEmpty()) {
			insert eligItem;
		}
		if (!tasks.isEmpty()) {
			insert tasks;
		}
		if (!cases.isEmpty()) {
			insert cases;
		}
		
		// If there are any assessment created without introduction first, we need to create introduction eligibility item at the same time
		if (enrlStageMap.size() > 0) {
		   createNonAssessmentTasks(enrlStageMap);
		}
	}
	
	
	@future private static void createNonAssessmentTasks(Map<Id,String>enrlStageMap) {
		List<Task> tasks = new List<Task>();

		for (Enrolment__c e : [Select Id, Assigned_Trainer__c, Assigned_Trainer__r.IsActive, Student__r.PersonContactId from Enrolment__c where id in : enrlStageMap.keySet()]) {

			Task t = new Task(
				Eligibility_Items__c = enrlStageMap.get(e.Id).replace('Assessment', 'Introduction'),
				RecordTypeId = onlineTaskRecordTypeId,
				Subject = 'Assessment Attempt',
				ownerId = (String.isNotBlank(e.Assigned_Trainer__c) && e.Assigned_Trainer__r.IsActive) ? e.Assigned_Trainer__c : UserInfo.getUserId(),
				whoId   = e.Student__r.PersonContactId,
				whatId  = e.Id,
				Status = 'Completed',
				ActivityDate = Date.today()
			);
			tasks.add(t); 
		}
		insert tasks;
	}
}