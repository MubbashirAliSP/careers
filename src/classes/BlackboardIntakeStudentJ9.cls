/*JOB9 Staging table loader job
 *This class is resposible for creating and updating
 *Blackboard User Roles
 *BB users roles are created based on Intake Student and only when Institutional Role is Assigned
 *to related qualification
 *Please Refer to ERD for further details about relationships
 *Created By Yuri Gribanov - CloudSherpas 15th July 2014
 *BATCH SIZE LIMIT 1 ONLY
 */


global with sharing class BlackboardIntakeStudentJ9 implements Database.Batchable<sObject> {

    global String query;

    global BlackboardIntakeStudentJ9() {
    /*Initial query to be passed into query locator performed against Enrolment Intake Units*/
        query = 'Select Id, Intake__r.Qualification__r.Blackboard_Institutional_Role__c, Enrolment__r.Student__c From Intake_Students__c WHERE Intake__r.Qualification__r.Blackboard_Institutional_Role__c != null AND Blackboard_Institutional_Role_Assigned__c = False AND Enrolment__r.Blackboard_Required__c = true';
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        Set<Id>accId = new Set<Id>();
        String BBIRId = '';
        String bbuserId = '';
        List<Blackboard_User_Role__c> bbuserRoleToUpdate = new List<Blackboard_User_Role__c>();
        List<Intake_Students__c>isToUpdate = new List<Intake_Students__c>();
        
        /*Loop though intake students and collect related StudentId and Insitution Role Id */
        for(SObject s :scope) {

             Intake_Students__c is = (Intake_Students__c) s;

             accId.add(is.Enrolment__r.Student__c);
             BBIRId = is.Intake__r.Qualification__r.Blackboard_Institutional_Role__c;
             is.Blackboard_Institutional_Role_Assigned__c = True;

             isToUpdate.add(is);
             
        }

        for(Blackboard_User__c bbuser : [Select Id From Blackboard_User__c Where Account__c in : accId LIMIT 1])
            bbuserId = bbuser.Id;
        


        bbuserRoleToUpdate = [Select Id, name, Blackboard_Role__c, Blackboard_User__c, Primary_Institutional_Role__c From Blackboard_User_Role__c WHERE Primary_Institutional_Role__c = True and Blackboard_User__c = : bbuserId And RecordType.DeveloperName = 'Faculty_Institutional'];

        for(Blackboard_User_Role__c bbuserrole : bbuserRoleToUpdate)
            bbuserrole.Primary_Institutional_Role__c = false;

        update bbuserRoleToUpdate;


        Blackboard_User_Role__c newbbuserRole = new Blackboard_User_Role__c(

            Blackboard_User__c = bbuserId,
            Blackboard_Role__c = BBIRId,
            Primary_Institutional_Role__c = true,
            RecordTypeId = [Select Id From RecordType Where DeveloperName = 'Faculty_Institutional'].Id
                


        );


       insert newbbuserRole;

       update isToUpdate;


    }

    global void finish(Database.BatchableContext BC) {
        
      BlackboardUserSyncBatch b1 = new BlackboardUserSyncBatch();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }

    
}