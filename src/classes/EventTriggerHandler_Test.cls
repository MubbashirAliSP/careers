/**
 * @description Test class for `EventTriggerHandler`
 * @author Ranyel Maliwanag
 * @date 14.JUN.2016
 */
@isTest
public with sharing class EventTriggerHandler_Test {
	public static Id cdpRecordType =  Schema.SObjectType.Event.getRecordTypeInfosByName().get('CDP Event').getRecordTypeId();

	@testSetup
	static void setup() {
		Account student = new Account(
                FirstName = 'Godric',
                LastName = 'Gryffindor',
                Student_Identifer__c = 'CX1230',
                PersonBirthdate = Date.newInstance(2015, 5, 19),
                PersonEmail = 'person@careersaustraliatest.com',
                Middle_Name__c = 'Taylor',
                Phone = '548242',
                PersonMobilePhone = '745635',
                Salutation = 'Mr'
            );
        insert student;

        Enrolment__c enrolment = new Enrolment__c(
                IPS_Sync_on_next_update__c = true,
                IPS_InPlace_Student__c = true,
                Student__c = student.Id,
                Enrolment_Status__c = 'Active (Commencement)',
                Mailing_Country__c = 'Australia',
                Mailing_Zip_Postal_Code__c = '4006',
                Name_of_suburb_town_locality__c = 'Fortitude Valley',
                Mailing_State_Provience__c = 'Queensland',
                Address_street_number__c = '100',
                Address_street_name__c = 'Brookes Street',
                Address_flat_unit_details__c = 'Ground Floor',
                Managed_by_GLS__c = true,
                GLS_Enrolment_ID__c = 'EX1230'
            );
        insert enrolment;

        Service_Cases__c serviceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application'
            );
        insert serviceCase;
	}


	/**
	 * @method updateCDPEventsCount
	 * @scenario An Event with Record Type of `CDP Event` and related to a Service Case gets created
	 * @acceptanceCriteria `CDPEventsCount__c` field on the Service Case gets incremeted
	 */
	static testMethod void updateCDPEventsCountTest1() {
		Service_Cases__c serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
		Test.startTest();
			System.assert(serviceCase.CDPEventsCount__c == null || serviceCase.CDPEventsCount__c == 0);

			Event record = new Event(
					RecordTypeId = cdpRecordType,
					WhatId = serviceCase.Id,
					IsAllDayEvent = true,
					ActivityDate = Date.today()
				);
			insert record;

			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
			System.assertEquals(1, serviceCase.CDPEventsCount__c);
		Test.stopTest();
	}


	/**
	 * @method updateCDPEventsCount
	 * @scenario An Event with Record Type of `Standard Event` and related to a Service Case gets created
	 * @acceptanceCriteria `CDPEventsCount__c` field on the Service Case remains the same
	 */
	static testMethod void updateCDPEventsCountTest2() {
		Service_Cases__c serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
		Test.startTest();
			System.assert(serviceCase.CDPEventsCount__c == null || serviceCase.CDPEventsCount__c == 0);

			Event record = new Event(
					RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Standard Event').getRecordTypeId(),
					WhatId = serviceCase.Id,
					IsAllDayEvent = true,
					ActivityDate = Date.today()
				);
			insert record;

			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
			System.assert(serviceCase.CDPEventsCount__c == null || serviceCase.CDPEventsCount__c == 0);
		Test.stopTest();
	}



	/**
	 * @method updateCDPEventsCount
	 * @scenario An Event with Record Type of `CDP Event` and related to a Service Case gets updated to relate to a different Service Case
	 * @acceptanceCriteria `CDPEventsCount__c` field on the old Service Case gets decremented and `CDPEventsCount__c` field on the new Service Case gets incremented
	 */
	static testMethod void updateCDPEventsCountTest3() {
		Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
		Service_Cases__c serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
		Event record = new Event(
				RecordTypeId = cdpRecordType,
				WhatId = serviceCase.Id,
				IsAllDayEvent = true,
				ActivityDate = Date.today()
			);
		insert record;

		Service_Cases__c newServiceCase = new Service_Cases__c(
                Enrolment__c = enrolment.Id,
                Account_Student_Name__c = student.Id,
                RecordTypeId = ServiceCaseTriggerHandler.formRecordType,
                Type__c = 'VFH Online Enrolment Application'
            );
        insert newServiceCase;

		Test.startTest();
			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			newServiceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :newServiceCase.Id];
			System.assert(newServiceCase.CDPEventsCount__c == null || newServiceCase.CDPEventsCount__c == 0);
			System.assertEquals(1, serviceCase.CDPEventsCount__c);

			record.WhatId = newServiceCase.Id;
			update record;

			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			newServiceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :newServiceCase.Id];
			System.assertEquals(0, serviceCase.CDPEventsCount__c, 'Running count ::: ' + serviceCase.CDPEventsCount__c);
			System.assertEquals(1, newServiceCase.CDPEventsCount__c);
		Test.stopTest();
	}


	/**
	 * @method updateCDPEventsCount
	 * @scenario An Event with Record Type of `CDP Event` and related to a Service Case gets deleted
	 * @acceptanceCriteria `CDPEventsCount__c` field on the Service Case gets decremented
	 */
	static testMethod void updateCDPEventsCountTest4() {
		Enrolment__c enrolment = [SELECT Id FROM Enrolment__c];
        Account student = [SELECT Id FROM Account WHERE Student_Identifer__c = 'CX1230'];
		Service_Cases__c serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c];
		Event record = new Event(
				RecordTypeId = cdpRecordType,
				WhatId = serviceCase.Id,
				IsAllDayEvent = true,
				ActivityDate = Date.today()
			);
		insert record;

		Test.startTest();
			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals(1, serviceCase.CDPEventsCount__c);

			delete record;

			serviceCase = [SELECT Id, CDPEventsCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals(0, serviceCase.CDPEventsCount__c);
		Test.stopTest();
	}
}