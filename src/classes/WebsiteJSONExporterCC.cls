/**
 * @description Custom controller for `WebsiteJSONExporter`
 * @author Ranyel Maliwanag
 * @date 28.JUL.2015
 * @history
 *       07.Jan.2016 Biao Zhang                    Add VFH Approved field into JSON
 */
public with sharing class WebsiteJSONExporterCC {
    public String jsonString {get; set;}

    public WebsiteJSONExporterCC() {
        Map<String, String> pageParameters = ApexPages.currentPage().getParameters();
        jsonString = '';

        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartArray();

        if (pageParameters.containsKey('type')) {
            Map<String, String> fields = new Map<String, String>();
            if (pageParameters.get('type') == 'CourseDisclaimer') {
                fields.put('WD_Qualification__c', 'Qualification: ID');
                fields.put('Website_Disclaimer_Option__r.Name', 'Website Disclaimer');
                buildGenerator(generator, [SELECT Id, WD_Qualification__c, Website_Disclaimer_Option__r.Name FROM Website_Disclaimer__c WHERE RecordType.Name = 'Website Disclaimer'], fields);
            } else if (pageParameters.get('type') == 'Courses') {
                fields.put('Id', 'Qualification: ID');
                fields.put('Which_Website__c', 'Which Website');
                fields.put('website_code__c', 'Website code');
                fields.put('website_title__c', 'Website title');
                fields.put('website_type__c', 'Website type');
                fields.put('website_sector__c', 'Website sector');
                fields.put('website_description__c', 'Website description');
                fields.put('website_aqf_code__c', 'Website aqf_code');
                fields.put('website_aqf_title__c', 'Website aqf_title');
                fields.put('website_pathway__c', 'Website pathway');
                fields.put('website_materials__c', 'Website materials');
                fields.put('website_requirements__c', 'Website requirements');
                fields.put('website_accreditation__c', 'Website accreditation');
                fields.put('website_apprenticeship__c', 'Website Apprenticeship?');
                fields.put('website_traineeship__c', 'Website Traineeship?');
                fields.put('Hidden_from_website__c', 'Hidden from website');
                fields.put('W_Course_Amount_All_States__c', 'Course Amount - All States');
                fields.put('W_Course_Amount_ACT__c', 'Course Amount - ACT');
                fields.put('W_Course_Amount_NSW__c', 'Course Amount - NSW');
                fields.put('W_Course_Amount_NT__c', 'Course Amount - NT');
                fields.put('W_Course_Amount_QLD__c', 'Course Amount - QLD');
                fields.put('W_Course_Amount_SA__c', 'Course Amount - SA');
                fields.put('W_Course_Amount_TAS__c', 'Course Amount - TAS');
                fields.put('W_Course_Amount_VIC__c', 'Course Amount - VIC');
                fields.put('W_Course_Amount_WA__c', 'Course Amount - WA');
                fields.put('W_Course_Amount_MEA_member__c', 'Course Amount - MEA member');
                fields.put('W_Course_Amount_MEA_non_member__c', 'Course Amount - MEA non-member');
                fields.put('VFH_Approved__c', 'VFH Approved');
                buildGenerator(generator, [SELECT Id, website_code__c, website_title__c, website_type__c, website_sector__c, website_description__c, website_aqf_code__c, website_aqf_title__c, website_materials__c, website_requirements__c, website_accreditation__c, website_apprenticeship__c, website_traineeship__c, website_pathway__c, W_Course_Amount_All_States__c, W_Course_Amount_ACT__c, W_Course_Amount_NSW__c, W_Course_Amount_NT__c, W_Course_Amount_QLD__c, W_Course_Amount_SA__c, W_Course_Amount_TAS__c, W_Course_Amount_VIC__c, W_Course_Amount_WA__c, Hidden_from_website__c, Which_Website__c, W_Course_Amount_MEA_member__c, W_Course_Amount_MEA_non_member__c, VFH_Approved__c FROM Qualification__c WHERE Visible_On_Website__c = true], fields);
            } else if (pageParameters.get('type') == 'DisclaimerOptions') {
                fields.put('Name', 'Website Disclaimer');
                fields.put('WDO_Funding_Type__c', 'Funding Type');
                fields.put('WDO_State__c', 'State');
                fields.put('WDO_Disclaimer__c', 'Disclaimer');
                buildGenerator(generator, [SELECT Id, Name, WDO_Funding_Type__c, WDO_State__c, WDO_Disclaimer__c FROM Website_Disclaimer__c WHERE RecordType.Name = 'Website Disclaimer Option'], fields);
            } else if (pageParameters.get('type') == 'DeliveryModes') {
                fields.put('Qualification__c', 'Qualification: ID');
                fields.put('Website_Delivery_Mode__c', 'Website Delivery Mode');
                fields.put('Website_Delivery_Mode_Notes__c', 'Website Delivery Mode Notes');
                fields.put('Website_Delivery_Mode_Locations__c', 'Website Delivery Mode Locations');
                fields.put('Website_duration_period__c', 'Website duration period');
                fields.put('Website_duration_type__c', 'Website duration type');
                fields.put('Requirements__c', 'Requirements');
                buildGenerator(generator, [SELECT Id, Qualification__c, Website_Delivery_Mode__c, Website_Delivery_Mode_Notes__c, Website_Delivery_Mode_Locations__c, Website_duration_period__c, Website_duration_type__c, Requirements__c FROM Website_Delivery_Mode__c WHERE Qualification__r.Visible_On_Website__c = true], fields);
            } else if (pageParameters.get('type') == 'DualCourses') {
                fields.put('Id', 'Website Dual Qualification: ID');
                fields.put('Which_Website__c', 'Which Website');
                fields.put('WDQ_Website_Title__c', 'Website Title');
                fields.put('WDQ_Description__c', 'Description');
                fields.put('WDQ_Qualification_1__c', 'Qualification 1 ID');
                fields.put('WDQ_Qualification_2__c', 'Qualification 2 ID');
                fields.put('WDQ_Course_Amount_ACT__c', 'Course Amount - ACT');
                fields.put('WDQ_Course_Amount_NSW__c', 'Course Amount - NSW');
                fields.put('WDQ_Course_Amount_NT__c', 'Course Amount - NT');
                fields.put('WDQ_Course_Amount_QLD__c', 'Course Amount - QLD');
                fields.put('WDQ_Course_Amount_SA__c', 'Course Amount - SA');
                fields.put('WDQ_Course_Amount_TAS__c', 'Course Amount - TAS');
                fields.put('WDQ_Course_Amount_VIC__c', 'Course Amount - VIC');
                fields.put('WDQ_Course_Amount_WA__c', 'Course Amount - WA');
                buildGenerator(generator, [SELECT Id, WDQ_Website_Title__c, WDQ_Description__c, WDQ_Qualification_1__c, WDQ_Qualification_2__c, WDQ_Course_Amount_ACT__c, WDQ_Course_Amount_NSW__c, WDQ_Course_Amount_NT__c, WDQ_Course_Amount_QLD__c, WDQ_Course_Amount_SA__c, WDQ_Course_Amount_TAS__c, WDQ_Course_Amount_VIC__c, WDQ_Course_Amount_WA__c, Which_Website__c FROM Website_Dual_Qualification__c WHERE WDQ_Visible_On_Website__c = true], fields);
            } else if (pageParameters.get('type') == 'Provider') {
                fields.put('WP_Qualification__c', 'Qualification: ID');
                fields.put('WP_RTO_Display_Name__c', 'RTO Display Name');
                fields.put('WP_RTO_code__c', 'RTO code');
                fields.put('WP_CRICOS_Number__c', 'CRICOS Number');
                buildGenerator(generator, [SELECT Id, WP_Qualification__c, WP_RTO_Display_Name__c, WP_RTO_code__c, WP_CRICOS_Number__c FROM Website_Provider__c WHERE WP_Qualification__r.Visible_On_Website__c = true], fields);
            } else if (pageParameters.get('type') == 'Units') {
                fields.put('Qualification__c', 'Qualification: ID');
                fields.put('Stage__r.Name', 'Stage');
                fields.put('Unit_Record_Type__c', 'Unit Record Type');
                fields.put('Unit_Code__c', 'Unit Code');
                fields.put('Unit_Name__c', 'Unit Name');
                buildGenerator(generator, [SELECT Id, Qualification__c, Stage__r.Name, Unit_Record_Type__c, Unit_Code__c, Unit_Name__c FROM Qualification_Unit__c WHERE Qualification__r.Visible_On_Website__c = true AND Unit_Record_Type__c = 'Unit of Competency' AND Don_t_auto_add_this_unit_to_enrolments__c = false], fields);
            } else if (pageParameters.get('type') == 'WebsiteStreams') {
                fields.put('Id', 'Website Stream ID');
                fields.put('Which_Website__c', 'Which Website');
                fields.put('Name', 'Website Title');
                fields.put('Qualification_1__c', 'Qualification 1 ID');
                fields.put('Qualification_2__c', 'Qualification 2 ID');
                fields.put('Qualification_3__c', 'Qualification 3 ID');
                fields.put('Description__c', 'Description');
                buildGenerator(generator, [SELECT Id, Name, Qualification_1__c, Qualification_2__c, Qualification_3__c, Description__c, Which_Website__c FROM Website_Stream__c], fields);
            }
        }
        generator.writeEndArray();
        jsonString = generator.getAsString();
        jsonString = jsonString.replaceAll('\"', '\\\"');
    }


    private static JSONGenerator buildGenerator(JSONGenerator generator, List<SObject> records, Map<String, String> objectFieldsMap) {
        List<String> apiFields = new List<String>(objectFieldsMap.keySet());
        for (SObject record : records) {
            generator.writeStartObject();
            for (String apiField : apiFields) {
                String value = '';
                if (getReferenceFieldValue(record, apiField) != null) {
                    value = String.valueOf(getReferenceFieldValue(record, apiField));
                }
                generator.writeStringField(objectFieldsMap.get(apiField), value);
            }
            generator.writeEndObject();
        }
        return generator;
    }

    private static Object getReferenceFieldValue(SObject source, String fieldPattern) {
        Object result = '';
        if (fieldPattern.contains('.')) {
            String reference = fieldPattern.substringBefore('.');
            String referenceField = fieldPattern.substringAfter('.');
            SObject referenceObject = source.getSObject(reference);
            if (referenceObject != null) {
                result = getReferenceFieldValue(referenceObject, referenceField);
            }
        } else {
            result = source.get(fieldPattern);
            if (result instanceof Id) {
                result = String.valueOf(result).substring(0, 15);
            }
        }
        return result;
    }
}