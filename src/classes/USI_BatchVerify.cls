global class USI_BatchVerify implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts {

    global String query;    
    global List<USI_Integration_Event__c> eventList = new List<USI_Integration_Event__c>();
    global USI_Integration_Log__c usiLog;
    global List<Account> accountList = new List<Account>();
    global Map<Id, Enrolment__c> studentEnrolmentMap = new Map<Id, Enrolment__c>();
    global Id studentId; //used for 1-sizestuEnrlMap JMDG 29.SEPT.2015
    global Set<Id> studentIdSet;

    global class usiVerifyResponse {
        String dateofbirth; 
        String firstname;
        String usi;
        String message;
        String lastname;
    }
    
    public class internalServerException extends Exception {
        //just for throwing exception
    }

    global USI_BatchVerify(Map<Id, Enrolment__c> stuEnrlMap ) {
        
        usiLog = USI_LogManager.createUSILog();
        
        if(usiLog == null)
            throw new internalServerException('Operation Terminated, No USI Log found');
        
        studentEnrolmentMap = stuEnrlMap;
        studentIdSet = studentEnrolmentMap.keySet();

        if (studentEnrolmentMap.size() == 1) { //added if statement for 1-size stuEnrlMap JMDG 29.SEPT.2015
            for (Id sId : studentIdSet){
                studentId = sId;
            }
            query = 'SELECT ID, FirstName, LastName, PersonBirthDate, Unique_student_identifier__c, Student_Identifer__c FROM Account WHERE Id = :studentId';
            system.debug('here');
        }
        
        
        
        else {
            query = 'SELECT ID, FirstName, LastName, PersonBirthDate, Unique_student_identifier__c, Student_Identifer__c FROM Account WHERE Id in :studentIdSet';
        }
        
    }

    global database.querylocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(query);
    
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
           
       for (SObject s :scope) {
           Account student = (Account) s;

           String sentBody;
        
           USI_Integration_Event__c event = new USI_Integration_Event__c();
           HTTPresponse verifyUSIres = new HTTPResponse();
           Enrolment__c enrl = studentEnrolmentMap.get(student.Id);
           
           try {                 
               sentBody = JSON.serializePretty(student);
               verifyUSIres =  USI_WebService.verifyUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
               event.JSON__c = sentBody;
               usiVerifyResponse uro = (usiVerifyResponse) JSON.deserialize(verifyUSIres.getBody(), usiVerifyResponse.class); 
                
               if (uro.message == 'Internal USI Error Occured, Please Check Heroku logs for more details' ) {     
                                
                   
                   event = USI_LogManager.errorHandle(uro.message, student.Id, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);
                   if(event.USI_Integration_Log__c == null)                      
                   
                   if(event.USI_Integration_Log__c != null)
                       eventList.add(event);
                    
                   verifyUSIres =  USI_WebService.verifyUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                   uro = (usiVerifyResponse) JSON.deserialize(verifyUSIres.getBody(), usiVerifyResponse.class); 
               }
               
                if(uro.USI.equalsIgnoreCase('valid') && uro.Firstname.equalsIgnoreCase('match') && uro.Lastname.equalsIgnoreCase('match') && uro.DateOfBirth.equalsIgnoreCase('match')) { 
                    try {
                                             
                        accountList.add(USI_UtilityClass.verifyUSIBatch(student.Id));
                        //event = USI_LogManager.createUSIEvent(usiLog.Id, student.Id, uro.message, false);
                        event = USI_LogManager.createUSIEvent(usilog.Id, student.Id, student.FirstName + ' ' + student.LastName, student.Student_Identifer__c, usilog.Id, false);

                        USI_UtilityClass.closeServiceCase(student.Id);
                    }
                    catch (exception e) {
                       
                        event = USI_LogManager.errorHandle(e.getMessage(), student.Id, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);

                    }
                }
               
                 //This is when usi is valid, but personal details are invalid
                if(uro.USI.equalsIgnoreCase('valid') && (uro.Firstname.equalsIgnoreCase('nomatch') || uro.Lastname.equalsIgnoreCase('nomatch') || uro.DateOfBirth.equalsIgnoreCase('nomatch') )) {
                    
                    String noMatchCode = '';
                    
                    if(uro.Firstname.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'Firstname does not match ';
                    if(uro.Lastname.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'Lastname does not match ';
                    if(uro.DateOfBirth.equalsIgnoreCase('nomatch'))
                        noMAtchCode = noMatchCode + 'DOB does not match ';
                    
                    event = USI_LogManager.errorHandle(noMAtchCode + ',First Name: ' + uro.firstname + ', Last Name: ' + uro.lastname + ', DOB: ' + uro.dateofbirth, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);
                    
                }
               
               
               
                if(uro.USI.equalsIgnoreCase('invalid')) {
                    
                    event = USI_LogManager.errorHandle('USI is Invalid, First Name: ' + uro.firstname + ', Last Name: ' + uro.lastname + ', DOB: ' + uro.dateofbirth, studentId, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);
                }
                if(uro.USI == '' || uro.USI == null) {
                     event = USI_LogManager.errorHandle(uro.message, student.Id, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);
                }
            }

            catch (exception e) {
                     event = USI_LogManager.errorHandle(e.getMessage(), student.Id, student.FirstName + ' '+ student.LastName, student.Student_Identifer__c, usilog.Id, false);

            }
            
            event.Received_JSON__c = verifyUSIRes.getBody();
                                   
            if(event.USI_Integration_Log__c != null)
                eventList.add(event);
       }
    
    }
        
    global void finish(Database.BatchableContext BC) {
       
        update accountList;
        insert eventList;
        
    }
}