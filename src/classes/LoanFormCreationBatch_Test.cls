/**
 * @description Test class for `LoanFormCreationBatch` class
 * @author Ranyel Maliwanag
 * @date 23.OCT.2015
 */
@isTest
public with sharing class LoanFormCreationBatch_Test {
    @testSetup
    static void setupBatch() {
        ApplicationsTestUtilities.initialiseCountryAndStates();
        ApplicationsTestUtilities.initialiseFormComponents();
        ApplicationsTestUtilities.initialiseLogins();
        ApplicationsTestUtilities.intiialiseApplicationCustomSettings();
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        portalSettings.LoanApplicationFormCreationOffset__c = 2;
        insert portalSettings;

        Qualification__c parentQualification = SASTestUtilities.createParentQualification();
        insert parentQualification;

        Qualification__c tailoredQualification = SASTestUtilities.createTailoredQualification(parentQualification);
        insert tailoredQualification;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        GuestLogin__c student = [SELECT Id FROM GuestLogin__c WHERE RecordType.Name = 'Student'];

        Funding_Stream_Types__c  VFHFundingStream = new Funding_Stream_Types__c();
        VFHFundingStream.Name = 'VFH';
        VFHFundingStream.FundingStreamShortname__c = 'VFH';
        insert VFHFundingStream;

        ApplicationForm__c form = new ApplicationForm__c(
                StudentCredentialsId__c = student.Id,
                IsMainApplicationForm__c = true,
                StateId__c = stateQLD.Id,
                Status__c = 'Submitted',
                SubmissionDate__c = Datetime.newInstance(2015, 10, 1),      // Thursday, 1 October 2015
                QualificationId__c = tailoredQualification.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId(),
                LLN_End_Date__c = Date.today(),
                LLN_Score_1__c = '3',
                LLN_Score_3__c = '4',
                FundingStreamTypeId__c = VFHFundingStream.Id
            );
        insert form;
    }

    static testMethod void testCaseNextDay() {
        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 2));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }


    static testMethod void testCaseMonday() {
        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(1, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testCaseMondayHoliday() {
        Holiday__c holiday = new Holiday__c(
                Date__c = Date.newInstance(2015, 10, 5),
                Name = 'Holiday Test'
            );
        insert holiday;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testCaseMondayStateHolidayPostive() {
        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Holiday__c holiday = new Holiday__c(
                Date__c = Date.newInstance(2015, 10, 5),
                Name = 'Holiday Test',
                StateId__c = stateQLD.Id
            );
        insert holiday;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testCaseMondayStateHolidayPostive2() {
        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Holiday__c holiday = new Holiday__c(
                Date__c = Date.newInstance(2015, 10, 5),
                Name = 'Holiday Test',
                StateId__c = stateQLD.Id
            );
        insert holiday;

        holiday = new Holiday__c(
                Date__c = Date.newInstance(2015, 10, 6),
                Name = 'Holiday Test 2',
                StateId__c = stateQLD.Id
            );
        insert holiday;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 6));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testCaseMondayStateHolidayNegative() {
        State__c stateNSW = [SELECT Id FROM State__c WHERE Short_Name__c = 'NSW'];
        Holiday__c holiday = new Holiday__c(
                Date__c = Date.newInstance(2015, 10, 5),
                Name = 'Holiday Test',
                StateId__c = stateNSW.Id
            );
        insert holiday;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(1, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testMinuteDelay() {
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        portalSettings.MinutesDelayed__c = 48;
        update portalSettings;

        ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];
        appForm.SubmissionDate__c = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        update appForm;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Datetime.newInstance(Date.today(), Time.newInstance(0, 60, 0, 0)));
            Database.executeBatch(nextBatch);
        Test.stopTest();
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault = true];
        if (BusinessHours.isWithin(bh.Id, Datetime.now())) {
            System.assertEquals(1, [SELECT count() FROM LoanForm__c]);
        }
    }

    static testMethod void testNegativeLLN1() {
        ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];
        appForm.SubmissionDate__c = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        appForm.LLN_Score_1__c = '2';
        update appForm;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testNegativeLLN2() {
        ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];
        appForm.SubmissionDate__c = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        appForm.LLN_Score_3__c = '2';
        update appForm;

        Test.startTest();
            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testClosedServiceCase() {
        ApplicationForm__c appForm = [SELECT Id, ServiceCaseId__c, StudentCredentialsId__r.AccountId__c FROM ApplicationForm__c];
        Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
        serviceCase.Account_Student_Name__c = appForm.StudentCredentialsId__r.AccountId__c;
        serviceCase.RecordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
        serviceCase.Status__c = 'Closed';
        insert serviceCase;

        appForm.ServiceCaseId__c = serviceCase.Id;
        update appForm;

        Test.startTest();
            System.assert(String.isNotBlank(appForm.ServiceCaseId__c));

            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
    }

    static testMethod void testClosedServiceCaseNegative() {
        ApplicationForm__c appForm = [SELECT Id, ServiceCaseId__c, StudentCredentialsId__r.AccountId__c FROM ApplicationForm__c];
        Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
        serviceCase.Account_Student_Name__c = appForm.StudentCredentialsId__r.AccountId__c;
        serviceCase.RecordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
        serviceCase.Status__c = 'New';
        insert serviceCase;

        appForm.ServiceCaseId__c = serviceCase.Id;
        update appForm;

        Test.startTest();
            System.assert(String.isNotBlank(appForm.ServiceCaseId__c));

            System.assertEquals(0, [SELECT count() FROM LoanForm__c]);
            // Simulating a run on the next day
            LoanFormCreationBatch nextBatch = new LoanFormCreationBatch(Date.newInstance(2015, 10, 5));
            Database.executeBatch(nextBatch);
        Test.stopTest();
        System.assertEquals(1, [SELECT count() FROM LoanForm__c]);
    }
}