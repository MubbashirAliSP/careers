public with sharing class NewAccountRedirect_CC {
	
	private ApexPages.StandardController controller {get; set;}
	private Account acct;
	
	public NewAccountRedirect_CC(ApexPages.StandardController controller) {
        
        //initialize the stanrdard controller
        this.controller = controller;
        this.acct = (Account)controller.getRecord();
    }
	public PageReference redirect(){
		Country__c country = new Country__c();
		Language__c language = new Language__c();
		Labour_Force_Status_Code__c labour = new Labour_Force_Status_Code__c();
		country = [select Id, Name from Country__c where Name='Not Specified'];
		language = [select Id, Name from Language__c where Name = 'Not Specified'];
		labour = [select Id, Name from Labour_Force_Status_Code__c where Name='Not Specified'];
		
		String url = ''; 
		url += '/setup/ui/recordtypeselect.jsp?ent=Account'; 
		url += '&save_new_url=001/e?';
		//country of birth
		url += '&CF00N90000004xMfg_lkid='+country.Id; 
		url += '&CF00N90000004xMfg=Not Specified'; 
		//labour force status
		url += '&CF00N90000004xMg6_lkid='+labour.Id; 
		url += '&CF00N90000004xMg6=Not Specified'; 
		//main language spoken
		url += '&CF00N90000004xMgD_lkid='+language.Id; 
		url += '&CF00N90000004xMgD=Not Specified'; 
		url += '&nooverride=1'; 
		url += '&retURL={!Account.Id}'; 
		
		return new PageReference(url);
	}
}