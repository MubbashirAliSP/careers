@isTest
public class USI_UtilityClass_Test {

	public static Account student = NATTestDataFactory.createStudent(); 
    
    public static String usi = 'X123V456YZ';
    
    static testMethod void testSetUsiBatch() {
        
        update USI_UtilityClass.setUSIBatch(usi, student.Id);
        
        Account studentCheck = [ SELECT Id, Unique_Student_Identifier__c, Validated_By_Webservice__c, Date_Of_Last_Sync__c FROM Account WHERE Id = :student.Id];
        
        system.assert(studentCheck.Unique_Student_Identifier__c == usi );
        system.assert(studentCheck.Validated_By_Webservice__c);
        system.assert(studentCheck.Date_Of_Last_Sync__c == date.today());
     
        
    }
    
    static testMethod void testVerifyUSIBatch() {
       
        update USI_UtilityClass.verifyUSIBatch(student.Id);
            
        Account studentCheck = [ SELECT Id, Unique_Student_Identifier__c, Validated_By_Webservice__c, Date_Of_Last_Sync__c FROM Account WHERE Id = :student.Id];
        
        system.assert(studentCheck.Validated_By_Webservice__c);
        system.assert(studentCheck.Date_Of_Last_Sync__c == date.today());
        
    }
    
    static testMethod void testCreateUsiFailureBatch() {
        
        update USI_UtilityClass.createUsiFailureBatch(student.Id);
        
        Account studentCheck = [ SELECT Id, Unique_Student_Identifier__c, Sync_Error__c, Invalidated_by_Webservice__c, Date_Of_Last_Sync__c FROM Account WHERE Id = :student.Id];
        
        system.assert(studentCheck.invalidated_By_Webservice__c);
        system.assert(studentCheck.Sync_Error__c);
        system.assert(studentCheck.Date_Of_Last_Sync__c == date.today());
        
    }
    
    static testMethod void testCreateServiceCase() {
        
    	Service_Cases__c svcCase = USI_UtilityClass.createServiceCase(student.Id, 'Stuff Happened', 'Create');
        
        Service_Cases__c checkSvcCase = [SELECT Id, Account_Student_Name__c, Description__c, USI_Issue_Type__c, Requires_USI_Assistance__c, Subject__c FROM Service_Cases__c WHERE ID = :svcCase.Id];
        
        system.assert(checkSvcCase.Account_Student_Name__c == student.Id);
        system.assert(checkSvcCase.Description__c == 'Stuff Happened');
        system.assert(checkSvcCase.USI_Issue_Type__c == 'Create'); 
        system.assert(checkSvcCase.Requires_USI_Assistance__c);
        system.assert(checkSvcCase.Subject__c == 'USI Generation/Verification Issue');
        

    }

    
}