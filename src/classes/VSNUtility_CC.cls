public with sharing class VSNUtility_CC {
    public string month {get;set;}
    public string year {get;set;}
    public string rto {get;set;}
    public transient List<Enrolment__c> enrolmentList {get;set;}
    public List<pageWrapper> pageWrapperList {get;set;}
    public string body {get;set;}
    private String libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'VSN Library' LIMIT 1].Id;
    public List<SelectOption> getmonthPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', ''));
        options.add(new SelectOption('1', 'January'));
        options.add(new SelectOption('2', 'February'));
        options.add(new SelectOption('3', 'March'));
        options.add(new SelectOption('4', 'April'));
        options.add(new SelectOption('5', 'May'));
        options.add(new SelectOption('6', 'June'));
        options.add(new SelectOption('7', 'July'));
        options.add(new SelectOption('8', 'August'));
        options.add(new SelectOption('9', 'September'));
        options.add(new SelectOption('10', 'October'));
        options.add(new SelectOption('11', 'November'));
        options.add(new SelectOption('12', 'December'));
        return options;
    }
    public List<SelectOption> getyearPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', ''));
        for(Integer i=2012; i <= 2050; i++){
            options.add(new SelectOption(string.valueOf(i), string.valueOf(i)));
        }
        return options;
    }
    public List<SelectOption> getRTOPicklist() {
        List<SelectOption> options = new List<SelectOption>();
        List<Account> rtos = new List<Account>();
        rtos = [select Id, Name from Account where Recordtype.Name =: 'Training Organisation'];
        options.add(new SelectOption('', ''));
        for(Account a : rtos){
            options.add(new SelectOption(a.Name, a.Name));
        }
        return options;
    }
    public PageReference generateEnrolmentList(){
        enrolmentList = new List<Enrolment__c>();
        pageWrapperList = new List<pageWrapper>();
        Set<String> enrolmentChecker = new Set<String>();
        if(month == null || year== null || month == '' || year== ''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please select Month and Year.'));
        }
        else if(rto == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please select RTO.'));
        }
        else{
            enrolmentList = [select Id, Start_Date__c, End_Date__c, Student__r.FirstName, Student__r.LastName, Student__r.PersonBirthdate,
                                Student__r.Victorian_Student_Number__c, Student__r.Student_Identifer__c, Student__r.Middle_Name__c, Student__r.Sex__c, 
                                Delivery_Location__r.State_Short_Name__c    
                                from Enrolment__c 
                                where (CALENDAR_MONTH(End_Date__c) >=: integer.valueOf(month) OR CALENDAR_YEAR(End_Date__c) >=: integer.valueOf(year))
                                AND (CALENDAR_MONTH(Start_Date__c) <=: integer.valueOf(month) AND CALENDAR_YEAR(Start_Date__c)<=: integer.valueOf(year))
                                AND Enrolment_Status__c LIKE 'Active%'
                                AND Delivery_Location__r.State_Short_Name__c LIKE 'VIC%'
                                AND Training_Organisation__c =: rto
                                ORDER BY Student__r.FirstName
                                ];
            system.debug('MONTH'+month);
            system.debug('YEAR'+year);
            if(enrolmentList.size() == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'No results found for selection.'));
            }                           
            else{
                for(Enrolment__c e :  enrolmentList){
                    Decimal age;
                    system.debug('age@@@'+e.Student__r.PersonBirthdate);
                    if(e.Student__r.PersonBirthdate!=null){
                        age = e.Student__r.PersonBirthdate.daysBetween(date.today())/365.2425;
                        system.debug('age***'+age);
                    }
                    if(!enrolmentChecker.contains(e.Student__r.Student_Identifer__c)){
                        if(age <= 25){
                            pageWrapperList.add(new PageWrapper(e, e.Student__r.Sex__c.left(1), UserInfo.getName()));
                            enrolmentChecker.add(e.Student__r.Student_Identifer__c);
                        }
                    }
                }
            }
            system.debug('enrolmentChecker***'+enrolmentChecker);
            system.debug('pageWrapperList***'+pageWrapperList);
        }
        return null;
    }
    
    public pageReference exportData() {
            
        Id libraryId;
        String queryString = '';
        
        try {libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'VSN Library' LIMIT 1].Id;
            List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
                if(existingContent.size() > 0){
                    delete existingContent;
                }
        }
        catch(Exception e){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'VSN Library not found or you do not have permission!'));}
        
        generateEnrolmentFile();
        
        return new PageReference('/sfc/#search?');
    }
    
    public void generateEnrolmentFile() {
        
        String body = '';

        for(pageWrapper e : pageWrapperList) 
        {
             body += e.enrolment.Student__r.Victorian_Student_Number__c + ',';
             body += e.enrolment.Student__r.FirstName + ',';
             body += e.enrolment.Student__r.LastName + ',';
             body += e.enrolment.Student__r.PersonBirthdate + ',';
             body += e.gender + ',';
             body += e.enrolment.Student__r.Middle_Name__c + ',';
             body += e.enrolment.Student__r.Student_Identifer__c + ',';
             body += e.username;
             body += '\r\n';
        }
            
            Blob pBlob = Blob.valueof(body);
            
            Date dInputDate = date.today();

            //Format Date
            DateTime dtValue = DateTime.newInstance(dInputDate.year(), dInputDate.month(), dInputDate.day());
            string sFormattedDate = dtValue.format('yyyy-MM-dd');
            
            if(Test.isRunningTest()) {
                pBlob = Blob.valueOf('Test Blob');
            }
            
            insert new ContentVersion(
                VersionData =  pBlob,
                Title = 'VSN_' + sFormattedDate,
                PathOnClient = '/VSN_' + sFormattedDate + '.csv',
                FirstPublishLocationId = libraryId
                );      
       // return new PageReference('/sfc/#search?');            
    }
    
    public class pageWrapper{
        public Enrolment__c enrolment {get;set;}
        public string gender {get;set;}
        public string username {get;set;}
        
        public pageWrapper(Enrolment__c enrolment, string gender, string username){
            this.enrolment = enrolment;
            if(gender!=null || gender!=''){
                this.gender = gender;
            }else{
                this.gender = '';
            }
            if(username!=null || username!=''){
                this.username = username;
            }else{
                this.username = '';
            }
        }
    }
}