/**
 * @description Updates the Inactive field on the Results object
 * @author Janella Lauren Canlas
 * @date 13.NOV.2012
 * @history
 * 		 13.NOV.2012	Janella Canlas		Created.
 * 		 06.FEB.2013	Janella Canlas		re-create Scheduled Apex and Add method to Update Intake Status add method to update Qualification checkbox
 *		 01.MAR.2016	Ranyel Maliwanag	- Added limit to query in updateIntakeStatus as it's reaching the Max DML rows limit. Need to reevaluate if this class is still relevant as I don't see it being referenced elsewhere
 */
public with sharing class UpdateResultStatus {
	public List<Results__c> resultsList= new List<Results__c>();
	public UpdateResultStatus(){
		executeUpdate();
		updateIntakeStatus(); 
	}
	public static void executeUpdate(){
		//retrieve all result records to be updated
		List<Results__c> resultsList = [SELECT Id, Inactive__c, Inactive_Date__c FROM Results__c WHERE Inactive_Date__c < TODAY AND Inactive__c = false];
		if(resultsList.size() > 0){
			for(Results__c r : resultsList){
				r.Inactive__c = true;
			}
			try{ update resultsList; }catch(Exception e){ System.debug('ERROR: ' + e.getMessage());}
		}
	}
	public static void updateIntakeStatus(){
		List<Intake__c> intakeList = [Select Id, Intake_Status__c, Inactive_Intake_Reason__c FROM Intake__c WHERE Intake_Status__c = 'ACTIVE' AND End_Date__c < TODAY LIMIT 500];
		if(intakeList.size() > 0){
			for(Intake__c i : intakeList){
				i.Intake_Status__c = 'Inactive';
				i.Inactive_Intake_Reason__c = 'Intake Expired'; 
			}
			try{
				update intakeList;}catch(Exception e){System.debug('ERROR: ' + e.getMessage());
			}
		}
	}
	public static void updateQualificationStatus(){
		List<Qualification__c> qualList = [Select Id, Active__c, Expiry_Date__c FROM Qualification__c WHERE Active__c = false AND Expiry_Date__c < TODAY];
		if(qualList.size() > 0){
			for(Qualification__c q : qualList){
				q.Active__c = true;
			}
			try{
				update qualList;}catch(Exception e){System.debug('ERROR: ' + e.getMessage());
			}
		}
	}
}