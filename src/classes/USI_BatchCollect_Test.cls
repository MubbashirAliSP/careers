@isTest
public class USI_BatchCollect_Test {
    
    public static Account student = NATTestDataFactory.createStudent();
    public static Country__c ctry = NATTestDataFactory.createCountry();
    public static ANZSCO__c anz = NATTestDataFactory.createANZSCO();
    public static Unit__c unt = NATTestDataFactory.createUnit();
    public static Account tOrg = NATTestDataFactory.createTrainingOrganisation();
    public static Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();
    
    public static State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
    public static Results__c res = NATTestDataFactory.createResult(sta.Id);
    public static Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
    public static Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
    public static Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);

    public static Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);   
    public static Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
    public static Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
    public static Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
    public static Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
    public static Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
    public static Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
    public static Enrolment__c enrl = NATTestDataFactory.createEnrolment(student.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
    public static Enrolment_Unit__c enu1 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);
    public static Enrolment_Unit__c enu2 = NATTestDataFactory.createEnrolmentUnit(unt.Id, enrl.Id, res.Id, loc.Id, liqu.Id);    
    
    public class usiCreateResponse {
        String USI;
        String message;
        String status;
    }
    
    public class usiVerifyResponse {
        String dateofbirth; 
        String firstname;
        String usi;
        String message;
        String lastname;
    }
    
    static void insertCustomSetting() {
        
        USI_Configuration__c usiConfig = new USI_Configuration__c();
        
        usiConfig.Name = 'Default';
        usiConfig.Timeout__c = 60000;
        usiConfig.VerifyUSI_endpoint__c = '/verifyUSI';
        usiConfig.CreateUSI_endpoint__c = '/createUSI';
        
        insert usiConfig;        
        
    }
    
    
    static testMethod void testCollectStudentsCreate() {
        
        usiCreateResponse ucr = new usiCreateResponse();
        ucr.USI = 'X123Y456ZV';
        ucr.message = 'Success'; 
        ucr.status = 'Success';
        
        
        String mockBody = JSON.serialize(ucr);
        USI_MockWebService fakeResponse = new USI_MockWebService(200, 'Complete', mockBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        student.ReSync__c = true;
        student.Validated_by_WebService__c = false;
        student.manually_verified__c = false;
        
        update student;
        
        Database.executeBatch(new USI_BatchCollect(true), 1);
                
    }
     
    static testMethod void testCollectStudentsValidate() {
        
        usiVerifyResponse uvr = new usiVerifyResponse();
        uvr.dateofbirth = String.valueOf(student.PersonBirthdate);
        uvr.firstname = student.FirstName;
        uvr.usi = 'X123Y456ZV';
        uvr.message = 'Success';
        uvr.lastname = student.LastName;

        
        String mockBody = JSON.serialize(uvr);
        USI_MockWebService fakeResponse = new USI_MockWebService(200, 'Complete', mockBody);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        student.Validated_by_WebService__c = false;
        student.manually_verified__c = false;
        student.Unique_student_identifier__c = 'X123Y456ZV';
        
        update student;
        
        Database.executeBatch(new USI_BatchCollect(false), 1);
                
    }
    

}