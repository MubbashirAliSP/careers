/*
Class Name: NATGeneratorUtility
Revision History:
07.JUL.2015     Michi Magsarili - MAM -  Updated existing logic for formatCUV and formatCOV for case 00076891 (SAS011706)
*/

public with sharing class NATGeneratorUtility {
    
     public static String formatCheckBox(Boolean checked) {
        if(checked)
            return 'Y';
        else
            return 'N';
     }
     
     public static Integer convertFromDecimalToInteger(Decimal decimalValue) {
        
        Integer integerValue = decimalValue.intValue();
        
        return integerValue;
     }
     
      //Format Date to DDMMYYYY
     public static String formatDate (String s) {
        if(s == null) {return '';}
        String[] temp = s.split('-');
        
        if (temp.size()>2)
            return temp[2]+temp[1]+temp[0];
        else return s;
     }
    
    /*
     *  Author: Patrisha Louisse O. Sales
     *  Description: Method used to format nominal hours
     *  History: 17-JUL-2012 - PSales - Created
     */
    
     public static String formatHours(String s, Decimal d) {
        String body = '';
        Integer l = Integer.valueOf(d);
        
        if (s == null) {
            for (Integer i = 0; i < l; i++) {
                body += '0';
            }
        } else if (s.length() < l) {
            for (Integer i = 0; i < (l-s.length());i++){
                body += '0';
            }
            body+=s;
        } else {
            body += s.substring(0,l);
        }
        
        return body;
     }
     
     public static String formatNominalHours(String s, Decimal d) {
        
        String body = '';
        Integer l = Integer.valueOf(d);
        
        if (s == null) {
            for (Integer i = 0; i < l; i++) {
                body += '0';
            }
        } else if (s.length() < l) {
            for (Integer i = 0; i < (l-s.length());i++){
                body  = '0' + body;
            }
            body+=s;
        } else {
            body += s.substring(0,l);
        }
        
        return body;
        
     }
    /*
     *  Author: Patrisha Louisse O. Sales
     *  Description: Method used to format each record for the txt file
     *  History: 30-JUL-2012 - PSales - Created for portability
     */
     public static String newFormatTxtFile(String s, Decimal d) {
        String body = '';
        Integer l = Integer.valueOf(d);
        
        if (s == null) {    //if null, pad only with spaces
            for (Integer i = 0; i < l; i++) {
                body += '@';
            }
        } else if (s.length() < l) {    //if less than the full length, pad remaining characters with spaces
            body += s;
            for (Integer i = 0; i < (l-s.length()); i++) {
                body += ' ';
            }
        } else if (s.length() == l) {   //if length is the same as the full length, as is
            body += s;
        } else {
            body += s.substring(0,l);   //if length exceeds full length, truncate
        }
        
        return body.toUppercase();
     }
    
    /*
     *  Author: Patrisha Louisse O. Sales
     *  Description: Method used to format each record for the txt file
     *  History: 06-JUL-2012 - PSales - Created for portability
     */
     public static String formatOrPadTxtFile(String s, Decimal d) {
        String body = '';
        Integer l = Integer.valueOf(d);
        
        if (s == null) {    //if null, pad only with spaces
            for (Integer i = 0; i < l; i++) {
                body += ' ';
            }
        } else if (s.length() < l) {    //if less than the full length, pad remaining characters with spaces
            body += s;
            //for (Integer i = 0; i < (l-s.length()); i++) {
            //    body += ' ';
            //}
            body = body.rightPad(l-s.length());
        } else if (s.length() == l) {   //if length is the same as the full length, as is
            body += s;
        } else {
            body += s.substring(0,l);   //if length exceeds full length, truncate
        }
        
        return body.toUppercase();
     }
     public static String formatOrPadTxtFile(String s, Decimal d, String body) {
        system.debug('%%s: '+s);
        system.debug('%%d: '+d);
        system.debug('%%body: '+body);
        
        if(s == '@@'){
            body = '99';
        }
        else if(s == '@@@@'){
            body = '9999';
        }
        else if (s == null || s==''){//if null, pad only with spaces
            system.debug('%%1');
            body = body.rightPad(Integer.valueOf(d));
            system.debug('%%1body: '+body);
        }else if (s.length() < Integer.valueOf(d)) {//if less than the full length, pad remaining characters with spaces
            system.debug('%%2');
            body += s;
            //body = body.rightPad(Integer.valueOf(d)-s.length());     
            body = body.rightPad(Integer.valueOf(d)); 
        }else if (s.length() == Integer.valueOf(d)){//if length is the same as the full length, as is
            system.debug('%%3');
            body += s;   
        }else{//if length exceeds full length, 
            system.debug('%%4');
            body += s.substring(0,Integer.valueOf(d));               
        }       
        return body.toUppercase();
     }
     
     public static String formatAmmount(Decimal d) {
        if(d!=null){
            Decimal cov = d;
            String covStr = String.valueOf((cov/10000000).setScale(9).toPlainString());
            String covSemiFinal = covStr.remove('.');
            Integer covSemiFinalLen = covSemiFinal.length();
            String covFinal = covSemiFinal.substring(2,covSemiFinalLen);

            system.debug('@@cov'+cov);
            system.debug('@@covStr'+covStr);
            system.debug('@@covSemiFinal'+covSemiFinal);
            system.debug('@@covSemiFinalLen'+covSemiFinalLen);
            system.debug('@@covFinal'+covFinal);
            return covFinal;
        }else{
            return '00000000';
        }
        /*String superFinal;
        if(s==null || s==''){
            superFinal='00000000';
        }else{
            String temp=s;
            List<String> strList = new List<String>();
            for(Integer i=0;i<temp.length();i++){
                system.debug('@@'+temp.substring(i,i+1));
                if(temp.substring(i,i+1)!='.'){
                    strList.add(temp.substring(i,i+1));
                }    
            }
            String finalValue = '';
            for(String t: strList){
                finalValue+=t;
            }
            system.debug('%%finalValue: '+finalValue);
            system.debug('%%finalValue: '+finalValue.length());
            String strZero = '';
            if(finalValue.length()<8){
                for(Integer i=0;i<(8-finalValue.length());i++){
                    strZero+='0';
                }
            }
            system.debug('^^strZero: '+strZero);
            superFinal = strZero+finalValue;
            system.debug('^^superFinal: '+superFinal);
        }
        return superFinal;*/
     }
     
     public static String formatOrPadTxtFileNSW(String s, Decimal d) {
        String body = '0';
        Integer l = Integer.valueOf(d);
        
        if (s == null) {    //if null, pad only with spaces
            for (Integer i = 0; i < l; i++) {
                body += '0';
            }
        } else if (s.length() < l) {    //if less than the full length, pad remaining characters with spaces
            body += s;
          
            body = body.rightPad(l-s.length());
        } else if (s.length() == l) {   //if length is the same as the full length, as is
            body += s;
        } else {
            body += s.substring(0,l);   //if length exceeds full length, truncate
        }
        
        return body.toUppercase();
     }
     
     public static String formatEFTSL(String s) {
        String y = '';
        if(s!=null){
            String x = s.remove('.');
            system.debug('%%x: '+x);
            system.debug('%%xLEN: '+x.length());
            for(Integer i=x.length();i<10;i++){
                system.debug('%%i: '+i);
                y+='0';
            }
            system.debug('%%y: '+y);
            system.debug('%%final: '+x+y);
            return x+y;
        }else{
            system.debug('%%final: null');
            return '0000000000';
        }
        return null;
    }
     
     /*Generates Auto Number Claim number from passed state */
     public static Decimal nextClaimNumber(String State) {
        
        List<Claim_Number__c> claimNumbersInDB = new List<Claim_Number__c>([Select Claim_Counter__c FROM Claim_Number__c WHERE State__c = : State order by Claim_Counter__c desc LIMIT 1]);
        
        //No Claims for state
        if(claimNumbersInDB.size() == 0)
            return 1.0;
        else
            return claimNumbersInDB.get(0).Claim_Counter__c + 1.0; // return next claim number
        
        
     }
     
     /*claim type calculation */
     
     public static List<Integer>claimCodes(String ClaimType) {
         if(ClaimType == 'Commencement Claim')
            return new List<Integer>{0,3,6};
            
         if(ClaimType == 'Midway Claim')
            return new List<Integer>{1,4,7};
            
         if(ClaimType == 'Final Claim')
            return new List<Integer>{2,5,8};
            
            
        return  new List<Integer>{2,5,8};
     }
     
     public static String convertLanguageSpokenCodeWA(String code) {
        
        if(code == '1201')
            return 'Y';
        else
            return 'N'; 
        
        
     }
     
     public static String formatPhone(String pnumber) {
        
        if(pnumber == null)
            return '';
        
        pnumber = pnumber.trim();

        pnumber = pnumber.replaceAll(' ', '');

        return pnumber;
        
     }
    
    public static String formatHEIMSBirthdate(Date bdate) {
        if(bdate == null)
         return '19010101';
        else {
            
             Date dateToday = bdate;
             String sMonth = String.valueof(dateToday.month());
             String sDay = String.valueof(dateToday.day());
                if(sMonth.length()==1){
                   sMonth = '0' + sMonth;
                    }
                 if(sDay.length()==1){
                    sDay = '0' + sDay;
                 }
    
             return String.valueof(dateToday.year())  + sMonth  + sDay ;
            
            
            
        }
            
        
    }
    
    public static String formatTFN(String s){ 
        //MAM 09/02/2013 modify condition statement to check if TFN is not empty start
        //if(s!=null || s!=''){
        if(String.IsNotEmpty(s)){
            return '00'+s;
        }
        //MAM 09/02/2013 end
        else{
            return '00';
        }
    }
    
    //MAM 09/06/2013 new method formatCountry start
    public static String formatCountry(String Country) {
        if(String.IsNotEmpty(Country) && Country.equals('AUSTRALIA')){
            return '';
        }else{
            return Country;
        }
    }
    //MAM 09/06/2013 end
    
    public static String formatPT(Enrolment__c e) {
        //MAM 09/02/2013 change to Salutation start
        //if(String.IsNotEmpty(e.Student__r.PersonTitle)){
        if(String.IsNotEmpty(e.Student__r.Salutation)){
            //Remove Periods('.') as this is not Alphanumerical 
            return e.Student__r.Salutation.Replace('.','');
        //MAM 09/02/2013 change to Salutation end   
        }else{
            if(e.Student__r.Sex__c == 'M - Male'){
                return 'Mr';
            }else if(e.Student__r.Sex__c == 'F - Female'){
                return 'Ms';
            }else{
                return 'Not Specified';
            }
        }
    }
    
    public static String formatHEIMSLocationCodeOfPermHomeResidence(Enrolment__c enrl) {
            //MAM 01/21/2014 change to Enrolment Address
            //MAM 09/26/2013 start Student Address
            //if(enrl.Student__r.PersonMailingCountry == 'AUSTRALIA'){
            if(enrl.Mailing_Country__c == 'AUSTRALIA'){
                //return 'A'+enrl.Student__r.PersonMailingPostalCode;
                return 'A'+enrl.Mailing_Zip_Postal_Code__c; 
            }
            //else if(enrl.Student__r.PersonMailingCountry != 'AUSTRALIA' && enrl.Overseas_Country__c != null){
            else if(enrl.Mailing_Country__c != 'AUSTRALIA' && enrl.Overseas_Country__c != null){
                return  'X'+enrl.Overseas_Country__r.Country_Code__c;
            }
            else{
                return  '99999';
            }
            //MAM 09/26/2013 end
            //MAM 01/21/2014 end
    }
    
    public static String formatHEIMSLocationCodeOfTempHomeResidence(Enrolment__c enrl) {
                //MAM 01/21/2014 change to Enrolment Address
                //MAM 09/26/2013 change to Student Address
                //if(enrl.Student__r.PersonMailingCountry == 'AUSTRALIA'){
                if(enrl.Mailing_Country__c == 'AUSTRALIA'){
                    //return 'A'+enrl.Student__r.PersonMailingPostalCode; 
                    return 'A'+enrl.Mailing_Zip_Postal_Code__c; 
                }
                //else if(enrl.Student__r.PersonMailingCountry != 'AUSTRALIA' && enrl.Overseas_Country__c != null){
                else if(enrl.Mailing_Country__c != 'AUSTRALIA' && enrl.Overseas_Country__c != null){
                    return  'X'+enrl.Overseas_Country__r.Country_Code__c;
                }
                else{
                    return  '99999';
                    
                }
                //MAM 09/26/2013 end
                //MAM 01/21/2014 end
    }
    
    public static String formatHEIMSYearOfArrival(Enrolment__c e) {
        if(e.Student__c !=null || e.Student__c !=''){
            if(e.Student__r.Country_of_Birth__r.Name=='Australia'){
                return '0001';
            }
            //MAM 01/21/2014 change to Enrolment Address
            //MAM 01/17/2014 Country of Birth is "Not Specified", then Arrival Status should default to "A999: No information on whether student was born in Australia or not"
            //else if((e.Student__r.Country_of_Birth__r.Name!='Australia' && !String.ValueOf(e.Student__r.Country_of_Birth__r.Name).Equals('Not Specified')) && 
            //         e.Student__r.PersonMailingCountry=='AUSTRALIA' && 
            //        (e.Year_of_Arrival_in_Australia__c==null || e.Year_of_Arrival_in_Australia__c=='')){
            else if((e.Student__r.Country_of_Birth__r.Name!='Australia' && !String.ValueOf(e.Student__r.Country_of_Birth__r.Name).Equals('Not Specified')) && 
                     e.Mailing_Country__c=='AUSTRALIA' && 
                    (e.Year_of_Arrival_in_Australia__c==null || e.Year_of_Arrival_in_Australia__c=='')){
                return 'A998';
            }
            else if(e.Student__r.Country_of_Birth__r.Name==null || 
                    e.Student__r.Country_of_Birth__r.Name=='' || 
                    String.ValueOf(e.Student__r.Country_of_Birth__r.Name).equals('Not Specified') ){
                return 'A999';
            }
            //else if((e.Student__r.Country_of_Birth__r.Name!='Australia' || e.Student__r.Country_of_Birth__r.Name!=null) && (e.Student__r.PersonMailingCountry!='AUSTRALIA' || e.Student__r.PersonMailingCountry==null)){
            else if((e.Student__r.Country_of_Birth__r.Name!='Australia' || e.Student__r.Country_of_Birth__r.Name!=null) && (e.Mailing_Country__c!='AUSTRALIA' || e.Mailing_Country__c==null)){
                return '0000';
            }
            else if(e.Student__r.Arrival_Status_Identifer__c=='****'){
                return e.Year_of_Arrival_in_Australia__c;
            }
            else if(e.Student__r.Arrival_Status_Identifer__c!='****' && (e.Student__r.Arrival_Status_Identifer__c!=null || e.Student__r.Arrival_Status_Identifer__c!='')){
                return e.Student__r.Arrival_Status_Identifer__c;
            //MAM 01/17/2014 end
            //MAM 01/21/2014 end
            }else{
                return '@@@@';
            }
        }else{
            return '****';
        }
    }
    
    public static String formatHEIMSDisabilityCode(Enrolment__c e) {
        
        String firstChar = '';
        String secondChar = '';
        String thirdChar = '';
        String fourthChar = '';
        String fifthChar = '';
        String sixthChar = '';
        String seventhChar = '';
        String eighthChar = '';
        
            if(e.Does_student_have_a_disability__c == 'Unknown'){
                firstChar = '0';
            }
            else if(e.Does_student_have_a_disability__c == 'Yes'){
                firstChar = '1';
            }
            else if(e.Does_student_have_a_disability__c == 'No'){
                firstChar = '2';
            }
            if(e.Indicate_Areas_of_Imparement__c != null){
                if(e.Indicate_Areas_of_Imparement__c.contains('Hearing')){
                    secondChar = '1';
                }
                else{
                    secondChar = '0';
                }
                if(e.Indicate_Areas_of_Imparement__c.contains('Learning')){
                    thirdChar = '1';
                }
                else{
                    thirdChar = '0';
                }
                if(e.Indicate_Areas_of_Imparement__c.contains('Mobility')){
                    fourthChar = '1';
                }
                else{
                    fourthChar = '0';
                }
                /*if(e.Indicate_Areas_of_Imparement__c.contains('Visual')){
                    fifthChar = '1';
                }
                else{
                    fifthChar = '0';
                }*/
                if(e.Indicate_Areas_of_Imparement__c.contains('Vision')){
                    fifthChar = '1';
                }
                else{
                    fifthChar = '0';
                }
                if(e.Indicate_Areas_of_Imparement__c.contains('Medical')){
                    sixthChar = '1';
                }
                else{
                    sixthChar = '0';
                }
                if(e.Indicate_Areas_of_Imparement__c.contains('Other')){
                    seventhChar = '1';
                }
                else{
                    seventhChar = '0';
                }
            }
            else{
                secondChar = '0';
                thirdChar = '0';
                fourthChar = '0';
                fifthChar = '0';
                sixthChar = '0';
                seventhChar = '0';
            }
            /*if(e.Does_student_have_a_disability__c == 'Unknown'){
                eighthChar = '0';
            }
            else if(e.Requested_advice_on_support_services__c == true && e.Does_student_have_a_disability__c == 'Yes'){
                eighthChar = '1';
            }
            else if(e.Requested_advice_on_support_services__c == false && e.Does_student_have_a_disability__c == 'Yes'){
                eighthChar = '2';
            }
            if(e.Does_student_have_a_disability__c == 'Yes' && e.Indicate_Areas_of_Imparement__c == null){
                e.addError('Please select Area(s) of Impairment.');
            }
            else if(e.Indicate_Areas_of_Imparement__c != null && e.Does_student_have_a_disability__c != 'Yes'){
                e.addError('Please determine if Student has a disability.');
            }*/
            //MAM 10/09/2013 Check for Disability Status first - if No or Unknown, then eightChar is 0
            //if(&& e.Requested_advice_on_support_services__c == null){
            if(e.Does_student_have_a_disability__c == 'Yes'){
                if(e.Requested_advice_on_support_services__c == true){
                    eighthChar = '1';
                }else if(e.Requested_advice_on_support_services__c == false){
                    eighthChar = '2';
                }
            }
            else{
                eighthChar = '0';
            }
            //MAM 10/09/2013 end
            
            return firstChar + secondChar + thirdChar + fourthChar + fifthChar + sixthChar + seventhChar + eighthChar;
        }       
        
        public static String formatHEP(Enrolment__c e) {            
            if(e.Highest_Educational_Participation__r.Code__c=='09'){
                return '090000';
            }else{
                if((e.Highest_Educational_Participation__r.Code__c!=null && e.Year_of_last_participation__c != null) && (e.Highest_Educational_Participation__r.Code__c!='' && e.Year_of_last_participation__c != '')){
                    return e.Highest_Educational_Participation__r.Code__c+e.Year_of_last_participation__c;
                }else if((e.Highest_Educational_Participation__r.Code__c==null && e.Year_of_last_participation__c == null) || (e.Highest_Educational_Participation__r.Code__c=='' && e.Year_of_last_participation__c == '')){
                    return '090000';
                }else if((e.Highest_Educational_Participation__r.Code__c!=null && e.Year_of_last_participation__c == null) || (e.Highest_Educational_Participation__r.Code__c!='' && e.Year_of_last_participation__c == '')){
                    return e.Highest_Educational_Participation__r.Code__c+'9999';
                }else if((e.Highest_Educational_Participation__r.Code__c==null && e.Year_of_last_participation__c != null) || (e.Highest_Educational_Participation__r.Code__c=='' && e.Year_of_last_participation__c != '')){
                    return '090000';
                }else{
                    return '090000';
                }
            }
        }
        
        public static String formatFOEOP(Enrolment__c e) {
            /*if(e.Field_of_Ed_of_prior_VET_Credit_RPL_Code__c != null || e.Field_of_Ed_of_prior_VET_Credit_RPL_Code__c!=''){
                return e.Field_of_Ed_of_prior_VET_Credit_RPL_Code__c;
            }else if((e.Field_of_Ed_of_prior_VET_Credit_RPL__c!=null || e.Field_of_Ed_of_prior_VET_Credit_RPL__c!='') && (e.Field_of_Ed_of_prior_VET_Credit_RPL_Code__c == null || e.Field_of_Ed_of_prior_VET_Credit_RPL_Code__c=='')){
                return e.Field_of_Ed_of_prior_VET_Credit_RPL__r.FoE_Code__c;
            }else{
                return '0000';
            }*/
            if(e.Field_of_Ed_of_prior_VET_Credit_RPL__c!=null || e.Field_of_Ed_of_prior_VET_Credit_RPL__c!=''){
                return e.Field_of_Ed_of_prior_VET_Credit_RPL__r.FoE_Code__c;
            }else{
                return '0000';
            }
        }
        
        //formatCOV - stands for Format Credit Offered Value (COV)
        public static String formatCOV(Enrolment__c e) {
            if(e.Credit_Offered_Value_full_time_load__c !=null){
                Decimal cov = (e.Credit_Offered_Value_full_time_load__c <= 0.10  && e.Credit_Used_Value_full_time_load__c != 0.0) ? 0.10 : e.Credit_Offered_Value_full_time_load__c; //MAM 07.JUL.2015 Case 00076891 (SAS011706) fix - Check if the Credit Offered value is below 0.10% - if so, round up the value to 0.10% else display value as is 
                String covStr = String.valueOf((cov/100).setScale(4)); 
                String covSemiFinal = covStr.remove('.');
                Integer covSemiFinalLen = covSemiFinal.length();
                String covFinal = covSemiFinal.substring(0,covSemiFinalLen-1);
                return covFinal;
            }else{
                return '    ';
            }
        }
        
        //formatCUV - stands for Format Credit Used Value (CUV)
        public static String formatCUV(Enrolment__c e) {
            if(e.Credit_Used_Value_full_time_load__c !=null){
                Decimal cov = (e.Credit_Used_Value_full_time_load__c <= 0.10 && e.Credit_Used_Value_full_time_load__c != 0.0) ? 0.10 : e.Credit_Used_Value_full_time_load__c; //MAM 07.JUL.2015 Case 00076891 (SAS011706) fix - Check if the Credit Used value is below 0.10% - if so, round up the value to 0.10% else display value as is
                String covStr = String.valueOf((cov/100).setScale(4));
                String covSemiFinal = covStr.remove('.');
                Integer covSemiFinalLen = covSemiFinal.length();
                String covFinal = covSemiFinal.substring(0,covSemiFinalLen-1);
                return covFinal;
            }else{
                return '    ';
            }
        }
        
        public static String formatRFS(String s) {
            if(s!=null || s!=''){
                return s;
            }else{
                return '  ';
            }
        }
        
        public static String formatCSHEPC(Enrolment__c e) {
            if(e!=null){
                return e.Credit_status_Higher_Ed_provider__r.Code__c;
            }else{
                return '0000';
            }
        }
        
        public static String formatCOBC(String s) {
            if(s!=null || s!=''){
                if(s=='1101'){
                    return '1100';
                }else{
                    return s;
                }
            }else{
                return '9999';
            }
        }
        
        public static String formatHEIMSCommencingLocationCodeOfPermanentHomeResidence(Enrolment__c enrl) {
            
            if(enrl.Domestic_School_Leaver__c == 'No')
                return '00001';
            else {
                    if(enrl.Post_code_of_Year_12_Perm_home_residence__c != null)
                        return 'A' + enrl.Post_code_of_Year_12_Perm_home_residence__c;
                    else
                        return '99999';
            }
            
        }
        
        //MAM 10/24/2013 new method for duplicate removal
        public static String RemoveDuplicates(String Content){
            List<String> Body = Content.split('\\r\n');
            Set<String> FinalContent = new Set<String>();
            String BodyFinal = '';
            for(string s: Body){
                if(!FinalContent.contains(s)){
                    FinalContent.add(s);
                    BodyFinal += s + '\r\n';
                }
            }
            return BodyFinal;
        }
        //MAM 10/24/2013 end
        
        //MAM 01/09/2014 new method for State conversion to abbreviation for HEIMS VDU
        public static String convertStatetoAbbreviation(String StateName){
            if(StateName.contains('New South Wales'))
                return 'NSW';
            else if(StateName.contains('Victoria'))
                return 'VIC';
            else if(StateName.contains('Queensland'))
                return 'QLD';
            else if(StateName.contains('Western Australia'))
                return 'WA';
            else if(StateName.contains('South Australia'))
                return 'SA';
            else if(StateName.contains('Tasmania'))
                return 'TAS';
            else if(StateName.contains('Northern Territory'))
                return 'NA';
            else if(StateName.contains('Australian Capital Territory'))
                return 'ACT';
            else if(StateName.contains('Australian Antarctic Territories'))
                return 'AAT';
            else
                return StateName;
        }
        //MAM 01/09/2014 end
        
        //MAM 01/20/2014 new method for address parsing for HEIMS VDU
        public static String parseAddress(String AddrBLDGPptyName, String AddrFlatUnitDetails, String AddrStreetNumber, String AddrStreetName){
            //Target Format:  Address_flat_unit_details__c Address_building_property_name__c Address_street_number__c Address_street_name__c
            //Example: 30Floor Maples Building 2023 Carmen Street
            String Address = '';
            
            if(!String.IsEmpty(AddrFlatUnitDetails)){
                Address+= AddrFlatUnitDetails + ' ';
            }
            
            if (!String.IsEmpty(AddrBLDGPptyName)){
                Address+= AddrBLDGPptyName + ' ';
            }
            
            if(!String.IsEmpty(AddrStreetNumber)){
                Address+= AddrStreetNumber + ' ';
            }
            
            if(!String.IsEmpty(AddrStreetName)){
                Address+= AddrStreetName;
            }
            return Address.Trim();
        }
        //MAM 01/20/2014 end
               /*
     *  Author: Paolo Mendoza
     *  Description: For VEN Aboriginal and Torres Strait Islander code
     *  History: 05-NOV-2014 - PM - Created for portability
     */
     
     public static string ATSIGenerator(String ATSI){
            if(ATSI == '1')
                return '3';
            else if(ATSI == '2')
                return '4';
            else if(ATSI == '3')
                return '5';
            else if(ATSI == '4')
                return '2';
            else if(ATSI == '@')
                return '9';
            else
                return ATSI;
     }
     
     //Paolo 05-NOV-2014 End
        
     
}