global class NATGeneratorBatchWARAPTStudent implements Database.Batchable<sObject>,Database.Stateful {

    global String query = '';
    global String body = '';
    private String LogId;
    public String libraryId;
    global NAT_Validation_Log__c log;
    
    
    
    public NATGeneratorBatchWARAPTStudent(String LId) {
    
      logId = LId;
    
      log = [ SELECT ID, RAPT_StudentQuery__c, Validation_State__c, Training_Organisation_Id__c, query_students__c, query_students_2__c, query_students_3__c, query_students_4__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
     
      query = log.RAPT_StudentQuery__c;
    
    }

    
     global database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    
     global void execute(Database.BatchableContext BC,List<SObject> scope) {
     
       Set<String> Identifiers = new Set<String>{'1201','9700','9701','9702','9799','@@@@'};
     
       for(SObject s :scope) {
           
           Boolean isLanguageSet = false;
       
           Account a = (Account) s;
     
           body += a.Student_Identifer__c + '\t';
                     
           body += a.LastName + '\t';
                     
           body += a.FirstName + '\t';
           
           if(a.PersonBirthDate == null)
               body += '' + '\t';
           else       
               body += formatDate(a.PersonBirthDate) + '\t';
           
           if(a.Sex_Identifier__c == null)
              body += '' + '\t';
           else         
              body += a.Sex_Identifier__c + '\t';
           
           if(a.PersonEmail == null)
              body += '' + '\t';
           else        
              body += a.PersonEmail + '\t';
                     
           body += a.Address_street_number__c + ' ' + a.Address_street_name__c + '\t';
                     
           body += a.Suburb_locality_or_town__c + '\t';
                     
           body += a.PersonMailingState + '\t';
                     
           body += a.Address_Post_Code__c + '\t';
                     
                     //Check if postal address populated
           if(a.Postal_delivery_box__c != null)
                 body += postalAddressInsert(a);
           else {
                         
                 body += a.Address_street_number__c + ' ' + a.Address_street_name__c + '\t';
                     
                 body += a.Suburb_locality_or_town__c + '\t';
                         
                 body += a.PersonMailingState + '\t';
                         
                 body += a.Address_Post_Code__c + '\t';
                         
            }            
                     
            body += NATGeneratorUtility.formatPhone(a.PersonHomePhone) + '\t';
                     
            body +=  NATGeneratorUtility.formatPhone(a.Work_Phone__c) + '\t';
                     
            body +=  NATGeneratorUtility.formatPhone(a.PersonMobilePhone) + '\t';
            
            if(a.Country_of_Birth_Identifer__c == null)
                body += '' + '\t';
            else       
                body += a.Country_of_Birth_Identifer__c + '\t';
            
            body += NATGeneratorUtility.convertLanguageSpokenCodeWA(a.Main_Language_Spoken_at_Home_Identifier__c) + '\t';
                
            if(a.Main_Language_Spoken_at_Home_Identifier__c == null)
                body += '' + '\t';
            else
                body += a.Main_Language_Spoken_at_Home_Identifier__c + '\t';
            
            if(a.Main_Language_Spoken_at_Home_Identifier__c != null) {  
              if(Identifiers.contains(a.Main_Language_Spoken_at_Home_Identifier__c)) {
                 body += '9' + '\t';
                 isLanguageSet = true;
                 }
            }    
            
            if(isLanguageSet == false) { 
              if(a.Proficiency_in_Spoken_English_Identifier__c == null)
                body += '@' + '\t';
              else
                body += a.Proficiency_in_Spoken_English_Identifier__c + '\t';
                        
            }         
            if(a.Indigenous_status_identifier__c == null)
                body += '' + '\t';
                
            else
                body += a.Indigenous_status_identifier__c + '\t';    
            
                     
            if(a.Disability_Flag_0__c == 'Y'){
                  if(a.Disability_Type__c.contains(';'))
                       body += '99' + '\t';
                        else
                       body += a.Disability_Type__c.Substring(0,2) + '\t';
                     } else if(a.Disability_Flag_0__c == 'N')
                       body += '0' + '\t';
                   else
                       body += '@' + '\t';
            
            if(a.Highest_School_Level_Comp_Identifier__c == null)
                 body += '' + '\t';
            else       
                 body += a.Highest_School_Level_Comp_Identifier__c + '\t';
            
            if(a.Year_Highest_Education_Completed__c == null)
                 body += '' + '\t'; 
            else
                body += a.Year_Highest_Education_Completed__c + '\t';
            
            if(a.At_School_Flag__c == null)
               body += '' + '\t';
            else      
               body += a.At_School_Flag__c + '\t';
                     
            if(a.Prior_Achievement_Flag__c == 'Y') {
                 if(a.Prior_Achievement_Type_s__c.contains(';'))
                     body += '999' + '\t';
                 else
                     body += a.Prior_Achievement_Type_s__c.Substring(0,3);
            } else if(a.Prior_Achievement_Flag__c == 'N')
                     body += '0' + '\t';
            else
                     body += '@' + '\t';
                     
                    // body += s.Prior_Educational_Achievement_Identifier__c + '\t';
            
            if(a.Labour_Force_Status_Identifier__c == null)
               body += '' + '\t';
            else      
               body += a.Labour_Force_Status_Identifier__c + '\t';
             
            if(a.study_Reason_Identifier__c == null || a.study_Reason_Identifier__c == '')
                body += '' + '\t';
            else {
                
                try {
                  body += WA_RAPT__c.getInstance(a.Study_Reason_Identifier__c).Study_Reason_Identifier_WA_RAPT__c + '\t';
                } catch(Exception e) {
                  body += a.study_Reason_Identifier__c + '\t';
                }
                
            }   
                
                     
            body += '\r\n';       
                     
                    
            
            
           
        }
     
     
     }

     global void finish(Database.BatchableContext BC) {
         
          NAT_Validation_Log__c natLog = [ SELECT Id, RAPT_StudentQuery__c,RAPT_EnrolmentQuery__c,Content_Library_Id__c, Delete_Existing_NAT_files__c, Validation_State__c, Query_Main__c, Query_Students__c, Query_Students_2__c, Query_Students_3__c, Query_Students_4__c, Training_Organisation_Id__c FROM NAT_Validation_Log__c WHERE ID = :logID ];
          
            if(body == '')
            body = ' ';
         
         Blob pBlob = Blob.valueof(body);
                    
         insert new ContentVersion(
         versionData =  pBlob,
         Title = 'student_' + System.today(),
         PathOnClient = '/student_' + System.today() + '.txt',
         FirstPublishLocationId = natLog.Content_Library_ID__c);  
         
         Database.executeBatch(new NATGeneratorBatchWARAPTEnrolment( natlog.RAPT_EnrolmentQuery__c, LogId  ) );
       
     
     }
     
      private String formatDate(Date bday) {
        
        Datetime yourDate = bday;
        String dateOutput = yourDate.format('dd/MM/yyyy');
        
        return dateOutput;
    
    }
    
     private String postalAddressInsert(Account s) {
        
        String body = '';
        
        body += s.Postal_delivery_box__c + '\t';
                     
        body += s.Postal_suburb_locality_or_town__c + '\t';
                     
        body += s.PersonOtherPostalCode + '\t';
                     
        body += s.Reporting_Other_State__r.Short_Name__c + '\t';
    
        return body;
           
    
    
    }

}