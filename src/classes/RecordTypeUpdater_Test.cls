/**
 * @description Test Class for class RecordTypeUpdater 
 * @author Jerome Almanza
 * @date 12.OCT.2015
 *
 * HISTORY
 * - 12.OCT.2015    Jerome Almanza  - Created.
 */

 @isTest
private class RecordTypeUpdater_Test {
	
	@isTest static void test_method_one() {
		RecordType rc = [Select Id from RecordType where Name = 'Student' And SObjectType = 'Account'];
		Account insAcc = new Account(LastName = 'Test Account 1', FirstName = 'Test Account 2');
		insert insAcc;

		
		Account acc = [Select RecordTypeId from Account where Id =: insAcc.Id];
        acc.RecordTypeId = rc.Id;
        update acc;

        Test.startTest();
        RecordTypeUpdater.convertToStudentAccount(acc.Id);
        Test.stopTest();
	}		
}