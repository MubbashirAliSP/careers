/**
 * @description Custom inbound email handler for creating Online Service Case records from emails coming from the online team as part of the Online Service Case Email Integration mini project.
 * @author Ranyel Maliwanag
 * @date 15.FEB.2016
 */
global with sharing class OnlineInboundEmailHandler implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		// Identify the active email handler to avoid multiple service cases being created if sent to multiple inboxes
		String activeAddress = envelope.toAddress.substringBefore('@');

		// Variable initialisations
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

		// Build a mapping from metadata contents 
		List<OnlineEmailService__mdt> onlineEmailServiceMetas = [SELECT EmailName__c, QueueId__c, Type__c, NextPlannedAction__c, NextPlannedActivityOffset__c FROM OnlineEmailService__mdt];
		Map<String, OnlineEmailService__mdt> queueEmailMap = new Map<String, OnlineEmailService__mdt>();

		for (OnlineEmailService__mdt meta : onlineEmailServiceMetas) {
			queueEmailMap.put(meta.EmailName__c, meta);
			System.debug('meta ::: ' + meta.EmailName__c);
		}

		// Parse email to check which email address it was sent to
		List<String> filteredEmails = new List<String>();
		for (String address : email.toAddresses) {
			System.debug('::: ' + address);
			String emailName = address.substringBefore('@');
			if (queueEmailMap.containsKey(emailName)) {
				filteredEmails.add(emailName);
			}
		}

		System.debug('Active Email ::: ' + activeAddress);

		// Parse if student email exists
		String studentEmailLine = email.plainTextBody.substringBefore(']]').substringAfter('[[');
		String studentEmail;
		Account student;
		if (studentEmailLine.startsWith('CASIS Student Email:')) {
			studentEmail = studentEmailLine.split(':')[1].trim();
			studentEmail = studentEmail.substringBefore('<');
		}

		if (String.isNotBlank(studentEmail)) {
			try {
				student = [SELECT Id FROM Account WHERE PersonEmail = :studentEmail LIMIT 1];
			} catch (Exception e) {
				System.debug('Student email does not exist');
			}
		}

		// Create service case
		Map<Task, Service_Cases__c> serviceCaseTaskMap = new Map<Task, Service_Cases__c>();
		for (String emailName : filteredEmails) {
			System.debug('filteredEmails ::: ' + emailName);
			if (emailName == activeAddress) {
				System.debug('::: true!');
				// Create Service Case
				Service_Cases__c serviceCase = new Service_Cases__c(
						OwnerId = queueEmailMap.get(emailName).QueueId__c,
						Type__c = queueEmailMap.get(emailName).Type__c,
						Subject__c = email.subject,
						Description__c = email.plainTextBody,
						Case_Origin__c = 'Email',
						Next_Planned_Action__c = queueEmailMap.get(emailName).NextPlannedAction__c,
						Next_Planned_Activity__c = Datetime.now().addDays(Integer.valueOf(queueEmailMap.get(emailName).NextPlannedActivityOffset__c)),
						RecordTypeId = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Online Service Case').getRecordTypeId()
					);
				if (student != null) {
					serviceCase.Account_Student_Name__c = student.Id;
				}

				// Create Task 
				Task emailTask = new Task(
						RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
						Subject = 'Email Received',
						ActivityDate = Date.today(),
						Status = 'Completed',
						OwnerId = UserInfo.getUserId(),
						Description = email.plainTextBody
					);
				serviceCaseTaskMap.put(emailTask, serviceCase);
			}
		}

		// Insert service case
		insert serviceCaseTaskMap.values();

		System.debug('sc ::: ' + serviceCaseTaskMap.values());

		// Populate Service Case in tasks
		List<Task> tasksForInsert = new List<Task>();
		for (Task record : serviceCaseTaskMap.keySet()) {
			record.WhatId = serviceCaseTaskMap.get(record).Id;
			tasksForInsert.add(record);
		}
		insert tasksForInsert;

		// Create attachments, if any
		List<Attachment> taskAttachments = new List<Attachment>();
		for (Task record : tasksForInsert) {
			Attachment emailAtt = new Attachment();
			emailAtt.Name = 'Email.html';
			emailAtt.Body = Blob.valueOf(email.htmlBody);
			emailAtt.ParentId = record.Id;
			taskAttachments.add(emailAtt);

			if (email.binaryAttachments != null) {
				for (Messaging.InboundEmail.BinaryAttachment emailFile : email.binaryAttachments) {
					Attachment fileAttachment = new Attachment();
					fileAttachment.Name = emailFile.fileName;
					fileAttachment.Body = emailFile.body;
					fileAttachment.ParentId = record.Id;
					taskAttachments.add(fileAttachment);
				}
			}
		}

		// Insert attachments, if any
		insert taskAttachments;
		return result;
	}
}