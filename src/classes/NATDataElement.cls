public class NATDataElement {
  
    public String fieldName {get; set;}
    public Integer fieldLength {get; set;}
    public Boolean customLogic {get; set;}
    public String fieldType {get; set;}
    
    public NATDataElement(String fn, Integer fl, Boolean cl, String ft) {
        this.fieldName = fn;
        this.fieldLength = fl;
        this.customLogic = cl;
        this.fieldType = ft;
        
    }
 
}