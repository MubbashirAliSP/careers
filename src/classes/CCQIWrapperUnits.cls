public with sharing class CCQIWrapperUnits {
	
	public Enrolment_Unit__c EU {get;set;}
    	public Integer EC {get;set;}
    	public Integer CC {get;set;}
    	
    	public CCQIWrapperUnits(Enrolment_Unit__c e, Integer e_count, Integer c_count) {
    		
    		EU = e;
    		EC = e_count;
    		CC = c_count;
    		
    	}
    	
    

}