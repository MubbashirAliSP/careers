/**
 * @description Test class for `EntryRequirementResitCX`
 * @author Ranyel Maliwanag
 * @date 22.FEB.2016
 * @history
 *		 03.MAR.2016	Ranyel Maliwanag	- Added Application Form information in the test setup as part of a newly introduced validation rule to make it required
 */
@isTest
public with sharing class EntryRequirementResitCX_Test {
	static Id llnERRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	static Id formSCRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();


	@testSetup
	static void setup() {
		ApplicationsTestUtilities.initialiseCountryAndStates();

		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        studentLogin.AccountId__c = student.Id;
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		insert enrolment;

		Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
		serviceCase.Enrolment__c = enrolment.Id;
		serviceCase.Account_Student_Name__c = student.Id;
		serviceCase.RecordTypeId = formSCRTID;
		serviceCase.ParentQualificationName__c = 'test qualification';
		insert serviceCase;

		ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;

		EntryRequirement__c requirement = new EntryRequirement__c(
				ApplicationFormId__c = appForm.Id,
				EnrolmentId__c = enrolment.Id,
				ServiceCaseId__c = serviceCase.Id,
				RecordTypeId = llnERRTID,
				Status__c = 'LLN - Pending'
			);
		insert requirement;

	}

	static testMethod void testRedirect() {
		String queryStringER = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		EntryRequirement__c record = Database.query(queryStringER);
		System.assert(!record.LLN_Re_Sit_Offer_Accepted__c);
		Test.setCurrentPage(Page.EntryRequirementResit);

		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(record);
            EntryRequirementResitCX controller = new EntryRequirementResitCX(stdController);

            PageReference result = controller.evaluateRedirects();
		Test.stopTest();

		List<EntryRequirement__c> requirements = Database.query(queryStringER);
		System.assert(requirements.size() > 1);
	}

	static testMethod void testAcceptedScores() {
		String queryStringER = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		EntryRequirement__c record = Database.query(queryStringER);
		record.Literacy_Level__c = 'English ACSF L2';
		record.Numeracy_Level__c = 'Maths ACSF L3';
		update record;

		Test.setCurrentPage(Page.EntryRequirementResit);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(record);
            EntryRequirementResitCX controller = new EntryRequirementResitCX(stdController);

            PageReference result = controller.evaluateRedirects();
		Test.stopTest();

		queryStringER += 'WHERE Id != \'' + record.Id + '\'';
		EntryRequirement__c resitRequirement = Database.query(queryStringER);
		System.assert(String.isBlank(resitRequirement.Literacy_Level__c));
		System.assert(String.isNotBlank(resitRequirement.Numeracy_Level__c));
	}

	static testMethod void testAcceptedScores1() {
		String queryStringER = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		EntryRequirement__c record = Database.query(queryStringER);
		record.Literacy_Level__c = 'English ACSF L3';
		record.Numeracy_Level__c = 'Maths ACSF L2';
		update record;

		Test.setCurrentPage(Page.EntryRequirementResit);
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(record);
            EntryRequirementResitCX controller = new EntryRequirementResitCX(stdController);

            PageReference result = controller.evaluateRedirects();
		Test.stopTest();

		queryStringER += 'WHERE Id != \'' + record.Id + '\'';
		EntryRequirement__c resitRequirement = Database.query(queryStringER);
		System.assert(String.isBlank(resitRequirement.Numeracy_Level__c));
		System.assert(String.isNotBlank(resitRequirement.Literacy_Level__c));
	}
}