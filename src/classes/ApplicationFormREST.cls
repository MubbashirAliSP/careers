/**
 * @description REST service for scoring Application Forms
 * @author Andrew Burgess
 * @date 08.DEC.2015
 */
@RestResource(urlMapping='/ApplicationFormScoring/*')
global without sharing class ApplicationFormREST {

	/**
	 * @description POST method handler for scoring AFs
	 * @author Andrew Burgess
	 * @date 08.DEC.2015
	 */
	@HttpPost
	global static String doPost(String formId, String score1, String score2, String score3, String score4,String startDate, String endDate) {         
		ApplicationForm__c form = [Select Id, LLN_Score_1__c, LLN_Score_2__c,LLN_Score_3__c,LLN_Score_4__c,LLN_Start_Date__c,LLN_End_Date__c From ApplicationForm__c Where id = : formId ];
		
		if(String.isBlank(score1)){
			score1 ='0';
		}
		
		if(String.isBlank(score2)){
			score2 ='0';
		}
		
		if(String.isBlank(score3)){
			score3 ='0';
		}
		
		if(String.isBlank(score4)){
			score4 ='0';
		}
		
		
		
		form.LLN_Score_1__c = score1;
		form.LLN_Score_2__c = Integer.valueOf(score2);
		form.LLN_Score_3__c = score3;
		form.LLN_Score_4__c = Integer.valueOf(score4);
		
		if(!String.isBlank(startDate)){
		form.LLN_Start_Date__c = date.valueof(startDate);
		}
		if(!String.isBlank(endDate)){
		form.LLN_End_Date__c = date.valueof(endDate);
		}
		try {
		 update form;
		 return 'Success';
		} catch(Exception e) {return e.getMessage();}
		
		
		//String result = 'A post request for Form ' + formId + ' has been received with the following scores: ' + score1 + ', ' + score2 + ', ' + score3 + ', ' + score4 + ', ' + score5 + '.';
		return 'Success';
	}
}