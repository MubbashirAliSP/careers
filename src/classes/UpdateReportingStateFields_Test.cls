/**
 * @description Test class for UpdateReportingStateFields trigger
 * @author Janella Lauren Canlas
 * @date 29.OCT.2012
 */

@isTest(SeeAllData=true)
private class UpdateReportingStateFields_Test {

    static testMethod void UpdateReportingStateFields_Test() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        String training = [select id from RecordType where Name='Training Organisation'  And SObjectType = 'Account'].Id; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        
        system.runAs(u){
            
            //insert student acct with Australian state
            Account sacct = new Account();
            sacct.RecordTypeId = stud;
            sacct.LastName = 'Test student Account';
            sacct.PersonMailingState = 'QLD';
            sacct.PersonOtherState = 'QLD';
            sacct.PersonOtherCountry = 'AUSTRALIA';
            sacct.PersonMailingCountry = 'AUSTRALIA';
            sacct.PersonMailingPostalCode = '4345';
            sacct.PersonOtherPostalCode = '4345';
            sacct.FirstName = 'You';
            sacct.Student_Identifer__c = 'H09098';
            sacct.Year_Highest_Education_Completed__c = '2001';
            sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
            sacct.PersonMobilePhone = '61847292112';
            sacct.Salutation = 'Mr.';
            sacct.Learner_Unique_Identifier__c = '12ab34cd56';
            insert sacct;
            
            Account acc = [select Reporting_Mailing_State__r.Name from Account where id =: sacct.Id];
            //System.assertEquals('Queensland', acc.Reporting_Mailing_State__r.Name);
            
            //update student acct with different Country
            sacct.PersonMailingState = 'CA';
            sacct.PersonOtherState = 'CA';
            sacct.PersonOtherCountry = 'UNITED STATES OF AMERICA';
            sacct.PersonMailingCountry = 'UNITED STATES OF AMERICA';
             //sacct.Student_Identifer__c = 'H0954594';
            sacct.Year_Highest_Education_Completed__c = '2001';
            update sacct;
            Account acc3 = [select Reporting_Mailing_State__r.Name from Account where id =: sacct.Id];
            //System.assertEquals('Other (Overseas but not an Australian territory or dependency)', acc3.Reporting_Mailing_State__r.Name);
            
            //insert training organization acct with Australian State
            Account tacct = new Account();
            tacct.RecordTypeId = training;
            tacct.Name = 'Test Training Account';
            tacct.BillingCountry ='AUSTRALIA';
            tacct.BillingState = 'QLD';
            tacct.BillingPostalCode = '4345';
            tacct.Training_Org_Identifier__c = 'test';
            tacct.Training_Org_Type__c = 'test';
            tacct.Proficiency_in_Spoken_English__c = '1 - Very well';
            //sacct.Student_Identifer__c = 'H09088';
            insert tacct; 
            Account tacc = [select Reporting_Billing_State__r.Name from Account where Name = 'Test Training Account'];
            //System.assertEquals('Queensland', tacc.Reporting_Billing_State__r.Name);
            
            //insert training organization acct with Different Country
            Account tacct2 = new Account();
            tacct2.RecordTypeId = training;
            tacct2.Name = 'Test Training Account2';
            tacct2.BillingCountry ='UNITED STATES OF AMERICA';
            tacct2.BillingState = 'CA';
            tacct2.Training_Org_Identifier__c = 'test';
            tacct2.Training_Org_Type__c = 'test';
            tacct2.Proficiency_in_Spoken_English__c = '1 - Very well';
            insert tacct2; 
            Account tacc2 = [select Reporting_Billing_State__r.Name from Account where Name = 'Test Training Account2'];
            //System.assertEquals('Other (Overseas but not an Australian territory or dependency)', tacc2.Reporting_Billing_State__r.Name);
            
            //insert training organization acct with Other Australian territories/dependencies
            Account tacct3 = new Account();
            tacct3.RecordTypeId = training;
            tacct3.Name = 'Test Training Account3';
            tacct3.BillingCountry ='AUSTRALIA';
            tacct3.BillingState = 'QLD';
            tacct3.BillingPostalCode = '4345';
            tacct3.Training_Org_Identifier__c = 'test';
            tacct3.Training_Org_Type__c = 'test';
            tacct3.Proficiency_in_Spoken_English__c = '1 - Very well';
            insert tacct3; 
            
            Account tacc3 = [select Reporting_Billing_State__r.Name from Account where Name = 'Test Training Account3'];
            //System.assertEquals('Other (Overseas but not an Australian territory or dependency)', tacc2.Reporting_Billing_State__r.Name);
            
            
            //insert student acct with Other Australian territories/dependencies
            Account sacct2 = new Account();
            sacct2.RecordTypeId = stud;
            sacct2.LastName = 'Test student Account2';
            //sacct2.PersonMailingState = 'WWW';
            //sacct2.PersonOtherState = 'WWW';
            sacct2.PersonOtherCountry = 'AUSTRALIA';
            sacct2.PersonMailingCountry = 'AUSTRALIA';
            sacct2.PersonMailingPostalCode = '4345';
            sacct2.PersonOtherPostalCode = '4345';
            sacct2.FirstName = 'You';
            sacct2.Student_Identifer__c = 'H09068';
            sacct2.Year_Highest_Education_Completed__c = '2001';
            sacct2.Proficiency_in_Spoken_English__c = '1 - Very well';
            sacct2.PersonMobilePhone = '61847292113';
            sacct2.Salutation = 'Mr.';
            sacct2.Learner_Unique_Identifier__c = '12ab34cd56';
            insert sacct2;
            
            Account acc2 = [select Reporting_Mailing_State__r.Name from Account where LastName = 'Test student Account2'];
            //System.assertEquals('Other Australian territories or dependencies', acc2.Reporting_Mailing_State__r.Name);
            
        }
    }
     static testMethod void CopyMailingAddress_Test() {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            String stud = [select id from RecordType where Name='Student'  And SObjectType = 'Account'].Id; 
            String training = [select id from RecordType where Name='Training Organisation'  And SObjectType = 'Account'].Id; 
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
            
            system.runAs(u){
                test.startTest();
                //insert student acct with Australian state
                Account sacct = new Account();
                sacct.RecordTypeId = stud;
                sacct.LastName = 'Test student Account';
                sacct.PersonMailingStreet = '1111';
                sacct.PersonMailingCity = 'Sydney';
                sacct.PersonMailingState = 'QLD';
                sacct.PersonMailingPostalCode = '4345';
                sacct.PersonMailingCountry = 'AUSTRALIA';
                sacct.FirstName = 'You';
                sacct.Student_Identifer__c = 'H09095';
                sacct.Year_Highest_Education_Completed__c = '2001';
                sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
                sacct.PersonMobilePhone = '61847292112';
                sacct.Salutation = 'Mr.';
                sacct.Learner_Unique_Identifier__c = '12ab34cd56';
                //sacct.Postal_is_the_same_as_Residential__c = true;
                insert sacct;
                
                //uncheck postal is the same as residential update account
                sacct.Postal_is_the_same_as_Residential__c = false;
                sacct.PersonOtherState = 'CA';
                sacct.PersonOtherCountry = 'UNITED STATES OF AMERICA';
                update sacct;
                test.stopTest();
            }
        }
}