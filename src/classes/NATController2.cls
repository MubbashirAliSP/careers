/**
 * @description N/A
 * @author N/A
 * @date N/A
 * @history
 *       28.OCT.2015    Ranyel Maliwanag    Excluded WebsiteOnly accounts when querying 
 *       08.JAN.2016    Biao Zhang          Create years option list based on current year
 */
public with sharing class NATController2 {

    public boolean deleteExistingFiles {get;set;}
    
    public boolean requiredForAVETMISS {get;set;}
    
    public boolean nswClaim {get;set;}
    
    public boolean displayPopup {get; set;}
    
    public String state {get;set;}
    
    public String reportingType {get;set;}
    
    public String reportingTypeNSW {get;set;}

    public String reportingTypeWA {get;set;}
    
    public String year {get;set;}
    
    public Set<String>ContractsCodes = new Set<String>();
    
    public String rto {get;set;}
    
    public List<SelectOption> states {get;set;}
    
    public List<SelectOption> types {get;set;}
    
    public List<SelectOption> nswTypes {get;set;}
    
    public List<SelectOption> waTypes {get;set;}
    
    public List<SelectOption> years {get;set;}
    
    public List<SelectOption> rtos {get;set;}
    
    public Map<Id,Decimal> Unit_NominalHoursMap = new Map<Id, Decimal>();
        
    public String pur_codes {get;set;} // used for pur. contracts query string
    
    public Set<String>tempPur_codes {get;set;} // used to reduce load on cpu time at generate screen
    
    /*Filter Fields */
    
    public List<SelectOption>subTypes {get;set;}
 
    public Boolean SetClaimDate {get;set;}
    
    public transient Date ClaimDate {get;set;}
        
    public transient String fundingSourceSearch {get;set;}
    
    public transient String ContractCodeSearch {get;set;}
    public transient String IncludeActive {Get; set; }
    public static final List<String> exlStates =  new List<String>  {'Northern Territory', 'Tasmania', 'Australian Capital Territory'};
    
    public Set<ID> location;
    public Set<ID> parentUoC;
    public Set<ID> qualification;
    public Set<Id>student;
    
    public Account TrainingOrgToValidate {get;set;}
    
    public transient List<Locations__c> TrainingOrgLocationsToValidate = new List<Locations__c>();
       
    public List<PurchasingContractView> FilteredContracts {get;set;}
   
    transient public List<Enrolment_Unit__c>FilteredEU = new List<Enrolment_Unit__c>();
       
    //public String selectedFile {get;set;}
    
    public Boolean isValidated {get;set;} // need to check, if validation ran before, if not do not clear lists
    public Boolean exportDisallowed {get;set;}
    
    public NATController2() {
    
        deleteExistingFiles = true;
        nswClaim = false;
      
        isValidated = false;
        exportDisallowed = true;
        displayPopup = false;
        setClaimDate = false;
        
        state = '--Select State--';
        
        /*Grab RTOs */
        
        rtos = new List<SelectOption>();
        
        rtos.add(new SelectOption('--None--', '--None--'));
        //Add RTOs to Select list and display on the page
        for(Account a : [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Training Organisation' AND WebsiteOnlyAccount__c = false]) {
            rtos.add(new SelectOption(a.id,a.name));
        }
        
                 
        types = new List<SelectOption>();
        types.add(new SelectOption('--Select Type--', '--Select Type--'));
        types.add(new SelectOption('State', 'State'));
        types.add(new SelectOption('National', 'National'));
        
        nswTypes = new List<SelectOption>();
        nswTypes.add(new SelectOption('--Select Type--', '--Select Type--'));
        nswTypes.add(new SelectOption('Smart and Skilled', 'Smart & Skilled'));
        nswTypes.add(new SelectOption('APL', 'APL'));
        
        waTypes = new List<SelectOption>();
        waTypes.add(new SelectOption('--Select Type--', '--Select Type--'));
        waTypes.add(new SelectOption('RAPT', 'RAPT'));
        waTypes.add(new SelectOption('AVETMISS', 'AVETMISS'));
          
        /*
        *   08.Jan.2016 Biao Zhang       Add years based on current year
        */
        years = new List<SelectOption>();
        Integer yearInt = 2010;
        Integer currentYear = System.Today().year();

        while(yearInt <= currentYear ) {
            years.add(new SelectOption(String.ValueOf(yearInt), String.ValueOf(yearInt)));
            yearInt++;
        }
        /*END*/
          
        if(year == null || year == '')  
            year = String.ValueOf(date.today().year());
            
        filterStates();
    }
    
    /*After RTO is selected, this method is called to retrieve relevant states */
    public pageReference filterStates() {
        
        Set<String> StateName = new Set<String>();
        
        states = new List<SelectOption>();
        
        if(rto == null)
            states.add(new SelectOption('--None','--None--'));
            
        else {
   
               
            for(Locations__c l : [Select Id, Child_State__c FROM Locations__c WHERE Training_Organisation__c = : rto /*AND Child_State__c not in :exlStates */]) {
                if(l.Child_State__c != null) {
                    StateName.add(l.Child_State__c);
                }
            }
                
             //Generate Picklist for the state
            states.add(new SelectOption('--Select State--', '--Select State--'));
            for(String s : StateName) {
                states.add(new SelectOption(s, s));
            }
            
        }
            
        return null;
     
    }
    
    public pageReference filterPurchasingContract() {
        List<String> fundingSourceSearchParams = fundingSourceSearch.split(' AND ');
        List<String> ContractCodeSearchParams = ContractCodeSearch.split(' AND ');
        
        String query;
        
        if(reportingType == 'State') {
              query = 'SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c ' +
            ' FROM Purchasing_Contracts__c ' +
            ' WHERE Training_Organisation__c = : rto AND Contract_State__c = : state';
        }
        else {
            query = 'SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c ' +
            ' FROM Purchasing_Contracts__c ' +
            ' WHERE Training_Organisation__c = : rto';
        }
       
        if(fundingSourceSearch !='' ) {
            query += ' AND State_Fund_Source__r.Code__c in :fundingSourceSearchParams ';
        }
        if(ContractCodeSearch != '') {
            query += ' AND (';
            boolean first = true;
            for ( String c : ContractCodeSearchParams ){
                if ( first )
                    first = false;
                else
                    query += ' OR ';
                query += 'Contract_Code__c LIKE \'%' + String.escapeSingleQuotes(c) + '%\''; 
            }
            query += ')';
        }
        if ( IncludeActive == 'active')
            query += ' AND Active__c = true';
        else if ( IncludeActive == 'inactive')
            query += ' AND Active__c = false';
        
        if ( requiredForAVETMISS == true )
            query += ' AND Required_For_AVETMISS_Reporting__c = true';
        
        
        //remember what was selected...
        Map<Id, PurchasingContractView> selected = new Map<Id, PurchasingContractView>();
        for ( PurchasingContractView pcv : FilteredContracts){
            if ( pcv.selected ){
                selected.put(pcv.pc.id, pcv);
            }
        }

        //No Filtering
        FilteredContracts = new List<PurchasingContractView>();
        for ( Purchasing_Contracts__c pc : Database.query(query) ){
            system.debug('qu: ' + query);
            FilteredContracts.add(new PurchasingContractView(pc, selected.containsKey(pc.id)));
            selected.remove(pc.id);
        }

        for ( PurchasingContractView pcv : selected.values()){
            FilteredContracts.add(pcv);
        }
        
        return null;
    }
    
    public pageReference continueButton() {
               
        if(state == '--None' || rto == '--None--') {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'RTO and State need to be selected');
            
            return null;
        }
        
        populateAvailableLists();
            
        return new PageReference('/apex/Natgenerator2'); 
            
    }
    
    public pageReference SetClaimButton() {
        
        system.debug(state);
        system.debug(rto);
        system.debug(deleteExistingFiles);
        system.debug(reportingTypeNSW);
        return new PageReference('/apex/NSWClaimingPurchasingContractFilter2?state=' + state + '&rto=' + rto + '&claim=' + nswClaim + '&de=' + deleteExistingFiles + '&rt=' + reportingTypeNSW);

    }
    public PageReference backButton(){
        if(ApexPages.currentPage().getParameters().get('state') == 'New South Wales') {
            return new PageReference('/apex/NSWClaiming2?state=' +ApexPages.currentPage().getParameters().get('state') + '&rto=' + ApexPages.currentPage().getParameters().get('rto')+ '&claim=' + ApexPages.currentPage().getParameters().get('claim'));
        }
        else if(ApexPages.currentPage().getParameters().get('state') == 'Western Australia') {
            return new PageReference('/apex/WAClaiming2?state=' +ApexPages.currentPage().getParameters().get('state') + '&rto=' + ApexPages.currentPage().getParameters().get('rto')+ '&claim=' + ApexPages.currentPage().getParameters().get('claim'));
        }
        else {
            return new PageReference('/apex/Natgenerator2'); 
        }
         
    }
        
    public pageReference cancelButton() {
        
        return new PageReference('/apex/NATGeneratorStateSelection2');
        
    }
    
    public pageReference cancelButtonHome() {
        
        return new PageReference('/home/home.jsp');
        
    }
    
    public pageReference validator() {
        
       TrainingOrgToValidate = [SELECT Id, Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account WHERE id = : rto];
        
       Integer pcvCount = 0;
       
       Boolean isWARAPT = false;
        
       List<String> selectedContractCodes = new List<String>();
       List<String> selectedContractIds = new List<String>();
        
        for ( PurchasingContractView pcv : FilteredContracts){
            if ( pcv.selected ){
                pcvCount++;
                selectedContractCodes.add(pcv.pc.Contract_Code__c);
                selectedContractIds.add(pcv.pc.Id);
            }
        }

        if(pcvCount == 0) {
            
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You must select at least one contract');

           return null;
        }
        
        if(state == '--Select State--') {
            state = 'National';
        }
        if(reportingTypeNSW == 'APL') {
            state = 'New South Wales APL';
        }
        
        if(reportingTypeWA == 'RAPT')
           isWARAPT = true;
        
        Database.executeBatch(new NATValidationBatch_Initial( TrainingOrgToValidate.Id, state, year, selectedContractCodes, selectedContractIds, deleteExistingFiles,isWARAPT) );

        return new PageReference('/apex/NATValidatorRunning');
        
    }
    
    public void populateAvailableLists() {
             
        subTypes = new List<SelectOption>();    
                
        subTypes.add(new SelectOption('Partial Submission', 'Partial Submission'));
        subTypes.add(new SelectOption('End Of Year Submission', 'End Of Year Submission'));
            
        FilteredContracts = new List<PurchasingContractView>();
        
        if(reportingType == 'State') {        
            for(Purchasing_Contracts__c c : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto AND Contract_State__c = : state]) { 
                FilteredContracts.add(new PurchasingContractView(c, false));
            }
        }  
        
        if(reportingType == 'National') { 
            state = 'National';
            for(Purchasing_Contracts__c c : [SELECT id, name, contract_code__c,State_Fund_Source__r.Code__c,Commencement_Date__c,Completion_Date__c,Contract_Type__c,Active__c,Contract_Name__c FROM Purchasing_Contracts__c WHERE  Training_Organisation__c = : rto ]) { 
                FilteredContracts.add(new PurchasingContractView(c, false));
            }
        }    
    }
    
    public void buildPurchasingContractString() { //Creates purcontract string to be used for a query
         
        pur_codes = '';
        tempPur_codes = new Set<String>();
         
        for(PurchasingContractView p : FilteredContracts ) {
            if(p.selected) {
                pur_codes += '\'' + p.pc.Contract_Code__c + '\',';
                tempPur_codes.add(p.pc.Contract_Code__c);
            }
        }
        
        pur_codes = pur_codes.lastIndexOf(',') > 0 ? '(' + pur_codes.substring(0,pur_codes.lastIndexOf(',')) + ')' : pur_codes;
        
    }
}