global class CANGenerationActivityScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
    CanGenerationActivityBatch b = new CanGenerationActivityBatch();
        database.executebatch(b, 1);
    }

}