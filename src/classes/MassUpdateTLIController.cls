/*  Author: Paolo Mendoza
    Date: 4/23/2015
    Description: Mass Assign Transaction Line Item to a Transaction Payment
                 Controller for MassUpdateTLI
*/


public class MassUpdateTLIController{
    
    public Transaction_Payment__c transAcc { get; set; }
    public Account_Transaction__c accTrans { get; set; }
    public List <Transaction_Line_item__c> tliList {get; set;}
    public List <TLIWrapperCls> tliWrapList {get; set;}
    public Set<String> selTLI {get;set;}   
    public Boolean hasSelTLI {get;set;}
    Set<Id> TLItemsId = new Set<Id> ();
    List<Transaction_Line_Item__c> tlItemList = new List<Transaction_Line_Item__c>();

 
    public MassUpdateTLIController(ApexPages.StandardController controller) {
    
        transAcc = [SELECT id,Name,Account_Transaction__c FROM Transaction_Payment__c WHERE Id =:ApexPages.CurrentPage().getParameters().get('id')];  
        accTrans = [SELECT id,Name FROM Account_Transaction__c WHERE Id =: transAcc.Account_Transaction__c];  
        tliList = [SELECT id, Name, Transaction_Payment__c, Account_Transaction__c,Item__c,Line_Detail__c, Total_Amount__c
         FROM Transaction_Line_item__c WHERE Account_Transaction__c =:accTrans.id]; 
        tliWrapList = new List<TLIWrapperCls>();
        selTLI = new Set<String>();
        
        for(Transaction_Line_item__c tli : tliList){
               tliWrapList.add(new TLIWrapperCls(tli));
          }
        
        
     }

    
    public void displaySelectedTLI(){
          selTLI.clear();
          hasSelTLI = false;
          for(TLIWrapperCls TLIWrapper : tliWrapList){
               if(TLIWrapper.isSelected){
                    hasSelTLI = true;
                    TLItemsId.add(TLIWrapper.tliList.id);
                    selTLI.add(TLIWrapper.tliList.id); 
               }
          }
          tlItemList = [SELECT id, Name, Transaction_Payment__c FROM Transaction_Line_item__c WHERE id =:TLItemsId];
          for(Transaction_Line_Item__c transItems: tlItemList){
              transItems.Transaction_Payment__c = transAcc.id;
          }
          update tlItemList;
     }
     
     public class TLIWrapperCls {
         public Boolean isSelected {get;set;}
         public Transaction_Line_item__c tliList {get;set;}
    
         public TLIWrapperCls(Transaction_Line_item__c tliList){
              this.isSelected = false;
              this.tliList = tliList;
         }
    }
}