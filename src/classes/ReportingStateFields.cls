/**
 * @description Reporting State Fields Trigger Handler
 * @author Janella Lauren Canlas
 * @date 31.OCT.2012
 * @history
 *       31.OCT.2012    Janella Canlas      - Created.
 *       22.JAN.2013    Janella Canlas      - extended method of Copy Mailing Address to Business Accounts
 *       15.FEB.2013    Michi Magsarili     - add provisions for Postal_is_the_same_as_Shipping__c field (only used in Business Accounts)
 *       07.NOV.2013    Yuri Gribanov       - Removed code to copy Mailing address fields into Other Addess fields for Student Recordtype due to AV7 changes
 *       24.SEP.2014    Warjie Malibago     - Add code to not allow recursive
 *       03.MAR.2016    Ranyel Maliwanag    - Used the Schema class to obtain Record Type Ids instead of repeatedly querying for the same thing which resulted to reaching SOQL 101 errors in production
 */
public with sharing class ReportingStateFields {
    public static Boolean hasRun = false;
    private static Id trainingOrgRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Training Organisation').getRecordTypeId();
    private static Id studentRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student').getRecordTypeId();
    /*
        This method will update the Reporting State fields depending on state
    */
    public static void updateReportingStateFields(List<Account> accs){
        if(hasRun == false){
            system.debug('**R Has Run? ' + hasRun);
        List<State__c>  stateList = [SELECT Name, Short_Name__c,Id from State__c];
        
        Map<String, Id> stateMap = new Map<String,Id>();
        
        Set<Id> accIdSet = new Set<Id>();
        Set<Id> recTypeSet = new Set<Id>();
        
        //retrieve accIds and recTypeIds
        for(Account a : accs){
            accIdSet.add(a.Id);
            recTypeSet.add(a.RecordTypeId);
        }
        //create a Map containing all states
        for(State__c s : stateList){
            if(!stateMap.containsKey(s.Short_Name__c)){
                //populate map
                stateMap.put(s.Short_Name__c,s.Id);
            }
        }
        
        for(Account a : accs){
              if(a.RecordTypeId == trainingOrgRecordType){
                //Reporting Billing State
                if(a.BillingState != 'QLD' && a.BillingState != 'NSW' && 
                    a.BillingState != 'VIC' && a.BillingState != 'TAS' && 
                    a.BillingState != 'SA' && a.BillingState != 'WA' && a.BillingState != 'NT'){
                        //if country is 'AUSTRALIA'
                        if(a.BillingCountry == 'AUSTRALIA'){
                            a.Reporting_Billing_State__c = stateMap.get('OTHER');
                        }
                        else{
                            a.Reporting_Billing_State__c = stateMap.get('OVERSEAS');
                        }
                }
                else if(a.BillingState == 'QLD' || a.BillingState == 'NSW' || 
                        a.BillingState == 'VIC' || a.BillingState == 'TAS' || 
                        a.BillingState == 'SA' || a.BillingState == 'WA' || a.BillingState == 'NT'){
                            a.Reporting_Billing_State__c = stateMap.get(a.BillingState);
                }
                /*else if(a.BillingState == null){
                            a.Reporting_Billing_State__c = '';
                }*/
            }
        }
        hasRun = true;
        }
    }
    
    /*
        -added J.Canlas 11062012
        -This method will copy Mailing Address upon insert if recordtype is Student and Postal is the same as Residential field is checked
    */
    public static void copyMailingAddress_insert(List<Account> accList){
        List<Account> acctToUpdate = new List<Account>();
        List<RecordType> recTypeList = new List<RecordType>();
        
        Set<Id> recTypeIds = new Set<Id>();
        //collect record type ids
        for(Account a : accList){
            recTypeIds.add(a.RecordTypeId);
        }
        //copy mailing address to the other address if recordtype is Student and Postal is the same as Residential field is checked
        //mmagsarili 02/15/2013  start
        for(Account a : accList){
            if(a.RecordTypeId != studentRecordType){ //Student record type
                if(a.Postal_is_the_same_as_Shipping__c == true){
                    a.ShippingCountry = a.BillingCountry;
                    a.ShippingStreet = a.BillingStreet;
                    a.ShippingCity = a.BillingCity;
                    a.ShippingState = a.BillingState;
                    a.ShippingPostalCode = a.BillingPostalCode;
                }
            }            
        }
        //mmagsarili 02/15/2013 end
    }
    /*
        -added J.Canlas 11062012
        -This method will copy Mailing Address upon update if recordtype is Student and Postal is the same as Residential field is checked
    */
    public static void copyMailingAddress_update(List<Account> accList, List<Account> oldAcctList){
        List<Account> acctToUpdate = new List<Account>();
        List<RecordType> recTypeList = new List<RecordType>();
        
        system.debug('**accList size: ' + accList.size());

        //return error if the person other fields are modified but the postal is the same as residential field is checked
        for(Account a : accList){
            for(Account old : oldAcctList){
                //mmagsarili 02/15/2013  start
                if(a.RecordTypeId != studentRecordType){ //Student record type
                    if(a.Id == old.Id && a.Postal_is_the_same_as_Shipping__c == true){
                        if(a.ShippingCity != old.ShippingCity || a.ShippingCountry != old.ShippingCountry || a.ShippingStreet != old.ShippingStreet || a.ShippingPostalCode != old.ShippingPostalCode || a.ShippingState != old.ShippingState){
                            if(old.ShippingCity == a.BillingCity && old.ShippingCountry == a.BillingCountry && old.ShippingState == a.BillingState && old.ShippingStreet == a.BillingStreet && old.ShippingPostalCode == a.BillingPostalCode){
                                a.addError('You cannot modify Shipping Address if "Postal is the same as Shipping field" is checked.');
                                system.debug('**ERROR HERE');
                            }
                        }
                    }
                }
               
                //mmagsarili 02/15/2013  end
            }
        }
        
       
        //copy mailing address to the other address if recordtype is Student and Postal is the same as Residential field is checked
        for(Account a : accList){
           //mmagsarili 02/15/2013  start
            if(a.RecordTypeId != studentRecordType){ //Student record type
                if(a.Postal_is_the_same_as_Shipping__c == true){
                    system.debug('**Update these');
                    a.ShippingCountry = a.BillingCountry;
                    a.ShippingStreet = a.BillingStreet;
                    a.ShippingCity = a.BillingCity;
                    a.ShippingState = a.BillingState;
                    a.ShippingPostalCode = a.BillingPostalCode;
                }
            }
           
        }
    }
    
}