/**
 * @description Test class for InPlaceExportBatch
 * @author Ranyel Maliwanag
 * @date 21.MAY.2015
 * @history
 *       13.NOV.2015    Ranyel Maliwanag    Added additional test method for running the clean up batch process
 */
@isTest
public with sharing class InPlaceExportBatch_Test {
    @testSetup
    static void batchSetup() {
        InPlace_faculty__c faculty = new InPlace_faculty__c(
                IPF_Description__c = 'Health Services',
                IPF_Sync_on_next_update__c = true
            );
        insert faculty;

        InPlace_Calendar__c calendar = new InPlace_Calendar__c(
                IPCAL_Description__c = '2015 Calendar Year',
                IPCAL_End_Date__c = Date.newInstance(2015, 1, 1),
                IPCAL_Start_Date__c = Date.newInstance(2015, 12, 31),
                IPC_Sync_on_next_update__c = true
            );
        insert calendar;

        Id parentLocationRTID = Schema.SObjectType.Locations__c.getRecordTypeInfosByName().get('Parent Location').getRecordTypeId();
        Id deliveryLocationRTID = Schema.SObjectType.Locations__c.getRecordTypeInfosByName().get('Training Delivery Location').getRecordTypeId();

        Locations__c parentLocation = new Locations__c(
                IPC_InPlace_Campus__c = true,
                IPC_Sync_on_next_update__c = true,
                Name = 'Sydney',
                RecordTypeId = parentLocationRTID
            );
        insert parentLocation;

        Locations__c location = new Locations__c(
                IPC_InPlace_Campus__c = true,
                IPC_Sync_on_next_update__c = true,
                Name = 'Sydney',
                Parent_Location__c = parentLocation.Id,
                RecordTypeId = deliveryLocationRTID
            );
        insert location;

        InPlace_Discipline__c discipline = new InPlace_Discipline__c(
                IPD_Description__c = 'Nursing',
                IPD_Sync_on_next_update__c = true
            );
        insert discipline;

        Qualification__c qualification = new Qualification__c(
                Name = 'HLT51612 VC3',
                Qualification_Name__c = 'Diploma of Nursing (Enrolled-Division 2 Nursing)',
                IPC_InPlace_Course__c = true,
                IPC_Sync_on_next_update__c = true,
                Effective_Date__c = Date.newInstance(2015, 4, 1),
                Expiry_Date__c = Date.newInstance(2017, 4, 1),
                IPC_Version__c = 1,
                InPlace_faculty__c = faculty.Id,
                InPlace_Discipline__c = discipline.Id
            );
        insert qualification;


        Unit__c unit = new Unit__c(
                Name = 'VP1AGED',
                IPU_Sync_on_next_update__c = true,
                InPlace_Unit__c = true,
                IPU_InPlace_Discipline__c = discipline.Id,
                IPU_InPlace_faculty__c = faculty.Id,
                IPU_Start_Date__c = Date.newInstance(2015, 1, 1),
                IPU_End_Date__c = Date.newInstance(2015, 12, 31),
                IPU_Unit_Version__c = 1
            );
        insert unit;

        InPlace_Unit_Version__c unitVersion = new InPlace_Unit_Version__c(
                Unit__c = unit.Id,
                Version_Number__c = 1,
                InPlace_Discipline__c = discipline.Id
            );
        insert unitVersion;

        VFH_Controller__c vfhController = new VFH_Controller__c(
                Report_Start_Date__c = Date.newInstance(2014, 3, 26),
                Report_End_Date__c = Date.newInstance(2015, 9, 30)
            );
        insert vfhController;


        Funding_Stream_Types__c fundingStream = new Funding_Stream_Types__c(
                Name = 'Vet Fee Help'
            );
        insert fundingStream;


        Program__c program = new Program__c(
                Qualification__c = qualification.Id
            );
        insert program;


        Intake__c intake = new Intake__c(
                Qualification__c = qualification.Id,
                End_Date__c = Date.newInstance(2016, 2, 26),
                Intake_Status__c = 'Active',
                Start_Date__c = Date.newInstance(2015, 7, 20),
                Funding_Stream__c = fundingStream.Id,
                Program__c = program.Id,
                Training_Delivery_Location__c = location.Id
            );
        insert intake;

        Intake_Unit__c intakeUnit = new Intake_Unit__c(
                IPUO_Sync_on_next_update__c = true,
                Intake__c = intake.Id,
                Unit_of_Study__c = unit.Id,
                IPUO_Calendar_Sequence_Number__c = calendar.Id
            );
        insert intakeUnit;


        Account student = new Account(
                FirstName = 'Godric',
                LastName = 'Gryffindor',
                Student_Identifer__c = 'CX1230',
                PersonBirthdate = Date.newInstance(2015, 5, 19),
                PersonEmail = 'person@inplace.com',
                Middle_Name__c = 'Taylor',
                Phone = '548242',
                PersonHomePhone = '548242',
                PersonMobilePhone = '745635',
                Salutation = 'Mr',
                Suburb_locality_or_town__c = 'Fortitude Valley',
                Emergency_Contact_Name__c = 'Albus Dumbledore',
                Relationship_to_Emergency_Contact__c = 'Headmaster',
                Emergency_Contact_Phone_Number__c = '789126',
                Emergency_Contact_Mobile_Number__c = '49785126',
                Sex__c = 'M - Male'
            );
        insert student;

        Attendance_Types__c attendanceType = new Attendance_Types__c(
                Name = 'Full-time'
            );
        insert attendanceType;


        Enrolment__c enrolment = new Enrolment__c(
                IPS_Sync_on_next_update__c = true,
                IPS_InPlace_Student__c = true,
                Student__c = student.Id,
                Enrolment_Status__c = 'Active (Commencement)',
                Qualification__c = qualification.Id,
                Type_of_Attendance__c = attendanceType.Id,
                Mailing_Country__c = 'Australia',
                Mailing_Zip_Postal_Code__c = '4006',
                Name_of_suburb_town_locality__c = 'Fortitude Valley',
                Mailing_State_Provience__c = 'Queensland',
                Address_street_number__c = '100',
                Address_street_name__c = 'Brookes Street',
                Address_flat_unit_details__c = 'Ground Floor'
            );
        insert enrolment;

        Qualification_Unit__c qualificationUnit = new Qualification_Unit__c(
                Qualification__c = qualification.Id,
                Unit__c = unit.Id
            );
        insert qualificationUnit;

        Purchasing_Contracts__c purchasingContract = new Purchasing_Contracts__c(
                Contract_Code__c = 'CC0001',
                Contract_Name__c = 'Marauder\'s Contract'
            );
        insert purchasingContract;

        Contract_Line_Items__c contractLineItem = new Contract_Line_Items__c(
                Purchasing_Contract__c = purchasingContract.Id,
                Line_Item_Number__c = '001'
            );
        insert contractLineItem;

        Line_Item_Qualifications__c liq = new Line_Item_Qualifications__c(
                Contract_Line_Item__c = contractLineItem.Id
            );
        insert liq;

        Line_Item_Qualification_Units__c liqu = new Line_Item_Qualification_Units__c(
                Line_Item_Qualification__c = liq.Id,
                Qualification_Unit__c = qualificationUnit.Id
            );
        insert liqu;

        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c(
                Enrolment__c = enrolment.Id,
                Unit__c = unit.Id,
                Line_Item_Qualification_Unit__c = liqu.Id
            );
        insert enrolmentUnit;


        Enrolment_Intake_Unit__c scheduledUnit = new Enrolment_Intake_Unit__c(
                Enrolment__c = enrolment.Id,
                Intake_Unit__c = intakeUnit.Id,
                Enrolment_Unit__c = enrolmentUnit.Id,
                IPSUO_Active__c = 'Y',
                IPSUO_Result__c = 'PASSED'
            );
        insert scheduledUnit;

        Placement_Readiness_Types__c readinessType = new Placement_Readiness_Types__c(
                In_Place_Reference_Number__c = 'ERS0001',
                In_Place_Reference_Number_Community__c = 'ERS0002',
                In_Place_Reference_Number_Aged_Care__c = 'ERS0003'
            );
        insert readinessType;

        Completed_Placement_Prerequisites__c placementPrerequisite = new Completed_Placement_Prerequisites__c(
                Completed_Prerequisite__c = readinessType.Id,
                Enrolment__c = enrolment.Id,
                IPE_Sync_on_next_update__c = true,
                Expiry_Date__c = Date.today()
            );
        insert placementPrerequisite;

        InPlaceSettings__c testSettings = new InPlaceSettings__c(
                ToggleSyncFlagOnExport__c = true
            );
        insert testSettings;
    }

    static testMethod void testRule1() {
        Test.startTest();
            InPlaceExportBatchStudentPrep batchProcess = new InPlaceExportBatchStudentPrep();
            Database.executeBatch(batchProcess);
        Test.stopTest();

        InPlace__c record;
        List<InPlace__c> exports = [SELECT Id, IPFaculty_Description__c FROM InPlace__c WHERE IPFaculty_FacultyCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPFaculty_Description__c));

        exports = [SELECT Id, IPCalendar_Description__c FROM InPlace__c WHERE IPCalendar_CalendarCode__c != null AND IPCalendar_CalendarSeqNum__c != null AND IPCalendar_EndDate__c != null AND IPCalendar_StartDate__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPCalendar_Description__c));

        exports = [SELECT Id, IPCampus_Description__c, IPCampus_CampusCode__c FROM InPlace__c WHERE IPCampus_CampusCode__c != null];
        System.assertEquals(2, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPCampus_Description__c));

        exports = [SELECT Id, IPDiscipline_DisciplineCode__c, IPDiscipline_Description__c FROM InPlace__c WHERE IPDiscipline_DisciplineCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPDiscipline_Description__c));

        exports = [SELECT Id, IPCourse_CourseCode__c, IPCourseVersion_CourseCode__c, IPCourseVersion_Version__c, IPCourseVersion_StartDate__c, IPCourseVersion_EndDate__c, IPCourseVersion_Description__c, IPCourseVersion_FacultyCode__c, IPCourseVersion_PrimaryDisciplineCode__c FROM InPlace__c WHERE IPCourse_CourseCode__c != null AND IPCourseVersion_CourseCode__c != null AND IPCourseVersion_Version__c != null AND IPCourseVersion_StartDate__c != null AND IPCourseVersion_FacultyCode__c != null AND IPCourseVersion_PrimaryDisciplineCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPCourseVersion_Description__c));

        exports = [SELECT Id, IPUnitVersion_Description__c, IPUnitVersion_DisciplineCode__c, IPUnitVersion_EndDate__c, IPUnitVersion_FacultyCode__c, IPUnitVersion_StartDate__c, IPUnitVersion_UnitCode__c, IPUnitVersion_UnitVersion__c FROM InPlace__c WHERE IPUnitVersion_UnitCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPUnitVersion_Description__c));
        System.assert(String.isNotBlank(record.IPUnitVersion_DisciplineCode__c));
        System.assert(record.IPUnitVersion_EndDate__c != null);
        System.assert(String.isNotBlank(record.IPUnitVersion_FacultyCode__c));
        System.assert(record.IPUnitVersion_StartDate__c != null);
        System.assert(record.IPUnitVersion_UnitVersion__c != null);

        exports = [SELECT Id, IPUnitOffering_CalendarSeqNum__c, IPUnitOffering_CampusCode__c, IPUnitOffering_UnitClass__c, IPUnitOffering_UnitCode__c, IPUnitOffering_UnitOfferingCode__c, IPUnitOffering_UnitVersion__c FROM InPlace__c WHERE IPUnitOffering_UnitOfferingCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPUnitOffering_CalendarSeqNum__c));
        System.assert(String.isNotBlank(record.IPUnitOffering_CampusCode__c));
        System.assert(String.isNotBlank(record.IPUnitOffering_UnitCode__c));
        System.assert(record.IPUnitOffering_UnitVersion__c != null);

        exports = [SELECT Id, IPStudent_DeceasedFlag__c, IPStudent_DOB__c, IPStudent_Email__c, IPStudent_ForeignDomesticStatusCode__c, IPStudent_FullTimePartTime__c, IPStudent_Gender__c, IPStudent_GivenName__c, IPStudent_OtherName__c, IPStudent_PhoneHome__c, IPStudent_PhoneMobile__c, IPStudent_StudentCode__c, IPStudent_StudentEnrolmentCode__c, IPStudent_Surname__c, IPStudent_Title__c, IPStudent_Username__c, IPStudentAddress_Active__c, IPStudentAddress_Country__c, IPStudentAddress_Postcode__c, IPStudentAddress_State__c, IPStudentAddress_Street__c, IPStudentAddress_StudentCode__c, IPStudentAddress_Suburb__c, IPStudentAddress_Unit__c FROM InPlace__c WHERE IPStudent_StudentCode__c != null AND IPStudentAddress_StudentCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPStudent_DeceasedFlag__c));
        System.assert(record.IPStudent_DOB__c != null);
        System.assert(String.isNotBlank(record.IPStudent_Email__c));
        System.assert(String.isNotBlank(record.IPStudent_ForeignDomesticStatusCode__c));
        System.assert(String.isNotBlank(record.IPStudent_FullTimePartTime__c));
        System.assert(String.isNotBlank(record.IPStudent_Gender__c));
        System.assert(String.isNotBlank(record.IPStudent_GivenName__c));
        System.assert(String.isNotBlank(record.IPStudent_OtherName__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneHome__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneMobile__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentEnrolmentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_Surname__c));
        System.assert(String.isNotBlank(record.IPStudent_Title__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Active__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Country__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Postcode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_State__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Street__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Suburb__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Unit__c));

        exports = [SELECT Id, IPStudentUnitOffering_Active__c, IPStudentUnitOffering_CECode__c, IPStudentUnitOffering_CourseCode__c, IPStudentUnitOffering_CourseVersion__c, IPStudentUnitOffering_Result__c, IPStudentUnitOffering_StudentCode__c, IPStudentUnitOffering_UnitOfferingCode__c, IPStudentUnitOffering_UOEnrolCode__c, IPStudentUnitOffering_Year__c FROM InPlace__c WHERE IPStudentUnitOffering_UnitOfferingCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_Active__c));
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_CECode__c));
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_CourseCode__c));
        System.assert(record.IPStudentUnitOffering_CourseVersion__c != null);
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_Result__c));
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_UnitOfferingCode__c));
        System.assert(String.isNotBlank(record.IPStudentUnitOffering_UOEnrolCode__c));
        System.assert(record.IPStudentUnitOffering_Year__c != null);


        exports = [SELECT Id, IPEntityRelationshipEntity_Action__c, IPEntityRelationshipEntity_AttrValue__c, IPEntityRelationshipEntity_EntityCode__c, IPEntityRelationshipEntity_ErRef__c, IPEntityRelationshipEntity_ExpiryDate__c FROM InPlace__c WHERE IPEntityRelationshipEntity_EntityCode__c != null AND IPEntityRelationshipEntity_ErRef__c LIKE 'ERS%'];
        //System.debug('ER exports size ::: ' + exports);
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPEntityRelationshipEntity_AttrValue__c));
        System.assert(String.isNotBlank(record.IPEntityRelationshipEntity_EntityCode__c));
        System.assert(String.isNotBlank(record.IPEntityRelationshipEntity_ErRef__c));
        System.assert(record.IPEntityRelationshipEntity_ExpiryDate__c != null);
    }

    static testMethod void testRule2() {
        InPlace_Discipline__c discipline = [SELECT Id FROM InPlace_Discipline__c];
        discipline.IPD_Description__c = 'Health Care';
        update discipline;

        Test.startTest();
            InPlaceExportBatchStudent batchProcess = new InPlaceExportBatchStudent();
            Database.executeBatch(batchProcess);
        Test.stopTest();

        InPlace__c record;
        List<InPlace__c> exports = [SELECT Id, IPStudent_DeceasedFlag__c, IPStudent_DOB__c, IPStudent_Email__c, IPStudent_ForeignDomesticStatusCode__c, IPStudent_FullTimePartTime__c, IPStudent_Gender__c, IPStudent_GivenName__c, IPStudent_OtherName__c, IPStudent_PhoneHome__c, IPStudent_PhoneMobile__c, IPStudent_StudentCode__c, IPStudent_StudentEnrolmentCode__c, IPStudent_Surname__c, IPStudent_Title__c, IPStudent_Username__c, IPStudentAddress_Active__c, IPStudentAddress_Country__c, IPStudentAddress_Postcode__c, IPStudentAddress_State__c, IPStudentAddress_Street__c, IPStudentAddress_StudentCode__c, IPStudentAddress_Suburb__c, IPStudentAddress_Unit__c FROM InPlace__c WHERE IPStudent_StudentCode__c != null AND IPStudentAddress_StudentCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPStudent_DeceasedFlag__c));
        System.assert(record.IPStudent_DOB__c != null);
        System.assert(String.isNotBlank(record.IPStudent_Email__c));
        System.assert(String.isNotBlank(record.IPStudent_ForeignDomesticStatusCode__c));
        System.assert(String.isNotBlank(record.IPStudent_FullTimePartTime__c));
        System.assert(String.isNotBlank(record.IPStudent_Gender__c));
        System.assert(String.isNotBlank(record.IPStudent_GivenName__c));
        System.assert(String.isNotBlank(record.IPStudent_OtherName__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneHome__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneMobile__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentEnrolmentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_Surname__c));
        System.assert(String.isNotBlank(record.IPStudent_Title__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Active__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Country__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Postcode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_State__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Street__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Suburb__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Unit__c));
    }

    static testMethod void testRule3() {
        InPlace_Discipline__c discipline = [SELECT Id FROM InPlace_Discipline__c];
        discipline.IPD_Description__c = 'Community Services Work';
        update discipline;
        
        Test.startTest();
            InPlaceExportBatchStudent batchProcess = new InPlaceExportBatchStudent();
            Database.executeBatch(batchProcess);
        Test.stopTest();

        InPlace__c record;
        List<InPlace__c> exports = [SELECT Id, IPStudent_DeceasedFlag__c, IPStudent_DOB__c, IPStudent_Email__c, IPStudent_ForeignDomesticStatusCode__c, IPStudent_FullTimePartTime__c, IPStudent_Gender__c, IPStudent_GivenName__c, IPStudent_OtherName__c, IPStudent_PhoneHome__c, IPStudent_PhoneMobile__c, IPStudent_StudentCode__c, IPStudent_StudentEnrolmentCode__c, IPStudent_Surname__c, IPStudent_Title__c, IPStudent_Username__c, IPStudentAddress_Active__c, IPStudentAddress_Country__c, IPStudentAddress_Postcode__c, IPStudentAddress_State__c, IPStudentAddress_Street__c, IPStudentAddress_StudentCode__c, IPStudentAddress_Suburb__c, IPStudentAddress_Unit__c FROM InPlace__c WHERE IPStudent_StudentCode__c != null AND IPStudentAddress_StudentCode__c != null];
        System.assertEquals(1, exports.size());
        record = exports.get(0);
        System.assert(String.isNotBlank(record.IPStudent_DeceasedFlag__c));
        System.assert(record.IPStudent_DOB__c != null);
        System.assert(String.isNotBlank(record.IPStudent_Email__c));
        System.assert(String.isNotBlank(record.IPStudent_ForeignDomesticStatusCode__c));
        System.assert(String.isNotBlank(record.IPStudent_FullTimePartTime__c));
        System.assert(String.isNotBlank(record.IPStudent_Gender__c));
        System.assert(String.isNotBlank(record.IPStudent_GivenName__c));
        System.assert(String.isNotBlank(record.IPStudent_OtherName__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneHome__c));
        System.assert(String.isNotBlank(record.IPStudent_PhoneMobile__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_StudentEnrolmentCode__c));
        System.assert(String.isNotBlank(record.IPStudent_Surname__c));
        System.assert(String.isNotBlank(record.IPStudent_Title__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Active__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Country__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Postcode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_State__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Street__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_StudentCode__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Suburb__c));
        System.assert(String.isNotBlank(record.IPStudentAddress_Unit__c));
    }

    static testMethod void batchCleanupTest() {
        InPlace__c exportRecord = new InPlace__c();
        insert exportRecord;
        
        Test.startTest();
            List<InPlace__c> exports = [SELECT Id FROM InPlace__c];
            System.assert(exports.size() > 0);

            InPlaceExportCleanupBatch bp = new InPlaceExportCleanupBatch(1);
            Id jobId = Database.executeBatch(bp);
        Test.stopTest();

        exports = [SELECT Id FROM InPlace__c];
        System.assert(exports.size() == 0);
    }
}