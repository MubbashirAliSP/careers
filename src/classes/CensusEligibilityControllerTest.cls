/*Test Class for CensusEligibilityController
 * Test Census1, Census2, Census3
 * Test Stage1, Stage2
 * Written by Yuri Gribanov, CloudSherpas 3rd December 2015
 */ 


@istest public class CensusEligibilityControllerTest {
    @testSetup
    static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(
            Alias = 'standt', 
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            LastName='EUOS_User',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='careerstestuser@careersaustralia.edu.au.test'
        );
        insert u;
    }
    
    private static testmethod void testCensus1() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 1');
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        intake.Stage__c = stage1.Id;
        update intake;
        
        Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        enrl.Assigned_Trainer__c = u.Id;
        update enrl;
        
        Test.startTest();
            Task t = new Task(
                Eligibility_Items__c = 'Stage 1 Introduction',
                RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
                Subject = 'Call',
                ownerId = u.Id,
                whoId   = enrl.Student__r.PersonContactId,
                whatId  = enrl.Id,
                Status = 'Completed'
            );
            insert t;
        Test.stopTest();

        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];
            
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C1 (incur debt)').getRecordTypeId());
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.Eligibility_Item__c, 'Stage 1 Introduction');
        system.assertEquals([Select EligibleforCensus__c From Census__c].EligibleforCensus__c, true);
    }
    
    private static testmethod void testCensus2() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();            
       
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 2');
        
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        
        intake.Stage__c = stage1.Id;
        
        update intake;
        
        Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        
        enrl.Assigned_Trainer__c = u.Id;
        
        update enrl;
        
        Test.startTest();
            
            Task t = new Task(Eligibility_Items__c = 'Stage 2 Introduction',
                              RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
                              Subject = 'Call',
                              ownerId = u.Id,
                              whoId   = enrl.Student__r.PersonContactId,
                              whatId  = enrl.Id,
                              Status = 'Completed');
        
            insert t;
        
        Test.stopTest();
            
        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];
            
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C2 (incur debt)').getRecordTypeId());
        system.assertEquals(item.Eligibility_Item__c, 'Stage 2 Introduction');
        system.assertEquals([Select EligibleforCensus__c From Census__c].EligibleforCensus__c, true);
    }
    
    private static testmethod void testCensus3() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();            
       
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 3');
        
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        
        intake.Stage__c = stage1.Id;
        
        update intake;
        
        Enrolment__c enrl = [Select Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        
        enrl.Assigned_Trainer__c = u.Id;
        
        update enrl;
        
        Test.startTest();
            
            Task t = new Task(Eligibility_Items__c = 'Stage 3 Lesson',
                              RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(),
                              Subject = 'Call',
                              ownerId = u.Id,
                              whoId   = enrl.Student__r.PersonContactId,
                              whatId  = enrl.Id,
                              Status = 'Completed');
        
            insert t;
        
        Test.stopTest();
            
        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];
            
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for C3 (incur debt)').getRecordTypeId());
        system.assertEquals(item.Eligibility_Item__c, 'Stage 3 Lesson');
        system.assertEquals([Select EligibleforCensus__c From Census__c].EligibleforCensus__c, true);
    }
    

    private static testmethod void testStage1() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 1');
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        intake.Stage__c = stage1.Id;
        update intake;
        
        Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        enrl.Assigned_Trainer__c = u.Id;
        update enrl;

        Test.startTest();
            AssessmentAttempt__c attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0001');
            insert attempt;
        Test.stopTest();
            
        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];

        Case testcase = [Select Id, Type, Status, Origin, Subject, Description, Priority, Submitted_By_User__c, RecordTypeId From Case LIMIT 1];

        Task testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() LIMIT 1];
        
        // Assert Eligiblity
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for Stage 2 (unit release)').getRecordTypeId());
        system.assertEquals(item.Eligibility_Item__c, 'Stage 1 Assessment');            
      
        // Assert Case
        system.assertEquals(testcase.Type, 'New');
        system.assertEquals(testcase.Origin, 'Web');
        system.assertEquals(testcase.Subject, 'Unit for Release');
        system.assertEquals(testcase.Description, 'Please Release the Stage 2 Units');
        system.assertEquals(testcase.Submitted_By_User__c, u.Id);
        system.assertEquals(testcase.RecordTypeId, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Unit For Release').getRecordTypeId());
        
        // Assert task
        system.assertEquals(testTask.RecordtypeId, Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId());
        system.assertEquals(testTask.Subject, 'Assessment Attempt');
        system.assertEquals(testTask.ownerId, u.Id);
        system.assertEquals(testTask.whoId, enrl.Student__r.PersonContactId);
        system.assertEquals(testTask.whatId, enrl.Id);
        system.assertEquals(testTask.Eligibility_Items__c, 'Stage 1 Assessment');
    }

    private static testmethod void testStage1DeactivatedTrainer() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 1');
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        intake.Stage__c = stage1.Id;
        update intake;
        
        Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        enrl.Assigned_Trainer__c = u.Id;
        update enrl;

        u.IsActive = false;
        update u;

        Test.startTest();
            AssessmentAttempt__c attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0001');
            insert attempt;
        Test.stopTest();
            
        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];
        Case testcase = [Select Id, Type, Status, Origin, Subject, Description, Priority, Submitted_By_User__c, RecordTypeId From Case LIMIT 1];
        Task testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() LIMIT 1];
        
        //assert Eligiblity
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for Stage 2 (unit release)').getRecordTypeId());
        system.assertEquals(item.Eligibility_Item__c, 'Stage 1 Assessment');            
      
        //assert Case
        
        system.assertEquals(testcase.Type, 'New');
        system.assertEquals(testcase.Origin, 'Web');
        system.assertEquals(testcase.Subject, 'Unit for Release');
        system.assertEquals(testcase.Description, 'Please Release the Stage 2 Units');
        system.assertEquals(testcase.Submitted_By_User__c, UserInfo.getUserId());
        system.assertEquals(testcase.RecordTypeId, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Unit For Release').getRecordTypeId());
        
        //assert task
        
        system.assertEquals(testTask.RecordtypeId, Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId());
        system.assertEquals(testTask.Subject, 'Assessment Attempt');
        system.assertEquals(testTask.ownerId, UserInfo.getUserId());
        system.assertEquals(testTask.whoId, enrl.Student__r.PersonContactId);
        system.assertEquals(testTask.whatId, enrl.Id);
        system.assertEquals(testTask.Eligibility_Items__c, 'Stage 1 Assessment');
    }

    private static testmethod void testStage1MultipleAttempts() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 1');
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        intake.Stage__c = stage1.Id;
        update intake;
        
        Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        enrl.Assigned_Trainer__c = u.Id;
        update enrl;       
       
        
        Test.startTest();
            // Create multiple assessment attempts for the same census
            List<AssessmentAttempt__c> attemptList = new List<AssessmentAttempt__c>();
            
            AssessmentAttempt__c attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0001');
            attemptList.add(attempt);
            attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0002');
            attemptList.add(attempt);
            insert attemptList;

            attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0003');
            insert attempt;
        Test.stopTest();
            
        List<Case> testcase = [Select Id, Type, Status, Origin, Subject, Description, Priority, Submitted_By_User__c, RecordTypeId From Case];
        System.assertEquals(1, testCase.size());
    }

    private static testmethod void testStage1Suspended() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 1');
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        intake.Stage__c = stage1.Id;
        update intake;
        
        Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        enrl.Assigned_Trainer__c = u.Id;
        enrl.Enrolment_Status__c = 'Suspended';
        update enrl;       
       
        
        Test.startTest();
            AssessmentAttempt__c attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0001');
            insert attempt;
        Test.stopTest();
            
        List<Census_Eligibility_Items__c> item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c];

        List<Case> testcase = [Select Id, Type, Status, Origin, Subject, Description, Priority, Submitted_By_User__c, RecordTypeId From Case];

        List<Task> testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId()];
        
        // Assert Eligiblity
        System.assert(item.isEmpty());
      
        // Assert Case
        System.assert(testCase.isEmpty());
        
        // Assert task
        System.assert(testTask.isEmpty());
    }
    
    private static testmethod void testStage2() {
        User u = [SELECT Id FROM User WHERE Username = 'careerstestuser@careersaustralia.edu.au.test'];

        Census__c census = SASTestUtilities.createCensus();            
       
        insert census;
        
        Stage__c stage1 = new Stage__c(Name = 'Stage 2');
        
        insert stage1;
        
        Intake__c intake = [Select Stage__c From Intake__c];
        
        intake.Stage__c = stage1.Id;
        
        update intake;
        
        Enrolment__c enrl = [Select Student__c, Student__r.PersonContactId, Assigned_Trainer__c From Enrolment__c LIMIT 1];
        
        enrl.Assigned_Trainer__c = u.Id;
        
        update enrl;       
       
        
        Test.startTest();
            
        AssessmentAttempt__c attempt = new AssessmentAttempt__c(census__c = census.Id, AccountId__c = enrl.Student__c,BBAssessmentRecordId__c = '0001');
            
        insert attempt;         
        
        Test.stopTest();
            
        Census_Eligibility_Items__c item = [Select Census__c,Enrolment__c,Eligibility_Item__c, RecordtypeId From Census_Eligibility_Items__c LIMIT 1];
        
                 
        Case testcase = [Select Id, Type, Status, Origin, Subject, Description, Priority, Submitted_By_User__c, RecordTypeId From Case LIMIT 1];
            
        
            
        Task testTask = [Select RecordtypeId, Subject, ownerid, whoid,whatid, status,Eligibility_Items__c  from Task Where RecordTypeId = : Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId() LIMIT 1];
        
        //assert Eligiblity
        system.assertEquals(item.Census__c, census.Id);
        system.assertEquals(item.Enrolment__c, enrl.Id);
        system.assertEquals(item.RecordTypeId, Schema.SObjectType.Census_Eligibility_Items__c.getRecordTypeInfosByName().get('Eligible for Stage 3 (unit release)').getRecordTypeId());
        system.assertEquals(item.Eligibility_Item__c, 'Stage 2 Assessment');            
      
        //assert Case
        
        system.assertEquals(testcase.Type, 'New');
        system.assertEquals(testcase.Origin, 'Web');
        system.assertEquals(testcase.Subject, 'Unit for Release');
        system.assertEquals(testcase.Description, 'Please Release the Stage 3 Units');
        system.assertEquals(testcase.Submitted_By_User__c, u.Id);
        system.assertEquals(testcase.RecordTypeId, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Unit For Release').getRecordTypeId());
        
        //assert task
        
        system.assertEquals(testTask.RecordtypeId, Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId());
        system.assertEquals(testTask.Subject, 'Assessment Attempt');
        system.assertEquals(testTask.ownerId, u.Id);
        system.assertEquals(testTask.whoId, enrl.Student__r.PersonContactId);
        system.assertEquals(testTask.whatId, enrl.Id);
        system.assertEquals(testTask.Eligibility_Items__c, 'Stage 2 Assessment');
    }
}