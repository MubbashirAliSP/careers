public class NatValidationMethods {


    public static Integer validateNAT00020(String logId, String query) {
        
        System.assertNotEquals(logId, null);
        
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
        Set<String> cnIdentifiers = new Set<String>{'1100','1101','1102','1199'};
        Set<String> stIdentifiers = new Set<String>{'09','99'};
       
        String state;
        Id rto = [Select Training_Organisation_Id__c From NAT_Validation_Log__c Where id = : logId].Training_Organisation_Id__c;
        String validationState = [Select Validation_State__c From NAT_Validation_Log__c Where id = : logId].Validation_State__c;

        String mainquery =          'Select Id,'+
                                    'LastModifiedDate,'+
                                    'Delivery_Location_Identifier__c,'+
                                    'Training_Organisation__r.Training_Org_Identifier__c,'+
                                    'Training_Organisation__r.Name,'+
                                    'Name,'+
                                    'Postcode__c,'+
                                    'Address_Location__c,'+
                                    'Child_Postcode__c,'+
                                    'Child_Suburb__c,'+
                                    'Country_Identifier__c,'+
                                    'State_Identifier__c,'+
                                    'Child_Country__c,'+
                                    'Child_Address_building_property_name__c,'+
                                    'Child_Address_flat_unit_details__c,'+
                                    'Child_Address_street_number__c,'+
                                    'Child_Address_street_name__c,'+
                                    'Country__r.Country_Code__c '+
                               'FROM Locations__c' +
                               ' WHERE Training_Organisation__c = : rto  AND '+
                                     'Child_State__c = : validationState AND '+
                                     'RecordType.Name = \'Training Delivery Location\' '+
                                     ' AND id in (Select Training_Delivery_Location__c ' + query + ')';
       

        List<Locations__c> locationList = Database.query(mainquery);


        for(Locations__c loc : locationList) {
            
           NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = loc.Id, Nat_File_Name__c = 'NAT00020',Parent_Record_Last_Modified_Date__c = loc.LastModifiedDate);
        
           if(loc.Delivery_Location_Identifier__c == null) 
                event.Error_Message__c = 'Delivery Location Identifier Cannot Be Blank';
                
           else if(loc.Child_Suburb__c == null) 
                event.Error_Message__c = 'Delivery Location Suburb Cannot Be Blank';

           else if((loc.Child_Address_street_name__c == null || loc.Child_Address_street_number__c == null) && state == 'Victoria')
                event.Error_Message__c = 'Street Number Or Street Name cannot be blank';
                
           else if(loc.Child_Postcode__c == null || loc.Country_Identifier__c == null || loc.State_Identifier__c == null)
                event.Error_Message__c = 'Postcode, Country Identifier or State Identifier Cannot Be Blank';
                
           else if(loc.Child_Postcode__c == 'OSPC') {
           
                if(!(cnIdentifiers.contains(loc.Country_Identifier__c)) || !(stIdentifiers.contains(loc.State_Identifier__c))) 
                        event.Error_Message__c = 'Country Identifier cannot be 1100, 1101, 1102, 1199 or State Identifier cannot be 09 or 99 when postcode = OSPC';
               
           }
           
           if(event.Error_Message__c != null)
            errorEventList.add(event);
        }
        
        insert errorEventList;

        return locationList.size();
    
    }
    
    public static Integer validateNAT00030(String logId, String query) {
    
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();

        Set<Id> qualificationIds = new Set<Id>();
        
        
        String mainquery = 'Select qualification__c From Enrolment__c Where Id in (Select Enrolment__c ' + query + ')';
           
        
        List<Enrolment__c> EnrolmentList = Database.query(mainquery);

        for(Enrolment__c enrl : EnrolmentList)
           qualificationIds.add(enrl.qualification__c);
         
        
        for(Qualification__c q : [SELECT id,
                                         Name,
                                         Qualification_Name__c,
                                         Recognition_Status_Identifier__c, 
                                         Qualification_Category_Identifier__c,
                                         Field_Of_Education_Identifier__c, 
                                         ANZSCO_Identifier__c,
                                         Vet_Non_Vet__c,
                                         NAT_Error_Code__c,
                                         LastModifiedDate
                                      FROM Qualification__c WHERE Id in : qualificationIds] ) {
            
           NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = q.Id, Nat_File_Name__c = 'NAT00030',Parent_Record_Last_Modified_Date__c = q.LastModifiedDate);
        
           if(q.Qualification_Category_Identifier__c == null) 
                event.Error_Message__c = 'Qualification Category Identifier Cannot Be Blank';
            
           else if(q.Field_Of_Education_Identifier__c == null) 
                event.Error_Message__c = 'Field of Education Identifier Cannot Be Blank';
           
           else if(q.Recognition_Status_Identifier__c == null)
                event.Error_Message__c = 'Recognition Status Identifier Cannot Be Blank';    
                
            
           else if(q.ANZSCO_Identifier__c == null) 
                event.Error_Message__c = 'ANZSCO Code Identifier Cannot Be Blank';
           
           else if(q.Recognition_Status_Identifier__c == 15.0 && q.Field_of_Education_Identifier__c > '312') 
                event.Error_Message__c = 'If Qualification/Course Recognition Identifier is 15 then Field of Education Identifier must be <= 312';
                
              
           else if((q.Recognition_Status_Identifier__c == 11.0 || q.Recognition_Status_Identifier__c == 12.0 )&& q.Vet_Non_Vet__c == false)
                event.Error_Message__c = 'If Qualification/Course Recognition Identifier cannot be 11 or 12 when Vet Flag is false';
                
           else if((q.ANZSCO_Identifier__c == 'NONVET' && q.Vet_Non_Vet__c != false) || (q.ANZSCO_Identifier__c != 'NONVET' && q.Vet_Non_Vet__c == false))
                event.Error_Message__c = 'For NONVET ANZSCO Identifier Vet Flag needs to be false/unticked'; 
            
           else if(q.Recognition_Status_Identifier__c == 14.0 && (q.Field_Of_Education_Identifier__c < '611' ||  q.Field_Of_Education_Identifier__c > '999') )
                event.Error_Message__c = 'Field of Education Identifier must be in range 611-999 when Qualification Identifier is 14';
                
           else if(q.Vet_Non_Vet__c != true && (q.Field_Of_Education_Identifier__c < '211' ||  q.Field_Of_Education_Identifier__c > '524') )
                event.Error_Message__c = 'If Qualification Category Level of Education Identifier is in range 211-621, Vet Fee flag must be true '; 
            
           else if(q.Recognition_Status_Identifier__c == 12.0 && (q.Field_Of_Education_Identifier__c < '211' ||  q.Field_Of_Education_Identifier__c > '524') )
                event.Error_Message__c = 'Field of Education Identifier must be in range 211-524 when Qualification Identifier is 12';
            
           if(event.Error_Message__c != null)   
            errorEventList.add(event);
              
        }
        
        insert errorEventList;

        return qualificationIds.size();
    
    }
    
    public static Integer validateNAT00060(String logId, String query) {
    
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
        
        String mainquery = 'Select Id,lastmodifieddate,name,Field_of_Education_Identifier__c,Vet_Non_Vet__c,Unit_Flag__c From Unit__c Where Id in (Select Unit__c ' + query + ')';
       

        List<Unit__c> unitList = Database.query(mainquery);
        
        for(Unit__c u : unitList ) {
           
          NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = u.Id, Nat_File_Name__c = 'NAT00060',Parent_Record_Last_Modified_Date__c = u.LastModifiedDate);
    
          if(u.Field_of_Education_Identifier__c == null) 
               event.Error_Message__c = 'Unit of Competency Field of Education Identifier cannot be blank';
               
          else if(u.Field_of_Education_Identifier__c.length() != 6 || u.Field_of_Education_Identifier__c.endsWith('0'))
               event.Error_Message__c = 'Unit of Competency Field of Education Identifier must be 6 digits and cannot end with 0';
          
          /*Its never null  
          else if(u.Vet_Non_Vet__c == null) 
               event.Error_Message__c = 'Unit of Competency Vet Fee Flag cannot be blank';
           */
          else if(u.Unit_Flag__c == 'C' && u.Vet_Non_Vet__c != true) 
                event.Error_Message__c = 'Unit of Competency Flag is C, so Vet Fee flag must be ticked';
                
          if(event.Error_Message__c != null)
            errorEventList.add(event);
           
         }

         insert errorEventList;

         return unitList.size();
    
    }
    
    public static Integer validateNAT00080(String logId, String query) {
        
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();

       /* query = query +' AND (Student_Identifer__c = null OR '+
                              'Name_for_encryption_student__c = null OR '+
                              'Highest_School_Level_Comp_Identifier__c = null OR  '+
                              'PersonBirthdate = null OR  '+
                              'Salutation = null OR  '+
                              'State_Identifier__c = null OR  '+
                              'Address_building_property_name__c = null OR  '+
                              'Address_flat_unit_details__c = null OR  '+
                              'Address_street_number__c = null OR  '+
                              'Address_street_name__c = null OR  '+
                              'Year_Highest_Education_Completed__c = null OR  '+
                              'Labour_Force_Status_Identifier__c = null OR  '+
                              'Sex__c = null OR '+                              
                              '(Highest_School_Level_Comp_Identifier__c = \'02\' AND At_School_Flag__c != \'N\') OR ' +
                              'Year_Highest_Education_Completed__c = \'@@@@\' OR ' +
                              '(Highest_School_Level_Comp_Identifier__c = \'09\' OR Highest_School_Level_Comp_Identifier__c = \'10\' OR Highest_School_Level_Comp_Identifier__c = \'11\' OR  Highest_School_Level_Comp_Identifier__c = \'12\') OR '+
                              '(USI_Status__c != \'VERIFIED\' AND validationState =  \'Victoria\') OR ' +
                              '(validationState =  \'Victoria\' AND Labour_Force_Status_1__c != null)' +
                              ' )'; */
        
        List<Account> studentList = Database.query(query);
        
        Set<String>otherLangCodes = new Set<String>{'1201','9700','9701','9702','9799'};

        String validationState = [Select Validation_State__c From NAT_Validation_Log__c Where id = : logId].Validation_State__c;
        
        
        for(Account s : studentList) {
            
            NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = s.Id, Nat_File_Name__c = 'NAT00080',Parent_Record_Last_Modified_Date__c = s.LastModifiedDate);
            NAT_Validation_Event__c event2 = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = s.Id, Nat_File_Name__c = 'NAT00085',Parent_Record_Last_Modified_Date__c = s.LastModifiedDate);
            
            //NAT0080 Validations

            if(s.Student_Identifer__c == null)
                event.Error_Message__c = 'Student Identifier cannot be blank';
                
            /*Validation rules prevents from Disablity Type to be null */
            else if(s.Disability_Flag_0__c == 'Y' && s.Disability_Type__c == null)
                event.Error_Message__c = 'Disability Types must be populated when Disability Flag is YES';
                
            
            /*Cannot be Blank as name is mandatory on the student record */  
            else if(s.Name_for_encryption_student__c == null)
                event.Error_Message__c = 'Name for encryption cannot be blank';
                
            //NAT00085
            else if(s.Salutation == null) 
                event2.Error_Message__c = 'Title/Salutation cannot be blank';

            else if(s.State_Identifier__c == null)
                event2.Error_Message__c = 'State Identifier Cannot Be Blank';
                
            
            else if(s.Highest_School_Level_Comp_Identifier__c == null)
                event.Error_Message__c = 'Highest School Level Completed Identifier cannot be blank';
                
            
            else if(s.PersonBirthdate == null) 
                event.Error_Message__c = 'Student Birthdate cannot be blank';                
           
             
            else if(s.Highest_School_Level_Comp_Identifier__c == '02' && s.At_School_Flag__c != 'N') 
                event.Error_Message__c = 'No – the student is not attending secondary school';     
              
            
            else if(s.Address_building_property_name__c == null && s.Address_flat_unit_details__c == null && s.Address_street_number__c == null && s.Address_street_name__c == null)
                event.Error_Message__c = 'Address Cannot Be Blank';
              
            
            
            else if((s.Highest_School_Level_Comp_Identifier__c == '09' || s.Highest_School_Level_Comp_Identifier__c == '10' || s.Highest_School_Level_Comp_Identifier__c == '11' || s.Highest_School_Level_Comp_Identifier__c == '12') && GlobalUtility.formatAgeFromDateOfBirth(s.PersonBirthDate) < 10)
                event.Error_Message__c = 'For Age Group < 10, Highest School Level Completed Identifier cannot be 09,10,11,12';
                
            
            else if(s.Year_Highest_Education_Completed__c == null)
                event.Error_Message__c = 'Year High Education Completed cannot be blank';
                
            
            else if( s.Year_Highest_Education_Completed__c != '@@@@' && Integer.valueof(s.Year_Highest_Education_Completed__c) < (s.PersonBirthdate).year() + 5 )
                    event.Error_Message__c = 'Birthdate + 5 Years cannot be less than Year High Education Completed';
                    
           
           
            else if (s.Year_Highest_Education_Completed__c != '@@@@' && (Integer.valueof(s.Year_Highest_Education_Completed__c) > s.PersonBirthdate.year() + 5 ) && (Integer.valueof(s.Year_Highest_Education_Completed__c) < s.PersonBirthdate.year() + 10 ) )
                    event.Error_Message__c = 'Year High Education Completed needs to be consistent with Date Of Birth';
                    
            
            else if(s.Sex__c == null)
                event.Error_Message__c = 'Gender cannot be blank';
                
            else if(s.Labour_Force_Status_Identifier__c == null)
                event.Error_Message__c = 'Labour Status Identifier cannot be blank';
               
            else if(s.Proficiency_in_Spoken_English_Identifier__c == null && !(otherLangCodes.contains(s.Main_Language_Spoken_at_Home_Identifier__c)))
                event.Error_Message__c = 'Main Language spoken at home identifier must equal to 1201,9700,9701,9702,9799 when proficiency in spoken english identifier is blank';    
            
            else if(s.Proficiency_in_Spoken_English_Identifier__c != null && (otherLangCodes.contains(s.Main_Language_Spoken_at_Home_Identifier__c)))
                event.Error_Message__c = 'Main Language spoken at home identifier must not equal to 1201,9700,9701,9702,9799 when proficiency in spoken english identifier is populated';
                
            else if(s.Learner_Unique_Identifier__c != null && s.Learner_Unique_Identifier__c.length() != 10)
                event.Error_Message__c = 'Learner Unit Identifier Must Be 10 Characters.';

            else if(s.USI_Status__c != 'VERIFIED' && validationState == 'Victoria') 
                event.Error_Message__c = 'This student has an un-validated or missing USI';
                
            else if(validationState == 'Victoria' && s.Labour_Force_Status_1__c != null) {
                
                List<Labour_Force_Status_NAT__c> lfs = Labour_Force_Status_NAT__c.getall().values();
            
                for(Labour_Force_Status_NAT__c l : lfs ) {
                    
                    if(s.Labour_Force_Status_1__r.Code__c == l.Code__c) {
                        
                        if(s.Client_Occupation_Identifier__c == null) {
                            event.Error_Message__c = 'If the student is employed, Client Occupation Identifier must not be blank';
                            
                        }
                        
                        else if(s.Client_Industry_of_Employment__c == null) {
                            event.Error_Message__c = 'If the student is employed, Client Industry of Employment must not be blank';
                            
                        }
                    }                   
                }
            }

            //WA Specific only
            else if(validationState == 'Western Australia' && s.Address_Dwelling_Type_Identifier__c == null) 
                    event.Error_Message__c = 'Address Dwelling Type Identifier is a mandatory data element for a street address and must not be left blank';
            else if(validationState == 'Western Australia' && s.At_School_Flag__c == 'Y' )
                    event.Error_Message__c = 'Education Identifier must not be blank';
              

            
            
            
            if(event.Error_Message__c != null && event.Error_Message__c != '' )
               errorEventList.add(event);
            if(event2.Error_Message__c != null && event2.Error_Message__c != '')
               errorEventList.add(event2);
                
        
        }
        
        insert errorEventList;

        return studentList.size();
    
    }

    public static Integer validateNAT00090(String logId, String query) {
      
      List<String>splitter = new List<String>();
        
      Integer recordCounter = 0;

      query = query +' AND Disability_Type__c != null ';

      List<Account> studentList = Database.query(query);


      for(Account a : studentList) {
            if(a.Disability_Flag_0__c == 'Y') {
                  splitter = a.Disability_Type__c.Split(';');
                      for(String s: splitter)
                        recordCounter = recordCounter + 1;
                        
            }
      }

      return recordCounter;
    }

    public static Integer validateNAT00100(String logId, String query) {

         List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();

         List<String>splitter = new List<String>();       
        
         String validationState = [Select Validation_State__c From NAT_Validation_Log__c Where id = : logId].Validation_State__c;

         query = query +' AND Prior_Achievement_Type_s__c != null ';

         Integer recordCounter = 0;

         List<Account> studentList = Database.query(query);

         for(Account a :studentList) {
             NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = a.Id, Nat_File_Name__c = 'NAT00100',Parent_Record_Last_Modified_Date__c = a.LastModifiedDate);
                    if(a.Prior_Achievement_Flag__c == 'Y') {
                        splitter = a.Prior_Achievement_Type_s__c.Split(';');
                            for(String s: splitter)
                               recordCounter = recordCounter + 1;
                        
                    }
                   
                    
                    if(validationState == 'Victoria' && a.Prior_Achievement_Flag__c == 'Y' && a.Prior_Education_Achievement_Recognition__c == null) {
                       event.Error_Message__c = 'Prior Achievement Recognition must be populated when Prior Achievement flag is YES';
                        
                    }
                    
               if(event.Error_Message__c != null)
                   errorEventList.add(event);
                    
        }

        insert errorEventList;

        return recordCounter;

    }
    
      
    public static Integer validateNAT00120(String logId, String query) {
         
        List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();

        Set<String>nswAllowedOutcomes = new Set<String>{'20','30','40','51','53','60','66','70'};
        Set<String>waAllowedOutcomes = new Set<String>{'51','52','53','54','60'};

        String WAqueryLogic = ' OR Enrolment__r.Enrolled_School_Identifier__c = null OR ' +
                              'Enrolment__r.Training_Type_Identifier__c = null OR ' +
                              'Enrolment__r.Enrolment_Category_Identifier__c = null OR ' +
                              'Enrolment__r.Enrolment_Category_Identifier__c = \'X - Exam Only\' OR ' +
                              'Reportable_Tuition_Fee__c = null OR ' +
                              'WA_Resource_Fee__c = null OR ' +
                              '((Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'51\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'52\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'53\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'54\') AND ' +
                              '(Delivery_Mode_Identifier__c =  \'90\' AND AVETMISS_National_Outcome__c != \'60\')) ';

        String collectionYear = [Select Collection_Year__c From NAT_Validation_Log__c Where id = : logId].Collection_Year__c;
        String validationState = [Select Validation_State__c From NAT_Validation_Log__c Where id = : logId].Validation_State__c;


        Integer recordCounter = 0;

       // for(Enrolment_Unit__c eu1 : database.query(query))
       //      recordCounter = recordCounter + 1;
         
          query = query +' AND (Delivery_Location_Identifier__c = null OR ' +
                         '      Scheduled_Hours__c = null OR ' +
                         '      Enrolment__r.Qualification_Identifier__c = null OR ' +
                         '      Enrolment__r.Start_Date__c = null OR ' +
                         '      Enrolment__r.End_Date__c = null OR ' +
                         '      (AVETMISS_National_Outcome__c = null AND Enrolment__r.Delivery_location_State__c != \'Victoria\' ) OR ' +
                         '      Commencing_Course_Identifier__c = null OR ' +
                         '      Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c = null OR ' +
                         '      Purchasing_Contract_Identifier__c = null OR ' +
                         '      VET_in_Schools__c = null OR ' +
                         '      Line_Item_Qualification_Unit__r.Nominal_Hours__c = null OR ' +
                         '      Study_Reason_Identifier__c = null OR ' +
                         '      (Enrolment__r.Victorian_Commencement_Date__c = null AND Enrolment__r.Delivery_location_State__c =  \'Victoria\' ) ' ;

          //add WA query conditions             
          if(validationState == 'Western Australia')
            query = query +  WAqueryLogic + ' )';

          else
            query = query + ' )';           
                         
         
      
         List<Enrolment_Unit__c> euList = Database.query(query);
         
         for(Enrolment_Unit__c eu : euList) {
             
              NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = eu.Id, Nat_File_Name__c = 'NAT00120',Parent_Record_Last_Modified_Date__c = eu.LastModifiedDate);
        
             if(eu.Delivery_Location_Identifier__c  == null)
                    event.Error_Message__c = 'Training Organisation Delivery Location Identifier cannot be blank';
                
                
             else if(eu.Scheduled_Hours__c == null) 
                     event.Error_Message__c = 'Scheduled Hours cannot be blank';
                   
             
                
             else if(eu.Enrolment__r.Qualification_Identifier__c == null)
                     event.Error_Message__c = 'Qualification Identifier cannot be blank';
                    
                
             else if(eu.Enrolment__r.Start_Date__c == null || eu.Enrolment__r.End_Date__c == null )   
                     event.Error_Message__c = 'Enrolment activity Start date Or End date  cannot be blank';        
                 
             else if(eu.AVETMISS_National_Outcome__c == null && eu.Enrolment__r.Delivery_location_State__c != 'Victoria')   
                     event.Error_Message__c = 'Outcome Identifier cannot be blank';
               
             else if(eu.Commencing_Course_Identifier__c == null) 
                     event.Error_Message__c = 'Commencing Course Identifier cannot be blank';
                    
             else if(eu.Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c == null)  
                     event.Error_Message__c = 'Funding Source Identifier cannot be blank';
               
             else if(eu.Purchasing_Contract_Identifier__c == null)   
                     event.Error_Message__c = 'Purchasing Contract Identifier cannot be blank';
             /*never null based of SFDC setup
             else if(eu.VET_in_Schools__c == null) 
                     event.Error_Message__c = 'VET in Schools Flag cannot be blank';
            */    
             else if(eu.Line_Item_Qualification_Unit__r.Nominal_Hours__c == null)
                     event.Error_Message__c = 'Line Item Qualification Nominal Hours Cannot be blank';
                    
             else if(eu.Enrolment__r.Victorian_Commencement_Date__c == null && eu.Enrolment__r.Delivery_location_State__c == 'Victoria')
                     event.Error_Message__c = 'Victorian Commencment Date (Located on Enrolment) cannot be blank';
                    
             else if(eu.Study_Reason_Identifier__c == null)      
                     event.Error_Message__c = 'Study Reason Identifier cannot be blank';

             else if(eu.Victorian_Tuition_Rate__c != null) {
                     if(String.ValueOf(Integer.ValueOf(eu.Victorian_Tuition_Rate__c)).length() > 4)
                     event.Error_Message__c = 'Victoria Tuition Rate cannot be greater than 4 digits';
             }

             else if((validationState == ' New South Wales') && !(nswAllowedOutcomes.contains(eu.AVETMISS_National_Outcome__c)))
                    event.Error_Message__c = 'New South Wales National Outcome Can only be 20,30,40,51,53,60,66,70';


             //WA specific


             else if(eu.Enrolment__r.Enrolled_School_Identifier__c == null)
                    event.Error_Message__c = 'Enrolled School Identifier cannot be blank';
             else if(eu.Enrolment__r.Training_Type_Identifier__c == null)
                    event.Error_Message__c = 'Training Type Identifier cannot be blank';

             else if(eu.Delivery_Mode_Identifier__c =='90' &&  !(waAllowedOutcomes.contains(eu.AVETMISS_National_Outcome__c)))
                    event.Error_Message__c = 'Outcome Identifier can only be 51,52,53,54,60 When Delivery Mode Identifier is 90';
           
             
             else if(eu.Enrolment__r.Enrolment_Category_Identifier__c == null)
                    event.Error_Message__c = 'Enrolment Category Identifier cannot be blank';

             else if(eu.Enrolment__r.Enrolment_Category_Identifier__c == 'X - Exam Only') {
                    if(eu.AVETMISS_National_Outcome__c == '70' ||  String.ValueOf(eu.End_Date__c.year()) != collectionYear)
                      event.Error_Message__c = ' Activity End Date must be in this collection year and the Outcome Identifier - National must not be 70';


             }

             else if(eu.Reportable_Tuition_Fee__c == null)
                   event.Error_Message__c = 'Reportable Tuition Fee cannot be blank';

             else if(eu.WA_Resource_Fee__c == null)
                   event.Error_Message__c = 'WA Resource Fee cannot be blank';

            
                    
             if(event.Error_Message__c != null)     
               errorEventList.add(event);


             

                
                
         }
         
         insert errorEventList;
         
        
           
         return recordCounter;
         
    }
    
    public static Integer validateNAT00130(String logId, String query) {
          
         Integer totalRecordCount = 0;

         List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
         
         String validationState = [Select Validation_State__c From NAT_Validation_Log__c Where id = : logId].Validation_State__c;
         
         if(validationState != 'Victoria') {            
             
             String awardQuery = 'SELECT ID, Enrolment__r.LastModifiedDate,Education_Training_International_Flag__c, Year_Program_Completed__c, Qualification_Issued__c, Enrolment__r.Training_Organisation_Identifier__c,'+
                                 'Enrolment__r.Student_Identifier__c, Enrolment__r.Start_Date__c, Award_ID_CASIS__c, Date_Issued__c, Enrolment__r.Parent_Qualification_Code__c,Enrolment__c  FROM Awards__c WHERE RecordType.Name = \'approved award\' '+  
                                 'AND Award_Type__c = \'Certificate\' AND Qualification_Issued__c = \'Y\' AND Enrolment__c in (Select Enrolment__c ' + query + ')';
             
             List<Awards__c> awardList = Database.query(awardQuery);
             
             for(Awards__c award : awardList) {
             
                 NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = award.Enrolment__c, Nat_File_Name__c = 'NAT00130',Parent_Record_Last_Modified_Date__c = award.Enrolment__r.LastModifiedDate);
    
                 if(award.Enrolment__r.Training_Organisation_Identifier__c == null)
                     event.Error_Message__c = 'Training Organisation Identifier cannot be blank';
                 else if(award.Enrolment__r.Parent_Qualification_Code__c == null) 
                     event.Error_Message__c = 'Qualification/Course Identifier cannot be blank';
                 else if(award.Enrolment__r.Student_Identifier__c == null) 
                     event.Error_Message__c = 'Student Identifier cannot be blank';
                
                 if(event.Error_Message__c != null)
                    errorEventList.add(event);  
             
             
             }
             
             insert errorEventList;
             
             totalRecordCount = awardList.size();
             
             return totalRecordCount;
         
         
         }
         
         else {
             
              String vicquery =  'SELECT id,lastmodifieddate, Name, Training_Organisation_Identifier__c,Parent_Qualification_Code__c, Qualification_Identifier__c, Student_Identifier__c, Delivery_location_State__c, Victorian_Commencement_Date__c FROM Enrolment__c WHERE Id IN  (Select Enrolment__c ' + query + ')';
              
              List<Enrolment__c> eList = Database.query(vicquery);
         

             for(Enrolment__c e : eList) {

                 NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = logid,Record_Id__c = e.Id, Nat_File_Name__c = 'NAT00130',Parent_Record_Last_Modified_Date__c = e.LastModifiedDate);
    
                 if(e.Training_Organisation_Identifier__c == null)
                     event.Error_Message__c = 'Training Organisation Identifier cannot be blank';
                 else if(e.Parent_Qualification_Code__c == null) 
                     event.Error_Message__c = 'Qualification/Course Identifier cannot be blank';
                 else if(e.Student_Identifier__c == null) 
                     event.Error_Message__c = 'Student Identifier cannot be blank';
                 else if(e.Victorian_Commencement_Date__c == null && e.Delivery_location_State__c == 'Victoria') 
                     event.Error_Message__c = 'Victorian Commencment Date (Located on Enrolment) cannot be blank';

            
                 if(event.Error_Message__c != null)
                    errorEventList.add(event);
                   
            }
            
                insert errorEventList;
                
                totalRecordCount = eList.size();
                
                return totalRecordCount;
         
         }
         
    
    }



}