/*This class is used for assigning sequence numbers for Qualification Units
*/

 


public with sharing class BlackboardQUSequencingController {

    public List<Qualification_Unit__c> q_units {get;set;} 

    public BlackboardQUSequencingController() {

        q_units = [Select name, 
                          Sequence_Number__c,
                         /* Unit_is_Approved_for_Blackboard_Delivery__c, */
                          Unit_Name__c,
                          Unit_Code__c 
                   From Qualification_Unit__c WHERE Unit__r.RecordType.Name = 'Unit Of Competency' AND Unit__r.RecordType.sObjectType = 'Unit__c' AND Qualification__c = : ApexPages.currentPage().getParameters().get('id') LIMIT 1000];    
        
    }
    
    /*Save Qualification Units into DB */
    public PageReference save() {

        try {
            update q_units;
        } catch(Exception e) { 

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            ApexPages.addMessage(myMsg);

            return null;}

        return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));

    }
    /*Cancel and go back to the qualification */
    public PageReference cancel() {

        return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));

    }
}