public class USI_WebService {
    
    public static USI_Configuration__c usiConfig = USI_Configuration__c.getInstance('Default');
    
    public class internalExceptionObject {
        String USI;
        String message;
        String status;
    }
    
    public static HTTPResponse createUSI(String body, String tOrgId) {
        
        Http http = new http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setMethod('POST');
        if(tOrgId == usiConfig.CAIT_Org_Id__c ) {
            req.setEndpoint(usiConfig.Create_USI_Endpoint_CAIT__c);
        }
        if(torgId == usiConfig.CAEI_Org_Id__c ) {
            req.setEndpoint(usiConfig.Create_USI_Endpoint_CAEI__c);
        }
        req.setBody(body);
        req.setTimeout(Integer.valueOf(usiConfig.Timeout__c));
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            internalExceptionObject ie = new internalExceptionObject();
            ie.USI = '';
            ie.message = e.getMessage();
            ie.status = 'InternalFailure';
            res.setBody(json.serialize(ie));
        } 
        
        return res;
        
    }
    
    public static HttpResponse verifyUSI(String body, String tOrgId) {
                
        Http http = new http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setMethod('POST');
		if(tOrgId == usiConfig.CAIT_Org_Id__c ) {
            req.setEndpoint(usiConfig.Verify_USI_Endpoint_CAIT__c);
        }
        if(torgId == usiConfig.CAEI_Org_Id__c ) {
            req.setEndpoint(usiConfig.Verify_USI_Endpoint_CAEI__c);
        }        
        req.setBody(body);
        req.setTimeout(Integer.valueOf(usiConfig.Timeout__c));
        
        try {
            res = http.send(req); 
        }
        catch (exception e) {
            internalExceptionObject ie = new internalExceptionObject();
            ie.USI = '';
            ie.message = e.getMessage();
            ie.status = 'InternalFailure';
            res.setBody(json.serialize(ie));
        } 
        
        return res;
        
    }
}