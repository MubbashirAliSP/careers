/*If Qualification units exist, redirect to normal page
*/

public with sharing class QualificationRedirectControllerExt {
    
     
    
        public PageReference action() {
            
            
            List<Qualification_Unit__c> qunits = [Select id From Qualification_Unit__c WHERE Qualification__c = :ApexPages.currentPage().getParameters().get('Id')];
            
            List<RecordType> recT = new List<RecordType>([SELECT Id, name FROM RecordType where name = 'Qualification' And SObjectType = 'Qualification__c' LIMIT 1]);
            
            system.debug('aaa' + ApexPages.currentPage().getParameters().get('Id') + ' '+ ApexPages.currentPage().getParameters().get('RecordType') + ' ' + ApexPages.currentPage().getParameters().get('newId'));
            // non qualification recordtypes, should be skipped for qual units creation
            if(ApexPages.currentPage().getParameters().get('RecordType') != recT.get(0).Id && ApexPages.currentPage().getParameters().get('newId') != null) {
                                        
                     PageReference pageAccount = new PageReference('/' + ApexPages.currentPage().getParameters().get('newId'));
                     pageAccount.setRedirect(true);
                     return pageAccount;
                                        
            }
            
            
            
            if(ApexPages.currentPage().getParameters().get('newId') != null)
                return new PageReference('/apex/QualificationUnitsAllocation?id=' + ApexPages.currentPage().getParameters().get('newId') );
                
            
                    
                
                if(qunits.size() > 0) {
                    
                 PageReference pageAccount = new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
                 pageAccount.setRedirect(true);
                 return pageAccount;
                    
                    
                }   
                else {
                    
                    
                    return new PageReference('/apex/QualificationUnitsAllocation?id=' + ApexPages.currentPage().getParameters().get('Id') );
            
                }
            
            
            
            return null;
            
            
        }
        
        @isTest static void testRedirectWithQual() {
            
            Account acc = new Account();
            Account accTraining = new Account();
            ANZSCO__c anz = new ANZSCO__c();
            Field_of_Education__c fld = new Field_of_Education__c();
            Qualification__c qual = new Qualification__c();
            Qualification__c qual2 = new Qualification__c();
            Unit__c unit = new Unit__c();
            Location_Loadings__c loading = new Location_Loadings__c(); 
            Qualification_Unit__c qunit = new Qualification_Unit__c();
            Qualification_Unit__c qunit2 = new Qualification_Unit__c();
            Enrolment__c enrl = new Enrolment__c();
            Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
            Funding_Source__c fSource = new Funding_Source__c();
            Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
            Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
            Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
            State__c state = new State__c();
            Country__c country = new Country__c();
            Country__c countryAU = new Country__c();
            Locations__c parentLoc = new Locations__c();
            Locations__c loc = new Locations__c();
            Locations__c loc2 = new Locations__c();
            Attendance_Types__c attend = new Attendance_Types__c();
            Enrolment_Unit__c eu = new Enrolment_Unit__c();
            Results__c res = new Results__c();
            Unit__c un = new Unit__c();
            Field_of_Education__c fe = new Field_of_Education__c();  
            Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
            Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
            
            
            Test.startTest();
            
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            //countryAU = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createState(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
            qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() + 365;
            enrl.Delivery_Location__c = loc.id;
            //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation start
            //enrl.Enrolment_Status__c = 'Completed';
            enrl.Enrolment_Status__c = 'Active (Commencement)'; 
            //MAM 04/08/2014 end
            
           
            //enrl.Overseas_Country__c = null;
            insert enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            eus.Census_Date__c = system.today();
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;
            
            
            
            PageReference testPage = Page.QualificationUnitsAllocationRedirect;
            Test.setCurrentPage(testPage);
            testPage.getParameters().put('Id', qual.Id);
            QualificationRedirectControllerExt controller = new QualificationRedirectControllerExt();
            
            controller.action();
            
            Test.stopTest();
            
                
            
        }
        
            @isTest static void testRedirectWithOutQual() {
                
              Test.startTest();
              
              PageReference testPage = Page.QualificationUnitsAllocationRedirect;
              Test.setCurrentPage(testPage);
              QualificationRedirectControllerExt controller = new QualificationRedirectControllerExt();
              
              controller.action();

             Test.stopTest();

            
            }
            
            @isTest static void testRedirectNonQualRecordType() {
              
                Account acc = new Account();
                Account accTraining = new Account();
                ANZSCO__c anz = new ANZSCO__c();
                Field_of_Education__c fld = new Field_of_Education__c();
                Qualification__c qual = new Qualification__c();
                Qualification__c qual2 = new Qualification__c();
                Unit__c unit = new Unit__c();
                Location_Loadings__c loading = new Location_Loadings__c(); 
                Qualification_Unit__c qunit = new Qualification_Unit__c();
                Qualification_Unit__c qunit2 = new Qualification_Unit__c();
                Enrolment__c enrl = new Enrolment__c();
                Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
                Funding_Source__c fSource = new Funding_Source__c();
                Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
                Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
                Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
                State__c state = new State__c();
                Country__c country = new Country__c();
                Country__c countryAU = new Country__c();
                Locations__c parentLoc = new Locations__c();
                Locations__c loc = new Locations__c();
                Locations__c loc2 = new Locations__c();
                Attendance_Types__c attend = new Attendance_Types__c();
                Enrolment_Unit__c eu = new Enrolment_Unit__c();
                Results__c res = new Results__c();
                Unit__c un = new Unit__c();
                Field_of_Education__c fe = new Field_of_Education__c();  
                Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
                Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
                
                  Test.startTest();
                  
                    acc = TestCoverageUtilityClass.createStudent();
                    accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                    anz = TestCoverageUtilityClass.createANZSCO();
                    fld = TestCoverageUtilityClass.createFieldOfEducation();
                    qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                    unit = TestCoverageUtilityClass.createUnit();
                    qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                    country = TestCoverageUtilityClass.createCountry();  
                    //countryAU = TestCoverageUtilityClass.queryAustralia();
                    state = TestCoverageUtilityClass.createState(country.Id);
                    loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                    fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                    pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
                    conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                    lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
                    lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                    parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
                    loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
                    loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
                    qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
                    del = TestCoverageUtilityClass.createDeliveryModeType();
                    attend = TestCoverageUtilityClass.createAttendanceType();
                    enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
                    enrl.Predominant_Delivery_Mode__c = del.Id; 
                    enrl.Type_of_Attendance__c = attend.Id;
                    enrl.Start_Date__c = system.today() - 365;
                    enrl.End_Date__c = system.today() + 365;
                    enrl.Delivery_Location__c = loc.id;
                    //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation start
                    //enrl.Enrolment_Status__c = 'Completed';
                    enrl.Enrolment_Status__c = 'Active (Commencement)'; 
                    //MAM 04/08/2014 end
                    
                    //enrl.Overseas_Country__c = null;
                    insert enrl;
                    
                    res = TestCoverageUtilityClass.createResult(state.Id);
                    un = TestCoverageUtilityClass.createUnit();
                    eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
                    eu.Start_Date__c = Date.TODAY() - 25;
                    eu.End_Date__c = Date.TODAY() - 1;
                    update eu;
                    fe = TestCoverageUtilityClass.createFieldOfEducation();
                    eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
                    eus.Census_Date__c = system.today();
                    insert eus;
                    res = TestCoverageUtilityClass.createResult(state.Id);
                
                    Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
                    
                   insert c;
                  
                   PageReference testPage = Page.QualificationUnitsAllocationRedirect;
                   Test.setCurrentPage(testPage);
                   testPage.getParameters().put('Id', qual2.Id);
                   testPage.getParameters().put('RecordType', qual2.RecordTypeId);
                   testPage.getParameters().put('newId', qual2.Id);
                   QualificationRedirectControllerExt controller = new QualificationRedirectControllerExt();
                
                   controller.action();
                   Test.stopTest();

            
            }

}