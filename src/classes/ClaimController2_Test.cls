@isTest
private class ClaimController2_Test {
	
    @istest(seeAllData=true) static void natGenerateNSWTest() {
    
        string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        
        System.runAs(u) {
    
            Set<String>ContractsCodes = new Set<String>();
            Set<Id> enrolmentIds = new Set<Id>();
	        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
	        Funding_Source__c fSource = new Funding_Source__c();
	        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();            
            Account trainingOrgNew = [SELECT Id,Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account where Name = 'Careers Australia Institute Of Training' AND RecordType.Name = 'Training Organisation' LIMIT 1];
            
            for(Purchasing_Contracts__c pur : [Select Id, Name From Purchasing_Contracts__c LIMIT 5])
                ContractsCodes.add(pur.Id);
            
            for(Enrolment__c enrl : [Select id, Name,
                                            Student__r.FirstName,
                                            Student__r.LastName,
                                            Student__r.PersonBirthDate,
                                            Student__r.Full_Student_Name__c,
                                            Student__r.Student_Identifer__c,
                                            Student__r.IsPersonAccount,
                                            Qualification__r.Name,
                                            Contract_Code__c,
                                            No_of_Enrolment_Claims__c,
                                            of_Enrolment_Completed__c
                                            
                                            FROM Enrolment__c
                                            WHERE Delivery_Location_State__c = : 'New South Wales' AND
                                                  NSW_Claiming_Allowed__c = 'Yes' AND
                                                  Contract_Code__c in :ContractsCodes
                                                 
                                                  LIMIT 5
                                                  ]) {
                                                  
                                                    enrolmentIds.add(enrl.Id);
                                                  

      
                                             }
            
            String rto = trainingOrgNew.Id;
            NATValidateOutCome newOutcome = NatValidator.validateNAT00020(ContractsCodes, '2012', 'Queensland',rto);
            NATValidateOutCome newOutcome2 = NatValidator.validateNAT00030(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome3 = NatValidator.validateNAT00060(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome4 = NatValidator.validateNAT00080(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome5 = NatValidator.validateNAT00085(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome6 = NatValidator.validateNAT00090(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome7 = NatValidator.validateNAT00100(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome8 = NatValidator.validateNAT00120(ContractsCodes, '2012', 'Queensland');
            NATValidateOutCome newOutcome9 = NatValidator.validateNAT00130(ContractsCodes, '2012', 'Queensland');


       
            Test.startTest();
            
            NATGeneratorUtility.formatCheckBox(false);
            NATGeneratorUtility.formatCheckBox(true);
            NATGeneratorUtility.formatHours('a', 1.0);
            NATGeneratorUtility.formatHours(null, 1.0);
            NATGeneratorUtility.formatHours('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('a', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile(null, 1.0);
            
           
            PageReference testPage = Page.NSWClaiming;
            PageReference testPage2 = Page.NatGeneratorValidation;
            testPage.getParameters().put('rto', trainingOrgNew.id);
            testPage.getParameters().put('state', 'New South Wales');
            testPage.getParameters().put('de', 'false');
            testPage.getParameters().put('claim', 'Dummy');

            Test.setCurrentPage(testPage);

      		            
            ClaimController2 controller = new ClaimController2();        
 			controller.includeUnclaimed = true;
            controller.claimType = 'Commencement Claim';
            controller.selectedFile = 'DummyFile';
            controller.fundingSourceSearch = '';
            controller.ContractCodeSearch = '';            
            controller.filterPurchasingContract();         
            controller.dummyClaimNumber.Claim_Date__c = system.today();           
            controller.NSWBackButton();       
            controller.NSWSearch();       
            controller.NSWNextButton();         
            controller.doClaim();      
            controller.CancelButton();          
            controller.NextToClaiming();

            
            Test.stopTest();
        
        }
        
    } 


    @istest(seeAllData=true) static void FilterwithValue() {
    
        string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        
        System.runAs(u) {
    
            Set<String>ContractsCodes = new Set<String>();
            Set<Id> enrolmentIds = new Set<Id>();
	        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
	        Funding_Source__c fSource = new Funding_Source__c();
	        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();            
            Account trainingOrgNew = [SELECT Id,Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account where Name = 'Careers Australia Institute Of Training' AND RecordType.Name = 'Training Organisation' LIMIT 1];
            
            for(Purchasing_Contracts__c pur : [Select Id, Name From Purchasing_Contracts__c LIMIT 5])
                ContractsCodes.add(pur.Id);
            
            for(Enrolment__c enrl : [Select id, Name,
                                            Student__r.FirstName,
                                            Student__r.LastName,
                                            Student__r.PersonBirthDate,
                                            Student__r.Full_Student_Name__c,
                                            Student__r.Student_Identifer__c,
                                            Student__r.IsPersonAccount,
                                            Qualification__r.Name,
                                            Contract_Code__c,
                                            No_of_Enrolment_Claims__c,
                                            of_Enrolment_Completed__c
                                            
                                            FROM Enrolment__c
                                            WHERE Delivery_Location_State__c = : 'New South Wales' AND
                                                  NSW_Claiming_Allowed__c = 'Yes' AND
                                                  Contract_Code__c in :ContractsCodes
                                                 
                                                  LIMIT 5
                                                  ]) {
                                                  
                                                    enrolmentIds.add(enrl.Id);
                                                  

      
                                             }
            
            String rto = trainingOrgNew.Id;

            Test.startTest();
            
            NATGeneratorUtility.formatCheckBox(false);
            NATGeneratorUtility.formatCheckBox(true);
            NATGeneratorUtility.formatHours('a', 1.0);
            NATGeneratorUtility.formatHours(null, 1.0);
            NATGeneratorUtility.formatHours('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('a', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile(null, 1.0);
            
           
            PageReference testPage = Page.NSWClaiming;
            PageReference testPage2 = Page.NatGeneratorValidation;
            testPage.getParameters().put('rto', trainingOrgNew.id);
            testPage.getParameters().put('state', 'New South Wales');
            testPage.getParameters().put('de', 'false');
            testPage.getParameters().put('claim', 'Dummy');

            Test.setCurrentPage(testPage);

      		            
            ClaimController2 controller = new ClaimController2();        
   			controller.includeUnclaimed = false;          
            controller.claimType = 'Commencement Claim';
            controller.selectedFile = 'DummyFile';
            controller.fundingSourceSearch = 'Dummy1';
            controller.ContractCodeSearch = 'Dummy1';            
            controller.filterPurchasingContract();         
            controller.dummyClaimNumber.Claim_Date__c = system.today();                  
   
            Test.stopTest();
        
        }
        
    } 	

     @istest(seeAllData=true) static void FilterwithFundingValue() {
    
        string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        
        System.runAs(u) {
    
            Set<String>ContractsCodes = new Set<String>();
            Set<Id> enrolmentIds = new Set<Id>();
	        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
	        Funding_Source__c fSource = new Funding_Source__c();
	        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();            
            Account trainingOrgNew = [SELECT Id,Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account where Name = 'Careers Australia Institute Of Training' AND RecordType.Name = 'Training Organisation' LIMIT 1];
            
            for(Purchasing_Contracts__c pur : [Select Id, Name From Purchasing_Contracts__c LIMIT 5])
                ContractsCodes.add(pur.Id);
            
            for(Enrolment__c enrl : [Select id, Name,
                                            Student__r.FirstName,
                                            Student__r.LastName,
                                            Student__r.PersonBirthDate,
                                            Student__r.Full_Student_Name__c,
                                            Student__r.Student_Identifer__c,
                                            Student__r.IsPersonAccount,
                                            Qualification__r.Name,
                                            Contract_Code__c,
                                            No_of_Enrolment_Claims__c,
                                            of_Enrolment_Completed__c
                                            
                                            FROM Enrolment__c
                                            WHERE Delivery_Location_State__c = : 'New South Wales' AND
                                                  NSW_Claiming_Allowed__c = 'Yes' AND
                                                  Contract_Code__c in :ContractsCodes
                                                 
                                                  LIMIT 5
                                                  ]) {
                                                  
                                                    enrolmentIds.add(enrl.Id);
      
                                             }
            
            String rto = trainingOrgNew.Id;

            Test.startTest();
            
            NATGeneratorUtility.formatCheckBox(false);
            NATGeneratorUtility.formatCheckBox(true);
            NATGeneratorUtility.formatHours('a', 1.0);
            NATGeneratorUtility.formatHours(null, 1.0);
            NATGeneratorUtility.formatHours('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('a', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile(null, 1.0);
            
           
            PageReference testPage = Page.NSWClaiming;
            PageReference testPage2 = Page.NatGeneratorValidation;
            testPage.getParameters().put('rto', trainingOrgNew.id);
            testPage.getParameters().put('state', 'New South Wales');
            testPage.getParameters().put('de', 'false');
            testPage.getParameters().put('claim', 'Dummy');

            Test.setCurrentPage(testPage);
            
            ClaimController2 controller = new ClaimController2();        
            
            controller.claimType = 'Commencement Claim';
            controller.selectedFile = 'DummyFile';
            controller.fundingSourceSearch = 'Dummy1';
            controller.ContractCodeSearch = '';            
            controller.filterPurchasingContract();         
            controller.dummyClaimNumber.Claim_Date__c = system.today();           

            Test.stopTest();
        
        }
        
    } 	

     @istest(seeAllData=true) static void FilterwithContractValue() {
    
        string testUser = NAT_Test_User__c.getInstance('NAT Test User ID').ID__c;
        User u = [ SELECT Id FROM User WHERE Id = : testUser ];
        
        
        System.runAs(u) {
    
            Set<String>ContractsCodes = new Set<String>();
            Set<Id> enrolmentIds = new Set<Id>();
	        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
	        Funding_Source__c fSource = new Funding_Source__c();
	        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();            
            Account trainingOrgNew = [SELECT Id,Training_Org_Identifier__c,Training_Org_Type_Identifier__c,BillingStreet,BillingCity,BillingPostalCode,BillingState,State_Identifier__c,NAT_Error_Code__c, Name, Software_Product_Name__c  FROM Account where Name = 'Careers Australia Institute Of Training' AND RecordType.Name = 'Training Organisation' LIMIT 1];
            
            for(Purchasing_Contracts__c pur : [Select Id, Name From Purchasing_Contracts__c LIMIT 5])
                ContractsCodes.add(pur.Id);
            
            for(Enrolment__c enrl : [Select id, Name,
                                            Student__r.FirstName,
                                            Student__r.LastName,
                                            Student__r.PersonBirthDate,
                                            Student__r.Full_Student_Name__c,
                                            Student__r.Student_Identifer__c,
                                            Student__r.IsPersonAccount,
                                            Qualification__r.Name,
                                            Contract_Code__c,
                                            No_of_Enrolment_Claims__c,
                                            of_Enrolment_Completed__c
                                            
                                            FROM Enrolment__c
                                            WHERE Delivery_Location_State__c = : 'New South Wales' AND
                                                  NSW_Claiming_Allowed__c = 'Yes' AND
                                                  Contract_Code__c in :ContractsCodes
                                                 
                                                  LIMIT 5
                                                  ]) {
                                                  
                                                    enrolmentIds.add(enrl.Id);
     
                                             }
            
            String rto = trainingOrgNew.Id;

            Test.startTest();
            
            NATGeneratorUtility.formatCheckBox(false);
            NATGeneratorUtility.formatCheckBox(true);
            NATGeneratorUtility.formatHours('a', 1.0);
            NATGeneratorUtility.formatHours(null, 1.0);
            NATGeneratorUtility.formatHours('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('a', 1.0);
            NAtGeneratorUtility.newFormatTxtFile('', 1.0);
            NAtGeneratorUtility.newFormatTxtFile(null, 1.0);
            
           
            PageReference testPage = Page.NSWClaiming;
            PageReference testPage2 = Page.NatGeneratorValidation;
            testPage.getParameters().put('rto', trainingOrgNew.id);
            testPage.getParameters().put('state', 'New South Wales');
            testPage.getParameters().put('de', 'false');
            testPage.getParameters().put('claim', 'Dummy');

            Test.setCurrentPage(testPage);

      		            
            ClaimController2 controller = new ClaimController2();        
            
            controller.claimType = 'Commencement Claim';
            controller.selectedFile = 'DummyFile';
            controller.fundingSourceSearch = '';
            controller.ContractCodeSearch = 'Dummy1';            
            controller.filterPurchasingContract();         
            controller.dummyClaimNumber.Claim_Date__c = system.today();           
  
            Test.stopTest();
        
        }
        
    } 	
}