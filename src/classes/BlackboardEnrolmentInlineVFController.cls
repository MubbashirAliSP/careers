/*This controller extension is supporting
 *inline VF page on Enrolment__c object, to show
 *support officers and trainers
 *created by Yuri Gribanov - CloudSherpas 7th August 2014 */


public with sharing class BlackboardEnrolmentInlineVFController {

	private final Enrolment__c enrolment;

    public List<Blackboard_User__c>bbusers {get;set;} // staff to display

    public Boolean showBlankSection {get;set;}
    public Boolean showFullSection {get;set;}
   

    public BlackboardEnrolmentInlineVFController(ApexPages.StandardController stdController) {
        this.enrolment = (Enrolment__c)stdController.getRecord();


     getTrainers();
    }

    public void getTrainers() {

       
        Set<Id>bbcIds = new Set<Id>();
        Set<Id>staffIds = new Set<Id>();

       for(Blackboard_Course_Membership__c bbcm : [Select Id, Blackboard_Course__c From Blackboard_Course_Membership__c Where Enrolment_Unit__r.Enrolment__c = : enrolment.Id])
            bbcIds.add(bbcm.Blackboard_Course__c);

       for(Blackboard_Course_Membership__c bbcm : [Select Id, Blackboard_User_Role__r.Blackboard_User__c From Blackboard_Course_Membership__c Where RecordType.DeveloperName = 'Staff' and Blackboard_Course__c in : bbcIds ])
            staffIds.add(bbcm.Blackboard_User_Role__r.Blackboard_User__c);


       


        bbusers = [Select First_Name__c, Last_Name__c From Blackboard_User__c Where id in : staffIds];

        if(bbusers.size() == 0)
            showBlankSection = true;
        if(bbusers.size() > 0)
            showFullSection = true;
       
    }

    
}