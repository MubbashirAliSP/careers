/**
 * @description Qualification Unit Trigger Handler
 * @author Janella Lauren Canlas
 * @date 27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas - Created.
 */
public with sharing class QualificationUnitTriggerHandler {
	/*
		This method updates the Qualification Unit record type based on the Unit record type
	*/
	public static void updateUnitRecordType(List<Qualification_Unit__c> quList){
		Set<Id> unitIds = new Set<Id>();
		List<Unit__c> unitList = new List<Unit__c>();
		//collect unit ids
		for(Qualification_Unit__c q : quList){
			unitIds.add(q.Unit__c);
		}
		//query related unit records
		unitList = [Select RecordType.Name, RecordTypeId, Id from Unit__c where Id IN: unitIds];
		//populate qualification unit recordtype for update
		for(Qualification_Unit__c q : quList){
			for(Unit__c  u : unitList){
				if(q.Unit__c == u.Id){
					q.Unit_Record_Type__c = u.RecordType.Name;
				}
			}
		}
	}
	/*
		This method will validate qualification units
	*/
	public static void validateQualificationUnits(List<Qualification_Unit__c> quList){
		Map<Id, Set<Id>> quMap = new Map<Id,Set<Id>>();
		Set<Id> qualificationIds = new Set<Id>();
		List<Qualification_Unit__c> qualUnitList = new List<Qualification_Unit__c>();
		for(Qualification_Unit__c q : quList){
			qualificationIds.add(q.Qualification__c);
		}
		qualUnitList = [select Id, Unit__c, Qualification__c from Qualification_Unit__c
						where Qualification__c IN: qualificationIds];
		for(Qualification_Unit__c q : qualUnitList){
			if(quMap.containsKey(q.Qualification__c)){
				Set<Id> temp = quMap.get(q.Qualification__c);
				temp.add(q.Unit__c);
				quMap.put(q.Qualification__c, temp);
			}
			else{
				quMap.put(q.Qualification__c, new Set<Id>{q.Unit__c});
			}
		}
		for(Qualification_Unit__c q : quList){
			if(quMap.containsKey(q.Qualification__c)){
				if(quMap.get(q.Qualification__c).contains(q.Unit__c)){
					q.addError('You cannot create Qualification Units with the same Unit.');
				}
			}
		}
	}
}