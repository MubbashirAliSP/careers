/**
 * @description Custom button handler for creating an LLN Resit entry requirement
 * @author Ranyel Maliwanag
 * @date 22.FEB.2016
 * @history
 *		 07.MAR.2016	Ranyel Maliwanag	- Added logic to carry across accepted scores (level 3 or higher) to the resit one
 */
public with sharing class EntryRequirementResitCX {
	public EntryRequirement__c entryRequirement {get; set;}

	public EntryRequirementResitCX(ApexPages.StandardController controller) {
		String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.EntryRequirement__c);
		queryString += 'WHERE Id = \'' + controller.getId() + '\'';
		entryRequirement = Database.query(queryString);
	}


	public PageReference evaluateRedirects() {
		Id llnERRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
		Group completeEntryRequirements = [SELECT Id FROM Group WHERE DeveloperName = 'Complete_Entry_Requirements'];

		// Create a new LLN Pending Entry Requirement
		EntryRequirement__c resitRequirement = new EntryRequirement__c(
				RecordTypeId = llnERRTID,
				ApplicationFormId__c = entryRequirement.ApplicationFormId__c,
				EnrolmentId__c = entryRequirement.EnrolmentId__c,
				ServiceCaseId__c = entryRequirement.ServiceCaseId__c,
				Status__c = 'LLN - Resit',
				OwnerId = entryRequirement.OwnerId,
				Email__c = entryRequirement.Email__c
			);
		if (String.isNotBlank(entryRequirement.Literacy_Level__c) && entryRequirement.Literacy_Level__c.right(1).isNumeric() && Integer.valueOf(entryRequirement.Literacy_Level__c.right(1)) >= 3) {
			resitRequirement.Literacy_Level__c = entryRequirement.Literacy_Level__c;
		}
		if (String.isNotBlank(entryRequirement.Numeracy_Level__c) && entryRequirement.Numeracy_Level__c.right(1).isNumeric() && Integer.valueOf(entryRequirement.Numeracy_Level__c.right(1)) >= 3) {
			resitRequirement.Numeracy_Level__c = entryRequirement.Numeracy_Level__c;
		}
		insert resitRequirement;

		// Close of the old one
		entryRequirement.Status__c = 'LLN - Closed';
		entryRequirement.LLN_Re_Sit_Offer_Accepted__c = true;
		entryRequirement.OwnerId = completeEntryRequirements.Id;
		entryRequirement.Optional__c = true;
		update entryRequirement;

		// Redirect to the new one
		PageReference result = new PageReference('/' + resitRequirement.Id);
		result.setRedirect(true);
		return result;
	}
}