global with sharing class BlackboardIntakeUnitLoadingJ1 implements Database.Batchable<sObject> {
    
    private final Id t3RecTypeId = [Select Id From RecordType Where DeveloperName = 'T3_Intake_Course' and sObjectType = 'Blackboard_Course__c'].Id;

    global String query;
    
    global BlackboardIntakeUnitLoadingJ1() {

        Date dt = System.today();
        String dateStr = DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('yyyy-MM-dd');

        query = 'Select Id,Unit_Start_Date__c,Sync_When_Start_Date_Reached__c,Intake__r.Qualification_Context__r.Context__r.Name,Unit_of_Study__r.Blackboard_Master_Course__r.Unit_Code__c From Intake_Unit__c Where (Intake__r.Qualification_Context__C != NULL AND Unit_of_Study__r.Blackboard_Master_Course__c != Null AND Blackboard_Template_Course__c = Null AND Unit_End_Date__c >=' + dateStr + ') OR (Sync_When_Start_Date_Reached__c = TRUE AND Blackboard_Template_Course__c = Null AND Intake__r.Qualification_Context__C != NULL)';
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        Map<String,String>templateCourseMap = new Map<String,String>();

        for(Blackboard_Course__c template : [Select Id, Master_Course__r.Unit_Code__c, Context__r.Name From Blackboard_Course__c Where RecordType.DeveloperName = 'T2_Template_Course' AND Student_Access_Approved__c = True AND Active__c = True LIMIT 50000]) {

            templateCourseMap.put(template.Master_Course__r.Unit_Code__c + '' + template.Context__r.Name, template.Id );
        }

        List<Blackboard_Course__c>bbcoursesForInsert = new List<Blackboard_Course__c>();
        List<Intake_Unit__c> intake_units = new List<Intake_Unit__c>();

        for(SObject s :scope) {

             Intake_Unit__c iu = (Intake_Unit__c) s;

             String mapKey = iu.Unit_of_Study__r.Blackboard_Master_Course__r.Unit_Code__c + '' +  iu.Intake__r.Qualification_Context__r.Context__r.Name;

                 if(templateCourseMap.containsKey(mapKey)) {
                    
                     if(iu.Unit_Start_Date__c <= System.today()) {

                        Blackboard_Course__c bbcourse = new Blackboard_Course__c(

                            Template_Course__c = templateCourseMap.get(mapKey),
                            Enable_Course__c = True,
                            Changed_Since_Last_Sync__c = True,
                            RecordTypeId = t3RecTypeId,
                            Intake_Unit__c = iu.Id


                        );
                         
                         iu.Blackboard_Template_Course__c = templateCourseMap.get(mapKey);
                        
                         bbcoursesForInsert.add(bbcourse);
                     }

                  
                  iu.Sync_When_Start_Date_Reached__c = True;
                  intake_units.add(iu);

                    
                 } else {
                    
                     iu.Sync_When_Start_Date_Reached__c = False;
                     intake_units.add(iu);
                    
                 }


            

         }

         insert bbcoursesForInsert;
         update intake_units;

        

    
    }
    
    global void finish(Database.BatchableContext BC) {
        
      BlackboardCourseSISConfirmSyncBatch b1 = new BlackboardCourseSISConfirmSyncBatch();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }
    
}