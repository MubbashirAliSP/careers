global without sharing class NATValidationBatch_Qualifications implements Database.Batchable<sObject>,Database.Stateful {
    
    global NAT_Validation_Log__c log;
    global String queryStringMain;
    global String endQuery;
    global Integer recordCount = 0;
    global String tOrgId;
    global String validationState;
    global Set<Id> qualificationIds = new Set<Id>();
    global String qualQuery;
    global List<NAT_Validation_Event__c> errorEventList = new List<NAT_Validation_Event__c>();
    global DateTime currentDT;

    global NATValidationBatch_Qualifications(Id logId) {
        
        
                      
    log = [ SELECT ID, Validation_State__c, Training_Organisation_Id__c, end_query__c, Log_Run_At__c, query_main__c, Collection_Year__c FROM NAT_Validation_Log__c WHERE ID =: logId];
                   
    String mainquery = 'Select qualification__c From Enrolment__c Where Id in (Select Enrolment__c ' + log.end_query__c + ')';
        
        currentDT = log.Log_Run_At__c;


    List<Enrolment__c> EnrolmentList = Database.query(mainquery);

    for(Enrolment__c enrl : EnrolmentList) {
    	qualificationIds.add(enrl.qualification__c);
    }
         
    qualQuery = 'SELECT id, Name, Qualification_Name__c, Recognition_Status_Identifier__c,'  +
                'Qualification_Category_Identifier__c, Field_Of_Education_Identifier__c, ' +
                'ANZSCO_Identifier__c, Vet_Non_Vet__c, NAT_Error_Code__c, LastModifiedDate ' +
                'FROM Qualification__c WHERE Id in : qualificationIds';
        
    }
    
    global database.querylocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(qualQuery);
            
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        try {
            for(SObject so : scope) {
                    
                Qualification__c q = (Qualification__c) so;
                recordCount++;
            
                NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = q.Id, Nat_File_Name__c = 'NAT00030',Parent_Record_Last_Modified_Date__c = q.LastModifiedDate);
            
               if(q.Qualification_Category_Identifier__c == null) 
                    event.Error_Message__c = 'Qualification Category Identifier Cannot Be Blank';
                
               else if(q.Field_Of_Education_Identifier__c == null) 
                    event.Error_Message__c = 'Field of Education Identifier Cannot Be Blank';
               
               else if(q.Recognition_Status_Identifier__c == null)
                    event.Error_Message__c = 'Recognition Status Identifier Cannot Be Blank';    
                    
                
               else if(q.ANZSCO_Identifier__c == null) 
                    event.Error_Message__c = 'ANZSCO Code Identifier Cannot Be Blank';
               
               else if(q.Recognition_Status_Identifier__c == 15.0 && q.Field_of_Education_Identifier__c > '312') 
                    event.Error_Message__c = 'If Qualification/Course Recognition Identifier is 15 then Field of Education Identifier must be <= 312';
                    
                  
               else if((q.Recognition_Status_Identifier__c == 11.0 || q.Recognition_Status_Identifier__c == 12.0 )&& q.Vet_Non_Vet__c == false)
                    event.Error_Message__c = 'If Qualification/Course Recognition Identifier cannot be 11 or 12 when Vet Flag is false';
                    
               else if((q.ANZSCO_Identifier__c == 'NONVET' && q.Vet_Non_Vet__c != false) || (q.ANZSCO_Identifier__c != 'NONVET' && q.Vet_Non_Vet__c == false))
                    event.Error_Message__c = 'For NONVET ANZSCO Identifier Vet Flag needs to be false/unticked'; 
                
               else if(q.Recognition_Status_Identifier__c == 14.0 && (q.Field_Of_Education_Identifier__c < '611' ||  q.Field_Of_Education_Identifier__c > '999') )
                    event.Error_Message__c = 'Field of Education Identifier must be in range 611-999 when Qualification Identifier is 14';
                    
               else if(q.Vet_Non_Vet__c != true && (q.Field_Of_Education_Identifier__c < '211' ||  q.Field_Of_Education_Identifier__c > '524') )
                    event.Error_Message__c = 'If Qualification Category Level of Education Identifier is in range 211-621, Vet Fee flag must be true '; 
                
               else if(q.Recognition_Status_Identifier__c == 12.0 && (q.Field_Of_Education_Identifier__c < '211' ||  q.Field_Of_Education_Identifier__c > '524') )
                    event.Error_Message__c = 'Field of Education Identifier must be in range 211-524 when Qualification Identifier is 12';
                
               if(event.Error_Message__c != null)   
                errorEventList.add(event);
                  
            }
            
        }
        catch (exception e) {
            log.System_Message__c = e.getMessage();
        }             
    }
    
    global void finish(Database.BatchableContext BC) {
        
        insert errorEventList;
        
        log.NAT00030_Record_Count__c = recordCount;
        
        update log;
        
        Database.executeBatch(new NATValidationBatch_Units( log.id, 'NAT60' ) );

    }
    
}