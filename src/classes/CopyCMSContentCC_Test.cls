/**
 * @description Test class for CopyCMSContentCC
 * @author Biao Zhang
 * @date 29.DEC.2015
 */
@isTest
private class CopyCMSContentCC_Test {
        
        static testMethod void test() {                    
        ANZSCO__c anz = TestCoverageUtilityClass.createANZSCO();
        Field_of_Education__c fld = TestCoverageUtilityClass.createFieldOfEducation();
        Qualification__c fromQual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        Qualification__c toQual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);

        Website_Provider__c provider = new Website_Provider__c(WP_Qualification__c = fromQual.Id);
        insert provider;

        Website_Delivery_Mode__c delivery = new Website_Delivery_Mode__c(Qualification__c = fromQual.Id);
        insert delivery;

        Website_Disclaimer__c disclaimer = new Website_Disclaimer__c(WD_Qualification__c = fromQual.Id);
        insert disclaimer;

        CopyCMSContentCC controller = new CopyCMSContentCC();
        controller.fromSearchString = controller.toSearchString = '';
        controller.searchQualityFrom();
        controller.searchQualityTo();

        controller.showOriginalQualifications();
        
        controller.fromQualifications.get(0).selected = true;
        controller.toQualifications.get(1).selected = true;

        controller.showOriginalQualifications();
        controller.showUpdatedQualifications();
        controller.save();
        }       
}