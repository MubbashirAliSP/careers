@istest public class CensusDateUpdateBatchTest {


    private static testmethod void testGenerateSummary() {
    
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
      
        system.runAs(u) {
       
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment_Unit__c enrl_unit = new Enrolment_Unit__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine = new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Awards__c award = new Awards__c();
        Awards__c award2 = new Awards__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Results__c sres = new Results__c();
        Intake__c itake = new Intake__c();
        Program__c prgm = new Program__c();
        Funding_Stream_Types__c fstream = new Funding_Stream_Types__c();
        Intake_Unit__c itakeUnit = new Intake_Unit__c();
    
        acc = TestCoverageUtilityClass.createStudent();
        accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
        anz = TestCoverageUtilityClass.createANZSCO();
        fld = TestCoverageUtilityClass.createFieldOfEducation();
        qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
        unit = TestCoverageUtilityClass.createUnit();
        qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
        country = TestCoverageUtilityClass.createCountry(); 
        state = TestCoverageUtilityClass.createState(country.Id); 
        fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
        prgm = TestCoverageUtilityClass.createProgram(qual.Id, loc2.Id);
        fstream = TestCoverageUtilityClass.createFundingStream();
        itake = TestCoverageUtilityClass.createIntake(qual.Id, prgm.Id, loc2.Id, fstream.Id);
        itakeUnit = TestCoverageUtilityClass.createIntakeUnit(itake.Id, unit.Id);
        
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        pcon.Contract_Name__c = 'test Contract';
        pcon.Contract_Type__c = 'Apprenticeship Funding';
        pcon.RecordTypeId = recordTypeId;
        pcon.State_Fund_Source__c = fSource.Id;
        pcon.Contract_Code__c = '1234567890';
        pCon.Training_Organisation__c = accTraining.Id;
        pcon.Contract_State_Lookup__c = state.Id;
        insert pcon;
        
        conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
        lineItem = TestCoverageUtilityClass.createLineItemQualificationFS(qual.Id, conLine.Id, fSource.Id);
        lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
        system.debug(lineItemUnit);
        system.debug(lineItem);
        res = TestCoverageUtilityClass.createNationalResult();
        res.Result_Code__c = '20';
        update res;
        sres = TestCoverageUtilityClass.CreateStateOutCome(res.id,state.id);
        sres.Result_Code__c = '20';
        sres.National_Outcome__c = res.Id;
        update sres;
                
        locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
        parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
        loc = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
        loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
        del = TestCoverageUtilityClass.createDeliveryModeType();
        attend = TestCoverageUtilityClass.createAttendanceType(); 
        
        
        List<Enrolment__c> enrolmentList = new List<Enrolment__c>();

            for(Integer i = 0; i < 5; i++) {
                Enrolment__c enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005';
                enrl.Other_Zip_Postal_Code__c = '4005';
                enrl.Enrolment_Status__c = 'Active (Commencement)';
                enrl.Start_Date__c = date.today() - 50;
                enrl.End_Date__c = date.newInstance( date.today().year(), date.today().month(), 01);
                enrl.Mailing_State_Provience__c = 'Queensland';
                enrolmentList.add(enrl);
            }
        
        insert enrolmentList;
        
       
        enrolmentList = [SELECT Id FROM Enrolment__c LIMIT 5];
        List<Enrolment_Unit__c> enrolmentUnitList = new List<Enrolment_Unit__c>();
        
        For(Enrolment__c en : enrolmentList) {
            for(Integer i = 0; i < 25; i++) {
                Enrolment_Unit__c newEU = new Enrolment_Unit__c();
                newEU.Enrolment__c = en.Id;
                newEU.Delivery_Mode__c = del.Id;
                newEU.Unit__c = unit.Id;
                newEU.Training_Delivery_Location__c = loc2.Id;
                newEU.Start_Date__c = date.today();
                 
                newEU.Line_Item_Qualification_Unit__c = lineItemUnit.Id;
               
                enrolmentUnitList.add(newEU);

            } 
        }
        
        insert enrolmentUnitList;
        
        Stage__c stage = new Stage__c();
        
        stage.Name = 'Stage 1';
        
        insert stage;
        
        itakeUnit.Stage__c = stage.Id;
        
        update itakeUnit;
                
        List<Enrolment_Unit__c> enrolmentUnits = [ SELECT Id, Enrolment__c FROM Enrolment_Unit__c ];
        
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        
        for(Enrolment_Unit__c enrlU : enrolmentUnits) {
            
            Enrolment_Intake_Unit__c eiuNew = new Enrolment_Intake_Unit__c();
            eiuNew.Enrolment__c = enrlU.Enrolment__c;
            eiuNew.Intake_Unit__c = itakeUnit.Id;
            eiuNew.Enrolment_Unit__c = enrlU.Id;
            eiuList.add(eiuNew);
               
        }
        
        insert eiuList;
            
        List<Enrolment_Intake_Unit__c> eiuList2 = [ SELECT ID FROM Enrolment_Intake_Unit__c WHERE Intake_Unit__r.Stage__r.Name != null ];
        
        system.debug(eiuList2);

        
        
        List<Enrolment_Unit_Of_Study__c> euosList = new List<Enrolment_Unit_Of_Study__c>();
        
        for(Enrolment_Unit__c enrlU : enrolmentUnits) {
            
            Enrolment_Unit_Of_Study__c euosNew = new Enrolment_Unit_Of_Study__c();
            euosNew.Enrolment_Unit__c = enrlU.Id;
            euosNew.Qualification_Unit__c = qunit.Id;
            euosNew.Census_Date__c = date.today().addMonths(1);
            euosNew.Amount_Received__c=1000;
            euosNew.Total_amount_charged__c=200;
            euosNew.Is_Census_Date_Updated__c = true;
            euosList.add(euosNew);
            
        }
        
        insert euosList;
        
                       
        List<Enrolment_Unit_of_Study__c> insertedEUOS = [ Select Id, Enrolment_Unit__c, Enrolment_Unit__r.Enrolment__c, Stage__c FROM Enrolment_Unit_Of_Study__c WHERE Stage__c != null ];
        
        system.debug('xyz' + insertedEUOS);
        
        List<Id> insertedEUOSid = new List<Id>();
        List<Enrolment_Unit_of_Study__c> updatedEUOS= new List<Enrolment_Unit_of_Study__c>();
        for(Enrolment_Unit_of_Study__c e :insertedEUOS) {
            insertedEUOSId.add(e.Id);
            e.Census_Date__c=date.today();
            updatedEUOS.add(e);
        }
       // update updatedEUOS;
        system.debug(insertedEUOSId);
        
        RevenueRecognitionSetting__c setting = new RevenueRecognitionSetting__c();
        setting.Name = 'Email';
        setting.Notification_Email__c = 'neha.batra@cloudsherpas.com';
        insert setting;
        
            
        RevenueRecognitionEUOSEngine.generateERS(insertedEUOSId);
        Database.executeBatch(new RevenueRecognitionBatch(), 200);  
         RevenueRecognitionEUOSEngine.generateERS(insertedEUOSId);    
            
       /* Enrolment__c enrolmentListCancelled = [SELECT Id FROM Enrolment__c LIMIT 1][0];
        enrolmentListCancelled.Enrolment_Status__c='Cancelled';
        update enrolmentListCancelled; */
        
        List<Enrolment_Revenue_Summary__c> ersList1 = [SELECT Id FROM Enrolment_Revenue_Summary__c ];
         List<Enrolment_Revenue_Summary__c> ersUpdate = new  List<Enrolment_Revenue_Summary__c>();
        for(Enrolment_Revenue_Summary__c ers : ersList1)    
        {          
           ers.Last_Historical_Record_Creation_Date__c=null;
            ersUpdate.add(ers);
        }
            update ersUpdate;
        
      
        Test.StartTest();
        
            CensusDateUpdateBatch b = new CensusDateUpdateBatch();
    
            database.executeBatch(b);
        
        Test.StopTest();
        
        List<Enrolment_Revenue_Summary__c> ersList = [SELECT Id FROM Enrolment_Revenue_Summary__c ];
        
        system.debug(ersList);
        
        Integer ersListSizeInitial = ersList.size();
        
        system.assert(ersList.size() > 0);
        
        }
      
    }

    private static testmethod void testScheduledJob() {
   
    
          Test.StartTest();
           CensusDateUpdateBatchScheduler scheduler = new CensusDateUpdateBatchScheduler();
           
           String sch = '0 0 23 * * ?';
           system.schedule('Test Territory Check', sch, scheduler);
       Test.StopTest();
    
        
    
    }

}