@isTest
private class NewEnrolmentRedirect_CC_Test {

    static testMethod void NewEnrolmentRedirect_CC_Test() {
      
        Id recTypeId;
            
        for(RecordType r:[Select Id from RecordType where DeveloperName = 'PersonAccount']){
            recTypeId=r.Id;
        }
          
        Account acc = new Account();
        acc.PersonMailingCity = 'test';
        acc.PersonMailingStreet = 'test';
        acc.PersonMailingCountry = 'AUSTRALIA';
        acc.PersonMailingState = 'QLD';
        acc.PersonMailingPostalCode = '4006';
        acc.PersonOtherStreet = 'test';
        acc.PersonOtherCity = 'test';
        acc.PersonOtherState = 'QLD';
        acc.PersonOtherCountry = 'AUSTRALIA';
        acc.PersonOtherPostalCode = '4006';
        acc.FirstName = 'Jeo';
        acc.LastName = 'Jandusay';
        acc.RecordTypeId  = recTypeId;
        acc.Work_Phone__c = '0882308400';
        acc.Salutation = 'Mr.';
        acc.Proficiency_in_Spoken_English__c = '@ - Not Specified';
        acc.Learner_Unique_Identifier__c = '12ab34cd56';
        insert acc;

        Field_of_Education__c foe = new Field_of_Education__c();
        foe.Name = 'No credit was offered for VET';
        insert foe;

        Credit_status_Higher_Ed_provider_code__c cshepc = new Credit_status_Higher_Ed_provider_code__c();
        cshepc.Name = 'No higher education credit offered';
        insert cshepc;
        
        test.startTest();
        
        PageReference testPage = Page.NewEnrolmentRedirect;
        testPage.getParameters().put('aId',acc.Id);
        Enrolment__c enrol = new Enrolment__c();
        Test.setCurrentPage(testPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(enrol);
        NewEnrolmentRedirect_CC enr = new NewEnrolmentRedirect_CC(controller);
        enr.redirect();
        
        test.stopTest();
    }
}