/**
 * @description Custom Lightning Components controller for the Student Engagement Console
 * @author Ranyel Maliwanag
 * @date 09.MAY.2016
 */
public with sharing class StudentEngagementConsoleLC {
    @AuraEnabled
    public static List<User> getTrainersList() {
        return [SELECT Id, Name FROM User WHERE (UserRole.DeveloperName = 'Online_Trainer_User' AND IsActive = true) OR (Id = :UserInfo.getUserId()) ORDER BY Name ASC];
    }

    @AuraEnabled
    public static List<SEC_Tab__mdt> getTabsList() {
        return [SELECT MasterLabel, DeveloperName, SEC_SortOrder__c FROM SEC_Tab__mdt ORDER BY SEC_SortOrder__c ASC];
    }

    @AuraEnabled
    public static List<SEC_Container__mdt> getContainersList(String currentTabName) {
        return [SELECT MasterLabel, DeveloperName, SEC_SortOrder__c, SEC_Tab__c FROM SEC_Container__mdt WHERE SEC_Tab__c = :currentTabName ORDER BY SEC_SortOrder__c ASC];
    }

    @AuraEnabled
    public static List<Task> getTrainerTaskList(String trainerId, String filterCode) {
        String queryString = 'SELECT Id, Subject, What.Name, Who.Name, WhoId, WhatId, ActivityDate ';
        queryString += 'FROM Task ';
        queryString += 'WHERE OwnerId = :trainerId AND IsClosed = false ';

        if (String.isNotBlank(filterCode)) {
            Map<String, String> codeFilters = new Map<String, String>{
                'Overdue' => 'ActivityDate < TODAY',
                'Today' => 'ActivityDate = TODAY',
                'Today + Overdue' => 'ActivityDate <= TODAY',
                'Tomorrow' => 'ActivityDate = TOMORROW',
                'Next 7 Days' => 'ActivityDate = NEXT_N_DAYS:7',
                'Next 7 Days + Overdue' => '(ActivityDate < TODAY OR ActivityDate = NEXT_N_DAYS:7)',
                'This Month' => 'ActivityDate = THIS_MONTH'
            };
            if (codeFilters.keySet().contains(filterCode)) {
                queryString += 'AND ' + codeFilters.get(filterCode) + ' ';
            }
        }

        queryString += 'ORDER BY ActivityDate ASC NULLS LAST ';
        queryString += 'LIMIT 20 ';
        return Database.query(queryString);
    }

    @AuraEnabled
    public static String getCurrentTrainerId() {
        return UserInfo.getUserId();
    }

    @AuraEnabled
    public static String getCurrentTrainerName() {
        return UserInfo.getName();
    }

    @AuraEnabled
    public static Task getCurrentTask(String taskId) {
        Task result = null;
        if (String.isNotBlank(taskId)) {
            result = [SELECT Id, Subject, Owner.Name, ActivityDate, Status, Priority, Description, What.Name, Who.Name, Contact_Type__c, Contact_Outcome__c, Eligibility_Items__c FROM Task WHERE Id = :taskId];
        }
        return result;
    }
    
    @AuraEnabled
    public static Task getNewTask() {
        Task result = (Task) Task.SObjectType.newSObject(Schema.SObjectType.Task.getRecordTypeInfosByName().get('Online Task').getRecordTypeId(), true);
        return result;
    }
    
    @AuraEnabled
    public static Task saveTask(Task record) {
        upsert record;
        System.debug('record ::: ' + record);
        return record;
    }

    @AuraEnabled
    public static List<SEC_ListView__mdt> getContainerListViews(String containerName) {
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_ListView__mdt);
        queryString += 'WHERE SEC_Container__c = :containerName ORDER BY SEC_SortOrder__c ASC';
        return Database.query(queryString);
    }

    @AuraEnabled
    public static SEC_ListViewWrapper getCurrentListView(String listViewName) {
        SEC_ListViewWrapper result = new SEC_ListViewWrapper();
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.SEC_ListView__mdt);
        queryString += 'WHERE DeveloperName = :listViewName';
        result.meta = Database.query(queryString);
        
        List<SEC_ListViewWrapper.SEC_FieldSet> fieldsets = new List<SEC_ListViewWrapper.SEC_FieldSet>();
        for (Schema.FieldSetMember fsm : Schema.getGlobalDescribe().get(result.meta.SEC_SObjectName__c).getDescribe().fieldSets.getMap().get(result.meta.SEC_FieldsetName__c).getFields()) {
            SEC_ListViewWrapper.SEC_FieldSet fs = new SEC_ListViewWrapper.SEC_FieldSet();
            fs.label = fsm.label;
            fs.fieldPath = fsm.fieldPath;
            fs.fieldType = String.valueOf(fsm.type);
            fieldsets.add(fs);
        }
        result.fieldsetMembers = fieldsets;
        result.sObjectName = Schema.getGlobalDescribe().get(result.meta.SEC_SObjectName__c).getDescribe().getLabelPlural();
        return result;
    }
    
    @AuraEnabled
    public static List<SObject> getCurrentListViewRecords(SEC_ListView__mdt meta, List<SEC_ListViewWrapper.SEC_FieldSet> fieldsetMembers, String trainerId) {
        List<SObject> result = new List<SObject>();
        SEC_ListViewWrapper wrapper = new SEC_ListViewWrapper();
        wrapper.meta = meta;
        Id currentTrainer = trainerId;
        System.debug('currentTrainer ::: ' + currentTrainer);
        List<String> validStatuses = new List<String> {
            'Active%'
        };
        
        // Set up the set of fields that needs to be queried for this specific data table
        Set<String> fieldsForQuery = new Set<String>();
        for (Schema.FieldSetMember fsm : wrapper.fieldsetMembersAuto) {
            fieldsForQuery.add(fsm.getFieldPath());
            
            // Special handling for reference fields: include the name in the query
            if (fsm.getType() == Schema.DisplayType.Reference && fsm.getFieldPath().endsWith('__c')) {
                fieldsForQuery.add(fsm.getFieldPath().replace('__c', '__r') + '.Name');
            }
        }
        // Ensure that the Id field is included in the query, in case it's not included in the fieldset
        if (!fieldsForQuery.contains('Id')) {
            fieldsForQuery.add('Id');
        }
        if (!fieldsForQuery.contains('Name')) {
            fieldsForQuery.add('Name');
        }
        // Additional query for categorisation
        if (wrapper.isCategorised && meta.SEC_CategorisationSource__c == 'Record' && String.isNotBlank(meta.SEC_CategorisationField__c) && !fieldsForQuery.contains(meta.SEC_CategorisationField__c)) {
            fieldsForQuery.add(meta.SEC_CategorisationField__c);
        }
        // Additional query for Enrolment actions
        if (meta.SEC_SObjectName__c == 'Enrolment__c') {
            fieldsForQuery.add('Student__r.PersonContactId');
            fieldsForQuery.add('Student__r.PersonEmail');
            fieldsForQuery.add('TrainerCategory__c');
            fieldsForQuery.add('Student__r.Blackboard_UserID__c');
        } else if (meta.SEC_SObjectName__c == 'AssessmentAttempt__c') {
            fieldsForQuery.add('AccountId__r.PersonContactId');
            fieldsForQuery.add('AccountId__r.PersonEmail');
            fieldsForQuery.add('AccountId__r.Blackboard_UserID__c');
            fieldsForQuery.add('BB_Course_PK1__c');
            fieldsForQuery.add('courseMembershipId__c');
            fieldsForQuery.add('outcomeDefinitionId__c');
        }
        
        // Build the query string based on the fields defined on the fieldset
        String queryString = 'SELECT ' + String.join(new List<String>(fieldsForQuery), ', ');
        queryString += ' FROM ' + meta.SEC_SObjectName__c;
        if (String.isNotBlank(meta.SEC_ListViewFilter__c)) {
            queryString += ' WHERE ' + meta.SEC_ListViewFilter__c;
        }
        if (String.isNotBlank(meta.FilterOptionsLabel__c)) {
            String labels = Label.AA_list_view_filter_options;
            List<String> exclusionList = labels.split(',');
            System.debug('exclusionList ::: ' + exclusionList);
            queryString += ' AND (NOT BBAssessmentId__c LIKE :exclusionList) ';
        }
        
        //String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c);
        //queryString += 'WHERE Assigned_Trainer__c = :trainerId';
        result = Database.query(queryString);
        return result;
    }

    @AuraEnabled
    public static List<SEC_Wrappers.PicklistValue> getPicklistValues(String objectType, String field) {
        List<SEC_Wrappers.PicklistValue> result = new List<SEC_Wrappers.PicklistValue>();
        System.debug(Schema.getGlobalDescribe().get(objectType));
        System.debug(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().containsKey(field));
        for (Schema.PicklistEntry entry : Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().get(field).getDescribe().getPicklistValues()) {
            result.add(new SEC_Wrappers.PicklistValue(entry));
        }
        return result;
    }
    
    
    @AuraEnabled
    public static Boolean saveAssignCategory(List<String> recordIds, String category) {
        List<Enrolment__c> records = new List<Enrolment__c>();
        for (String recordId : recordIds) {
            records.add(new Enrolment__c(Id = recordId, TrainerCategory__c = category));
        }
        update records;
        return true;
    }
    
    
    @AuraEnabled
    public static Map<String, Object> getEnrolment(String eid) {
        Map<String, Object> result = new Map<String, Object>();
        Enrolment__c enrolment = [SELECT Id, Delivery_Location_State__c, Enrolment_Status__c, Parent_Qualification_Name__c, Start_Date__c, Student__c, Student_Identifier__c, TeachingStatus__c, Credit_RPL_details_of_prior_study__c, Mailing_State_Provience__c, FirstContactStatus__c, TrainerCategory__c, Completion_Date__c,
            
            Student__r.Name, Student__r.RiskCategory__c, Student__r.RecordType.Name, Student__r.Phone, Student__r.PersonMobilePhone, Student__r.PersonEmail, Student__r.Disability_Type__c,

            (SELECT Id, AVETMISS_National_Outcome__c, Start_Date__c, Unit__c, Unit__r.Name FROM Enrolment_Units__r)
            
            FROM Enrolment__c
            WHERE Id = :eid
        ];
        result.put('enrolment', enrolment);
        result.put('enrolmentUnits', getEnrolmentUnits(enrolment));
        return result;
    }
    
    
    /**
     * @description Creates a log a call record
     * @author Ranyel Maliwanag
     * @date 06.JUN.2016
     */
    @AuraEnabled
    public static Boolean saveLogACall(Task referenceRecord, List<Enrolment__c> sourceRecords) {
        List<Task> tasksForInsert = new List<Task>();
        for (Enrolment__c record : sourceRecords) {
            Task recordTask = referenceRecord.clone();
            recordTask.WhatId = record.Id;
            recordTask.WhoId = record.Student__r.PersonContactId;
            tasksForInsert.add(recordTask);
        }
        if (!tasksForInsert.isEmpty()) {
            insert tasksForInsert;
        }
        return true;
    }
    
    
    /**
     * @description
     */
    @AuraEnabled
    public static List<Account> getAccountAlerts(List<Enrolment__c> enrolments) {
        // Fetch Account Ids for querying
        Set<Id> accountIds = new Set<Id>();
        for (Enrolment__c enrolment : enrolments) {
            accountIds.add(enrolment.Student__c);
        }
        
        return [SELECT Id, (SELECT Id FROM Events WHERE RecordType.Name = 'Account Alert') FROM Account WHERE Id IN :accountIds];
    }
    
    
    @AuraEnabled
    public static Boolean markAsGraded(List<String> recordIds) {
        List<AssessmentAttempt__c> records = new List<AssessmentAttempt__c>();
        for (String recordId : recordIds) {
            AssessmentAttempt__c record = new AssessmentAttempt__c(
                Id = recordId,
                AssessmentStatus__c = 'COMPLETED'
            );
            records.add(record);
        }
        
        if (!records.isEmpty()) {
            update records;
        }
        return true;
    }
    
    
    @AuraEnabled
    public static void passiveAction(){}


    /**
     * @description Creates a list of Wrapped Enrolment Unit items for the Enrolment Unit records of the given enrolment
     * @author Ranyel Maliwanag
     * @date 03.JUN.2016
     */
    private static List<SEC_Wrappers.EnrolmentUnit> getEnrolmentUnits(Enrolment__c enrolment) {
        List<SEC_Wrappers.EnrolmentUnit> result = new List<SEC_Wrappers.EnrolmentUnit>();
        for (Enrolment_Unit__c record : enrolment.Enrolment_Units__r) {
            SEC_Wrappers.EnrolmentUnit unit = new SEC_Wrappers.EnrolmentUnit(record);
            result.add(unit);
        }
        return result;
    }
}