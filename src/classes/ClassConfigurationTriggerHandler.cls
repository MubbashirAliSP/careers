/**
 * @description Trigger Handler for Class Configuration object
 * @author Janella Lauren Canlas
 * @date 05.NOV.2012
 *
 * HISTORY
 * - 05.NOV.2012  Janella Canlas - Created.
 * - 16.AUG.2013  Michelle Magsarili - updated 12hr logic under updateClassRecords
 * - 13.FEB.2015  Kim Saclag - Removed comment for deleteClassRecords and insertClassRecords
 * - 05.AUG.2015  Miguel de Guia - added system.debug('endDate!!!'+endDate); System.debug(i.Id); changed Datetime startDate = tempIntake.Start_Date__c;;
 */
public with sharing class ClassConfigurationTriggerHandler {
  /*
    this method will insert class records upon creation of a Class Configuration record
  */
  
  public static void insertOneOffClassRecords(List<Class_Configuration__c> configList) {
        
        for(Class_Configuration__c c : configList){ /*Limited to 1 record, so DML is inside the loop */
            
            Classes__c tempClass  = new Classes__c(Intake__c = c.Intake__c, Intake_Unit__c = c.Intake_Unit__c,Room__c = c.Class_Room__c, Start_Date_Time__c = c.Start_Date_Time__c,End_Date_Time__c = c.End_Date_Time__c,Class_Configuration__c =c.Id);
            
            insert tempClass;
        }
        
        
    
  
  
  }
  
  public static void insertClassRecords(List<Class_Configuration__c> configList){
    Map<Id,Intake__c> intakeMap = new Map<Id, Intake__c>();
    Map<Id,String> recordTypeMap = new Map<Id,String>();
    List<Intake__c> intakeList = new List<Intake__c>();
    List<RecordType> recTypeList = new List<RecordType>();
    List<Classes__c> classList = new List<Classes__c>();
    Set<Id> intakeIds = new Set<Id>();
    //query record types
    recTypeList = [SELECT id, name from RecordType where Name = 'Reoccuring Class'];
    //create map of record type Id and Name
    for(RecordType r : recTypeList){
      if(!recordTypeMap.containsKey(r.Id)){
        recordTypeMap.put(r.Id,r.Name);
      }
    }
    //retrieve related intake ids
    for(Class_Configuration__c c : configList){
      intakeIds.add(c.Intake__c);
    }
    //query related intake records 
    System.debug(intakeIds);
    intakeList = [SELECT id, Start_Date__c, End_Date__c FROM Intake__c WHERE Id IN: intakeIds];
    
    //create a map of intake ids and records
    for(Intake__c i : intakeList){
      if(!intakeMap.containsKey(i.Id)){
        intakeMap.put(i.Id,i);
      }
      System.debug('id!!!'+i.Id);
    }
    System.debug('intakeMap ' + intakeMap);
    //loop to create new Class records
    for(Class_Configuration__c c : configList){
      system.debug('recordTypeMap.get(c.RecordTypeId) ' + recordTypeMap.get(c.RecordTypeId));
      if(recordTypeMap.get(c.RecordTypeId) == 'Reoccuring Class'){
        //related intake for specific configuration
        Intake__c tempIntake = intakeMap.get(c.Intake__c);
        //day of the week from class configuration
        String day = c.Day__c;
        //split chosen days
        String[] daySplit = day.split(';');
        //create set to separate days
        Set<String> daySet = new Set<String>();
        for(integer i=0;i<daySplit.size();i++){
          daySet.add(daySplit[i]);
        }
        System.debug('daySet'+daySet);
        
        Datetime endDate = tempIntake.End_Date__c;
        String dayFromArray;
        System.debug('daySplit.size()' + daySplit.size());
        for(integer i=0;i<daySplit.size();i++){
          system.debug('$$$ i: '+ i);
          Datetime startDate = tempIntake.Start_Date__c; //JMDG 06.Aug.2015 SAS016374 and SAS016628 FIX
          system.debug('startDate!!!'+startDate);
          system.debug('endDate!!!'+endDate);
          while(startDate <= endDate){
            String dayOfWeek=startDate.format('EEEE');
            System.debug('dayofweek'+dayOfWeek);
            if(dayOfWeek == daySplit[i]){
              System.debug('daySplit[i]'+daySplit[i]);
              //variables for manipulating start time
              String startTime = c.Class_Start_Time__c;
              String[] hourSplit = startTime.split(':',2);
              Integer hourInt = 0;
              String[] minSplit = hourSplit[1].split(' ',2);
              String minute = minSplit[0];
              String second = '00';
              if(minSplit[1] == 'PM' && hourSplit[0] != '12'){
                hourInt = Integer.valueOf(hourSplit[0]) + 12;
              }
              else if(minSplit[1] == 'PM' && hourSplit[0] == '12'){
                hourInt = 12;
              }
              else{
                hourInt = Integer.valueOf(hourSplit[0]);
              }
              String hour ='';
              if(hourSplit[0].length() == 1){
                hour = '0' + hourInt;
              }
              else{
                hour = string.valueOf(hourInt);
              }
              //variables for manipulating end time
              String endTime = c.Class_End_Time__c;
              String[] endhourSplit = endTime.split(':',2);
              String[] endminSplit = endhourSplit[1].split(' ',2);
              Integer endHourInt = 0;
              String endHour ='';
              system.debug('endminSplit[0]'+endminSplit[0]);
              system.debug('endminSplit[1]'+endminSplit[1]);
              if(endminSplit[1] == 'PM' && endhourSplit[0] != '12'){
                endHourInt = Integer.valueOf(endhourSplit[0]) + 12;
              }
              else if(endminSplit[1] == 'PM' && endhourSplit[0] == '12'){
                endHourInt = 12;
              }
              else{
                endHourInt = Integer.valueOf(endhourSplit[0]);
              }
              if(endHourSplit[0].length() == 1){
                endHour = '0' + endHourInt;
              }
              else{
                endHour = string.valueOf(endHourInt);
              }
              String endMinute = endminSplit[0];
              //populate start and end times
              String tempStartDateTime = startDate.format('yyyy-MM-dd') + ' ' + hour +':'+minute+':'+second;
              String tempEndDateTime = startDate.format('yyyy-MM-dd') + ' ' + endHour +':'+endMinute+':'+second;
              //instantiate new class record
              Classes__c tempClass = new Classes__c();
              system.debug('%%0c.Intake_Unit__c: '+c.Intake_Unit__c);
              if(c.Intake_Unit__c != null){
                system.debug('%%1c.Intake_Unit__c: '+c.Intake_Unit__c);
                tempClass.Intake_Unit__c = c.Intake_Unit__c;
              }
              tempClass.Intake__c = tempIntake.Id;
              tempClass.Class_Configuration__c = c.Id;
              tempClass.Start_Date_Time__c = dateTime.valueOf(tempStartDateTime);
              tempClass.End_Date_Time__c = dateTime.valueOf(tempEndDateTime);
              tempClass.Room__c = c.Class_Room__c;
              System.debug('Date ' + tempStartDateTime + ' ' + tempEndDateTime);
              System.debug('DateTime ' +dateTime.valueOf(tempStartDateTime) + ' ' + dateTime.valueOf(tempEndDateTime));
              //system.debug('@@##c.Intake__r.Duplicated__c: '+c.Intake__r.Duplicated__c);
              //system.debug('@@##c.Intake__c: '+c.Intake__c);
              //if(c.Intake__r.Duplicated__c == false){
                classList.add(tempClass);
              //}
            }
            startDate = startDate.addDays(1);
            system.debug('startDate!!!'+startDate);
            system.debug('endDate!!!'+endDate);
          }  
        }
        
      }
    }
    //insert classes
    try{
      insert classList;}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());}
  }
  /*
    This method will delete all Class Records once the Class Configuration has been deleted.
  */
  public static void deleteClassRecords(Set<Id> configIds){
    system.debug('CONFIGIDS '+configIds);
    try{
      delete new List<Classes__c>([select id from Classes__c where Class_Configuration__c IN: configIds]);}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());}
  }
  /*
    This method will the Classes if the details on the Class Configuration
    record related has been updated
  */
  public static void updateClassRecords(List<Class_Configuration__c> configList,List<Class_Configuration__c> oldConfigList){
    Map<Id,Class_Configuration__c> conMap = new Map<Id,Class_Configuration__c>();
    Map<Id,Class_Configuration__c> oldConMap = new Map<Id,Class_Configuration__c>();
    List<Classes__c> classList = new List<Classes__c>();
    List<Classes__c> classToUpdate = new List<Classes__c>();
    Set<Id> configIds = new Set<Id>();
    
    for(Class_Configuration__c c : configList){
      //create map of Class Configuration ids and records
      if(!conMap.containsKey(c.Id)){
        conMap.put(c.Id,c);
      }
      //collect Class Configuration Ids
      configIds.add(c.Id);
    }
    //Query related classes
    classList = [select id, Intake_Unit__c, Room__c, Start_Date_Time__c, End_Date_Time__c  from Classes__c where Class_Configuration__c IN: configIds];
    //create map of Class Configuration Ids and Old record
    for(Class_Configuration__c c : oldConfigList){
      if(!oldConMap.containsKey(c.Id)){
        oldConMap.put(c.Id,c);
      }
    }
    //loop updated class configuration records to update class records
    for(Class_Configuration__c c : configList){
      Class_Configuration__c oldConfig = oldConMap.get(c.Id);
      //if the class configuration start or end time has been change, delete classes and re-insert them  
      if(c.Class_Start_Time__c != oldConfig.Class_Start_Time__c || c.End_Date_Time__c != oldConfig.End_Date_Time__c || c.Day__c != oldConfig.Day__c || c.Class_End_Time__c != oldConfig.Class_End_Time__c){
        //KS 12/13/2015 Removed comment for deleteClassRecords and insertClassRecords
        deleteClassRecords(configIds);
        insertClassRecords(configList);
        for(Classes__c cl : classList){
            Integer year = cl.Start_Date_Time__c.year();
            Integer month = cl.Start_Date_Time__c.month();
            Integer day = cl.Start_Date_Time__c.day();
            List<String> tempStrList = c.Class_Start_Time__c.split(' ');
            List<String> hourMinuteTempStrList = tempStrList[0].split(':');
            String strFinalHour = hourMinuteTempStrList[0];
            //MAM 08/16/2013 12-hr update start
            if((tempStrList[1]=='PM' && Integer.valueof(hourMinutetempStrList[0]) != 12) ||
               (tempStrList[1]=='AM' && Integer.valueof(hourMinutetempStrList[0]) == 12)){
                Integer finalHour = Integer.valueOf(hourMinuteTempStrList[0])+12;
                strFinalHour = String.valueOf(finalHour);
            }
           
            String stringDate = string.valueOf(year) + '-' + string.valueOf(month) + '-' + string.valueOf(day) + ' ' + strFinalHour + ':' + hourMinuteTempStrList[1] +  ':' + '0'; 
            Datetime myDate = datetime.valueOf(stringDate);
            cl.Start_Date_Time__c = myDate;
          
            Integer year2 = cl.End_Date_Time__c.year();
            Integer month2 = cl.End_Date_Time__c.month();
            Integer day2 = cl.End_Date_Time__c.day();
            List<String> tempStrList2 = c.Class_End_Time__c.split(' ');
            List<String> hourMinutetempStrList2 = tempStrList2[0].split(':');
            String strfinalHour2 = hourMinutetempStrList2[0];
            System.debug('&&strfinalHour2: '+strfinalHour2);
            
            if((tempStrList2[1]=='PM' && Integer.valueof(hourMinutetempStrList2[0]) != 12) ||
               (tempStrList2[1]=='AM' && Integer.valueof(hourMinutetempStrList2[0]) == 12)){
                Integer finalHour2 = Integer.valueOf(hourMinutetempStrList2[0])+12;
                strfinalHour2 = String.valueOf(finalHour2);
            }
            
            //MAM 08/16/2013 12-hr update end
            String stringDate2 = string.valueOf(year2) + '-' + string.valueOf(month2) + '-' + string.valueOf(day2) + ' ' + strfinalHour2 + ':' + hourMinutetempStrList2[1] +  ':' + '0'; 
            System.Debug('&&stringDate2: ' + stringDate2);
            Datetime myDate2 = datetime.valueOf(stringDate2);
            cl.End_Date_Time__c = myDate2;
            classToUpdate.add(cl);
        }
      }else{
        for(Classes__c cl : classList){
          cl.Room__c = c.Class_Room__c;
          classToUpdate.add(cl);
        }
      }
    }
    //update classes
    try{
      update classToUpdate;}catch(Exception e){System.debug('E MESSAGE: ' + e.getMessage());}

  }
}