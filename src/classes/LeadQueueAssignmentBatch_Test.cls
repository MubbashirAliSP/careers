/**
 * @description Test class for LeadQueueAssignmentBatch
 * @author Ranyel Maliwanag
 * @date 12.MAY.2015
 * @history
 *       01.JUN.2015    Ranyel Maliwanag    - Integrated Holiday support to skip execution during defined holidays
 *       04.JUN.2015    Ranyel Maliwanag    - Updated inserted tasks to have `Status = Completed` as per updated requirements to consider only completed Tasks
 */
@isTest
public with sharing class LeadQueueAssignmentBatch_Test {
    @testSetup
    static void triggerSetup() {
        // Condition 1 Setup
        Lead condition1Lead = new Lead(
                LastName = 'Test Lead 1',
                Status = 'Open',
                Referral_Source_Type_Detail__c = 'Contactability',
                Salutation = 'Mr'
            );
        insert condition1Lead;

        List<Task> tasksForInsert = new List<Task>();
        for (Integer i = 0; i < 2; i++) {
            Task testTask = new Task(
                    WhoId = condition1Lead.Id,
                    Contact_Type__c = 'Not Contacted',
                    Contact_Outcome__c = 'No Answer',
                    Subject = 'Call',
                    Status = 'Completed'
                );
            tasksForInsert.add(testTask);
        }
        insert tasksForInsert;


        // Condition 2 Setup
        Lead condition2Lead = new Lead(
                LastName = 'Test Lead 2',
                Status = 'Open',
                Referral_Source_Type_Detail__c = 'Contactability',
                //CreatedDate = Datetime.now().addHours(-25)
                Salutation = 'Mr'
            );
        insert condition2Lead;

        
        // Condition 3 Setup
        Lead condition3Lead = new Lead(
                Salutation = 'Mr',
                LastName = 'Test Lead 3',
                Status = 'Not Interested',
                Referral_Source_Type_Detail__c = 'Contactability'
            );
        insert condition3Lead;


        // Condition 4 Setup
        Lead condition4Lead = new Lead(
                Salutation = 'Mr',
                LastName = 'Test Lead 4',
                Status = 'Expression of Interest',
                Referral_Source_Type_Detail__c = 'Contactability',
                Unavailable_Program_Interested_in__c = 'Test'
            );
        insert condition4Lead;


        // Condition 5 Setup
        Lead condition5Lead = new Lead(
                Salutation = 'Mr',
                LastName = 'Test Lead 5',
                Status = 'Open',
                Referral_Source_Type_Detail__c = 'Contactability'
            );
        insert condition5Lead;

        tasksForInsert = new List<Task>();
        for (Integer i = 0; i < 4; i++) {
            Task testTask = new Task(
                    WhoId = condition5Lead.Id,
                    Contact_Type__c = 'Contacted',
                    Contact_Outcome__c = 'Call Back',
                    Subject = 'Call',
                    Status = 'Completed'
                );
            tasksForInsert.add(testTask);
        }
        insert tasksForInsert;


        // Condition 6 Setup
        Lead condition6Lead = new Lead(
                Salutation = 'Mr',
                LastName = 'Test Lead 6',
                Status = 'Open',
                Referral_Source_Type_Detail__c = 'Contactability'
            );
        insert condition6Lead;

        Task condition6Task = new Task(
                WhoId = condition6Lead.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'Call Back',
                Subject = 'Call',
                Status = 'Completed'
                //CreatedDate = Datetime.now().addHours(-49)
            );
        insert condition6Task;

        // Condition 7 Setup
        Lead condition7Lead = new Lead(
                Salutation = 'Mr',
                LastName = 'Test Lead 7',
                Status = 'Open',
                Referral_Source_Type_Detail__c = 'Contactability'
            );
        insert condition7Lead;

        Task condition7Task = new Task(
                WhoId = condition6Lead.Id,
                Contact_Type__c = 'Contacted',
                Contact_Outcome__c = 'DNC Registered',
                Subject = 'Call',
                Status = 'Completed'
            );
        insert condition7Task;
    }

    static testMethod void testCase1() {
        Test.startTest();
            LeadQueueAssignmentBatch batchProcess = new LeadQueueAssignmentBatch();
            Id jobId = Database.executeBatch(batchProcess);
        Test.stopTest();

        AsyncApexJob jobRecord = [SELECT Id, JobItemsProcessed, TotalJobItems FROM AsyncApexJob WHERE Id = :jobId];
        System.debug('Job record ::: ' + jobRecord);
    }

    static testMethod void testHolidays() {
        Holiday holidayRecord = new Holiday(
                ActivityDate = Date.today(),
                IsAllDay = true,
                Name = 'It\'s a holiday!!'
            );
        insert holidayRecord;

        Test.startTest();
            LeadQueueAssignmentBatch batchProcess = new LeadQueueAssignmentBatch();
            Id jobId = Database.executeBatch(batchProcess);
        Test.stopTest();

        AsyncApexJob jobRecord = [SELECT Id, JobItemsProcessed, TotalJobItems FROM AsyncApexJob WHERE Id = :jobId];
        System.assertEquals(0, jobRecord.JobItemsProcessed);
    }
}