@isTest
private class SetUserFieldTest {
static testMethod void TestSetUserField(){
Test.startTest();
Profile adminProfile = [Select Id, Name, UserLicense.LicenseDefinitionKey From Profile Where Name = 'System Administrator'];
User u = new User(Alias='Test',Email='test.user@careersaustralia.edu.au',
           EmailEncodingKey='UTF-8',FirstName='Test',LastName='Test', LanguageLocaleKey='en_US',
           LocaleSidKey='en_GB',ProfileId=adminProfile.Id,timezonesidkey='Europe/London',Title='Test',
           username='test.user@careersaustralia.edu.au');
insert u;
Case c = new Case(Subject = 'Test Case', SuppliedEmail = u.Email, Student_Identifier__c = 'Student A');
insert c;

Test.stopTest();
}
}