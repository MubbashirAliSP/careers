/**
 * @description Central trigger handler for the Event object
 * @author Ranyel Maliwanag
 * @date 14.JUN.2016
 */
public without sharing class EventTriggerHandler {
	public static Id cdpRecordType =  Schema.SObjectType.Event.getRecordTypeInfosByName().get('CDP Event').getRecordTypeId();
	

	public static void onAfterInsert(List<Event> records) {
		Map<Id, SObject> updatesMap = new Map<Id, SObject>();

		for (Service_Cases__c serviceCase : updateCDPEventsCount(records)) {
			updatesMap.put(serviceCase.Id, serviceCase);
		}

		if (!updatesMap.isEmpty()) {
			update updatesMap.values();
		}
	}

	public static void onAfterUpdate(List<Event> records, Map<Id, Event> oldMap) {
		Map<Id, SObject> updatesMap = new Map<Id, SObject>();

		for (Service_Cases__c serviceCase : updateCDPEventsCount(records, oldMap)) {
			updatesMap.put(serviceCase.Id, serviceCase);
		}

		if (!updatesMap.isEmpty()) {
			update updatesMap.values();
		}
	}

	public static void onAfterDelete(List<Event> records) {
		Map<Id, SObject> updatesMap = new Map<Id, SObject>();

		for (Service_Cases__c serviceCase : updateCDPEventsCount(records)) {
			updatesMap.put(serviceCase.Id, serviceCase);
		}

		if (!updatesMap.isEmpty()) {
			update updatesMap.values();
		}
	}


	/**
	 * @description Wrapper method for updateCDPEventsCount without an old Map
	 * @author Ranyel Maliwanag
	 * @date 14.JUN.2016
	 */
	private static List<Service_Cases__c> updateCDPEventsCount(List<Event> records) {
		return updateCDPEventsCount(records, null);
	}


	/**
	 * @description Updates the `CDPEventsCount__c` field on the related Service Case records
	 * @author Ranyel Maliwanag
	 * @date 14.JUN.2016
	 */
	private static List<Service_Cases__c> updateCDPEventsCount(List<Event> records, Map<Id, Event> oldMap) {
		Set<Id> serviceCaseIds = new Set<Id>();
		for (Event record : records) {
			if (String.isNotBlank(record.WhatId) && String.valueOf(record.WhatId.getSObjectType()) == 'Service_Cases__c' && record.RecordTypeId == cdpRecordType && oldMap == null) {

				// Conditions:
				// * Event is related to a Service Case Record
				// * Event Record Type is CDP Event
				// * Method is fired from a non-update trigger event
				serviceCaseIds.add(record.WhatId);
			}
			
			if (oldMap != null && oldMap.containsKey(record.Id) && String.isNotBlank(oldMap.get(record.Id).WhatId) && oldMap.get(record.Id).WhatId != record.WhatId && String.valueOf(oldMap.get(record.Id).WhatId.getSObjectType()) == 'Service_Cases__c' && record.RecordTypeId == cdpRecordType) {

				// Conditions:
				// * Event was related to a previous Service Case record
				// * Event is now related to a new record
				// * Event Record Type is CDP Event
				serviceCaseIds.add(record.WhatId);
				serviceCaseIds.add(oldMap.get(record.Id).WhatId);
			}
		}

		// Query Service Cases with CDP Events
		List<Service_Cases__c> serviceCases = [SELECT Id, (SELECT Id FROM Events WHERE RecordTypeId = :cdpRecordType) FROM Service_Cases__c WHERE Id IN :serviceCaseIds];
		for (Service_Cases__c serviceCase : serviceCases) {
			// Populate the `CDPEventsCount__c` field
			serviceCase.CDPEventsCount__c = serviceCase.Events.size();
		}

		return serviceCases;
	}
}