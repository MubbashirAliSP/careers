@isTest
private class SetStudentDetails{
static testMethod void testSetStudentDetails(){
Test.startTest();
Account testAcc = new Account();
testAcc.RecordTypeId = '012900000003KLG';
testAcc.FirstName = 'test';
testAcc.LastName = 'acc';
insert testAcc;
Student__c testStudent = new Student__c();
testStudent.Student_First_Given_Name__c = testAcc.FirstName;
testStudent.Student_Last_Name__c = testAcc.LastName;
testStudent.Student_Account__c = testAcc.Id;
insert testStudent;
Case testCase = new Case();
testCase.Student_Name__c = testStudent.Id; 
insert testCase;
Test.stopTest();

List<Case> caseList = [SELECT Student_Name__c, ContactId FROM Case WHERE Id =: testCase.Id];
//system.assert(caseList[0].Student_Name__c != null);
//system.assert(caseList[0].ContactId != null);
}
}