/*
    17.OCT.2013    Warjie Malibago    Added External Id in the query list
*/
/*Controls the creation of qualifications
 *This is done only once, once unit qualification is created, this controller 
 *won't be called again for the qualification */

public with sharing class QualificationUnitsAllocationController {
    
     public List<wrapper> queryList { get; set; }
     
     public String SelectedSearch { get; set; }
     
     public String ref{get;set;}
     
     public String searchText { get; set; }
     
     public Wrapper w {get;set;}
     
     public boolean displayPopup {get; set;}
     
     public List<Training_Package_Unit__c> queryResult {get; set;}
     
     public List<Unit__c> unitsOfStudyList {get;set;}
     
     public final Qualification__c Qual = [SELECT Id, Training_Package__r.Training_Package_Name__c, Training_Package__c FROM Qualification__c WHERE id = :ApexPages.currentPage().getParameters().get('Id')];
     
     //WBM 10.17.2013 'external'
     public QualificationUnitsAllocationController(){w = new Wrapper(false,'aaa','bbb','ccc', 'tpname', 'external');}
    
     
     public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Code','Code'));
            options.add(new SelectOption('Unit Name','Unit Name'));
            return options;
     }
     
     
     public pageReference cancel() {
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id') );
        
     }
     
     public pageReference addQualificationUnits() {
        
        List<Qualification_Unit__c> newUnits = new List<Qualification_Unit__c>();
        
        MAP<String,Id>StageMap = new Map<String,Id>();
        
        for(Stage__c st : [Select Id, Name FROM STAGE__c]) {
            
            StageMap.put(st.name, st.id);
        }
        
        for(wrapper units : queryList) {
            
            if(units.selected) {
            
            Qualification_Unit__c unit = new Qualification_Unit__c (
            
                Qualification__c = ApexPages.currentPage().getParameters().get('Id'),
                Unit__c = units.tid,
                
                Stage__c = StageMap.get(units.SelectedStage),
                Unit_Type__c = units.selectedType
                
            
            
                );
                
                newUnits.add(unit);
            
            }
        
            
        }
        try{
            insert newUnits;    
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id') );
        }
        catch(Exception e){
             ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
             return null;
        }
        
        
        
        
     }
     
      public void closePopup() {displayPopup = false;}
        
    
 
    public PageReference showPopup() {
       
        displayPopup = true;
           
        return null;
        
        
    }
    
    public pageReference retrieveUnitsOfStudy() {
        unitsOfStudyList = new List<Unit__c>();
        
        for(Unit__c u : [Select Id, Name, Unit_Name__c From Unit__c Where RecordType.Name = 'Unit Of Study' And Parent_UoC__r.Unit_Name__c = : ApexPages.currentPage().getParameters().get('uos') LIMIT 25])
            unitsOfStudyList.add(u);
            
        showPopUp();
        return null;
    }
     
        
      public pageReference dosearch() {
        
        Set<Id> existingUnitId = new Set<Id>();
        
        List<Qualification_Unit__c> existingUnits = new List<Qualification_Unit__c>([SELECT id, Unit__c
                                                          FROM Qualification_Unit__c
                                                          WHERE Qualification__c = : Qual.id LIMIT 1000]);
                                                          
        for(Qualification_Unit__c u : existingUnits)
            existingUnitId.add(u.Unit__c);
        
        ref='';
        if((searchText.length() <3) && (SelectedSearch == 'Unit Name')){
            ref = 'Please enter at least 3 characters for Unit Name';
        } 
        else if(searchText.length() <2){
            ref = 'Please enter at least 2 characters for Unit Code';
        }
        else {
            //WBM 10.17.2013 External_Id__c in qryString
             String strLikeVal = '%' + searchText + '%';
             transient String qryString;
                if(SelectedSearch == 'Unit Name')
                    qryString = 'SELECT Id, Training_Package__r.Training_Package_Name__c, Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name, Unit_of_Competency__r.External_Id__c FROM Training_Package_Unit__c where Unit_of_Competency__r.Unit_Name__c like : strLikeVal and Unit_of_Competency__r.This_Unit_is_Inactive__c = false order by Unit_of_Competency__r.Name limit 1000';
                else 
                    qryString = 'SELECT Id, Training_Package__r.Training_Package_Name__c, Unit_of_Competency__r.Unit_Name__c, Unit_of_Competency__r.Name, Name, Unit_of_Competency__r.External_Id__c FROM Training_Package_Unit__c where Unit_of_Competency__r.Name like : strLikeVal and Unit_of_Competency__r.This_Unit_is_Inactive__c = false order by Unit_of_Competency__r.Name limit 1000';
           
            queryResult = Database.query(qryString);
            queryList = new List<wrapper>();
            
            
            
            for(Training_Package_Unit__c tu: queryResult){
                 if(!existingUnitId.contains(tu.Unit_of_Competency__c))
                     system.debug('**existingUnitId: ' + existingUnitId);
                     system.debug('**Unit of competency: ' + tu.Unit_of_Competency__c);
                     system.debug('**Id: ' + tu.Id);
                     //WBM 10.17.2013 tu.External_Id__c
                    queryList.add(new wrapper(false, tu.Unit_of_Competency__c,tu.Unit_of_Competency__r.Unit_Name__c, tu.Unit_of_Competency__r.Name, tu.Training_Package__r.Training_Package_Name__c, tu.Unit_of_Competency__r.External_Id__c));

            }      
            
        }
            
         
        
        
        return null;
    }   
        
     
    
     public class wrapper
     {
    
            public String un {get;set;}
            public String sfid {get;set;}
            public String tid {get;set;}
            //WBM 10.17.2013 ext
            public String ext {get; set;}
            public Boolean selected {get;set;}
            public String Stage {get;set;}
            public String SelectedStage {get;set;}
            public String trainingPackageName {get;set;}
            public String selectedType {get;set;}
   
    
            public List<SelectOption>getStages() {
                     List<SelectOption> options = new List<SelectOption>();
                     options.add(new SelectOption('Stage 1','Stage 1'));
                     options.add(new SelectOption('Stage 1a','Stage 1a'));
                     options.add(new SelectOption('Stage 1b','Stage 1b'));
                     options.add(new SelectOption('Stage 2','Stage 2'));
                     options.add(new SelectOption('Stage 2a','Stage 2a'));
                     options.add(new SelectOption('Stage 2b','Stage 2b'));
                     options.add(new SelectOption('Stage 3','Stage 3'));
                     options.add(new SelectOption('Stage 4','Stage 4'));
                     options.add(new SelectOption('Stage 5','Stage 5'));
                     options.add(new SelectOption('Stage 6','Stage 6'));
                     options.add(new SelectOption('Stage 7','Stage 7'));
                     options.add(new SelectOption('Stage 8','Stage 8'));
                     options.add(new SelectOption('Stage 9','Stage 9'));
                     options.add(new SelectOption('Stage 10','Stage 10'));
                    
             return options;
            }
            
            public List<SelectOption>getTypes() {
                 List<SelectOption> options = new List<SelectOption>();
                 
                     
                 options.add(new SelectOption('Core','Core'));
                 options.add(new SelectOption('Elective','Elective'));
                
                 
                return options;
            }
    
            //WBM 10.17.2013 external, ext
             public wrapper(Boolean sel,String id,String a, String b, String tpName, String external)
             {
                selected = sel;
                tid =id;
                un = a;        
                sfid = b;           
                Stage = SelectedStage;
                trainingPackageName = tpName;        
                ext = external;            
             } 
    
    
            
        }
        
        @isTest static void unitTest() {
        
        Training_Package__c tp = new Training_Package__c(name = 'TpTest', Training_Package_Name__c = 'TpTest');
        Training_Package_Unit__c tpu = new Training_Package_Unit__c();
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c(); 
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        
            Test.startTest();
            insert tp;
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
           // qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            //countryAU = TestCoverageUtilityClass.queryAustralia();
          /*  state = TestCoverageUtilityClass.createState(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
            qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() + 365;
            enrl.Delivery_Location__c = loc2.id;
            enrl.Enrolment_Status__c = 'Completed';
           
            //enrl.Overseas_Country__c = null;
            insert enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            eus.Census_Date__c = system.today();
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;
            */
            tpu = new Training_Package_Unit__c(Unit_of_Competency__c = unit.Id, Training_Package__c = tp.Id);
            
            insert tpu;
            
            PageReference testPage = Page.QualificationUnitsAllocation;
            Test.setCurrentPage(testPage);
            testPage.getParameters().put('Id', qual.Id);
            QualificationUnitsAllocationController controller = new QualificationUnitsAllocationController();
            
            controller.searchText = 'Test Unit';
            
            controller.getItems();
            
            controller.cancel();
            
            controller.dosearch();
            
            controller.addQualificationUnits();
            
            controller.w.getStages();
            
            controller.w.getTypes();
            
            
            Test.stopTest();
        
    }
        
    
    
}