/**
 * @description Test class for ApplicationsLandingCC
 * @author Ranyel Maliwanag
 * @date 15.JUN.2015
 */
@isTest
public with sharing class ApplicationsLandingCC_Test {
    @testSetup
    static void setupController() {
        ApplicationsTestUtilities.initialiseCountryAndStates();

        GuestLogin__c guestLogin = new GuestLogin__c(
                FirstName__c = 'Tom',
                LastName__c = 'Riddle',
                Email__c = 'triddle@hogwarts.edu',
                Password__c = 'LordVoldemort',
                Phone__c = '123'
            );
        insert guestLogin;

        State__c testState = [SELECT Id FROM State__c LIMIT 1];
        ApplicationForm__c form1 = new ApplicationForm__c(
                StudentCredentialsId__c = guestLogin.Id,
                StateId__c = testState.Id,
                StaffId__c = UserInfo.getUserId()
            );
        insert form1;
    }

    static testMethod void invalidAuthenticationControllerTest() {
        GuestLogin__c guestLogin = [SELECT Id FROM GuestLogin__c];
        String tamperedId = guestLogin.Id;
        tamperedId = '001' + tamperedId.right(15);

        Test.startTest();
            // Landing page has been accessed with a tampered authentication Id
            // Assertion: User is unauthenticated
            Test.setCurrentPage(Page.ApplicationsLanding);
            ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('authenticationId', tamperedId, null, -1, true)});
            ApplicationsLandingCC controller = new ApplicationsLandingCC();
            //System.assert(controller.isAuthenticated);
            controller = new ApplicationsLandingCC();
            System.assert(!controller.isAuthenticated);
        Test.stopTest();
    }

    static testMethod void resetMessageCookieControllerTest() {
        Test.startTest();
            Test.setCurrentPage(Page.ApplicationsLanding);
            ApexPages.currentPage().setCookies(new List<Cookie>{new Cookie('notificationMessage', 'Lumos', null, -1, true)});
            ApplicationsLandingCC controller = new ApplicationsLandingCC();
            System.assert(ApexPages.hasMessages());
            controller = new ApplicationsLandingCC();
        Test.stopTest();
    }

    static testMethod void authenticateLoginTest() {
        Test.startTest();
            // Initial page load
            // Assertion: User is not authenticated
            Test.setCurrentPage(Page.ApplicationsLanding);
            ApplicationsLandingCC controller = new ApplicationsLandingCC();
            System.assert(!controller.isAuthenticated);

            // User enters non-existing credentials
            controller.guestLogin.Email__c = 'adumbledore@hogwarts.edu';
            controller.guestLogin.Password__c = 'TheElderWand';
            controller.authenticateLogin();
            System.assert(!controller.isAuthenticated);
            System.assert(ApexPages.hasMessages());

            // User enters incorrect credentials
            controller.guestLogin.Email__c = 'triddle@hogwarts.edu';
            controller.guestLogin.Password__c = 'HeWhoShallNotBeNamed';
            controller.authenticateLogin();
            System.assert(!controller.isAuthenticated);

            // User enters correct credentials
            controller.guestLogin.Email__c = 'triddle@hogwarts.edu';
            controller.guestLogin.Password__c = 'LordVoldemort';
            Test.setCurrentPage(controller.authenticateLogin());
            controller = new ApplicationsLandingCC();
            System.assert(controller.isAuthenticated);
        Test.stopTest();
    }

    static testMethod void logoutTest() {
        // Initial setup: User is logged in
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApplicationsLandingCC controller = new ApplicationsLandingCC();
        controller.guestLogin.Email__c = 'triddle@hogwarts.edu';
        controller.guestLogin.Password__c = 'LordVoldemort';
        Test.setCurrentPage(controller.authenticateLogin());
        controller = new ApplicationsLandingCC();

        Test.startTest();
            // Assert initial condition: User is authenticated
            System.assert(controller.isAuthenticated);

            // User logs out
            // Assertion: User is not authenticated anymore
            Test.setCurrentPage(controller.logout());
            controller = new ApplicationsLandingCC();
            System.assert(!controller.isAuthenticated);
        Test.stopTest();
    }

    static testMethod void launchNewApplicationTest() {
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApplicationsLandingCC controller = new ApplicationsLandingCC();
        
        Test.startTest();
            // Assert initial condition
            System.assertEquals(Page.ApplicationsLanding.getUrl(), ApexPages.currentPage().getUrl());
            Test.setCurrentPage(controller.launchNewApplication());
            System.assertEquals(Page.ApplicationsApply.getUrl(), ApexPages.currentPage().getUrl());
        Test.stopTest();
    }

    static testMethod void pageLoadActionAuthenticatedTest() {
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApplicationsLandingCC controller = new ApplicationsLandingCC();

        Test.startTest();
            PageReference result = controller.evaluateParameters();
            //System.assertEquals(null, result);

            controller.guestLogin.Email__c = 'triddle@hogwarts.edu';
            controller.guestLogin.Password__c = 'LordVoldemort';
            Test.setCurrentPage(controller.authenticateLogin());
            controller = new ApplicationsLandingCC();
            System.assert(controller.isAuthenticated);
            System.assertNotEquals(null, controller.guestLogin.Id);

            ApexPages.currentPage().getParameters().put('confirm', controller.guestLogin.Id);
            result = controller.evaluateParameters();
            System.assertNotEquals(null, result);
            Test.setCurrentPage(result);
            System.assertEquals(Page.ApplicationsConfirm.getUrl() + '?confirm=' + controller.guestLogin.Id, ApexPages.currentPage().getUrl());

            system.debug(controller.hasNext);
            system.debug(controller.hasPrevious);
            system.debug(controller.pageNumber);
            system.debug(controller.maxPageNumber);
            controller.next();
            controller.previous();
        Test.stopTest();
    }

    static testMethod void pageLoadActionUnauthenticatedTest() {
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApplicationsLandingCC controller = new ApplicationsLandingCC();
        GuestLogin__c guestLogin = [SELECT Id FROM GuestLogin__c];

        Test.startTest();
            ApexPages.currentPage().getParameters().put('confirm', guestLogin.Id);
            PageReference result = controller.evaluateParameters();
            //System.assertEquals(null, result);

            controller.guestLogin.Email__c = 'triddle@hogwarts.edu';
            controller.guestLogin.Password__c = 'LordVoldemort';
            Test.setCurrentPage(controller.authenticateLogin());
            controller = new ApplicationsLandingCC();
            System.assertEquals(Page.ApplicationsConfirm.getUrl() + '?confirm=' + controller.guestLogin.Id, ApexPages.currentPage().getUrl());
        Test.stopTest();
    }


    static testMethod void cancelApplicationFormTest() {
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApplicationsLandingCC controller = new ApplicationsLandingCC();
        ApplicationForm__c form1 = [SELECT Id FROM ApplicationForm__c];

        Test.startTest();
            //System.assert(controller.studentApplicationForms.size() > 0);
            //System.assert(controller.studentApplicationFormsMap.containsKey(form1.Id));

            //controller.formForCancellationId = form1.Id;
            //System.debug('::: cancelling form');
            //controller.cancelApplicationForm();
            //System.debug('::: form cancelled');
        Test.stopTest();
    }


    static testMethod void businessParameterLoginTest() {
        // Record creations
        Account businessAccount = ApplicationsTestUtilities.initialiseBusinessAccounts();
        insert businessAccount;

        // Setup
        Test.setCurrentPage(Page.ApplicationsLanding);
        ApexPages.currentPage().getParameters().put('bid', businessAccount.BusinessIdentifier__c);
        Test.startTest();
            ApplicationsLandingCC controller = new ApplicationsLandingCC();

            // Assertion: Business account is not null
            System.assertNotEquals(null, controller.businessAccount);

            // Assertion: Business account in controller is the business account expected
            System.assertEquals(businessAccount.Id, controller.businessAccount.Id);

            // Assertion: Guest login has record type of Student

            // Assertion: Guest login has no Business Account related to it

            // Action: User fills in login form and clicks on login

            // Assertion: Guest login is now associated to Business Account
        Test.stopTest();
    }

}