/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for ServiceCaseHandler Class and ServiceCaseHandlerAfterUpdate Trigger
Test Class:
History
08/01/2012     Created      Sairah Hadjinoor
------------------------------------------------------------*/

@isTest
private class ServiceCaseHandler_Test {

    static testMethod void testServiceCaseTrigger() {
        Account acc = new Account();
        Service_Cases__c sc = new Service_Cases__c();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='ServiceCase_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
                    
        User u2 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='ServiceCase_User2', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='standuser2@testorg.com');
        
        
        system.runAs(u){
        	test.startTest();
        		Link__c link = new Link__c();
        		link.Name = 'Salesforce Link';
        		insert link;

        		insert u2;
        		system.debug('u2***'+u2);
        		acc = TestCoverageUtilityClass.createStudent();
        		
        		sc.Account_Student_Name__c = acc.Id;
		        sc.Case_Origin__c = 'Web';
		        sc.Status__c = 'New';
		        sc.OwnerId = u.id;
		        insert sc;
		        system.debug('1. sc.OwnerId***'+sc.OwnerId);
		        sc.OwnerId = u2.Id;
		        update sc;
		        system.assertEquals(u2.Id,sc.OwnerId);
        	test.stopTest();
        }
        /*Id recrdType = [select Id from RecordType where Name = 'Student' LIMIT 1].Id;
        //Test.startTest();
        List<Account> accList = new List<Account>();
            for(integer i=0; i<=100;i++){
                Account sacct = new Account();
                sacct.RecordTypeId = recrdType;
                sacct.LastName = 'Test student Account';
                sacct.PersonMailingCountry = 'AUSTRALIA';
                sacct.PersonOtherCountry = 'AUSTRALIA';
                sacct.Student_Identifer__c = 'H75880' +i;
                sacct.FirstName = 'You'+i;
                accList.add(sacct);
            
            }
        insert accList;
        
        
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        List<User> userList = new List<User>();
            for(integer i=0; i<=2; i++){
                User u = new User(Alias = 'standt', Email=i+'standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing'+i,FirstName='Test'+i, LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test'+i);
                 userList.add(u);
            }
        insert userList;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        User u2 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        system.runAs(u){
	        Test.startTest();
		        List<Service_Cases__c> newService = new List<Service_Cases__c>();
		        
		            for(integer i=0; i<=100;i++){
		                Service_Cases__c sc = new Service_Cases__c();
		                sc.Account_Student_Name__c = accList[i].id;
		                sc.Case_Origin__c = 'Web';
		                sc.Status__c = 'New';
		                sc.Case_Owner__c = u.id;
		                newService.add(sc);
		            }
		        insert newService;
		        
		        for(Service_Cases__c sc: newService){
		            sc.Case_Owner__c = u2.Id;
		        }
		        update newService;
		        
	        
	        Test.stopTest();
        }
        System.assertEquals(accList.size(), 101);*/
    }
}