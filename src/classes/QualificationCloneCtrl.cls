public class QualificationCloneCtrl {
    private Qualification__c newRec {get;set;}    
    public Boolean allChecked { get; set; }
    public List<QUWrapper> allQualificationUnits { get; set; }
    
    
    public QualificationCloneCtrl(ApexPages.StandardController controller) {
        newRec = (Qualification__c) controller.getRecord();
        Id qId = newRec.Id;
        List<Qualification_Unit__c> quList = Database.query('Select Id, Stage__c, Name, Unit__c, Unit__r.Name, Unit_Type__c from Qualification_Unit__c where Qualification__c = : newRec.qId');
        allQualificationUnits = new List<QUWrapper>();
        allChecked = false;
        newRec.Award_Based_On__c = newRec.Id;
        for(Qualification_Unit__c qu: quList){
            allQualificationUnits.add(new QUWrapper(qu));
        }
        
    }
    public PageReference CheckAll(){
        for(QUWrapper qu : allQualificationUnits){
            qu.selected = allChecked;
        }
  
        return null;
    }
    /*public PageReference cancelPage(){
        PageReference pageRef = new PageReference('/apex/QualificationDetail?id=' + newRec.Id);
                
        return pageRef;
    }*/
    public PageReference saveNew(){
  
        Qualification__c qualClone = new Qualification__c(Name = newRec.Name, Qualification_Name__c = newRec.Qualification_Name__c, Training_Package_Order__c = newRec.Training_Package_Order__c, Description__c = newRec.Description__c,
                        Qualification_Category__c = newRec.Qualification_Category__c,
                        Recognition_Status__c = newRec.Recognition_Status__c,
                        VFH_Approved__c = newRec.VFH_Approved__c, Include_in_CCQI_export__c = newRec.Include_in_CCQI_export__c,
                        Award_Based_On__c = newRec.Id, Field_of_Education__c = newRec.Field_of_Education__c,
                        ANZSCO__c = newRec.ANZSCO__c, Vocational_Training_Area__c = newRec.Vocational_Training_Area__c,
                        Core_Units_Required__c = newRec.Core_Units_Required__c, Elective_Units_Required__c = newRec.Elective_Units_Required__c,
                        Active__c = newRec.Active__c, Effective_Date__c = newRec.Effective_Date__c, Accredited_Until__c = newRec.Accredited_Until__c, Expiry_Date__c = newRec.Expiry_Date__c);
        insert qualClone;
        
        List<Qualification_Unit__c> quList = new List<Qualification_Unit__c>();
        for(QUWrapper qu : allQualificationUnits){
            if(qu.selected){
                Qualification_Unit__c newQU = new Qualification_Unit__c(Qualification__c = qualClone.Id, Unit__c = qu.qu.Unit__c, Unit_Type__c = qu.qu.Unit_Type__c, Stage__c = qu.qu.Stage__c);
                quList.add(newQU);
            }
        }
        insert quList;
        PageReference qualPage = new ApexPages.StandardController(qualClone).view();
        qualPage.setRedirect(true);
        return qualPage;
        
         
    }
     public class QUWrapper {
      
      public Qualification_Unit__c qu{get; set;}
            public Boolean selected {get; set;}
            
            public QUWrapper(Qualification_Unit__c c){
                qu = c;
                selected = false;
            }
            
     }
}