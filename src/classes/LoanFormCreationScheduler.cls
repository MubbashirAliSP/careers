/**
 * @description Schedulable interface implementation for the LoanFormCreation batch
 * @author Ranyel Maliwanag
 * @date 03.NOV.2015
 */
public without sharing class LoanFormCreationScheduler implements Schedulable {
    public LoanFormCreationBatch batchProcess;

    public LoanFormCreationScheduler() {
        batchProcess = new LoanFormCreationBatch(null);
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new LoanFormCreationBatch(null));
    }
}