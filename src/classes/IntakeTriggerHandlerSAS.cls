/**
 * @description Central trigger handler for the `Intake__c` object
 * @author Ranyel Maliwanag
 * @date 16.MAR.2016
 */
public without sharing class IntakeTriggerHandlerSAS {
	/**
	 * @description Handles all after insert operations for the Intake object
	 * @author Ranyel Maliwanag
	 * @date 16.MAR.2016
	 */
	public static void onAfterInsert(List<Intake__c> records) {
		List<Intake__c> withAdjustedCensusDates = new List<Intake__c>();
		for (Intake__c record : records) {
			if (record.Adjusted_Census_Date__c != null) {
				withAdjustedCensusDates.add(record);
			}
		}

		adjustRelatedCensusDates(withAdjustedCensusDates);
	}

	/**
	 * @description Handles all after update operations for the Intake object
	 * @author Ranyel Maliwanag
	 * @date 16.MAR.2016
	 */
	public static void onAfterUpdate(List<Intake__c> records, Map<Id, Intake__c> oldRecordsMap) {
		List<Intake__c> withAdjustedCensusDates = new List<Intake__c>();
		for (Intake__c record : records) {
			if (record.Adjusted_Census_Date__c != oldRecordsMap.get(record.Id).Adjusted_Census_Date__c) {
				withAdjustedCensusDates.add(record);
			}
		}
		adjustRelatedCensusDates(withAdjustedCensusDates);
	}


	/**
	 * @description Updates the census date on the census records that are related to the given intakes
	 * @author Ranyel Maliwanag
	 * @date 16.MAR.2016
	 */
	private static void adjustRelatedCensusDates(List<Intake__c> records) {
		// Collection of all intake Ids for re-querying
		Set<Id> intakeIds = new Set<Id>();

		for (Intake__c record : records) {
			intakeIds.add(record.Id);
		}

		// Collection of all Census records for update
		List<Census__c> censusesForUpdate = new List<Census__c>();

		// Query all affected intakes and their related census records
		for (Intake__c record : [SELECT Id, Census_Date__c, (SELECT Id FROM Censuses__r) FROM Intake__c WHERE Id IN :intakeIds]) {
			for (Census__c census : record.Censuses__r) {
				// Update the census date on the census record to the adjusted census date
				census.CensusDate__c = record.Census_Date__c;
				censusesForUpdate.add(census);
			}
		}

		update censusesForUpdate;
	}
}