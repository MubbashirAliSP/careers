/**
 * @description Utility Class for Test Coverages
 * @author      Janella Lauren Canlas
 * @Company     CloudSherpas
 * @date        22.JAN.2013
 *
 * HISTORY
 * - 22.JAN.2013    Janella Canlas      Created.
 * - 08.APR.2014    Michelle Magsarili  Change Status to Active to pass Enrolment Validation in createEnrolmenteuos method
 * - 09.AUG.2014    Yuri Gribanov       Major fix to queries to include recordtype IDS.
 * - 07.NOV.2014    Sairah Hadjinoor    Added Client_Occupation_Identifier__c field on Account creation
 */
@isTest
public class TestCoverageUtilityClass {
    //Create ANZSCO__c record
    public static ANZSCO__c createANZSCO(){
        ANZSCO__c anz = new ANZSCO__c();
        anz.Name = 'Test ANZSCO';
        insert anz;
        return anz;
    }
    //Create Field of Education Record
    public static Field_of_Education__c createFieldOfEducation(){
        Field_of_Education__c fld = new Field_of_Education__c();
        //Field_of_Education__c fld2 = new Field_of_Education__c();
        Id recrdType = [select Id from RecordType where DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        fld.RecordTypeId = recrdType;
        fld.Name = 'Test Field of Education';
        //fld2.RecordTypeId = recrdType2;
        //fld2.Name = 'Test Field of Education2';     
        insert fld;
       // insert fld2;
        return fld;
        //return fld2;
    }

    public static Field_of_Education__c createFieldOfEducation2(){
        Field_of_Education__c fld2 = new Field_of_Education__c();
        //Field_of_Education__c fld2 = new Field_of_Education__c();
        Id recrdType = [select Id from RecordType where DeveloperName = 'Division' and SObjectType = 'Field_of_Education__c' LIMIT 1].Id;
        fld2.RecordTypeId = recrdType;
        fld2.Name = 'Test Field of Education';
        //fld2.RecordTypeId = recrdType2;
        //fld2.Name = 'Test Field of Education2';     
        insert fld2;
       // insert fld2;
        return fld2;
        //return fld2;
    }


    //Create Unit Record
    public static Unit__c createUnit(){//change back to Competency jcanlas 26MAR2013
        Id recrdType = [select Id from RecordType where DeveloperName = 'Unit_of_Competency' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c un = new Unit__c();
        un.Name = 'Test Unit';
        un.RecordTypeId = recrdType;
        un.Include_In_CCQI_Export__c = true;
        insert un;

        createUnitHoursAndPoints(un.Id, null);
        return un;
    }
    //Create Study Unit Record
    public static Unit__c createUnitofStudy(Id unitC){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Unit_of_Study' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c un = new Unit__c();
        un.Name = 'Test Unit';
        un.RecordTypeId = recrdType;
        un.Parent_UoC__c = unitC;
        insert un;
        return un;
    }
    //Create Unit Record
    public static Unit__c createUnitWithParent(Id parentId){
        Id recrdType = [select Id from RecordType where Name = 'Unit of Study' and sObjectType ='Unit__c' LIMIT 1].Id;
        Unit__c un = new Unit__c();
        un.Name = 'Test Unit';
        un.RecordTypeId = recrdType;
        un.Parent_UoC__c = parentId;
        insert un;
        return un;
    }
    //Create Intake
    public static Intake__c createIntake(Id qualId, Id program, Id location, Id fundingStream){
        VFH_Controller__c vfh = createVFHController();
        Intake__c i = new Intake__c();
        //i.Name = 'Test Intake';
        i.Start_Date__c = date.today();
        i.End_Date__c = date.today() + 30;
        i.Qualification__c = qualId;
        i.VFH_Controller__c = vfh.Id;
        i.Program__c = program;
        i.Training_Delivery_Location__c = location;
        i.Funding_Stream__c = fundingStream;
        insert i;
        return i;
    }
    //Create VFH Controller
    public static VFH_Controller__c createVFHController(){
        VFH_Controller__c v = new VFH_Controller__c();
        v.Report_End_Date__c = date.today() + 50;
        v.Report_Start_Date__c = date.today();
        insert v;
        return v;
    }
    //Create Intake Unit Record
    public static Intake_Unit__c createIntakeUnit(Id intakeId, Id unitId){
        Intake_Unit__c i = new Intake_Unit__c();
        i.Intake__c = intakeId;
        i.Unit_of_Study__c = unitId;
        insert i;
        return i;
    }
    
    //Create Location record
    public static Locations__c createLocation(Id countryId, Id pl){
        Id recrdtype = [select id from RecordType where DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.RecordTypeId = recrdtype;
        l.Name = 'Sample Room';
        l.Country__c = countryId;
        l.Parent_Location__c = pl;
        insert l;
        return l;
    }
    
    //Create Location record
    public static Locations__c createLocationWithTO(Id countryId, Id pl, Id trainingOrg){
        Id recrdtype = [select id from RecordType Where DeveloperName='Training_Delivery_Location' AND SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.RecordTypeId = recrdtype;
        l.Name = 'Sample Room';
        l.Country__c = countryId;
        l.Parent_Location__c = pl;
        l.Training_Organisation__c = trainingOrg;
        insert l;
        return l;
    }
    
    //Create Parent Location Record
    public static Locations__c createParentLocation(Id country, Id state, Id loading){
        Id recrdtype = [select id from RecordType where DeveloperName='Parent_Location' And SObjectType = 'Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.Name = 'name';
        l.Country__c = country;
        l.State_Lookup__c = state;
        l.Location_Loading__c = loading;
        l.RecordTypeId = recrdtype;
        //l = [select id, Country__c from Locations__c where Country__c != null and RecordType.Name ='Parent Location' LIMIT 1];
        insert l;
        return l;
    }
    
    public static Location_Loadings__c createLocationLoadings(Id state){
        Location_Loadings__c l = new Location_Loadings__c();
        l.Name = 'loading name';
        l.State__c = state;
        l.Price_Factor__c = 50;
        insert l;
        return l;
    }
    
    //Create Parent Location Record
    public static Locations__c createParentLocationRoom(){
        Id recrdtype = [select id from RecordType where Name='Room'].Id;
        Locations__c l = new Locations__c();
        l = [select id, Country__c from Locations__c where Country__c != null and RecordType.Name ='Parent Location' LIMIT 1];
        return l;
    }
    //Create Location record with Parent - Room Recordtype
    public static Locations__c createLocationWithParent(Id countryId, Id acc, Id parentLoc, Id state ){
        Id recrdtype = [select id from RecordType where DeveloperName='Room' And SObjectType ='Locations__c'].Id;
        Locations__c l = new Locations__c();
        l.RecordTypeId = recrdtype;
        l.Name = 'Sample Room';
        l.Country__c = countryId;
        l.Training_Organisation__c = acc;
        l.Parent_Location__c = parentLoc;
        l.State_Lookup__c = state;
        insert l;
        return l;
    }
    
    //Create Location record with Parent - Training Delivery Location Recordtype
    public static Locations__c createLocationWithParentTR(Id countryId, Id acc, Id parentLoc, Id state ){
        Id recrdtype = [select id from RecordType where DeveloperName='Training_Delivery_Location' And SObjectType = 'Locations__c'].Id;
        Locations__c lo = new Locations__c();
        lo.RecordTypeId = recrdtype;
        lo.Name = 'Sample Training';
        lo.Country__c = countryId;
        lo.Training_Organisation__c = acc;
        lo.Parent_Location__c = parentLoc;
        lo.State_Lookup__c = state;
        insert lo;
        return lo;
    }
    
    //Create Country record
    public static Country__c createCountry(){
        Country__c c = new Country__c();
        c.Country_Code__c = '1123';
        c.Name = 'Country Name';
        insert c;
        return c;
    }
    //Query Australia Country Record
    public static Country__c queryAustralia(){
        Country__c c = new Country__c();
        c.Country_Code__c = '1123';
        c.Name = 'Australia';
        insert c;
        return c;
    }
    //Create Qualification Record
    public static Qualification__c createQualification(String anzId, String fldId){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Qualification'  And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = recrdType;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anzId;
        qual.Field_of_Education__c = fldId;
        qual.Recognition_Status__c = '14 - Other Courses';     
        insert qual;
        return qual;
    }
    //Create Tailored Qualification Record
    public static Qualification__c createTailoredQualification(String anzId, String fldId){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Tailored_Qualification' And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = recrdType;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anzId;
        qual.Field_of_Education__c = fldId;
        qual.Recognition_Status__c = '14 - Other Courses';     
        insert qual;
        return qual;
    }
    
    //Create Tailored Qualification Record
    public static Qualification__c createTailoredQualificationAward(String anzId, String fldId, String QualId){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Tailored_Qualification' And SObjectType = 'Qualification__c' LIMIT 1].Id;
        Qualification__c qual = new Qualification__c();
        qual.RecordTypeId = recrdType;
        qual.Name = 'Test Qualification';
        qual.Qualification_Name__c = 'Test Qualification Name';
        qual.ANZSCO__c = anzId;
        qual.Field_of_Education__c = fldId;
        qual.Recognition_Status__c = '14 - Other Courses';
        qual.Award_based_on__c = QualId;
        insert qual;
        return qual;
    }
    
    //create Qualification Unit Record
    public static Qualification_Unit__c createQualificationUnit(String qualId, String unitId){
        Qualification_Unit__c qu = new Qualification_Unit__c();
        qu.Qualification__c = qualId;
        qu.Unit__c = unitId;
        qu.Unit_Type__c = 'Core';
        insert qu;
        return qu;
    }
    //create Class Configuration Record
    public static Class_Configuration__c createClassConfiguration(Id intakeId, Id iuId){
        Id recrdtype = [select id from RecordType where DeveloperName='Reoccuring_Class' AND SObjectType = 'Class_Configuration__c'].Id;
        Class_Configuration__c c = new Class_Configuration__c();
        c.RecordTypeId = recrdtype;
        c.Intake__c = intakeId;
        c.Intake_Unit__c = iuId;
        c.Day__c = 'Monday';
        c.Class_Start_Time__c = '9:00 AM';
        c.Class_End_Time__c = '12:00 PM';
        insert c;
        return c;
    }
    //create Language Record
    public static Language__c createLanguage(){
        Language__c l = new Language__c();
        l.Name = 'Test Language';
        l.Code__c = '111';
        insert l;
        return l;
    }
    //create Account Record (Student for EUOS)
    public static Account createStudentEUOS(Id language){
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student';
        sacct.FirstName = 'First Name';
        sacct.Main_Language_Spoken_at_Home__c = language;
        sacct.Student_Identifer__c = 'H0001';
        sacct.PersonMobilePhone = '61847292111';
        sacct.Year_Highest_Education_Completed__c = '2001';
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.Salutation = 'Mr.';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.PersonMailingPostalCode = '4005';
        sacct.PersonMailingState = 'Queensland';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        insert sacct;
        return sacct;

    }
    //create Account Record (Student)
    public static Account createStudent(){
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student Account';
        sacct.FirstName = 'First Name';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.PersonMailingPostalCode = '4005';
        sacct.PersonMailingState = 'Queensland';
        sacct.PersonMobilePhone = '61847292111';
        sacct.Student_Identifer__c = 'H89373';
        sacct.PersonBirthDate = system.today() - 5000;
        sacct.Year_Highest_Education_Completed__c = '2000';
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.Sex__c = 'F - Female';
        sacct.Salutation = 'Mr.';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        sacct.Client_Occupation_Identifier__c = '1 - Manager';
        insert sacct;
        return sacct;

    }

    //create Account Record (Training Organisation)
    public static Account createTrainingOrganisation(){
        Id recrdType = [select Id from RecordType where DeveloperName = 'Training_Organisation' And SObjectType = 'Account' LIMIT 1].Id;
        Account sacct = new Account();
        sacct.RecordTypeId = recrdType;
        sacct.Name = 'training';
        sacct.BillingCountry = 'NEW ZEALAND';
        sacct.Training_Org_Identifier__c = 'test';
        sacct.Training_Org_Type__c = 'test';
        sacct.Client_Occupation_Identifier__c = '1 - Manager';
        
        insert sacct;
        return sacct;
    }
    //create Staff Member Record
    public static Staff_Member__c createStaffMember(Id intake, Id user){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Staff_Member__c st = new Staff_Member__c();
        st.Intake__c = intake;
        st.Trainer__c = user;
        //insert st;
        return st;          
    }
    //create Attendance Record
    public static Attendance_Types__c createAttendanceType(){
        Attendance_Types__c attend = new Attendance_Types__c();
        attend.Name = 'Attendance Type';
        attend.Code__c = 'X';
        insert attend;
        return attend;
    }
    //create Class Enrolment Record
    public static Class_Enrolment__c createClassEnrolment(Id clas, Id eiu, Id attend){
        Class_Enrolment__c c = new Class_Enrolment__c();
        c.Class__c = clas;
        c.Enrolment_Intake_Unit__c = eiu;
        c.Attendance_Type__c = attend;
        //insert c;
        return c;
    }
    //Create Enrolment Intake Unit Record
    public static Enrolment_Intake_Unit__c createEnrolmentIntakeUnit(Id enrol, Id intakeUnit, Id enrolUnit){
        Enrolment_Intake_Unit__c e = new Enrolment_Intake_Unit__c();
        e.Enrolment__c = enrol;
        e.Intake_Unit__c = intakeUnit;
        e.Enrolment_Unit__c = enrolUnit;
        //insert e;
        return e;
    }
    //Create Enrolment Record
    public static Enrolment__c createEnrolment(Id student,Id qual, id liq, id loc, Id country){
        Enrolment__c e = new Enrolment__c();
        e.Student__c = student;
        e.Qualification__c = qual;
        e.Line_Item_Qualification__c = liq;
        //e.Enrolment_Status__c = 'Active (Commencement)';
        e.Enrolment_Status__c = 'Completed';
        e.Start_Date__c = date.today()+5;
        e.End_Date__c = date.today()+10;
        e.Delivery_Location__c = loc;
        e.Overseas_Country__c = country;
        e.Study_Reason__c = '01 - To get a job';
        e.Victorian_Commencement_Date__c = e.start_date__c+5;
        
        return e;
    }
        public static Enrolment__c createEnrolments(Id student,Id qual, id liq, id loc, Id country){
        Enrolment__c e = new Enrolment__c();
        e.Student__c = student;
        e.Qualification__c = qual;
        e.Line_Item_Qualification__c = liq;
        //e.Enrolment_Status__c = 'Active (Commencement)';
        e.Enrolment_Status__c = 'Cancelled';
        e.Start_Date__c = date.today()+5;
        e.End_Date__c = date.today()+10;
        e.Delivery_Location__c = loc;
        e.Overseas_Country__c = country;
        e.Study_Reason__c = '01 - To get a job';
        e.Victorian_Commencement_Date__c = e.start_date__c+5;
        
        return e;
    }
        public static Enrolment__c createEnrolments2(Id student,Id qual, id liq, id loc, Id country){
        Enrolment__c e = new Enrolment__c();
        e.Student__c = student;
        e.Qualification__c = qual;
        e.Line_Item_Qualification__c = liq;
        //e.Enrolment_Status__c = 'Active (Commencement)';
        e.Enrolment_Status__c = 'Cancelled';
        e.Start_Date__c = date.today()+5;
        e.End_Date__c = date.today()+10;
        e.Delivery_Location__c = loc;
        e.Overseas_Country__c = country;
        e.Study_Reason__c = '01 - To get a job';
        e.Victorian_Commencement_Date__c = e.start_date__c+5;        
        return e;
    }


    //Create Enrolment Record for EUOS
    public static Enrolment__c createEnrolmenteuos(Id student,Id qual, id liq, id loc, Id country, String countryName, string hasDisability, string areasOfImpairment, boolean request, Id del, Id attend){
        Highest_Educational_Participation_Level__c hep = new Highest_Educational_Participation_Level__c();
        hep.Name = 'Highest Educational';
        hep.Code__c = '1';
        insert hep;
        
        Enrolment__c e = new Enrolment__c();
        e.Student__c = student;
        e.Qualification__c = qual;
        e.Line_Item_Qualification__c = liq;
        //e.Enrolment_Status__c = 'Active (Commencement)';
        e.Enrolment_Status__c = 'Completed';
        e.Start_Date__c = date.today()+5;
        e.End_Date__c = date.today()+10;
        e.Delivery_Location__c = loc;
        e.Overseas_Country__c = country;
        e.Study_Reason__c = '01 - To get a job';
        e.Victorian_Commencement_Date__c = e.Start_Date__c+5;
        
       
        
        e.Predominant_Delivery_Mode__c = del;
        e.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
        insert e;
        
        return e;
    }
    //Query Student Status Code Type
    public static Id insertStudentStatusCodeType(String studStatusCode){
        Student_Status_Code_Type__c s = new Student_Status_Code_Type__c();
        List<Student_Status_Code_Type__c> sList = new List<Student_Status_Code_Type__c>();
        sList = [select Id, Code__c from Student_Status_Code_Type__c where Code__c =: studStatusCode];
        if(sList.size() == 0){
            s.Name = 'Test Status';
            s.Code__c = studStatusCode;
            insert s;
        }
        else{
            s = sList[0];
        }
        return s.Id;
    }
    //Query RPL Details of Study Type
    public static Id queryRPL(String rplCode){
        RPL_prior_study_Types__c rpl = new RPL_prior_study_Types__c();
        rpl = [select Id, Code__c from RPL_prior_study_Types__c where Code__c =: rplCode];
        return rpl.Id;
    }
    //Create Credit_status_Higher_Ed_provider_code__c Record
    public static Credit_status_Higher_Ed_provider_code__c createCreditStatus(){
        Credit_status_Higher_Ed_provider_code__c c = new Credit_status_Higher_Ed_provider_code__c();
        c.Name = 'Test Credit Status';
        c.Code__c = '101';

        insert c;
        return c;
    }
    //Create Level_of_Prior_Study_for_RPL_Type__c Record
    public static Level_of_Prior_Study_for_RPL_Type__c createLevelPriorStudy(){
        Level_of_Prior_Study_for_RPL_Type__c l = new Level_of_Prior_Study_for_RPL_Type__c();
        l.Name = 'Test Level of Prior Study';
        l.Code__c = '101';
        insert l;
        return l;
    }
    //Create Education_Provider_Type__c Record
    public static Education_Provider_Type__c createEducationProviderType(){
        Education_Provider_Type__c l = new Education_Provider_Type__c();
        l.Name = 'Test Education Provider Type';
        l.Code__c = '101';
        insert l;
        return l;
    }
    //Create Result Record
    public static Results__c createResult(Id state){
        Id recType = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c newRes = new Results__c();
            newRes.RecordTypeId = recType;
            newRes.Name = 'test results';
            newRes.State__c = state; 
          insert newRes;
          return newRes;
    }
    
     //Create Result Record
    public static Results__c createNationalResult(){
        Id recType = [select Id from RecordType where DeveloperName = 'National_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c newRes = new Results__c();
            newRes.RecordTypeId = recType;
            newRes.Name = 'test results';
            newRes.Result_Code__c = '20'; 
          insert newRes;
          return newRes;
    }
    
    //Create Result Record
    public static Results__c CreateStateOutCome(Id natIncome, Id stateId){
        Id recType = [select Id from RecordType where DeveloperName = 'State_Outcome' and sObjectType = 'Results__c'].Id;
        Results__c newRes = new Results__c();
            newRes.RecordTypeId = recType;
            newRes.Name = 'test results';
            newRes.State__c = stateId;
            newRes.National_Outcome__c = natIncome;
            newRes.Result_Code__c = '90';
          insert newRes;
          return newRes;
    }
    
    //Create Result Record
    public static Results__c createResultWithNational(Id state, Id natIncome){
        Results__c newRes = new Results__c();
            newRes.Name = 'test results';
            newRes.State__c = state;
            newRes.National_Outcome__c = natIncome;
          insert newRes;
          return newRes;
    }
    //Create Enrolment Unit
    public static Enrolment_Unit__c createEnrolmentUnit(Id unitC, Id enrol,Id res, Id loc, Id liqu){
        Enrolment_Unit__c newEnrolUnit = new Enrolment_Unit__c();
                newEnrolUnit.Unit__c = unitC;
                newEnrolUnit.Enrolment__c = enrol;
                newEnrolUnit.Start_Date__c = date.today() + 5;
                newEnrolUnit.End_Date__c = date.today() + 10;
                newEnrolUnit.Unit_Result__c = res;
                newEnrolUnit.Training_Delivery_Location__c = loc;

                newEnrolUnit.Line_Item_Qualification_Unit__c = liqu;
    //            newEnrolUnit.Unit_Result__r.National_Outcome_Code__c = '90';
                
          insert newEnrolUnit;
          return newEnrolUnit;
    }
    public static Enrolment_Unit__c createEnrolmentUnits(Id unitC, Id enrol,Id res, Id loc, Id del, Id liqu){
        Enrolment_Unit__c newEnrolUnit = new Enrolment_Unit__c();
                newEnrolUnit.Unit__c = unitC;
                newEnrolUnit.Enrolment__c = enrol;
                newEnrolUnit.Start_Date__c = date.today() + 5;
                newEnrolUnit.End_Date__c = date.today() + 10;
                newEnrolUnit.Unit_Result__c = res;
                newEnrolUnit.Training_Delivery_Location__c = loc;
                newEnrolUnit.Delivery_Mode__c = del;
                newEnrolUnit.Line_Item_Qualification_Unit__c = liqu;
    //            newEnrolUnit.Unit_Result__r.National_Outcome_Code__c = '90';
                
          insert newEnrolUnit;
          return newEnrolUnit;
    }

    //Create State Record
    public static State__c createState(Id country){
        State__c newState = new State__c();
        newState.Name = 'Victoria';
        newState.Country__c = Country;
        newState.Short_Name__c = 'VIC';
        newState.Code__c = '02';
        insert newState;
        return newState;
    }
    
    //Create State Record
    public static State__c createStateQLD(Id country){
        State__c newState = new State__c();
        newState.Name = 'Queensland';
        newState.Country__c = Country;
        newState.Short_Name__c = 'QLD';
        newState.Code__c = '03';
        insert newState;
        return newState;
    }
    
    //Create Funding Source Record
    public static Funding_Source__c createFundingSource(Id state){
        RecordType recId = [Select Id,Name from RecordType where DeveloperName = 'State_Funding_Source' and sObjectType = 'Funding_Source__c'];
        Funding_Source__c f = new Funding_Source__c();
        f.RecordTypeId = recId.Id;
        f.Fund_Source_Name__c = 'test Source';
        f.Code__c = '2334';
        f.State__c = state;
        insert f;
        return f;
    }
    //Create Purchasing Contract record
   public static Purchasing_Contracts__c createPurchasingContract(Id fSource){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = fSource;
        p.Contract_Code__c = '1234567890';
        insert p;
        return p;
    }
    //Create Purchasing Contract record
    public static Purchasing_Contracts__c createPurchasingContractState(Id fSource, Id acc, Id state){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = state;
        p.Contract_Code__c = '1234567890';
        p.Training_Organisation__c = acc;
        insert p;
        return p;
    }
    
    //Create Purchasing Contract record
    public static Purchasing_Contracts__c createPurchasingContractWithTOState(Id fSource, Id trainingOrg, Id state){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = fSource;
        p.Contract_Code__c = '1234567890';
        p.Training_Organisation__c = trainingOrg;
        p.Contract_State_Lookup__c = state;
        insert p;
        return p;
    }
    
        public static Purchasing_Contracts__c createPurchasingContractWithTO(Id fSource, Id trainingOrg){
        Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
        Purchasing_Contracts__c p = new Purchasing_Contracts__c();
        p.Contract_Name__c = 'test Contract';
        p.Contract_Type__c = 'Apprenticeship Funding';
        p.RecordTypeId = recordTypeId;
        p.State_Fund_Source__c = fSource;
        p.Contract_Code__c = '1234567890';
        p.Training_Organisation__c = trainingOrg;
        insert p;
        return p;
    }
    
    //Create Contract Line Item record
    public static Contract_Line_Items__c createContractLineItem(Id pCon){
        Contract_Line_Items__c c = new Contract_Line_Items__c();
        c.Line_Item_Number__c = '001';
        c.Purchasing_Contract__c = pCon;
        insert c;
        return c;
    }
    //create Line Item Qualification record
    public static Line_Item_Qualifications__c createLineItemQualification(Id qualification, Id con){
        Line_Item_Qualifications__c  l= new Line_Item_Qualifications__c();
        l.Qualification__c = qualification;
        l.Training_Catalogue_Item_No__c = '0001';
        l.Contract_Line_Item__c = con;
        insert l;
        return l;
    }
        //create Line Item Qualification record
    public static Line_Item_Qualifications__c createLineItemQualificationFS(Id qualification, Id con, Id fundSource){
        Line_Item_Qualifications__c  l= new Line_Item_Qualifications__c();
        l.Qualification__c = qualification;
        l.Training_Catalogue_Item_No__c = '0001';
        l.Contract_Line_Item__c = con;
        l.State_Fund_Source__c = fundSource;
        insert l;
        return l;
    }
    //create Line Item Qualification Unit recrd
    public static Line_Item_Qualification_Units__c createLineItemQualificationUnit(Id lineItem, Id qualUnit){
        Line_Item_Qualification_Units__c  l= new Line_Item_Qualification_Units__c();
        l.Qualification_Unit__c = qualUnit;
        l.Line_Item_Qualification__c = lineItem;
        insert l;
        return l;
    }
    //create Unit Hours and Points record 
    public static Unit_Hours_and_Points__c createUnitHoursAndPoints(Id unit, Id state){
        Unit_Hours_and_Points__c  u= new Unit_Hours_and_Points__c();
        u.Unit__c = unit;
        u.State__c = state;
        u.Nominal_Hours__c = 1.5;
        u.Points__c = 1;
        insert u;
        return u;
    }
    //create Delivery Mode Type record
    public static Delivery_Mode_Types__c createDeliveryModeType(){
        Delivery_Mode_Types__c d = new Delivery_Mode_Types__c();
        d.Name = 'Sample Delivery';
        d.Code__c = '1123';
        d.Type__c = 'AVETMISS';
        insert d;
        return d;
    }
    //create Enrolment Unit of Study Record
    public static Enrolment_Unit_of_Study__c createEnrolmentUnitofStudy(Id euId, Id qunitId, Id field){
        Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
        e.Enrolment_Unit__c = euId;
        e.Qualification_Unit__c = qunitId;
        e.Total_amount_charged__c = 10000;
        e.Amount_paid_upfront__c = 5000;
        e.Field_of_Ed_of_prior_VET_Credit_RPL__c = field;
        //insert e;
        return e;
    }
    //create Award Record
    public static Awards__c createAward(Id enrolment){
        RecordType rec = [select Id from RecordType where Name='Draft Award'];
        Awards__c a = new Awards__c();
        a.RecordTypeId = rec.Id;
        a.Award_Type__c = 'Certificate';
        a.Enrolment__c = enrolment;
        insert a;
        return a;
    }
    //create Funding Stream Record
    public static Funding_Stream_Types__c createFundingStream(){
        Funding_Stream_Types__c f = new Funding_Stream_Types__c();
        f.Name = 'Funding Stream';
        insert f;
        return f;
    }
    //create Program record
    public static Program__c createProgram(Id qual, Id loc){
        Program__c p = new Program__c();
        p.Program_Start_Date__c = date.today();
        p.Program_End_Date__c = date.today()+1;
        p.Qualification__c = qual;
        p.Training_Organisation_Delivery_Location__c = loc;
        insert p;
        return p;
    }
    //create Task record
    public static Task createTask(Id whatId){
        Task t = new Task();
        t.WhatId = whatId;
        t.Subject = 'Print & Issue Approved Award';
        t.Status = 'In Progress';
        t.Priority = 'Normal';
        
        insert t;
        return t;
    }
    //create Tuition Fee Record
    public static Tuition_Fee__c createTuitionFee(Id qualId, Id stateId){
        Tuition_Fee__c t = new Tuition_Fee__c();
        t.Qualification__c = qualId;
        t.Amount_per_Scheduled_Hour__c = 45.50;
        t.State__c = stateId;
        insert t;
        return t;
    }
    //create Intake Unit of Study Record
    public static Intake_Unit_of_Study__c createIntakeUnitOfStudy(Id intakeUnit, Id unit){
        Intake_Unit_of_Study__c ius = new Intake_Unit_of_Study__c();
        ius.Intake_Unit_of_Competency__c = intakeUnit;
        ius.Unit__c = unit; 
        ius.Scheduled_Hours__c = 35;
        insert ius;
        return ius;
    }
    //create Intake Unit of Study Tuition Fee Record
    public static Intake_Unit_of_Study_Tuition_Fee__c createIntakeUnitofStudyTuitionFee(Id state, Id ius){
        Intake_Unit_of_Study_Tuition_Fee__c i = new Intake_Unit_of_Study_Tuition_Fee__c();
        i.State__c = state;
        i.Intake_Unit_of_Study__c = ius;
        
        insert i;
        return i;
    }
    //create ID Card Record
    public static ID_Card__c createIdCard(Id award, Id student){
        ID_Card__c id = new ID_Card__c();
        id.Award__c = award;
        id.Student__c = student;
        insert id;
        return id;
    }
    //create class attendance type
    public static Class_Attendance_Type__c createClassAttendanceType(){
        Class_Attendance_Type__c cat = new Class_Attendance_Type__c();
        cat.Name = 'NAME';
        cat.Code__c = 'N';
        insert cat;
        return cat;
    }
    //create variation reason code record
    public static Variation_Reason_Code__c createVariationReasonCode(){
        Variation_Reason_Code__c v = new Variation_Reason_Code__c();
        v.Name = 'Variation Reason Code';
        v.Code__c = 'code';
        insert v;
        return v;
    }
    //create delivery strategy record
    public static Delivery_Strategy__c createDeliveryStrategy(Id delMode){
        Delivery_Strategy__c d = new Delivery_Strategy__c();
        d.Code__c = '32';
        d.Name = 'name';
        d.Description__c = 'description';
        d.Parent_Delivery_Mode_Type__c = delMode;
        insert d;
        return d;
    }
    //create staff Id card record
    public static Staff_ID_Card__c createStaffIDCard(Id user){
        Staff_ID_Card__c s = new Staff_ID_Card__c();
        s.Staff_Member__c = user;
        s.Staff_Member_2__c = user;
        insert s;
        return s;
    }
    //create funding stream type
    public static Funding_Stream_Types__c createFundingStreamType(){
        Funding_Stream_Types__c  fst = new Funding_Stream_Types__c();
        fst.Name = 'TEST FUNDING STREAM TYPE';
        insert fst;
        return fst;
    }
    
    public static Enrolment_Placement_Detail__c createEnrolmentPlacementDetail(Id enrolmentId){
        String business = [select id from RecordType where DeveloperName='Business_Account' AND SObjectType = 'Account'].Id; 
        Account acct = new Account();
        acct.RecordTypeId = business;
        acct.Name ='business account';
        acct.Business_Account_Type__c = 'Placement Facility';
        acct.Placement_Facility_Type__c = 'Aged Care';
        acct.Proficiency_in_Spoken_English__c = '1 - Very well';
        insert acct;
        
        Enrolment_Placement_Detail__c e = new Enrolment_Placement_Detail__c();
        e.Placement_Facility__c = acct.Id;
        e.Enrolment__c = enrolmentId;
        e.Start_Date__c = date.today();
        e.End_Date__c = date.today() + 1;
        e.Number_of_Placement_Hours__c = 100;
        insert e;
        return e;
    }

    public static Dual_Enrolment__c createDualEnrolment(Enrolment__c enrol1, Enrolment__c enrol2){
        Dual_Enrolment__c dualenrol = new Dual_Enrolment__c();
        dualenrol.Enrolment_1__c = enrol1.Id;
        dualenrol.Enrolment_2__c = enrol2.Id;
        insert dualenrol;
        return dualenrol;
    }

    public static Completion_Status_Type__c createFailCompletionStatusType() {
        Completion_Status_Type__c type = new Completion_Status_Type__c();
        type.Pass__c = false;
        type.code__c = 'test code';
        insert type;
        return type;
    }
}