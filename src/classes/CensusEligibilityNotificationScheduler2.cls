global class CensusEligibilityNotificationScheduler2 implements Schedulable {
    
    global void execute(SchedulableContext sc) {
    CensusEligibilityNotificationBatch2 b = new CensusEligibilityNotificationBatch2();
        database.executebatch(b, 200);
    }

}