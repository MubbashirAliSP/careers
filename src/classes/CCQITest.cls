@isTest
public with sharing class CCQITest {
    @testSetup
    static void setup() {
        List<CCQITrainingOrg__c> setupList = new List<CCQITrainingOrg__c>();

        CCQITrainingOrg__c setup = new CCQITrainingOrg__c(
                Name = '1',
                Field__c = 'Training Org Identifier',
                Length__c = 10
            );
        setupList.add(setup);

        setup = new CCQITrainingOrg__c(
                Name = '2',
                Field__c = 'Training Org Description',
                Length__c = 100
            );
        setupList.add(setup);

        setup = new CCQITrainingOrg__c(
                Name = '3',
                Field__c = 'Offshore Indicator Flag',
                Length__c = 1
            );
        setupList.add(setup);

        setup = new CCQITrainingOrg__c(
                Name = '4',
                Field__c = 'Year',
                Length__c = 4
            );
        setupList.add(setup);
        insert setupList;
    }
    
     static testMethod void CCQItest1() {
        
         Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c(); 
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Results__c res2 = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        
        List<Qualification__c> qualList = new List<Qualification__c>();
        
            
            
            Test.startTest();
            
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            qual.Include_in_CCQI_export__c = true;
            update qual;
            
            for(Qualification__c q: [Select Id, Name, Qualification_Name__c FROM Qualification__c WHERE Include_in_CCQI_export__c = true 
                                    /*AND Name IN ('TLI40207','MEM20105','MEM20198','MEM20105A','MEM20105TT')*/ limit 2000]){
                qualList.add(q);
            }          
            //insert qualList;
            system.debug('**Qual List: ' + qualList.size());
            
            unit = TestCoverageUtilityClass.createUnit();
            //qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            qunit = TestCoverageUtilityClass.createQualificationUnit(qualList[0].Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            //countryAU = TestCoverageUtilityClass.queryAustralia();
            state = TestCoverageUtilityClass.createStateQLD(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, accTraining.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            //lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qualList[0].Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id, accTraining.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
          
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            //enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qualList[0].Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Enrolment_Status__c = 'Active (Commencement)';
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() -1;            
            enrl.Delivery_Location__c = loc.id;
            
           
            //enrl.Overseas_Country__c = null;
            insert enrl;
            
            Id recType = [select Id from RecordType where Name = 'National Outcome' and sObjectType = 'Results__c'].Id;
            Results__c newRes = new Results__c();
            newRes.RecordTypeId = recType;
            newRes.Name = 'test results';
            newRes.Result_Code__c = '20'; 
            insert newRes;
          
             Enrolment_Unit__c eUnit = new Enrolment_Unit__c();
                eUnit.Unit__c = unit.id;
                eUnit.Enrolment__c = enrl.id;
                eUnit.Start_Date__c = date.today() - 15;
                eUnit.End_Date__c = date.today() - 10;
                eUnit.Unit_Result__c = newRes.id;
                eUnit.Training_Delivery_Location__c = loc.id;
                eUnit.Line_Item_Qualification_Unit__c = lineItemUnit .id;
                eUnit.Result_Outcome_Code__c = '70';
            insert eUnit;
            
            res = TestCoverageUtilityClass.createNationalResult();
            res2 = TestCoverageUtilityClass.CreateStateOutCome(res.id,state.id);
            
            un = TestCoverageUtilityClass.createUnit();
          
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res2.id, loc.Id, lineItemUnit.Id);
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eUnit.Id, qunit.Id, fe.Id);
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;

                PageReference testPage = Page.CCQIGeneration;
            //testPage.getParameters().put('enrolmentId', enrl.Id);
                Test.setCurrentPage(testPage);
            
                CCQIController controller = new CCQIController();
                
                controller.state = 'QLD';
                controller.rto = acc.id;
                controller.year = String.valueOf(system.today().year() - 1);
                
                controller.cancel();
                controller.next();
                
                controller.generateData();
                
                controller.exportData();
            
            Test.stopTest();
    }

}