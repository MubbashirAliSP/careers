/**
 * @description Batch process for sending out invoices to students based on their progression for a given census
 * @author Ranyel Maliwanag
 * @date 02.NOV.2015
 * @history
 *       19.NOV.2015    Ranyel Maliwanag    - Changed condition for the invoice notifications to run if and only if it's 14 days prior to the Census Date
 *                                          - Added additional safeguard that Census Date must not be in the past
 *       08.JAN.2016    Ranyel Maliwanag    - Added EUOS filtering to exclude Census records that don't have an EUOS with a Census date from invoicing
 *       18.JAN.2016    Ranyel Maliwanag    - Changed Census Date filter criteria to be exactly the reference date (i.e., exactly 14 days from today)
 */
public without sharing class CensusInvoiceGenerationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    public String queryString;
    public Date referenceDate;
    public ApplicationsPortalCentralSettings__c portalSettings;
    public Integer offset;

    /**
     * @description Default batch class constructor
     * @author Ranyel Maliwanag
     * @date 02.NOV.2015
     * @history
     *       08.JAN.2016    Ranyel Maliwanag    - Added nested EUOS query
     *       18.JAN.2016    Ranyel Maliwanag    - Changed CensusDate__c criteria to be `= :referenceDate` instead of `<= :referenceDate`
     */
    public CensusInvoiceGenerationBatch() {
        portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        offset = Integer.valueOf(portalSettings.InvoicingOffset__c);
        referenceDate = Date.today().addDays(offset);

        // Query criteria: Get all Census records that have progressed, met the date criteria, and have not been sent an invoice yet
        queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Census__c, ', (SELECT Id, Census_Date__c FROM EnrolmentUnitsOfStudy__r)');
        queryString += 'WHERE CensusDate__c = :referenceDate AND InvoiceSentDate__c = null AND IsInvoiceGenerated__c != true AND CensusDate__c >= TODAY AND EnrolmentId__r.Enrolment_Status__c LIKE \'Active%\' AND EnrolmentId__r.RecordType.Name = \'Dual Funding Enrolment\'';
        System.debug('Post Constructor Query ::: ' + queryString);
    }

    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 02.NOV.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('Pre Start Query ::: ' + queryString);
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 02.NOV.2015
     * @history
     *       08.JAN.2015    Ranyel Maliwanag    - Added loop through of EUOS to make sure that at least one EUOS has a census date before marking the Census record as eligible for invoicing and removing lookup connections on EUOS that don't have a census date
     */
    public void execute(Database.BatchableContext BC, List<Census__c> records) {
        List<Enrolment_Unit_of_Study__c> euosForUpdate = new List<Enrolment_Unit_of_Study__c>();

        for (Census__c record : records) {
            Boolean isInvoiceEligible = false;

            for (Enrolment_Unit_of_Study__c euos : record.EnrolmentUnitsOfStudy__r) {
                if (euos.Census_Date__c == null) {
                    euos.CensusId__c = null;
                    euosForUpdate.add(euos);
                } else {
                    isInvoiceEligible = true;
                }
            }
            record.IsInvoiceGenerated__c = isInvoiceEligible;
            System.debug('::: ' + record.Name);
        }

        update euosForUpdate;
        update records;
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 02.NOV.2015
     */
    public void finish(Database.BatchableContext BC) {
    }
}