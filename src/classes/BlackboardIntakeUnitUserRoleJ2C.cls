/*JOB2b Staging table loader job
 *This class is resposible for loading creating and loading 
 *Blackboard Course Memberships
 *Course memberships are created from Intake Unit User Roles.
 *JOB2C does not require to create blackboard users and blackboard user roles prior
 *to creation of BBCM
 *This job is designed for T2(Template Course)
 *Please Refer to ERD for further details about relationships
 *Created By Yuri Gribanov - CloudSherpas 15th July 2014
 */
global with sharing class BlackboardIntakeUnitUserRoleJ2C implements Database.Batchable<sObject> {

    global String query;


    global BlackboardIntakeUnitUserRoleJ2C() {
        /*Initial query to be passed into query locator performed against Enrolment Intake Units*/
        query = 'Select Id,Blackboard_User_Role__c,Intake_Unit__c,Intake_Unit__r.Blackboard_Template_Course__c From Intake_Unit_User_Role__c WHERE Intake_Unit__r.Blackboard_Template_Course__c != null AND Blackboard_Course_Membership_Created__c = false';
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List<sObject> scope) {

         Set<Id>t2Ids = new Set<Id>();
         Map<Id,Id>t2t3Map = new Map<Id,Id>();

        List<Blackboard_Course_Membership__c> bbcmList = new List<Blackboard_Course_Membership__c>();
        List<Intake_Unit_User_Role__c> intakeuserRolesToUpdate = new List<Intake_Unit_User_Role__c>();
        //Recordtypes for BBCM
        List<RecordType>bbcmRecTypes = New List<RecordType>([Select Id,DeveloperName From RecordType Where DeveloperName = 'Staff']);

        for(SObject s :scope) {

             Intake_Unit_User_Role__c iuur = (Intake_Unit_User_Role__c) s;

              t2Ids.add(iuur.Intake_Unit__r.Blackboard_Template_Course__c);

             

        }

        for(Blackboard_Course__c bbcourse : [Select id,Template_Course__c From Blackboard_Course__c Where Template_Course__c in : t2Ids ])
            t2t3Map.put(bbcourse.Template_Course__c, bbcourse.Id);
        

        for(SObject s :scope) {

             Intake_Unit_User_Role__c iuur = (Intake_Unit_User_Role__c) s;

              Blackboard_Course_Membership__c bbcm = new Blackboard_Course_Membership__c(

                   Intake_Unit_User_Role__c = iuur.Id,
                   Intake_Unit__c = iuur.Intake_Unit__c,
                   Blackboard_User_Role__c = iuur.Blackboard_User_Role__c,
                   Blackboard_Course__c = t2t3Map.get(iuur.Intake_Unit__r.Blackboard_Template_Course__c),
                   RecordTypeId = bbcmRecTypes.get(0).Id       
                );


              iuur.Blackboard_Course_Membership_Created__c = true;

              intakeuserRolesToUpdate.add(iuur);

              bbcmList.add(bbcm);

             

        }


        

        insert bbcmList;
        update intakeuserRolesToUpdate;


    }

    global void finish(Database.BatchableContext BC) {
        
      BlackboardIntakeStudentJ9  b1 = new BlackboardIntakeStudentJ9();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }
}