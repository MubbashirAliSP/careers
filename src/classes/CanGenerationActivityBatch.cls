global class CanGenerationActivityBatch implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts  {
    
    global String query = 'Select Id From Enrolment__c Where IsReadyForCAN__c = true And Can_Generated__c = false';
    global String sessionId = '';
    
    global database.querylocator start(Database.BatchableContext BC) {
         
        return Database.getQueryLocator(query);
    
    }
    
    //Set Session Id
    public CANGenerationActivityBatch() {sessionId = UserInfo.getSessionId();}
     
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {         
        
         List<Enrolment__c> recordForCANGeneration = new List<Enrolment__c>();
         Set<Id>recordIdsForCANGeneration = new Set<Id>();
         String error = '';
             
         for (SObject s :scope) {
           Enrolment__c enrl = (Enrolment__c) s;
           enrl.Can_Generated__c = true;
           recordForCANGeneration.add(enrl);
           recordIdsForCANGeneration.add(enrl.Id);
         }
         
         try {
             HttpRequest req = new HttpRequest();
             req.setEndpoint('https://' + Url.getSalesforceBaseUrl().getHost() + '/services/apexrest/PDFGeneration/');
             req.setMethod('POST');
             req.setBody('{"recordIdList":' + JSON.serialize(recordIdsForCANGeneration) + ', "sObjectType":"Enrolment__c"}');
             req.setHeader('Authorization', 'Bearer ' + sessionId);
             req.setHeader('Content-Type', 'application/json');
             Http http = new Http();
             
             if(!Test.isRunningTest())
              HttpResponse res = http.send(req);
             
            
             
         } catch (Exception e) {error = e.getMessage();}
         
         if(error != '')
             throw new internalServerException (error);
             
         update recordForCANGeneration;
             
          
         
         
     }
    
    global void finish(Database.BatchableContext BC) {
       
       
        
    }
    
    public class internalServerException extends Exception {
        //just for throwing exception
    }

}