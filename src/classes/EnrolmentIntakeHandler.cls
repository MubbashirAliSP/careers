public with sharing class EnrolmentIntakeHandler {
	
	public static void linkEnrolmentIntakes(Set<Id> intakeIds, Set<Id> enrolmentIds) {
		
		List<Enrolment_Intake_Unit__c> EnrlIntakeUnitInsert = new List<Enrolment_Intake_Unit__c>();
		
		List<Intake__c> intakes = new List<Intake__c>([SELECT id, Name, (Select id, Name,Unit_of_Study__c FROM Intake_Unit__r) FROM Intake__c WHERE Id in : intakeIds]);
		
		List<Enrolment__c>enrolments = new List<Enrolment__c>([Select Id, Name, (Select Id, Name, Unit__c,Enrolment__c From Enrolment_Units__r) FROM Enrolment__c WHERE Id in : enrolmentIds]);
		
		List<Enrolment_Unit__c> EnrlUnits = new List<Enrolment_Unit__c>();
		
		List<Intake_Unit__c> IntakeUnits = new List<Intake_Unit__c>();
		
		
		
			for(Enrolment__c e : enrolments) {
				for(Enrolment_Unit__c eu : e.Enrolment_Units__r)
					EnrlUnits.add(eu);
			}	
					
			for(Intake__c i : intakes) {
				
				for(Intake_Unit__c iu : i.Intake_Unit__r)
					IntakeUnits.add(iu);
			}		
				
			
			
			
			for(Enrolment_Unit__c eu: EnrlUnits) {
				
				for(Intake_Unit__c iu : IntakeUnits) {
					
					if(eu.Unit__c == iu.Unit_of_Study__c) {
						
						Enrolment_Intake_Unit__c EnrlIntakeUnit = new Enrolment_Intake_Unit__c(Enrolment__c = eu.Enrolment__c,
																	  Intake_Unit__c = iu.id);
																	  
					    EnrlIntakeUnitInsert.add(EnrlIntakeUnit);
						
					}
						
				}
			}
			
		
		insert EnrlIntakeUnitInsert;
		
		
		
	}

}