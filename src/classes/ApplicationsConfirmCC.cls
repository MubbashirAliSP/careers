/**
 * @description Controller for `ApplicationsConfirm` page. Gateway for handling email confirmation requests
 * @author Ranyel Maliwanag
 * @date 20.JUL.2015
 */
public without sharing class ApplicationsConfirmCC {
    public Boolean isAuthenticated {get; set;}
    public GuestLogin__c guestLogin {get; set;}


    /**
     * @description Default constructor for the class
     * @author Ranyel Maliwanag
     * @date 20.JUL.2015
     */
    public ApplicationsConfirmCC() {
        isAuthenticated = EnrolmentsSiteUtility.checkAuthentication(ApexPages.currentPage());
        guestLogin = new GuestLogin__c();
    }


    /**
     * @description Page load action handler to evaluate parameters passed for email confirmation details
     * @author Ranyel Maliwanag
     * @date 20.JUL.2015
     */
    public PageReference evaluateParameters() {
        PageReference result = null;
        // Store GET parameters for reference
        Map<String, String> parameters = ApexPages.currentPage().getParameters();

        // Process parameters
        if (parameters.containsKey('confirm')) {
            if (isAuthenticated) {
                // Condition: The user is already logged in
                // Verify that the email they are trying to confirm is theirs
                Id confirmationId = parameters.get('confirm');
                Cookie authCookie = ApexPages.currentPage().getCookies().get('authenticationId');
                Id authenticationId = authCookie.getValue();

                if (authenticationId != null && confirmationId == authenticationId) {
                    // Condition: The authenticated user is confirming his own credentials
                    // Update the credentials to reflection the confirmed status
                    try {
                        guestLogin = [SELECT Id FROM GuestLogin__c WHERE Id = :confirmationId];
                        guestLogin.IsEmailConfirmed__c = true;
                        update guestLogin;

                        result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsLanding, confirmationId);
                        List<Cookie> currentCookies = result.getCookies().values();
                        Cookie messageCookie = new Cookie('notificationMessage', 'You have successfully confirmed your email.', null, 0, true);
                        currentCookies.add(messageCookie);
                        result.setCookies(currentCookies);
                    } catch (QueryException e) {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The confirmation Id you are trying to confirm is invalid'));
                    } catch (Exception e) {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
                    }
                } else {
                    // Condition: The authenticated user is confirming an invalid credential. It may be an incorrectly copied Id or the Id of a different credential
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The confirmation Id you are trying to confirm is invalid'));
                }
            } else {
                // Condition: The user is not logged in
                result = EnrolmentsSiteUtility.generatePageReference(Page.ApplicationsLanding, null);
                result.getParameters().put('confirm', parameters.get('confirm'));
            }
        }
        return result;
    }
}