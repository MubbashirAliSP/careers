/**
 * @description Collection of utility classes for the new enrolments site
 * @author Ranyel Maliwanag
 * @date 12.JUN.2015
 */
public without sharing class EnrolmentsSiteUtility {
    /**
     * @description Generates a `PageReference` result with the given targetUrl and a corresponding authentication flag cookie
     * @author Ranyel Maliwanag
     * @date 12.JUN.2015
     * @param (PageReference) apexPage: The VisualForce Page to be used with the `PageReference` object
     * @param (Id) loginId: The record Id for the authenticated user. Expected to be null for unauthenticated users.
     */
    public static PageReference generatePageReference(PageReference apexPage, Id loginId) {
        // Generate authentication cookie to be associated with the PageReference result
        Cookie authCookie = new Cookie('authenticationId', loginId, null, -1, true);

        // Actual PageReference result to be returned
        PageReference result = apexPage;
        result.setCookies(new List<Cookie>{authCookie});
        return result;
    }


    public static PageReference generatePageReference(PageReference apexPage, Id loginId, Id campusId) {
        // Generate authentication cookie to be associated with the PageReference result
        Cookie authCookie = new Cookie('authenticationId', loginId, null, -1, true);

        // Generate campus cookie to be associated with the PageReference result
        Cookie campusCookie = new Cookie('campusId', campusId, null, -1, true);

        // Actual PageReference result to be returned
        PageReference result = apexPage;
        result.setCookies(new List<Cookie>{authCookie, campusCookie});
        return result;
    }


    /**
     * @description Determines whether or not the current page is authenticated depending on the authentication cookie
     * @author Ranyel Maliwanag
     * @date 15.JUN.2015
     * @param (PageReference) apexPage: The current page context that we are checking
     * @return (Boolean): True if the current page has an authenticated user viewing it and false otherwise.
     */
    public static Boolean checkAuthentication(PageReference apexPage) {
        Cookie authCookie = apexPage.getCookies().get('authenticationId');
        if (authCookie == null) {
            authCookie = new Cookie('authenticationId', null, null, -1, true);
        }
        return Boolean.valueOf(String.isNotBlank(authCookie.getValue()));
    }


    public static Boolean checkCampus(PageReference apexPage) {
        Cookie authCookie = apexPage.getCookies().get('campusId');
        if (authCookie == null) {
            authCookie = new Cookie('campusId', null, null, -1, true);
        }
        return Boolean.valueOf(String.isNotBlank(authCookie.getValue()));
    }

    public static Boolean checkCampusCode(PageReference apexPage) {
        Cookie authCookie = apexPage.getCookies().get('campusCode');
        if (authCookie == null) {
            authCookie = new Cookie('campusCode', null, null, -1, true);
        }
        return Boolean.valueOf(String.isNotBlank(authCookie.getValue()));
    }


    /**
     * @description Returns the list of fields to which this object has lookups to
     * @author Ranyel Maliwanag
     * @date 10.JUL.2015
     * @param (SObject) record: The source record we are getting lookup fields from
     * @return Map<String, Schema.DescribeFieldResult>
     */
    public static Map<String, Schema.DescribeFieldResult> getLookupFields(Schema.DescribeSObjectResult sObjectDescribe) {
        Map<String, Schema.DescribeFieldResult> result = new Map<String, Schema.DescribeFieldResult>();
        for (Schema.SObjectField field : sObjectDescribe.fields.getMap().values()) {
            if (!field.getDescribe().getReferenceTo().isEmpty()) {
                result.put(field.getDescribe().getName(), field.getDescribe());
            }
        }
        return result;
    }


    /**
     * @description Returns a comma-separated list of fields for the given SObject. To be used with the query string builders in this class
     * @author Ranyel Maliwanag
     * @date 13.JUL.2015
     * @param (Schema.DescribeSObjectResult) sObjectDescribe: The SObject describe record to fetch fields data from
     * @return (String)
     */
    private static String getFieldList(Schema.DescribeSObjectResult sObjectDescribe) {
        String result = '';
        Boolean isInitial = true;
        for (Schema.SObjectField field : sObjectDescribe.fields.getMap().values()) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();

            if (!isInitial) {
                result += ', ';
            } else {
                isInitial = false;
            }

            result += fieldDescribe.getName();
        }
        return result;
    }


    /**
     * @description Returns a query string for the given SObject Describe record
     * @author Ranyel Maliwanag
     * @date 11.JUL.2015
     * @param (Schema.DescribeSObjectResult) sObjectDescribe: The SObject describe record to fetch fields data from 
     * @return (String)
     */
    public static String getQueryString(Schema.DescribeSObjectResult sObjectDescribe) {
        String result = 'SELECT ';
        result += getFieldList(sObjectDescribe);
        result += ' FROM ' + sObjectDescribe.getName() + ' ';
        return result;
    }


    public static String getQueryString(Schema.DescribeSObjectResult sObjectDescribe, String extraSelect) {
        String result = 'SELECT ';
        result += getFieldList(sObjectDescribe);
        if (String.isNotBlank(extraSelect)) {
            result += extraSelect;
        }
        result += ' FROM ' + sObjectDescribe.getName() + ' ';
        return result;
    }

    public static Object getReferenceFieldValue(SObject source, String fieldPattern) {
        System.debug('source ::: ' + source + ' ::: ' + fieldPattern);
        Object result = '';
        if (fieldPattern.contains('.')) {
            String reference = fieldPattern.substringBefore('.');
            String referenceField = fieldPattern.substringAfter('.');
            SObject referenceObject = source.getSObject(reference);
            if (referenceObject != null) {
                result = getReferenceFieldValue(referenceObject, referenceField);
            }
        } else {
            result = source.get(fieldPattern);
        }
        return result;
    }

    public static String maskAnswer(String answer) {
        return answer.replaceAll('.', '*');
    }
}