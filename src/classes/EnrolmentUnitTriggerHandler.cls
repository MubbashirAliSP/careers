/**
 * @description Enrolment Unit Trigger Handler 
 * @author Janella Lauren Canlas
 * @date 07.NOV.2012
 *
 * @history
 * - 07.NOV.2012    Janella Canlas      - Created.
 * - 24.JAN.2013    Janella Canlas      - added method to populate Campus Location on EUOS
 * - 06.FEB.2013    Janella Canlas      - add code to populate Report for AVETMISS code on EU upon create
 * - 20.FEB.2013    Janella Canlas      - add method to populate Unit Value
 * - 28.AUG.2014    Warjie Malibago     - add populateDeliveryModeType method
 * - 29.AUG.2014    Warjie Malibago     - add populateEUOSResult method
 * - 22.SEP.2014    Warjie Malibago     - optimize code to fix SQL governor limit
 * - 19.NOV.2014    Nick Guia           - optimize code to fix SQL governor limit
 *                                      - Modified event handlers
 * - 09.SEP.2015    Ranyel Maliwanag    - Changed references to Child Post Code to Delivery Location Identifier when updating the Campus Location Code
 * - 29.MAR.2016    Biao Zhang          - Update populate EUOS result
*/ 
 
public with sharing class EnrolmentUnitTriggerHandler {

    private static Boolean isRunning = false;

    /**
    *   @author Nick Guia
    *   @description Before Insert event handler
    */
    public static void onBeforeInsert(List<Enrolment_Unit__c> eUnitList) {
        //Variables:
        //Set of Line item qualification IDs
        Set<Id> liqUnitIdSet = new Set<Id>();
        //Set of Enrolment IDs
        Set<Id> enrolmentIdSet = new Set<Id>();
        //Set of Location IDs
        Set<Id> locIdSet = new Set<Id>();
        //Set of Unit IDs
        Set<Id> unitIdSet = new Set<Id>();
        //Map of Unit and Enrolment Unit
        Map<Id, Enrolment_Unit__c> updateUnitMap = new Map<Id, Enrolment_Unit__c>();
        //Map of Enrolment Unit Id and Enrolment Unit
        Map<Id, Enrolment_Unit__c> enrolmentUnitMap = new Map<Id, Enrolment_Unit__c>();

        //Filter events/Data Collection
        for(Enrolment_Unit__c eu : eUnitList){
            enrolmentUnitMap.put(eu.Id, eu);
            if(eu.Unit__c != null) {
                unitIdSet.add(eu.Unit__c);
                updateUnitMap.put(eu.Unit__c, eu);
            }
            if(eu.Line_Item_Qualification_Unit__c != null) {
                liqUnitIdSet.add(eu.Line_Item_Qualification_Unit__c);
            }
            if(eu.Enrolment__c != null) {
                enrolmentIdSet.add(eu.Enrolment__c);
            }
            if(eu.Training_Delivery_Location__c != null) {
                locIdSet.add(eu.Training_Delivery_Location__c);
            }
        }

        //Call methods
        if(!updateUnitMap.isEmpty()){
            assignUnitRecordType(enrolmentUnitMap);
        }
        if(!liqUnitIdSet.isEmpty()){
            populateUnitValue(liqUnitIdSet, eUnitList);
        }
        if(!enrolmentIdSet.isEmpty()){
            populateDeliveryMode(enrolmentIdSet, eUnitList);
        }
        if(!unitIdSet.isEmpty() && !locIdSet.isEmpty()){
            populateNominalHoursBeforeInsert(unitIdSet, locIdSet, eUnitList);
        }
    }

    /**
    *   @author Nick Guia
    *   @description Before Update event handler
    */
    public static void onBeforeUpdate(List<Enrolment_Unit__c> eUnitList, Map<Id, Enrolment_Unit__c> newEUnitMap, Map<Id, Enrolment_Unit__c> oldEUnitMap){
        //Variables:
        //Line item qualification IDs
        Set<Id> liqUnitIdSet = new Set<Id>();
        //map of Unit and Enrolment Unit 
        Map<Id, Enrolment_Unit__c> updateUnitMap = new Map<Id, Enrolment_Unit__c>();


        //Filter events/Data collection
        for(Enrolment_Unit__c eu : eUnitList){
            //if Unit was changed
            if(eu.Unit__c != oldEUnitMap.get(eu.Id).Unit__c
                && eu.Unit__c != null) 
            {
                updateUnitMap.put(eu.Unit__c, eu);
            }
            //if Line Item Qualification Unit was changed
            if(eu.Line_Item_Qualification_Unit__c != oldEUnitMap.get(eu.Id).Line_Item_Qualification_Unit__c
                && eu.Line_Item_Qualification_Unit__c != null) 
            {
                liqUnitIdSet.add(eu.Line_Item_Qualification_Unit__c);
            }
        }

        //Call methods
        populateNominalHours(newEUnitMap);
        
        //if Unit was changed
        if(!updateUnitMap.isEmpty()){
            assignUnitRecordType(updateUnitMap);
        }
        if(!liqUnitIdSet.isEmpty()){
            populateUnitValue(liqUnitIdSet, eUnitList);
        }
    }

    /**
    *   @author Nick Guia
    *   @description After Insert event handler
    *
    *   Uncomment this when you will need a function for after insert
    */
    //public static void onAfterInsert(List<Enrolment_Unit__c> eUnitList, Map<Id, Enrolment_Unit__c> newEUnitMap) {
        
    //}

    /**
    *   @author Nick Guia
    *   @description Before Update event handler
    */
    public static void onAfterUpdate(List<Enrolment_Unit__c> eUnitList, Map<Id, Enrolment_Unit__c> newEUnitMap, Map<Id, Enrolment_Unit__c> oldEunitMap) {
        populateCampusLocation(newEUnitMap.keySet());
        populateDeliveryModeType(newEUnitMap.keySet());

        List<Enrolment_Unit__c> resultUpdatedEUs = new List<Enrolment_Unit__c>();
        for(Enrolment_Unit__c eu : eUnitList){
            if(eu.Unit_Result__c != null && eu.Unit_Result__c != oldEunitMap.get(eu.Id).Unit_Result__c) {
                resultUpdatedEUs.add(eu);
            }
        }
        //if(!resultUpdatedEUs.isEmpty()) populateEUOSResult(resultUpdatedEUs);
    }


    /**
    *   @author Original: Nick Guia
    *   @description Assigns corresponding value to Unit Record Type
    *   @history
    *       Nick Guia   Nov. 20, 2014   - Created
    */
    public static void assignUnitRecordType(Map<Id, Enrolment_Unit__c> eUnitMap){
        Map<Id,Unit__c> unitMap = new Map<Id,Unit__c>([Select Id, RecordType.Name FROM Unit__c WHERE Id IN :eUnitMap.keySet()]);

        if(!unitMap.isEmpty()) {
            for(Enrolment_Unit__c eu : eUnitMap.values()) {
                if(unitMap.containsKey(eu.Unit__c)) {
                    eu.Unit_Record_Type__c = unitMap.get(eu.Unit__c).RecordType.Name;
                }
            }
        }
    }


    /**
    *   @author Original: Warjie Malibago
    *   @description This method will populate the Delivery Mode field with the Enrolment record's Predominant Delivery Mode value
    *   @history
    *       Nick Guia   Nov. 19, 2014   - code optimization
    */
    public static void populateDeliveryMode(Set<Id> enrolmentIds, List<Enrolment_Unit__c> euList){

        //query Enrolment records
        Map<Id, Enrolment__c> enrolmentMap = new Map<Id, Enrolment__c>([SELECT Id, Predominant_Delivery_Mode__c, 
                                                                        RecordType.Name, Start_Date__c, End_Date__c 
                                                                        FROM Enrolment__c 
                                                                        WHERE Id IN: enrolmentIds]);
        
        for(Enrolment_Unit__c eu : euList){

            if(enrolmentMap.containsKey(eu.Enrolment__c)){
                eu.Delivery_Mode__c = enrolmentMap.get(eu.Enrolment__c).Predominant_Delivery_Mode__c;

                if(enrolmentMap.get(eu.Enrolment__c).RecordType.Name == 'Dual Funded Enrolment' || 
                    enrolmentMap.get(eu.Enrolment__c).RecordType.Name == 'Standard Enrolment')
                {
                    eu.Report_for_AVETMISS__c = true;
                }
                /*if(enrolmentMap.get(eu.Enrolment__c).External_Id__c == null){
                    eu.Start_Date__c = enrolmentMap.get(eu.Enrolment__c).Start_Date__c;
                    eu.End_Date__c = enrolmentMap.get(eu.Enrolment__c).End_Date__c;
                }*/
            }
        }
    }


    /**
    *   @author Original: unknown
    *   @description This method will populate the Enrolment Unit's Scheduled Hours depending on the 
    *                equivalent Unit Hour and Points record
    *   @history
    *       Nick Guia   Nov. 19, 2014   - Code optimization
    *                                   - Changed input from List to Map
    */
    public static void populateNominalHours(Map<Id, Enrolment_Unit__c> newEUnitMap){

        Set<Id> unitIds = new Set<Id>();
        Map<Id, Enrolment_Unit__c> euMap = new Map<Id, Enrolment_Unit__c>();
        Map<Id, List<Unit_Hours_and_Points__c>> uhpMap = new Map<Id, List<Unit_Hours_and_Points__c>>();

        //query Enrolment Unit records (to retrieve data from different related object) and collect unit IDs
        for(Enrolment_Unit__c eu : [SELECT Id, Training_Delivery_Location__r.State_Lookup__c, 
                                    Training_Delivery_Location__r.Child_State__c, Unit__c 
                                    FROM Enrolment_Unit__c 
                                    WHERE Id IN: newEUnitMap.keySet()])
        {
            euMap.put(eu.Id, eu);
            unitIds.add(eu.Unit__c);
        }

        //create map of UHP ids and lists of UHP records
        for(Unit_Hours_and_Points__c uhp : [SELECT Id, Nominal_Hours__c, Points__c, State__c,
                                            State__r.Name, Unit__c 
                                            FROM Unit_Hours_and_Points__c 
                                            WHERE Unit__c IN: unitIds]) 
        {
            if(uhpMap.containsKey(uhp.Unit__c)){
                uhpMap.get(uhp.Unit__c).add(uhp);
            } else{
                uhpMap.put(uhp.Unit__c, new List<Unit_Hours_and_Points__c> {uhp});
            }
        }


        //loop Enrolment units to update based on State Lookup
        for(Enrolment_Unit__c e : newEUnitMap.values()){

            if(euMap.containsKey(e.Id)){
                Enrolment_Unit__c eu = euMap.get(e.Id);

                if(!uhpMap.containsKey(eu.Unit__c)){                    
                    //loop to populate Scheduled Hours with the Nominal Hours data of the UHP record with the same state as the Unit
                    for(Unit_Hours_and_Points__c u : uhpMap.get(eu.Unit__c)){
                        //if(eu.Training_Delivery_Location__r.State_Lookup__c == u.State__c){
                        //if(eu.Training_Delivery_Location__r.Child_State__c == u.State__c){
                        if(eu.Training_Delivery_Location__r.Child_State__c == u.State__r.Name){
                            e.Scheduled_Hours__c = u.Nominal_Hours__c;
                        }
                    }
                }
            }

            /*else{
                system.debug('^^else');
                Enrolment_Unit__c eu = euMap.get(e.Id);
                if(uhpMap.get(eu.Unit__c)!=null){
                    system.debug('^^@');
                    //retrieve list of unit hours and points with the same unit id
                    List<Unit_Hours_and_Points__c> uhLst = uhpMap.get(eu.Unit__c);
                    //loop to populate Scheduled Hours with the Nominal Hours data of the UHP record with the same state as the Unit
                    for(Unit_Hours_and_Points__c u : uhLst){
                        system.debug('^^#');
                        system.debug('^^eu.Training_Delivery_Location__r.State_Lookup__c: '+eu.Training_Delivery_Location__r.State_Lookup__c);
                        system.debug('^^u.State__c: '+u.State__c);
                        if(eu.Training_Delivery_Location__r.State_Lookup__c == u.State__c){
                            system.debug('^^$');
                            system.debug('^^u.Nominal_Hours__c: '+u.Nominal_Hours__c);
                            e.Scheduled_Hours__c = u.Nominal_Hours__c;
                        }
                    }
                }
            }*/
        }
    }
    
    /**
    *   @author Original: unknown
    *   @description populate nominal hours
    *   @history
    *       Nick Guia   Nov. 19, 2014   Code optimization
    */
    public static void populateNominalHoursBeforeInsert(Set<Id> unitIds, Set<Id> locIds, List<Enrolment_Unit__c> euList){

        Map<Id, List<Unit_Hours_and_Points__c>> unitMap = new Map<Id, List<Unit_Hours_and_Points__c>>();
        
        //create location map
        Map<Id, Locations__c> locMap = new Map<Id, Locations__c>([SELECT Id, Parent_Location__r.State_Lookup__c 
                                                                FROM Locations__c 
                                                                WHERE id IN: locIds]);

        //create unit hours and points map
        for(Unit_Hours_and_Points__c uhp : [SELECT Id, Nominal_Hours__c, Points__c, State__c, Unit__c 
                                            FROM Unit_Hours_and_Points__c 
                                            WHERE Unit__c IN: unitIds])
        {
            if(unitMap.containsKey(uhp.Unit__c)) {
                unitMap.get(uhp.Unit__c).add(uhp);
            } else {
                unitMap.put(uhp.Unit__c, new List<Unit_Hours_and_Points__c> {uhp});
            }
        }
        
        //assign nominal hours
        for(Enrolment_Unit__c e: euList){

            if(unitMap.containsKey(e.Unit__c)){             

                for(Unit_Hours_and_Points__c u: unitMap.get(e.Unit__c)){
                    if(locMap.get(e.Training_Delivery_Location__c).Parent_Location__r.State_Lookup__c == u.State__c)
                    {
                        e.Scheduled_Hours__c = u.Nominal_Hours__c;
                    }
                }
            }
        }       
    }

    /**
    *   @author Original: unknown
    *   @description This method will populate Campus Location on all EUOS related records upon update.
    *   @history
    *       Nick Guia   Nov. 19, 2014   - Code optimization
    *                                   - Changed input to Map
    */
    public static void populateCampusLocation(Set<Id> euIdSet){

        List<Enrolment_Unit_of_Study__c> euosUpdateList = new List<Enrolment_Unit_of_Study__c>();
        List<Enrolment_Unit_of_Study__c> euosList = [SELECT Id, Campus_Location_Code__c, Enrolment_Unit__c, 
                                                    Enrolment_Unit__r.Training_Delivery_Location__r.Postcode__c,
                                                    Enrolment_Unit__r.Training_Delivery_Location__c, 
                                                    Enrolment_Unit__r.Training_Delivery_Location__r.Child_Country__c,
                                                    Enrolment_Unit__r.Training_Delivery_Location__r.Delivery_Location_Identifier__c
                                                    FROM Enrolment_Unit_of_Study__c 
                                                    WHERE Enrolment_Unit__c IN: euIdSet];
        /*eUnitList = [select Training_Delivery_Location__r.Postcode__c,
                            Training_Delivery_Location__c,
                            Training_Delivery_Location__r.Country__r.Name
                            FROM Enrolment_Unit__c
                            WHERE Id IN: euIds
                        ];
        for(Enrolment_Unit__c e : eUnitList){
            euMap.put(e.Id, e);
        }*/

        if(euosList.size() > 0){
            for(Enrolment_Unit_of_Study__c euos : euosList){
                //if(euMap.containsKey(euos.Enrolment_Unit__c)){
                //Enrolment_Unit__c eu = euMap.get(euos.Enrolment_Unit__c);
                if(euos.Enrolment_Unit__r.Training_Delivery_Location__r.Child_Country__c == 'Australia' && 
                    euos.Enrolment_Unit__r.Training_Delivery_Location__c != null)
                {
                    euos.Campus_Location_Code__c = 'A' + euos.Enrolment_Unit__r.Training_Delivery_Location__r.Delivery_Location_Identifier__c;
                    euosUpdateList.add(euos);
                } else if(euos.Enrolment_Unit__r.Training_Delivery_Location__r.Child_Country__c != 'Australia' && 
                    euos.Enrolment_Unit__r.Training_Delivery_Location__c != null)
                {
                    euos.Campus_Location_Code__c = 'X' + euos.Enrolment_Unit__r.Training_Delivery_Location__r.Delivery_Location_Identifier__c;
                    euosUpdateList.add(euos);
                } else if(euos.Enrolment_Unit__r.Training_Delivery_Location__c == null){
                    euos.Campus_Location_Code__c = '99999';
                    euosUpdateList.add(euos);
                }
                //}
            }
            
            try{
                update euosUpdateList;
            } catch(Exception e){
                system.debug('ERROR UPDATING Enrolment_Unit_of_Study__c: ' + e.getMessage());
            }
        }
    }

    /**
    *   @author Original: unknown
    *   @description This method will populate Unit Value upon insert/update
    *   @history 
    *       Nick Guia   Nov. 19,2014    - removed unused input: Set<Id> euIds
    *                                   - Code optimization
    */
    public static void populateUnitValue(Set<Id> liquIds, List<Enrolment_Unit__c> euList){

        //get Unit Values
        Map<Id, Line_Item_Qualification_Units__c> liquMap = new Map<Id, Line_Item_Qualification_Units__c>(
                                                                [SELECT Id, Unit_Value__c 
                                                                FROM Line_Item_Qualification_Units__c 
                                                                WHERE Id IN: liquIds]);
        
        //assign Unit Values
        for(Enrolment_Unit__c e : euList){
            if(liquMap.containsKey(e.Line_Item_Qualification_Unit__c)){
                e.Unit_Value__c = liquMap.get(e.Line_Item_Qualification_Unit__c).Unit_Value__c;
            }
        }
    }
    

    /**
    *   @author Original: unknown
    *   @description This method will update the Delivery Mode to Internal Mode of Attendance at an on-shore or off-shore campus if result is 6 or 51
    *   @history 
    *       Nick Guia   Nov. 19,2014    Code optimization
    */
    public static void populateDeliveryModeType(Set<Id> euIds){

        if(isRunning == false){
            
            isRunning = true;
            Profile_CS__c prof = Profile_CS__c.getInstance('DeliveryModeUpdate');
            //requery to get Result_Code__c
            List<Enrolment_Unit__c> euList = [SELECT Id, Name, Delivery_Mode__c, Unit_Result__r.Result_Code__c 
                                            FROM Enrolment_Unit__c 
                                            WHERE Id IN: euIds];
               
            if(euList.size() > 0){
                for(Enrolment_Unit__c eu: euList){
                    if(eu.Unit_Result__r.Result_Code__c == '51' || eu.Unit_Result__r.Result_Code__c == '6'){
                        eu.Delivery_Mode__c = prof.Profile_Id__c;
                    }
                }
                
                try{
                    update euList;
                } catch(Exception e){
                    system.debug('ERROR: ' + e.getMessage());
                }    
            }
        }
    }
   

    /**
    *   @author Original: unknown
    *   @description This method will map Enrolment Unit result to Enrolment Unit of Study result
    *   @history 
    *       Nick Guia   Nov. 19,2014    - Code optimization
    *                                   - Changed input to EU Id Set
    *       29.Mar.2016 Biao Zhang      - Rewrite all logic
    */
    //public static void populateEUOSResult(List<Enrolment_Unit__c> enrolmentUnits){
    //    //Map of Results__c->Id TO Results__c
    //    Map<Id, Results__c> resultMap = new Map<Id, Results__c>();
    //    for (Results__c r : [SELECT Id, Name, National_Outcome_Code__c FROM Results__c WHERE Inactive__c = false]) {
    //        resultMap.put(r.Id, r);
    //    }

    //    //Map of Results__c->National_Outcome_Code__c TO Completion_Status_Type__c->Name 
    //    Map<String, String> resultToCompletionStatusTypeMap = new Map<String, String>();
    //    resultToCompletionStatusTypeMap.put('20', 'Successfully completed all the requirements');
    //    resultToCompletionStatusTypeMap.put('30', 'Failed');
    //    resultToCompletionStatusTypeMap.put('40', 'Withdrew without penalty');
    //    resultToCompletionStatusTypeMap.put('51', 'Recognition of prior learning');
    //    resultToCompletionStatusTypeMap.put('52', 'Unit to be commenced later in the year or still in process of completing');
    //    resultToCompletionStatusTypeMap.put('60', '');
    //    resultToCompletionStatusTypeMap.put('61', 'Unit to be commenced later in the year or still in process of completing');
    //    resultToCompletionStatusTypeMap.put('70', 'Unit to be commenced later in the year or still in process of completing');
    //    resultToCompletionStatusTypeMap.put('81', 'Successfully completed all the requirements');
    //    resultToCompletionStatusTypeMap.put('82', 'Withdrew without penalty');
    //    resultToCompletionStatusTypeMap.put('90', 'Unit to be commenced later in the year or still in process of completing');

    //    //Map of Completion_Status_Type__c->Id TO Completion_Status_Type__c->Name
    //    Map<String, Id> completionStatusTypeMap = new Map<String, Id>();
    //    for (Completion_Status_Type__c cs : [SELECT Id, Name FROM Completion_Status_Type__c]) {
    //        completionStatusTypeMap.put(cs.Name, cs.Id);
    //    }

    //    //Map of Enrolment Unit to its first blank result Enrolment Unit of Study. 
    //    Map<Id, Enrolment_Unit_of_Study__c> euIdtoEUOSMap = new Map<Id, Enrolment_Unit_of_Study__c>();

    //    Set<Id> euIds = new Set<Id>();
    //    for(Enrolment_Unit__c eu : enrolmentUnits) {
    //        euIds.add(eu.Id);
    //    }
    //    //Order by DESC thus overwrite the latter one if there are more than one EUOS for a EU
    //    for(Enrolment_Unit_of_Study__c euos : [SELECT  Enrolment_Unit__c, Result__c FROM Enrolment_Unit_of_Study__c WHERE Enrolment_Unit__c =: euIds ORDER BY Name DESC]) {
    //        if(euIdtoEUOSMap.containsKey(euos.Enrolment_Unit__c)) {
    //            if(euos.Result__c == null) {
    //                euIdtoEUOSMap.put(euos.Enrolment_Unit__c, euos);
    //            }
    //        } else {
    //            euIdtoEUOSMap.put(euos.Enrolment_Unit__c, euos);
    //        }
    //    }

    //    //update EUOS
    //    for(Enrolment_Unit__c eu : enrolmentUnits) {
    //        if(euIdtoEUOSMap.containsKey(eu.Id)) {
    //            if(resultMap.get(eu.Unit_Result__c).National_Outcome_Code__c == '90' && resultMap.get(eu.Unit_Result__c).Name.containsIgnoreCase('fail') ) {
    //                //90 can map to either 'Unit to be commenced later in the year of still in process of completing' or 'Failed'
    //                //by default 90 map to 'Unit to be commenced later in the year of still in process of completing', so we only care for special case 'fail' here
    //                euIdtoEUOSMap.get(eu.Id).Result__c = completionStatusTypeMap.get(resultToCompletionStatusTypeMap.get('30'));
    //            } else {
    //                euIdtoEUOSMap.get(eu.Id).Result__c = completionStatusTypeMap.get(resultToCompletionStatusTypeMap.get(resultMap.get(eu.Unit_Result__c).National_Outcome_Code__c));
    //            }
    //        }
    //    }

    //    update euIdtoEUOSMap.values();
    //}

    public static void enrolmentUnitSupersession(List<Enrolment_Unit__c> newEnrolmentUnitList, Map<Id, Enrolment_Unit__c> oldEnrolmentUnitMap){
        /**
         * @description This captures the enrolment supersession whenever the unit changes
         *              -up to a maximum of 5 updates only.
         * @author Sairah Hadjinoor
         * @date 06.NOV.2014 
         * @history - Updated By Neha Batra 16.DEC.2014 while reconciling changes across different org.
         */
        for(Enrolment_Unit__c eUnit: newEnrolmentUnitList){
            //check if the unit has changed
            //so that the trigger will only run on change of unit
            if(eUnit.Unit__c!= oldEnrolmentUnitMap.get(eUnit.Id).Unit__c){
                //check if the Has_Been_Superseded__c field is false
                //if it is, that would mean that that is the first time the Unit is changing
                if(eUnit.Supersession_Count__c==null){ 
                    eUnit.Supersession_Count__c = 0;
                }
                if(!eUnit.Has_Been_Superseded__c){ 
                    //populate superseded 1 fields
                    //this is for the old values of enrolment unit ref # and Unit
                    //change the end date of EU 1 to date today, meaning the Unit for EU 1 has ended once the unit has changed
                    eUnit.Superseded_1_EU_ID__c = eUnit.Name;
                    eUnit.Superseded_1_Unit_ID__c = oldEnrolmentUnitMap.get(eUnit.Id).Unit_Code__c;
                    eUnit.Superseded_1_EU_End_Date__c = Date.today();
                    //populate superseded 2 fields
                    //this is for the new values of enrolment unit ref # and Unit
                    //superseded end date will remain blank because the Unit is still in progress
                    eUnit.Superseded_2_EU_ID__c = eUnit.Name + 'A';
                    eUnit.Superseded_2_Unit_ID__c = eUnit.Unit_Code__c;
                    //has been superseded checkbox is set to true
                    //this would let the code know that the unit has been changed already at least once
                    eUnit.Has_Been_Superseded__c = true;
                    eUnit.Supersession_Count__c =2;
                }
                else{
                    if(eUnit.Superseded_1_EU_End_Date__c!=null && eUnit.Superseded_2_EU_End_Date__c == null){
                        //populate superseded 3 fields
                        //this is for the new values of enrolment unit ref # and Unit
                        //change the end date of EU 2 to date today, meaning the Unit for EU 2 has ended once the unit has changed
                        eUnit.Superseded_3_EU_ID__c = eUnit.Name+'B';
                        eUnit.Superseded_3_Unit_ID__c = eUnit.Unit_Code__c;
                        eUnit.Superseded_2_EU_End_Date__c = Date.today();
                        eUnit.Supersession_Count__c =3;
                    }
                    else if(eUnit.Superseded_2_EU_End_Date__c!=null && eUnit.Superseded_3_EU_End_Date__c == null){
                        //populate superseded 4 fields
                        //this is for the new values of enrolment unit ref # and Unit
                        //change the end date of EU 3 to date today, meaning the Unit for EU 3 has ended once the unit has changed
                        eUnit.Superseded_4_EU_ID__c = eUnit.Name+'C';
                        eUnit.Superseded_4_Unit_ID__c = eUnit.Unit_Code__c;
                        eUnit.Superseded_3_EU_End_Date__c = Date.today();
                        eUnit.Supersession_Count__c =4;
                    }
                    else if(eUnit.Superseded_3_EU_End_Date__c!=null && eUnit.Superseded_4_EU_End_Date__c == null){
                        //populate superseded 5 fields
                        //this is for the new values of enrolment unit ref # and Unit
                        //change the end date of EU 4 to date today, meaning the Unit for EU 4 has ended once the unit has changed
                        eUnit.Superseded_5_EU_ID__c = eUnit.Name+'D';
                        eUnit.Superseded_5_Unit_ID__c = eUnit.Unit_Code__c;
                        eUnit.Superseded_4_EU_End_Date__c = Date.today();
                        eUnit.Supersession_Count__c =5;
                    }
                    else if(eUnit.Superseded_4_EU_End_Date__c!=null && eUnit.Superseded_5_EU_End_Date__c == null){
                        //populate superseded 6 fields
                        //this is for the new values of enrolment unit ref # and Unit
                        //change the end date of EU 5 to date today, meaning the Unit for EU 5 has ended once the unit has changed
                        eUnit.Superseded_6_EU_ID__c = eUnit.Name+'E';
                        eUnit.Superseded_6_Unit_ID__c = eUnit.Unit_Code__c; 
                        eUnit.Superseded_5_EU_End_Date__c = Date.today();
                        eUnit.Supersession_Count__c =6;
                    }
                    else{
                        //populate superseded 6 enrolment unit end date
                        //this is the maximum number of time the unit will be changed 
                        //so we will only change the end date
                        //for any subsequent changes after this,  we will not do anything
                        if(eUnit.Superseded_5_EU_End_Date__c!=null && eUnit.Superseded_6_EU_End_Date__c == null){
                            eUnit.Superseded_6_EU_End_Date__c = Date.today();
                        }
                        
                    }
                }
            }
        }

    }
}