public without sharing class NATDataDefinition {
    
    /* 
     * NATDataDefinition Class - Data definition class used with NAT Generator 2
     * Mar 2015 - M.Wheeler - CloudSherpas
     */
    
    // Strings used here to define field references, if they need to changed for country wide AVETMISS requirements, they are to be changed here    
    
    //Common Fields - Utility
    Public Static final String BlankField = 'BlankField';


    //Common Fields - Account Object
    public static final String StateIdentifier = 'State_Identifier__c';
    public static final String StudentIdentifier = 'Student_Identifier__c'; //NAT00120,NAT00130
    public static final String StudentIdentifier2 = 'Student_Identifer__c'; //NAT00080-100 some muppet spelt the field name wrong :/
    
    //Common Fields - Qualification/Enrolment Object , NAT00130, NAT00030
    public static final String ParentQualificationCode = 'Parent_Qualification_Code__c';

  
    
    // NAT00020 Fields - Location__c Object
    public static final String TrainingOrganisationTrainingOrgIdentifier = 'Training_Organisation__r.Training_Org_Identifier__c';
    public static final String DeliveryLocationIdentifier = 'Delivery_Location_Identifier__c';
    public static final String LocName = 'Name';
    public static final String ChildPostcode = 'Child_Postcode__c';
    public static final String LocStateIdentifier = 'State_Identifier__c';
    public static final String ChildSuburb = 'Child_Suburb__c';
    public static final String CountryIdentifier = 'Country_Identifier__c';
    
    
    // NAT00030 Fields - Qualfication__c Object
    public static final String ParentQualificationName = 'Parent_Qualification_Name__c';
    public static final String ReportableQualificationHours = 'BlankField'; //Need to find field for each state
    public static final String RecognitionStatusIdentifier = 'Recognition_Status_Identifier__c';
    public static final String QualificationCategoryIdentifier = 'Qualification_Category_Identifier__c';
    public static final String FieldOfEducationIdentifier = 'Field_Of_Education_Identifier__c';
    public static final String ANZSCOIdentifier = 'ANZSCO_Identifier__c';
    public static final String VetNonVet = 'Vet_Non_Vet__c';

    // NAT00060 Fields - Unit Object
    public static final String SubjectFlag = 'C';
    public static final String SubjectIdentifier = 'Name';
    public static final String SubjectName = 'Unit_Name__c';
    public static final String SubjectFieldOfEducationIdentifier = 'Field_of_Education_Identifier__c';    
    public static final String VetFlag = 'Vet_Non_Vet__c';  
   
    
    // NAT00080 Fields - Account Object 
    public static Final String NameForEncryption = 'Name_for_encryption_student__c';
    public static Final String FullStudentName = 'Full_Student_Name__c';
    public static Final String HighestSchoolLevelCompIdentifier = 'Highest_School_Level_Comp_Identifier__c';
    public static Final String YearHighestEducationCompleted = 'Year_Highest_Education_Completed__c';
    public static Final String Sex = 'Sex__c';
    public static Final String PersonBirthDate = 'PersonBirthDate';
    public static Final String PersonMailingPostalCode = 'PersonMailingPostalCode';
    public static Final String IndigenousStatusIdentifier = 'Indigenous_Status_Identifier__c';
    public static Final String MainLanguageSpokenatHomeIdentifier = 'Main_Language_Spoken_at_Home_Identifier__c';
    public static Final String LabourForceStatusIdentifier = 'Labour_Force_Status_Identifier__c';
    public static Final String CountryofBirthIdentifer = 'Country_of_Birth_Identifer__c';
    public static Final String DisabilityFlag0 = 'Disability_Flag_0__c';
    public static Final String PriorAchievementFlag = 'Prior_Achievement_Flag__c';
    public static Final String AtSchoolFlag = 'At_School_Flag__c';
    public static Final String ProficiencyinSpokenEnglishIdentifier = 'Proficiency_in_Spoken_English_Identifier__c';
    public static Final String PersonMailingCity = 'PersonMailingCity';
    public static Final String UniqueStudentIdentifier = 'Unique_Student_Identifier__c';
    //Public static String StateIdentifier = 'State_Identifier__c';
    
    public static Final String Addressbuildingpropertyname = 'Address_building_property_name__c';
    public static Final String Addressflatunitdetails = 'Address_flat_unit_details__c';
    public static Final String Addressstreetnumber = 'Address_street_number__c';
    public static Final String Addressstreetname = 'Address_street_name__c';
    
    // NAT00085 Fields - Account Object
    public static Final String ClientTitle = 'Salutation';
    public static Final String ClientFirstGivenName = 'FirstName';
    public static Final String ClientLastName = 'LastName';
    public static Final String PostalBuildingPropertyName = 'Postal_building_property_name__c';
    public static Final String PostalFlatUnitDetails = 'Postal_flat_unit_details__c';
    public static Final String PostalStreetNumber = 'Postal_street_number__c';
    public static Final String PostalStreetName = 'Postal_street_name__c';
    public static Final String PostalDeliveryBox = 'Postal_delivery_box__c';
    public static Final String PostalSuburbLocalityTown = 'Postal_suburb_locality_or_town__c';
    public static Final String PostalPostCode = 'PersonOtherPostalCode';
    public static Final String ReportingOtherStateIdentifier = 'Reporting_Other_State_Identifier__c';
    public static Final String PersonHomePhone = 'PersonHomePhone';
    public static Final String PersonWorkPhone = 'Work_Phone__c';
    public static Final String PersonMobilePhone = 'PersonMobilePhone';
    public static Final String PersonEmail = 'PersonEmail';
    
     // NAT00090 Fields - Account Object
    public static Final String DisabilityTypeIdentifier = 'Disability_Type__c';

    // NAT00100 Fields - Account Object
    public static Final String PriorEducationAchievementIdentifier = 'Prior_Achievement_Type_s__c';
    
     // NAT00120 Fields - Enrolment Unit Object
    public static Final String TrainingDeliveryLocationIdentifier = 'Delivery_Location_Identifier__c';
    public static Final String EUSubjectIdentifier = 'Unit_of_Competency_Identifier__c';
    public static Final String ActivityStartDate = 'Start_Date__c';
    public static Final String ActivityEndDate = 'End_Date__c';
    
    public static Final String DeliveryModeIdentifier = 'Delivery_Mode_Identifier__c';
    public static Final String OutcomeIdentifierNational = 'AVETMISS_National_Outcome__c';
    public static Final String ScheduledHours = 'Scheduled_Hours__c';    
    public static Final String CommencingProgramIdentifier = 'Commencing_Course_Identifier__c';
    public static Final String ClientIdentifierApprenticeships = 'Client_Identifier_New_Apprenticeships__c';
    public static Final String StudyReasonIdentifier = 'Study_Reason_Identifier__c';
    public static Final String VETInSchoolsFlag = 'VET_in_Schools__c';
    public static Final String ClientTuitionFee = 'Reportable_Tuition_Fee__c';
    public static Final String FeeExemptionTypeIdentifier = 'Enrolment__r.Fee_Exemption__r.Code__c'; 
    public static Final String PurchasingContractIdentifier = 'Purchasing_Contract_Identifier__c';
    public static Final String PurchasingContractScheduleIdentifier = 'Purchasing_Contract_Schedule_Identifier__c';
    public static Final String HoursAttended = 'Hours_Attended__c';
    //NAT00120 Relationship Fields
    public static Final String ProgramIdentifier = 'Enrolment__r.Qualification__r.Parent_Qualification_Code__c';
    public static Final String FundingSourceNational = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.National_Fund_Source_Identifier__c';
    public static Final String TrainingContractIdentifier = 'Enrolment__r.Training_Contract_Identifier__c';
    public static Final String SpecificFundingIdentifier = 'Enrolment__r.Specific_Program_Identifier__c';
    public static Final String StateFundSourceIdentifier = 'Line_Item_Qualification_Unit__r.Line_Item_Qualification__r.State_Fund_Source_Identifier__c';
    public static Final String AssociatedCourseIdentifier = 'Enrolment__r.Qualification__r.Associated_Course_Identifier__c';

    
     // NAT00060 Fields
    public Static Final String NominalHours = 'Line_Item_Qualification_Unit__r.Nominal_Hours__c';
    
    
    // NAT00130 Fields - Enrolment Object
    public static Final String TrainingOrganisationIdentifier = 'Enrolment__r.Training_Organisation_Identifier__c';
    public static Final String ParentQualificationCodeE = 'Enrolment__r.Parent_Qualification_Code__c';
    public static Final String StudentIdentifierE = 'Enrolment__r.Student_Identifier__c';

  
     // NAT00130 Fields - Award Object
    public static Final String YearProgramCompleted = 'Year_Program_Completed__c';
    public static Final String IssuedFlag = 'Qualification_Issued__c';
   
    
    
    
    // NAT00010 Fields - Account Object
    public static Final String TrainingOrgIdentifier = 'Account.Training_Org_Identifier__c';
    public static Final String AVETMISSOrganisationName = 'Account.AVETMISS_Organisation_Name__c';
    public static Final String TrainingOrgTypeIdentifier = 'Account.Training_Org_Type_Identifier__c';
    public static Final String BillingStreet = 'Account.BillingStreet';
    public static Final String BillingCity = 'Account.BillingCity';
    public static Final String BillingPostalCode = 'Account.BillingPostalCode';
    public static Final String StateIdentifierA = 'Account.State_Identifier__c';
    
    // NAT00010 Fields - Contact Object
    public static Final String FirstName = 'FirstName';
    public static Final String LastName = 'LastName';
    public static Final String Phone = 'Phone';
    public static Final String Fax = 'Fax';
    public static Final String Email = 'Email';
    public static Final String FirstLast = 'FirstLast';

    public static Map<Integer, NATDataElement> nat00010() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();
        
        positions.put(1, new NATDataElement(TrainingOrgIdentifier, 10, true, 'string') );
        positions.put(2, new NATDataElement(AVETMISSOrganisationName, 100, true, 'string' ) );  
        positions.put(3, new NATDataElement(TrainingOrgTypeIdentifier, 2, true, 'string' ) ); 
        positions.put(4, new NATDataElement(BillingStreet, 50, true, 'string' ) );   
        positions.put(5, new NATDataElement(BlankField, 50, true, 'string' ) );
        positions.put(6, new NATDataElement(BillingCity, 50, true, 'string' ) ) ;   
        positions.put(7, new NATDataElement(BillingPostalCode, 4, true, 'string' ) ); 
        positions.put(8, new NATDataElement(StateIdentifierA, 2, true, 'string' ) );
        positions.put(9, new NATDataElement(FirstLast, 60, true, 'string') );
        positions.put(10, new NATDataElement(Phone, 20, false, 'string') );
        positions.put(11, new NATDataElement(Fax, 20, false, 'string') );
        positions.put(12, new NATDataElement(Email, 80, false, 'string') );
                
        return positions;
        
    }
    
    public static Map<Integer, NATDataElement> nat00020() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();
        
        positions.put(1, new NATDataElement(TrainingOrganisationTrainingOrgIdentifier, 10, true, 'string') );
        positions.put(2, new NATDataElement(DeliveryLocationIdentifier, 10, FALSE, 'string' ) );   
        positions.put(3, new NATDataElement(LocName, 100, FALSE, 'string' ) ); 
        positions.put(4, new NATDataElement(ChildPostcode, 4, FALSE, 'string' ) ); 
        positions.put(5, new NATDataElement(LocStateIdentifier, 2, FALSE, 'string' ) ) ;   
        positions.put(6, new NATDataElement(ChildSuburb, 50, FALSE, 'string' ) ); 
        positions.put(7, new NATDataElement(CountryIdentifier, 4, FALSE, 'string' ) );  
        //positions.put(8, new NATDataElement(BlankField, 1, true, 'string') );
        return positions;
        
    }
   
    public static Map<Integer, NATDataElement> nat00030() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(ParentQualificationCode, 10, false, 'string') );
        positions.put(2, new NATDataElement(ParentQualificationName, 100, false, 'string') );
        positions.put(3, new NATDataElement(ReportableQualificationHours, 4, true, 'number') );
        positions.put(4, new NATDataElement(RecognitionStatusIdentifier, 2, false, 'string') );
        positions.put(5, new NATDataElement(QualificationCategoryIdentifier, 3, false, 'number') );
        positions.put(6, new NATDataElement(FieldOfEducationIdentifier, 4, false, 'number') );
        positions.put(7, new NATDataElement(ANZSCOIdentifier, 6, false, 'string') );
        positions.put(8, new NATDataElement(VetNonVet, 1, false, 'boolean' ) ); 
         
        return positions;
        
    }
    
    public static Map<Integer, NATDataElement> nat00060() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(SubjectFlag, 1, true, 'string') );
        positions.put(2, new NATDataElement(SubjectIdentifier, 12, false, 'string') );
        positions.put(3, new NATDataElement(SubjectName, 100, false, 'string') );
        positions.put(4, new NATDataElement(SubjectFieldOfEducationIdentifier, 6, false, 'string') );
        positions.put(5, new NATDataElement(VetFlag, 3, false, 'boolean') ); 
        //positions.put(6, new NATDataElement(NominalHours, 4, true, 'number') ); field is repoted, but within NAT gen class
       
        return positions;
        
    }
     public static Map<Integer, NATDataElement> nat00080() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(StudentIdentifier2, 10, false, 'string') );
        positions.put(2, new NATDataElement(NameForEncryption, 60, false, 'string') );
        positions.put(3, new NATDataElement(HighestSchoolLevelCompIdentifier, 2, false, 'string') );
        positions.put(4, new NATDataElement(YearHighestEducationCompleted, 4, false, 'string') );
        positions.put(5, new NATDataElement(Sex, 1, false, 'string') );
        positions.put(6, new NATDataElement(PersonBirthDate, 8, false, 'date') );
        positions.put(7, new NATDataElement(PersonMailingPostalCode, 4, false, 'string') );
        positions.put(8, new NATDataElement(IndigenousStatusIdentifier, 1, false, 'string' ) ); 
        positions.put(9, new NATDataElement(MainLanguageSpokenatHomeIdentifier, 4, false, 'string' ) ); 
        positions.put(10, new NATDataElement(LabourForceStatusIdentifier, 2, false, 'string' ) ); 
        positions.put(11, new NATDataElement(CountryofBirthIdentifer, 4, false, 'string' ) ); 
        positions.put(12, new NATDataElement(DisabilityFlag0, 1, false, 'string' ) ); 
        positions.put(13, new NATDataElement(PriorAchievementFlag, 1, false, 'string' ) ); 
        positions.put(14, new NATDataElement(AtSchoolFlag, 1, false, 'string' ) ); 
        positions.put(15, new NATDataElement(ProficiencyinSpokenEnglishIdentifier, 1, true, 'string' ) ); 
        positions.put(16, new NATDataElement(PersonMailingCity, 50, false, 'string' ) );
        positions.put(17, new NATDataElement(UniqueStudentIdentifier, 10, false, 'string' ) );
        positions.put(18, new NATDataElement(StateIdentifier, 2, false, 'string' ) );
        positions.put(19, new NATDataElement(Addressbuildingpropertyname, 50, false, 'string' ) );
        positions.put(20, new NATDataElement(Addressflatunitdetails, 30, false, 'string' ) );
        positions.put(21, new NATDataElement(Addressstreetnumber, 15, false, 'string' ) );
        positions.put(22, new NATDataElement(Addressstreetname, 70, false, 'string' ) );
         
         
         
        return positions;
        
    }
    
    
     public static Map<Integer, NATDataElement> nat00085() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(StudentIdentifier2, 10, false, 'string') );
        positions.put(2, new NATDataElement(ClientTitle, 4, false, 'string') );
        positions.put(3, new NATDataElement(ClientFirstGivenName, 40, false, 'string') );
        positions.put(4, new NATDataElement(ClientLastName, 40, false, 'string') );
        positions.put(5, new NATDataElement(PostalBuildingPropertyName, 50, false, 'string') );
        positions.put(6, new NATDataElement(PostalFlatUnitDetails, 30, false, 'string') );
        positions.put(7, new NATDataElement(PostalStreetNumber, 15, false, 'string') );
        positions.put(8, new NATDataElement(PostalStreetName, 70, false, 'string' ) ); 
        positions.put(9, new NATDataElement(PostalDeliveryBox, 22, false, 'string' ) ); 
        positions.put(10, new NATDataElement(PostalSuburbLocalityTown, 50, false, 'string' ) ); 
        positions.put(11, new NATDataElement(PostalPostCode, 4, false, 'string' ) ); 
        positions.put(12, new NATDataElement(ReportingOtherStateIdentifier, 2, true, 'string' ) ); 
        positions.put(13, new NATDataElement(PersonHomePhone, 20, false, 'string' ) ); 
        positions.put(14, new NATDataElement(PersonWorkPhone, 20, false, 'string' ) ); 
        positions.put(15, new NATDataElement(PersonMobilePhone, 20, false, 'string' ) );
        positions.put(16, new NATDataElement(PersonEmail, 80, false, 'string' ) );
      
         
        return positions;
        
    }
    
    public static Map<Integer, NATDataElement> nat00090() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(StudentIdentifier2, 1, true, 'string') );
        positions.put(2, new NATDataElement(DisabilityTypeIdentifier, 2, true, 'string') );
        
        return positions;
    
    }
    
      public static Map<Integer, NATDataElement> nat00100() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(StudentIdentifier2, 1, true, 'string') );
        positions.put(2, new NATDataElement(PriorEducationAchievementIdentifier, 3, true, 'string') );

        
        return positions;
    
    }
    
       public static Map<Integer, NATDataElement> nat00120() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(TrainingDeliveryLocationIdentifier, 10, false, 'string') );
        positions.put(2, new NATDataElement(StudentIdentifier, 10, false, 'string') );
        positions.put(3, new NATDataElement(EUSubjectIdentifier, 12, false, 'string') );
        positions.put(4, new NATDataElement(ProgramIdentifier, 10, true, 'string') );
        positions.put(5, new NATDataElement(ActivityStartDate, 8, false, 'date') );
        positions.put(6, new NATDataElement(ActivityEndDate, 8, false, 'date') );
        positions.put(7, new NATDataElement(DeliveryModeIdentifier, 2, false, 'number') );
        positions.put(8, new NATDataElement(OutcomeIdentifierNational,2, false, 'string') );
        positions.put(9, new NATDataElement(ScheduledHours, 4, false, 'number') );
        positions.put(10, new NATDataElement(FundingSourceNational, 2, true, 'number') );
        positions.put(11, new NATDataElement(CommencingProgramIdentifier, 1, false, 'number') );
        positions.put(12, new NATDataElement(TrainingContractIdentifier, 10, true, 'string') );
        positions.put(13, new NATDataElement(ClientIdentifierApprenticeships, 10, false, 'string') );
        positions.put(14, new NATDataElement(StudyReasonIdentifier, 2, false, 'string') );
        positions.put(15, new NATDataElement(VETInSchoolsFlag, 1, false, 'string') );
        positions.put(16, new NATDataElement(SpecificFundingIdentifier, 10, true, 'string') );
        positions.put(17, new NATDataElement(OutcomeIdentifierNational, 3, false, 'string') );
        positions.put(18, new NATDataElement(StateFundSourceIdentifier, 3, true, 'string') );
        positions.put(19, new NATDataElement(ClientTuitionFee, 4, false, 'number') );
        positions.put(20, new NATDataElement(FeeExemptionTypeIdentifier, 1, true, 'string') );
        positions.put(21, new NATDataElement(PurchasingContractIdentifier, 12, true, 'string') ); 
        positions.put(22, new NATDataElement(PurchasingContractScheduleIdentifier, 3, false, 'string') ); 
        positions.put(23, new NATDataElement(HoursAttended, 4, false, 'number') );
        positions.put(24, new NATDataElement(AssociatedCourseIdentifier, 10, true, 'string') ); 
           
        
        return positions;
    
    }
    
    public static Map<Integer, NATDataElement> nat00130() {
        
        Map<Integer, NATDataElement> positions = new Map<Integer, NATDataElement>();        
        
        positions.put(1, new NATDataElement(TrainingOrganisationIdentifier, 10, true, 'string') );
        positions.put(2, new NATDataElement(ParentQualificationCodeE, 10, true, 'string') );
        positions.put(3, new NATDataElement(StudentIdentifierE, 10, true, 'string') );
        positions.put(4, new NATDataElement(YearProgramCompleted, 4, false, 'number') );
        positions.put(5, new NATDataElement(IssuedFlag, 1, false, 'string') );
        
          return positions;
    }
     
}