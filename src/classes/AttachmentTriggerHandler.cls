/**
*	@author	Nick Guia
*	@description Trigger handler class for AttachmentTrigger
*	@history
*		AUG.18.2015		Nick Guia	Created
*/
public class AttachmentTriggerHandler {

	private static final String INTERVENTION_STRATEGY_RECORDTYPE_NAME = 'INT - Intervention Strategy';

	public static void onBeforeInsert(List<Attachment> attachmentList) {
		//on before insert logic here
	}

	public static void onAfterInsert(List<Attachment> newAttachmentList, Map<Id,Attachment> oldAttachmentMap) {
		Set<Id> enrolmentIds = new Set<Id>();
		for(Attachment a : newAttachmentList) {
			//filter attachments from enrolment object
			if(a.ParentId.getSobjectType() == Enrolment__c.SobjectType &&
				a.Name.contains(INTERVENTION_STRATEGY_RECORDTYPE_NAME))
			{
				enrolmentIds.add(a.ParentId);
			}
		}

		if(!enrolmentIds.isEmpty()) {
			deleteEnrolmentPlaceholders(enrolmentIds);
		}
	}

	/**
	*	@description method for deleting placeholder objects related to enrolment once
	*				conga document has been attached to enrolment record
	*/
	private static void deleteEnrolmentPlaceholders(Set<Id> enrolmentIds) {
		//get existing placeholders related to the enrolment
		List<Intervention_Letter_Placeholder__c> deletePlaceholderList = [SELECT Id FROM Intervention_Letter_Placeholder__c WHERE Enrolment__c in :enrolmentIds];
		delete deletePlaceholderList;
	}
}