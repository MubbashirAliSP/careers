@isTest
private class PurchasingContractsAndChildren_Test {
    /*
        Test Purchasing Contract Trigger Handler
    */
    static testmethod void testPConTriggerHandler(){
        Id rt = [select Id from RecordType where Name='Unit Funded' and SObjectType ='Purchasing_Contracts__c'].Id;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='PCON_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Purchasing_Contracts__c pc = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c cli = new Contract_Line_Items__c();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Line_Item_Qualifications__c liq = new Line_Item_Qualifications__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qu = new Qualification_Unit__c();
        Qualification_Unit__c qu2 = new Qualification_Unit__c();
        Line_Item_Qualification_Units__c liqu = new Line_Item_Qualification_Units__c();
        Unit_Hours_and_Points__c uhp = new Unit_Hours_and_Points__c();
        State__c st = new State__c();
        Country__c c = new Country__c();
        Enrolment__c enrolment = new Enrolment__c();
        Account student = new Account();
        Account accTraining = new Account();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c(); 
        State__c state = new State__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation();
                c = TestCoverageUtilityClass.createCountry();
                st = TestCoverageUtilityClass.createState(c.Id);
                fSource =  TestCoverageUtilityClass.createFundingSource(st.Id);
                pc = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, accTraining.Id);
                cli = TestCoverageUtilityClass.createContractLineItem(pc.Id);
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                uhp = TestCoverageUtilityClass.createUnitHoursAndPoints(unit.Id, st.Id);
                qu = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                qu2 = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                liq = TestCoverageUtilityClass.createLineItemQualification(qual.Id, cli.Id);
                liqu = TestCoverageUtilityClass.createLineItemQualificationUnit(liq.Id, qu.Id);
                student = TestCoverageUtilityClass.createStudent();
                state = TestCoverageUtilityClass.createState(c.Id);
                loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(c.Id, state.Id, loading.Id);
                //loc = TestCoverageUtilityClass.createLocationWithParentTR(c.Id, accTraining.Id, parentLoc.Id, state.Id); 
                loc = TestCoverageUtilityClass.createLocationWithTO(c.Id, parentLoc.Id, accTraining.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParent(c.Id, accTraining.Id, parentLoc.Id, state.Id);
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType();
                enrolment = TestCoverageUtilityClass.createEnrolment(student.Id, qual.Id, liq.Id, loc2.Id, c.Id);
                enrolment.Type_of_Attendance__c = attend.Id;
                enrolment.Predominant_Delivery_Mode__c = del.Id;
                enrolment.Delivery_Location__c = loc.id;
                insert enrolment;
                pc.RecordTypeId = rt;
                pc.Contract_Type__c = 'Other Government Funding';
                update pc;
            test.stopTest();
        }
        
    }
    
}