/**
 * @description Awards Object Trigger Handler Test Coverage
 * @author Janella Lauren Canlas
 * @date 13.NOV.2012
 *
 * HISTORY
 * - 07.FEB.2013    Janella Canlas      Created.
 * - 04.MAR.2013    Janella Canlas      Removed seeAllData=true tag
 * - 08.APR.2014    Michelle Magsarili  Change Status to Active to pass Enrolment Validation
 * - 01.JUL.2014    Scott Gassmann      Updated Test Class to Pass in Production 
 * - 20.JAN.2016    Biao Zhang          Test method for Award Unit creation
 */
@isTest
private class AwardsTriggerHandler_Test {

    static testMethod void testAwardTriggerHandler() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Enrolment_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment_Unit__c enrl_unit = new Enrolment_Unit__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Location_Loadings__c locationLoading = new Location_Loadings__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Awards__c award = new Awards__c();
        Awards__c award2 = new Awards__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Results__c res = new Results__c();
        Results__c sres = new Results__c();
        
        system.runAs(u){
            test.startTest();
                acc = TestCoverageUtilityClass.createStudent();
                accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
                anz = TestCoverageUtilityClass.createANZSCO();
                fld = TestCoverageUtilityClass.createFieldOfEducation();
                qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
                unit = TestCoverageUtilityClass.createUnit();
                qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
                country = TestCoverageUtilityClass.createCountry(); 
                state = TestCoverageUtilityClass.createState(country.Id); 
                fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
                system.debug('**FSource: ' + fSource.Name + ' -- ' + fSource.Id);
                system.debug('**Acc: ' + acc.Name + ' -- ' + acc.Id);
                system.debug('**State: ' + state.Name + ' -- ' + state.Id);
                //pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id, acc.Id, state.Id);
                
                Id recordTypeId = [select id from recordtype where sObjectType = 'Purchasing_Contracts__c' limit 1].Id;
                pcon.Contract_Name__c = 'test Contract';
                pcon.Contract_Type__c = 'Apprenticeship Funding';
                pcon.RecordTypeId = recordTypeId;
                pcon.State_Fund_Source__c = fSource.Id;
                pcon.Contract_Code__c = '1234567890';
                pCon.Training_Organisation__c = accTraining.Id;
                pcon.Contract_State_Lookup__c = state.Id;
                insert pcon;
                
                conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
                lineItem = TestCoverageUtilityClass.createLineItemQualificationFS(qual.Id, conLine.Id, fSource.Id);
                lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
                res = TestCoverageUtilityClass.createNationalResult();
                res.Result_Code__c = '20';
                update res;
                sres = TestCoverageUtilityClass.CreateStateOutCome(res.id,state.id);
                sres.Result_Code__c = '20';
                sres.National_Outcome__c = res.Id;
                update sres;
                
                
                locationLoading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
                parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, locationLoading.Id);
                loc = TestCoverageUtilityClass.createLocationWithParent(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id); 
                del = TestCoverageUtilityClass.createDeliveryModeType();
                attend = TestCoverageUtilityClass.createAttendanceType(); 
                enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id);
                enrl.Overseas_Country__c = null;
                enrl.Predominant_Delivery_Mode__c = del.Id;
                enrl.Type_of_Attendance__c = attend.Id;
                enrl.Mailing_Country__c = 'AUSTRALIA';
                enrl.Mailing_Zip_Postal_Code__c = '4005';
                enrl.Other_Zip_Postal_Code__c = '4005';
                enrl.Enrolment_Status__c = 'Active (Commencement)'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                enrl.Start_Date__c = date.today() - 50;
                enrl.End_Date__c = date.today() + 50;
                enrl.Mailing_State_Provience__c = 'Queensland';
                insert enrl;
                String enrId = enrl.Id;
                system.debug('**Enrolment: ' + enrId);
                //update enrl;
               
                Enrolment_Unit__c enrl_unit1 = [SELECT Id, Name, Unit_Result__c, Result_Outcome_Code__c FROM Enrolment_Unit__c WHERE Enrolment__c =: enrId];
                enrl_unit1.Unit_Result__c = sres.Id;
                enrl_unit1.Result_Outcome_Code__c = '20';
                enrl_unit1.Start_Date__c = Date.TODAY() - 25;
                enrl_unit1.End_Date__c = Date.TODAY() - 1;
                update enrl_unit1;
                                 
                Variation_Reason_Code__c varCode = new Variation_Reason_Code__c(
                Name='Additional record or new record reported for the first time.',
                Code__c = '5');
                insert varCode;

                enrl = [SELECT No_Enrolment_Units_with_Final_Results__c, Number_of_Enrolment_Units__c FROM Enrolment__c WHERE Id =: enrId];
                enrl.Enrolment_Status__c = 'Completed'; //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation
                update enrl;
                //MAM 04/08/2014 end
                                                                                             
                award = TestCoverageUtilityClass.createAward(enrl.Id);

                Id approvedRTID = Schema.SObjectType.Awards__c.getRecordTypeInfosByName().get('Approved Award').getRecordTypeId();
                award.RecordTypeId = approvedRTID;

                system.assertEquals(0, [SELECT count() from AwardUnit__c]);
                update award;
                system.assertEquals(1, [SELECT count() from AwardUnit__c]);

                try
                {
                    award2 = TestCoverageUtilityClass.createAward(enrl.Id);
                }
                catch(Exception e)
                {
                    Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot create 2 Award Certificates for this Enrolment.') ? true : false;
                    System.AssertEquals(expectedExceptionThrown, true);
                } 
            test.stopTest();
        }
    }

}