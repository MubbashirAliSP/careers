/** 
 * @description Batch process for exporting various records for InPlace
 * @author Ranyel Maliwanag
 * @date 18.JUN.2015
 */
public without sharing class InPlaceExportBatchFaculty implements Database.Batchable<SObject> {
    public String queryString;
    public InPlaceSettings__c settings;
    
    public InPlaceExportBatchFaculty() {
        settings = InPlaceSettings__c.getInstance();
        queryString = 'SELECT IPF_Description__c, IPF_Sync_on_next_update__c, Name, Id ';
        queryString += 'FROM InPlace_faculty__c ';
        queryString += 'WHERE IPF_Sync_on_next_update__c = true';
    }


    /**
     * @description start() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(queryString);
    }


    /**
     * @description execute() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void execute(Database.BatchableContext BC, List<InPlace_faculty__c> records) {
        List<InPlace__c> exports = new List<InPlace__c>();
        Set<String> exportedIdentifiers = new Set<String>();

        for (InPlace_faculty__c record : records) {
            if (!exportedIdentifiers.contains(record.Name)) {
                InPlace__c forExport = new InPlace__c(
                        IPFaculty_Description__c = record.IPF_Description__c, 
                        IPFaculty_FacultyCode__c = record.Name
                    );
                exports.add(forExport);
                exportedIdentifiers.add(record.Name);
            }
            
            record.IPF_Sync_on_next_update__c = false;
        }

        insert exports;
        
        if (settings.ToggleSyncFlagOnExport__c) {
            // Condition: custom setting is activated to unset the sync flags on export
            update records;
        }
    }


    /**
     * @description finish() method implementation of the Database.Batchable interface
     * @author Ranyel Maliwanag
     * @date 18.JUN.2015
     */
    public void finish(Database.BatchableContext BC) {
        InPlaceExportBatchCampus nextBatch = new InPlaceExportBatchCampus();
        Database.executeBatch(nextBatch);
    }
}