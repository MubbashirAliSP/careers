public with sharing class StaffIDPhoto_CC {
    
    private ApexPages.StandardController controller {get; set;}
    private Staff_ID_Card__c idCard;
    public Staff_ID_Card__c idCardRecord{get;set;}
    public StaffIDPhoto_CC(ApexPages.StandardController controller) {
        
        //initialize the stanrdard controller
        this.controller = controller;
        this.idCard = (Staff_ID_Card__c)controller.getRecord();
        idCardRecord = [select Id, Photo_ID__c FROM Staff_ID_Card__c where Id =: idCard.Id];
    }
    
    public Attachment attachment {
    get{
        if (attachment == null)
            attachment = new Attachment();
            return attachment;
    }
    set;
    }
    
    public PageReference upload() {
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = idCard.Id; // the record the file is attached to
        attachment.IsPrivate = false;
        try {
            insert attachment;
            //idCard.Staff_Photo_URL__c = 'https://cs6.salesforce.com/servlet/servlet.FileDownload?file='+attachment.id;
            idCardRecord.Photo_ID__c = attachment.Id;
            update idCardRecord;
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          return null;
        } finally {
          attachment = new Attachment(); 
        }
     
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        return null;
    }
    
    public PageReference deleteImage(){
        Attachment attach = [select Id from Attachment where Id =: idCardRecord.Photo_ID__c];
        try {
            if(attach != null){
                delete attach;
            }
            //idCard.Staff_Photo_URL__c = 'https://cs6.salesforce.com/servlet/servlet.FileDownload?file='+attachment.id;
            idCardRecord.Photo_ID__c = null;
            update idCardRecord;
            return null;} catch (DMLException e) {ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting attachment'));return null;
        } finally {
          attachment = new Attachment(); 
        }
    }
}