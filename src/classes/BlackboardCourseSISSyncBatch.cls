/*This class handles SIS component of course object for Blackboard Integration
 *It will retrieve Blackboard_course__c records that are only
 *T3 type and are only for insertion into BB
 *update and get are handled by BlackboardCourseWSSyncBatch.class
 *create by Yuri Gribanov 30th May 2014 - CloudSherpas
*/


global with sharing class BlackboardCourseSISSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {


    
    global final String query = 'Select Available_After_Sync__c, RecordType.DeveloperName,Id,Course_Description__c, Name, Template_Course__c,Template_Course__r.Name, create__c,Blackboard_Course_Name__c From Blackboard_Course__c Where create__c = True AND Sync_Required__c = True AND RecordType.DeveloperName = \'T3_Intake_Course\'';
    
    global BlackboardCourseSISSyncBatch() {


        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Blackboard_Course__c>bbcourses = new List<Blackboard_Course__c>();

        for(SObject s :scope) {
             //collect blackboard t3 courses into a list
             Blackboard_Course__c bbc = (Blackboard_Course__c) s;
             bbcourses.add(bbc);

         }

        //send bulk courses to SIS WS
        BlackboardCourseWS.saveCoursesSIS(bbcourses);
    
    }
    
    global void finish(Database.BatchableContext BC) {

        //proceed to next step, BlackboardCourseSyncbatch for t2/t3(get,update)/t4/t5
        BlackboardCourseSyncBatch b1 = new BlackboardCourseSyncBatch();
        //We test each functionality separate, chaining disabled to unit tests
        if(!Test.isRunningTest())
         database.executebatch(b1,1);
        
    }
    
}