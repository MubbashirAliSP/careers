/**
 * @description Test class for `EntryRequirementsTriggerHandler`
 * @author Ranyel Maliwanag
 * @date 29.FEB.2016
 */
@isTest
public with sharing class EntryRequirementTriggerHandler_Test {
	static Id formRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
	static Id llnRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();
	static Id year12RTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('Year 12 Certificate').getRecordTypeId();

	@testSetup 
	static void setup() {
		ApplicationsTestUtilities.initialiseCountryAndStates();
		ApplicationsTestUtilities.initialiseFormComponents();

		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
		studentLogin.AddressState__c = 'QLD';
		studentLogin.AccountId__c = student.Id;
		insert studentLogin;

		State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

		Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
		serviceCase.Account_Student_Name__c = student.Id;
		serviceCase.RecordTypeId = formRTID;
		serviceCase.ParentQualificationName__c = 'Diploma of Business';
		insert serviceCase;

		ApplicationForm__c appForm = new ApplicationForm__c(
				StudentCredentialsId__c = studentLogin.Id,
				StateId__c = stateQLD.Id,
				QualificationId__c = testQual.Id,
				RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
			);
		insert appForm;
	}

	static testMethod void processFailedACSFLevelsTest() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c requirement = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ServiceCaseId__c = serviceCase.Id,
				ApplicationFormId__c = appForm.Id
			);
		insert requirement;

		Test.startTest();
			requirement.Literacy_Level__c = 'English ACSF PL1';
			requirement.Numeracy_Level__c = 'Maths ACSF L2';
			update requirement;

			serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals('Closed', serviceCase.Status__c);

			requirement = [SELECT Id, Status__c, Literacy_Level__c, Numeracy_Level__c FROM EntryRequirement__c WHERE Id = :requirement.Id];
			System.assertEquals('LLN - Closed', requirement.Status__c);

			Opportunity opp = [SELECT Id, LLN_Literacy_Level__c, LLN_Numeracy_Level__c FROM Opportunity WHERE Service_Case__c = :serviceCase.Id AND RecordType.Name = 'Certificate Opportunity'];
			System.assertEquals(requirement.Literacy_Level__c, opp.LLN_Literacy_Level__c);
			System.assertEquals(requirement.Numeracy_Level__c, opp.LLN_Numeracy_Level__c);
		Test.stopTest();
	}

	static testMethod void processPassedACSFLevelsTest() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c llnER = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id
			);
		insert llnER;

		EntryRequirement__c year12ER = new EntryRequirement__c(
				RecordTypeId = year12RTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'Awaiting Upload'
			);
		insert year12ER;

		Test.startTest();
			llnER.Literacy_Level__c = 'English ACSF L4';
			llnER.Numeracy_Level__c = 'Maths ACSF L3';
			update llnER;

			serviceCase = [SELECT Id, Status__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertNotEquals('Closed', serviceCase.Status__c);

			year12ER = [SELECT Id, Status__c, Literacy_Level__c, Numeracy_Level__c FROM EntryRequirement__c WHERE Id = :year12ER.Id];
			System.assertEquals('Closed', year12ER.Status__c);
		Test.stopTest();
	}

	static testMethod void processEnrolmentFastTrackingTest() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c llnER = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id
			);
		insert llnER;

		Test.startTest();
			serviceCase = [SELECT Id, IsEnrolmentFastTracked__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(!serviceCase.IsEnrolmentFastTracked__c);

			llnER.Literacy_Level__c = 'English ACSF L2';
			llnER.Numeracy_Level__c = 'Maths ACSF L2';
			update llnER;

			serviceCase = [SELECT Id, IsEnrolmentFastTracked__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(serviceCase.IsEnrolmentFastTracked__c);
		Test.stopTest();
	}

	static testMethod void processEnrolmentFastTrackingTest1() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		serviceCase.Primary_LLN_Support_Strategies__c = '3 - Accelerate';
		update serviceCase;

		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c llnER = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'LLN - Closed'
			);
		insert llnER;

		EntryRequirement__c llnERResit = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'LLN - Resit'
			);
		insert llnERResit;

		Test.startTest();
			serviceCase = [SELECT Id, IsEnrolmentFastTracked__c, LLNResitCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(serviceCase.LLNResitCount__c == 1);
			System.assert(!serviceCase.IsEnrolmentFastTracked__c);

			llnERResit.Literacy_Level__c = 'English ACSF L2';
			update llnERResit;

			serviceCase = [SELECT Id, OwnerId, IsEnrolmentFastTracked__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(!serviceCase.IsEnrolmentFastTracked__c);
		Test.stopTest();
	}

	static testMethod void processEnrolmentFastTrackingTest2() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		serviceCase.Primary_LLN_Support_Strategies__c = '3 - Accelerate';
		update serviceCase;

		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c llnER = new EntryRequirement__c(
				RecordTypeId = llnRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id
			);
		insert llnER;

		Test.startTest();
			serviceCase = [SELECT Id, IsEnrolmentFastTracked__c, LLNResitCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(serviceCase.LLNResitCount__c == 0);
			System.assert(!serviceCase.IsEnrolmentFastTracked__c);

			llnER.Literacy_Level__c = 'English ACSF L2';
			update llnER;

			serviceCase = [SELECT Id, IsEnrolmentFastTracked__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assert(!serviceCase.IsEnrolmentFastTracked__c);
		Test.stopTest();
	}

	static testMethod void updateServiceCaseResitCountTest() {
		Service_Cases__c serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		System.assertEquals(0, serviceCase.LLNResitCount__c);

		Test.startTest();
			EntryRequirement__c llnER = new EntryRequirement__c(
					RecordTypeId = llnRTID,
					ApplicationFormId__c = appForm.Id,
					ServiceCaseId__c = serviceCase.Id,
					Status__c = 'LLN - Resit'
				);
			insert llnER;

			serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals(1, serviceCase.LLNResitCount__c);
		Test.stopTest();
	}

	static testMethod void updateServiceCaseResitCountTest1() {
		Service_Cases__c serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c];
		serviceCase.LLNResitCount__c = null;
		update serviceCase;
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		System.assertEquals(null, serviceCase.LLNResitCount__c);

		Test.startTest();
			EntryRequirement__c llnER = new EntryRequirement__c(
					RecordTypeId = llnRTID,
					ApplicationFormId__c = appForm.Id,
					ServiceCaseId__c = serviceCase.Id,
					Status__c = 'LLN - Resit'
				);
			insert llnER;

			serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals(1, serviceCase.LLNResitCount__c);
		Test.stopTest();
	}

	static testMethod void updateServiceCaseResitCountMultipleTest() {
		Service_Cases__c serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		System.assertEquals(0, serviceCase.LLNResitCount__c);

		Test.startTest();
			EntryRequirement__c llnER1 = new EntryRequirement__c(
					RecordTypeId = llnRTID,
					ApplicationFormId__c = appForm.Id,
					ServiceCaseId__c = serviceCase.Id,
					Status__c = 'LLN - Resit'
				);
			insert llnER1;

			EntryRequirement__c llnER2 = new EntryRequirement__c(
					RecordTypeId = llnRTID,
					ApplicationFormId__c = appForm.Id,
					ServiceCaseId__c = serviceCase.Id,
					Status__c = 'LLN - Resit'
				);
			insert llnER2;

			serviceCase = [SELECT Id, LLNResitCount__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
			System.assertEquals(2, serviceCase.LLNResitCount__c);
		Test.stopTest();
	}

	static testMethod void testLowestERStatus() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c awaitUploadER = new EntryRequirement__c(
				RecordTypeId = c3gRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'Awaiting Upload'
			);
		insert awaitUploadER;

		serviceCase = [SELECT LowestERStatus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
		System.assertEquals('Awaiting Upload', serviceCase.LowestERStatus__c);

		EntryRequirement__c confirmedER = new EntryRequirement__c(
				RecordTypeId = c3gRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'Confirmed'
			);

		insert confirmedER;

		serviceCase = [SELECT LowestERStatus__c FROM Service_Cases__c WHERE Id = :serviceCase.Id];
		System.assertEquals('Awaiting Upload', serviceCase.LowestERStatus__c);
	}


	static testMethod void createMilestonesTest() {
		Service_Cases__c serviceCase = [SELECT Id FROM Service_Cases__c];
		ApplicationForm__c appForm = [SELECT Id FROM ApplicationForm__c];

		EntryRequirement__c c3gER = new EntryRequirement__c(
				RecordTypeId = c3gRTID,
				ApplicationFormId__c = appForm.Id,
				ServiceCaseId__c = serviceCase.Id,
				Status__c = 'Awaiting Upload'
			);
		insert c3gER;

		Test.startTest();
			c3gER = [SELECT Id, (SELECT Id FROM EnrolmentApplicationMilestones__r) FROM EntryRequirement__c WHERE Id = :c3gER.Id];
			System.assert(c3gER.EnrolmentApplicationMilestones__r.isEmpty());

			c3gER.Status__c = 'Confirmed';
			update c3gER;

			c3gER = [SELECT Id, (SELECT Id FROM EnrolmentApplicationMilestones__r) FROM EntryRequirement__c WHERE Id = :c3gER.Id];
			System.assert(!c3gER.EnrolmentApplicationMilestones__r.isEmpty());
		Test.stopTest();
	}
}