/*This class is used to display Nat files
 *information in VF
 *This class has been moved out from Nat Controller class
 *to reduce the size of NAT Controller and to use
 *in other classes
 *Created by YG - CloudSherpas 25/11/2013
 */

public with sharing class NatViewer {

	 	   public String NatName {get;set;}
           public Integer NatCount {get;set;}
           public String NatStatus {get;set;}
           public Integer NatWarning {get;set;}
           public Integer NatError {get;set;}
           public Integer NATCPUTime {get;set;}
           
           public NatViewer(String name, Integer countt, String status, Integer warnings, Integer errors, Integer cpuTime)
           {
              NatName = name;
              NatCount = countt;
              NatStatus = status;
              NatWarning = warnings;
              NatError = errors;   
              NATCPUTime = cpuTime;       
                        
           }
           
             

}