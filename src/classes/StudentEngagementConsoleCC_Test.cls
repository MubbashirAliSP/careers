/**
 * @description Test class for `StudentEngagementConsoleCC`
 * @author Ranyel Maliwanag
 * @date 12.FEB.2016
 */
@isTest
public with sharing class StudentEngagementConsoleCC_Test {
	@testSetup
	static void setup() {
		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		enrolment.Assigned_Trainer__c = UserInfo.getUserId();
		insert enrolment;
	}

	@testSetup
	static void setupUser() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User testUser = SASTestUtilities.createUser();
		testUser.ProfileId = p.id;
		testUser.Email = 'olympia@careersaustralia.edu.au.test';
		testUser.Username = 'olympia@careersaustralia.edu.au.test';
		insert testUser;
	}

	static testMethod void generalConstructorTest() {
		Test.setCurrentPage(Page.StudentEngagementConsole);
		Test.startTest();
			StudentEngagementConsoleCC controller = new StudentEngagementConsoleCC();

			System.assert(String.isBlank(controller.studentId));
			System.assert(!controller.isStudentViewMode);

			System.assert(controller.userMap != null);

			System.assertEquals(controller.currentTrainer, UserInfo.getUserId());
			System.assertEquals(controller.currentTrainerName, UserInfo.getName());
			System.assert(controller.trainerOptions != null);

			controller.switchTabs();
		Test.stopTest();
	}

	static testMethod void currentTabParamTest() {
		Test.setCurrentPage(Page.StudentEngagementConsole);
		ApexPages.currentPage().getParameters().put('tab', 'Home');
		Test.startTest();
			StudentEngagementConsoleCC controller = new StudentEngagementConsoleCC();

			System.assertEquals(controller.currentTabName, 'Home');
		Test.stopTest();
	}

	static testMethod void invalidCurrentTabParamTest() {
		Test.setCurrentPage(Page.StudentEngagementConsole);
		ApexPages.currentPage().getParameters().put('tab', 'Homez');
		Test.startTest();
			StudentEngagementConsoleCC controller = new StudentEngagementConsoleCC();

			System.assertEquals(controller.currentTabName, 'Home');
		Test.stopTest();
	}

	static testMethod void eidParamTest() {
		Enrolment__c enrolment = [SELECT Id FROM Enrolment__c LIMIT 1];
		Test.setCurrentPage(Page.StudentEngagementConsole);
		ApexPages.currentPage().getParameters().put('eid', enrolment.Id);
		Test.startTest();
			StudentEngagementConsoleCC controller = new StudentEngagementConsoleCC();

			System.assertEquals(controller.studentId, enrolment.Id);
			controller.switchTabs();
		Test.stopTest();
	}

	static testMethod void viewAsParamTest() {
		User testUser = [SELECT Id FROM User WHERE Username = 'olympia@careersaustralia.edu.au.test'];
		Test.setCurrentPage(Page.StudentEngagementConsole);
		ApexPages.currentPage().getParameters().put('viewAs', testUser.Id);
		Test.startTest();
			StudentEngagementConsoleCC controller = new StudentEngagementConsoleCC();

			System.assertEquals(controller.viewAs, testUser.Id);
			System.assertNotEquals(controller.currentTrainerName, UserInfo.getName());
			controller.switchTabs();
		Test.stopTest();
	}
}