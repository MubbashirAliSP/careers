/**
 * @description Test class for `EnrolmentHistoryScheduler` class
 * @author Ranyel Maliwanag
 * @date 22.SEP.2015
 */
@isTest
public with sharing class EnrolmentHistoryScheduler_Test {
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Enrolment History Schedule Test', schedule, new EnrolmentHistoryScheduler(200, Date.today()));
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}