@isTest
private class LocationsTests {
	
	@isTest static void addressFieldsCopy() {
		
		Location_Loadings__c loading = new Location_Loadings__c(); 
		State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();

		Test.startTest();

		 country = TestCoverageUtilityClass.createCountry();  
         state = TestCoverageUtilityClass.createState(country.Id);
         loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
		 parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);

		 parentLoc.Address_building_property_name__c = 'Property Name';
		 parentLoc.Address_flat_unit_details__c = 'Flat Details';
		 parentLoc.Address_street_number__c = '1';
		 parentLoc.Address_street_name__c = 'Street Name';

		 update parentLoc;

		 Locations__c newParentLoc = [Select Address_Location__c From Locations__c Where id = : parentLoc.Id];

		 System.AssertEquals(newparentLoc.Address_Location__c,'Property Name, Flat Details, 1 Street Name');

		 newParentLoc.Address_building_property_name__c = null;
		 newParentLoc.Address_flat_unit_details__c = null;
		 newParentLoc.Address_street_number__c = null;
		 newParentLoc.Address_street_name__c = null;

		 update newParentLoc;

		Test.stopTest();
	}
	
	
	
}