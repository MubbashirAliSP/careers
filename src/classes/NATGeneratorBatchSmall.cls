/**
 * @description Batch Class for NAT00010 NAT00020 and NAT00030
 * @author 
 * @date 31.JUL.2013
 *
 * HISTORY
 * -31.JUL.2013    Created.
 * -24.SEP.2013    Michelle Magsarili restructure code to perform multiple batch process for different files to prevent duplicate processing
 * -24.OCT.2013    Michelle Maagarili resturcture code to perform a separate run for NAT10 and a different execution for NAT20 and NAT30 to avoid Apex Heaps Limits
 */
global class NATGeneratorBatchSmall implements Database.Batchable<sObject>,Database.Stateful {
    
    global final String query;
    global final String functionquery;
    private String currentState;
    private String libraryId;
    private String tOrgId;
    global String NATFile;
    global String body10;
    global String body20;
    global String body30;
    
    global integer natFileNumber = 0;
    global integer natFileName = 0;
    
    //MAM 09/24/2013 start separating the batch jobs according to NAT file names
    global NATGeneratorBatchSmall(String NATFileName, String q, String state, String tId) {
        functionquery = q;
        currentState = state;
        
        tOrgId = tId;
        
        if(state == 'Queensland')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files QLD' LIMIT 1].Id;
            
        if(state == 'Victoria')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files VIC' LIMIT 1].Id;
        
        if(state == 'New South Wales')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files NSW' LIMIT 1].Id;
        
        if(state == 'South Australia')
            libraryId = [Select c.Name, c.Id From ContentWorkspace c WHERE c.Name = 'NAT Files SA' LIMIT 1].Id;
        
        
        /*We will delete all files in content folder for the state */
        
        //List<ContentDocument> existingContent = [Select Id From ContentDocument WHERE parentId = : libraryId];
        
        NATFile = NATFileName.toUpperCase();
        
        if(NATFile.equals('NAT00010')){
            //query for Training Organization - NAT00010 LIMIT to 1 since there's only 1 record to be retrieved
            query = 'SELECT Id,AVETMISS_Organisation_Name__c, Training_Org_Identifier__c, Training_Org_Type_Identifier__c, BillingStreet, National_Provider_Number__c, ' + 
                    'BillingCity, BillingPostalCode, BillingState, State_Identifier__c, NAT_Error_Code__c, Name, Software_Product_Name__c,Software_Vendor_Email_Address__c ' + 
                    'FROM Account WHERE id =\'' + tOrgId + '\' LIMIT 1';
        }
        else{ //NAT00020 NAT00030
            query = q;
        }
    }
    //MAM 09/24/2013
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        body10 = '';
        body20 = '';
        body30 = '';
        System.debug('##query '+ query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
        //MAM 09/24/2013 file reconstruction start 
        if(NATFile.equals('NAT00010')){
        
            Account TrainingOrg;
            
            for(SObject s :scope) {
                TrainingOrg = (Account) s;
            }
            
            Contact trainingOrgContact = [Select c.Phone,
                                                 c.FirstName,
                                                 c.LastName,
                                                 c.MailingStreet,
                                                 c.MailingState, 
                                                 c.MailingPostalCode,
                                                 c.MailingCountry,
                                                 c.MailingCity,
                                                 c.Id,
                                                 c.Fax,
                                                 c.Email
                                         From Contact c
                                         Where accountid = : tOrgId LIMIT 1];
             
             //Add Training Organisation Identifier
             if (currentState == 'Victoria') {
                body10 += NATGeneratorUtility.formatHours(trainingOrg.National_Provider_Number__c, NAT00010__c.getInstance('01').Length__c);
             } 
             else {
                body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.Training_Org_Identifier__c, NAT00010__c.getInstance('01').Length__c,'');
             }
              
             body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.AVETMISS_Organisation_Name__c, NAT00010__c.getInstance('02').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.Training_Org_Type_Identifier__c, NAT00010__c.getInstance('03').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.BillingStreet, NAT00010__c.getInstance('04').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile('', NAT00010__c.getInstance('05').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.BillingCity, NAT00010__c.getInstance('06').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.BillingPostalCode, NAT00010__c.getInstance('07').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.State_Identifier__c, NAT00010__c.getInstance('08').Length__c,'');
                     
                     if(currentState == 'Victoria') {
                         body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrgContact.LastName + ', ' + trainingOrgContact.FirstName, NAT00010__c.getInstance('09').Length__c,'');
                     }
                     
                     else {
                         body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrgContact.FirstName + ', ' + trainingOrgContact.LastName, NAT00010__c.getInstance('09').Length__c,'');
                     }
                     
                     body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrgContact.Phone, NAT00010__c.getInstance('10').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrgContact.Fax, NAT00010__c.getInstance('11').Length__c,'') +
                     NATGeneratorUtility.formatOrPadTxtFile(trainingOrgContact.Email, NAT00010__c.getInstance('12').Length__c,'');
                
              if (currentState == 'Victoria') {
                    body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.Software_Product_Name__c, NAT00010__c.getInstance('13').Length__c,'');
                    body10 += NATGeneratorUtility.formatOrPadTxtFile(trainingOrg.Software_Vendor_Email_Address__c, NAT00010__c.getInstance('14').Length__c,'');
              }

                    
                
            body10 += '\r\n';
        }
        //MAM 10/24/2013 start
        else{ //NAT00020 NAT00030
            Set<String>qualificationIds = new Set<String>();
            Set<String>locationIds = new Set<String>();
            Map<Id,Decimal>Unit_NominalHoursMap = new Map<Id,Decimal>();
            
            for(SObject s :scope) {
             
                 Enrolment_Unit__c eu = (Enrolment_Unit__c) s;
                 locationIds.add(eu.Training_Delivery_Location__c);
                 Unit_NominalHoursMap.put(eu.Enrolment__r.Qualification__r.Parent_Qualification_Id__c, eu.Enrolment__r.qualification_hours__c);
                
            }
            
            for (Locations__c locs : [Select Id,
                                     Delivery_Location_Identifier__c,
                                     Training_Organisation__r.Training_Org_Identifier__c,
                                     Training_Organisation__r.National_Provider_Number__c,
                                     Training_Organisation__r.Name,
                                     Name,
                                     Postcode__c,
                                     Address_Location__c,
                                     Child_Postcode__c,
                                     Child_Suburb__c,
                                     Country_Identifier__c,
                                     State_Identifier__c,
                                     Child_Country__c,
                                     NAT_Error_Code__c,
                                     Country__r.Country_Code__c,
                                     Child_Address_building_property_name__c,
                                     Child_Address_flat_unit_details__c,
                                     Child_Address_street_number__c,
                                     Child_Address_street_name__c
                                  FROM Locations__c
                                  WHERE  id in : locationIds]) {
                
                //Add Training Organisation Identifier
                
                if (currentState == 'Victoria') {
                    body20 += NATGeneratorUtility.formatHours(locs.Training_Organisation__r.National_Provider_Number__c, NAT00020__c.getInstance('01').Length__c);
                } 
                
                else {
                    body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Training_Organisation__r.Training_Org_Identifier__c, NAT00020__c.getInstance('01').Length__c,'');
                }
                
               body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Delivery_Location_Identifier__c, NAT00020__c.getInstance('02').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(locs.Name, NAT00020__c.getInstance('03').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Postcode__c, NAT00020__c.getInstance('04').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(locs.State_Identifier__c, NAT00020__c.getInstance('05').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Suburb__c, NAT00020__c.getInstance('06').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(locs.Country_Identifier__c, NAT00020__c.getInstance('07').Length__c,'');

                         if (currentState == 'Victoria') {
                                body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Address_building_property_name__c, NAT00020__c.getInstance('08').Length__c,'');
                                body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Address_flat_unit_details__c, NAT00020__c.getInstance('09').Length__c,'');
                                body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Address_street_number__c, NAT00020__c.getInstance('10').Length__c,'');
                                body20 += NATGeneratorUtility.formatOrPadTxtFile(locs.Child_Address_street_name__c, NAT00020__c.getInstance('11').Length__c,'');
                         }
                
                 body20 += '\r\n';
             
      
            }
            
            Decimal qHours = 0.0;
            
            for (Qualification__c ql : [SELECT id,
                                                 Name,
                                                 Qualification_Name__c,
                                                 Recognition_Status_Identifier__c, 
                                                 Qualification_Category_Identifier__c,
                                                 Field_Of_Education_Identifier__c, 
                                                 ANZSCO_Identifier__c,
                                                 Vet_Non_Vet__c,
                                                 NAT_Error_Code__c,
                                                 QLD_Reportable_Qualification_Hours__c,
                                                 VIC_Reportable_Qualification_Hours__c,
                                                 SA_Reportable_Qualification_Hours__c,
                                                 Parent_Qualification_Code__c,
                                                 Parent_Qualification_Name__c
                                          FROM Qualification__c WHERE Id in : Unit_NominalHoursMap.keySet()]) {
                
                if(currentState == 'Queensland')
                    qHours = ql.QLD_Reportable_Qualification_Hours__c;
                if(currentState == 'Victoria')
                    qHours = ql.VIC_Reportable_Qualification_Hours__c;
                if(currentState == 'South Australia')
                    qHours = ql.SA_Reportable_Qualification_Hours__c; 
                
                //Add Qualification/Course Identifier
                body30 += NATGeneratorUtility.formatOrPadTxtFile(ql.Parent_Qualification_Code__c, NAT00030__c.getInstance('01').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(ql.Parent_Qualification_Name__c, NAT00030__c.getInstance('02').Length__c,'')+
                        NATGeneratorUtility.formatHours(String.valueOf(qHours), NAT00030__c.getInstance('03').Length__c).substringBefore('.')+
                        NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(ql.Recognition_Status_Identifier__c), NAT00030__c.getInstance('04').Length__c,'')+
                        NATGeneratorUtility.formatOrPadTxtFile(ql.Qualification_Category_Identifier__c, NAT00030__c.getInstance('05').Length__c,'')+
                        
                        NATGeneratorUtility.formatHours(String.valueOf(ql.Field_Of_Education_Identifier__c), NAT00030__c.getInstance('03').Length__c) +
                        // NATGeneratorUtility.formatHours(String.valueOf(ql.Field_Of_Education_Identifier__c), NAT00030__c.getInstance('06').Length__c,'')+
                        
                        NATGeneratorUtility.formatOrPadTxtFile(String.valueOf(ql.ANZSCO_Identifier__c), NAT00030__c.getInstance('07').Length__c,'');
                if (ql.Vet_Non_Vet__c) {
                    body30 += NATGeneratorUtility.formatOrPadTxtFile('Y', NAT00030__c.getInstance('08').Length__c,'') + '\r\n';
                } else {
                    body30 += NATGeneratorUtility.formatOrPadTxtFile('N', NAT00030__c.getInstance('08').Length__c,'') + '\r\n';
                }
                
            }
            
        }
        
    	/*
        natFileNumber++;

        if(currentState == 'Queensland') {
            
            Integer remainder = math.mod(natFileNumber, 300);
            
            if(remainder == 0) {   
                natFileName++;
				String fBody20 = NATGeneratorUtility.RemoveDuplicates(body20);
            	Blob pBlob20 = Blob.valueof(fBody20);
            	insert new ContentVersion(
                versionData =  pBlob20,
                Title = 'NAT00020_' + natFileName,
                PathOnClient = '/NAT00020_' + natFileName + '.txt',
                FirstPublishLocationId = libraryId);
                
                body20 = '';
        
            String fBody30 = NATGeneratorUtility.RemoveDuplicates(body30);
            Blob pBlob30 = Blob.valueof(fbody30);
            insert new ContentVersion(
                versionData =  pBlob30,
                Title = 'NAT00030_' + natFileName,
                PathOnClient = '/NAT00030.txt_'+ natFileName + '.txt',
                FirstPublishLocationId = libraryId);
                
                body30 = '';
        	}	
        } */
           
    }


    global void finish(Database.BatchableContext BC) {
        
        if(NATFile.equals('NAT00010')){
            Blob pBlob10 = Blob.valueof(body10);
            insert new ContentVersion(
                versionData =  pBlob10,//EncodingUtil.base64Encode(pBlob),
                Title = 'NAT00010',
                PathOnClient = '/NAT00010.txt',
                FirstPublishLocationId = libraryId);
                
            //Execute batch again for NAT00020 and NAT00030
            Database.executeBatch(new NATGeneratorBatchSmall('NAT00020',functionquery,currentState, tOrgId), 2000);//NAT20 and NAT30
         }
        /* else {
            if(currentState == 'Queensland') {
            
            if(body20 != '') {
                natFileName++;
                String fBody20 = NATGeneratorUtility.RemoveDuplicates(body20);
            	Blob pBlob20 = Blob.valueof(fBody20);
            	insert new ContentVersion(
                versionData =  pBlob20,
                Title = 'NAT00020_' + natFileName,
                PathOnClient = '/NAT00020_'+ natFileName + '.txt',
                FirstPublishLocationId = libraryId);
        
            String fBody30 = NATGeneratorUtility.RemoveDuplicates(body30);
            Blob pBlob30 = Blob.valueof(fbody30);
            insert new ContentVersion(
                versionData =  pBlob30,
                Title = 'NAT00030_' + natFileName,
                PathOnClient = '/NAT00030.txt_'+ natFileName + '.txt',
                FirstPublishLocationId = libraryId);
               
           	}   
            
            String batchFileS20 = 'TYPE NAT00020_*.txt >> NAT00020.txt' + '\r\n' + 'DEL NAT00020_*.txt' + '\r\n' + 'DEL combine20.bat';
            Blob batchFile20 = Blob.valueof(batchFileS20);
            
       	 	insert new ContentVersion(
                versionData =  batchFile20,
                Title = 'combine20',
                PathOnClient = '/combine20.bat',
                FirstPublishLocationId = libraryId);
                
            String batchFileS30 = 'TYPE NAT00030_*.txt >> NAT00030.txt' + '\r\n' + 'DEL NAT00030_*.txt' + '\r\n' + 'DEL combine30.bat';
            Blob batchFile30 = Blob.valueof(batchFileS30);
            
       	 	insert new ContentVersion(
                versionData =  batchFile30,
                Title = 'combine30',
                PathOnClient = '/combine30.bat',
                FirstPublishLocationId = libraryId);
            
        }
            
            
            
            
            
            
        } */
        
           
        else{
            //Do parsing at the end of the process
            String fBody20 = NATGeneratorUtility.RemoveDuplicates(body20);
            Blob pBlob20 = Blob.valueof(fBody20);
            insert new ContentVersion(
                versionData =  pBlob20,
                Title = 'NAT00020',
                PathOnClient = '/NAT00020.txt',
                FirstPublishLocationId = libraryId);
        
            String fBody30 = NATGeneratorUtility.RemoveDuplicates(body30);
            Blob pBlob30 = Blob.valueof(fbody30);
            insert new ContentVersion(
                versionData =  pBlob30,
                Title = 'NAT00030',
                PathOnClient = '/NAT00030.txt',
                FirstPublishLocationId = libraryId);
            
        }
    }
}