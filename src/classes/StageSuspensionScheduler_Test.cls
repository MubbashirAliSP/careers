/**
 * @description Test class for `StageSuspensionScheduler` class
 * @author Ranyel Maliwanag
 * @date 18.JAN.2016
 */
@isTest
public with sharing class StageSuspensionScheduler_Test {
	static testMethod void testScheduling() {
		Test.startTest();
			String schedule = '0 0 0 * * ?';
			String jobId = System.schedule('Stage Suspension Batch Scheduler Test', schedule, new StageSuspensionScheduler());
			
			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
			
			System.assertNotEquals(null, ct);
			System.assertEquals(schedule, ct.CronExpression);
			System.assertEquals(0, ct.TimesTriggered);
		Test.stopTest();
	}
}