/**
 * @description Test class for `AwardUnitsGenerationCX` class
 * @author Ranyel Maliwanag
 * @date 04.JAN.2016
 */
@isTest
public with sharing class AwardUnitsGenerationCX_Test {
	@testSetup
	static void setup() {
		Qualification__c qualification = SASTestUtilities.createQualification(null, null);
		insert qualification;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(qualification);
		enrolment.Qualification__c = qualification.Id;
		insert enrolment;

		System.debug('qual ::: ' + enrolment.Qualification__c);

		Awards__c award = new Awards__c();
		award.Enrolment__c = enrolment.Id;
		insert award;
	}

	static testMethod void constructorTest() {
		Awards__c award = [SELECT Id FROM Awards__c];

		Test.startTest();
			Test.setCurrentPage(Page.AwardUnitsGeneration);
			ApexPages.StandardController stdController = new ApexPages.StandardController(award);
			AwardUnitsGenerationCX controller = new AwardUnitsGenerationCX(stdController);

			controller.evaluateRedirects();
		Test.stopTest();
	}
}