/**
 * @description Funding Source Trigger Handler 
 * @author Janella Lauren Canlas
 * @date 26.NOV.2012
 *
 * HISTORY
 * - 26.NOV.2012  Janella Canlas - Created.
 * HISTORY
 * - 28.JUN.2013  Eu Cadag - code optimization to isolate the error: Too many code statements: 200001  .
 */
public with sharing class FundingSourceTriggerHandler {
  /*
    This will update the related Line Item Qualification Units Nominal Hours and Points
  */
  @future 
  public static void LineItemQualification(Set<Id> cliIds){
    List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();
    List<Contract_Line_Items__c> cliList = new List<Contract_Line_Items__c>();
    List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
    List<Line_Item_Qualification_Units__c> liquUpdate = new List<Line_Item_Qualification_Units__c>();
    Map<Id, List<Qualification_Unit__c>> qualMap = new Map<Id, List<Qualification_Unit__c>>();
    Map<Id, String> stateMap = new Map<Id, String>();
    Map<Id, List<Line_Item_Qualification_Units__c>> liquMap = new Map<Id, List<Line_Item_Qualification_Units__c>>();
    Map<Id, List<Unit_Hours_and_Points__c>> unitMap = new Map<Id, List<Unit_Hours_and_Points__c>>();
    //query related Line Item Qualification Unit records
    liquList = [Select Unit_Name__c, Unit_Cost__c, Unit_Code__c, RecordTypeId, Qualification_Unit__c, Points__c, Nominal_Hours__c, 
          Name, Line_Item_Qualification__c, Id, Contract_State__c, Contract_Code__c, Line_Item_Qualification__r.Contract_Line_Item__c,
          Line_Item_Qualification__r.Qualification__c
          From Line_Item_Qualification_Units__c    
          Where Line_Item_Qualification__r.Contract_Line_Item__c IN: cliIds ];
    //create map of Line Item Qualification Id and corresponding Contract State      
    for(Line_Item_Qualification_Units__c l : liquList){
      for(Contract_Line_Items__c c : cliList){
        if(l.Line_Item_Qualification__r.Contract_Line_Item__c == c.Id){
          if(!stateMap.containsKey(l.Line_Item_Qualification__c)){
            stateMap.put(l.Line_Item_Qualification__c,c.Purchasing_Contract__r.Contract_State__c);
          }
        }
      }
    }
    //create map of Unit Id and corresponding list of Line Item Qualification Units
    for(Line_Item_Qualifications__c u : liqList){
      for(Line_Item_Qualification_Units__c l : liquList){
        if(l.Line_Item_Qualification__c == u.Id){
          if(liquMap.containsKey(u.Id)){
            List<Line_Item_Qualification_Units__c> tempList = liquMap.get(u.Id);
                    tempList.add(l);
                    liquMap.put(u.Id,tempList);
          }
          else{
            liquMap.put(u.Id,new List<Line_Item_Qualification_Units__c>{l});
          }
        }
      }
    }
    system.debug('liquList.size()'+liquList.size());
    //code to update line item qualification unit Nominal Hours and Points
    for(Line_Item_Qualifications__c l : liqList){
      
      List<Qualification_Unit__c> tempQual = new List<Qualification_Unit__c>();
      List<Unit_Hours_and_Points__c> unitHours = new List<Unit_Hours_and_Points__c>();
      
      Id qId = l.Qualification__c;
      tempQual = qualMap.get(qId);
      liquUpdate = liquMap.get(l.Id);
      
      system.debug('liquUpdate***'+liquUpdate);
      system.debug('tempQual.size()'+tempQual.size());
      
      for(Qualification_Unit__c q : tempQual){
        system.debug('unitHours.size()'+unitHours.size());
        string state ='';
        if(stateMap.get(l.Id)!= null){
          state = stateMap.get(l.Id);
        }
        if(unitMap.get(q.Unit__c) != null){
          unitHours = unitMap.get(q.Unit__c);
          for(Unit_Hours_and_Points__c u : unitHours){
            for(Line_Item_Qualification_Units__c li : liquUpdate){
              //populate fields if states are equal
              if(li.Contract_State__c == u.State__r.Name){
                li.Nominal_Hours__c = u.Nominal_Hours__c;
                li.Points__c = u.Points__c;
              }
              else{
                li.Nominal_Hours__c = null;
                li.Points__c = null;
              }
            }
          }
        }
      }
    }
    // update line item qualification records  
    try{
      update liquUpdate;}catch(Exception e){system.debug('***ERROR:'+ e.getMessage());
    }
  }
  
  public static void updateLIQU(List<Funding_Source__c> fundList){
    
    Set<Id> fundIds = new Set<Id>();
    Set<Id> cliIds = new Set<Id>();
    Set<Id> liqIds = new Set<Id>();
    Set<Id> unitIds = new Set<Id>();
    Set<Id> qIds = new Set<Id>();
    Map<Id, Id> MapQIds = new Map<Id, Id>();
    
    List<Contract_Line_Items__c> cliList = new List<Contract_Line_Items__c>();
    List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();
    List<Qualification_Unit__c> quList = new List<Qualification_Unit__c>();
    List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
    List<Unit_Hours_and_Points__c> unitList = new List<Unit_Hours_and_Points__c>();
    List<Line_Item_Qualification_Units__c> liquUpdate = new List<Line_Item_Qualification_Units__c>();
    
    Map<Id, List<Unit_Hours_and_Points__c>> unitMap = new Map<Id, List<Unit_Hours_and_Points__c>>();
    Map<Id, List<Qualification_Unit__c>> qualMap = new Map<Id, List<Qualification_Unit__c>>();
    Map<Id, List<Line_Item_Qualification_Units__c>> liquMap = new Map<Id, List<Line_Item_Qualification_Units__c>>(); 
    Map<Id, String> stateMap = new Map<Id, String>(); 
    
    //collect funding source ids
    for(Funding_Source__c f : fundList){
      fundIds.add(f.Id);
    }
    //query related Contract Line Item records
    cliList = [SELECT ID, Purchasing_Contract__r.Contract_State__c FROM Contract_Line_Items__c 
          WHERE Purchasing_Contract__r.State_Fund_Source__c IN: fundIds];
    //collect related Contract Line Item ids
    for(Contract_Line_Items__c c : cliList){
      cliIds.add(c.Id);
    }
    system.debug('***cliIds'+cliIds);
    //query related Line Item Qualification records
    liqList = [Select Training_Catalogue_Item_No__c, Total_Line_Item_Qualification_Amount__c, Qualification__c, Qualification_Name__c,
          Qualification_Hours__c, Qualification_Amount_Unit__c, No_of_Participants__c, Name, Id, Delta_Qual__c,
          Contract_Line_Item__c, Contract_Code__c, Completion_Date__c, Commencement_Date__c, Active__c 
          From Line_Item_Qualifications__c 
          WHERE Contract_Line_Item__c IN: cliIds];
    //collect Line Item Qualification and Qualification Ids      
    for(Line_Item_Qualifications__c liq : liqList){
      liqIds.add(liq.Id);
      qIds.add(liq.Qualification__c);
      MapQIds.put(liq.Qualification__c, liq.Qualification__c);
    }
    system.debug('***qIds'+qIds);
    //query qualification units related
    quList = [SELECT Unit__c, Unit__r.Name, Unit_Type__c, Stage__c, Qualification__c FROM Qualification_Unit__c WHERE Qualification__c IN: qIds];
    //create map of qualification ids and list of qualification units
    /************************************ This is the previous code modified by Eu Dated 2013-06-28*********************************
    for(Id i : qIds){
      for(Qualification_Unit__c q : quList){
        if(q.Qualification__c == i){
          if(qualMap.containsKey(i)){
            List<Qualification_Unit__c> tempList = qualMap.get(i);
                      tempList.add(q);
                      qualMap.put(i,tempList);
          }
          else{
            qualMap.put(i,new List<Qualification_Unit__c>{q});
          }
        }
      }
    }
    *************************************/
    /************************************ This is the updated code by Eu Dated 2013-06-28*********************************/
      for(Qualification_Unit__c q : quList){
        if(MapQIds.containsKey(q.Qualification__c)){
          if(qualMap.containsKey(MapQIds.get(q.Qualification__c))){
            List<Qualification_Unit__c> tempList = qualMap.get(MapQIds.get(q.Qualification__c));
                      tempList.add(q);
                      qualMap.put(MapQIds.get(q.Qualification__c),tempList);
          }
          else{
            qualMap.put(MapQIds.get(q.Qualification__c),new List<Qualification_Unit__c>{q});
          }
        }
      }
    /*************************************/
    system.debug('***qualMap'+qualMap);
    //collect unit ids related
    for(Qualification_Unit__c q : quList){
      unitIds.add(q.Unit__c);
    }
    system.debug('***unitIds'+unitIds);
    //query related Unit Hours and Points
    unitList = [SELECT Nominal_Hours__c, Points__c, State__c,State__r.Name,Unit__c FROM Unit_Hours_and_Points__c WHERE Unit__c IN: unitIds];
    system.debug('***unitList'+unitIds);
    //create map of Unit Id and List of Unit Hours and Points records
    for(Unit_Hours_and_Points__c u : unitList){
      if(unitMap.containsKey(u.Unit__c)){
        List<Unit_Hours_and_Points__c> tempList = unitMap.get(u.Unit__c);
                tempList.add(u);
                unitMap.put(u.Unit__c,tempList);
                system.debug('u.State__r.Name'+u.State__r.Name);
      }
      else{
        unitMap.put(u.Unit__c,new List<Unit_Hours_and_Points__c>{u});
      }
      
    }
    LineItemQualification(cliIds);
  }
}