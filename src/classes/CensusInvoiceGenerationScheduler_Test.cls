/**
 * @description Test class for `CensusInvoiceGenerationScheduler` class
 * @author Ranyel Maliwanag
 * @date 16.NOV.2015
 */
@istest
public with sharing class CensusInvoiceGenerationScheduler_Test {
    @testSetup
    static void batchSetup() {
        ApplicationsPortalCentralSettings__c portalSettings = SASTestUtilities.createApplicationsPortalSettings();
        insert portalSettings;

        Census__c census = SASTestUtilities.createCensus();
        insert census;
    }
    
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Census Invoice Generation Test', schedule, new CensusInvoiceGenerationScheduler());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}