/**
 * @description Schedulable interface implementation for the ProgressionSuspension Batch
 * @author Biao Zhang
 * @date 25.NOV.2015
 */
public without sharing class ProgressionSuspensionBatchScheduler implements Schedulable {
	public ProgressionSuspensionBatch batchProcess;

    public ProgressionSuspensionBatchScheduler() {
        batchProcess = new ProgressionSuspensionBatch(null);
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(batchProcess);
    }
}