/**
 * @description Test class for `UploadEntryRequirementsCC` class
 * @author Biao Zhang
 * @date 07.MAR.2016
 */
@isTest
private class UploadEntryRequirementsCC_Test {
	static Id llnERRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('LLN').getRecordTypeId();
	static Id formSCRTID = Schema.SObjectType.Service_Cases__c.getRecordTypeInfosByName().get('Form').getRecordTypeId();
	static Id c3gRTID = Schema.SObjectType.EntryRequirement__c.getRecordTypeInfosByName().get('C3G').getRecordTypeId();

	@testSetup
	static void setup() {
		ApplicationsTestUtilities.initialiseCountryAndStates();

		Account student = SASTestUtilities.createStudentAccount();
		insert student;

		GuestLogin__c studentLogin = ApplicationsTestUtilities.createGuestLogin();
        studentLogin.AddressState__c = 'QLD';
        studentLogin.AccountId__c = student.Id;
        insert studentLogin;

        State__c stateQLD = [SELECT Id FROM State__c WHERE Short_Name__c = 'QLD'];
        Qualification__c testQual = new Qualification__c(
                Name = 'HLD',
                Qualification_Name__c = 'HLD 101'
            );
        insert testQual;

		Enrolment__c enrolment = SASTestUtilities.createEnrolment(student);
		insert enrolment;

		Service_Cases__c serviceCase = SASTestUtilities.createServiceCase();
		serviceCase.Enrolment__c = enrolment.Id;
		serviceCase.Account_Student_Name__c = student.Id;
		serviceCase.RecordTypeId = formSCRTID;
		insert serviceCase;

		ApplicationForm__c appForm = new ApplicationForm__c(
                StudentCredentialsId__c = studentLogin.Id,
                StateId__c = stateQLD.Id,
                QualificationId__c = testQual.Id,
                RecordTypeId = Schema.SObjectType.ApplicationForm__c.getRecordTypeInfosByName().get('Enrolment').getRecordTypeId()
            );
        insert appForm;
 }

	static testMethod void test() {
		ApplicationForm__c form = [Select Id from ApplicationForm__c];

        Test.setCurrentPage(Page.UploadEntryRequirements);
        ApexPages.currentPage().getParameters().put('formId', form.Id);

        Test.startTest();
            UploadEntryRequirementsCC controller = new UploadEntryRequirementsCC();
            controller.newEntryRequirement.Category__c = 'Date of Birth';
            controller.newEntryRequirement.Type__c = 'Drivers License';
            controller.document.Name = 'test';
	        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
	        controller.document.body = bodyBlob;
            controller.upload();
        Test.stopTest();

	}
	
}