global class USI_BatchCreate implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts {

    global String query;    
    global List<USI_Integration_Event__c> eventList = new List<USI_Integration_Event__c>();
    global USI_Integration_Log__c usiLog = USI_LogManager.createUSILog();
    global List<Account> accountList = new List<Account>();
    global Map<Id, Enrolment__c> studentEnrolmentMap = new Map<Id, Enrolment__c>();

    global class usiCreateResponse {
        String USI;
        String message;
        String status;
    }
    
    public class internalServerException extends Exception {
        //just for throwing exception
    }

    global USI_BatchCreate(Map<Id, Enrolment__c> stuEnrlMap ) {
        
        studentEnrolmentMap = stuEnrlMap;
        
        query = 'SELECT ID, ' +
            'Acquisition_Date__c, ' +
            'Card_Colour__c, ' +
            'Certificate_Number_Encrypted__c, ' +
            'Country_Of_Issue__c, ' +
            'Date_Printed__c, ' +
            'Document_Number_Encrypted__c, ' +
            'Expiry_Date_Encrypted__c, ' +
            'Immi_Card_Number_Encrypted__c, ' +
            'Individual_Reference_Number_Encrypted__c, ' +
            'Licence_Number_Encrypted__c, ' +
            'Medicare_Card_Number_Encrypted__c, ' +
            'Name_Line_1_Encrypted__c, ' +
            'Name_Line_2_Encrypted__c, ' +
            'Name_Line_3_Encrypted__c, ' +
            'Name_Line_4_Encrypted__c, ' +
            'Passport_Number_Encrypted__c, ' +
            'Registration_Date__c, ' +
            'Registration_Number_Encrypted__c, ' +
            'Registration_State__c, ' +
            'Registration_Year__c, ' +
            'State__c, ' +
            'Stock_Number_Encrypted__c, ' +
            'Account__c, ' +
            'Account__r.FirstName, ' +
            'RecordType.Name, ' +
            'Account__r.LastName, ' +
            'Account__r.PersonEmail, ' +
            'Account__r.PersonBirthDate, ' +
            'Account__r.Sex_Identifier__c, ' +
            'Account__r.Country_of_Birth__r.Name, ' +
            'Account__r.Country_Studying_In__r.Name, ' +
            'Account__r.Preferred_Contact_Method__c, ' +
            'Account__r.Address_Country__r.Name, ' +
            'Account__r.Address_building_property_name__c, ' +
            'Account__r.Address_street_number__c, ' +
            'Account__r.Address_street_name__c, ' +
            'Account__r.Address_Post_Code__c, ' +
            'Account__r.Suburb_locality_or_town__c, ' +
            'Account__r.Reporting_Billing_State__r.Name, ' +
            'Account__r.Town_City_Of_Birth__c, ' +
            'Account__r.PersonHomePhone, ' +
            'Account__r.PersonMobilePhone, ' +
            'Account__r.Student_Identifer__c, ' +
            'Account__r.Training_Org_Identifier__c ' +
            'FROM USI_Identification_Document__c ' +
            'WHERE (Account__r.Require_USI_Creation__c = true OR Account__r.ReSync__c = true) AND Account__r.Validated_by_webservice__c = false AND Account__r.manually_verified__c = false AND ' +
            '(Account__r.Recordtype.DeveloperName = \'PersonAccount\' AND Account__r.Recordtype.SObjectType = \'Account\')';
            
        
    }

    global database.querylocator start(Database.BatchableContext BC) {
         
        return Database.getQueryLocator(query);
    
    }
    
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
           
       for (SObject s :scope) {
           USI_Identification_Document__c document = (USI_Identification_Document__c) s;

           String sentBody;
        
           USI_Integration_Event__c event = new USI_Integration_Event__c();
           HTTPresponse createUSIres = new HTTPResponse();
           Enrolment__c enrl = studentEnrolmentMap.get(document.Account__c);
            
           try {
               sentBody = JSON.serializePretty(document);
               event.JSON__c = sentBody;
               createUSIres =  USI_WebService.createUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
               usiCreateResponse uro;
               if(createUSIres.getStatusCode() == 200 ) {
                   uro = (usiCreateResponse) JSON.deserialize(createUSIres.getBody(), usiCreateResponse.class); 
               }
               
               if(createUSIres.getStatusCode() == 500 ) {
                   throw new internalServerException('Internal Server Error');
               }
               
                                
               //only retry with this error message
               if (uro.message == 'Internal USI Error Occured, Please Check Heroku logs for more details' ) {
                   event = USI_LogManager.errorHandle(uro.message, document.Account__c, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, usiLog.Id, true);
                   eventList.add(event);
                   
                   createUSIres =  USI_WebService.createUSI(sentBody, enrl.Delivery_Location__r.Training_Organisation__r.Training_Org_Identifier__c);
                   uro = (usiCreateResponse) JSON.deserialize(createUSIres.getBody(), usiCreateResponse.class); 
               }
               
               if(uro.USI.length() == 10 && !uro.Status.equalsIgnoreCase('failure') ) {
                   try {
                       accountList.add(USI_UtilityClass.setUSIBatch(uro.USI, document.Account__c));
                       event = USI_LogManager.createUSIEvent(usiLog.Id, document.Account__c, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, uro.message, true);
                       USI_UtilityClass.closeServiceCase(document.Account__c);
                   }
                   catch (exception e) {
                       event = USI_LogManager.errorHandle(e.getMessage(), document.Account__c, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, usiLog.Id, true);
                   }
               }
               else {
                   event = USI_LogManager.errorHandle(uro.message, document.Account__c, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, usiLog.Id, true);
               }
           }
           
           catch (exception e) {
              event = USI_LogManager.errorHandle(e.getMessage(), document.Account__c, document.Account__r.FirstName + ' ' + document.Account__r.LastName, document.Account__r.Student_Identifer__c, usiLog.Id, true);
           }
           
           event.Received_JSON__c = createUSIRes.getBody();
           
           eventList.add(event);
       }
    }
        
    global void finish(Database.BatchableContext BC) {
       
        update accountList;
        insert eventList;
        
    }
}