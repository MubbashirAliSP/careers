/*------------------------------------------------------------
Author:        Jade Serrano
Company:       Cloud Sherpas
Description:   Test Class for EnrolmentUnitList_CC
Test Class:    
History
21 MAY 2013     Jade Serrano    Created 
------------------------------------------------------------*/
@isTest
public class EnrolmentUnitList_CC_Test{
    
    static testMethod void EnrolmentUnitList_CC1() {
        UserRole ur = [Select Id from UserRole Where Name = 'VFH Data Management' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='EUOS_User', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = ur.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Language__c language = new Language__c();
        Enrolment_Intake_Unit__c enrolmentIntakeUnit = new Enrolment_Intake_Unit__c();
        List<Enrolment_Intake_Unit__c> eiuList = new List<Enrolment_Intake_Unit__c>();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Credit_status_Higher_Ed_provider_code__c creditStatus = new Credit_status_Higher_Ed_provider_code__c();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Funding_Stream_Types__c fundStream = new Funding_Stream_Types__c();
        Program__c program = new Program__c();
        Results__c result = new Results__c();
        Enrolment_Unit__c enrolmentUnit = new Enrolment_Unit__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        system.runAs(u){
            test.startTest();
            language = TestCoverageUtilityClass.createLanguage();
            acc = TestCoverageUtilityClass.createStudentEUOS(language.Id);
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.queryAustralia(); 
            state = TestCoverageUtilityClass.createState(country.Id); 
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContractWithTo(fSource.Id,accTraining.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id, accTraining.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType(); 
            enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id, 'AUSTRALIA', 'Unknown', '', false, del.Id,attend.Id);
            enrl.Start_Date__c = Date.TODAY() - 50;
            enrl.End_Date__c = Date.TODAY() + 50;
            update enrl;
            result = TestCoverageUtilityClass.createResult(state.Id);
            enrolmentUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, result.Id, loc.Id, lineItemUnit.Id);
            fundStream = TestCoverageUtilityClass.createFundingStream();
            program = TestCoverageUtilityClass.createProgram(qual.Id, loc.Id);
            intake = TestCoverageUtilityClass.createIntake(qual.Id,program.Id,loc.Id,fundStream.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            creditStatus = TestCoverageUtilityClass.createCreditStatus();
            List<Enrolment_Unit__c> euList = [select Id from Enrolment_Unit__c
            where Enrolment__c =: enrl.Id
            ];
            
            for(Enrolment_Unit__c eu: euList){
                eu.Start_Date__c = Date.TODAY() - 25;
                eu.End_Date__c = Date.TODAY() - 1;
            }
            update euList;
            Set<Id> euIds = new Set<Id>();
            
            for(Enrolment_Unit__c eu : euList){
            euIds.add(eu.Id);
            Enrolment_Intake_Unit__c eiu = new Enrolment_Intake_Unit__c();
            eiu = TestCoverageUtilityClass.createEnrolmentIntakeUnit(enrl.Id, intakeUnit.Id, eu.Id);
            eiuList.add(eiu);
            }
            insert eiuList;
            List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>();


            Field_of_Education__c fieldOfEducation = new Field_of_Education__c();
            Id recrdType = [select Id from RecordType where Name = 'Division' LIMIT 1].Id;
            fieldOfEducation.RecordTypeId = recrdType;
            fieldOfEducation.Name = 'No credit was offered for VET';
            insert fieldOfEducation;

            Education_Provider_Type__c educatorProviderType = new Education_Provider_Type__c();
            educatorProviderType.Name = 'No credit/RPL was offered for VET';
            educatorProviderType.Code__c = '12';
            insert educatorProviderType;

            Level_of_Prior_Study_for_RPL_Type__c level = new Level_of_Prior_Study_for_RPL_Type__c();
            level.Name = 'No credit/RPL was offered for VET';
            level.Code__c = '12';
            insert level;

            Credit_status_Higher_Ed_provider_code__c credit = new Credit_status_Higher_Ed_provider_code__c();
            credit.Name = 'No higher education credit offered';
            credit.Code__c = '12';
            insert credit;

            Recognition_of_Prior_Learning_Status__c rec = new Recognition_of_Prior_Learning_Status__c();
            rec.Name = 'Unit of study is NOT an RPL unit of study';
            rec.Code__c = '12';
            insert rec;

            RPL_prior_study_Types__c rpl2 = new RPL_prior_study_Types__c();
            rpl2.Name = 'No credit/RPL was offered';
            rpl2.Code__c = '1234';
            insert rpl2;

            //Insert Completion Status Type 
            Completion_Status_Type__c cst = new Completion_Status_Type__c();
            cst.Name = 'Unit to be commenced later in the year or still in process of completing';
            cst.Code__c = '4';
            cst.Coding_Notes__c = 'Or completion status not yet determined';
            insert cst;
            
            Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
            for(Enrolment_Unit__c eu : euList){
                //Enrolment_Unit_of_Study__c e = new Enrolment_Unit_of_Study__c();
                e = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fld.Id);
                e.Credit_status_Higher_Ed_provider_code__c = creditStatus.Id;
                e.RPL_details_of_prior_study__c = rpl2.Id;
                //e.Start_Date__c = date.newInstance(2012,1,1);
                e.Result__c = null; //set Result__c to NULL
                euosList.add(e);
            }
            
            insert euosList;
            
            PageReference pageRef = Page.EnrolmentUnitList;
            Test.setCurrentPageReference(pageRef);
            
            Test.setCurrentPageReference(new PageReference('Page.EnrolmentUnitList')); 
            System.currentPageReference().getParameters().put('eId', enrl.id);
            
            /*
            EnrolmentUnitList_CC.wrapperEUOS controller2 = new EnrolmentUnitList_CC.wrapperEUOS(euosList);
            EnrolmentUnitList_CC controller = new EnrolmentUnitList_CC();

            controller.save();
            controller.removeEUOS();
            controller.cancel();
            */
            EnrolmentUnitList_CC controller = new EnrolmentUnitList_CC();
            EnrolmentUnitList_CC.wrapperEUOS controller2 = new EnrolmentUnitList_CC.wrapperEUOS(e);
            
            controller2.selected = true;
            controller.wrapEUOS[0].selected = true;
            controller.removeEUOS();
            controller.save();
            controller.cancel();
            
            test.stopTest();
        }
    }

}