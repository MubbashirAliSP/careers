/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for WorkPlaceVisit_CX Class
Test Class:
History
10/01/2013     Created      Sairah Hadjinoor
01/02/2013     Updated      Cletz Cadiz
------------------------------------------------------------*/
@isTest
private class WorkPlaceVisit_CX_Test {

    
    public static Event newEvent(Id enrolmentId, Id contactId, Id userId){
        Event e = new Event();
        e.OwnerId = userId;
        e.WhoId = contactId;
        e.WhatId = enrolmentId;
        e.Subject = 'Meeting';
        e.StartDateTime = date.today();
        e.EndDateTime = date.today().addDays(2);
        return e;
    
    }
    
    public static Enrolment_Intake_Unit__c eiunit(Id enrol, Id intake, Id enrolUnit){
        Enrolment_Intake_Unit__c e = new Enrolment_Intake_Unit__c();
        e.Enrolment__c = enrol;
        e.Intake_Unit__c = intake;
        e.Enrolment_Unit__c = enrolUnit;
        return e;
    }
    static testMethod void insertEnrolmentUnits() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Testing',FirstName='Test', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Enrolment__c enrl2 = new Enrolment__c();
        Country__c country = new Country__c();
        State__c state = new State__c();
        State__c state2 = new State__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        Unit_Hours_and_Points__c unitHours = new Unit_Hours_and_Points__c();
        Enrolment_Unit__c enrlUnitRec = new Enrolment_Unit__c();
        Enrolment_Unit__c enrlUnit = new Enrolment_Unit__c();
        Intake__c intake = new Intake__c();
        Intake_Unit__c intakeUnit = new Intake_Unit__c();
        Class_Configuration__c classConfig = new Class_Configuration__c();
        Staff_Member__c staff = new Staff_Member__c();
        Enrolment_Intake_Unit__c enrlIntakeUnit = new Enrolment_Intake_Unit__c();
        Class_Enrolment__c clEnrolment = new Class_Enrolment__c();
        Results__c res = new Results__c();
        Account acct = new Account();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Event thisEvent = new Event();
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Program__c prog = new Program__c();
        Funding_Stream_Types__c fundStrtype = new Funding_Stream_Types__c();
        Location_Loadings__c loading = new Location_Loadings__c();
        Locations__c loc2 = new Locations__c();
        system.runAs(u){
            
            test.startTest();
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();           
            state = TestCoverageUtilityClass.createState(country.Id); 
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id);
            fundStrtype = TestCoverageUtilityClass.createFundingStreamType();
            pCon = TestCoverageUtilityClass.createPurchasingContractWithTO(fSource.Id, accTraining.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.Id, state.Id, loading.Id);
            loc = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id, state.Id);
            loc2 = TestCoverageUtilityClass.createLocationWithTO(country.Id, parentLoc.Id,accTraining.Id); 
            prog = TestCoverageUtilityClass.createProgram(qual.Id,loc.Id);
            //loc2 = TestCoverageUtilityClass.createLocationWithParent(countryAU.Id, accTraining.Id, parentLoc.Id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            //enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual.Id, lineItem.Id, loc.Id, country.Id);
            enrl = TestCoverageUtilityClass.createEnrolmenteuos(acc.Id, qual.Id, lineItem.Id, loc2.Id, country.Id, 'AUSTRALIA', 'Unknown', '', false, del.Id,attend.Id);
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Predominant_Delivery_Mode__c = del.Id;
            //enrl.Overseas_Country__c = null;
            //insert enrl;
            
            enrl.Enrolment_Status__c = 'Active';
            update enrl;
            
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            enrlUnit = TestCoverageUtilityClass.createEnrolmentUnit(unit.Id, enrl.Id, res.Id, loc2.Id, lineItemUnit.Id);
            
            //enrlUnit = [SELECT id from Enrolment_Unit__c WHERE Enrolment__c =: enrl.Id LIMIT 1];
            
            intake = TestCoverageUtilityClass.createIntake(qual.Id, prog.Id, loc.Id, fundStrtype.Id);
            intakeUnit = TestCoverageUtilityClass.createIntakeUnit(intake.Id, unit.Id);
            enrlIntakeUnit = eiunit(enrl.Id,intakeUnit.Id,enrlUnit.id); insert enrlIntakeUnit;
            
            Contact con= [Select Id, Name,AccountId from Contact where AccountId=:acc.Id LIMIT 1];
            
            
            thisEvent = newEvent(enrl.id,con.Id, u.Id);
            insert thisEvent;
            
            Link__c link = new Link__c();
            link.Name = 'Salesforce Link';
            
            insert link;
            
            PageReference testPage = Page.WorkplaceVisit;
            testPage.getParameters().put('id',thisEvent.id);
            Test.setCurrentPage(testPage);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(thisEvent);
            WorkPlaceVisit_CX workPlace = new WorkPlaceVisit_CX(standardController);
            workPlace.enrolUnitId = enrlUnit.id;
            workPlace.save();
            workPlace.cancel();
            
            test.stopTest();
        }
    }
}