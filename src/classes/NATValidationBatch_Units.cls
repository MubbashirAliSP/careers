/**
 * @description N/A
 * @author N/A
 * @date N/A
 * @history
 *       01.FEB.2016    Ranyel Maliwanag    - Added try-catch block on the email send call on `finish()` to let normal execution flow for sandboxes that have email deliverability turned off
 */
global without sharing class NATValidationBatch_Units implements Database.Batchable<sObject>,Database.Stateful {
      
    global String queryStringMain;
    global NAT_Validation_Log__c log;
    global Integer recordCountNat60 = 0;
    global Integer recordCountNat130 = 0;
    global String NATFile;
    global List<NAT_Validation_Event__c> errorEventList60 = new List<NAT_Validation_Event__c>();
    global List<NAT_Validation_Event__c> errorEventList130 = new List<NAT_Validation_Event__c>();
    global DateTime currentDT;

    global NATValidationBatch_Units(Id logId, String inNatFile) {
        
        NATFile = inNatFile;

        log = [ SELECT ID, Validation_State__c, Training_Organisation_Id__c, end_query__c, Collection_Year__c, NAT00010_Record_Count__c,
                NAT00020_Record_Count__c, NAT00030_Record_Count__c, NAT00060_Record_Count__c, NAT00080_Record_Count__c,
                NAT00085_Record_Count__c, NAT00090_Record_Count__c, NAT00100_Record_Count__c, NAT00120_Record_Count__c, 
                NAT00130_Record_Count__c, log_run_at__c FROM NAT_Validation_Log__c WHERE ID =: logId];
        
        currentDT = log.Log_Run_At__c;
    
        if(NATFile == 'NAT60') {
            queryStringMain = 'Select Id,lastmodifieddate,name,Field_of_Education_Identifier__c,Vet_Non_Vet__c,Unit_Flag__c From Unit__c Where Id in (Select Unit__c ' + log.end_query__c + ')';
        }
        
        if(NATFile == 'NAT130') {
            if(log.Validation_State__c != 'Victoria') {            
             
             	queryStringMain = 'SELECT ID, Enrolment__r.LastModifiedDate,Education_Training_International_Flag__c, Year_Program_Completed__c, Qualification_Issued__c, Enrolment__r.Training_Organisation_Identifier__c,'+
                                 'Enrolment__r.Student_Identifier__c, Enrolment__r.Start_Date__c, Award_ID_CASIS__c, Date_Issued__c, Enrolment__r.Parent_Qualification_Code__c,Enrolment__c  FROM Awards__c WHERE RecordType.Name = \'approved award\' '+  
                                 'AND Award_Type__c = \'Certificate\' AND Qualification_Issued__c = \'Y\' AND Enrolment__c in (Select Enrolment__c ' + log.End_Query__c + ')';
            }
            if(log.Validation_State__c == 'Victoria') {            
             
             	queryStringMain = 'SELECT id,lastmodifieddate, Name, Training_Organisation_Identifier__c,Parent_Qualification_Code__c, Qualification_Identifier__c, Student_Identifier__c, Delivery_location_State__c, Victorian_Commencement_Date__c FROM Enrolment__c WHERE Id IN  (Select Enrolment__c ' + log.End_Query__c + ')';

    		}
        }
    }

    global database.querylocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(queryStringMain);
            
    }
      
    global void execute(Database.BatchableContext BC,List<SObject> scope) {
        
         try {
            if(NATFile == 'NAT60') {

            for(SObject so : scope) {
                recordCountNat60++;
                Unit__c u = (Unit__c) so;
                
                NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.id,Record_Id__c = u.Id, Nat_File_Name__c = 'NAT00060',Parent_Record_Last_Modified_Date__c = u.LastModifiedDate);
    
                    if(u.Field_of_Education_Identifier__c == null) 
                        event.Error_Message__c = 'Unit of Competency Field of Education Identifier cannot be blank';
                   
                    else if(u.Field_of_Education_Identifier__c.length() != 6 || u.Field_of_Education_Identifier__c.endsWith('0'))
                        event.Error_Message__c = 'Unit of Competency Field of Education Identifier must be 6 digits and cannot end with 0';
              
                     else if(u.Unit_Flag__c == 'C' && u.Vet_Non_Vet__c != true) 
                            event.Error_Message__c = 'Unit of Competency Flag is C, so Vet Fee flag must be ticked';
                    
                     if(event.Error_Message__c != null) {
                        errorEventList60.add(event);
                    }
            	}
            }
             
             if(NATFile == 'NAT130') {
                
                 if(log.Validation_State__c != 'Victoria') {
                 	for(SObject so : scope) {
                        recordCountNat130++;
                        Awards__c award = (Awards__c) so;
                 
                        NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.id,Record_Id__c = award.Enrolment__c, Nat_File_Name__c = 'NAT00130',Parent_Record_Last_Modified_Date__c = award.Enrolment__r.LastModifiedDate);
        
                        if(award.Enrolment__r.Training_Organisation_Identifier__c == null)
                            event.Error_Message__c = 'Training Organisation Identifier cannot be blank';
                        else if(award.Enrolment__r.Parent_Qualification_Code__c == null) 
                            event.Error_Message__c = 'Qualification/Course Identifier cannot be blank';
                        else if(award.Enrolment__r.Student_Identifier__c == null) 
                            event.Error_Message__c = 'Student Identifier cannot be blank';
                    
                        if(event.Error_Message__c != null) {
                            errorEventList130.add(event);  
                        }
             		} 
                }
                 if(log.Validation_State__c == 'Victoria') {
                              
             		for(SObject so : scope) {
                        recordCountNat130++;
                        Enrolment__c e = (Enrolment__c) so;

                 		NAT_Validation_Event__c event = new NAT_Validation_Event__c(NAT_Validation_Log__c = log.Id, Record_Id__c = e.Id, Nat_File_Name__c = 'NAT00130',Parent_Record_Last_Modified_Date__c = e.LastModifiedDate);
    
                         if(e.Training_Organisation_Identifier__c == null)
                             event.Error_Message__c = 'Training Organisation Identifier cannot be blank';
                         else if(e.Parent_Qualification_Code__c == null) 
                             event.Error_Message__c = 'Qualification/Course Identifier cannot be blank';
                         else if(e.Student_Identifier__c == null) 
                             event.Error_Message__c = 'Student Identifier cannot be blank';
                         else if(e.Victorian_Commencement_Date__c == null && e.Delivery_location_State__c == 'Victoria') 
                             event.Error_Message__c = 'Victorian Commencment Date (Located on Enrolment) cannot be blank';
						
                        if(event.Error_Message__c != null) {
                            errorEventList130.add(event);  
                        }                   
                 	}
             	}
         	}
         }
        
        catch (exception e) {
            log.System_Message__c = e.getMessage();
        }
    }
      
    global void finish(Database.BatchableContext BC) {
                        
        if(NATFile == 'NAT60') {
            
            insert errorEventList60;

            log.NAT00060_Record_Count__c = recordCountNat60;

            Database.executeBatch(new NATValidationBatch_Units( log.id, 'NAT130' ) );
            
            update log;

        }
        
        if (NATFile == 'NAT130') {
            
            insert errorEventList130;

            log.NAT00130_Record_Count__c = recordCountNat130;
            log.Validation_Complete__c = true;
            
            update log;
            
            try {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {UserInfo.getUserEmail()};  
                mail.setToAddresses(toAddresses);  
                mail.setSubject('NAT Validation is Complete');  
                mail.setPlainTextbody('NAT Validation completed, please click on the URL to check the log. ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id);
                mail.setHTMLBody('NAT Validation completed, please <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + log.id + '">click here</a> to check the log. <br>' +
                                'NAT10 Records: ' + log.NAT00010_Record_Count__c + '<br>' +
                                'NAT20 Records: ' + log.NAT00020_Record_Count__c + '<br>' +
                                'NAT30 Records: ' + log.NAT00030_Record_Count__c + '<br>' +
                                'NAT60 Records: ' + log.NAT00060_Record_Count__c + '<br>' +
                                'NAT80 Records: ' + log.NAT00080_Record_Count__c + '<br>' +
                                'NAT85 Records: ' + log.NAT00085_Record_Count__c + '<br>' +
                                'NAT90 Records: ' + log.NAT00090_Record_Count__c + '<br>' +
                                'NAT100 Records: ' + log.NAT00100_Record_Count__c + '<br>' +
                                'NAT120 Records: ' + log.NAT00120_Record_Count__c + '<br>' +
                                'NAT130 Records: ' + log.NAT00130_Record_Count__c + '<br> <br>' +
                                'Please take note of files with no records, make sure to delete the blank space in NAT file before submission');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
            } catch (Exception e) {
                // silent exception
            }
        	
        
        }
    }
}