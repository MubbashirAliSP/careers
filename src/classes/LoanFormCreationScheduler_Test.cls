/**
 * @description Test class for LoanFormCreationScheduler class
 * @author Ranyel Maliwanag
 * @date 09.NOV.2015
 */
@isTest
public with sharing class LoanFormCreationScheduler_Test {
    static testMethod void testScheduling() {
        Test.startTest();
            String schedule = '0 0 0 * * ?';
            String jobId = System.schedule('Loan Form Creation Test', schedule, new LoanFormCreationScheduler());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            System.assertNotEquals(null, ct);
            System.assertEquals(schedule, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}