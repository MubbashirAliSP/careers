public with sharing class ASMFeedbackSSController {
    @AuraEnabled
    public static list<string> getFeedbackTypesSS() {
        list<string> listFeedbackTypes = new list<string>();
        try{
            Schema.DescribeFieldResult describeFieldDResult = Case.Type.getDescribe();
            list<Schema.PicklistEntry> listPickListEntries = describeFieldDResult.getPicklistValues();
            
            for(Schema.PicklistEntry pickListEntry : listPickListEntries)
            {
                listFeedbackTypes.add(pickListEntry.getValue());
            }
        }
        catch(Exception excep){
            throw new AuraHandledException('An error occured while retrieving Feedback Types.');
        }
        return listFeedbackTypes;
    }
    
    /*@AuraEnabled
    public static Case getFeedbackTypesSS() {
        return new Case();
    }*/
    
    @AuraEnabled
    public static string saveFeedbackSS(string feedbackType, string subject, string message) {
        Case objCase = new Case();
        try{
            objCase.Subject = subject;
            objCase.Type = feedbackType;
            objCase.Description = message;
            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Student Feedback').getRecordTypeId();
            objCase.AccountId = [Select Id, Contact.AccountId From User Where Id =: UserInfo.getUserId()].Contact.AccountId;
            objCase.Origin = 'Web';
            insert objCase;
            system.debug('---Case:' + objCase.Id);
        }
        catch(Exception excep){
            system.debug('---Exception:' + excep.getMessage());
            throw new AuraHandledException('An error occured while creating a Case.');
        }
        return objCase.Id;
    }
}