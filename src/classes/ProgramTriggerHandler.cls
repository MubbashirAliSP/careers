/*
Author: Paolo Mendoza
Date:   23/1/2015
Description: Handler for ProgramTrigger. 
*/

public class ProgramTriggerHandler{

    public static void handleBeforeUpdate(List<Program__c> prog){
    // Makes the intakes related to the Program to inactive if the checkbox Active is ticked
        Set<Id> ProgramActive = new Set<Id>();
        Set<Id> ProgramInactive = new Set<Id>();
         for(Program__c p : prog){
                //Getting id of the Program if inactive
                if(p.Inactive_Program__c == true){
                    
                    ProgramInactive.add(p.id);
                }
                //Getting id of the Program if Active
                if(p.inactive_Program__c== false){
                    ProgramActive.add(p.id);
                }
            }     
            if(ProgramActive.size()>0){
            List<Intake__c> IntaketoInactive = new List<Intake__c>([Select Intake_Status__c, Inactive_Intake_Reason__c, Program__c FROM Intake__c where Program__c =: ProgramActive]);
                if(IntaketoInactive.size()>0){        
                    for(Intake__c i: IntaketoInactive){
                        i.Intake_Status__c = 'Active'; 
                        i.Inactive_Intake_Reason__c = ''; 
                           
                    }         
                    update IntaketoInactive;
                }
            }
            if(ProgramInactive.size()>0){
            List<Intake__c> IntaketoActive = new List<Intake__c>([Select Intake_Status__c, Inactive_Intake_Reason__c, Program__c FROM Intake__c where Program__c =: ProgramInactive]);
                if(IntaketoActive.size()>0){        
                    for(Intake__c i: IntaketoActive){
                         i.Intake_Status__c = 'Inactive';
                        i.Inactive_Intake_Reason__c = 'Management decision';  
                    }         
                    update IntaketoActive;
                }
            }
    }
    
}