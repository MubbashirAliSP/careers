/*
Author: Jade Serrano
Company: Cloud Sherpas
Description: Extension class that redirects new enrolment button to enrolment recordtype page and putting url parameters for auto populate
History
07/05/2013 Jade Serrano Created
01/30/2014 Michelle Magsarili Updated Address fields to new student fields
*/
public with sharing class NewEnrolmentRedirect_CC{

    private ApexPages.StandardController controller {get; set;}
    private Enrolment__c enrol;
    //student id
    private String accid = '';

    public NewEnrolmentRedirect_CC(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        this.enrol = (Enrolment__c)controller.getRecord();
        
        //get student id for student fields
        accId = ApexPages.currentPage().getParameters().get('aId');
        system.debug('%%accId: '+accId);
        
    }
    
    public String NullRemover(String value) { /*If field value is null, inserts blank */
        
        if(value == null)
            return '';
            
        else return value;
    
    }
    
    public PageReference redirect(){
    
        system.debug('%%redirect');
        
        //query needed student fields
        List<Account> accList = new List<Account>();
        accList = [SELECT id, Name, PersonMailingCity, Address_building_property_name__c,Address_flat_unit_details__c,Address_street_number__c, 
                          Address_street_name__c,PersonMailingStreet, PersonMailingCountry, PersonMailingState, PersonMailingPostalCode, 
                          PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherCountry, PersonOtherPostalCode,
                          Suburb_locality_or_town__c,Address_Country__r.Name,Reporting_Billing_State__r.Short_Name__c,Address_Post_Code__c,
                          Postal_delivery_box__c,Postal_suburb_locality_or_town__c,Reporting_Other_State__r.Short_Name__c,Postal_Country__r.Name,Postal_Post_Code__c,Postal_building_property_name__c,
                          Postal_flat_unit_details__c,Postal_street_name__c,Postal_street_number__c
                   FROM Account WHERE id =: accId LIMIT 1];
    
        //query needed lookup fields
        List<Field_of_Education__c> foeList = new List<Field_of_Education__c>();
        List<Credit_status_Higher_Ed_provider_code__c> cshepcList = new List<Credit_status_Higher_Ed_provider_code__c>();
        
        foeList = [SELECT id, Name FROM Field_of_Education__c WHERE Name = 'No credit was offered for VET' LIMIT 1];
        system.debug('%%foeList: '+foeList);
        cshepcList = [SELECT id, Name FROM Credit_status_Higher_Ed_provider_code__c WHERE Name = 'No higher education credit offered' LIMIT 1];
        system.debug('%%cshepcList: '+cshepcList);
        
        String url = ''; 
        //recordtype url
        url += '/setup/ui/recordtypeselect.jsp?ent=01I90000000o27h';
        url += '&save_new_url=a1K/e?';
        
        //url += '/setup/ui/recordtypeselect.jsp?ent=Account'; 
        //url += '&save_new_url=001/e?';
        
        //check if Field_of_Education__c has a value before auto populate
        if(foeList.size()>0){
            //Field_of_Ed_of_prior_VET_Credit_RPL__c lookup field
            //uat field
            url += '&CF00NO0000000ursD_lkid='+foeList[0].Id;
            url += '&CF00NO0000000ursD='+foeList[0].Name;       
            //production field
            url += '&CF00N90000006Bnmn_lkid='+foeList[0].Id;
            url += '&CF00N90000006Bnmn='+foeList[0].Name;
        }
        
        //check if Credit_status_Higher_Ed_provider_code__c has a value before auto populate
        if(cshepcList.size()>0){
            //Credit_status_Higher_Ed_provider__c lookup field
            //uat field
            url += '&CF00NO0000000ursI_lkid='+cshepcList[0].Id;
            url += '&CF00NO0000000ursI='+cshepcList[0].Name;
            //production field
            url += '&CF00N90000006Bnmk_lkid='+cshepcList[0].Id;
            url += '&CF00N90000006Bnmk='+cshepcList[0].Name;
        }
        
        //check if student has a value before auto populate
        if(accList.size()>0){
            //Residential Address Information copied to Enrolment from Person Account
            url += '&00N90000008H55T='+NullRemover(accList[0].Address_building_property_name__c);
            url += '&00N90000008H55U='+NullRemover(accList[0].Address_flat_unit_details__c);
            url += '&00N90000008H55W='+NullRemover(accList[0].Address_street_number__c);
            url += '&00N90000008H55V='+NullRemover(accList[0].Address_street_name__c);
            url += '&00N90000004xMnO='+NullRemover(accList[0].Suburb_locality_or_town__c);
            url += '&00N90000004xMnP='+NullRemover(accList[0].Address_Country__r.Name);
            url += '&00N90000004xMnQ='+NullRemover(accList[0].Reporting_Billing_State__r.Short_Name__c);
            url += '&00N90000004xMnS='+NullRemover(accList[0].Address_Post_Code__c);
            //Postal Address Information copied to Enrolment from Person Account
            url += '&00NN0000000kU7X='+NullRemover(acclist[0].Postal_building_property_name__c);
            url += '&00NN0000000kU7c='+NullRemover(acclist[0].Postal_flat_unit_details__c);
            url += '&00NN0000000kU7h='+NullRemover(acclist[0].Postal_street_number__c);
            url += '&00NN0000000kU7m='+NullRemover(acclist[0].Postal_street_name__c);   
            url += '&00N90000004xMnc='+NullRemover(accList[0].Postal_delivery_box__c);
            url += '&00N90000004xMnZ='+NullRemover(accList[0].Postal_suburb_locality_or_town__c);
            url += '&00N90000004xMnb='+NullRemover(accList[0].Reporting_Other_State__r.Short_Name__c);
            url += '&00N90000004xMna='+NullRemover(accList[0].Postal_Country__r.Name);
            url += '&00N90000004xMnd='+NullRemover(accList[0].Postal_Post_Code__c);
            //MAM 01/30/2014 end
            //Enrolment_Status__c
            url += '&00N90000004xMn7=Active (Commencement)';
            //Student__c
            url += '&CF00N90000004xMet_lkid='+accList[0].Id;
            url += '&CF00N90000004xMet='+accList[0].Name;
            //return url
            url += '&retURL='+accList[0].Id;
        }
        
        //ignore button override to retain url parameters after enrolment recordtype page
        url += '&nooverride=1'; 
        system.debug('%%url: '+url);
        
        return new PageReference(url);
        
    }

}