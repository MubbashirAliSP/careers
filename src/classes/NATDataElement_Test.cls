@isTest
public class NATDataElement_Test {
    
	@isTest static void testDataElement() {
            
        test.startTest();
            
        NATDataElement ndeTest = new NATDataElement('abc', 1, false, 'string');
            
        system.assertEquals( 1, ndeTest.fieldLength );
        system.assertEquals( 'abc', ndeTest.fieldName );
        system.assertEquals( 'string', ndeTest.fieldType );
        system.assertEquals( false, ndeTest.customLogic );
    
        test.stopTest();
         
    }

}