global with sharing class BlackboardInitializeAndLoginJ0 implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
   

    global String query;
    
    global BlackboardInitializeAndLoginJ0() {

        query = 'Select Session_ID__c From Blackboard_Sync_Log__c Where Session_ID__c = null Order by CreatedDate desc LIMIT 1';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

         
        HttpResponse initResponse; //initialize response
        HttpResponse loginReponse; //login response
      
        String sessionId = '';
        String result ='';
        
        if(!test.IsRunningTest()) {
    
            BlackboardcontextWS contextWS = new BlackboardcontextWS();
            //We do not need to test login and initialize step. It has been tested in IntegrationTest Class
            if(!Test.IsRunningTest()) {
        
                initResponse = contextWS.initialize();
                XmlStreamReader reader = initResponse.getXmlStreamReader();    
                
        
                while(reader.hasNext()) {
                     //  Start at the beginning of the book and make sure that it is a book
                    if (reader.getEventType() == XmlTag.START_ELEMENT) {
                        if ('return'== reader.getLocalName()) {
                        sessionId = BlackboardUtility.parseResult(reader);             
                        break;
                        }
                    }
                    reader.next();
                }
             
               loginReponse = contextWS.login(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).userid__c, Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).password__c, 'YG','YG','', Integer.ValueOf(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).expectedLifeSeconds__c), sessionId);
             
               XmlStreamReader reader2 = loginReponse.getXmlStreamReader();
           }
           
       }
       
       if(test.IsRunningTest())
           sessionId ='TestSessionId';
     
        for(SObject s :scope) {

            Blackboard_Sync_Log__c log = (Blackboard_Sync_Log__c) s;
            
            log.Session_Id__c = sessionId;
            
            update log;

        }
       
       


    }
    
    global void finish(Database.BatchableContext BC) {
        
      BlackboardIntakeUnitLoadingJ1 b1 = new BlackboardIntakeUnitLoadingJ1();
      if(!Test.IsRunningTest())
          database.executebatch(b1,1);
        
    }
    
}