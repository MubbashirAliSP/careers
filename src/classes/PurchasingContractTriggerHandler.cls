/**
 * @description Line Item Qualification Unit Trigger Handler
 * @author Janella Lauren Canlas
 * @date 26.NOV.2012
 *
 * HISTORY
 * - 26.NOV.2012	Janella Canlas		- Created.
 * - 05.FEB.2013	Janella Canlas		- added method to populate Contract Type on Enrolments
 */
public with sharing class PurchasingContractTriggerHandler {
	/*
		This method will update the recordtypes of Contract Line Item related records
	*/
	public static void updateRecordTypes(List<Purchasing_Contracts__c> pcList){
		Set<Id> pcIds = new Set<Id>();
		Set<Id> cliIds = new Set<Id>();
		Set<Id> liqIds = new Set<Id>();
		
		List<Purchasing_Contracts__c> pconList = new List<Purchasing_Contracts__c>();
		List<Contract_Line_Items__c> cliList = new List<Contract_Line_Items__c>();
		List<RecordType> recTypeList = new List<RecordType>();
		List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
		List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();	
		
		Map<String,List<RecordType>> recTypeMap = new Map<String,List<RecordType>>();
		
		//collect purchasing contract ids
		for(Purchasing_Contracts__c p : pcList){
			pcIds.add(p.Id);
		}
		//query purchasing contract records
		pconList = [select Id, RecordType.Name FROM Purchasing_Contracts__c WHERE Id IN: pcIds];
		
		//query contract line items records
		recTypeList = [SELECT Id, Name, SObjectType from RecordType Where Name IN('AHC Funded','Unit Funded')/* AND SobjectType='Contract_Line_Items__c'*/];
		//create map of Contract Line Item record type name with corresponding Id
		
		for(RecordType r : recTypeList){
			if(recTypeMap.containsKey(r.sObjectType)){
				List<RecordType> temp = recTypeMap.get(r.sObjectType);
				temp.add(r);
				recTypeMap.put(r.sObjectType,temp);
			}
			else{
				recTypeMap.put(r.sObjectType,new List<RecordType>{r});
			}
		}
		
		
		//query contract line item records
		cliList = [SELECT Id, RecordTypeId, Purchasing_Contract__c from Contract_Line_Items__c where Purchasing_Contract__c IN: pcIds];
		system.debug('***pconList'+pconList);
		system.debug('***cliList'+cliList);
		//populate contract line item record type
		for(Purchasing_Contracts__c p : pconList){
			for(Contract_Line_Items__c c : cliList){
				system.debug('***p.Id'+p.Id);
				system.debug('***c.Purchasing_Contract__c'+c.Purchasing_Contract__c);
				if(p.Id == c.Purchasing_Contract__c){
					List<RecordType> recTemp = recTypeMap.get('Contract_Line_Items__c');
					for(RecordType r : recTemp){
						if(p.RecordType.Name == r.Name){
							c.RecordTypeId = r.Id;
						}
					}
				}
			}
		}
		//update contract line items
		try{update cliList;}catch(Exception e){System.debug('***ERROR:'+e.getMessage());}
		
		//collect inserted contract line items
		for(Contract_Line_Items__c c : cliList){
			cliIds.add(c.Id);
		}
		//query line item qualification records
		liqList = [SELECT Id, RecordTypeId, Contract_Line_Item__c, Contract_Line_Item__r.Purchasing_Contract__c from Line_Item_Qualifications__c where Contract_Line_Item__c IN: cliIds];
		//populate line item qualification record tpye
		for(Purchasing_Contracts__c p : pconList){
			for(Line_Item_Qualifications__c l : liqList){
				system.debug('***p.Id'+p.Id);
				if(p.Id == l.Contract_Line_Item__r.Purchasing_Contract__c){
					
					List<RecordType> recTemp = recTypeMap.get('Line_Item_Qualifications__c');
					for(RecordType r : recTemp){
						if(p.RecordType.Name == r.Name){
							l.RecordTypeId = r.Id;
						}
					}
				}
			}
		}
		//update line item qualification records
		try{update liqList;}catch(Exception e){System.debug('***ERROR:'+e.getMessage());}
		
		//collect line item qualification ids
		for(Line_Item_Qualifications__c c : liqList){
			liqIds.add(c.Id);
		}
		//query line item qualification units to be updated
		liquList = [SELECT Id, RecordTypeId, Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c from Line_Item_Qualification_Units__c where Line_Item_Qualification__c IN: liqIds];
		//populate line item qualification unit recordtype
		for(Purchasing_Contracts__c p : pconList){
			for(Line_Item_Qualification_Units__c l : liquList){
				system.debug('***p.Id'+p.Id);
				if(p.Id == l.Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c){
					
					List<RecordType> recTemp = recTypeMap.get('Line_Item_Qualification_Units__c');
					for(RecordType r : recTemp){
						if(p.RecordType.Name == r.Name){
							l.RecordTypeId = r.Id;
						}
					}
				}
			}
		}
		//update line item qualification unit records
		try{update liquList;}catch(Exception e){System.debug('***ERROR:'+e.getMessage());}
	}
	
	/*
		This method populates the Purchasing Contract National Fund Source Code
	*/
	public static void populateNFSC(List<Purchasing_Contracts__c> pcList){
		Set<Id> SFIds = new Set<Id>();
		List<Funding_Source__c> fsList = new List<Funding_Source__c>();
		//collect related funding source ids
		for(Purchasing_Contracts__c p : pcList){
			SFIds.add(p.State_Fund_Source__c);
		}
		//query related funding source records
		fsList = [select id, National_Funding_Source_Code__c, National_Funding_Source_Name__c From Funding_Source__c Where Id IN:SFIds];
		Map<Id, Funding_Source__c> fsMap = new Map<Id, Funding_Source__c>();
		for(Funding_Source__c f : fsList){
			fsMap.put(f.Id, f);
		}
		//populate purchasing contract National Fund Source Code for update
		
	}
	/*
		This method populates Contract Type on related Enrolment records upon update
	*/
	public static void populateContractType(Set<Id> pcIds,List<Purchasing_Contracts__c> pcList){
		List<Enrolment__c> enrolmentList = new List<Enrolment__c>();
		enrolmentList = [select Id,Contract_Type__c, Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c from Enrolment__c where Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c IN: pcIds];
		Map<Id, Purchasing_Contracts__c> pcMap = new Map<Id, Purchasing_Contracts__c>();
		for(Purchasing_Contracts__c p : pcList){
			pcMap.put(p.Id, p);
		}
		
		for(Enrolment__c e : enrolmentList){
			if(pcMap.containsKey(e.Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c)){
				e.Contract_Type__c = pcMap.get(e.Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c).Contract_Type__c;
			}
		}
		try{update enrolmentList;}catch(Exception e){system.debug('ERROR: ' + e.getMessage());}
	}
	
}