/*
 * @description Enrolment Unit of Study iterable class for HEIMS pagination
 * @author D. Crisologo (CloudSherpas)
 * @date 19.JUN.2013
 
 * @date 26.MAR.2014 W. Malibago -- Divide euosWrapperList by 2 to avoid list out of bounds error
*/
global class EnrolmentUnitOfStudyIterable implements Iterator<list<EnrolmentUnitOfStudyWrapper>>{

   transient List<Enrolment_Unit_of_Study__c> euos {get; set;}
   public transient List<EnrolmentUnitOfStudyWrapper> euosWrapperList {get; set;}
   transient List<EnrolmentUnitOfStudyWrapper> euosWrapperListRequested {get; set;}
   Integer i {get; set;}
   public Set<Id> relatedEnrolmentIds {get; set;}   
   public Integer setPageSize {get; set;}
   private String sQuery;
   private Date dtStart1;
   private Date dtEnd1;
   private Boolean includePreviouslyExported;

    /*
     * @description Class constructor for EnrolmentUnitOfStudyIterable
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        sQuery - the query string to be invoked in class constructor
     *         dtStart1 - start date selected
     *         dtEnd1 - end date selected
     *         includePreviouslyExported - if all that were previously exported should be included in the select
     * @returns:      None
    */
   public EnrolmentUnitOfStudyIterable(String sQuery, Date dtStart1, Date dtEnd1, Boolean includePreviouslyExported){
       this.sQuery = sQuery;
       this.dtStart1 = dtStart1;
       this.dtEnd1 = dtEnd1;
       this.includePreviouslyExported = includePreviouslyExported;
       euos = Database.Query(sQuery);
       euosWrapperList = new list<EnrolmentUnitOfStudyWrapper>();
       euosWrapperListRequested = new list<EnrolmentUnitOfStudyWrapper>();  
       relatedEnrolmentIds = new Set<Id>();
       for(Enrolment_Unit_of_Study__c eu : euos) {
            EnrolmentUnitOfStudyWrapper euosWrapper = new EnrolmentUnitOfStudyWrapper(false, eu);
            euosWrapperList.add(euosWrapper);
            relatedEnrolmentIds.add(eu.Enrolment_Unit__r.Enrolment__c);
       }
       setPageSize = 100;
       i = 0;
   }  

    /*
     * @description check if current iteration is not yet the last record
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      Boolean - returns true if current iteration is not yet on the last record
    */
   global boolean hasNext(){
   
   String sQueryOffset;
   //if(i>0){
        //sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i*100-1);
   //}else{
        sQueryOffset = sQuery;
   //}
   system.debug('%%sQueryOffset: '+sQueryOffset);
   euos = Database.Query(sQueryOffset);
   euosWrapperList = new list<EnrolmentUnitOfStudyWrapper>();
   relatedEnrolmentIds = new Set<Id>();
   for(Enrolment_Unit_of_Study__c eu : euos) {
        EnrolmentUnitOfStudyWrapper euosWrapper = new EnrolmentUnitOfStudyWrapper(false, eu);
        euosWrapperList.add(euosWrapper);
        relatedEnrolmentIds.add(eu.Enrolment_Unit__r.Enrolment__c);
   }
   
       if(i >= euosWrapperList.size()) {
           return false;
       } else {
           return true;
       }
   }
  
    /*
     * @description check if current iteration is not yet the first record
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      Boolean - returns true if current iteration is not yet on the first record
    */
   global boolean hasPrevious(){
       if(i <= setPageSize) {
           return false;
       } else {
           return true;
       }
   }  

    /*
     * @description moves the iteration on the next pagesize records
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      list<EnrolmentUnitOfStudyWrapper> - next set of records
    */
   global list<EnrolmentUnitOfStudyWrapper> next(){      
       euosWrapperListRequested = new list<EnrolmentUnitOfStudyWrapper>();
       Integer startNumber;       
       
       String sQueryOffset;
       if(i>0){
            sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i-100);
       }else{
            sQueryOffset = sQuery;
       }
       system.debug('%%sQueryOffset: '+sQueryOffset);
       euos = Database.Query(sQueryOffset);
       relatedEnrolmentIds = new Set<Id>();
       for(Enrolment_Unit_of_Study__c eu : euos) {
            EnrolmentUnitOfStudyWrapper euosWrapper = new EnrolmentUnitOfStudyWrapper(false, eu);
            euosWrapperList.add(euosWrapper);
            relatedEnrolmentIds.add(eu.Enrolment_Unit__r.Enrolment__c);            
       }
       
       //has to divide it by 2 to avoid List out of bounds
       Integer size = euosWrapperList.size() / 2;

       if(hasNext())
       { 
           if(size <= (i + setPageSize)) {
               startNumber = i;
               i = size;
           } else {
               i = (i + setPageSize);
               startNumber = (i - setPageSize);
           }
          
           for(integer start = startNumber; start < i; start++) //0; 0<24, 0++
           {
               euosWrapperListRequested.add(euosWrapperList[start]);
           }
       }
       return euosWrapperListRequested;
   }
  
    /*
     * @description moves the iteration on the previous pagesize records
     * @author D. Crisologo
     * @date 19.JUN.2013
     * @input:        None
     * @returns:      list<EnrolmentUnitOfStudyWrapper> - previous set of records
    */
   global list<EnrolmentUnitOfStudyWrapper> previous(){     
       euosWrapperListRequested = new list<EnrolmentUnitOfStudyWrapper>();

       String sQueryOffset;
       if(i>0){
            sQueryOffset = sQuery + 'OFFSET ' + String.valueOf(i-100);
       }else{
            sQueryOffset = sQuery;
       }
       system.debug('%%sQueryOffset: '+sQueryOffset);
       euos = Database.Query(sQueryOffset);
       relatedEnrolmentIds = new Set<Id>();
       for(Enrolment_Unit_of_Study__c eu : euos) {
            EnrolmentUnitOfStudyWrapper euosWrapper = new EnrolmentUnitOfStudyWrapper(false, eu);
            euosWrapperList.add(euosWrapper);
            relatedEnrolmentIds.add(eu.Enrolment_Unit__r.Enrolment__c);
       }
       
       integer size = euosWrapperList.size();
       if(i == size)
       {
           if(math.mod(size, setPageSize) > 0)
           {   
               i = size - math.mod(size, setPageSize);
           }
           else
           {
               i = (size - setPageSize);
           }
       }
       else
       {
           i = (i - setPageSize);
       }
      
       for(integer start = (i - setPageSize); start < i; ++start)
       {
           euosWrapperListRequested.add(euosWrapperList[start]);
       }
       return euosWrapperListRequested;
   }  
   
}