public class NATConstructor {
    
    /* 
     * NATConstructor Class - Constructs NAT file strings from a record, data definition and state values for NAT Generator 2
     * Mar 2015 - M.Wheeler - CloudSherpas
     */
    
    public static String natFile(sObject s, Map<Integer, NATDataElement> dataDefinition, String state) {
        
        String natBody = '';
               
    	for(Integer a = 1; a < dataDefinition.size() + 1; a++) { // loop through AVETMISS data elements
          
            NATDataElement element = dataDefinition.get(a);
           
            if(!element.customLogic ) {
                
                if( element.fieldType.equalsIgnoreCase('string') ) {
                    if(s.get(element.fieldName) != null) {
                        natBody += NATGeneratorFormat.formatString(String.valueOf(s.get(element.fieldName)), element.fieldLength);
                    }
                    else {
                        natBody += NATGeneratorFormat.formatString('', element.fieldLength);
                    }
                }
                else if( element.fieldType.equalsIgnoreCase('date') ) { 
                    if(s.get(element.fieldName) != null) {
						natBody += NATGeneratorFormat.formatDate(String.valueOf(s.get(element.fieldName)));                    
                    }
                    else {
						natBody += NATGeneratorFormat.formatDate('');                    
                    }
                    
                }
                else if ( element.fieldType.equalsIgnoreCase('number') ) {
                    if(s.get(element.fieldName) != null) {
                    	natBody += NATGeneratorFormat.formatNumber(String.valueOf(s.get(element.fieldName)), element.fieldLength);
                    }
                    else {
                    	natBody += NATGeneratorFormat.formatNumber('', element.fieldLength);
                    }                    
                }
                else if (element.fieldType.equalsIgnoreCase('boolean') ) {
                    natBody += NATGeneratorFormat.formatCheckBox( boolean.valueOf(s.get(element.fieldName) ) );
                }
            }
            if( element.customLogic ) { // if custom logic flag is true
                natBody += NATLogicEngine.doLogic(s, element , state);
            }
        }   
        
    return natBody;   
    }
    
    public static String vicNat130(Awards__c a, Enrolment__c e, Map<Integer, NATDataElement> dataDefinition) {
        
        /*String natBody = '';
               
    	for(Integer b = 1; b < dataDefinition.size(); b++) { // loop through AVETMISS data elements
          
            NATDataElement element = dataDefinition.get(b);
            
            NATGeneratorUtility.formatHours(String.valueOf(enrl.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c), NAT00130__c.getInstance('01').Length__c) +
			NATGeneratorUtility.formatOrPadTxtFile(enrl.Parent_Qualification_Code__c, NAT00130__c.getInstance('02').Length__c,'') +
            NATGeneratorUtility.formatOrPadTxtFile(enrl.Student_Identifier__c, NAT00130__c.getInstance('03').Length__c,'');
            if(enrl.awards__r.size() == 0) { // if enrolment has no award, output @@@@ for year completed and N for qualification issued in NAT130 file
                body130 += '@@@@';
                body130 += 'N';
                    	
            }
            
            else { // otherwise, outout data from award record, year completed and qualification issues
                for(Awards__c award : enrl.awards__r ) {
                    body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Year_Program_Completed__c, NAT00130__c.getInstance('04').Length__c,'');
                    body130 += NATGeneratorUtility.formatOrPadTxtFile(award.Qualification_Issued__c, NAT00130__c.getInstance('05').Length__c,'');
                }
            }
            body130 += NATGeneratorUtility.formatOrPadTxtFile(NATGeneratorUtility.formatDate(String.valueOf(enrl.Victorian_Commencement_Date__c)), NAT00130__c.getInstance('06').Length__c,'');
        }   
        */
    return null;   
    } 
}