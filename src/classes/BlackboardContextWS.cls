global class BlackboardContextWS {

     public HTTPResponse initialize() {
        
        String username = 'session';
        String password = 'nosession';
        Datetime expiresDT = Datetime.now() + 1;
        String created = Datetime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String expires = expiresDT.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        String SoapXMLBody;
        
        SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
        
            '<wsu:Timestamp wsu:Id="TS-15"><wsu:Created>'+created+'</wsu:Created><wsu:Expires>'+expires+'</wsu:Expires></wsu:Timestamp>'+
            '<wsse:UsernameToken wsu:Id="UsernameToken-14"><wsse:Username>'+username+'</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+password+'</wsse:Password><wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">lYe3RV5nQx26UHRcRAL4yQ==</wsse:Nonce><wsu:Created>'+created+'</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header>' +
                        ' <soapenv:Body/> ' +
                      '</soapenv:Envelope>';
                      
                      
        string SoapXML;
        SoapXML = SoapXMLBody;
        Integer ContentLength = 0;
        ContentLength = SoapXML.length();
        
        String sessionId = '';


       

        Http h = new Http();

        HttpRequest req = new HttpRequest();

        HttpResponse res = new HttpResponse();
      

        req.setMethod('POST');

        req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Context_WS_Endpoint__c);

        req.setHeader('Content-type','text/xml');
        req.setHeader('Content-Length',ContentLength.format());

        
        req.setHeader('SoapAction','initialize');
    
        req.setBody(SoapXML);
        res = h.send(req);
               
        return res;
        
    }
    
     public HttpResponse login(String userid,String password,String clientVendorId,String clientProgramId,String loginExtraInfo,Integer expectedLifeSeconds, String sessionId) {
     
        String username = 'session';
        String result = '';
       
        Datetime expiresDT = Datetime.now().addSeconds(expectedLifeSeconds);
        String created = Datetime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String expires = expiresDT.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        String SoapXMLBody;
        
        
        SoapXMLBody = '<soapenv:Envelope xmlns:con="http://context.ws.blackboard" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
          '<soapenv:Header>' +
            '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
                '<wsu:Timestamp wsu:Id="TS-24">'+
                     '<wsu:Created>'+created+'</wsu:Created>'+
                     '<wsu:Expires>'+expires+'</wsu:Expires>'+
               '</wsu:Timestamp>'+
            '<wsse:UsernameToken wsu:Id="UsernameToken-23">'+
                '<wsse:Username>'+username+'</wsse:Username>'+
                '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+sessionId+'</wsse:Password>'+
                '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">kULlu7lj2x6Sihm/JOhk6A==</wsse:Nonce>'+
                
                '<wsu:Created>'+created+'</wsu:Created>'+
                
            '</wsse:UsernameToken></wsse:Security>'+
        
        
       ' </soapenv:Header>'+
        
            ' <soapenv:Body> ' +
            
            ' <con:login> ' +
              
                 '<con:userid>'+userid+'</con:userid> ' +
                 '<con:password>'+password+'</con:password> ' +
                 '<con:clientVendorId>'+clientVendorId+'</con:clientVendorId> ' +
                 '<con:clientProgramId>'+clientProgramId+'</con:clientProgramId> ' +
                 '<con:loginExtraInfo xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/> ' +
                 '<con:expectedLifeSeconds>'+expectedLifeSeconds+'</con:expectedLifeSeconds> ' +
              
            '  </con:login> ' +  
                
          ' </soapenv:Body> ' +     
        '</soapenv:Envelope>';
                 
                  
        string SoapXML;
        SoapXML = SoapXMLBody;
        Integer ContentLength = 0;
        ContentLength = SoapXML.length();
                      
        Http h = new Http();

        HttpRequest req = new HttpRequest();

        HttpResponse res = new HttpResponse();
      

        req.setMethod('POST');

        req.setEndPoint(Blackboard_Configuration__c.getInstance(BlackboardUtility.GetEndpoint()).Context_WS_Endpoint__c); 

        req.setHeader('Content-type','text/xml');
        req.setHeader('Content-Length',ContentLength.format());

        
        req.setHeader('SoapAction','login');
        
        system.debug(soapXML);
    
        req.setBody(SoapXML);
        res = h.send(req);
        
        return res;
               
           
     }
    
}