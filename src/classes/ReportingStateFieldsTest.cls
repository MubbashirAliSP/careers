/*------------------------------------------------------------
Author:        Sairah Hadjinoor
Company:       CloudSherpas
Description:   Test Class for ReportingStateFields Class and AccountBeforeInsertBeforeUpdate Trigger
Test Class:
History
18/12/2012     Created      Sairah Hadjinoor
15/02/2013     Edited       Michi Magsarili      modified method createAccount5 for new checkbox field Postal_is_the_same_as_Shipping__c
05/03/2013     Updated      Janella Canlas 
------------------------------------------------------------*/

@isTest
private class ReportingStateFieldsTest {

    static testMethod void createAccount() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        List<Account> accList = new List<Account>();
        
        String stud = [select id from RecordType where Name='Student' And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student Account';
        sacct.FirstName = 'First Name';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.PersonMailingState ='QLD';
        sacct.PersonMailingPostalCode = '4000';
        sacct.Student_Identifer__c = 'H89373';
        sacct.PersonBirthDate = system.today() - 50;
        sacct.Year_Highest_Education_Completed__c = '2012';
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.Postal_is_the_same_as_Residential__c = true;
        sacct.PersonMobilePhone = '61847292112';
        sacct.Salutation = 'Mr.';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        
        Id recrdType = [select Id from RecordType where Name = 'Training Organisation'  And SObjectType = 'Account' LIMIT 1].Id;
        Account tacct = new Account();
        tacct.RecordTypeId = recrdType;
        tacct.Name = 'training';
        tacct.BillingCountry = 'AUSTRALIA';
        tacct.BillingState ='QLD';
        tacct.BillingPostalCode = '4000';
        tacct.Training_Org_Identifier__c = 'test';
        tacct.Training_Org_Type__c = 'test';
        tacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        tacct.Postal_is_the_same_as_Shipping__c = true;
        
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c ctry = new Country__c();
            ctry.Name = 'Country';
            ctry.Country_Code__c = '@@@@';
            insert ctry;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            insert sacct;
            insert tacct;
            try{
                sacct.PersonOtherCity = 'City';
                update sacct;
                System.AssertEquals(sacct.PersonOtherCity, 'City');
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot modify Postal Address if "Postal is the same as Residential field" is checked.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            try{
                tacct.ShippingCity = 'City';
                update tacct;
                System.AssertEquals(sacct.PersonOtherCity, 'City');
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot modify Shipping Address if "Postal is the same as Shipping field" is checked.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            Test.stopTest();
            System.assertEquals(sacct.LastName, 'Test student Account');
        }
    }
    
    static testMethod void createAccount2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='Account_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test');
        List<Account> accList = new List<Account>();
        
        String stud = [select id from RecordType where Name='Student'  And SObjectType = 'Account'].Id; 
        Account sacct = new Account();
        sacct.RecordTypeId = stud;
        sacct.LastName = 'Test student Account';
        sacct.FirstName = 'First Name';
        sacct.PersonMailingCountry = 'AUSTRALIA';
        sacct.PersonMailingState ='OTHER';
        sacct.Student_Identifer__c = 'H89373';
        sacct.PersonBirthDate = system.today() - 50;
        sacct.Year_Highest_Education_Completed__c = '2011';
        sacct.Postal_is_the_same_as_Residential__c = true;
        sacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        sacct.PersonMobilePhone = '61847292112';
        sacct.Salutation = 'Mr.';
        sacct.Learner_Unique_Identifier__c = '12ab34cd56';
        Id recrdType = [select Id from RecordType where Name = 'Training Organisation'  And SObjectType = 'Account' LIMIT 1].Id;
        Account tacct = new Account();
        tacct.RecordTypeId = recrdType;
        tacct.Name = 'training';
        tacct.BillingCountry = 'AUSTRALIA';
        tacct.BillingState ='OTHER';
        tacct.Training_Org_Identifier__c = 'test';
        tacct.Training_Org_Type__c = 'test';
        tacct.Postal_is_the_same_as_Shipping__c = true;
        tacct.Proficiency_in_Spoken_English__c = '1 - Very well';
        system.runAs(u){
            Test.startTest();
            Labour_Force_Status_Code__c labor = new Labour_Force_Status_Code__c();
            labor.Name = 'Labor';
            labor.Code__c = '@@';
            insert labor;
            Country__c ctry = new Country__c();
            ctry.Name = 'Country';
            ctry.Country_Code__c = '@@@@';
            insert ctry;
            Language__c language = new Language__c();
            language.Name = 'Language';
            language.Code__c = '@@@@';
            insert language;
            insert sacct;
            insert tacct;
            Test.stopTest();
            System.assertEquals(sacct.LastName, 'Test student Account');
        }
    }
    
}