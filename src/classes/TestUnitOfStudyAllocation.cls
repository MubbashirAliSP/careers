@istest(seeAllData=true)
public class TestUnitOfStudyAllocation {
    
     
    static testMethod void unitTest() {
        
        Qualification__c q = [Select id From Qualification__c Limit 1];
        Unit__c u = [Select Id From Unit__c Limit 1];
        
        PageReference testPage = Page.UnitsOfStudyAllocation;
        testPage.getParameters().put('id', q.Id);
        Test.setCurrentPage(testPage);
        
        UnitsOfStudyAllocationController controller = new UnitsOfStudyAllocationController();
        controller.cancel();
        controller.save();

        //throw exception
        PageReference testPage2 = Page.UnitsOfStudyAllocation;
        testPage2.getParameters().put('id', 'throwError');
        Test.setCurrentPage(testPage2);        
        UnitsOfStudyAllocationController controller2 = new UnitsOfStudyAllocationController();
        controller2.save();
        
       UnitsOfStudyAllocationWrapper w = new UnitsOfStudyAllocationWrapper(false,u);
       w.getStages();
       w.getTypes();
        
        
    }

}