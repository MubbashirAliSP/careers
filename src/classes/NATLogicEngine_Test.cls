@isTest
public class NATLogicEngine_Test {
    
    
    @isTest public static void testNoObject() {
        
        Account stu = NATTestDataFactory.createStudent();

        NATDataElement nde = new NATDataElement('BlankField', 2, true, 'string'); 
        NATDataElement nde1 = new NATDataElement('PublicPrivateTrainingOrganisationFlag', 2, true, 'string'); 

        
        String output = NATLogicEngine.doLogic(stu, nde, 'Queensland' );
        
        system.assertEquals('  ', output);
        
        String output1 = NATLogicEngine.doLogic(stu, nde1, 'Queensland' );
        
        system.assertEquals('2 ', output1);


        
    }
    
        @isTest public static void testContactObject() {
            
            Account tOrg = NATTestDataFactory.createTrainingOrganisation();
            tOrg.Training_Org_Identifier__c = '1234';
            tOrg.National_Provider_Number__c = '1234';
            tOrg.Software_Product_Name__c = 'salesforce';
            tOrg.Software_Vendor_Email_Address__c = 'email@domain.com';
            tOrg.AVETMISS_Organisation_Name__c = 'careers';
            tOrg.Training_Org_Identifier__c = 'careers';
            update tOrg;
                
            Contact tOrgCt = new Contact(FirstName = 'Test', LastName = 'Test', AccountId = tOrg.Id);
            
            Insert tOrgCt;
                
          	Contact tOrgCt2 = [SELECT ID, Account.National_Provider_Number__c, Account.Software_Product_Name__c, Account.Software_Vendor_Email_Address__c,
                              Account.AVETMISS_Organisation_Name__c, Account.Training_Org_Type_Identifier__c, Account.BillingStreet, Account.BillingCity,
                              Account.BillingPostalCode, Account.State_Identifier__c, Account.Training_Org_Identifier__c FROM Contact WHERE ID = :tOrgCt.Id ];
         
            NATDataElement nde1 = new NATDataElement('Account.National_Provider_Number__c', 20, true, 'string');
            NATDataElement nde2 = new NATDataElement('Account.Software_Product_Name__c', 20, true, 'string');
            NATDataElement nde3 = new NATDataElement('Account.Software_Vendor_Email_Address__c', 20, true, 'string');
            NATDataElement nde4 = new NATDataElement('Account.AVETMISS_Organisation_Name__c', 20, true, 'string');
            NATDataElement nde5 = new NATDataElement('Account.Training_Org_Type_Identifier__c', 20, true, 'string');
            NATDataElement nde6 = new NATDataElement('Account.BillingStreet', 20, true, 'string');
            NATDataElement nde7 = new NATDataElement('Account.BillingCity', 20, true, 'string');
            NATDataElement nde8 = new NATDataElement('Account.BillingPostalCode', 20, true, 'string');
            NATDataElement nde9 = new NATDataElement('Account.State_Identifier__c', 20, true, 'string');
            NATDataElement nde10 = new NATDataElement('Account.Training_Org_Identifier__c', 20, true, 'string');

            
            String output1 = NATLogicEngine.doLogic(tOrgCt2, nde1, 'Queensland' );
			String output2 = NATLogicEngine.doLogic(tOrgCt2, nde2, 'Queensland' );
			String output3 = NATLogicEngine.doLogic(tOrgCt2, nde3, 'Queensland' );
            String output4 = NATLogicEngine.doLogic(tOrgCt2, nde4, 'Queensland' );
            String output5 = NATLogicEngine.doLogic(tOrgCt2, nde5, 'Queensland' );
            String output6 = NATLogicEngine.doLogic(tOrgCt2, nde6, 'Queensland' );
            String output7 = NATLogicEngine.doLogic(tOrgCt2, nde7, 'Queensland' );
            String output8 = NATLogicEngine.doLogic(tOrgCt2, nde8, 'Queensland' );
            String output9 = NATLogicEngine.doLogic(tOrgCt2, nde9, 'Queensland' );
            String output10 = NATLogicEngine.doLogic(tOrgCt2, nde10, 'Queensland' );
            
        }
    
    @isTest public static void testAccountObject() {
        
        /*          
        
            if (definition.fieldName == 'Proficiency_in_Spoken_English_Identifier__c' ) {
                
                if(st.Main_Language_Spoken_at_Home_Identifier__c != null ){ 
                        output += NATGeneratorFormat.formatString( String.valueOf(st.Proficiency_in_Spoken_English_Identifier__c) , definition.fieldLength );
                
            }
            
            if (definition.fieldName == 'Address_Road_Suffix_Identifier__c' ) {
                if(st.Address_Road_Suffix_Identifier__c != null) {

            
             if (definition.fieldName == 'Address_Road_Type_Identifier__c' ) {
                if(st.Address_Road_Type_Identifier__c != null) {
     
             if (definition.fieldName == 'Address_Dwelling_Type_Identifier__c' ) {
            
            if (definition.fieldName == 'Postal_Road_Suffix_Identifier__c' ) {

            }
             if (definition.fieldName == 'Postal_Road_Type_Identifier__c' ) {

            
             if (definition.fieldName == 'Postal_Dwelling_Type_Identifier__c' ) {
       

            if (definition.fieldName == 'EnglishMainLanguageFlag' ) {
                if (st.Main_Language_Spoken_at_Home_Identifier__c == '1201' ) {
               
           
            if (definition.fieldName == 'Prior_Achievement_Type_s__c' ) {

                         output += NATGeneratorFormat.formatString( st.Student_Identifer__c , 10 );                        
      
            if (definition.fieldName == 'Prior_Education_Achievement_Recognition__c') {

            
            if (definition.fieldName == 'Disability_Type__c') {
               

            if (definition.fieldName == 'State_Identifier__c' ) {
              

            if (state == 'Victoria') {
                if(definition.fieldName == 'Client_Industry_of_Employment__r.Division__c') {
                    String inField;
                    if(st.getSobject('Client_Industry_of_Employment__r') != null ){
                        inField = (String) st.getSobject('Client_Industry_of_Employment__r').get('Division__c');
                    }
                    else{
                        inField = '';
                    }
                output =  output = NATGeneratorFormat.formatString( inField , definition.fieldLength );     
            }
    
            
                if(definition.fieldName == 'Proficiency_in_Spoken_English_Identifier__c') {   
                    if (st.Main_Language_Spoken_at_Home_Identifier__c != null) {
                        Boolean isMatch = false;
                        List<NAT00080_English_Languages__c> nat80el = NAT00080_English_Languages__c.getall().values();            
                        for(NAT00080_English_Languages__c n : nat80el) {
                            if (st.Main_Language_Spoken_at_Home_Identifier__c == n.Code__c) {
                                output = NATGeneratorFormat.formatString( '' , definition.fieldLength );
                                isMatch = true;
                            }                                       
                        }            
                        if (!isMatch) {
                            output = NATGeneratorFormat.formatString( String.valueOf(st.get( definition.fieldName )) , definition.fieldLength );
                        }
                    }
                }
        }
             
         if (definition.fieldName == 'Postal_building_property_name__c' ) {
             if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                    output += NATGeneratorFormat.formatString( st.Address_building_property_name__c , definition.fieldLength );
             }
             if (st.Postal_street_number__c != null && st.Postal_street_name__c != null) {
                output += NATGeneratorFormat.formatString( st.Postal_building_property_name__c , definition.fieldLength ); 
             }
         }
            
         if (definition.fieldName == 'Postal_flat_unit_details__c' ) {
             if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                    output += NATGeneratorFormat.formatString( st.Address_flat_unit_details__c , definition.fieldLength );
             }
             if (st.Postal_street_number__c != null && st.Postal_street_name__c != null) {
                output += NATGeneratorFormat.formatString( st.Postal_flat_unit_details__c , definition.fieldLength ); 
             }
         }
            
         if (definition.fieldName == 'Postal_street_number__c' ) {
             if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                    output += NATGeneratorFormat.formatString( st.Address_street_number__c , definition.fieldLength );
                }
             if (st.Postal_street_number__c != null && st.Postal_street_name__c != null) {
                output += NATGeneratorFormat.formatString( st.Postal_street_number__c , definition.fieldLength ); 
             }
		}

         if (definition.fieldName == 'Postal_street_name__c' ) {
             if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                    output += NATGeneratorFormat.formatString( st.Address_street_name__c , definition.fieldLength );
             }
             if (st.Postal_street_number__c != null && st.Postal_street_name__c != null) {
                output += NATGeneratorFormat.formatString( st.Postal_street_name__c , definition.fieldLength ); 
             }
		}
		
		if (definition.fieldName == 'Postal_suburb_locality_or_town__c' ) {
            if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                    output += NATGeneratorFormat.formatString( st.Suburb_locality_or_town__c , definition.fieldLength );
            }
             if (st.Postal_street_number__c != null && st.Postal_street_name__c != null) {
                output += NATGeneratorFormat.formatString( st.Postal_suburb_locality_or_town__c , definition.fieldLength ); 
             }
		}

		if (definition.fieldName == 'PersonOtherPostalCode' ) {
            if (st.Postal_street_number__c == null && st.Postal_street_name__c == null) {
                if(st.Address_Post_Code__c != null) {
                    output += NATGeneratorFormat.formatString( st.Address_Post_Code__c , definition.fieldLength );
                }
                else{
                    output += NATGeneratorFormat.formatString( '@@@@', definition.fieldLength );
                }
            }
            else {
                if(st.PersonOtherPostalCode != null) {
                    output += NATGeneratorFormat.formatString( st.PersonOtherPostalCode , definition.fieldLength ); 
                }
                else{
                    output += NATGeneratorFormat.formatString( '@@@@', definition.fieldLength );
                }
             }
        }
            
        }

        */
        
        
    }
    
    @isTest public static void testEnrolment() {
        
         Account stu = NATTestDataFactory.createStudent();
         Country__c ctry = NATTestDataFactory.createCountry();
         ANZSCO__c anz = NATTestDataFactory.createANZSCO();
         Unit__c unt = NATTestDataFactory.createUnit();
         Account tOrg = NATTestDataFactory.createTrainingOrganisation();
         Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();

         State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
         Results__c res = NATTestDataFactory.createResult(sta.Id);
         Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
         Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
         Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
        
	 	Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);   
     	Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
    	Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
     	Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
     	Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
     	Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
     	Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
	 	Enrolment__c enrl = NATTestDataFactory.createEnrolment(stu.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
        //    public static Awards__c createAward(Id enrolment){

        
        Enrolment__c enrl1 = [ SELECT ID, Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c FROM Enrolment__c WHERE ID = :enrl.Id];

        NATDataElement nde1 = new NATDataElement('IssuedFlag', 20, true, 'string'); 
        NATDataElement nde2 = new NATDataElement('YearProgramCompleted', 20, true, 'string'); 
        NATDataElement nde3 = new NATDataElement('Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c', 20, true, 'string'); 

        String output1 = NATLogicEngine.doLogic(enrl1, nde1, 'Queensland' );
        String output2 = NATLogicEngine.doLogic(enrl1, nde2, 'Queensland' );
        String output3 = NATLogicEngine.doLogic(enrl1, nde3, 'Queensland' );

        
    }
    
    @isTest public static void testAward() {
        
         Account stu = NATTestDataFactory.createStudent();
         Country__c ctry = NATTestDataFactory.createCountry();
         ANZSCO__c anz = NATTestDataFactory.createANZSCO();
         Unit__c unt = NATTestDataFactory.createUnit();
         Account tOrg = NATTestDataFactory.createTrainingOrganisation();
         Field_of_Education__c foe = NATTestDataFactory.createFieldOfEducation();

         State__c sta = NATTestDataFactory.createStateQLD(ctry.Id);
         Results__c res = NATTestDataFactory.createResult(sta.Id);
         Location_Loadings__c lload = NATTestDataFactory.createLocationLoadings(sta.Id);
         Locations__c ploc = NATTestDataFactory.createParentLocation(ctry.Id, sta.Id, lload.Id);
         Locations__c loc = NATTestDataFactory.createLocation(ctry.Id, ploc.Id, tOrg.Id);
        
	 	Funding_Source__c fsrc = NATTestDataFactory.createFundingSource(sta.Id);   
     	Qualification__c qual = NATTestDataFactory.createQualification(anz.Id, foe.Id);
    	Qualification_Unit__c qualu = NATTestDataFactory.createQualificationUnit(qual.Id, unt.Id);
     	Purchasing_Contracts__c pcon = NATTestDataFactory.createPurchasingContractWithTOState(fsrc.Id, torg.Id, sta.Id);  
     	Contract_Line_Items__c cli = NATTestDataFactory.createContractLineItem(pcon.id);
     	Line_Item_Qualifications__c liq = NATTestDataFactory.createLineItemQualificationFS(qual.Id, cli.Id, fsrc.id);
     	Line_Item_Qualification_Units__c liqu = NATTestDataFactory.createLineItemQualificationUnit(liq.Id, qualu.Id);
	 	Enrolment__c enrl = NATTestDataFactory.createEnrolment(stu.Id, qual.Id, liq.Id, loc.Id, ctry.Id);
        
        Awards__c awrd = NATTestDataFactory.createAward(enrl.Id);
        
        
        /*
  			'Enrolment__r.Parent_Qualification_Code__c'
            'Enrolment__r.Student_Identifier__c'      
            'Enroment__r.Parent_Qualification_Code__c'
            'Enrolment__r.Training_Organisation_Identifier__c'
            'Enrolment__r.Victorian_Commencement_Date__c'
            'Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c'
         */
        
		Awards__c awrd1 = [ SELECT ID, Enrolment__r.Parent_Qualification_Code__c, Enrolment__r.Student_Identifier__c,
            Enrolment__r.Training_Organisation_Identifier__c, Enrolment__r.Victorian_Commencement_Date__c,
            Enrolment__r.Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c FROM Awards__c WHERE ID =: awrd.Id ];
        
        
        NATDataElement nde1 = new NATDataElement('Enrolment__r.Parent_Qualification_Code__c', 20, true, 'string'); 
        NATDataElement nde2 = new NATDataElement('Enrolment__r.Student_Identifier__c', 20, true, 'string'); 
        NATDataElement nde3 = new NATDataElement('Enrolment__r.Training_Organisation_Identifier__c', 20, true, 'string'); 
        NATDataElement nde4 = new NATDataElement('Enrolment__r.Victorian_Commencement_Date__c', 20, true, 'string'); 
        NATDataElement nde5 = new NATDataElement('Delivery_Location__r.Training_Organisation__r.National_Provider_Number__c', 20, true, 'string'); 

        String output1 = NATLogicEngine.doLogic(awrd1, nde1, 'Queensland' );
        String output2 = NATLogicEngine.doLogic(awrd1, nde2, 'Queensland' );
        String output3 = NATLogicEngine.doLogic(awrd1, nde3, 'Queensland' );
        String output4 = NATLogicEngine.doLogic(awrd1, nde4, 'Queensland' );
        String output5 = NATLogicEngine.doLogic(awrd1, nde5, 'Queensland' );

        
    }
    
    
    
    



}