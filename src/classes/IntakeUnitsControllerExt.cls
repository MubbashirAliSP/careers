public without sharing class IntakeUnitsControllerExt {
    
    public final Intake__c intake;
    
    public String SelectedUnit { get; set; }
    public String SelectedSearch { get; set; }
    public Boolean pRender{get;set;}
    public Boolean nRender{get;set;}
    public String pageNo{get;set;}
    public Integer recordCount{get;set;}
    public Integer pages{get;set;}
    private Integer offsetVal;
    public Boolean error {get;set;}
    public String searchText { get; set; }
    
    public String ref{get;set;}
    
    private Integer currPage{get;set;}
    public void previous(){
        
        
        
        currPage --;
        pageNo = 'Page ' + currPage;
        displayTable(currPage);
        
    }
    
    private void displayTable(Integer pNum){
        pageNo = 'Page ' + pNum + ' of ' + pages;
        queryList = mapPages.get(pNum);
        if((pNum == 1 && pages == 1)||(pages < 1)){
            pRender = false;
            nRender = false;
            if(pages < 1) pageNo = '';
        }
        else {
        if(pNum <= 1){ 
            pRender = false;
            nRender = true;
        } else {
            pRender = true;
        }     
        if(pNum >= pages) { 
            nRender = false;
            pRender = true;
        }
        }  
    }
    
    
    public void next(){
        
        
        currPage ++;
        pageNo = 'Page ' + currPage;
        displayTable(currPage);
            
    }
     
    
    public Transient List<Qualification_Unit__c> queryResult { get; set; }
    public Transient List<Training_Package_Unit__c> queryResult2 { get; set; }
    public List<queryResultSet> queryList { get; set; }
    public Map<Integer, List<queryResultSet>> mapPages ;
    
    
    
    public IntakeUnitsControllerExt(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest())
            stdController.addFields(dynamicFields);
            this.intake = (Intake__c)stdController.getRecord();   
            //jcanlas 26MAR2013 start - instantiate queryList
            queryList = new List<queryResultSet>();  
            //jcanlas 26MAR2013 end
    }
    
    public List<String> dynamicFields {
        get {
            if (dynamicFields == null) {
                dynamicFields = new List<String>();
                dynamicFields.add('Qualification__c');
                
            }
            return dynamicFields ;
        }
        private set;
    }
    
    
    public pageReference search() {
        
        ref='';
        if((searchText.length() <5) && (SelectedSearch == 'Unit Name')){
            ref = 'Please enter at least 5 characters for Unit Name';
        } 
        else if(searchText.length() <2){
            ref = 'Please enter at least 2 characters for Unit Code';
        }
        else {
            
        //String tempQualif = intake.Qualification__c;
        Id QualId = intake.Qualification__c;
        transient String strLikeVal = '%' + searchText + '%';
        transient String qryString;
        transient String UOCRecType = 'Unit Of Competency'; //jcanlas 21MAR2013 change to 'Competancy' 26MAR2013 change back to Competency
        if(SelectedSearch == 'Unit Name'){
            if(SelectedUnit == 'All'){
                
                qryString = 'SELECT stage__c,Stage__r.Name,Unit_Type__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit__r.Unit_Name__c like : strLikeVal and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
            else if(SelectedUnit == 'None'){
                qryString = 'SELECT stage__c,Stage__r.Name,Unit_Type__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit_Type__c = null and Unit__r.Unit_Name__c like : strLikeVal and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
            else {
                qryString = 'SELECT stage__c,Stage__r.Name,Unit_Type__c, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit_Type__c = : SelectedUnit and Unit__r.Unit_Name__c like : strLikeVal  and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
        }
        else {
            if(SelectedUnit == 'All'){
                
                qryString = 'SELECT stage__c,Unit_Type__c,Stage__r.Name, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit__r.Name like : strLikeVal and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
            else if(SelectedUnit == 'None'){
                qryString = 'SELECT stage__c,Unit_Type__c,Stage__r.Name, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit_Type__c = null AND Unit__r.Name like : strLikeVal and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
            else {
                qryString = 'SELECT stage__c,Unit_Type__c,Stage__r.Name, Unit__r.Unit_Name__c, Unit__r.Name, Name FROM Qualification_Unit__c where Qualification__c  = :QualId AND Unit_Type__c = : SelectedUnit and Unit__r.Name like : strLikeVal and Unit_Record_Type__c = : UOCRecType order by Unit__r.Name';
            }
        } 
        system.debug('***QualId'+QualId);
        system.debug('***strLikeVal'+strLikeVal);
        system.debug('***UOCRecType'+UOCRecType);
        queryResult = Database.query(qryString) ;
        queryList = new List<queryResultSet>();
        system.debug('***qryString'+qryString);
        system.debug('***queryList'+queryList);
        for(Qualification_Unit__c qu: queryResult){
            queryList.add(new queryResultSet(false,qu.Unit__r.Name, qu.Unit__r.Unit_Name__c, qu.Unit__r.Id, qu.Stage__c, qu.Unit_Type__c,qu.Stage__r.Name));
        }
        
                
        List<queryResultSet> tempList = queryList;
        recordCount = tempList.size();
       
        offsetVal=0;
        pages = recordCount/20;
        
        if (Math.mod(recordCount,20)>0) {
                pages++;
                
        }
        
        
        
        mapPages = new Map<Integer, List<queryResultSet>>();
        //offsetVal = 20;
        
        Integer numRec = 20;
        for(Integer i = 1; i<=pages; i++){
            if(i == pages) { 
                if(Math.mod(recordCount,20) > 0)
                numRec = Math.mod(recordCount,20);
                else
                numRec = 20;
            }
            List<queryResultSet> qr = new List<queryResultSet>();
            
                for(Integer j = offsetVal; j < offsetVal + numRec; j++){
                    
                    qr.add(tempList[j]);
                }
            
            
            mapPages.put(i, qr);
            offsetVal += 20;
        }
        
        currPage = 1;
        displayTable(currPage);
        }
        return null; 
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Code','Code'));
            options.add(new SelectOption('Unit Name','Unit Name'));
            return options;
    }
    public List<SelectOption> getItems2() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All','All'));            
            options.add(new SelectOption('Core','Core'));
            options.add(new SelectOption('Elective','Elective'));
            options.add(new SelectOption('None','None'));
            return options;
    }
    
    
    
    public PageReference getSelected()
    {
       
        return null;
    }
    
    public pageReference addIntakeUnit(){
        /*Additional functionality added to look to units of study and add them as intake unit of study 
         * 02/01/13 by YG
        */
        error = false;
        List<Intake_Unit__c> IntakeUnits = new List<Intake_Unit__c>();
        
        List<Intake_Unit_of_Study__c> IntakeUnitsOfStudy = new List<Intake_Unit_of_Study__c>();
        
        Set<Id>UOCIds = new Set<Id>();
        system.debug('queryList***'+queryList);
        if(queryList.size() > 0){
            //Insert Intake Units
            for(queryResultSet qa : queryList) {
                
                    if(qa.selected == true) {
                        
                    UOCIds.add(qa.sfid);
                
                    Intake_Unit__c IU = new Intake_Unit__c(Intake__c = intake.id,
                                                            Unit_of_Study__c = qa.sfid,
                                                            Stage__c = qa.Stage,
                                                            Unit_Type__c = qa.UnitType);
                    
                    
                    IntakeUnits.add(IU);
                    }
                                                            
                    
                
            }
            system.debug('IntakeUnits***'+IntakeUnits);
            if(IntakeUnits.size() > 0) {
                insert IntakeUnits;
                            
                //insert Intake Units Of Study
                
                for(Unit__c units : [Select id,Parent_UoC__c FROM Unit__c WHERE Parent_UoC__c in : UOCIds ]) {
                    
                    for(Intake_Unit__c iuc : IntakeUnits) {
                        
                        if(units.Parent_UoC__c == iuc.Unit_of_Study__c) {
                            Intake_Unit_of_Study__c IUS = new Intake_Unit_of_Study__c(Intake_Unit_of_Competency__c = iuc.id, Unit__c = units.id );
                            IntakeUnitsOfStudy.add(IUS);
                        }
                        
                    
                    }
                    
                }
                
                
               insert IntakeUnitsOfStudy;
                
                
            }
            return null;
        }
        //jcanlas 26MAR2013 - add error handler if no unit chosen start
        else{
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select a Unit.'));
            return null;
        }
        //jcanlas 26MAR2013 end
    }


    public class queryResultSet
{
    public String cd {get;set;}
    public String un {get;set;}
    public String sfid {get;set;}
    public Boolean selected {get;set;}
    public String Stage {get;set;}
    public String UnitType {get;set;}
    public String StageName {get;set;}
    public queryResultSet(Boolean sel,String a, String b, Id c, string s, String ut, String sn)
    {
        selected = sel;
        cd = a;
        un = b;
        sfid = c;
        Stage = s;
        UnitType = ut;
        StageName = sn;
    }
}

}