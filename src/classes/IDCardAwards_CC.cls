public with sharing class IDCardAwards_CC {
	
	private ApexPages.StandardController controller {get; set;}
	private ID_Card__c idCard;
	public ID_Card__c idCardRecord = new ID_Card__c();
	public List<Awards__c> awardList {get;set;}
	public List<pageWrapper> pageWrapperList {get;set;}
	public string awardId {get;set;}
	public ID_Card__c idCardRec {get;set;}
	public IDCardAwards_CC(ApexPages.StandardController controller) {
        
        //initialize the stanrdard controller
        this.controller = controller;
        this.idCard = (ID_Card__c)controller.getRecord();
        idCardRecord = [select Id, Student__c, Award__c from ID_Card__c where Id =: idCard.Id];
 		fillPageWrapper();
 		awardId = idCardRecord.Award__c;
 		idCardRec = idCardRecord;
    }
	public void fillPageWrapper(){
		pageWrapperList = new List<pageWrapper>();
		awardList = [select Id, Name, Enrolment__r.Name,Enrolment__r.Qualification__r.Qualification_Name__c,Enrolment__r.Qualification__r.Name from Awards__c Where Enrolment__r.Student__c =: idCardRecord.Student__c];
		//Boolean checkRadio = false;
		for(Awards__c a : awardList){
			system.debug('a.Id***'+ a.Id);
			system.debug('idCardRecord.Award__c***'+ idCardRecord.Award__c);    
			if(a.Id == idCardRecord.Award__c){
				pageWrapperList.add(new pageWrapper(a,true));
			}
			else{
				pageWrapperList.add(new pageWrapper(a,false));
			}
			
		}
		system.debug('pageWrapperList***'+ pageWrapperList);  
	}	 
    public PageReference updateIDCard(){
    	PageReference p = new PageReference('/'+idCardRecord.Id);
    	p.setRedirect(true);
    	idCardRecord.Award__c = awardId;
    	try{
    		update idCardRecord;
    		return null;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'ERROR: ' + e.getMessage()));return null;
    	}
    	
    }
    public class pageWrapper{
    	public Awards__c award {get;set;}
    	public boolean radio {get;set;}
    	public pageWrapper(Awards__c award, Boolean radio){
    		this.award = award;
    		this.radio = radio;
    	}
    }
}