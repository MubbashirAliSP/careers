public with sharing class NATValidateOutCome {
	
	public List<SObject>dynamicRecords {get;set;}
	public Integer fileCount {get;set;}
	public String filename {get;set;}
	public Integer CPUTimeInMilliseconds {get;set;}
	
	public NatValidateOutcome(String fname, List<SObject>records, Integer Count, Integer CPUtime) {
		
		dynamicRecords = records;
		fileCount = Count;
		filename = fname;
		CPUTimeInMilliseconds = CPUTime;
	}

}