/**
 * @description Custom inbound email handler for creating task records to store email sends that are done via workflow email alerts
 * @author Ranyel Maliwanag
 * @date 07.DEC.2015
 */
global without sharing class SASInboundEmailHandler implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		// Variable initialisations
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

		// @TODO: Handler logic
		for (Messaging.InboundEmail.Header header : email.headers) {
			System.debug(header.name + ' ::: ' + header.value);
		}

		String sasMeta = email.htmlBody.substringAfter('meta name="sasmeta"').substringBefore('/>');
		if (String.isBlank(sasMeta)) {
			sasMeta = email.htmlBody.substringAfter('data-application="sasmeta"').substringBefore('/>');
		}
		String parentId = sasMeta.substringAfter('data-parentId="').substringBefore('"');
		String emailTemplate = sasMeta.substringAfter('data-templateName="').substringBefore('"');
		String contactId = sasMeta.substringAfter('data-contactId="').substringBefore('"');

		System.debug('email ::: \n' + email.htmlBody + '\n\n\n\n\n');
		System.debug('meta ::: ' + sasMeta);
		System.debug(parentId + ' ::: ' + emailTemplate + ' ::: ' + contactId);

		Task emailSend = new Task(
				WhatId = parentId,
				Status = 'Completed',
				Subject = '[Automatic Email Sent] ' +  emailTemplate
			);
		if (String.isNotBlank(contactId)) {
			emailSend.WhoId = contactId;
		}
		insert emailSend;
		
		List<Attachment> taskAttachments = new List<Attachment>();
		Attachment emailAtt = new Attachment();
		emailAtt.Name = 'Email.html';
		emailAtt.Body = Blob.valueOf(email.htmlBody);
		emailAtt.ParentId = emailSend.Id;
		taskAttachments.add(emailAtt);

		if (email.binaryAttachments != null) {
			for (Messaging.InboundEmail.BinaryAttachment emailFile : email.binaryAttachments) {
				Attachment fileAttachment = new Attachment();
				fileAttachment.Name = emailFile.fileName;
				fileAttachment.Body = emailFile.body;
				fileAttachment.ParentId = emailSend.Id;
				taskAttachments.add(fileAttachment);
			}
		}
		insert taskAttachments;

		return result;
	}
}