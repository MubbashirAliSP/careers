/*Need an ability to update award, so without sharing is used */
/*
* History
* Juan Miguel de Guia 7/28/2015 - Added active__c on SOQL part on line 35
* Juan Miguel de Guia 7/29/2015 - Commented out the if statement on line 70 and 75
*/
public without sharing class AwardTemplatePage_CC {
  public Id awardId;
  public List<pageWrapper> pageWrapperList {get;set;}
  public String templateId {get;set;}
  public String typeOutput {get;set;}
  public Map<Integer,Id> MapAwardTemplates {get; set;}
  private final String templateType = ApexPages.currentPage().getParameters().get('type');
  
  public AwardTemplatePage_CC(){
    awardId = ApexPages.currentPage().getParameters().get('awardId');
    fillPageWrapper();
    
    if(templateType == 'award')
      typeOutput = 'Award';
    else
      typeOutput = 'Academic Transcript';
    
  }
  
  public Awards__c getAwardRecord(){
    
    Awards__c award = new Awards__c();
    award = [select Id, Name, Award_Template_ID__c, Enrolment__r.Qualification__r.Internal_Qualification_Reference__c from Awards__c where Id =: awardId];
    return award;
  }
  
  public List<APXTConga4__Conga_Template__c> getAwardTemplates(){
    List<APXTConga4__Conga_Template__c> congaTemplate = new List<APXTConga4__Conga_Template__c>();
    
    for(APXTConga4__Conga_Template__c t : [select APXTConga4__Name__c, Id, APXTConga4__Template_Type__c, APXTConga4__Description__c,  AwardTemplateId__c, Active__c
              from APXTConga4__Conga_Template__c
              where APXTConga4__Template_Type__c  in  ('Award','Academic Transcript')
              ORDER BY APXTConga4__Name__c, CreatedDate ASC] ) {
    
      
    
      if(templateType == 'award') {
        if(t.APXTConga4__Template_Type__c == 'Award')
          congaTemplate.add(t);
          
      }
      else
        if(t.APXTConga4__Template_Type__c == 'Academic Transcript') {
          congaTemplate.add(t);
          
        }
      
    
    }
    
    return congaTemplate;
  }
  //code filling up page table
    public void fillPageWrapper(){
        pageWrapperList = new List<pageWrapper>();
        List<APXTConga4__Conga_Template__c> templateList = new List<APXTConga4__Conga_Template__c>();
        templateList = getAwardTemplates();
        Set<String> sAwardTemplateName = new Set<String>();
        MapAwardTemplates = new  Map<Integer,Id>();
        
        for(APXTConga4__Conga_Template__c t : templateList){
            //Do not Append to List if Template has a counterpart AwardTemplateId__c with a different number
            if(t.AwardTemplateId__c != null){
                //if(math.mod(Integer.ValueOf(t.AwardTemplateId__c), 2) == 1){
                    pageWrapper w = new pageWrapper(t);
                    pageWrapperList.add(w);
                    sAwardTemplateName.add(t.APXTConga4__Name__c);
                    
                //}
                MapAwardTemplates.put(Integer.valueOf(t.AwardTemplateId__c), t.id);
            }
            else{
                pageWrapper w = new pageWrapper(t);
                pageWrapperList.add(w);
                sAwardTemplateName.add(t.APXTConga4__Name__c);
            }
        }
        system.debug('pageWrapperList'+pageWrapperList);
    }
    //page wrapper
    public class pageWrapper{
        public APXTConga4__Conga_Template__c template {get;set;}
        
        public pageWrapper(APXTConga4__Conga_Template__c template){
            this.template = template;
        }
    }
    
    //copy Award template Id to Award template record
    public PageReference save(){
      
      system.debug('****templateId'+templateId);
      String strtemplateId = templateId;
      PageReference p = new PageReference('/'+awardId);
      p.setRedirect(true);
      Awards__c awardRec = getAwardRecord();
      
      if(templateId == ''){
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Please select a template.'));
            return null;
      }
      else{
        for(PageWrapper pw : pageWrapperList){
          if(templateType == 'award')
          {
            System.debug('pw.template.id :' + pw.template.id );
            System.debug('##strtemplateId: ' + strtemplateId);
            if(pw.template.id == strtemplateId){
                //Check if there is AwardTemplateId__c: assumes that the record is UNIQUE if field EMPTY
                if(pw.template.AwardTemplateId__c != null){
                     //Check for the Qualification__r.Internal_Qualification_Reference__c
                    if(String.IsNotEmpty(awardRec.Enrolment__r.Qualification__r.Internal_Qualification_Reference__c)){
                        //Values divisible by 2 with remainders are the templates with brackets
                        strtemplateId = MapAwardTemplates.get(Integer.valueOf(pw.template.AwardTemplateId__c)+ 1);
                    }
                    else{
                        //Values divisible by 2 without remainders are the templates without brackets
                        strtemplateId = MapAwardTemplates.get(Integer.valueOf(pw.template.AwardTemplateId__c)); 
                    }
                    
                }
                awardRec.Award_Template_ID__c = strtemplateId;
            }
            
          }
          else{
            System.debug('##strtemplateId else: ' + strtemplateId);
            awardRec.Academic_Transcript_Template_ID__c = strtemplateId;
          }
        }
        try{
          System.debug('##awardRec: ' + awardRec);
          update awardRec;
          return p;}catch(Exception e){ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));return null;}
      }
    }
    //cancel and go back to Award record
    public PageReference cancel(){
      PageReference p = new PageReference('/'+awardId);
      p.setRedirect(true);
      return p;
    }
}