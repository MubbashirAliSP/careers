/**
 * @description Test class for CensusInvoiceGenerationBatch class
 * @author Ranyel Maliwanag
 * @date 04.NOV.2015
 */
@isTest
public with sharing class CensusInvoiceGenerationBatch_Test {
    @testSetup
    static void batchSetup() {
        ApplicationsPortalCentralSettings__c portalSettings = SASTestUtilities.createApplicationsPortalSettings();
        portalSettings.InvoicingOffset__c = 14;
        insert portalSettings;

        Census__c census = SASTestUtilities.createCensus();
        census.CensusDate__c = Date.today().addDays(Integer.valueOf(portalSettings.InvoicingOffset__c));
        insert census;
    }

    static testMethod void testGeneration() {
        ApplicationsPortalCentralSettings__c portalSettings = ApplicationsPortalCentralSettings__c.getInstance();
        String queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Enrolment__c, ', RecordType.Name');
        Enrolment__c enrolment = Database.query(queryString);

        System.assertEquals('Dual Funding Enrolment', enrolment.RecordType.Name);
        System.assert(enrolment.Enrolment_Status__c.startsWith('Active'));

        queryString = EnrolmentsSiteUtility.getQueryString(Schema.SObjectType.Census__c);
        Census__c census = Database.query(queryString);

        System.assertEquals(Date.today().addDays(Integer.valueOf(portalSettings.InvoicingOffset__c)), census.CensusDate__c);
        System.assert(!census.IsInvoiceGenerated__c);
        
        Test.startTest();
            CensusInvoiceGenerationBatch nextBatch = new CensusInvoiceGenerationBatch();
            Database.executeBatch(nextBatch);
        Test.stopTest();
    }
}