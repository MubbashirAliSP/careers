/**
 *Test Class for HEIMS controller and Generator
 */
@isTest(SeeAllData=true)
private class HEIMS_test {
    

    /*static testMethod void myUnitTest() {
    
        date startDate =  date.newInstance(2013, 5, 1);
        date endDate =  date.newInstance(2013, 6, 30);
        
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c(); 
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        
            
        Test.startTest();
        
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            state = TestCoverageUtilityClass.createState(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
            qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() + 365;
            enrl.Delivery_Location__c = loc.id;
            enrl.Enrolment_Status__c = 'Completed';
           
            insert enrl;
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            eus.Census_Date__c = system.today();
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;
            
        
            PageReference testPage = Page.HEIMSGeneration;
            Test.setCurrentPage(testPage);
            
            HEIMSController controller = new HEIMSController();
            
            controller.CancelButton();
            
            controller.backButton();
            
            controller.rto = acc.id;
            
            controller.NextButton1();
            
            controller.display = 'VCC/VCU';
            
            
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
            
            controller.exportFile();
            
            controller.display = 'CHESSN'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
           
            controller.exportFile();
            
            
            controller.display = 'VDU'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();   
           
            controller.exportFile();
            
            controller.display = 'VSR'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
           
            controller.exportFile();            
            
            controller.display = 'VER'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
           
            controller.exportFile();
            
            controller.display = 'VLL/VEN'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
            
            controller.nextEnrolmentUnitOfStudy();
            
            controller.previousEnrolmentUnitOfStudy();
            
            controller.nextEnrolment();
            
            controller.previousEnrolment();
           
            controller.exportFile();
            
        Test.stopTest();
    }*/
    
    static testMethod void myUnitTest1() {
        
        List<Enrolment__c> erlList = new List<Enrolment__c>();
    
    date startDate =  date.newInstance(2013, 6, 9);
    date endDate =  date.newInstance(2013, 6, 15);
        
        Account acc = new Account();
        Account accTraining = new Account();
        ANZSCO__c anz = new ANZSCO__c();
        Field_of_Education__c fld = new Field_of_Education__c();
        Qualification__c qual = new Qualification__c();
        Qualification__c qual2 = new Qualification__c();
        Unit__c unit = new Unit__c();
        Location_Loadings__c loading = new Location_Loadings__c(); 
        Qualification_Unit__c qunit = new Qualification_Unit__c();
        Qualification_Unit__c qunit2 = new Qualification_Unit__c();
        Enrolment__c enrl = new Enrolment__c();
        Purchasing_Contracts__c pCon = new Purchasing_Contracts__c();
        Funding_Source__c fSource = new Funding_Source__c();
        Contract_Line_Items__c conLine= new Contract_Line_Items__c ();
        Line_Item_Qualifications__c lineItem = new Line_Item_Qualifications__c ();
        Line_Item_Qualification_Units__c lineItemUnit = new Line_Item_Qualification_Units__c ();
        State__c state = new State__c();
        Country__c country = new Country__c();
        Country__c countryAU = new Country__c();
        Locations__c parentLoc = new Locations__c();
        Locations__c loc = new Locations__c();
        Locations__c loc2 = new Locations__c();
        Attendance_Types__c attend = new Attendance_Types__c();
        Enrolment_Unit__c eu = new Enrolment_Unit__c();
        Results__c res = new Results__c();
        Unit__c un = new Unit__c();
        Field_of_Education__c fe = new Field_of_Education__c();  
        Enrolment_Unit_of_Study__c eus = new Enrolment_Unit_of_Study__c();   
        Delivery_Mode_Types__c del = new Delivery_Mode_Types__c();
        
            
        
        
            acc = TestCoverageUtilityClass.createStudent();
            accTraining = TestCoverageUtilityClass.createTrainingOrganisation(); 
            anz = TestCoverageUtilityClass.createANZSCO();
            fld = TestCoverageUtilityClass.createFieldOfEducation();
            qual = TestCoverageUtilityClass.createQualification(anz.Id, fld.Id);
            unit = TestCoverageUtilityClass.createUnit();
            qunit = TestCoverageUtilityClass.createQualificationUnit(qual.Id, unit.Id);
            country = TestCoverageUtilityClass.createCountry();  
            state = TestCoverageUtilityClass.createState(country.Id);
            loading = TestCoverageUtilityClass.createLocationLoadings(state.Id);
            fSource = TestCoverageUtilityClass.createFundingSource(state.Id); 
            pCon = TestCoverageUtilityClass.createPurchasingContract(fSource.Id);
            conLine = TestCoverageUtilityClass.createContractLineItem(pCon.Id); 
            lineItem = TestCoverageUtilityClass.createLineItemQualification(qual.Id, conLine.Id);
            lineItemUnit = TestCoverageUtilityClass.createLineItemQualificationUnit(lineItem.id, qunit.id);
            parentLoc = TestCoverageUtilityClass.createParentLocation(country.id,state.id,loading.id);
            loc = TestCoverageUtilityClass.createLocation(country.Id, parentLoc.Id); 
            loc2 = TestCoverageUtilityClass.createLocationWithParentTR(country.Id, accTraining.Id, parentLoc.Id,state.id); 
            qual2 = TestCoverageUtilityClass.createTailoredQualificationAward(anz.Id, fld.Id,qual.id);
            del = TestCoverageUtilityClass.createDeliveryModeType();
            attend = TestCoverageUtilityClass.createAttendanceType();
            enrl = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
            enrl.Predominant_Delivery_Mode__c = del.Id; 
            enrl.Type_of_Attendance__c = attend.Id;
            enrl.Start_Date__c = system.today() - 365;
            enrl.End_Date__c = system.today() + 365;
            enrl.Delivery_Location__c = loc.id;
            //MAM 04/08/2014 Change Status to Active to pass Enrolment Validation start
            //enrl.Enrolment_Status__c = 'Completed';
            enrl.Enrolment_Status__c = 'Active (Commencement)'; 
            //MAM 04/08/2014 end
            
            insert enrl;
        	
        	for(Integer i = 0;i<200;i++)
            {
                Enrolment__c enrl1 = new Enrolment__c();
                enrl1 = TestCoverageUtilityClass.createEnrolment(acc.Id, qual2.Id, lineItem.Id, loc.Id, country.Id);
                erlList.add(enrl1);
            }
        	insert erlList;
            
            String query;
        	Set<ID> enrlID = new Set<ID>();
        	enrlID.add(enrl.id);
        	List<EnrolmentWrapper> dataList = new List<EnrolmentWrapper>();
        	List<EnrolmentWrapper> dataList1 = new List<EnrolmentWrapper>();
        	query ='Select id from Enrolment__c where Student__c =\''+acc.Id+'\'';
        	system.debug('queryyyyyyyyyyyyy'+query);
            EnrolmentIterable enrlment = new EnrolmentIterable(query,enrlID);
            enrlment.setPageSize = 100;
        	//enrlment.i=100;
        	dataList=enrlment.next();
        	dataList=enrlment.next();
            dataList1 = enrlment.previous();
            
            res = TestCoverageUtilityClass.createResult(state.Id);
            un = TestCoverageUtilityClass.createUnit();
            eu = TestCoverageUtilityClass.createEnrolmentUnit(un.Id, enrl.Id, res.Id, loc.Id, lineItemUnit.Id);
            eu.Start_Date__c = Date.TODAY() - 25;
            eu.End_Date__c = Date.TODAY() - 1;
            update eu;
            fe = TestCoverageUtilityClass.createFieldOfEducation();
            eus = TestCoverageUtilityClass.createEnrolmentUnitofStudy(eu.Id, qunit.Id, fe.Id);
            eus.Census_Date__c = system.today();
            insert eus;
            res = TestCoverageUtilityClass.createResult(state.Id);
        
            Contact c = new Contact(LastName = 'blah', accountId = accTraining.Id);
            
            insert c;
            
        
            PageReference testPage = Page.HEIMSGeneration;
            Test.setCurrentPage(testPage);
            
        Test.startTest();
            HEIMSController controller = new HEIMSController();
            
            controller.rto = acc.id;
            
            controller.CancelButton();
            
            controller.backButton();
            
            controller.NextButton1();
            
            controller.display = 'VLL/VEN'; 
             
            controller.dummyClaimNumber.Report_Start_Date__c  = startDate;
            controller.dummyClaimNumber.Report_End_Date__c = endDate;
            
            controller.NextButton2();
            
            controller.refreshData();
            
            controller.nextEnrolmentUnitOfStudy();
            
            controller.previousEnrolmentUnitOfStudy();
            
            controller.nextEnrolment();
        
            Boolean flag = controller.hasNextEnrolment;
        	Boolean flag1 =controller.hasPreviousEnrolment;
            //controller.previousEnrolment();
            
            //controller.previousEnrolment();
           
            controller.exportFile();
        Test.stopTest();
    
    }
}