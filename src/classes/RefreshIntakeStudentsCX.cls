/**
 * @description Custom controller extension for RefreshIntakeStudentsPage
 * @author Nick Guia
 * @date 7.AUG.2015
 */
public class RefreshIntakeStudentsCX {

	ApexPages.StandardSetController standardController;

	//constants
	private static final String DEFERRED_NOT_STATE_SUBSIDISED_CODE = '401';
	private static final String DEFERRED_RESTRICTED_ACCESS_ARRANGEMENT = '402';
	private static final String DEFERRED_ACT_GOVERNMENT = '410';
	private static final String DEFERRED_NSW_GOVERNMENT = '404';
	private static final String DEFERRED_NT_GOVERNMENT = '409';
	private static final String DEFERRED_QLD_GOVERNMENT = '405';
	private static final String DEFERRED_TAS_GOVERNMENT = '408';
	private static final String DEFERRED_VICTORIAN_GOVERNMENT = '403';
	private static final String DEFERRED_WA_GOVERNMENT = '407';
	private static final String DEFERRED_SOUTH_ASIAN_GOVERNMENT = '406';
	private static final String PAID_FULL_NON_STATE = '501';
	private static final String PAID_FULL_RESTRICTED_ACCESS = '502';
	private static final String PAID_FULL_ACT_GOVERNMENT = '510';
	private static final String PAID_FULL_NSW_GOVERNMENT = '504';
	private static final String PAID_FULL_NT_GOVERNMENT = '509';
	private static final String PAID_FULL_QLD_GOVERNMENT = '505';
	private static final String PAID_FULL_TAS_GOVERNMENT = '508';
	private static final String PAID_FULL_VICTORIAN_GOVERNMENT = '503';
	private static final String PAID_FULL_WA_GOVERNMENT = '507';
	private static final String PAID_FULL_SOUTH_AUSTRALIAN_GOVERNMENT = '506';
	private static final String ACT_CODE = 'ACT';
	private static final String NSW_CODE = 'NSW';
	private static final String NT_CODE = 'NT';
	private static final String QLD_CODE = 'QLD';
	private static final String TAS_CODE = 'TAS';
	private static final String VIC_CODE = 'VIC';
	private static final String WA_CODE = 'WA';
	private static final String SA_CODE = 'SA';

	/**
     * @description Class constructor
     * @param (ApexPages.StandardController) controller: The standard controller that will be extended by this class
     */
	public RefreshIntakeStudentsCX(ApexPages.StandardSetController standardSetController) {
		this.standardController = standardSetController;
	}

	public PageReference processStudents() {
		Map<Id, List<Intake_Students__c>> enrolmentIntakeStudentsMap = new Map<Id, List<Intake_Students__c>>();
		Map<Id, Id> enrolmentToQualificationMap = new Map<Id, Id>();
		Map<Id, String> enrolmentToStatusCodeMap = new Map<Id, String>();
		Map<Id, String> enrolmentToStateMap = new Map<Id, String>();
		List<Intake_Students__c> updateIntakeStudentsList = new List<Intake_Students__c>();
		List<Intake_Students__c> intakeStudentsList = new List<Intake_Students__c>();
		//collect intake student Ids
		Set<Id> isIds = new Map<Id,SObject> (standardController.getSelected()).keySet();
		for(Intake_Students__c is : [SELECT Enrolment__c, Enrolment__r.Qualification__c, Enrolment__r.Student_Status_Code__r.Code__c, 
											Enrolment__r.Delivery_Location__r.State_Short_Name__c FROM Intake_Students__c
											WHERE Id in :isIds])
		{
			enrolmentToQualificationMap.put(is.Enrolment__c, is.Enrolment__r.Qualification__c);
			enrolmentToStatusCodeMap.put(is.Enrolment__c, is.Enrolment__r.Student_Status_Code__r.Code__c);
			enrolmentToStateMap.put(is.Enrolment__c, is.Enrolment__r.Delivery_Location__r.State_Short_Name__c);

			if(!enrolmentIntakeStudentsMap.containsKey(is.Enrolment__c)) {
				enrolmentIntakeStudentsMap.put(is.Enrolment__c, new List<Intake_Students__c> {is});
			} else {
				enrolmentIntakeStudentsMap.get(is.Enrolment__c).add(is);
			}
		}

		//for(Intake_Students__c is : intakeStudentsList) {
		//	enrolmentToQualificationMap.put(is.Enrolment__c, is.Enrolment__r.Qualification__c);
		//	enrolmentToStatusCodeMap.put(is.Enrolment__c, is.Enrolment__r.Student_Status_Code__r.Code__c);
		//	enrolmentToStateMap.put(is.Enrolment__c, is.Enrolment__r.Delivery_Location__r.State_Short_Name__c);

		//	if(!enrolmentIntakeStudentsMap.containsKey(is.Enrolment__c)) {
		//		enrolmentIntakeStudentsMap.put(is.Enrolment__c, new List<Intake_Students__c> {is});
		//	} else {
		//		enrolmentIntakeStudentsMap.get(is.Enrolment__c).add(is);
		//	}
		//}

		//build map of qualification ID and qualification object
		Map<Id, Qualification__c> qualificationMap = new Map<Id, Qualification__c>([SELECT Id, 
			Course_Amount_ACT__c, Course_Amount_NSW__c, Course_Amount_NT__c, Course_Amount_QLD__c,
			Course_Amount_SA__c, Course_Amount_TAS__c, Course_Amount_VIC__c, Course_Amount_WA__c,
			S_Course_Amount_ACT__c, S_Course_Amount_NSW__c, S_Course_Amount_NT__c, S_Course_Amount_QLD__c,
			S_Course_Amount_SA__c, S_Course_Amount_TAS__c, S_Course_Amount_VIC__c, S_Course_Amount_WA__c,
			Government_Loan_Fees_ACT__c, Government_Loan_Fees_NSW__c, Government_Loan_Fees_NT__c, 
			Government_Loan_Fees_QLD__c, Government_Loan_Fees_SA__c, Government_Loan_Fees_TAS__c,
			Government_Loan_Fees_VIC__c, Government_Loan_Fees_WA__c FROM Qualification__c
			WHERE Id in :enrolmentToQualificationMap.values()]);

		for(Id enrolmentId : enrolmentIntakeStudentsMap.keySet()) {
			for(Intake_Students__c is : enrolmentIntakeStudentsMap.get(enrolmentId)) {
				//check status code
				String statusCode = enrolmentToStatusCodeMap.get(enrolmentId);
				String state = enrolmentToStateMap.get(enrolmentId);
				Qualification__c q = qualificationMap.get(enrolmentToQualificationMap.get(enrolmentId));

				System.debug('*** DEBUG - QUALIFICATION ID : ' + q.Id);
				System.debug('*** DEBUG - statusCode : ' + statusCode);
				System.debug('*** DEBUG - state : ' + state);

				if(statusCode != null && state != null && q != null) {
					if(statusCode == DEFERRED_NOT_STATE_SUBSIDISED_CODE ||
						statusCode == DEFERRED_RESTRICTED_ACCESS_ARRANGEMENT)
					{
						System.debug('*** CASE 1');
						//pull course fees from the Full Course amount
						//and the loan fee from the Government Loan Fee
						if(state == ACT_CODE) {
							is.Course_Amount__c = q.Course_Amount_ACT__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_ACT__c;
						} else if(state == NSW_CODE) {
							is.Course_Amount__c = q.Course_Amount_NSW__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_NSW__c;
						} else if(state == NT_CODE) {
							is.Course_Amount__c = q.Course_Amount_NT__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_NT__c;
						} else if(state == QLD_CODE) {
							is.Course_Amount__c = q.Course_Amount_QLD__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_QLD__c;
						} else if(state == SA_CODE) {
							is.Course_Amount__c = q.Course_Amount_SA__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_SA__c;
						} else if(state == TAS_CODE) {
							is.Course_Amount__c = q.Course_Amount_TAS__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_TAS__c;
						} else if(state == VIC_CODE) {
							is.Course_Amount__c = q.Course_Amount_VIC__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_VIC__c;
						} else if(state == WA_CODE) {
							is.Course_Amount__c = q.Course_Amount_WA__c;
							is.Loan_Amount__c = q.Government_Loan_Fees_WA__c;
						} else {
							System.debug('ERROR! Invalid State!');
						}
					} else if(statusCode == DEFERRED_ACT_GOVERNMENT || statusCode == DEFERRED_NSW_GOVERNMENT ||
						statusCode == DEFERRED_NT_GOVERNMENT || statusCode == DEFERRED_QLD_GOVERNMENT ||
						statusCode == DEFERRED_TAS_GOVERNMENT || statusCode == DEFERRED_VICTORIAN_GOVERNMENT ||
						statusCode == DEFERRED_WA_GOVERNMENT || statusCode == DEFERRED_SOUTH_ASIAN_GOVERNMENT) 
					{
						System.debug('*** CASE 2');
						//pull course fees from the Subsidised Course amount and 
						//the Loan fee and the Government loan fee populates as $0.00
						if(state == ACT_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_ACT__c;
							is.Loan_Amount__c = 0;
						} else if(state == NSW_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_NSW__c;
							is.Loan_Amount__c = 0;
						} else if(state == NT_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_NT__c;
							is.Loan_Amount__c = 0;
						} else if(state == QLD_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_QLD__c;
							is.Loan_Amount__c = 0;
						} else if(state == SA_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_SA__c;
							is.Loan_Amount__c = 0;
						} else if(state == TAS_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_TAS__c;
							is.Loan_Amount__c = 0;
						} else if(state == VIC_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_VIC__c;
							is.Loan_Amount__c = 0;
						} else if(state == WA_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_WA__c;
							is.Loan_Amount__c = 0;
						} else {
							System.debug('ERROR! Invalid State!');
						}
					} else if(statusCode == PAID_FULL_NON_STATE || 
						statusCode == PAID_FULL_RESTRICTED_ACCESS) 
					{
						System.debug('*** CASE 3');
						//pull course fees from the Full Course amount  
						//and the loan fee and the Government loan fee populates as $0.00
						if(state == ACT_CODE) {
							is.Course_Amount__c = q.Course_Amount_ACT__c;
							is.Loan_Amount__c = 0;
						} else if(state == NSW_CODE) {
							is.Course_Amount__c = q.Course_Amount_NSW__c;
							is.Loan_Amount__c = 0;
						} else if(state == NT_CODE) {
							is.Course_Amount__c = q.Course_Amount_NT__c;
							is.Loan_Amount__c = 0;
						} else if(state == QLD_CODE) {
							is.Course_Amount__c = q.Course_Amount_QLD__c;
							is.Loan_Amount__c = 0;
						} else if(state == SA_CODE) {
							is.Course_Amount__c = q.Course_Amount_SA__c;
							is.Loan_Amount__c = 0;
						} else if(state == TAS_CODE) {
							is.Course_Amount__c = q.Course_Amount_TAS__c;
							is.Loan_Amount__c = 0;
						} else if(state == VIC_CODE) {
							is.Course_Amount__c = q.Course_Amount_VIC__c;
							is.Loan_Amount__c = 0;
						} else if(state == WA_CODE) {
							is.Course_Amount__c = q.Course_Amount_WA__c;
							is.Loan_Amount__c = 0;
						} else {
							System.debug('ERROR! Invalid State!');
						}
					} else if(statusCode == PAID_FULL_ACT_GOVERNMENT ||
						statusCode == PAID_FULL_NSW_GOVERNMENT ||
						statusCode == PAID_FULL_NT_GOVERNMENT ||
						statusCode == PAID_FULL_QLD_GOVERNMENT ||
						statusCode == PAID_FULL_TAS_GOVERNMENT ||
						statusCode == PAID_FULL_VICTORIAN_GOVERNMENT ||
						statusCode == PAID_FULL_WA_GOVERNMENT ||
						statusCode == PAID_FULL_SOUTH_AUSTRALIAN_GOVERNMENT) 
					{
						System.debug('*** CASE 4');
						//pull course fees pull from the Subsidised Course amount  
						//and the loan fee and the Government loan fee populates as $0.00
						if(state == ACT_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_ACT__c;
							is.Loan_Amount__c = 0;
						} else if(state == NSW_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_NSW__c;
							is.Loan_Amount__c = 0;
						} else if(state == NT_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_NT__c;
							is.Loan_Amount__c = 0;
						} else if(state == QLD_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_QLD__c;
							is.Loan_Amount__c = 0;
						} else if(state == SA_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_SA__c;
							is.Loan_Amount__c = 0;
						} else if(state == TAS_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_TAS__c;
							is.Loan_Amount__c = 0;
						} else if(state == VIC_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_VIC__c;
							is.Loan_Amount__c = 0;
						} else if(state == WA_CODE) {
							is.Course_Amount__c = q.S_Course_Amount_WA__c;
							is.Loan_Amount__c = 0;
						} else {
							System.debug('ERROR! Invalid State!');
						}
					} else {
						System.debug('ERROR! Invalid Status Code!!');
					}
				} else {
					System.debug('ERROR! Please check if Enrolment has a state, student status code, or qualification.');
				}
				System.debug('*** IS ID : ' + is.Id);
				System.debug('*** IS Course_Amount__c : ' + is.Course_Amount__c);
				System.debug('*** IS Loan_Amount__c : ' + is.Loan_Amount__c);
				updateIntakeStudentsList.add(is);
			}
		}
		update updateIntakeStudentsList;
		//return to original page
		return standardController.save();
	}
}