/*This is the run future methods on person account */
public with sharing class PersonAccountHandlerFuture {
    
    Public Static boolean allow = true;
    
    public static void stopTrigger(){
         allow = false;
    }
 
    public static boolean canIRun(){
        return allow;
    }
    
    @future
    public static void insertStudent(Set<Id> personsIds) {
    
        List<RecordType> studentRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'];
        
        List<Student__c>newStudents = new List<Student__c>(); 
        
        LIST<Account>persons = [Select Id,
                                       firstname,
                                       lastname,
                                       Student_Identifer__c,
                                       Student_Id__c,
                                       PersonHomePhone,
                                       PersonMobilePhone,
                                       PersonBirthdate,
                                       PersonEmail,
                                       Salutation
                                       
                                FROM Account where id in : personsIds AND RecordTypeId =: studentRecordType[0].Id];
        
        
        for(Account p : persons) {
            
            Set<String>dupeIdsCheck = new Set<String>();
            
            if(p.Student_Identifer__c == null)
                p.Student_Identifer__c = p.Student_Id__c;
            
            Student__c student = New Student__c(Student_account__c = p.id,
                                  Student_First_Given_Name__c = p.FirstName,
                                  Student_Last_Name__c = p.LastName,
                                  Student_Idenfier__c = GlobalUtility.formatStudentId(p.Student_Identifer__c),
                                  Telephone_Number_Home__c = p.PersonHomePhone,
                                  Telephone_Number_Mobile__c = p.PersonMobilePhone,
                                  Date_of_Birth__c = p.PersonBirthdate,
                                  Email__c = p.PersonEmail,
                                  Student_Title__c = p.Salutation);
            
            
            
            if(!dupeIdsCheck.contains(p.Student_Identifer__c))                    
                newStudents.add(student);
            
            dupeIdsCheck.add(p.Student_Identifer__c);
        }
        
        insert newStudents;
        stopTrigger();
        update persons;
        
        
    }
    
     /*AV7 address fields are mantained here */
    //@future
    public static void mantainAVETMISS7AddressFields(Set<Id>accIds) {
        
        List<Account>Accounts = new List<Account>([Select Reporting_Other_State__c, Reporting_Other_State__r.Name,a.Postal_Country__r.Name,a.Address_Country__r.Name,a.Reporting_Billing_State__r.Name,Reporting_Billing_State__c, a.Suburb_locality_or_town__c,a.Postal_suburb_locality_or_town__c, a.Postal_delivery_box__c, a.Postal_Post_Code__c, a.Postal_Country__c, a.PersonOtherStreet, a.PersonOtherState, a.PersonOtherPostalCode, a.PersonOtherPhone, a.PersonOtherCountry, a.PersonOtherCity, a.PersonMobilePhone, a.PersonMailingStreet, a.PersonMailingState, a.PersonMailingPostalCode, a.PersonMailingCountry, a.PersonMailingCity, a.Address_street_number__c, a.Address_street_name__c, a.Address_flat_unit_details__c, a.Postal_building_property_name__c, a.Address_building_property_name__c, a.Address_Post_Code__c, a.Address_Country__c, a.Postal_flat_unit_details__c, a.Postal_street_number__c, Postal_street_name__c From Account a Where Id in : accIds and a.IsPersonAccount =True]);
        
        for(Account acc : Accounts) {
            
            /*Process Mailing Address */
            if(acc.Address_building_property_name__c != null && acc.Address_flat_unit_details__c != null && acc.Address_street_number__c != null && acc.Address_street_name__c != null )    
                acc.PersonMailingStreet = acc.Address_building_property_name__c + ', ' + acc.Address_flat_unit_details__c + ', ' + acc.Address_street_number__c + ' ' + acc.Address_street_name__c;
            else {
                 String building = '';
                 String Flat = '';
                 String snumber = '';
                 String sname = '';
                
                 if(acc.Address_building_property_name__c != null)
                    building = acc.Address_building_property_name__c;
                 if(acc.Address_flat_unit_details__c != null)
                    flat = acc.Address_flat_unit_details__c;
                 if(acc.Address_street_number__c != null)
                    sNumber = acc.Address_street_number__c;
                 if(acc.Address_street_name__c != null)
                    sname = acc.Address_street_name__c;
                 
                 acc.PersonMailingStreet = building + ', ' + flat + ', ' + snumber + ' ' + sname;
                 acc.PersonMailingStreet = acc.PersonMailingStreet.removeStart(', ');
                 acc.PersonMailingStreet = acc.PersonMailingStreet.ReplaceAll(', , ', ' ');
                 acc.PersonMailingStreet = acc.PersonMailingStreet.removeStart(',');
                 acc.PersonMailingStreet = acc.PersonMailingStreet.Trim();
            }
            if(acc.Suburb_locality_or_town__c != null)
                acc.PersonMailingCity = acc.Suburb_locality_or_town__c;
            if(acc.Address_Post_Code__c != null)
                acc.PersonMailingPostalCode = acc.Address_Post_Code__c;
            if(acc.Reporting_Billing_State__c != null)
                acc.PersonMailingState = acc.Reporting_Billing_State__r.Name;
            if(acc.Address_Country__c != null)
                acc.PersonMailingCountry = acc.Address_Country__r.Name;
                
                
            /*Process Postal Address */
            if(acc.Postal_Delivery_Box__c != null ){
                acc.PersonOtherStreet = acc.Postal_Delivery_Box__c;
                
                //physical address not allowed...
                acc.Postal_building_property_name__c = null;
                acc.Postal_flat_unit_details__c = null;
                acc.Postal_street_number__c = null;
                acc.Postal_street_name__c = null;
                    
            }else if( acc.Postal_building_property_name__c != null && acc.Postal_flat_unit_details__c != null && acc.Postal_street_number__c != null && acc.Postal_street_name__c != null ){
                acc.PersonOtherStreet = acc.Postal_building_property_name__c + ', ' + acc.Postal_flat_unit_details__c + ', ' + acc.Postal_street_number__c + ' ' + acc.Postal_street_name__c;
                
                //po box address not allowed...
                acc.Postal_Delivery_Box__c = null;
            }else {
                 String building = '';
                 String Flat = '';
                 String snumber = '';
                 String sname = '';
                
                 if(acc.Postal_building_property_name__c != null)
                    building = acc.Postal_building_property_name__c;
                 if(acc.Postal_flat_unit_details__c != null)
                    flat = acc.Postal_flat_unit_details__c;
                 if(acc.Postal_street_number__c != null)
                    sNumber = acc.Postal_street_number__c;
                 if(acc.Postal_street_name__c != null)
                    sname = acc.Postal_street_name__c;
                 
                 acc.PersonOtherStreet = building + ', ' + flat + ', ' + snumber + ' ' + sname;
                 acc.PersonOtherStreet = acc.PersonOtherStreet.removeStart(', ');
                 acc.PersonOtherStreet = acc.PersonOtherStreet.ReplaceAll(', , ', ' ');
                 acc.PersonOtherStreet = acc.PersonOtherStreet.removeStart(',');
                 acc.PersonOtherStreet = acc.PersonOtherStreet.Trim();
                 
                 //po box address not allowed...
                 acc.Postal_Delivery_Box__c = null;
            }
            
            if(acc.Postal_suburb_locality_or_town__c != null)
                acc.PersonOtherCity = acc.Postal_suburb_locality_or_town__c;
            if(acc.Postal_Post_Code__c != null)
                acc.PersonOtherPostalCode = acc.Postal_Post_Code__c;
            if(acc.Reporting_Other_State__c != null)
                acc.PersonOtherState = acc.Reporting_Other_State__r.Name;
            if(acc.Postal_Country__c != null)
                acc.PersonOtherCountry = acc.Postal_Country__r.Name;
        }
        
        stopTrigger();
        update Accounts;
    }

}