/**
 * @description    Test class for UserTrigger and UserTriggerHandler
 * @date           31.JULY.2014
 * @author         Warjie Malibago
 * @history        
 * @date 8.OCT.2015   Jerome Almanza     Added the following method for testing UserTrigger: testUnitAdelaide, testUnitBowen, testUnitBrisbaneGothaStreet, 
 *                                        testUnitBrookesStreet, testUnitBurleigh, testUnitCaboolture, testUnitGLSManila, testUnitHindmarsh, testUnitMelbourne, testUnitNerang, 
 *                                        testUnitNewcastle, testUnitParramatta, testUnitPerth, testUnitRomaStreet, testUnitSalisbury, testUnitSouthport, testUnitSydney, 
 *                                        testUnitToowoomba, testUnitTownsville
**/
@isTest
private class UserTriggerHandler_Test{
    static testMethod void testUnit(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            update u;
        }                    
    } 
    /**
     * @description Method - testUnitAdelaide for Adelaide
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */ 
    static testMethod void testUnitAdelaide(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Adelaide';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitBowen for Bowen Hills
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */ 
    static testMethod void testUnitBowen(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Bowen Hills';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitBrisbaneGothaStreet for BrisbaneGothaStreet
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
    static testMethod void testUnitBrisbaneGothaStreet(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Brisbane (Gotha Street)';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitBrookesStreet for BrookesStreet
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitBrookesStreet(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Brookes Street';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitBurleigh for Burleigh
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitBurleigh(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Burleigh';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitCaboolture for Caboolture
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitCaboolture(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Caboolture';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitGLSManila for GLSManila
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitGLSManila(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'GLS - Manila';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitHindmarsh for Hindmarsh
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
    static testMethod void testUnitHindmarsh(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Hindmarsh';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitMelbourne for Melbourne
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
    static testMethod void testUnitMelbourne(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Melbourne';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitNerang for Nerang
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitNerang(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Nerang';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitNewcastle for Newcastle
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
    static testMethod void testUnitNewcastle(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Newcastle';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitParramatta for Parramatta
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
    static testMethod void testUnitParramatta(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Parramatta';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitPerth for Perth
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitPerth(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Perth';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitRomaStreet for RomaStreet
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */       
    static testMethod void testUnitRomaStreet(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Roma Street - Transit Centre';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitSalisbury for Salisbury
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */   
     static testMethod void testUnitSalisbury(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Salisbury';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitSouthport for Southport
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitSouthport(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Southport';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitSydney for Sydney
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */     
    static testMethod void testUnitSydney(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Sydney';
            update u;
        }                    
    }
    /**
     * @description Method - testUnitToowoomba for Toowoomba
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */           
    static testMethod void testUnitToowoomba(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Toowoomba';
            update u;
        }                    
    } 
    /**
     * @description Method - testUnitTownsville for Townsville
     * @author Jerome Almanza
     * @date 23.OCT.2015
     */      
     static testMethod void testUnitTownsville(){
        Profile p = [SELECT Id, UserLicenseId, UserLicense.Name FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                    EmailEncodingKey='UTF-8', LastName='UHP_User', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='careerstestuser@careersaustralia.edu.au.test',
                    User_License_Type__c = p.UserLicense.Name);    
        system.runAs(u){
            u.User_License_Type__c = 'Salesforce Platform';
            u.Campus__c = 'Townsville';
            update u;
        }                    
    }                                                                  
}