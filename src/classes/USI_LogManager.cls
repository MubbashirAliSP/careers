/*This class manages the creation of the log
 *for USI integration
 */

public class USI_LogManager {

    //Create USI log
    public static USI_Integration_Log__c createUSILog() {
    
        USI_Integration_Log__c log = new USI_Integration_Log__c();
        insert log;
        return log;
    
    }
    
    //public static USI_Integration_Event__c createUSIEvent(String logId, String recordId, String message, Boolean isCreate) {
    public static USI_Integration_Event__c createUSIEvent(String logId, String recordId, String studentName, String studentNumber, String message, Boolean isCreate) {

        
        
        USI_Integration_Event__c event = new USI_Integration_Event__c();
        
        if(isCreate) {
            event.Service__c = 'Create';
        }
        else {
            event.Service__c = 'Verify';
        }
        
        event.Event_Status__c = 'Success';
        
        event.USI_Integration_Log__c = logId;
        event.Record_Id__c = recordId;
        event.Message__c = message;
        event.Student_Name__c = studentName;
        event.Student_Number__c = studentNumber;
                
        return event;
        
    }
    
    //public static USI_Integration_Event__c errorHandle( String errorMessage, Id studentId, String logId, Boolean isCreate ) {
    public static USI_Integration_Event__c errorHandle( String errorMessage, Id studentId, String studentName, String studentNumber, String logId, Boolean isCreate ) {

        
        USI_Integration_Event__c  event = new USI_Integration_Event__c();
        event.USI_Integration_Log__c = logId;
        event.Record_Id__c = studentId;
        event.Event_Status__c = 'Failure';
        event.Message__c = errorMessage; 
        
        if(isCreate) {
            event.Service__c  = 'Create';
        }
        else {
            event.Service__c  = 'Verify';
        }
                  
        USI_UtilityClass.createServiceCase(studentId, errorMessage, event.Service__c);

        if(event.Message__c == null) {
            event.Message__c = 'An unknown error has occured. Detail: ' + errorMessage;
        }
        
        event.Student_Name__c = studentName;
        event.Student_Number__c = studentNumber;

        return event;
        
    }
}