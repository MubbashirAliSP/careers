@istest global class CMSMockHTTPResponseGenerator implements HttpCalloutMock {
    
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://CareersAusTRIAL89987.jitterbit.eu/clouddev/1.0/processsobject', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"ok","message":"0 courses imported and 91 course updated"}');
        res.setStatusCode(200);
        return res;
    }

}