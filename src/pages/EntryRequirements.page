<apex:page docType="html-5.0" showHeader="false" sidebar="false" cache="false" controller="EntryRequirementsCC">
    <apex:stylesheet value="{!URLFOR($Resource.Bootstrap3, 'css/bootstrap.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.ApplicationsPortal, 'css/flat-ui-pro.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EnrolmentFormCSS)}" />
    <apex:includeScript value="{!URLFOR($Resource.ApplicationsPortal, 'js/jquery-1.11.3.min.js')}" />

    <style>
        .login-form-container {
            width: 200px;
            margin: 0 auto;
            text-align: center;
            border-radius: 3px;
            padding: 20px 10px;
            box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.5);
        }
        .ca-logo {
            margin: 10px;
        }
        .hero-image {
            height: 350px;
        }
        .hero-text {
            font-size: 50px;
            color:#FFFFFF;
            font-family: 'Gotham';
            padding-top: 270px;
            text-shadow: 5px 5px 7px #2d2d2d;
        }
        .hero-text-container {
            width: 960px;
            height: auto;
            margin: 0px auto;
        }
        .notconfirm-text {
            font-size: 20px;
            padding: 15px;
            text-align:center
        }
        .noselect {
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none;   /* Chrome/Safari/Opera */
            -khtml-user-select: none;    /* Konqueror */
            -moz-user-select: none;      /* Firefox */
            -ms-user-select: none;       /* IE/Edge */
            user-select: none;           /* non-prefixed version, currently
                                              not supported by any browser */
        }
    </style>

    <apex:outputPanel styleClass="applications-listing full-width-container">
        <apex:form >
            <div class="masthead">
                <div class="ca-logo">
                    <img width="200" height="66" title="Careers Australia" alt="Careers Australia" src="{!URLFOR($Resource.ApplicationsPortal, 'images/careers-australia-logo-large-red.jpg')}" />
                </div>
                <div class="hero-image" alt="My Applications" style="background-image: url({!URLFOR($Resource.ApplicationsPortal,'images/my_apps_hero.jpg')})">
                    <div class="hero-text-container">
                    <div class="hero-text"><strong>Entry Requirements</strong></div>
                    </div>
                </div>
                <div class="title-hero">
                    <div class="enrolment-form-container">
                    </div>
                </div>
                <div class="user-bar">
                    <div class="enrolment-form-container">
                        <div class="page-title">
                            <div class="page-name">
                                <span class="greeting">{!form.ParentQualificationName__c}</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="enrolment-form-container">
                <apex:outputPanel layout="block" styleClass="page-table">
                    <apex:outputPanel layout="block" styleClass="content-holder" id="applicationsBlockStudent">
                        <apex:pageMessages />
                        <table>
                            <tr>
                                <th style="display:{!If(isVFHRendered,'table-cell', 'none')}">Requirement Type</th>
                                <th style="display:{!If(not(isVFHRendered), 'table-cell', 'none')}">Category</th>
                                <th style="display:{!If(not(isVFHRendered), 'table-cell', 'none')}">Type</th>
                                <th>Status</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                            <apex:repeat value="{!entryRequirements}" var="requirement">
                                <tr>
                                    <td style="display:{!If(isVFHRendered, 'table-cell', 'none')}">
                                        {!requirement.RecordType.Name}
                                    </td>
                                    <td style="display:{!If(not(isVFHRendered), 'table-cell', 'none')}">
                                        <apex:inputField value="{!requirement.Category__c}" styleClass="disabled"/>
                                    </td>
                                    <td style="display:{!If(not(isVFHRendered), 'table-cell', 'none')}">
                                        <apex:inputField value="{!requirement.Type__c}" rendered="{!requirement.Type__c == null}"/>
                                        <apex:outputField value="{!requirement.Type__c}" rendered="{!requirement.Type__c != null}"/>
                                    </td>
                                    <td>
                                        {!requirement.Status__c}
                                    </td>
                                    <td>
                                        <apex:inputFile rendered="{!requirement.Status__c == 'Awaiting Upload'}" value="{!attachmentMap[requirement.Id].Body}" fileName="{!attachmentMap[requirement.Id].Name}"></apex:inputFile>
                                        <apex:outputLink value="{!URLFOR($Action.Attachment.Download, attachmentMap[requirement.Id].Id)}" target="_blank" rendered="{!attachmentMap[requirement.Id].Id != null}">
                                            {!attachmentMap[requirement.Id].Name}
                                        </apex:outputLink>
                                    </td>
                                    <td>
                                        <!-- <apex:commandButton rendered="{!(requirement.Status__c == 'Awaiting Upload' || (ISBLANK(requirement.Type__c) && NOT(ISBLANK(requirement.Category__c))))}" action="{!upload}" value="Submit"></apex:commandButton> -->

                                        <apex:commandLink styleClass="btn" style="color: black" rendered="{!(requirement.Status__c == 'Awaiting Upload' || (ISBLANK(requirement.Type__c) && NOT(ISBLANK(requirement.Category__c))))}" action="{!uploadSingle}" value="Submit">
                                            <apex:param assignTo="{!submittedERId}" name="submittedERId" value="{!requirement.Id}"></apex:param>
                                        </apex:commandLink>
                                        <apex:outputPanel layout="none" rendered="{!BEGINS(requirement.Status__c, 'LLN') && NOT(CONTAINS(requirement.Status__c, 'Confirmed')) && NOT(CONTAINS(requirement.Status__c, 'Closed'))}">
                                            <a href="#" data-link="{!requirement.LLN_Link__c}" class="lln-link btn" style="color: black">Skills Warm Up</a>
                                        </apex:outputPanel>
                                    </td>
                                </tr>
                            </apex:repeat>
                        </table>
                        <script>
                            $(document).ready(function(){
                                $(".disabled").each(function(){
                                    $(this).prop('disabled', 'disabled');
                                });

                                $('.page-table').on('click', '.lln-link', function(){
                                    window.open($(this).data('link'));
                                });
                            });
                        </script>
                    </apex:outputPanel>
                    <div class="page-table-buttons">
                        <!-- <apex:commandLink styleClass="ca-btn-action" action="{!upload}" value="Submit"></apex:commandLink> -->
                        <apex:outputLink style="margin-left:10px;" rendered="{!not(isVFHRendered)}" styleClass="ca-btn-action" value="{!$Page.UploadEntryRequirements}?formId={!form.Id}">New Entry Requirement</apex:outputLink>
                        <apex:outputLink style="margin-left:10px;" styleClass="ca-btn-action" value="{!$Page.ApplicationsLanding}">Back to My Applications</apex:outputLink>
                        <apex:outputLink style="margin-left:10px;" rendered="{!isUsiNextVisible}" styleClass="ca-btn-action" value="{!$Page.USI_Landing}?data={!form.StudentCredentialsId__r.AccountId__c}">Proceed with USI Application</apex:outputLink>
                    </div>
                </apex:outputPanel>
            </div>
            <hr/>
            <div class="entry-requirement-instruction text-left col-xs-8 col-xs-offset-2" style="display:{!If(not(isVFHRendered), 'block', 'none')}">
                <h4>INSTRUCTIONS</h4>
                    <br />
                    As a minimum you must upload supporting one document for each section:
                <ol class="container">
                    <div class="col-xs-4">
                    <li>Evidence of Date of Birth (a copy of one of the following is acceptable)
                        <ul>
                            <li>Driver's Licence</li>
                            <li>18+ Card</li>
                            <li>Heavy Vehicle or Marine Licence</li>
                            <li>Birth Certificate or Birth extract</li>
                            <li>Australian, New Zealand or International Passport</li>
                        </ul>
                    </li>
                    <li>
                        Proof of Queensland Residency (a copy of one of the following is acceptable)
                        <ul>
                            <li>Driver's Licence</li>
                            <li>Heavy Vehicle or Marine Licence</li>
                            <li>Dept of Veterans' Affairs/ Pensioner Concession Card</li>
                            <li>Health Care Card</li>
                            <li>Commonwealth Seniors Health Card</li>
                        </ul>
                    </li>
                    </div>
                    <div class="col-xs-4">
                    <li>
                        Evidence of Citizenship or Australian Residency (from the following list you select what is your citizenship / residency status and then you will need to provide a copy of one of the associated documents)
                        <ol>
                            <li>Proof of Australian / NZ Citizenship
                                <ul>
                                    <li>Birth Certificate or Birth extract</li>
                                    <li>Australian, New Zealand or International Passport</li>
                                    <li>Medicare Card (green only)</li>
                                </ul>
                            </li>
                            <li>Proof of Australian Residency Permanent
                                <ul>
                                    <li>Certificate of Evidence of Residence Status (CERS)</li>
                                    <li>Medicare Card (green only)</li>
                                </ul>
                            </li>
                            <li>Proof of Australian Residency Temporary
                                <ul>
                                    <li>Blue Medicare Card</li>
                                </ul>
                            </li>
                        </ol>
                    </li>
                    </div>
                    <div class="col-xs-4">
                    <b>IF YOU ARE CLAIMING EITHER OF THE FOLLOWING PLEASE PROVIDE A COPY OF THE ASSOCIATED DOCUMENT</b>
                    <li>
                        Concession Eligibility (a copy of one of the following is acceptable)
                        <ul>
                            <li>Dept of Veterans' Affairs/ Pensioner Concession Card</li>
                            <li>Health Care Card</li>
                            <li>Commonwealth Seniors Health Card</li>
                        </ul>
                    </li>
                    <li>
                        Fee Free Training Yr 12 (a copy of one of the following is acceptable)
                        <ul>
                            <li>Senior Statement</li>
                            <li>Statement of Results</li>
                        </ul>
                    
                    </li>
                    </div>
                </ol>
                Please contact Careers Australia with any questions or concerns 1300 887 696.
            </div>
        </apex:form>
    </apex:outputPanel>

    
</apex:page>