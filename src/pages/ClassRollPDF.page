<!-- ********************************************************************************************************** -->
<!-- Company: Cloud Sherpas                                                                                     -->
<!-- Date: 11.NOV.2012                                                                                          -->
<!-- Author: Janella Lauren Canlas                                                                              -->
<!-- Description: Class Roll PDF page                                                                           -->
<!--                                                                                                            -->
<!-- HISTORY:                                                                                                   -->
<!-- 11.NOV.2012    Janella Canlas      Created.                                                                -->           
<!-- 10.FEB.2014    Bayani Cruz         Removed Extra ED INITIAL Column.                                        -->
                    
<!-- ********************************************************************************************************** -->
<apex:page controller="GenerateClassRoll_CC" renderAs="pdf" title="Class Roll PDF">
    <head>
    <style media="all"> 
        @page {
            
            margin-top: 0.5cm;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-bottom: 1.0in;
            size:landscape legal;
            
            @bottom-left {
                content: element(footer); 
            }
            @top-left {
                content: element(header);
            }
        }
        
        div.footer {
            position : running(footer) ;
            page-break-after:always;
        }
        div.header {
            position : running(header) ;
            
        }
        
        table.fixed { table-layout:fixed; }
        .vt2 {
            color:#000;
            border:0px solid red;
            writing-mode:tb-rl;
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            -webkit-transform:rotate(-90deg);
            -moz-transform:rotate(-90deg);
            -o-transform: rotate(-90deg);
            white-space:nowrap;
            width:20px;
            height:20px;
            font-size:14px;
            font-weight:bold;
        }
    </style> 
    </head>
    <apex:repeat value="{!pageWrapperList}" var="p">
        <div style="page-break-after:always;">
        <div id="header"><apex:image value="{!$Resource.Careers_AU_Logo_2}" width="10%" height="10%"/></div>
        <br/>
        <table width="100%" cellspacing="0" cellpadding="0" class="fixed">
            <tr>
                <td>
                    <apex:outputText value="CLASS ROLL" style="font-weight:bold; font-size: 12px;"/>
                </td>
                <td style="border:1px solid black;font-size: 12px;">
                    <apex:outputText value="{!IntakeRecord.Training_Delivery_Location__r.Name}"/>&nbsp;
                    <apex:outputText value="{!IntakeRecord.Qualification_Name__c}"/><br/>
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                       <apex:param value="{!IntakeRecord.Start_Date__c}"/>
                    </apex:outputText>
                    <apex:outputText value=" - "/>
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                       <apex:param value="{!IntakeRecord.End_Date__c}"/>
                    </apex:outputText>
                </td>
                <td style="border:1px solid black;font-size: 12px;text-align:right;">
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                       <apex:param value="{!fromDate}"/>
                    </apex:outputText>
                    <apex:outputText value=" - "/>
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                       <apex:param value="{!toDate}"/>
                    </apex:outputText><br/>
                    Intake ID: &nbsp; <apex:outputText value="{!IntakeRecord.Name}"/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border: 1px solid black;font-size:12px;">
                    <apex:outputText value="Educator: {!trainer}"/>
                </td>
            </tr>
            </table>
            <table cellspacing="0" width="100%" cellpadding="0" class="fixed">
                <!--HEADERS WITH CLASSES-->
                <tr>
                    <td width="100%">
                        <table cellspacing="0" width="100%" class="fixed" height="100%">
                            <tr>
                                <td style="border: 1px solid black;font-size:12px;width:5%;">
                                    <apex:outputText value="Student #"/>
                                </td>
                                <td style="border: 1px solid black;font-size:12px;width:10%;">
                                    <apex:outputText value="Student Name"/>
                                </td>
                                <td class="vt2" style="border: 1px solid black;font-size:12px;width:4%;">
                                    <apex:outputText value="LLN/"/><br/>
                                    <apex:outputText value="DIS"/>
                                </td>
                                <td style="border: 1px solid black;font-size:12px;width:5%;">
                                    <apex:outputText value="Induction/"/><br/>
                                    <apex:outputText value="LLN Complete"/>
                                </td>
                                <apex:repeat value="{!p.header}" var="h">
                                    <td style="font-size:12px;padding:0px;text-align:center;width:10%;">
                                        <table cellspacing="0" width="100%" height="100%"> 
                                            <tr>
                                                <td style="font-size:12px;border: 1px solid black;height:30px;">
                                                    <apex:outputText value=" {!h.startDate}"/>
<!--                                                    <apex:outputText value="{0, date, dd/MM/yyyy hh:ss a}">-->
<!--                                                        <apex:param value="{!DATETIMEVALUE(h.startDate)}"/>-->
<!--                                                    </apex:outputText>-->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:12px;border: 1px solid black;font-weight:bold; 

height:20px;">
                                                    <apex:outputText value="{!h.unitCode}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:12px;border: 1px solid black;height:20px;">
                                                    Signature
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </apex:repeat>
                                <!-- EXTRA ED INITIAL SPACE //BAC 10.FEB.2014
                                <td width="7%" style="font-size:12px;padding:0px;text-align:center;height:100%;" >
                                <table cellspacing="0" width="100%" cellpadding="0" height="100%">
                                    <tr>
                                        <td style="font-size:12px;border: 1px solid black;height:30px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:12px;border: 1px solid black;height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:12px;border: 1px solid black;height:20px;">
                                            ED INITIAL
                                        </td>
                                    </tr>
                                </table>
                            </td> -->
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <!--STUDENT ROWS-->
                <tr>
                    <td>
                        <table cellspacing="0" width="100%" height="100%" class="fixed">
                            <apex:repeat value="{!p.attendance}" var="at">
                                
                                    <tr>
                                        <td style="font-size:12px;border: 1px solid black;width:5%;">
                                            <apex:outputText value="{!at.identifier}"/>
                                        </td>
                                        <td style="font-size:12px;border: 1px solid black;width:10%;">
                                            <apex:outputText value="{!at.student}"/>
                                        </td>
                                        <td style="font-size:12px;border: 1px solid black;width:4%;">
                                            <apex:outputText value="{!at.LLN}"/>
                                        </td>
                                        <td style="font-size:12px;border: 1px solid black;width:5%;">
                                            <apex:outputText value="TBA"/>
                                        </td>
                                        <apex:repeat value="{!at.attendance}" var="att">
                                            <td style="font-size:12px;border: 1px solid black;width:10%;">
                                                <apex:outputText value="{!att.attend}"/>
                                            </td>
                                        </apex:repeat>
                                        <!-- BAC 10.FEB.2014 td style="font-size:12px;border: 1px solid black;width:7%;"></td -->
                                    </tr>
                                
                            </apex:repeat>
                        </table>
                    </td>
                </tr>
            </table>
        
            <!--FOOTER-->
            <div class="footer" >
                <table width="100%" >
                    <tr>
                       <td width="30%" >
                            <table width="100%" >
                                <tr>
                                    <td style="border-top: 1px solid black;width:100%;text-align:center;height:100%">
                                        <apex:outputText value="{!trainer}"/>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="100%">
                                        Educator's Signature 
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="30%">
                            <table width="100%" >
                                <tr>
                                    <td style="border-top: 1px solid black; width:100%" align="center">Date</td>
                                </tr>
                                <tr>
                                    <td >
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%">
                            <table width="100%" cellspacing="0">
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">ABSENT</td>
                                    <td style="font-size:12px;border: 1px solid black;">A</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">ABSENT ADVISED</td>
                                    <td style="font-size:12px;border: 1px solid black;">AA</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">ARRIVED LATE</td>
                                    <td style="font-size:12px;border: 1px solid black;">AL</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">PUBLIC HOLIDAY</td>
                                    <td style="font-size:12px;border: 1px solid black;">PH</td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%">
                            <table width="100%" cellspacing="0">
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">LEFT EARLY</td>
                                    <td style="font-size:12px;border: 1px solid black;">LE</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">PRESENT</td>
                                    <td style="font-size:12px;border: 1px solid black;">P</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">NOT REQUIRED TO ATTEND</td>
                                    <td style="font-size:12px;border: 1px solid black;">NR</td>
                                </tr>
                                <tr>
                                    <td style="font-size:12px;border: 1px solid black;">CROSS CREDIT TRANSFER</td>
                                    <td style="font-size:12px;border: 1px solid black;">CCT</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </apex:repeat>
    
<!--    </apex:repeat>-->
</apex:page>