/**
 * @description Dual Enrolment object Trigger 
 * @author      Sairah Hadjinoor
 * @Company     CloudSherpas
 * @date        01.DEC.2014
 *
 * HISTORY
 * - 01.DEC.2014    Sairah Hadjinoor      Created.
 */

trigger DualEnrolmentTrigger on Dual_Enrolment__c (before insert, after insert, before update, after update, before delete, after delete) {


	//Runs on before events only
	//uncomment this just in case we already need before events
    /*if(Trigger.isBefore ) { 
    	//calls onBeforeInsert method from  DualEnrolmentTriggerHandler for insert event
    	if(Trigger.isInsert){     
        	DualEnrolmentTriggerHandler.onBeforeInsert(Trigger.new); 
        }
        //uncomment this just in case we need a before update event
        /*if(Trigger.isUpdate){
        	DualEnrolmentTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap); 
        }*/
        //uncomment this just in case we need a before delete event
        /*if(Trigger.isDelete){
        	DualEnrolmentTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap); 
        }
    }*/
    //Runs on after events only 
    if(Trigger.isAfter ) {
    	//calls onAfterInsert method from  DualEnrolmentTriggerHandler for insert event  
    	if(Trigger.isInsert){
    		DualEnrolmentTriggerHandler.onAfterInsert(Trigger.new); 
    	} 
    	//uncomment this just in case we need a after update event 
    	/*if(Trigger.isUpdate){
    		DualEnrolmentTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    	}*/
    	//calls onAfterDelete method from  USIIdentificationDocumentTriggerHandler for delete event 
        if(Trigger.isDelete){
        	DualEnrolmentTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap); 
        } 
    }
}