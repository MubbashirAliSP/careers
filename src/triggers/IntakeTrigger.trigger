/**
 * @description Trigger for the Intake object
 * @author Ranyel Maliwanag
 * @date 16.MAR.2016
 */
trigger IntakeTrigger on Intake__c (after insert, after update) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			IntakeTriggerHandlerSAS.onAfterInsert(trigger.new);
		}

		if (trigger.isUpdate) {
			IntakeTriggerHandlerSAS.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}