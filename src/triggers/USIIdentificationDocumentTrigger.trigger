/**
 * @description USI Identification Document object Trigger 
 * @author      Sairah Hadjinoor
 * @Company     CloudSherpas
 * @date        23.OCT.2014
 *
 * HISTORY
 * - 23.OCT.2014    Sairah Hadjinoor      Created.
 */
trigger USIIdentificationDocumentTrigger on USI_Identification_Document__c (before insert, after insert, before update, after update, after delete) {

	//Runs on before events only
    if(Trigger.isBefore ) { 
    	//calls onBeforeInsert method from  USIIdentificationDocumentTriggerHandler for insert event
    	if(Trigger.isInsert){     
        	USIIdentificationDocumentTriggerHandler.onBeforeInsert(Trigger.new); 
        }
        //uncomment this just in case we need a before update event
        /*if(Trigger.isUpdate){
        	USIIdentificationDocumentTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap); 
        }*/
        //uncomment this just in case we need a before delete event
        /*if(Trigger.isDelete){
        	USIIdentificationDocumentTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap); 
        }*/
    }
    //Runs on after events only 
    if(Trigger.isAfter ) {
    	//calls onAfterInsert method from  USIIdentificationDocumentTriggerHandler for insert event  
    	if(Trigger.isInsert){
    		USIIdentificationDocumentTriggerHandler.onAfterInsert(Trigger.new); 
    	} 
    	//uncomment this just in case we need a after update event 
    	/*if(Trigger.isUpdate){
    		USIIdentificationDocumentTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    	}*/
    	//uncomment this just in case we need an after delete event
    	//calls onAfterDelete method from  USIIdentificationDocumentTriggerHandler for delete event 
        if(Trigger.isDelete){
        	USIIdentificationDocumentTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap); 
        } 
    }
}