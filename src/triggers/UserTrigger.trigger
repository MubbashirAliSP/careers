/*
 * @description    Trigger for user object
 * @date           31.JULY.2014
 * @author         Warjie Malibago
 * @History
 *  09.MAY.2016     Biao Zhang          create contact for new user under Careers Australia Internal Users account          
*/
trigger UserTrigger on User (before insert, before update, after insert, after update) {
    Set<Id> userId = new Set<Id>();
    List<User> userList = new List<User>();
    static Boolean proceed = true;
    
    for(User u: Trigger.New){
        userId.add(u.Id);
    }
    if(trigger.isAfter){
        if(Trigger.isInsert){
            for(User u: Trigger.New){
                if(proceed){
                    proceed = UserTriggerHandler.updateUserLicenseType(u.User_License_Type__c, u.Id);
                }
                //UserTriggerHandler.createContactForInternalUserAccount(userId);
            }
        }
        if(Trigger.isUpdate){ 
            List<Id> deativatedUsers = new List<Id>();
            for(User u: Trigger.New){
                if(proceed){
                    proceed = UserTriggerHandler.updateUserLicenseType(u.User_License_Type__c, u.Id);
                }
                if(u.IsActive == false) {
                    deativatedUsers.add(u.Id);
                }
            }
            if(deativatedUsers.size() > 0) {
                //UserTriggerHandler.removeContactFromInternalUserAccount(deativatedUsers);
            }
        }
    } 
/*
 * @description    User fields are populated based on Campus Picklist selection
 * @date           10.SEP.2015
 * @author         Troy Collins
 * @history
    * 02.JUN.2016   Troy Collins    Added details for Green Square
*/
    if (trigger.isBefore) {
        for (User userRecord : trigger.new){ 
            if (userRecord.Campus__c == 'Adelaide') {
                userRecord.Street = 'Level 1, 22 Pulteney Street';
                userRecord.City = 'Adelaide';
                userRecord.State = 'South Australia';
                userRecord.PostalCode = '5000';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey = 'Australia/Adelaide'; 
            }
            if (userRecord.Campus__c == 'Bowen Hills') {
                userRecord.Street = '16 - 18 Thompson Street';
                userRecord.City = 'Bowen Hills';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4006';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Brisbane (Gotha Street)') {
                userRecord.Street = '121 - 123 Gotha Street';
                userRecord.City = 'Fortitude Valley';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4006';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Brookes Street') {
                userRecord.Street = '100 Brookes Street';
                userRecord.City = 'Fortitude Valley';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4006';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Burleigh') {
                userRecord.Street = '6 Ern Harley Drive';
                userRecord.City = 'Burleigh';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4220';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Burleigh') {
                userRecord.Street = '6 Ern Harley Drive';
                userRecord.City = 'Burleigh';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4220';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Caboolture') {
                userRecord.Street = '13 Haskings Street';
                userRecord.City = 'Caboolture';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4510';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Green Square') {
                userRecord.Street = 'Green Square, 515 St Pauls Terrace';
                userRecord.City = 'Fortitude Valley';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4006';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey = 'Australia/Brisbane';
            } 
            if (userRecord.Campus__c == 'GLS - Manila') {
                userRecord.Street = 'Rockwell Business Center, Ortigas';
                userRecord.City = 'Manila';
                userRecord.State = 'National Capital Region';
                userRecord.PostalCode = '1600';
                userRecord.Country = 'Philippines';
                userRecord.TimeZoneSidKey = 'Asia/Manila';
                //Troy - removed Locale as requested by GLS team 29.SEP.2015
                //userRecord.LocaleSidKey = 'en_PH';
            }
            if (userRecord.Campus__c == 'Hindmarsh') {
                userRecord.Street = 'Lot 3, 7 - 13 Ridley Street';
                userRecord.City = 'Hindmarsh';
                userRecord.State = 'South Australia';
                userRecord.PostalCode = '5007';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey = 'Australia/Adelaide';
            }
            if (userRecord.Campus__c == 'Melbourne') {
                userRecord.Street = '196 Flinders Street';
                userRecord.City = 'Melbourne';
                userRecord.State = 'Victoria';
                userRecord.PostalCode = '3000';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey ='Australia/Sydney';
            }
            if (userRecord.Campus__c == 'Nerang') {
                userRecord.Street = '2 Palings Court';
                userRecord.City = 'Nerang';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4211';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Newcastle') {
                userRecord.Street = '4 Frost Drive';
                userRecord.City = 'Newcastle';
                userRecord.State = 'New South Wales';
                userRecord.PostalCode = '2304';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey ='Australia/Sydney';
            }
            if (userRecord.Campus__c == 'Parramatta') {
                userRecord.Street = 'Level 1, 25 - 35 George Street';
                userRecord.City = 'Parramatta';
                userRecord.State = 'New South Wales';
                userRecord.PostalCode = '2150';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey ='Australia/Sydney';
            }
            if (userRecord.Campus__c == 'Perth') {
                userRecord.Street = '154 Abernethy Road';
                userRecord.City = 'Perth';
                userRecord.State = 'Western Australia';
                userRecord.PostalCode = '6104';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey = 'Australia/Perth';
            }
            if (userRecord.Campus__c == 'Roma Street - Transit Centre') {
                userRecord.Street = '171 Roma St, Roma Street Transit Centre';
                userRecord.City = 'Brisbane';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4000';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Salisbury') {
                userRecord.Street = '460 - 492 Beaudesert Road';
                userRecord.City = 'Salisbury';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4107';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Southport') {
                userRecord.Street = '42 Marine Parade';
                userRecord.City = 'Southport';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4215';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Sydney') {
                userRecord.Street = 'Level 5, 815 George Street';
                userRecord.City = 'Sydney';
                userRecord.State = 'New South Wales';
                userRecord.PostalCode = '2000';
                userRecord.Country = 'Australia';
                userRecord.TimeZoneSidKey ='Australia/Sydney';
            }
            if (userRecord.Campus__c == 'Toowoomba') {
                userRecord.Street = 'Unit 4E, 7 Gardner Court';
                userRecord.City = 'Toowoomba';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4350';
                userRecord.Country = 'Australia';
            }
            if (userRecord.Campus__c == 'Townsville') {
                userRecord.Street = 'Lot 5, 602 Ingham Road, Bohle';
                userRecord.City = 'Townsville';
                userRecord.State = 'Queensland';
                userRecord.PostalCode = '4818';
                userRecord.Country = 'Australia';
            } 
            // Troy: user field updates end
        }
    }    
}