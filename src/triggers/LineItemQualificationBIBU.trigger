/**
 * @description 	Line Item Qualification Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas		Created.
 */
trigger LineItemQualificationBIBU on Line_Item_Qualifications__c (before insert, before update) {
	List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'LIQ_User'){
		if(Trigger.isInsert){
			for(Line_Item_Qualifications__c l : Trigger.new){
				liqList.add(l);
			}
			LineItemQualificationTriggerHandler.populateRecordType(liqList);
		}
	}
}