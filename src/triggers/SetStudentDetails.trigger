trigger SetStudentDetails on Case (before insert, before update) {
for(Case c : Trigger.new){
List<Student__c> studentSelected = [SELECT Student_Account__c, Student_Idenfier__c, Id, Student_First_Given_Name__c, Student_Last_Name__c FROM Student__c WHERE Student_Idenfier__c = :c.Student_Identifier__c OR Id =: c.Student_Name__c LIMIT 1];
if(studentSelected != null && studentSelected.size()>0){
c.Account__c = studentSelected[0].Student_Account__c;
c.Student_Identifier__c = studentSelected[0].Student_Idenfier__c;
c.Student_Name__c = studentSelected[0].Id;

String firstName = studentSelected[0].Student_First_Given_Name__c;
String lastName = studentSelected[0].Student_Last_Name__c;

List<Contact> contactList = [SELECT Id FROM Contact WHERE AccountId =: c.Account__c AND FirstName =: firstName AND LastName =: lastName];
if(contactList != null && contactList.size()==1){
c.ContactId = contactList[0].Id;
}
}
}
}