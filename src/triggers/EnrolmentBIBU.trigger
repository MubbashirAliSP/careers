/**
 * @description     Enrolment Object Trigger Before Insert, Before Update
 * @author          Janella Lauren Canlas
 * @Company         CloudSherpas
 * @date            03.DEC.2012
 *
 * HISTORY
 * - 03.DEC.2012    Janella Canlas      Created.
 * - 21.JAN.2013    YG - Added a call to update NSW allowed claims field
 * - 23.JAN.2013    Janella Canlas      Call Populate Student Address
 * - 05.FEB.2013    Janella Canlas      Call method to populate Contract Type
 * - 17.SEP.2013    Michelle Magsarili    Call new method populateAssignedTrainerProfile
 * - 11.JUN.2014    Warjie Malibago     Removed unnecessary codes
 */
trigger EnrolmentBIBU on Enrolment__c (before insert, before update) {
    
    Set<Id> qualificationIds = new Set<Id>();
    Set<Id> enrolmentIds = new Set<Id>();
    Set<Id> liqIds = new Set<Id>();
    
    List<Line_Item_Qualifications__c> liqList = new List<Line_Item_Qualifications__c>();
    List<Qualification__c> qualList = new List<Qualification__c>();
    List<Enrolment__c> eList = new List<Enrolment__c>();
    List<Enrolment__c>claimAllowedElist = new List<Enrolment__c>();
    Set<String> DLS = new Set<String>();
    Set<Id> compIds = new Set<Id>();
    List<Enrolment__c> compList = new List<Enrolment__c>();

    private static Id vfhEnrolmentRTID = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get('VET Fee Help Enrolment').getRecordTypeId();
    private static Id dualFundingEnrolemntRTID = Schema.SObjectType.Enrolment__c.getRecordTypeInfosByName().get('Dual Funding Enrolment').getRecordTypeId();
    private static Id tailorQualRTID = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Tailored Qualification').getRecordTypeId();
    private static Id qualRTID = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Qualification').getRecordTypeId();
    private static Id courseQualRTID = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Course').getRecordTypeId();

    if(!Test.isRunningTest() || UserInfo.getName() == 'Enrolment_User'){
        if(Trigger.isInsert){
            system.debug('**EN BIBU Insert!');
            for(Enrolment__c e : Trigger.new){
                liqIds.add(e.Line_Item_Qualification__c);
                qualificationIds.add(e.Qualification__c);
                enrolmentIds.add(e.Id);
                eList.add(e);
                claimAllowedElist.add(e);
            }
            
            system.debug('**liqIds: ' + liqIds + ' ,qualList: ' + qualList + ' ,claimAllowedElist: ' + claimAllowedElist);            
            
            //query related line item qualifications and add error if qualification is invalid
            liqList = [select Qualification__c, Id from Line_Item_Qualifications__c where id IN: liqIds];
            for(Enrolment__c e : Trigger.new){
                for(Line_Item_Qualifications__c l : liqList){
                    if(e.Line_Item_Qualification__c == l.Id && e.Qualification__c != l.Qualification__c){
                        e.addError('Please select a valid Line Item Qualification.');
                    }
                }
                DLS.add(e.Delivery_Location_State__c);
            }
            //query related qualifications and add error if qualification is inactive
            Map<Id, Qualification__c> qualIdMapping = new Map<Id, Qualification__c>();
            qualList = [select Id, Active__c, Award_based_on__r.Qualification_Name__c, RecordTypeId,
                        Award_based_on__r.Name, Name, RecordType.name, Qualification_Name__c 
                        from Qualification__c where Id IN: qualificationIds];
            for(Qualification__c q : qualList) {
                qualIdMapping.put(q.Id, q);
            }
            for(Enrolment__c e : Trigger.new){
                if(PersonAccountHandler.validateTFN(e.Tax_File_Number__c) == false){
                    e.AddError('Invalid Tax File Number');
                }
                if(e.External_Id__c == null){
                    for(Qualification__c q : qualList){
                        if(e.Qualification__c == q.Id && q.Active__c == true){
                            e.addError('Please select an Active Qualification.');
                        }
                    }
                }
            }
            
            if(claimAllowedElist.size() > 0){
                if(DLS.contains('New South Wales')){
                    system.debug('**UPDATE CLAIMS!');
                    EnrolmentTriggerHandler.updateNoOfAllowedClaimsNSW(claimAllowedElist);
                }
            }
            EnrolmentTriggerHandler.populateAssignedTrainerProfile(eList); //MAM 09/17/2013 new method populateAssignedTrainerProfile
            EnrolmentTriggerHandler.populateQualificationFields(enrolmentIds, eList, qualList);//3FEB2013 jcanlas; pass qualificationIds WBM 7JUL2014
            //added 01232013 call method to populate Student Address
            EnrolmentTriggerHandler.populateStudentAddresses(eList);
            EnrolmentTriggerHandler.populateContractType(enrolmentIds, eList);//added 05FEB2013 jcanlas
            EnrolmentTriggerHandler.populateDeliveryModeType(enrolmentIds, eList);
            
            system.debug('%%isInsert');
            
            Set<Id> quaIds = new Set<Id>();
            for(Enrolment__c e: Trigger.new){
                quaIds.add(e.Qualification__c);
                system.debug('FIREe.Qualification__c: '+e.Qualification__c);
            }

            Map<Id,String> quaRecordTypeIdNameMap = new Map<Id,String>();
            List<Qualification__c> qualList2 = new List<Qualification__c>([SELECT id, RecordType.Name FROM Qualification__c WHERE id in: quaIds]);
            for(Qualification__c q: qualList2){
                system.debug('FIREq.Id: '+q.Id);
                system.debug('FIREq.RecordType.Name: '+q.RecordType.Name);
                quaRecordTypeIdNameMap.put(q.Id,q.RecordType.Name);
            }
            
            for(Enrolment__c e: Trigger.new){
                if(e.RecordTypeId == vfhEnrolmentRTID && qualIdMapping.get(e.Qualification__c).RecordTypeId != tailorQualRTID){
                    if(userinfo.getProfileId() != '00e90000000rnWHAAY')
                        e.addError('Qualification record type must be "Tailored Qualification"');
                } else if(e.RecordTypeId == dualFundingEnrolemntRTID && (qualIdMapping.get(e.Qualification__c).RecordTypeId == qualRTID || qualIdMapping.get(e.Qualification__c).RecordTypeId == courseQualRTID)){
                    if(userinfo.getProfileId() != '00e90000000rnWHAAY')
                        e.addError('Qualification record type must be "Tailored Qualification"');
                }
            }
        }
        
        if(Trigger.isUpdate) {       
            system.debug('**EN BIBU Update!');
            Set<Id> quaIds = new Set<Id>();
            Set<Id> rId = new Set<Id>();
            Set<Id> enrId = new Set<Id>();
            for(Enrolment__c e: Trigger.new){
                quaIds.add(e.Qualification__c);
                rId.add(e.RecordTypeId);
                enrId.add(e.Id);
                system.debug('**WATERe.Qualification__c: '+e.Qualification__c);
            }
                        
            for(Enrolment__c e: Trigger.new){
                DLS.add(e.Delivery_Location_State__c);
            }
           
          
            Map<Id, Qualification__c> qualIdMapping = new Map<Id, Qualification__c>();
            List<Qualification__c> qualList3 = new List<Qualification__c>([SELECT id, Award_based_on__r.Qualification_Name__c, RecordTypeId, RecordType.Name,
                                            Award_based_on__r.Name, Name, Qualification_Name__c
                                            FROM Qualification__c WHERE id in: quaIds]);
            for(Qualification__c q: qualList3){
                qualIdMapping.put(q.Id,q);
            }
            
            for(Enrolment__c e: Trigger.new){
                //Uncommented after load
                if(e.RecordTypeId == vfhEnrolmentRTID && qualIdMapping.get(e.Qualification__c).RecordTypeId != tailorQualRTID){
                    if(userinfo.getProfileId() != '00e90000000rnWHAAY') //System Admin
                     e.addError('Qualification record type must be "Tailored Qualification"');
                }else if(e.RecordTypeId == dualFundingEnrolemntRTID && (qualIdMapping.get(e.Qualification__c).RecordTypeId == qualRTID || qualIdMapping.get(e.Qualification__c).RecordTypeId == courseQualRTID)){
                    if(userinfo.getProfileId() != '00e90000000rnWHAAY')
                     e.addError('Qualification record type must be "Tailored Qualification"');
                }
            }
            
            for(Enrolment__c e : Trigger.new){
                //validate tax file number
                if(PersonAccountHandler.validateTFN(e.Tax_File_Number__c) == false){
                    system.debug('** UPDaTE INVALID TFN!');
                    e.AddError('Invalid Tax File Number');  
                }           
                //Only if start or end date has changed, we may need to recalculate number of allowed claims
                if(e.Start_Date__c != Trigger.oldMap.get(e.id).Start_Date__c || e.End_Date__c != Trigger.oldMap.get(e.id).End_Date__c){
                    claimAllowedElist.add(e);
                }
                if(e.Enrolment_Status__c == 'Completed' && e.Enrolment_Status__c != Trigger.oldMap.get(e.Id).Enrolment_Status__c){
                    compIds.add(e.Id);
                    compList.add(e);
                }
                enrolmentIds.add(e.Id);//added 13FEB2013 jcanlas
                eList.add(e);//added 13FEB2013 jcanlas
                Enrolment__c oldEnrolment = Trigger.oldMap.get(e.Id);
            }
            system.debug('***enrolmentIds'+enrolmentIds);
            system.debug('***eList'+eList);
            if(claimAllowedElist.size() > 0){
                if(DLS.contains('New South Wales')){
                    system.debug('**UPDATE CLAIMS 2!');
                    EnrolmentTriggerHandler.updateNoOfAllowedClaimsNSW(claimAllowedElist);
                }
            }   
            EnrolmentTriggerHandler.populateAssignedTrainerProfile(eList); //MAM 09/17/2013 new method populateAssignedTrainerProfile
            EnrolmentTriggerHandler.populateQualificationFields(enrolmentIds, eList, qualList3);//added 13FEB2013 jcanlas; pass quaIds WBM 7JUL2014
            EnrolmentTriggerHandler.populateContractType(enrolmentIds, eList);//added 13FEB2013 jcanlas
            if(compList.size() > 0){
                system.debug('**POPULATE VARIATION');
                EnrolmentTriggerHandler.populateVariationReason(compIds, compList);
            }           
            system.debug('%%isUpdate');            
        }          
    }
}