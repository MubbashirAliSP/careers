/**
 * @description Trigger handler for Qualification object 
 * @author Yuri Gribanov
 * @date 16.OCT.2012
 *
 * HISTORY
 * - 16.OCT.2012    Yuri Gribanov   - Created.
 * - 18.JAN.2013    Janella Canlas  - call Method to update related EUoS records
 * - 06.MAY.2013    Jade Serrano    - call QualificationTriggerHandler.updateDisciplineCode method to update discipline code of Enrolment Unit of Study
 * - 27.OCT.2015    Yuri Gribanov   - Added CMS Integration Trigger call
 */
trigger QualificationAfterInsertAfterUpdateTrigger on Qualification__c (after insert, after update) {
    if(!Test.isRunningTest() || UserInfo.getName() == 'QUAL_User'){
        if(trigger.isInsert) {
            Set<Id> qualIds = new Set<Id>();
            Set<Id> qualificationIds = new Set<Id>();
            for(Qualification__c qual : Trigger.New) {  
                //if(qual.RecordType.Name == 'Tailored Qualification')
                    qualIds.add(qual.Award_Based_On__c);
                    qualificationIds.add(qual.Id);
            }
            QualificationTriggerHandler.cloneTailoredQualificationUnits(qualIds, Trigger.New);
        }   
        
        if(trigger.isUpdate){
            Set<Id> quaIds = new Set<Id>();
            //String disciplineCode = '';
            for(Qualification__c qua: Trigger.new){
                if(Trigger.oldMap.get(qua.id).Discipline_code__c!=qua.Discipline_code__c){
                    //system.debug('##');
                    quaIds.add(qua.id);
                    //system.debug('##quaIds: '+quaIds);
                    //disciplineCode = String.valueOf(qua.Discipline_code__r.FoE_Code__c);
                    //system.debug('##disciplineCode: '+disciplineCode);
                    QualificationTriggerHandler.updateDisciplineCode(quaIds);
                }
            }
            
            
            Set<Id> qualificationIds = new Set<Id>();
            for(Qualification__c qual : Trigger.New) {  
                Qualification__c oldQual = Trigger.oldMap.get(qual.Id);
                    if(oldQual.Course_of_Study_Type__c != qual.Course_of_Study_Type__c){
                        qualificationIds.add(qual.Id);
                    }
            }
            if(qualificationIds.size() > 0){
                QualificationTriggerHandler.updateEUOS(qualificationIds);
            }
        }
    }
    
    /*CMS Integration Trigger code here */
    
    if(trigger.isUpdate) {
        
        set<Id>qualIdsStaging = new Set<Id>();
        set<Id>qualIdsLive = new Set<Id>();
        
        for(Qualification__c qual : Trigger.New) {
            
            //Visible on website flag has been ticked, this will trigger initial integration
            if(qual.Website_Live_Upsert_Required__c == false && (qual.Visible_On_Website__c == true && Trigger.Oldmap.get(qual.Id).Visible_On_Website__c == false) || (qual.Visible_On_Website__c == true && (qual.Website_Update_Required__c == true && Trigger.Oldmap.get(qual.Id).Website_Update_Required__c == false)) )
            	qualIdsStaging.add(qual.Id);
            
            if((qual.Website_Live_Upsert_Required__c == true && Trigger.Oldmap.get(qual.Id).Website_Live_Upsert_Required__c == false) && qual.Visible_On_Website__c == true)
                qualIdsLive.add(qual.Id);
        }        
        
        
        if(qualIDsStaging.size() > 0) {
            
            CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Staging', Integration_Message__c = 'Processing' );
            insert log;
            
            //Tested outside of this trigger
            if(!Test.isRunningTest()) {
                CMSIntegrationController.post(Log.Id,qualIDsStaging, false);
                CMSIntegrationController.resetUpdateRequiredFlagQual(qualIDsStaging);
            }
        }
        
        if(qualIdsLive.size() > 0) {
            
            CMS_Integration_Log__c log = new CMS_Integration_Log__c(Enviroment__c = 'Live', Integration_Message__c = 'Processing' );
            insert log;
            
            //Tested outside of this trigger
            if(!Test.isRunningTest()) {
                CMSIntegrationController.post(Log.Id,qualIDsLive, true);
                CMSIntegrationController.resetUpdateRequiredFlagQual(qualIDsLive);
            }
            
        }
        
    }

}