trigger PostFeedOnChatter on Class_Enrolment__c (after insert, after update) {
	
	Set<Id> ceIdset = new Set<Id>();
	String groupId = [select Id from CollaborationGroup where Name='QLD - Bookings'].Id; 
	String userId = [select Id from User where Name='CASIS Alert'].Id;
	for(Class_Enrolment__c c : Trigger.new){
		if(c.Attendance_Code__c == 'A'){
			ceIdset.add(c.Id);
		}
	}
	createSignedRecordPost(ceIdset, groupId, userId);
	private List<FeedItem> createSignedRecordPost(Set<Id> ceIds, String grp, String usr)
	{
	    List<FeedItem> feedList = new List<FeedItem>();
	    List<Class_Enrolment__c> ceList = new List<Class_Enrolment__c>();
	    List<Purchasing_Contracts__c> pcList = new List<Purchasing_Contracts__c>();
	    Set<String> contractCodes = new Set<String>();
	    Map<String,Purchasing_Contracts__c> ceContractMap = new Map<String,Purchasing_Contracts__c>();
	    ceList = [Select Id, Student_Name__c, Enrolment_Intake_Unit__r.Enrolment__r.Contract_Code__c, Enrolment__r.Contract_Code__c, Enrolment_Intake_Unit__c from Class_Enrolment__c where Id IN: ceIds];
	    for(Class_Enrolment__c ce : ceList){
	    	if(ce.Enrolment_Intake_Unit__c != null){
	    		contractCodes.add(ce.Enrolment_Intake_Unit__r.Enrolment__r.Contract_Code__c);
	    	}
	    	else{
	    		contractCodes.add(ce.Enrolment__r.Contract_Code__c);
	    	}
	    }
	    pcList = [select Id, Contract_State__c, Contract_Type__c, Contract_Code__c FROM Purchasing_Contracts__c 
	    					WHERE Contract_Code__c IN: contractCodes AND Contract_State__c='Queensland' AND Contract_Type__c = 'Apprenticeship Funding'];
	    for(Purchasing_Contracts__c pc : pcList){
	    	ceContractMap.put(pc.Contract_Code__c, pc);
	    }
	    for(Class_Enrolment__c ce : ceList){//create new feed item to insert
	    	if(ceContractMap.containsKey(ce.Enrolment_Intake_Unit__r.Enrolment__r.Contract_Code__c) || ceContractMap.containsKey(ce.Enrolment__r.Contract_Code__c)){
	    		FeedItem post = new FeedItem();
			    post.ParentId = grp;
			    post.CreatedById = usr;
			    post.Body = 'Please check student progress due to absenteeism.';
			    post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + ce.Id;
			    post.Title = ce.Student_Name__c;
			    feedList.add(post);
	    	}
	    }
	    insert feedList;	
	    return feedList;
	}
}