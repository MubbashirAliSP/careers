/*If result assigned to EU changes, this trigger will check to identify
 *if result has been declared as code which should remove the unit from blackboard
 *sequence calculations and if so, will mark related blackboard course membership as remove from blackboard sequence = true
 *written by Yuri Gribanov - Cloudsherpas 23 July 2014
*/


trigger BlackboardMarkBBCMResulted on Enrolment_Unit__c (after update) {

	Set<Id>euIds = new Set<Id>();
	List<Blackboard_Course_Membership__c> bbcmToUpdate = new List<Blackboard_Course_Membership__c>();

	/*we will reduce number of records processed by checking if result has changed
	 *on enrolment unit */
	for(Enrolment_Unit__c eu : Trigger.New) {

		if(eu.Unit_Result__c != Trigger.OldMap.get(eu.id).Unit_Result__c)
			euIds.add(eu.Id);

	}

	for(Enrolment_Unit__c eu : [Select Unit_Result__r.National_Outcome__r.Remove_From_Blackboard_Sequence__c , (Select Remove_From_Blackboard_Sequence__c From Blackboard_Course_Memberships__r) From Enrolment_Unit__c where id in : euIds AND Unit_Result__r.National_Outcome__r.Remove_From_Blackboard_Sequence__c = true]) {

		for(Blackboard_Course_Membership__c bbcm : eu.Blackboard_Course_Memberships__r) {

			bbcm.Remove_From_Blackboard_Sequence__c = true;
			bbcmToUpdate.add(bbcm);
		}


	}


	update bbcmToUpdate;
	

}