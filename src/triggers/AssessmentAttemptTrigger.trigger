/**
 * @description Trigger definition for the `AssessmentAttempt__c` object
 * @author Biao Zhang
 * @date 11.NOV.2015
 */
trigger AssessmentAttemptTrigger on AssessmentAttempt__c (before insert, before update, after insert, after update, after delete) {
	if (trigger.isBefore) {
        if (trigger.isInsert) {
            AssessmentAttemptTriggerHandler.onBeforeInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            AssessmentAttemptTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            AssessmentAttemptTriggerHandler.onAfterInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            AssessmentAttemptTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }

        if (trigger.isDelete) {
            AssessmentAttemptTriggerHandler.onAfterDelete(trigger.old);
        }
    }
}