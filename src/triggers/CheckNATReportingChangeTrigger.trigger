/**
 * @description n/a
 * @author n/a
 * @date n/a
 * @history
 *       31.MAR.2016    Ranyel Maliwanag     - Added System Admin profile users to the validation exemption
 */
trigger CheckNATReportingChangeTrigger on Purchasing_Contracts__c (before update) {
    Id systemAdminProfileId = '00e90000000rnWH';

    if (Test.isRunningTest() || UserInfo.getProfileId() == systemAdminProfileId || UserInfo.getUserName().indexOf('casis-admin@cloudsherpas.com') < 0 ) { //allow CS admin to make this change... dataloader required
        for (Purchasing_Contracts__c c : Trigger.New) {
            if (c.Don_t_include_tuition_fees_in_NAT_report__c != Trigger.OldMap.get(c.id).Don_t_include_tuition_fees_in_NAT_report__c) {
                
                //i know this is not bulkified - but there's no way to batch this query over all the contracts
                //only triggering on changed anyway so unlikely this will trigger for any batches
                string id15 = ((string)c.id).substring(0, 15);
                
                Tuition_Fee_History__c[] tfhs = [ select id from Tuition_Fee_History__c Where Enrolment_Unit__r.Enrolment__r.Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c = :c.id or Enrolment_Unit__r.Enrolment__r.Line_Item_Qualification__r.Contract_Line_Item__r.Purchasing_Contract__c = :id15 limit 1];
                
                if (tfhs.size() > 0 ){
                    c.Don_t_include_tuition_fees_in_NAT_report__c.addError('Tutition Fee History already exists. Contact your system administrator if you need to change this field.');
                }
            }
        }
    }
}