/*
Author: Paolo Mendoza
Date:   23/1/2015
*/

trigger ProgramTrigger on Program__c (before insert, before update, after insert, after update) {

    
   
    if(trigger.isBefore){
        if(trigger.isUpdate){ 
           try{
               ProgramTriggerHandler.handleBeforeUpdate(Trigger.new);
           }
           catch(exception e){
               //
           }
        }    
        if(trigger.isInsert){
        
        }
    }
    
    if(trigger.isAfter){
        if(trigger.isInsert){
        
        }
        if(trigger.isUpdate){
        
        }
    
    }
    

}