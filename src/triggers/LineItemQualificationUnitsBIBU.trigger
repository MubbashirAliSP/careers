/**
 * @description 	Line Item Qualification Unit Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas		Created.
 */
trigger LineItemQualificationUnitsBIBU on Line_Item_Qualification_Units__c (before insert, before update) {
	List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'LIQ_User'){
		if(Trigger.isInsert){
			for(Line_Item_Qualification_Units__c l : Trigger.new){
				liquList.add(l);
			}
			LineItemQualificationUnitsTriggerHandler.populateRecordType(liquList);
		}
	}
	
}