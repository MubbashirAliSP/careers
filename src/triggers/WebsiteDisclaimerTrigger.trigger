trigger WebsiteDisclaimerTrigger on Website_Disclaimer__c (after update) {
    
    WebsiteDisclaimerTriggerHandler.onAfterUpdate(Trigger.New, Trigger.OldMap);

}