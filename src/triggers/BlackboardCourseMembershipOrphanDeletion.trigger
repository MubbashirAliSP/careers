/*if Enrolment Intake Unit is deleted, we need to delete related
 *BBCM where there is no primary key assigned to bbcm yet
 *written by Yuri Gribanov - Cloudsherpas 23 July 2014
*/



trigger BlackboardCourseMembershipOrphanDeletion on Enrolment_Intake_Unit__c (before delete) {

	Set<Id>eiuIds = new Set<Id>();
	List<Blackboard_Course_Membership__c>bbcmToDelete = new List<Blackboard_Course_Membership__c>();

	for(Enrolment_Intake_Unit__c eui : Trigger.Old) {

		eiuIds.add(eui.Id);

	}

	for(Blackboard_Course_Membership__c bbcm : [Select Id from Blackboard_Course_Membership__c Where Blackboard_Primary_Key__c = null And Enrolment_Intake_Unit__c in : eiuIds])
		bbcmToDelete.add(bbcm);


	delete bbcmToDelete;




}