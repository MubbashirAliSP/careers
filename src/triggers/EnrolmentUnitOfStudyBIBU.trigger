/**
 * @description Enrolment Unit of Study Object Trigger Before Insert, Before Update
 * @author      Janella Lauren Canlas
 * @Company     CloudSherpas
 * @date        14.JAN.2013
 *
 * HISTORY
 * - 14.JAN.2013    Janella Canlas      Created.
 * - 18.JAN.2013    Janella Canlas      Call Method populateHELMS558 upon insert/update from EnrolmentUnitOfStudyTriggerHandler
 * - 21.JAN.2013    Janella Canlas      Call method populateLFSI upon insert
 * - 22.JAN.2013    Janella Canlas      Call method populateCensusDate upon insert/update
 *                                      Call method populateCountryOfBirthCode upon insert/update
 * - 23.JAN.2013    Janella Canlas      Call method populateStudentFields (consolidated method of all field population from Student data)
 *                                      Removed populateLFSI (field converted to formula)
 *                                      Call method populateHigherEdProvider upon insert/update
 *                                      Call method validateRPLPriorStudy upon insert/update
 *                                      Call method to populate Field of Ed of prior VET upon insert/update
 * - 24.JAN.2013    Janella Canlas      Call method to populate Loan Fee upon insert/update
 *                                      Call method to populate Disablitiy fields upon insert (data from Enrolment)
 *                                      Call method to populate and contruct Disability code upon insert/update
 * - 31.JAN.2013    Janella Canlas      Call method to populate Credit Offered Value and Credit Used Value upon insert/update
 * - 12.FEB.2013    Janella Canlas      Call method to populate Aboriginal and Torres Strait Is. Status Code upon update
 */
 
//trigger EnrolmentUnitOfStudyBIBU on Enrolment_Unit_of_Study__c (before insert, before update) {
//    Set<Id> eUnitSet = new Set<Id>();
//    Set<Id> mSet = new Set<Id>();
//    List<Enrolment_Unit_of_Study__c> mList = new List<Enrolment_Unit_of_Study__c>();
//    List<Enrolment_Unit_of_Study__c> euosList = new List<Enrolment_Unit_of_Study__c>(); 
//    List<Enrolment_Unit_of_Study__c> euosAborList = new List<Enrolment_Unit_of_Study__c>();
//    list<Enrolment_Unit_of_Study__c> euosChangedResults = new list<Enrolment_Unit_of_Study__c>();
//    list<Enrolment_Unit_of_Study__c> censusDateUpdates = new list<Enrolment_Unit_of_Study__c>();

//    if(!Test.isRunningTest() || UserInfo.getName() == 'EUOS_User'){
//        if(Trigger.isInsert){
//            for(Enrolment_Unit_of_Study__c eus: Trigger.new){
//                if(eus.cloned__c  == false){
//                    eUnitSet.add(eus.Enrolment_Unit__c);
//                    euosList.add(eus);
                    
//                    if(eus.External_ID__c == null){
//                        mList.add(eus);
//                        mSet.add(eus.Enrolment_Unit__c);
//                     }
//                }
                
//            }
//            if(mList.size() > 0){
//                EnrolmentUnitOfStudyTriggerHandler.populateStudentFields(mSet, mList);//added jcanlas 09APR2013
//                EnrolmentUnitOfStudyTriggerHandler.populateDisabilityFields(mSet, mList); //added jcanlas 09APR2013                
//                EnrolmentUnitOfStudyTriggerHandler.populateCreditValues(mList);//added jcanlas 09APR2013
//                EnrolmentUnitOfStudyTriggerHandler.populateRPLFields(mList);//added jcanlas 09APR2013
//            }
//            EnrolmentUnitOfStudyTriggerHandler.validateRPLPriorStudy(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateHELMS558(euosList);//added jcanlas 01182013
//            EnrolmentUnitOfStudyTriggerHandler.populateCensusDate(eUnitSet, euosList);//added jcanlas 01222013
//            EnrolmentUnitOfStudyTriggerHandler.populateHigherEdProvider(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateFieldOfEdPriorStudy(eUnitSet, euosList); //added jcanlas 01232013
//            //EnrolmentUnitOfStudyTriggerHandler.populateLoanFee(euosList);//added jcanlas 01242013
////            EnrolmentUnitOfStudyTriggerHandler.populateDisabilityFields(eUnitSet, euosList); //added jcanlas 01242013
//            EnrolmentUnitOfStudyTriggerHandler.populateDisabilityCode(euosList);//added jcanlas 01242013
////            EnrolmentUnitOfStudyTriggerHandler.populateCreditValues(euosList);//added jcanlas 31JAN2013
////            EnrolmentUnitOfStudyTriggerHandler.populateRPLFields(euosList);//added jcanlas 31JAN2013
////            EnrolmentUnitOfStudyTriggerHandler.populateStudentFields(eUnitSet, euosList);//added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateLabourForceStatusCode(eUnitSet, euosList); //added jcanlas 01FEB2013
//            RevenueRecognitionEUOSEngine.populateStage(eUnitSet,euosList);//added by Neha Batra 20/03/2015
//        }

//        if(Trigger.isUpdate){
//            //system.debug('##isUpdate1');    
//            for(Enrolment_Unit_of_Study__c eus: Trigger.new){
//                eUnitSet.add(eus.Enrolment_Unit__c);
//                euosList.add(eus);
//                /*added jcanlas 12FEB2013*/
//                if(eus.Aboriginal_and_Torres_Strait_Is_Status__c != Trigger.oldMap.get(eus.Id).Aboriginal_and_Torres_Strait_Is_Status__c){
//                    //system.debug('##isUpdate2');                    
//                    euosAborList.add(eus);
//                    //system.debug('##euosAborList: '+euosAborList.size());
//                    //system.debug('##euosAborList: '+euosAborList);
//                }
//                if(eus.Result__c != Trigger.oldMap.get(eus.Id).Result__c){
//                    euosChangedResults.add(eus);
//                }
//                 if(eus.Census_Date__c != Trigger.oldMap.get(eus.Id).Census_Date__c){
//                    censusDateUpdates.add(eus);
//                }
//            }
//            /*added jcanlas 12FEB2013*/
//            if(euosAborList.size() > 0){
//                EnrolmentUnitOfStudyTriggerHandler.updateAboriginalCode(euosAborList);
//            }
//            EnrolmentUnitOfStudyTriggerHandler.validateRPLPriorStudy(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateHELMS558(euosList);//added jcanlas 01182013
//            EnrolmentUnitOfStudyTriggerHandler.populateHigherEdProvider(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateFieldOfEdPriorStudy(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateLoanFee(euosList); //added jcanlas 01242013
//            EnrolmentUnitOfStudyTriggerHandler.populateDisabilityCode(euosList);//added jcanlas 01242013
//            EnrolmentUnitOfStudyTriggerHandler.populateCreditValues(euosList);//added jcanlas 31JAN2013
//            EnrolmentUnitOfStudyTriggerHandler.populateStudentFields(eUnitSet, euosList); //added jcanlas 01232013
//            EnrolmentUnitOfStudyTriggerHandler.populateLabourForceStatusCode(eUnitSet, euosList); //added jcanlas 01FEB2013
//            EnrolmentUnitOfStudyTriggerHandler.cloneFailedEUOS(euosChangedResults);
//            RevenueRecognitionEUOSEngine.populateStage(eUnitSet,euosList);//added by Neha Batra 20/03/2015
//           // RevenueRecognitionEUOSEngine.generateERS(censusDateUpdates);
//        }
//    }
//}

trigger EnrolmentUnitOfStudyBIBU on Enrolment_Unit_of_Study__c (before insert, before update) {
    if(!Test.isRunningTest() || UserInfo.getName() == 'EUOS_User'){
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                EnrolmentUnitOfStudyTriggerHandler.onBeforeInsert(Trigger.new);
            }

            if(Trigger.isUpdate) {
                EnrolmentUnitOfStudyTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
        }
    }
}