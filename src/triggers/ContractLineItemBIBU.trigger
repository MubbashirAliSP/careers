/**
 * @description 	Contract Line Item Object Trigger Before Insert, Before Update
 * @author 			Janella Lauren Canlas
 * @Company			CloudSherpas
 * @date 			27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas		Created.
 */
trigger ContractLineItemBIBU on Contract_Line_Items__c (before insert, before update) {
	
	List<Contract_Line_Items__c> cliList = new List<Contract_Line_Items__c>();
	
	if(!Test.isRunningTest() || UserInfo.getName() == 'CLI_User'){
		if(Trigger.isInsert){
			for(Contract_Line_Items__c c: Trigger.new){
				cliList.add(c);
			}
			ContractLineItemTriggerHandler.populateRecordType(cliList);
		}
		
		if(Trigger.isUpdate){
			//nothing yet
		}
	}
}