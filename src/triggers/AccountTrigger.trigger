/**
 * @description Trigger for Account object
 * @author Ranyel Maliwanag
 * @date 01.JUL.2015
 */
trigger AccountTrigger on Account (before insert, before update, after update) {
	if (trigger.isBefore) {
		if (trigger.isInsert) {
			AccountTriggerHandler.onBeforeInsert(trigger.new);
		}

		if (trigger.isUpdate) {
			AccountTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
		}
	}

	if (trigger.isAfter) {
		if (trigger.isUpdate) {
			AccountTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}