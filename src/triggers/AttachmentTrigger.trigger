/**
*	@author	Nick Guia
*	@description Trigger for Attachment object
*	@history
*		AUG.18.2015		Nick Guia	Created
*/
trigger AttachmentTrigger on Attachment (before insert, after insert) {
	if(trigger.isBefore) {
		if(trigger.isInsert) {
			//before insert logic here
		}
	} else if(trigger.isAfter) {
		if(trigger.isInsert) {
			AttachmentTriggerHandler.onAfterInsert(Trigger.new, Trigger.oldMap);
		}
	}
}