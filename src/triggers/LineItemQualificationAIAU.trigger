/**
 * @description 	Line Item Qualification Object Trigger After Insert, After Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			23.NOV.2012
 *
 * HISTORY
 * - 23.NOV.2012	Janella Canlas		Created.
 */
trigger LineItemQualificationAIAU on Line_Item_Qualifications__c (after insert, after update) {
	List<Line_Item_Qualifications__c> LIQualList = new List<Line_Item_Qualifications__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'LIQ_User'){
		if(Trigger.isInsert){
			for(Line_Item_Qualifications__c l : Trigger.new){
				system.debug('****l.RecordType.Name'+l.RecordType.Name);
				Id recTypeId = [select id, Name, SobjectType from RecordType where Name='Unit Funded' and SobjectType ='Line_Item_Qualifications__c'].Id;
				LIQualList.add(l);
			}
			system.debug('****LIQualList'+LIQualList);
			LineItemQualificationTriggerHandler.insertLineItemQualificationUnits(LIQualList);
		}
	}
}