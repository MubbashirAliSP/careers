/**
 * @description     Account Object Trigger After Insert, After Update
 * @author          Vienna Austria
 * @Company         CloudSherpas
 * @date            31.OCT.2012
 *
 * HISTORY
 * - 11.OCT.2012    Vienna Austria      Created.
 * - 01.FEB.2013    Yuri Gribanov       Updated.
 * - 04.FEB.2013    Janella Canlas      Comment codes for populating Intake Letter Address
 * - 06.FEB.2013    Janella Canlas      Call method to populate Intake Student Mobile
 * - 08.AUG.2013    Michelle Magsarili  Fix calls on SOQL to avoid hitting SOQL limit
 * - 06.NOV.2013    Yuri Gribanov       AV7 Changes, added a call to Future method to update new address fields
 * - 09.MAY.2014    Warjie Malibago     Fix calls on SOQL to avoid hitting SOQL limit
 * - 23.OCT.2014    Sairah Hadjinoor    updated to call method that deletes all USI document when a student's USI is verified
 * - 24.MAR.2016    Biao Zhang          Reduce SOQL by getting RecordTypeID by schema
 */ 

trigger AccountAfterInsertAfterUpdate on Account (after insert, after update) {
    private static Id recordTypeSL = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student Lead').getRecordTypeId();
    private static Id recordTypeS = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student').getRecordTypeId();

    //MAM 08/08/2013 end 
    Set<Id> personIds = new Set<Id>();
    Set<Id> studentIds = new Set<Id>();
    Set<Id> euosIds = new Set<Id>();
    List<Account>AccountsForUpdate = new List<Account>();
    List<Account>studentList = new List<Account>();
    List<Account>euosList = new List<Account>();
    if(!Test.isRunningTest() || UserInfo.getName() == 'Account_User'){
        if(Trigger.isInsert) {
        system.debug('**Insert');
            for(Account p : trigger.New) {
                AccountsForUpdate.add(p);//added Janella Canlas 11062012 
                if(p.IsPersonAccount)
                    personIds.add(p.id);
                    system.debug('**Is Person Account: ' + p.id);
            }
            if(personIds.size() > 0){
                if(PersonAccountHandlerFuture.canIRun()) {
                    PersonAccountHandlerFuture.insertStudent(personIds);
                    PersonAccountHandlerFuture.mantainAVETMISS7AddressFields(personIds);
                }          
            }
        }
        
        if(Trigger.isUpdate) {
            system.debug('**IsUpdate');
            Set<Id>newPersonIds = new Set<Id>();
            Set<Id>  accIdsSet = new Set<Id>();
              for(Account p : trigger.New){                
                AccountsForUpdate.add(p);//added Janella Canlas 11062012
                Account oldAcc = Trigger.oldMap.get(p.Id);            
                                             
                if(p.PersonMobilePhone != oldAcc.PersonMobilePhone){
                    studentList.add(p);
                    studentIds.add(p.Id);
                }
                if(p.CHESSN__c != oldAcc.CHESSN__c || p.Tax_File_Number__c != oldAcc.Tax_File_Number__c || p.Country_of_Birth_Code__c != oldAcc.Country_of_Birth_Code__c || p.Main_Language_Spoken_at_Home_Identifier__c != oldAcc.Main_Language_Spoken_at_Home_Identifier__c){
                    euosIds.add(p.Id);
                    euosList.add(p);
                }
                system.debug('**Old Record Type Id: ' + oldAcc.RecordTypeId + ' ,recordTypeSL: ' + recordTypeSL);
                system.debug('**p Record Type Id: ' + p.RecordTypeId + ' ,recordTypeS: ' + recordTypeS);
                system.debug('**Account id: ' + p.id);
                if(oldAcc.RecordTypeId == recordTypeSL && p.RecordTypeId == recordTypeS){
                    personIds.add(p.Id);
                    system.debug('**Fit criteria!');
                }
                
                if(p.isPersonAccount)
                    newPersonIds.add(p.Id);
                
                if((p.Validated_by_Webservice__c && !Trigger.oldMap.get(p.Id).Validated_by_Webservice__c) || 
                    (p.Manually_Verified__c && !Trigger.oldMap.get(p.Id).Manually_Verified__c)){
                    accIdsSet.add(p.Id);
                }
                
            }
            //ReportingStateFields.copyMailingAddress(AccountsForUpdate); //added J.Canlas 11062012
            if(euosList.size() > 0){
                PersonAccountHandler.updateEUOS(euosList, euosIds);  
            }
            if(studentList.size() > 0){
                PersonAccountHandler.populateIntakeStudentMobile(studentIds, studentList);
            }
            
            if(newPersonIds.size() > 0 && PersonAccountHandlerFuture.canIRun()){
                PersonAccountHandlerFuture.mantainAVETMISS7AddressFields(newPersonIds);
            }    
            system.debug('**personIds: ' + personIds);
            if(!accIdsSet.isEmpty()){
                //S.Hadjinoor - update - 23/10/2014
                //call method that will delete the USI documents when the student is verified
                PersonAccountHandler.deleteAllUSIDocument(accIdsSet);
            }

            /*
            List<Account> accList = new List<Account>([SELECT Student_Identifer__c, Student_Id__c FROM Account WHERE Id IN: personIds]);
            system.debug('**accList1 size: ' + accList.size() + ' ,list: ' + accList);
            
            for(Account a: accList){
                a.Student_Identifer__c = a.Student_Id__c;
            }
            update accList;   
            */         
            //WBM Start 09.05.2014
            if(personIds.size() > 0){
                List<Account> accList = new List<Account>([SELECT Student_Identifer__c, Student_Id__c FROM Account WHERE Id IN: personIds]);
                system.debug('**accList1 size: ' + accList.size() + ' ,list: ' + accList);
                
                for(Account a: accList){
                    a.Student_Identifer__c = a.Student_Id__c;
                }
                update accList;
            }
            //WBM End
        }
    }
}