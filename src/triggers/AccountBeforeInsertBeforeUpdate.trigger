/**
 * @description Unit Hours and Points Trigger Handler 
 * @author      Vienna Austria
 * @Company     CloudSherpas
 * @date        11.OCT.2012
 *
 * HISTORY
 * - 11.OCT.2012    Vienna Austria      Created.
 * - 31.OCT.2012    Janella Canlas      Call updateResultingFields method upon insert/update
 * - 06.NOV.2012    Janella Canlas      Call copyMailingAddress method upon insert/update
 * - 07.NOV.2013    Yuri Gribanov       Updated original code, to use Trigger.Old and Trigger.New instead of building lists in loops
 * - 23.OCT.2014    Sairah Hadjinoor    Updated to set USI ID Document Submitted to false when  'Validated by Webservice' or 'Manually Verified' is changed to true
 */
trigger AccountBeforeInsertBeforeUpdate on Account (before insert, before update) {
    
    List<Account>personAccounts = new List<Account>();
    List<Account>personAccountsOld = new List<Account>();
    List<Account> oldAcctList = new List<Account>(); //added jcanlas 01142013
    
    List<Account>addressAcctList = new List<Account>();
    List<Account> oldAddressAcctList = new List<Account>();
    
    List<Account>stateAcctList = new List<Account>();
    List<Account> oldStateAcctList = new List<Account>();
    List<Account> lookupPopulateList = new List<Account>();
    if(!Test.isRunningTest() || UserInfo.getName() == 'Account_User'){
        if(Trigger.isInsert) {
            system.debug('**ACCOUNT INSERT!');
            /*Person Accounts Code Here */
            for(Account person : Trigger.New) {
                 if(person.isPersonAccount == true){
                    if(Trigger.New.size() == 1) {
                        if(PersonAccountHandler.validateTFN(person.Tax_File_Number__c) == false)
                            person.AddError('Invalid Tax File Number');
                    }
                    
                    if(person.Legacy_Student_ID__c != null){
                        person.Student_Identifer__c = person.Legacy_Student_ID__c;
                    } 
                    if(person.Main_Language_Spoken_at_Home__c == null || person.Country_of_Birth__c == null || person.Labour_Force_Status_1__c == null){
                        lookupPopulateList.add(person);
                    }
                }

            }       
           
            ReportingStateFields.copyMailingAddress_insert(Trigger.New); 
            ReportingStateFields.updateReportingStateFields(Trigger.New);
            if(lookupPopulateList.size() > 0){
                PersonAccountHandler.populatePersonAccountLookups(lookupPopulateList);
            }
        }
        
        if(Trigger.isUpdate) {
            system.debug('**ACCOUNT UPDATE!');
             for(Account person : Trigger.New) {
                if(Trigger.New.size() == 1) {
                    if(PersonAccountHandler.validateTFN(person.Tax_File_Number__c) == false)
                        person.AddError('Invalid Tax File Number'); 
                }
                //S.Hadjinoor - update - 23/10/2014
                //Set usi Id document to false when validated by webservice or manually verified flag is set to true
                if((person.Validated_by_Webservice__c && !Trigger.oldMap.get(person.Id).Validated_by_Webservice__c) || 
                    (person.Manually_Verified__c && !Trigger.oldMap.get(person.Id).Manually_Verified__c)){
                    person.USI_ID_Document_Submitted__c = false;
                }          
             }
            
            ReportingStateFields.copyMailingAddress_update(Trigger.New, Trigger.Old);
            ReportingStateFields.updateReportingStateFields(Trigger.New);
            
        }
    }
}