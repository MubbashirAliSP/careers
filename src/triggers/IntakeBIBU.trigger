/**
 * @description Intake Object Trigger Before Insert, Before Update
 * @author      Janella Lauren Canlas
 * @Company     CloudSherpas
 * @date        22.JAN.2013
 *
 * HISTORY
 * - 22.JAN.2013    Janella Canlas      Created.
 * - 05.FEB.2013    Janella Canlas      Call method populateVFHController.
 */
trigger IntakeBIBU on Intake__c (before insert, before update) {
    List<Intake__c> intakeList = new List<Intake__c>();
    Set<Id> intakeIdSet = new Set<Id>();
    if(Trigger.isInsert){
        for(Intake__c i : Trigger.new){
            intakeList.add(i);
            intakeIdSet.add(i.Id);
        }   
        //IntakeTriggerHandler.populateEUOSCensusDate(intakeIdSet, intakeList);
        IntakeTriggerHandler.populateVFHController(intakeIdSet, intakeList); //added jcanlas 05FEB2013
    }
    if(Trigger.isUpdate){
        for(Intake__c i : Trigger.new){
            if(i.VFH_Controller__c == null){
                intakeList.add(i);
                intakeIdSet.add(i.Id);
            }        
        }   
        //IntakeTriggerHandler.populateEUOSCensusDate(intakeIdSet, intakeList);
        if(intakeList.size() > 0){
            IntakeTriggerHandler.populateVFHController(intakeIdSet, intakeList); //added jcanlas 05FEB2013
        }
    }
}