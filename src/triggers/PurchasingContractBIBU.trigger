/**
 * @description 	Purchasing Contract Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			06.DEC.2012
 *
 * HISTORY
 * - 06.DEC.2012	Janella Canlas		Created.
 */
trigger PurchasingContractBIBU on Purchasing_Contracts__c (before insert, before update) {
	List<Purchasing_Contracts__c> pcList = new List<Purchasing_Contracts__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'PCON_User'){
		if(Trigger.isInsert){
			for(Purchasing_Contracts__c p : Trigger.new){
				pcList.add(p);
			}
			PurchasingContractTriggerHandler.populateNFSC(pcList);
		}
		if(Trigger.isUpdate){
			for(Purchasing_Contracts__c p : Trigger.new){
				pcList.add(p);
			}
			PurchasingContractTriggerHandler.populateNFSC(pcList);
		}
	}
}