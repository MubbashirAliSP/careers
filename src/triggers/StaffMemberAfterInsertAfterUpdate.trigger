trigger StaffMemberAfterInsertAfterUpdate on Staff_Member__c (after insert, after update) {
	List<Staff_Member__c> staffList = new List<Staff_Member__c>();
	if(Trigger.isInsert){
		for(Staff_Member__c s : Trigger.new){
			staffList.add(s);
		}
		StaffMemberTriggerHandler.insertClassStaffMember(staffList);
	}
	else if(Trigger.isUpdate){
		List<Staff_Member__c> stListCh = new List<Staff_Member__c>();
		List<Staff_Member__c> stListUn = new List<Staff_Member__c>();
		for(Staff_Member__c sm : Trigger.new){
			Staff_Member__c oldSM = Trigger.oldMap.get(sm.Id);
			if(sm.Add_staff_member_to_all_classes__c != oldSM.Add_staff_member_to_all_classes__c && sm.Add_staff_member_to_all_classes__c == true){
				stListCh.add(sm);
			}
			else if(sm.Add_staff_member_to_all_classes__c != oldSM.Add_staff_member_to_all_classes__c && sm.Add_staff_member_to_all_classes__c == false){
				stListUn.add(sm);
			}
		}
		StaffMemberTriggerHandler.insertClassStaffMember(stListCh);
		StaffMemberTriggerHandler.deleteClassStaffMember(stListUn);
	}
}