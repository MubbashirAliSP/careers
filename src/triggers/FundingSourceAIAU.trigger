/**
 * @description 	Funding Source Object Trigger After Insert, After Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			26.NOV.2012
 *
 * HISTORY
 * - 26.NOV.2012	Janella Canlas		Created.
 */
trigger FundingSourceAIAU on Funding_Source__c (after insert, after update) {
	List<Funding_Source__c> fundList = new List<Funding_Source__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'FS_User'){
		if(Trigger.isInsert){
			//nothing yet
		}
		if(Trigger.isUpdate){
			for(Funding_Source__c f : Trigger.new){
				Funding_Source__c oldFS = Trigger.oldMap.get(f.Id);
				system.debug('oldFS*** '+ oldFS);
				if(f.State__c != oldFS.State__c){
					fundList.add(f);
				}
			}
			system.debug('fundList*** '+ fundList);
			if(fundList.size() > 0){
				FundingSourceTriggerHandler.updateLIQU(fundList);
			}
		}
	}
	
}