trigger UnitOfStudyAfterInsertAfterUpdate on Unit__c (after insert, after update) {
	
	if(Trigger.isInsert) {
		Id recTypeId = [Select Id From RecordType Where DeveloperName = 'Unit_Of_Study' AND SObjectType = 'Unit__c' LIMIT 1].Id;
		Set<Id>unitsOfCompIds = new Set<Id>();
		List<Unit__c>UnitsOfCompList = new List<Unit__c>();
		List<Unit_Hours_and_Points__c>newUnitsPointsHoursList = new List<Unit_Hours_and_Points__c>();
		List<Unit_Hours_and_Points__c>newUnitsPointsHoursListToInsert = new List<Unit_Hours_and_Points__c>();
		
		for(Unit__c unit : Trigger.New) {
			if(unit.RecordTypeId == recTypeId)
				unitsOfCompIds.add(unit.Parent_UoC__c);
		}
		
		system.debug('@@@@size' + unitsOfCompIds.size());
		
		
		if(unitsOfCompIds.size() > 0) {
			
			newUnitsPointsHoursList = [Select u.Unit__c, u.Unit_Record_Type__c, u.Tuition_Fee_Hours__c, u.Points__c, u.Placement_Hours__c, u.Nominal_Hours__c, u.Name, u.Institutional_Hours__c, u.State__c From Unit_Hours_and_Points__c u Where u.Unit__c in :unitsOfCompIds];
			
			system.debug('@@@@size2' + newUnitsPointsHoursList.size());
			
			for(Unit__c unit : Trigger.New) {
				for(Unit_Hours_and_Points__c unitsHours : newUnitsPointsHoursList) {
					if(unit.Parent_UoC__c == unitsHours.Unit__c) {
						Unit_Hours_and_Points__c hours = unitsHours.Clone(false);
			    		hours.Unit__c = unit.Id;
			    		newUnitsPointsHoursListToInsert.add(hours);
					}
				}
			}
			
			system.debug('@@@@size3' + newUnitsPointsHoursListToInsert.size());
			insert newUnitsPointsHoursListToInsert;
		}
	
	}
	
}