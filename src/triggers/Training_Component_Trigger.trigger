trigger Training_Component_Trigger on Training_Component__c (before update, before insert) {

    //update all fields of education
    Set<String> codes = new Set<String>();
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Fields_of_Education__c == null && tc.Fields_of_Education_Code__c != null ){
            codes.add(tc.Fields_of_Education_Code__c);
        }
    }
    Map<String, Id> lookup = new Map<String, Id>();
    for ( Field_of_Education__c f : [ select Id, FoE_Code__c From Field_of_Education__c Where FoE_Code__c in :codes ] ){
        lookup.put(f.FoE_Code__c, f.id);
    }
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Fields_of_Education__c == null && tc.Fields_of_Education_Code__c != null ){
            tc.Fields_of_Education__c = lookup.get(tc.Fields_of_Education_Code__c);
        }
    }
    
    
    //match to existing units
    codes = new Set<String>();
    lookup = new Map<String, Id>();
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Unit' && tc.Unit__c == null && tc.Code__c != null ){
            codes.add(tc.Code__c);
        }
    }
    for ( Unit__c u : [ select Id, Name From Unit__c Where Name in :codes ] ){
        lookup.put(u.Name, u.id);
    }
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Unit' && tc.Unit__c == null && tc.Code__c != null ){
            tc.Unit__c = lookup.get(tc.Code__c);
        }
    }
    
    
    //match to existing training package units
    codes = new Set<String>();
    lookup = new Map<String, Id>();
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Unit' && tc.Training_Package_Unit__c == null && tc.Code__c != null ){
            codes.add(tc.Code__c);
        }
    }
    for ( Training_Package_Unit__c u : [ select Id, National_Code__c From Training_Package_Unit__c Where National_Code__c in :codes ] ){
        lookup.put(u.National_Code__c, u.id);
    }
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Unit' && tc.Training_Package_Unit__c == null && tc.Code__c != null ){
            tc.Training_Package_Unit__c = lookup.get(tc.Code__c);
        }
    }
    
    
    
    //match to existing training packages
    codes = new Set<String>();
    lookup = new Map<String, Id>();
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'TrainingPackage' && tc.Training_Package__c == null && tc.Code__c != null ){
            codes.add(tc.Code__c);
        }
    }
    for ( Training_Package__c u : [ select Id, Name From Training_Package__c Where Name in :codes ] ){
        lookup.put(u.Name, u.id);
    }
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'TrainingPackage' && tc.Training_Package__c == null && tc.Code__c != null ){
            tc.Training_Package__c = lookup.get(tc.Code__c);
        }
    }
    
    
    
    //match to existing qualifications
    codes = new Set<String>();
    lookup = new Map<String, Id>();
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Qualification' && tc.Qualification__c == null && tc.Code__c != null ){
            codes.add(tc.Code__c);
        }
    }
    for ( Qualification__c u : [ select Id, Name From Qualification__c Where Name in :codes ] ){
        lookup.put(u.Name, u.id);
    }
    for ( Training_Component__c tc : Trigger.New ){
        if ( tc.Component_Type__c == 'Qualification' && tc.Qualification__c == null && tc.Code__c != null ){
            tc.Qualification__c = lookup.get(tc.Code__c);
        }
    }
}