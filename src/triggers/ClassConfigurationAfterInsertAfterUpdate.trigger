/**
 * @description 	Class Configuration Trigger After Insert, After Update, Before Delete
 * @author 			Janella Lauren Canlas
 * @Company			CloudSherpas
 * @date 			05.NOV.2013
 *
 * HISTORY
 * - 05.NOV.2013	Janella Canlas		Created.
 * - 09.Jan.2014    Yuri Gribanov - Added one of class configuration. Other code untouched */

trigger ClassConfigurationAfterInsertAfterUpdate on Class_Configuration__c (after insert, after update, before delete) {
	
	List<Class_Configuration__c> configList = new List<Class_Configuration__c>();
	List<Class_Configuration__c> oldConfigList = new List<Class_Configuration__c>();
	Set<Id> configIds = new Set<Id>();
	
	Id recordtypeId = [Select Id From Recordtype Where DeveloperName = 'One_Off_Class'].Id;
	
	if(Trigger.isInsert){ /*One off Class Configuration call */
		if(Trigger.New.Size() == 1 && Trigger.New.get(0).RecordTypeId == recordtypeId) {
			
			ClassConfigurationTriggerHandler.insertOneOffClassRecords(Trigger.New);
		
		}
		
		for(Class_Configuration__c c : Trigger.new){
			configList.add(c);
		}
		ClassConfigurationTriggerHandler.insertClassRecords(configList);
	}
	
	else if(Trigger.isUpdate){
		for(Class_Configuration__c c : Trigger.new){
			configIds.add(c.Id);
			configList.add(c);
			oldConfigList.add(Trigger.oldMap.get(c.Id));
		}
		ClassConfigurationTriggerHandler.updateClassRecords(configList,oldConfigList);
	}
	else if(Trigger.isDelete){
		for(Class_Configuration__c c : Trigger.old){
			configIds.add(c.Id);
		}
		ClassConfigurationTriggerHandler.deleteClassRecords(configIds);
	}
}