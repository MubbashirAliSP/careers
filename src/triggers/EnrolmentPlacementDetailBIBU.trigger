trigger EnrolmentPlacementDetailBIBU on Enrolment_Placement_Detail__c (before insert, before update) {
	List<Enrolment_Placement_Detail__c> epdList = new List<Enrolment_Placement_Detail__c>();
	if(Trigger.isInsert){
		for(Enrolment_Placement_Detail__c e : Trigger.new){
			epdList.add(e);
		}
		EnrolmentPlacementDetailHandler.populateStudent(epdList);		
	}
}