/**
 * @description 	Intake Unit Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			14.DEC.2012
 *
 * HISTORY
 * - 14.DEC.2012	Janella Canlas		Created.
 */
trigger IntakeUnitBIBU on Intake_Unit__c (before insert, before update) {
	List<Intake_Unit__c> iuList = new List<Intake_Unit__c>();
	if(Trigger.isInsert){
		for(Intake_Unit__c i : Trigger.new){
			iuList.add(i);
		}
		IntakeUnitTriggerHandler.populateDate(iuList);
	}
}