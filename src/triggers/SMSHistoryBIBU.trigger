trigger SMSHistoryBIBU on smagicinteract__smsMagic__c (after insert) {
    List<smagicinteract__smsMagic__c> smsHistoryList = new List<smagicinteract__smsMagic__c>();
    Set<Id> smsHistoryId = new Set<Id>();
    if(Trigger.isAfter && Trigger.isInsert){
        for(smagicinteract__smsMagic__c s : Trigger.new){
            if(s.Account__c == null && s.Intake_Students__c != null){
                smsHistoryList.add(s);
                smsHistoryId.add(s.Id);
            }
        }
        if(smsHistoryList.size() > 0){
            SMSHistoryTriggerHandler.relateRecordToAccount(smsHistoryList, smsHistoryId);
        }
    }
}