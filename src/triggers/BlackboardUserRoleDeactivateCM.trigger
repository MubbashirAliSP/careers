trigger BlackboardUserRoleDeactivateCM on Blackboard_User_Role__c (before update) {
	
	Set<Id>bbuIds = new Set<Id>();
    
	List<Blackboard_Course_Membership__c> bbcmToUpdate = new List<Blackboard_Course_Membership__c>();
    
    List<Blackboard_Course_Membership__c> bbcmReactivate = [ SELECT Id, 
                                                           	 Blackboard_User_Role__r.Id
                                                             FROM Blackboard_Course_Membership__c 
                                                             WHERE Blackboard_User_Role__c IN :trigger.new AND (Create__c = true OR Reactivate__c = true) AND RecordType.DeveloperName = 'Student_T3'];
   	Set<Id> bbcmReactivateIds = new Set<Id>();
    
    for(Blackboard_Course_Membership__c bbcm : bbcmReactivate ) {
        bbcmReactivateIds.add(bbcm.Blackboard_User_Role__r.Id);
    }
    
	
	for(Blackboard_User_Role__c bbur : Trigger.New) {
        system.debug(bbcmReactivateIds);
        
        if(bbcmReactivateIds.size() > 0) {
            if( !bbcmReactivateIds.contains(bbur.Id) && bbur.Active_Student_T3_Memberships__c == 0 && Trigger.OldMap.get(bbur.Id).Active_Student_T3_Memberships__c > 0) {
                bbuIds.add(bbur.Blackboard_User__c);
                
            }
        }
        if(bbcmReactivateIds.size() == 0) {  
            if(bbur.Active_Student_T3_Memberships__c == 0 && Trigger.OldMap.get(bbur.Id).Active_Student_T3_Memberships__c > 0)
                bbuIds.add(bbur.Blackboard_User__c); 
        }
    }
	
	for(Blackboard_Course_Membership__c bbcm : [SELECT Enable_Course_Membership__c
												FROM Blackboard_Course_Membership__c
												WHERE Blackboard_User_Role__r.Blackboard_User__c in : bbuIds AND
												      Enable_Course_Membership__c = True AND
												      (RecordType.DeveloperName = 'Student_T4' OR RecordType.DeveloperName = 'Student_T5')]) {
		
		bbcm.Enable_Course_Membership__c = False;
		
		bbcmToUpdate.add(bbcm);
		
	}
	
	update bbcmToUpdate;

}