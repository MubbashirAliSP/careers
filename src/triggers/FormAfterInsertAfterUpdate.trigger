trigger FormAfterInsertAfterUpdate on Form__c (after insert, after update) {
    
    /*This code is for online application form processing.
     *Once form is submitted, this code will not be executed
     *by YG - Cloudsherpas
     */
    if(Trigger.New.size() == 1) {
        if(Trigger.New.get(0).Double_Diploma__c != null /*&& Trigger.New.get(0).Form_Status__c == 'Draft'*/) {
            Double_Diploma_Mapping__c dd = [Select Qualification1__c, Qualification2__c From Double_Diploma_Mapping__c Where Id = : Trigger.New.get(0).Double_Diploma__c];
            Form__c form = Trigger.New.get(0).Clone(false);
            form.VET_Qualification__c = dd.Qualification2__c;
            form.Related_Form__c = Trigger.New.get(0).Id;
            form.Double_Diploma__c = null;
            form.token__c = null;
            if(Trigger.isUpdate) {
                //Form__c existingRelatedForm = [Select Id From Form__c Where Related_Form__c = : Trigger.New.get(0).Id];
                //form.Id = existingRelatedForm.Id;
                for(Form__c existingRelatedForm: [Select Id From Form__c Where Related_Form__c = : Trigger.New.get(0).Id] ) {
                    form.Id = existingRelatedForm.Id;
                }
            }
            
            upsert form;

        }
        
        //========================================================================
        //Map the form status field to the form status of the parent service case
        //=========================================================================
        
        Map<Id, String> serviceCaseStatusMap = new Map<Id, String>();
        
        for(Form__c f: trigger.new) {
            if(trigger.isAfter&&trigger.isInsert) {
                serviceCaseStatusMap.put(f.service_case__c, f.form_status__c);
            } else if (trigger.isAfter&&trigger.isUpdate) {
                //add only if the form status had been updated
                if(f.form_status__c!=trigger.oldMap.get(f.Id).form_status__c) {
                    serviceCaseStatusMap.put(f.service_case__c, f.form_status__c);
                }
            }
        }
        
        if(serviceCaseStatusMap.size()>0) {
        
            List<Service_Cases__c> scToUpdate = new List<Service_Cases__c>();
            for(Service_Cases__c sc: [Select Id, related_form_status__c from service_cases__c where id IN: serviceCaseStatusMap.keySet()]){
                sc.related_form_status__c = serviceCaseStatusMap.get(sc.Id);
                scToUpdate.add(sc);
            }
            
            if(scToUpdate.size()>0) {
                update scToUpdate;
            }
        
        }
        
    }
    
}