/**
 * @description 	Intake Unit of Study Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			07.JAN.2013
 *
 * HISTORY
 * - 07.JAN.2013	Janella Canlas		Created.
 */
trigger IntakeUnitOfStudyBIBU on Intake_Unit_of_Study__c (before insert, before update) {
	List<Intake_Unit_of_Study__c> iusList = new List<Intake_Unit_of_Study__c>();
	if(Trigger.isInsert){
		for(Intake_Unit_of_Study__c i : trigger.new){
			iusList.add(i);
		}
		IntakeUnitOfStudyTriggerHandler.populateScheduledHours(iusList);
	}
}