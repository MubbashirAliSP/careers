/**
 * @description 	Class Object Trigger After Insert, After Update
 * @author 			Janella Lauren Canlas
 * @Company			CloudSherpas
 * @date 			06.NOV.2012
 *
 * HISTORY
 * - 06.NOV.2012	Janella Canlas		Created.
 */
trigger ClassesAfterInsertAfterUpdate on Classes__c (after insert, after update) {
	List<Classes__c> classList = new List<Classes__c>();
	
	if(Trigger.isInsert){
		for(Classes__c c : Trigger.new){
			classList.add(c);
		}
		ClassesTriggerHandler.insertClassStaffMembers(classList);
	}
	
	if(Trigger.isUpdate){
		//nothing yet		
	}
}