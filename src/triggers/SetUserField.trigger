/**
 * @description Trigger for Case Object populates 'Submitted by' field 
 * @author Thiru Sandaran
 * @date 21.JAN.2015
 */

/*

13/08/2015 - Troy Collins - Trigger modifications
	1) Added author details
	2) Formatting updated to meet code readability standards
*/

trigger SetUserField on Case (before insert, before update) {
	for(Case c : Trigger.new){
		List<User> submittedByUser = [SELECT Id FROM User WHERE Email=: c.SuppliedEmail LIMIT 1];
		if(submittedByUser != null && submittedByUser.size()>0){
			c.Submitted_By_User__c = submittedByUser[0].Id;
		}
	}   
}