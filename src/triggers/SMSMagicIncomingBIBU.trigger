trigger SMSMagicIncomingBIBU on smagicinteract__Incoming_SMS__c (before insert, before update) {
    List<Account> accountList = new List<Account>();               
    String senderMobileNumber = null;                              
    String senderMobileNumber2 = null;                             
    String senderMobileNumber3 = null;    
    String senderMobileNumber4 = null;                             
    //Jyoti changes Set<String> sMobileNoSet = new Set<String>();
    List<String> sMobileNoArg = new List<String>(); //Jyoti changes
    if(Trigger.isInsert){
        for (smagicinteract__Incoming_SMS__c sms : Trigger.new){                     
            senderMobileNumber = sms.smagicinteract__Mobile_Number__c;        
            if(senderMobileNumber.length() ==12    &&senderMobileNumber.substring(0,2) == '44'){
                senderMobileNumber = senderMobileNumber.substring(2,12);
                sMobileNoArg.add(senderMobileNumber); //Jyoti changes
                 system.debug(system.logginglevel.INFO,'senderMobileNumber '+senderMobileNumber );
            }                                                              
            if(senderMobileNumber.length() ==10){                          
                senderMobileNumber = '0'+senderMobileNumber;  
                 system.debug(system.logginglevel.INFO,'senderMobileNumber '+senderMobileNumber );
                sMobileNoArg.add(senderMobileNumber);  //Jyoti changes          
            }                                                              
            senderMobileNumber2 = senderMobileNumber.substring(0,5) + ' '+senderMobileNumber.substring(6,11); 
             system.debug(system.logginglevel.INFO,'senderMobileNumber2@@ '+senderMobileNumber2 );
            sMobileNoArg.add(senderMobileNumber2);      //Jyoti changes                
            senderMobileNumber3 = senderMobileNumber.substring(1,11);
             system.debug(system.logginglevel.INFO,'senderMobileNumber3## '+senderMobileNumber3 );
            sMobileNoArg.add(senderMobileNumber3); //Jyoti changes
            senderMobileNumber4 = '0'+senderMobileNumber.substring(2,11);
             system.debug(system.logginglevel.INFO,'senderMobileNumber4## '+senderMobileNumber4 );
            sMobileNoArg.add(senderMobileNumber4 );
            system.debug('**Set: ' + sMobileNoArg); //Jyoti changes
            //accountList = [select Id, Name from Account where PersonMobilePhone = :senderMobileNumber or PersonMobilePhone =:senderMobileNumber2 or PersonMobilePhone = :senderMobileNumber3];
            //02 JUNE 2013
            //Jade Serrano
            //changed PersonMobilePhone to SMS_Mobile_Number__c because of number format issue
            //accountList = [select Id, Name from Account where SMS_Mobile_Number__c = :senderMobileNumber or SMS_Mobile_Number__c =:senderMobileNumber2 or SMS_Mobile_Number__c = :senderMobileNumber3]; 
           /*Jyoti changes** 
           accountList = [select Id, Name from Account where SMS_Mobile_Number__c in: sMobileNoSet ];
            if(accountList.size() >0){                                     
                sms.Account__c = accountList[0].Id;                        
            }    Jyoti changes*/
            
             //Jyoti changes start
            String searchquery ='';
            for(integer i=0; i<sMobileNoArg.size();i++){
                searchquery += ' OR {'+i+'}';
            }
            if(searchquery.startsWith(' OR')){
                searchquery = searchquery.subString(3);
                if(searchquery != null)
                    searchquery = searchquery.trim();
            }
            searchquery = String.format(searchquery, sMobileNoArg);
            system.debug(system.logginglevel.INFO,'searchquery'+searchquery); 
            List<List<sObject>> sobjList = [FIND :searchquery IN ALL FIELDS RETURNING Account(Id)]; 
            system.debug(system.logginglevel.INFO,'sobjList ***'+sobjList );
            List<Account> accList = sobjList [0];
                if(accList.size() > 0){
                sms.Account__c= accList [0].Id;
                }
             //Jyoti changes end                                             
        }
    }
}