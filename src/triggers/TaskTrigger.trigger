/**
 * @description: Trigger for Task object
 * @author Ranyel Maliwanag
 * @date 12.MAY.2015
 */
trigger TaskTrigger on Task (before insert, before update, after insert, after update, after delete) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            TaskTriggerHandler.onBeforeInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            TaskTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
    
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            TaskTriggerHandler.onAfterInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            TaskTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }

        if (trigger.isDelete) {
            TaskTriggerHandler.onAfterDelete(trigger.old);
        }
    }
}