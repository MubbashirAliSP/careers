/**
 * @description     Enrolment Unit Object Trigger Before Insert, Before Update
 * @author          Yuri Gribanov
 * @Company         CloudSherpas
 * @date            19.OCT.2012
 *
 * HISTORY
 * - 19.OCT.2012    Yuri Gribanov       Created.
 * - 07.NOV.2012    Janella Canlas      Added method calls to populate Enrolment Unit fields
 * - 24.JAN.2013    Janella Canlas      Added method to call populateCampusLocation upon update
 * - 20.FEB.2013    Janella Canlas      call populateUnitValue upon insert/update   
 * - 08.MAY.2013    Jade Serrano        Added after insert
 * - 19.NOV.2014    Nick Guia           Modified trigger structure to prevent redundant loops
 * - 16.DEC.2014    Neha Batra			Updated code for unit supersession logic
 */
trigger EnrolmentUnitBeforeInsertBeforeUpdate on Enrolment_Unit__c (before insert, before update, after insert) {
    if(!Test.isRunningTest() || UserInfo.getName() == 'EnrolmentUnit_User'){

        if (Trigger.isBefore) {
            if(Trigger.isInsert) {
                EnrolmentUnitTriggerHandler.onBeforeInsert(Trigger.new);                
            }

            if(Trigger.isUpdate) {
                EnrolmentUnitTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                //Neha Batra 16.DEC.2014
                //call enrolmentUnitSupersession method
                EnrolmentUnitTriggerHandler.enrolmentUnitSupersession(Trigger.new, Trigger.oldMap);
            }
        }
    }
}