trigger ResultRequestTrigger on Result_Request__c (after update) {

	List<Enrolment_Unit__c> euToUpdate = new List<Enrolment_Unit__c>();
	List<Enrolment_Unit_Of_Study__c> euosToUpdate = new List<Enrolment_Unit_Of_Study__c>();

	Map<Id,Result_Request__c>enrol_unitMap = new Map<Id,Result_Request__c>();
	Map<Id,Id>enrolment_unit_of_studyMap = new Map<Id,Id>();

	for(Result_Request__c res : Trigger.New) {

		if(res.Approval_status__c == 'Approved' && Trigger.oldMap.get(res.Id).Approval_status__c != 'Approved') {

			enrol_unitMap.put(res.Enrolment_Unit__c, res);
			enrolment_unit_of_studyMap.put(res.Enrolment_Unit_Of_Study__c, res.Enrolment_Unit_Of_Study_Result__c);

		}
	}

	

	List<Delivery_Mode_Types__c> delModeType = new List<Delivery_Mode_Types__c>([Select Id From Delivery_Mode_Types__c Where Code__c = '90' LIMIT 1 ]);

	for(Enrolment_Unit__c eu : [Select Unit_Result__c,End_Date__c,AVETMISS_National_Outcome__c,Delivery_Mode__r.Code__c From Enrolment_Unit__c Where id in : enrol_unitMap.KeySet()]) {
		eu.Unit_Result__c = enrol_unitMap.get(eu.Id).Enrolment_Unit_Result__c;
		eu.End_Date__c = enrol_UnitMap.get(eu.Id).End_Date_For_Approval__c;

		

			if(delModeType.size() == 1 && enrol_unitMap.get(eu.Id).Update_Eu_Delivery_Mode__c == true )
				eu.Delivery_Mode__c = delModeType.get(0).Id;
			
		
			
		
		euToUpdate.add(eu);
	}

	for(Enrolment_Unit_Of_Study__c euos : [Select Result__c from Enrolment_Unit_Of_Study__c where id in : enrolment_unit_of_studyMap.KeySet()]) {

		euos.Result__c = enrolment_unit_of_studyMap.get(euos.Id);
		euosToUpdate.add(euos);
	}

	update euToUpdate;
	update euosToUpdate;




}