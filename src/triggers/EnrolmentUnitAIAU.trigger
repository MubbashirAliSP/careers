/**
 * @description     Enrolment Unit Object Trigger After Insert, After Update
 * @author          Yuri Gribanov
 * @Company         CloudSherpas
 * @date            24.JAN.2013
 *
 * @history
 * - 24.JAN.2013    Janella Canlas      Created.
 * - 28.AUG.2014    Warjie Malibago    add populateDeliveryModeType
 * - 29.AUG.2014    Warjie Malibago    add populateEUOSResult
 * - 19.NOV.2014    Nick Guia          Modified trigger structure to prevent redundant loops
 */
trigger EnrolmentUnitAIAU on Enrolment_Unit__c (after insert, after update) {
    if(!Test.isRunningTest() || UserInfo.getName() == 'EnrolmentUnit_User'){

        if (Trigger.isAfter) {
            if(Trigger.isInsert) {
                //Uncomment this when you will need a function for after insert
                //EnrolmentUnitTriggerHandler.onAfterInsert(Trigger.new);
            }

            if(Trigger.isUpdate) {
                EnrolmentUnitTriggerHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
            }
        }
    }
    
}