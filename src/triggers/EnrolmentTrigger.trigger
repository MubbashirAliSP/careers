trigger EnrolmentTrigger on Enrolment__c (before insert, before update, after update, after insert) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            EnrolmentTriggerHandler.onBeforeInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            EnrolmentTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }

    if (trigger.isAfter) {
        if (trigger.isInsert) {
            EnrolmentTriggerHandler.onAfterInsert(trigger.new);
        }
        
        if (trigger.isUpdate) {
            EnrolmentTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}