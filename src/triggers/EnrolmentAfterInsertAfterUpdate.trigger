/**
 * @description     Enrolment Object Trigger After Insert, After Update
 * @author          Janella Lauren Canlas
 * @Company         CloudSherpas
 * @date            31.OCT.2012
 *
 * HISTORY
 * - 31.OCT.2012    Janella Canlas      Created.
 * - 17.JAN.2013    Janella Canlas      Call updateEUOS method after update
 * - 01.FEB.2013    Janella Canlas      Call populateStudentStatus AIAU
 * - 04.FEB.2013    Janella Canlas      Call method to populateIntakeLetterAddress (after update)
 * -                Janella Canlas      Comment codes for populateIntakeLetterAddress
 * - 06.MAY.2013    Jade Serrano        Added validation for Credit_Offered_Value_full_time_load__c, Credit_Used_Value_full_time_load__c, Field_of_Ed_of_prior_VET_Credit_RPL__c
 * - 11.JUN.2014    Warjie Malibago     Removed unnecessary codes
 * - 28.JUL.2014    Warjie Malibago     Code optimization; use map instead of SELECT inside loop
 * - 01.AUG.2014    Warjie Malibago     Pass Student id to deleteClassEnrolment; see WBM
 */ 
trigger EnrolmentAfterInsertAfterUpdate on Enrolment__c (after insert, after update) {
    Set<Id> qualificationIds = new Set<Id>();
    Set<Id> enrolmentIds = new Set<Id>();
    Set<Id> statusIds = new Set<Id>();
    Set<Id> euosIds = new Set<Id>();
    List<Enrolment__c> eList = new List<Enrolment__c>();
    List<Enrolment__c> euosList = new List<Enrolment__c>();
    List<Enrolment__c> statusList = new List<Enrolment__c>();
    List<Account> personAccounts = new List<Account>();
    Set<Id> enrolmentIdsforeu = new Set<Id>();
    Set<Id> studentId = new Set<Id>(); //01AUG2014 WBM
    Map<Id, Enrolment__c> enrlMap = new Map<Id, Enrolment__c>(); //28JUL2014 WBM
    /*Set<Id> eIds = new Set<Id>();
    List<Enrolment__c> enrolmentList = new List<Enrolment__c>();*/
    try{
    if(!Test.isRunningTest() || UserInfo.getName() == 'Enrolment_User'){
        if(Trigger.isInsert){
            system.debug('**EN AFTER INSERT!');
            Map<Id, String>StudentWithStudyReason = new Map<Id,String>();
            for(Enrolment__c e : Trigger.new){
                if(e.External_Id__c == null){
                    enrolmentIdsforeu.add(e.Id);
                    qualificationIds.add(e.Qualification__c);
                }
                enrolmentIds.add(e.Id);
                StudentWithStudyReason.put(e.Student__c, e.Study_Reason__c);
                eList.add(e);
                //e.Student__r.Study_Reason_Identifier__c = e.Study_Reason__c.substring(0,2);
            }
            system.debug('enrolmentIdsforeu'+enrolmentIdsforeu);
            system.debug('qualificationIds'+qualificationIds);
            if(enrolmentIdsforeu.size() > 0 && qualificationIds.size() > 0){
                EnrolmentTriggerHandler.insertEnrolmentUnits(enrolmentIdsforeu, Trigger.New, qualificationIds);
            }
            EnrolmentTriggerHandler.populateStudentStatus(enrolmentIds, eList);//added 01FEB2013 jcanlas
            //Puts Study Reason onto student
            List<Account> students  = new List<Account>([Select Id, Study_Reason_Identifier__c From Account WHERE Id in : StudentWithStudyReason.keySet()]);
            if(students.size() > 0){
                for(Account a : students){
                    if(StudentWithStudyReason.get(a.id) != null){
                        a.Study_Reason_Identifier__c = StudentWithStudyReason.get(a.id).substring(0,2);
                    }
                }
                system.debug('**UPDATE STUDENTS');
                update students;
            }
        }
        if(Trigger.isUpdate){
            system.debug('**EN AFTER UPDATE!');
            //28JUL2014 WBM
            List<Enrolment__c> enr = new List<Enrolment__c>();
            for(Enrolment__c e: Trigger.new){
                enrlMap.put(e.Id, e);
                //enr.add(e);
            }
            
            
            enr = [SELECT Id, Name, Credit_Offered_Value_full_time_load__c, Credit_Used_Value_full_time_load__c, Field_of_Ed_of_prior_VET_Credit_RPL__c, Credit_status_Higher_Ed_provider__c,
                    Enrolment_Status__c, Start_Date__c, End_Date__c, Domestic_School_Leaver__c, Post_code_of_Year_12_Perm_home_residence__c, Name_of_suburb_town_locality__c, Other_Country__c, Mailing_Country__c 
                         FROM Enrolment__c WHERE Id IN: enrlMap.keySet()]; 
            system.debug('**Maps: ' + enr);
                         
            for(Enrolment__c e : /*Trigger.new*/ enrlMap.values()){
                Enrolment__c oldEnrolment = Trigger.oldMap.get(e.Id);
                /*enr = [SELECT Id, Credit_Offered_Value_full_time_load__c, Credit_Used_Value_full_time_load__c, Field_of_Ed_of_prior_VET_Credit_RPL__c, Credit_status_Higher_Ed_provider__c
                         FROM Enrolment__c WHERE Id =: e.Id];*/     

                if(oldEnrolment.Credit_Offered_Value_full_time_load__c!=e.Credit_Offered_Value_full_time_load__c){
                    system.debug('**FORMAT CREDIT VALUE CODE');
                    EnrolmentTriggerHandler.formatCreditOfferedValueCode(e.Credit_Offered_Value_full_time_load__c, String.valueOf(e.Id), enr); //17JUL2014 WBM; added Trigger.New
                }
                
                if(oldEnrolment.Credit_Used_Value_full_time_load__c!=e.Credit_Used_Value_full_time_load__c){
                    system.debug('**FORMAT CREDIT FULL TIME');
                    EnrolmentTriggerHandler.formatCreditUsedValueFullTimeLoad(e.Credit_Used_Value_full_time_load__c, e.Id, enr); //17JUL2014 WBM; added Trigger.New
                }
                
                if(oldEnrolment.Field_of_Ed_of_prior_VET_Credit_RPL__c!=e.Field_of_Ed_of_prior_VET_Credit_RPL__c){
                    system.debug('**FORMAT CREDIT RPL');
                    EnrolmentTriggerHandler.formatFieldOfEdOfPriorVETCreditRPLCode(e.Field_of_Ed_of_prior_VET_Credit_RPL__c, e.Id, enr); //17JUL2014 WBM; added Trigger.New
                }
                
                if(oldEnrolment.Credit_status_Higher_Ed_provider__c!=e.Credit_status_Higher_Ed_provider__c){
                    system.debug('**FORMAT CREDIT PROVIDER');
                    EnrolmentTriggerHandler.formatCreditStatusHigherEdProvider(e.Credit_status_Higher_Ed_provider__c, e.Id, enr); //17JUL2014 WBM; added Trigger.New
                }
                
                if(e.Enrolment_Status__c != oldEnrolment.Enrolment_Status__c){
                    eList.add(e);
                    enrolmentIds.add(e.Id);
                    if(e.Enrolment_Status__c == 'Withdrawn' || e.Enrolment_Status__c == 'Cancelled'){
                        statusIds.add(e.Id);
                        statusList.add(e);
                        studentId.add(e.Student__c); //01AUG2014 WBM
                    }
                }
                if(e.Start_Date__c != oldEnrolment.Start_Date__c || e.Domestic_School_Leaver__c != oldEnrolment.Domestic_School_Leaver__c || e.Post_code_of_Year_12_Perm_home_residence__c != oldEnrolment.Post_code_of_Year_12_Perm_home_residence__c || e.Name_of_suburb_town_locality__c != oldEnrolment.Name_of_suburb_town_locality__c || e.Other_Country__c != oldEnrolment.Other_Country__c || e.Mailing_Country__c != oldEnrolment.Mailing_Country__c){
                    euosIds.add(e.Id);
                    euosList.add(e);
                }
            }
            if(euosList.size() > 0){
                system.debug('**UPDATE EUOS');
                EnrolmentTriggerHandler.updateEUOS(euosIds, euosList);
            }
            if(eList.size() > 0){
                system.debug('**POPULATE STUDENT STATUS');
                EnrolmentTriggerHandler.populateStudentStatus(enrolmentIds, eList);//added 01FEB2013 jcanlas    
            }
            if(statusList.size() > 0){
                system.debug('**DELETE CLASS ENROLMENT');
                EnrolmentTriggerHandler.deleteClassEnrolments(statusIds, statusList, studentId); //01AUG2014 WBM
            }
            /*EnrolmentTriggerHandler.populateIntakeStudentLetterAddress(eIds, enrolmentList);*/
        }
    }
    }
    catch(Exception e){
    }
}