/**
 * @description     Unit Hours and Points Object Trigger After Insert, After Update
 * @author          Janella Canlas
 * @Company         CloudSherpas
 * @date            08.JAN.2013
 *
 * HISTORY
 * - 08.JAN.2013    Janella Canlas      Created.
 * - 04.JUL.2013    Eu Cadag            Code Optimization. Added Set of ID
 * - 2.AUG.2013     YG					Added coded to create units points hours on unit of study
 */
trigger UnitHoursPointsAIAU on Unit_Hours_and_Points__c (after insert, after update) {
	
		Set<Id>unitsOfCompIds = new Set<Id>();
		Set<Id>unitsOfStudyIds = new Set<Id>();
		
		List<Unit_Hours_and_Points__c>newUnitsPointsHoursList = new List<Unit_Hours_and_Points__c>();
		
		for(Unit_Hours_and_Points__c u : Trigger.New){
			if(u.Unit_Record_Type__c == 'Unit_of_Competency')
				unitsOfCompIds.add(u.Unit__c);
		}
		
		List<Unit__c>UnitsToUpdate = [Select Id,Parent_UoC__c From Unit__c
									  WHERE Manually_Override_Hours_and_Points__c = False AND
									   RecordType.DeveloperName = 'Unit_of_Study' AND Parent_UoC__c  In : unitsOfCompIds];
		
		if(Trigger.isUpdate) {
			
			for(Unit__c unit : UnitsToUpdate)
				unitsOfStudyIds.add(unit.Id);
			
			List<Unit_Hours_and_Points__c>unitsAndHoursToDelete = [Select Id From Unit_Hours_and_Points__c Where Unit__c in : unitsOfStudyIds];
			
			delete unitsAndHoursToDelete;
			
			List<Unit_Hours_and_Points__c>unitsAndHoursInDB = [Select u.Unit__c, u.Unit_Record_Type__c, u.Tuition_Fee_Hours__c, u.Points__c, u.Placement_Hours__c, u.Nominal_Hours__c, u.Name, u.Institutional_Hours__c, u.State__c From Unit_Hours_and_Points__c u Where u.Unit__c in :unitsOfCompIds ];
			
			for(Unit__c u : UnitsToUpdate) {
				
				for(Unit_Hours_and_Points__c h : unitsAndHoursInDB) {
		    		if(h.Unit__c == u.Parent_UoC__c) {
		    			Unit_Hours_and_Points__c hours = h.Clone(false);
		    			hours.Unit__c = u.Id;
		    			newUnitsPointsHoursList.add(hours);
		    		}
		    	}
				
			 	
			}
			
			
		}
									   
	    if(Trigger.isInsert) {
	    
		    for(Unit__c u : UnitsToUpdate) {
		    	for(Unit_Hours_and_Points__c h : Trigger.New) {
		    		if(h.Unit__c == u.Parent_UoC__c) {
		    			Unit_Hours_and_Points__c hours = h.Clone(false);
		    			hours.Unit__c = u.Id;
		    			newUnitsPointsHoursList.add(hours);
		    		}
		    	}
		    }
	    }
	    insert newUnitsPointsHoursList;
	
	
    List<Unit_Hours_and_Points__c> uhpList = new List<Unit_Hours_and_Points__c>();
    Set<Id> unitIds = new Set<Id>();
    Set<Id> IDUnits = new Set<Id>();//Added by Eu
    if(!Test.isRunningTest() || UserInfo.getName() == 'UHP_User'){
        if(trigger.isUpdate){
            for(Unit_Hours_and_Points__c u : trigger.new){
                unitIds.add(u.Unit__c);
                uhpList.add(u);
                IDUnits.add(u.Id);//Added by Eu
            }
            //UnitHoursPointsTriggerHandler.updateEnrolmentUnit(unitIds,uhpList); //Comment this one by Eu
            //UnitHoursPointsTriggerHandler.updateIntakeUnitOfStudy(unitIds, uhpList); //Comment this one by Eu
            UnitHoursPointsTriggerHandler.updateEnrolmentUnitLimit(unitIds,IDUnits); //Added by Eu
            UnitHoursPointsTriggerHandler.updateIntakeUnitOfStudyLimit(unitIds,IDUnits); //Added by Eu
        }
    }
    
}