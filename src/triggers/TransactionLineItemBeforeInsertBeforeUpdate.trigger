//    13.10.2014 - Paolo Mendoza - Added populating campus__c from enrolment for insert and update
//    03.12.2014 - Nick Guia - Added null checker on CampusMap

trigger TransactionLineItemBeforeInsertBeforeUpdate on Transaction_Line_Item__c (before insert, before update) {
    
    
    //handles line item inserts
    if(Trigger.isInsert) {
    
        Set<Id> IdTLI = new Set<Id>();
        List<Enrolment__c> Ecampus = new List<Enrolment__c> ();
        
        List<RecordType>ItemRecType = new List<RecordType>([Select Id, Name FROM RecordType WHERE name = 'Item' LIMIT 1]);
        Map<Id,Item__c> Items = new Map<Id,Item__c>([Select Id, Product_Amount__c,Tax_Attributable__c,Total_Product_Amount__c FROM Item__c]);
        Set<Id>accTranIds = new Set<Id>();
        Set<Id>enrlIds = new Set<Id>();
        Map<Id,Account_Transaction__c> accTransMap = new Map<Id,Account_Transaction__c>();
        Map<Id,Enrolment__c> enrlMap = new Map<Id,Enrolment__c>();
        List<Enrolment_Unit__c> EusForUpdate = new List<Enrolment_Unit__c>();
        Set<Id> EUIds = new Set<Id>();
        
               
        
        for(Transaction_Line_Item__c TLI : Trigger.New) {
            IdTLI.add(TLI.Enrolment__c);
            accTranIds.add(TLI.Account_Transaction__c);
            enrlIds.add(TLI.Enrolment__c);
            if(TLI.Enrolment_Unit__c != null)
                EUIds.add(TLI.Enrolment_Unit__c);
            
            if(TLI.RecordTypeId == ItemRecType.get(0).Id) {
                TLI.Tax_Attributable__c = Items.get(TLI.Item__c).Tax_Attributable__c * TLI.Quantity__c;
                TLI.Amount__c = Items.get(TLI.Item__c).Product_Amount__c * TLI.Quantity__c;
            }
            
        }
        //PM
        //Map<Id,Enrolment__c> CampusMap = new Map<Id,Enrolment__c>([Select id, Campus__c from Enrolment__c where id in : IdTLI]);
        //if(!CampusMap.isEmpty()) {
        //    for(Transaction_Line_Item__c TLI : Trigger.New) {
        //        if(Campusmap.containsKey(TLI.Enrolment__c)) {
        //            TLI.Campus__c = Campusmap.get(TLI.Enrolment__c).Campus__c;
        //        }
        //    }
        //}
           

        
            if(EUIds.size() > 0) {
                for(Enrolment_Unit__c e : [Select Id, Invoiced_Date__c FROM Enrolment_Unit__c WHERE Id in : EUIds ]) {
                    e.Invoiced_Date__c = System.today();
                    EusForUpdate.add(e);
                }
            
                update EusForUpdate;
            }
        
        //Need to check that line item is inserted against the correct transaction
        
        if(accTranIds.size() > 0) {
            accTransMap = new Map<Id,Account_Transaction__c>([Select Id, Training_Organisation__c, Name,Payer__r.RecordType.Name, Payer__c From Account_Transaction__c Where Id in : accTranIds]);
            enrlMap = new Map<Id,Enrolment__c>([Select Id, Training_Organization_ID__c, Employer__c,Invoice_Employer__c, Invoice_Funding_Body__c, Funding_Body__c From Enrolment__c WHERE id in : enrlIds]);
            
            //Validation code below
            for(Transaction_Line_Item__c TLI : Trigger.New) {
                if(TLI.Bypass_Enrolment_RTO_APEX_Validation__c != true) {
                    if(enrlMap.ContainsKey(TLI.Enrolment__c) && accTransMap.containsKey(TLI.Account_Transaction__c)){ //make sure key exist
                        if(enrlMap.get(TLI.Enrolment__c).Training_Organization_ID__c != accTransMap.get(TLI.Account_Transaction__c).Training_Organisation__c)
                            TLI.AddError('Training Organisation of Enrolment Selected Does Not Match Training Organisation of Account Transaction');
                    
                        if(TLI.Student__c != accTransMap.get(TLI.Account_Transaction__c).Payer__c && accTransMap.get(TLI.Account_Transaction__c).Payer__r.RecordType.Name == 'Student')
                            TLI.AddError('Student Does Not Match Payer of the Account Transaction');
                            
                        if(accTransMap.get(TLI.Account_Transaction__c).Payer__r.RecordType.Name == 'Business Account') {
                            if((accTransMap.get(TLI.Account_Transaction__c).Payer__c != enrlMap.get(TLI.Enrolment__c).Employer__c) && (accTransMap.get(TLI.Account_Transaction__c).Payer__c != enrlMap.get(TLI.Enrolment__c).Funding_Body__c))
                                TLI.AddError('Employer  Does Not Match Payer of the Account Transaction');
                            if(enrlMap.get(TLI.Enrolment__c).Invoice_Employer__c != true && enrlMap.get(TLI.Enrolment__c).Invoice_Funding_Body__c != true)
                                TLI.AddError('Enrolment you have selected, does not have invoice employer checked. This must be checked before invoice can be created for employer');
                        }
                    }
                }
            }
        }
            
        
    }
    
    //if(Trigger.isUpdate) {
    //    Set<Id> IdTLI = new Set<Id>();
    //    List<Enrolment__c> Ecampus = new List<Enrolment__c> ();
    //    Map<Id,Enrolment__c> CampusMap = new Map<Id,Enrolment__c>();
        
    //    //PM
    //    try{
    //            for(Transaction_Line_Item__c TLI : Trigger.New) {
    //                IdTLI.add(TLI.Enrolment__c);
    //            }
    //            CampusMap = new Map<Id,Enrolment__c>([Select id, Campus__c from Enrolment__c where id in : IdTLI]);
    //            for(Transaction_Line_Item__c TLI : Trigger.New) {
    //                TLI.Campus__c = Campusmap.get(TLI.Enrolment__c).Campus__c;
    //            }
    //       }
    //       catch(exception e){
    //           //
    //       }
        
    //}
         

}