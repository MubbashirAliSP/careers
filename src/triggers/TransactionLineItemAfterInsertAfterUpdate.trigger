trigger TransactionLineItemAfterInsertAfterUpdate on Transaction_Line_Item__c (after insert, after update) {
    
    Set<Id>enrlIdsVic = new Set<Id>();
    TransalctionLineItemHandler tlih = new TransalctionLineItemHandler();

    if(Trigger.isInsert) {
        
        for(Transaction_Line_Item__c TLI : Trigger.New) {
            
            if(TLI.isVicClientFeeOther__c == true) { //Victorian Invoicing only
                enrlIdsVic.add(TLI.Enrolment__c);
            }
            
        }

        if(enrlIdsVic.size() > 0) {
                
           Map<Id,Decimal>EnrolmentSumMap = new Map<Id,Decimal>();
                   
           List<Enrolment_Unit__c>EusToUpdate = new List<Enrolment_Unit__c>();
                  
           Decimal summedAmount = 0.0;
                   
           /*Loop through All enrolments and all Transactional Line Items attached and sum Total Amount
             against each Enrolment */
           
           for(Enrolment__c enrolment : [Select Id, (Select Enrolment__c, Total_Amount__c From Transaction_Line_Items__r Where isVicClientFeeOther__c = True  ) From Enrolment__c where id in : enrlIdsVic]) {
                       
               for(Transaction_Line_Item__c TLIinDB : enrolment.Transaction_Line_Items__r) {
                           
                   summedAmount = summedAmount + TLIinDB.Total_Amount__c;
                           
                }
                       
               EnrolmentSumMap.put(enrolment.Id, summedAmount);
               summedAmount = 0.0; // reset amount
                   
           }
                   
           /*Loop through all Enrolments and all Enrolment Units attached
             devide summed amount for each enrolment by number of EUs attached
             Store the amount against each EU */
                   
           for(Enrolment__c enrolment : [Select id,Number_of_Enrolment_Units__c, (Select id,Client_Fees_Other__c from Enrolment_Units__r) From Enrolment__c where id in : enrlIdsVic] ) {
                       
               for(Enrolment_Unit__c eu : enrolment.Enrolment_Units__r) {
                           
                   eu.Client_Fees_Other__c = EnrolmentSumMap.get(enrolment.Id) / enrolment.Number_of_Enrolment_Units__c;
                           
                   EusToUpdate.add(eu);
               }
                    
           }

           update EusToUpdate;          
        }       
        tlih.onInsert(trigger.new);
    }

    if(Trigger.isUpdate) {
        tlih.onUpdate(trigger.new, trigger.old);
    }
}