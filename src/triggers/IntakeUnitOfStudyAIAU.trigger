trigger IntakeUnitOfStudyAIAU on Intake_Unit_of_Study__c (after insert, after update) {
	List<Intake_Unit_of_Study__c> iusList = new List<Intake_Unit_of_Study__c>();
	Set<Id> iusIds = new Set<Id>();
	if(Trigger.isInsert){
		for(Intake_Unit_of_Study__c i : Trigger.new){
			iusList.add(i);
			iusIds.add(i.Id);
		}
		IntakeUnitOfStudyTriggerHandler.insertTuitionFees(iusIds, iusList);
	}
	if(Trigger.isUpdate){
		for(Intake_Unit_of_Study__c i : Trigger.new){
			iusList.add(i);
			iusIds.add(i.Id);
		}
		IntakeUnitOfStudyTriggerHandler.updateTuitionFees(iusIds, iusList);
	}
}