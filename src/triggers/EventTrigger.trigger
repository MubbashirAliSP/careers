/**
 * @description Trigger for Service Case object
 * @author Ranyel Maliwanag
 * @date 14.JUN.2016
 */
trigger EventTrigger on Event (after insert, after update, after delete) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			EventTriggerHandler.onAfterInsert(trigger.new);
		}

		if (trigger.isUpdate) {
			EventTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}

		if (trigger.isDelete) {
			EventTriggerHandler.onAfterDelete(trigger.old);
		}
	}
}