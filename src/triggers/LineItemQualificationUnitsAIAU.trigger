trigger LineItemQualificationUnitsAIAU on Line_Item_Qualification_Units__c (after insert, after update) {
	Set<Id> liquIds = new Set<Id>();
	List<Line_Item_Qualification_Units__c> liquList = new List<Line_Item_Qualification_Units__c>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'LIQ_User'){
		if(Trigger.isUpdate){
			for(Line_Item_Qualification_Units__c l : Trigger.new){
				liquIds.add(l.Id);
				liquList.add(l);
			}
			LineItemQualificationUnitsTriggerHandler.populateUnitValue(liquIds, liquList);
		}
	}
}