/**
 * @description Trigger definition for Enrolment Letters(Correspondence) Object
 * @author Nick Guia
 * @date 04.AUG.2015
 */
trigger EnrolmentLettersTrigger on Enrolment_Letters__c (before insert, after insert, after update, after delete) {
	if (trigger.isBefore) {
        if (trigger.isInsert) {
            EnrolmentLettersTriggerHandler.onBeforeInsert(Trigger.new);
        }
    }
}