/**
 * @description Trigger definition for the `EntryRequirement__c` object
 * @author Ranyel Maliwanag
 * @date 27.JAN.2016
 */
trigger EntryRequirementTrigger on EntryRequirement__c (before update, after insert, after update) {
	if (trigger.isBefore) {
		if (trigger.isUpdate) {
			EntryRequirementTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
		}
	}
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			EntryRequirementTriggerHandler.onAfterInsert(trigger.new);
		}

		if (trigger.isUpdate) {
			EntryRequirementTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}