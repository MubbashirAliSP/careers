/**
 * @description Enrolment object Delete Trigger 
 * @author      
 * @Company     CloudSherpas
 * @date        
 *
 * HISTORY
 * - 01.DEC.2014    Sairah Hadjinoor      Updated to follow bulk trigger handler 
 *                                        
 */

trigger EnrolementDeletedTrigger on Enrolment__c (before delete, after delete) {

    if(Trigger.isBefore){
        Set<Id> ids = Trigger.oldMap.keySet();
        delete [ select id from Enrolment_History__c Where Enrolment__c in :ids ];
        delete [ select id from Fee_Exemption__c Where Enrolment__c in :ids ];
    }

    if(Trigger.isAfter){
    //S.Hadjinoor 02.12.2014
        EnrolmentTriggerHandler.deleteDualEnrolment(Trigger.Old);
    }
}