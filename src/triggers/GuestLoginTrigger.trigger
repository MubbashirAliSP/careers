/**
 * @description Trigger definition for the `GuestLogin__c` object
 * @author Ranyel Maliwanag
 * @date 29.JUL.2015
 */
trigger GuestLoginTrigger on GuestLogin__c (before insert, before update) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            GuestLoginTriggerHandler.onBeforeInsert(trigger.new);
        }

        if (trigger.isUpdate) {
        	GuestLoginTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
}