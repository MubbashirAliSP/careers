/**
 * @description Trigger definition for the `ApplicationForm__c` object
 * @author Ranyel Maliwanag
 * @date 17.AUG.2015
 */
trigger ApplicationFormTrigger on ApplicationForm__c (before insert, before update, after insert, after update) {
    if (trigger.isBefore) {
    	if (trigger.isInsert) {
    		ApplicationFormTriggerHandler.onBeforeInsert(trigger.new);
    	}
        if (trigger.isUpdate) {
            ApplicationFormTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }

    if (trigger.isAfter) {
        if (trigger.isInsert) {
            ApplicationFormTriggerHandler.onAfterInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            ApplicationFormTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}