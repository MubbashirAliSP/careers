/**
 * @description Trigger definition for the Case object
 * @author Ranyel Maliwanag
 * @date 04.AUG.2015
 */
trigger CaseTrigger on Case (before insert, before update, after insert, after update, after delete) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            CaseTriggerHandler.onBeforeInsert(Trigger.new);
        }

        if (trigger.isUpdate) {
            CaseTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            CaseTriggerHandler.onAfterInsert(Trigger.new);
        }

        if (trigger.isUpdate) {
            CaseTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }

        if (trigger.isDelete) {
            CaseTriggerHandler.onAfterDelete(Trigger.old);
        }
    }
}