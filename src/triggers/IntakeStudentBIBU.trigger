/**
 * @description Intake Student Trigger Before Insert Before Update 
 * @author 		Janella Canlas
 * @Company		CloudSherpas
 * @date 		31.JAN.2013
 *
 * HISTORY
 * - 31.JAN.2013	Janella Canlas		Created.
 * - 04.FEB.2013	Janella Canlas		Comment out codes to populateIntakeLetterAddress
 */
trigger IntakeStudentBIBU on Intake_Students__c (before insert, before update) {
	List<Intake_Students__c> intakeStudentList = new List<Intake_Students__c>();
	if(Trigger.isInsert){
		for(Intake_Students__c i : Trigger.new){
			intakeStudentList.add(i);
		}
		IntakeStudentTriggerHandler.populateMobileNumber(intakeStudentList);
		/*IntakeStudentTriggerHandler.populateIntakeLetterAddress(intakeStudentList);*/
	}
	if(Trigger.isUpdate){
		/*for(Intake_Students__c i : Trigger.new){
			intakeStudentList.add(i);
		}
		IntakeStudentTriggerHandler.populateIntakeLetterAddress(intakeStudentList);*/
	}
}