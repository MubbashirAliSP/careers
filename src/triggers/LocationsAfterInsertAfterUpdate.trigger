trigger LocationsAfterInsertAfterUpdate on Locations__c (before insert, before update) {

	private final Id RecordTypeId = [Select Id From RecordType Where DeveloperName = 'Parent_Location' AND SobjectType = 'Locations__c'].Id;

	for (Locations__c so : Trigger.new) {
		

		if(so.RecordTypeId == RecordTypeId) { /*parent location recordtypeids only */

			 if(so.Address_building_property_name__c != null && so.Address_flat_unit_details__c != null && so.Address_street_number__c != null && so.Address_street_name__c != null )    
                so.Address_Location__c = so.Address_building_property_name__c + ', ' + so.Address_flat_unit_details__c + ', ' + so.Address_street_number__c + ' ' + so.Address_street_name__c;

             else {

             	 String building = '';
                 String Flat = '';
                 String snumber = '';
                 String sname = '';

             	 if(so.Address_building_property_name__c != null)
                    building = so.Address_building_property_name__c;
                 if(so.Address_flat_unit_details__c != null)
                    flat = so.Address_flat_unit_details__c;
                 if(so.Address_street_number__c != null)
                    sNumber = so.Address_street_number__c;
                 if(so.Address_street_name__c != null)
                    sname = so.Address_street_name__c;
                 
                 so.Address_Location__c = building + ', ' + flat + ', ' + snumber + ' ' + sname;
                 so.Address_Location__c = so.Address_Location__c.removeStart(', ');
                 so.Address_Location__c = so.Address_Location__c.ReplaceAll(', , ', ' ');
                 so.Address_Location__c = so.Address_Location__c.removeStart(',');
                 so.Address_Location__c = so.Address_Location__c.Trim();

             }
		}

	}

}