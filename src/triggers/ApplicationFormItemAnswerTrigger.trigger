trigger ApplicationFormItemAnswerTrigger on ApplicationFormItemAnswer__c (before insert, before update, after insert, after update) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            ApplicationFormItemAnswerTriggerHandler.onBeforeInsert(trigger.new);
        }

        if (trigger.isUpdate) {
            ApplicationFormItemAnswerTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }

    if (trigger.isAfter) {
    	if (trigger.isInsert) {
    		ApplicationFormItemAnswerTriggerHandler.onAfterInsert(trigger.new);
    	}

    	if (trigger.isUpdate) {
    		ApplicationFormItemAnswerTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
    	}
    }
}