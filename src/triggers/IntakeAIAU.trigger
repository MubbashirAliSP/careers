/**
 * @description Intake Object Trigger Before Insert, Before Update
 * @author      Janella Lauren Canlas
 * @Company     CloudSherpas
 * @date        22.JAN.2013
 *
 * HISTORY
 * - 22.JAN.2013    Janella Canlas      Created.
 * - 19.JUL.2013	Patrisha Sales		Changed condition on event update to use Start Date and End Date instead of Census Date
 */
trigger IntakeAIAU on Intake__c (after insert, after update) {
    List<Intake__c> intakeList = new List<Intake__c>();
    Set<Id> intakeIdSet = new Set<Id>();
    if(!Test.isRunningTest() || UserInfo.getName() == 'Intake_User'){
        if(Trigger.isInsert){
            for(Intake__c i : Trigger.new){
                    intakeList.add(i);
                    intakeIdSet.add(i.Id);
            }
            if(intakeList.size() > 0){
                IntakeTriggerHandler.populateEUOSCensusDate(intakeIdSet, intakeList);
            }
        }
        if(Trigger.isUpdate){
            for(Intake__c i : Trigger.new){
                Intake__c oldInt = Trigger.oldMap.get(i.Id);
                //updated condition since original condition used Census Date which is actually a formula field - Patrisha Sales
                if(oldInt.Start_Date__c != i.Start_Date__c || oldInt.End_Date__c != i.End_Date__c){
                    intakeList.add(i);
                    intakeIdSet.add(i.Id);
                }
            }
            if(intakeList.size() > 0){
                IntakeTriggerHandler.populateEUOSCensusDate(intakeIdSet, intakeList);
            }   
        }
    }
    
}