/**
 * @description 	Awards Object Trigger
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			06.FEB.2013
 * @history
 * 		06.FEB.2013		Janella Canlas		Created.
 * 		20.JAN.2016		Biao Zhang			Create Award Units when Award is approved
 */
Trigger AwardTrigger on Awards__c (before insert, before update, after update) {
	if (Trigger.isBefore) {
        if (Trigger.isInsert) {
        	AwardsTriggerHandler.avoidDuplicateCertificate(Trigger.new);
        	AwardsTriggerHandler.populateReportForAvetmiss(Trigger.new);
        }

        if (Trigger.isUpdate) {
        }

        if (Trigger.isDelete) {
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
        }

        if (Trigger.isUpdate) {
        	AwardsTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }

        if (Trigger.isDelete) {
        }
    }
}