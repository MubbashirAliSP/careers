/**
 * @description 	Qualification Unit Object Trigger Before Insert, Before Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			27.NOV.2012
 *
 * HISTORY
 * - 27.NOV.2012	Janella Canlas		Created.
 */
trigger QualificationUnitBIBU on Qualification_Unit__c (before insert, before update) {
	List<Qualification_Unit__c> quList = new List<Qualification_Unit__c>();
	//Set<Id> quChecker = new Set<Id>();
	//Map<Id,Set<Id>> qualMap = new Map<Id,Set<Id>>();
	
	if(!Test.isRunningTest() || UserInfo.getName() == 'qUnit_User'){
		if(Trigger.isInsert){
			for(Qualification_Unit__c q : Trigger.new){
				quList.add(q);
				/*quChecker.add(q.Unit__c);
				if(qualMap.containsKey(q.Qualification__c)){
					Set<Id> temp = qualMap.get(q.Qualification__c);
					temp.add(q.Unit__c);
					qualMap.put(q.Qualification__c, temp);
				}
				else{
					qualMap.put(q.Qualification__c, new Set<Id>{q.Unit__c});
				}*/
			}
			/*for(Qualification_Unit__c q : Trigger.new){
				if(qualMap.get(q.Qualification__c).contains(q.Unit__c)){
					q.addError('You cannot create Qualification Units with the same Unit.');
				}
			}*/
			QualificationUnitTriggerHandler.validateQualificationUnits(quList);
			QualificationUnitTriggerHandler.updateUnitRecordType(quList);
		}
		if(Trigger.isUpdate){
			for(Qualification_Unit__c q : Trigger.new){
				quList.add(q);
			}
			QualificationUnitTriggerHandler.updateUnitRecordType(quList);
		}
	}
}