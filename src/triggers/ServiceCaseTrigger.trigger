/**
 * @description Trigger for Service Case object
 * @author Ranyel Maliwanag
 * @date 29.MAY.2015
 */
trigger ServiceCaseTrigger on Service_Cases__c (before insert, before update, after insert, after update) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            ServiceCaseTriggerHandler.onBeforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            ServiceCaseTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }

    if (trigger.isAfter) {
        if (trigger.isInsert) {
            ServiceCaseTriggerHandler.onAfterInsert(trigger.new);
        }
    	if (trigger.isUpdate) {
    		ServiceCaseTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
    	}
    }
}