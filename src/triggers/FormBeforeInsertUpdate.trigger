trigger FormBeforeInsertUpdate on Form__c (before insert, before update) {
    for(Form__c f: trigger.new) {
        if(f.Same_as_Residential__c) {
            f.Postal_Street_Address__c = f.Residential_Street_Address__c;
            f.Postal_State__c = f.Residential_State__c;
            f.Postal_Postcode__c = f.Residential_Postcode__c;
            f.Postal_Town_Suburb__c = f.Residential_Town_Suburb__c;
        }
    }
}