/**
 * @description 	Class Enrolment Trigger After Insert, After Update, Before Delete
 * @author 			Janella Lauren Canlas
 * @Company			CloudSherpas
 * @date 			11.JAN.2013
 *
 * HISTORY
 * - 11.JAN.2013	Janella Canlas		Created.
 */
trigger ClassEnrolmentAIAU on Class_Enrolment__c (after insert, after update, before delete) {
	
	List<Class_Enrolment__c> ceList = new List<Class_Enrolment__c>();
	Set<Id> intakeIds = new Set<Id>();
	
	if(Trigger.isInsert){
		for(Class_Enrolment__c c : Trigger.new){
			ceList.add(c);
		}
		//ClassEnrolmentTriggerHandler.insertIntakeStudents(ceList);
	}
	
	if(Trigger.isDelete){
		for(Class_Enrolment__c c : Trigger.old){
			ceList.add(c);
		}
		ClassEnrolmentTriggerHandler.deleteIntakeStudents(ceList);
	}
}