/**
 * @description 	Purchasing Contract Object Trigger After Insert, After Update
 * @author 			Janella Canlas
 * @Company			CloudSherpas
 * @date 			26.NOV.2012
 *
 * HISTORY
 * - 26.NOV.2012	Janella Canlas		Created.
 */
trigger PurchasingContractAIAU on Purchasing_Contracts__c (after insert, after update) {
	
	List<Purchasing_Contracts__c> pcList = new List<Purchasing_Contracts__c>();
	List<Purchasing_Contracts__c> pcUpdatedList = new List<Purchasing_Contracts__c>();
	Set<Id> pcIds = new Set<Id>();
	if(!Test.isRunningTest() || UserInfo.getName() == 'PCON_User'){
		if(Trigger.isInsert){
			//nothing yet
		}
		if(Trigger.isUpdate){
			for(Purchasing_Contracts__c p : Trigger.new){
				if(p.RecordTypeId != Trigger.oldMap.get(p.Id).RecordTypeId){
					pcList.add(p);
				}
				if(p.Contract_Type__c != Trigger.oldMap.get(p.Id).Contract_Type__c){
					pcIds.add(p.Id);
					pcUpdatedList.add(p);
				}
			}
			if(pcList.size() > 0){
				PurchasingContractTriggerHandler.updateRecordTypes(pcList);
			}
			if(pcIds.size() > 0){
				PurchasingContractTriggerHandler.populateContractType(pcIds, pcUpdatedList);
			}
		}
	}
	
}