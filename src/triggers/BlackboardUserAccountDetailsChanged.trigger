/*This trigger maintains changes done to the account and sets the flag
 *on BB user that indicates that re -sync with blackboard is required
 *written by Yuri Gribanov - Cloudsherpas 23 July 2014
*/


trigger BlackboardUserAccountDetailsChanged on Account (before update) {

	Set<Id>accids = new Set<Id>();
	List<Blackboard_User__c>bbusersToUpdate = new List<Blackboard_User__c>();

	for(Account a : Trigger.New) {
		//user details changed
		if(a.PersonBirthdate != Trigger.OldMap.get(a.id).PersonBirthdate ||
		   a.PersonEmail != Trigger.OldMap.get(a.id).PersonEmail ||
		   a.FirstName != Trigger.OldMap.get(a.id).FirstName || 
		   a.LastName != Trigger.OldMap.get(a.id).LastName ||
		   a.PersonMobilePhone != Trigger.OldMap.get(a.Id).PersonMobilePhone ) {


			accids.add(a.Id);
		}
	}

	//query bb user and set the flag for re synching with blackboard
	if(!accids.isEmpty()) {
		for(Blackboard_User__c bbuser : [Select Changed_Since_Last_Sync__c From Blackboard_User__c Where Account__c in : accids AND RecordType.DeveloperName = 'Student']) {
			bbuser.Changed_Since_Last_Sync__c = true;
			bbusersToUpdate.add(bbuser);
		}

		update bbusersToUpdate;
	}
}