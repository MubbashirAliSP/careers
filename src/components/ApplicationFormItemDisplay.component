<apex:component controller="ApplicationFormItemDisplayCC">
    <apex:attribute assignTo="{!formItemAnswerRecord}" name="formItemAnswer" type="ApplicationFormItemAnswer__c" description="The ApplicationFormItemAnswer record that needs to be rendered" />
    <apex:attribute assignTo="{!formItemRecord}" name="formItem" type="ApplicationFormItem__c" description="The ApplicationFormItem record that needs to be rendered" />
    <apex:attribute assignTo="{!isSubmitted}" name="isFormSubmitted" type="Boolean" description="The ApplicationForm record submission status" />
    <apex:attribute assignTo="{!fundingStreamId}" name="formFundingStreamId" type="Id" description="Form funding stream Id" />
    <apex:attribute assignTo="{!qualificationId}" name="formQualificationId" type="Id" description="Form qualification Id" />
    <!-- Editable displays -->
    <apex:outputPanel rendered="{!NOT(isReadOnly)}">
        <!-- Form type: Text -->
        <apex:outputPanel rendered="{!formItem.QuestionType__c = 'Text'}">
            <apex:outputPanel layout="block" styleClass="item-help">
                <apex:outputField value="{!formItem.HelpText__c}"></apex:outputField>
            </apex:outputPanel>
            <div>
                <apex:inputField html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!formItemAnswer.Answer__c}" label="{!formItem.DisplayText__c}" />
            </div>
        </apex:outputPanel>

        <!-- Form type: Checkbox -->
        <apex:outputPanel rendered="{!formItem.QuestionType__c = 'Checkbox'}">
            <apex:outputPanel layout="block" styleClass="item-help">
                <apex:outputField value="{!formItem.HelpText__c}"></apex:outputField>
            </apex:outputPanel>
            <apex:inputCheckbox html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!formItemAnswer.Answer__c}" label="{!formItem.DisplayText__c}" />
        </apex:outputPanel>

        <!-- Form type: Lookup -->
        <apex:outputPanel rendered="{!AND(formItem.QuestionType__c = 'Lookup', NOT(isRenderAsDropdown))}">
            <apex:selectCheckboxes html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!lookupAnswer}" label="{!formItem.DisplayText__c}" styleClass="apex-checkbox" layout="{!layout}">
                <apex:selectOptions value="{!lookupChoices}"></apex:selectOptions>
            </apex:selectCheckboxes>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!AND(formItem.QuestionType__c = 'Lookup', isRenderAsDropdown)}">
            <apex:selectList html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!lookupSelectAnswer}" size="10" label="{!formItem.DisplayText__c}" styleClass="apex-checkbox">
                <apex:selectOptions value="{!lookupChoices}"></apex:selectOptions>
            </apex:selectList>
        </apex:outputPanel>

        <!-- Form type: Fixed Dropdown -->
        <apex:outputPanel rendered="{!AND(formItem.QuestionType__c = 'Dropdown', !hasOtherOptions)}">
            <apex:selectCheckboxes html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!dropdownAnswer}" label="{!formItem.DisplayText__c}" styleClass="apex-checkbox" layout="{!layout}">
                <apex:selectOptions value="{!formChoices}"></apex:selectOptions>
            </apex:selectCheckboxes>
            <apex:outputPanel layout="block" styleClass="item-help">
                <apex:outputField value="{!formItem.HelpText__c}"></apex:outputField>
            </apex:outputPanel>
        </apex:outputPanel>

        <!-- Form type: Dropdown with Other -->
        <apex:outputPanel rendered="{!AND(formItem.QuestionType__c = 'Dropdown', hasOtherOptions)}">
            <apex:selectCheckboxes html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!dropdownAnswer}" label="{!formItem.DisplayText__c}" styleClass="apex-checkbox" layout="{!layout}">
                <apex:selectOptions value="{!formChoices}"></apex:selectOptions>
            </apex:selectCheckboxes>
            <apex:inputText value="{!otherChoice}" rendered="{!layout = 'lineDirection'}"></apex:inputText>
            
            <apex:outputPanel layout="block" rendered="{!layout = 'pageDirection'}" styleClass="other-item-holder">
                <apex:inputText value="{!otherChoice}"></apex:inputText>
            </apex:outputPanel>
        </apex:outputPanel>

        <!-- Form type: Multiple-select Dropdown -->
        <apex:selectList html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!multiSelectOptions}" label="{!formItem.DisplayText__c}" rendered="{!formItem.QuestionType__c = 'Multiple-select Dropdown'}" multiselect="true">
            <apex:selectOptions value="{!formChoices}"></apex:selectOptions>
        </apex:selectList>

        <!-- Form type: Date -->
        <apex:inputField html-data-itemId="{!formItem.Id}" html-data-itemKey="{!formItem.Name}" required="{!formItem.IsRequired__c}" value="{!formItemAnswer.DateAnswer__c}" label="{!formItem.DisplayText__c}" rendered="{!formItem.QuestionType__c = 'Date'}" showDatePicker="true" />

        <!-- Form type: Encrypted -->
        <apex:inputText required="{!formItem.IsRequired__c}" value="{!encryptedAnswer}" label="{!formItem.DisplayText__c}" rendered="{!formItem.QuestionType__c = 'Encrypted'}" />
    </apex:outputPanel>

    <!-- Read only displays -->
    <apex:outputPanel rendered="{!isReadOnly}">
        <apex:outputField value="{!formItemAnswer.Answer__c}" rendered="{!AND(NOT(formItem.QuestionType__c = 'Date'), NOT(isAlternateAvailable))}"></apex:outputField>
        <apex:outputField value="{!formItemAnswer.ReadableAnswer__c}" rendered="{!AND(NOT(formItem.QuestionType__c = 'Date'), isAlternateAvailable)}"></apex:outputField>
        <apex:outputField value="{!formItemAnswer.DateAnswer__c}" rendered="{!formItem.QuestionType__c = 'Date'}"></apex:outputField>
    </apex:outputPanel>
</apex:component>